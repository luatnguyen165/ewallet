const DbService = require('moleculer-db');
const MongooseAdapter = require('moleculer-db-adapter-mongoose');
const MongooseAction = require('moleculer-db-adapter-mongoose-action');
const eWalletMonitorModel = require('./model/eWalletMonitor.model');

module.exports = {
	name: 'eWalletMonitorModel',

	version: 1,

	mixins: [DbService],

	adapter: new MongooseAdapter(process.env.FE_MONGO_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		keepAlive: true

	}),

	model: eWalletMonitorModel,

	/**
   * Settings
   */
	settings: {
	},

	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: MongooseAction(),

	/**
   * Events
   */
	events: {

	},

	/**
   * Methods
   */
	methods: {
	},

	/**
   * Service created lifecycle event handler
   */
	created() {

	},

	/**
   * Service started lifecycle event handler
   */
	started() { },

	/**
   * Service stopped lifecycle event handler
   */

	// async stopped() {},

	async afterConnected() {
		this.logger.info('Connected successfully...');
	}
};
