const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		default: null
	},
	fullname: {
		type: String
	},
	phone: {
		type: String,
		required: true
	},
	amount: {
		type: Number,
		default: 0
	},
	fee: {
		type: Number,
		default: 0
	},
	total: {
		type: Number,
		default: 0
	},
	bulkId: {
		type: String,
		default: null
	},
	orderId: {
		type: Number,
		default: null
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.ISEC_STATE),
		default: null,
		required: true
	},
	donatedInfo: {
		_id: false,
		accountId: {
			type: Number
		},
		fullname: {
			type: String
		},
		phone: {
			type: String
		},
		createdAt: {
			type: Date
		}
	},
	transaction: {
		type: String,
		required: true,
		unique: true
	},
	partnerTransaction: { // gateTransactionId cũ, sửa lại vì DMS cũng dùng cho nhìu đối tác khác
		type: String,
		default: null
	},
	description: {
		type: String,
		default: null
	},
	scratchInfo: {
		_id: false,
		partnerId: {
			type: Number,
			default: null
		},
		accountId: {
			type: Number,
			default: null
		},
		phone: String,
		title: {
			type: String
		},
		createdAt: {
			type: Date
		},
		username: {
			type: String,
			default: null
		},
		description: String
	},
	iSec: {
		_id: false,
		encryptString: {
			type: String
		},
		hint: {
			type: String
		},
		checksum: {
			type: String,
			unique: true,
			sparse: true
		}
	},
	trace: [{
		_id: false,
		accountId: {
			type: Number
		},
		phone: {
			type: String
		},
		fullname: {
			type: String
		},
		transaction: {
			type: String
		},
		iSec: {
			hint: {
				type: String
			}
		},
		createdAt: {
			type: Date
		}
	}],
	scope: [String],
	history: [String],
	source: {
		type: String,
		default: GeneralConstant.ISEC_SOURCE.CREATED,
		enum: _.values(GeneralConstant.ISEC_SOURCE)
	},
	method: {
		type: Object,
		default: null
	},
	payment: {
		id: {
			type: Number,
			default: 0
		},
		transaction: {
			type: String,
			default: null
		},
		method: {
			type: String,
			default: null
		},
		state: {
			type: String,
			default: null
		},
		description: {
			type: String,
			default: null
		}
	}
}, {
	collection: 'Service_ISec',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ phone: 1 }, { sparse: false, unique: false });
Schema.index({ transaction: 1 }, { sparse: true, unique: true });
Schema.index({ orderId: 1 }, { sparse: false, unique: false, index: true });
Schema.index({ 'iSec.checksum': 1, transaction: 1, bulkId: 1, state: 1, accountId: 1, 'iSec.hint': 1, id: 1 }, { index: true });
/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
