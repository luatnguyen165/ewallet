const moleculerI18n = require('moleculer-i18n-js');
const path = require('path');
const GeneralConstant = require('./constants/general.constant');

module.exports = {
	name: 'eWalletHistory',

	version: 1,

	mixins: [moleculerI18n],

	i18n: {
		directory: path.join(__dirname, 'locales'),
		locales: ['vi', 'en'],
		defaultLocale: 'vi'
	},

	/**
	 * Settings
	 */
	settings: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		getDetailHistory: {
			rest: {
				method: 'POST',
				fullPath: '/ewallet/detailHistory',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					transaction: 'string'
				}
			},
			handler: require('./actions/getDetailHistory.action')
		},
		getHistory: {
			rest: {
				method: 'POST',
				fullPath: '/ewallet/history',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					filter: {
						$$type: 'object',
						transaction: 'string|optional',
						state: {
							type: 'array',
							items: {
								type: 'string',
								enum: [
									GeneralConstant.STATE.PENDING,
									GeneralConstant.STATE.SUCCEEDED,
									GeneralConstant.STATE.FAILED
								]
							},
							default: [
								GeneralConstant.STATE.PENDING,
								GeneralConstant.STATE.SUCCEEDED,
								GeneralConstant.STATE.FAILED
							]
						},
						changed: 'string|optional',
						type: 'string|optional',
						startDate: 'string|optional',
						endDate: 'string|optional'
					},
					paging: {
						$$type: 'object',
						start: 'number|optional|default: 0',
						limit: 'number|optional|default: 10'
					},
					option: {
						$$type: 'object',
						isDetail: 'boolean|default: false'
					}
				}
			},
			handler: require('./actions/getHistory.action')
		},
		create: {
			params: {
				dataHistory: {
					$$type: 'object',
					accountId: 'number',
					service: {
						$$type: 'object',
						type: 'string',
						id: 'number',
						transaction: 'string',
						state: 'string'
					},
					amount: 'number',
					fee: 'number|default:0',
					discount: 'number|default:0',
					cashback: 'number|default:0',
					bonus: 'number|default:0',
					total: 'number',
					state: 'string',
					payment: {
						$$type: 'object|optional',
						id: 'number',
						transaction: 'string',
						method: 'string',
						state: 'string',
						description: 'string|optional'
					},
					transport: {
						$$type: 'object|optional',
						transportId: 'number',
						accountId: 'number|optional',
						transaction: 'string',
						method: 'string',
						state: 'string'
					}
				}
			},
			handler: require('./actions/create.action')
		},
		update: {
			params: {
				fields: {
					$$type: 'object'
				},
				updateInfo: {
					$$type: 'object'
				}
			},
			handler: require('./actions/update.action')
		},
		historyList: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/sdk/history/list',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					filter: {
						$$type: 'object',
						// transaction: 'string|optional',
						// state: {
						// 	type: 'array',
						// 	items: {
						// 		type: 'string',
						// 		enum: [
						// 			GeneralConstant.STATE.PENDING,
						// 			GeneralConstant.STATE.SUCCEEDED,
						// 			GeneralConstant.STATE.FAILED
						// 		]
						// 	},
						// 	default: [
						// 		GeneralConstant.STATE.PENDING,
						// 		GeneralConstant.STATE.SUCCEEDED,
						// 		GeneralConstant.STATE.FAILED
						// 	]
						// },
						// changed: 'string|optional',
						type: 'string|optional',
						startDate: 'string|optional',
						endDate: 'string|optional'
					},
					paging: {
						$$type: 'object|optional',
						start: 'number|optional|default: 0',
						limit: 'number|optional|default: 10'
					},
					opts: {
						$$type: 'object|optional',
						isDetail: 'boolean|default: false'
					}
				}
			},
			handler: require('./actions/historyList.action.rest')
		},
		historyDetail: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/sdk/history/detail',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					transaction: 'string'
				}
			},
			handler: require('./actions/historyDetail.action.rest')
		}
	},

	async started() {
	},

	/**
	* Events
	*/
	events: {},

	/**
	* Methods
	*/
	methods: {
		GetPaymentMethod: require('./methods/getPaymentMethod.method'),
		GetServiceName: require('./methods/getServiceName.method'),
		GetBillDetail: require('./methods/bill.method'),
		GetMobileCardDetail: require('./methods/mobileCard.method'),
		GetMobileTopupDetail: require('./methods/mobileTopup.method'),
		GetIsecDetail: require('./methods/isec.method'),
		GetDepositDetail: require('./methods/deposit.method'),
		GetWithdrawDetail: require('./methods/withdraw.method'),
		GetRefundMoneyDetail: require('./methods/refundMoney.method'),
		GetTransferMoneyDetail: require('./methods/transferMoney.method'),
		GetSocialPaymentDetail: require('./methods/socialPayment.method'),
		GetSalaryDetail: require('./methods/salary.method'),
		GetOpenEwalletPaymentDetail: require('./methods/openEwalletPayment.method'),
		GetWalletPayDetail: require('./methods/walletPay.method'),
		GetWalletQRDetail: require('./methods/walletQr.method'),
		GetPayQrCodeDetail: require('./methods/payQRcode.method'),
		GetPayCreditStatementDetail: require('./methods/creditStatement.method'),
		GetAdvanceMoneyDetail: require('./methods/advanceMoney.method'),
		GetDefaultDetail: require('./methods/default.method'),
		MethodService: require('./methods/methodService.method'),
		GetDetailHistory: require('./methods/getDetailHistory.method'),
		GetIcon: require('./methods/getIcon.method')
	}
};
