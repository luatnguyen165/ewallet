module.exports = {
	ISEC: {
		isHidden: false,
		default: false,
		disable: false,
		title: 'iSec',
		subTitle: 'iSec',
		items: [
		]
	},
	POWE: {
		isHidden: false,
		default: false,
		disable: false,
		title: 'Điện – Power bill',
		subTitle: 'Điện',
		items: [
			{
				fullname: 'Điện lực TPHCM – HCM EVN',
				code: 'EVNHCM',
				isHidden: true,
				gateway: 'OCB',
				default: false,
				disable: false
			},

			{
				fullname: 'Điện lực Miền Nam',
				code: 'EVNMN',
				isHidden: true,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				fullname: 'Điện lực các tỉnh khác',
				code: 'VNPAYEVN',
				isHidden: true,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				fullname: 'Điện lực Hà Nội – Ha Noi EVN',
				code: 'EVNHANOI',
				isHidden: true,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				fullname: 'Điện lực Miền Trung',
				code: 'EVN-MIENTRUNG',
				isHidden: true,
				default: false,
				gateway: 'OCB',
				disable: false
			}
			// {
			//   fullname: 'Điện lực TPHCM',
			//   code: 'EVNHCM',
			//   isHidden: false,gateway:'ESTIO',
			//   default: true,
			//   disable: false,
			// },

			// {
			//   fullname: 'Điện lực Hà Nội',
			//   code: 'EVNHN',
			//   isHidden: false,
			//   default: false,gateway:'ESTIO',
			//   disable: false,
			// },
			// {
			//   fullname: 'Điện lực Miền Bắc',
			//   code: 'EVNMIENBAC',
			//   isHidden: false,
			//   default: false,gateway:'ESTIO',
			//   disable: false,
			// },
			// {
			//   fullname: 'Điện lực Miền Nam',
			//   code: 'EVNMN',
			//   isHidden: false,
			//   default: false,gateway:'ESTIO',
			//   disable: false,
			// },
			// {
			//   fullname: 'Điện lực các tỉnh khác',
			//   code: 'VNPAYEVN',
			//   isHidden: false,
			//   default: false,gateway:'ESTIO',
			//   disable: false,
			// }
		]
	},
	WATE: {
		isHidden: false,
		default: false,
		disable: false,
		title: 'Nước – Water bill',
		subTitle: 'Nước',
		items: [
			// {
			//   code: 'NUOC-BENTHANH',
			//   fullname: 'Cấp nước Bến Thành',
			//   isHidden: false, default: false,gateway:'OCB', disable: false
			// },
			{
				code: 'NUOC-CHOLON',
				fullname: 'Cấp nước Chợ Lớn',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-CUCHI',
				fullname: 'Cấp nước Củ Chi',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-BINHCHANH',
				fullname: 'Cấp nước Bình Chánh',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-BINHDUONG',
				fullname: 'Cấp nước Bình Dương',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-ANGIANG',
				fullname: 'Cấp nước An Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-VUNGTAU',
				fullname: 'Cấp nước Bà Rịa Vũng Tàu',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},

			{
				code: 'NUOC-BENTRE',
				fullname: 'Cấp nước Bến Tre',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},

			{
				code: 'NUOC-BINHTHUAN',
				fullname: 'Cấp nước Bình Thuận',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-CANTHO',
				fullname: 'Cấp nước Cần Thơ',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-CAOBANG',
				fullname: 'Cấp nước Cao Bằng',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},

			{
				code: 'NUOC-DAKLAK',
				fullname: 'Cấp nước Đắk Lắk',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-DANANG',
				fullname: 'Cấp nước Đà Nẵng',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-DONGNAI',
				fullname: 'Cấp nước Đồng Nai',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-DONGTHAP',
				fullname: 'Cấp nước Đồng Tháp',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},

			{
				code: 'NUOC-GIADINH',
				fullname: 'Cấp nước Gia Định',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},

			{
				code: 'NUOC-GIALAI',
				fullname: 'Cấp nước Gia Lai',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			// {
			//   code: 'NUOC-HAIPHONG',
			//   fullname: 'Cấp nước Hải Phòng',
			//   isHidden: false, default: false,gateway:'OCB', disable: false
			// },
			// {
			//   code: 'NUOC-HAIPHONG2',
			//   fullname: 'Cấp nước Hải Phòng số 2 (H. An Dương)',
			//   isHidden: false, default: false,gateway:'OCB', disable: false
			// },
			{
				code: 'NUOC-HANAM',
				fullname: 'Cấp nước Hà Nam',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-HANOI2',
				fullname: 'Cấp nước Hà Nội 2',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-HANOI3',
				fullname: 'Cấp nước Hà Nội 3',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-HANOIHAWACO',
				fullname: 'Cấp nước Hà Nội Hawaco',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-HANOIVTS',
				fullname: 'Cấp nước Hà Nội VTS',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-HOABINH',
				fullname: 'Cấp nước Hòa Bình',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			// {
			//   code: 'NUOC-HUE',
			//   fullname: 'Cấp nước Thừa Thiên Huế',
			//   isHidden: false, default: false,gateway:'OCB', disable: false
			// },
			{
				code: 'NUOC-LAICHAU',
				fullname: 'Cấp nước Lai Châu',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-LONGAN',
				fullname: 'Cấp nước Long An',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-LONGKHANH',
				fullname: 'Cấp nước Long Khánh',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-NHABE',
				fullname: 'Cấp nước Nhà Bè',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-NHONTRACH',
				fullname: 'Cấp nước Nhơn Trạch',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-PHUHOATAN',
				fullname: 'Cấp nước Phú Hòa Tân',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-QUANGNINH',
				fullname: 'Cấp nước Quảng Ninh',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-QUANGTRI',
				fullname: 'Cấp nước Quảng Trị',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-SOCTRANG',
				fullname: 'Cấp nước Sóc Trăng',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-SONLA',
				fullname: 'Cấp nước Sơn La',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-SONTAY',
				fullname: 'Cấp nước Sơn Tây',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-TANHOA',
				fullname: 'Cấp nước Tân Hòa',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-TAYHANOI',
				fullname: 'Cấp nước sạch Tây Hà Nội',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-THOTNOT',
				fullname: 'Cấp nước Thốt Nốt',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-THUDUC',
				fullname: 'Cấp nước Thủ Đức',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-TIENGIANG',
				fullname: 'Cấp nước Tiền Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-TRANOC',
				fullname: 'Cấp nước Trà Nóc',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-TRUNGAN',
				fullname: 'Cấp nước Trung An',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'NUOC-VINHLONG',
				fullname: 'Cấp nước Vĩnh Long',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			// {
			//   code: 'NUOC-VIWACO',
			//   fullname: 'Cấp nước Viwaco',
			//   isHidden: false, default: false,gateway:'OCB', disable: false
			// },
			{
				code: 'NUOC-BACLIEU',
				fullname: 'Cấp nước Bạc Liêu',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			// ESTIO
			// {
			//   fullname: 'Nước Tp. Hồ Chí Minh',
			//   code: 'NHCM',
			//   isHidden: false,
			//   default: true,
			//   gateway: 'ESTIO',
			//   disable: false
			// },
			{
				fullname: 'Cấp nước Nông thôn',
				code: 'NHCM_NONGTHON',
				isHidden: false,
				default: true,
				gateway: 'ESTIO',
				disable: false
			},
			// {
			//   fullname: 'Cấp nước Trung An',
			//   code: 'NHCM_TRUNGAN',
			//   isHidden: false,
			//   default: true,
			//   gateway: 'ESTIO',
			//   disable: false
			// },
			// {
			//   fullname: 'Cấp nước Chợ Lớn',
			//   code: 'NHCM_CHOLON',
			//   isHidden: false,
			//   default: true,
			//   gateway: 'ESTIO',
			//   disable: false
			// },
			// {
			//   fullname: 'Cấp nước Thủ Đức',
			//   code: 'NHCM_THUDUC',
			//   isHidden: false,
			//   default: true,
			//   gateway: 'ESTIO',
			//   disable: false
			// },
			// {
			//   fullname: 'Cấp nước Phú Hòa Tân',
			//   code: 'NHCM_PHUHOATAN',
			//   isHidden: false,
			//   default: true,
			//   gateway: 'ESTIO',
			//   disable: false
			// },
			{
				fullname: 'Cấp nước Hóa An - Đồng Nai',
				code: 'NHCM_HOAANDONGNAI',
				isHidden: false,
				default: true,
				gateway: 'ESTIO',
				disable: false
			},
			// ẩn NHCM cho các giao dịch cũ tt đươc
			{
				fullname: 'Nước TP.Hồ Chí Minh',
				code: 'NHCM',
				isHidden: true,
				default: true,
				gateway: 'ESTIO',
				disable: false
			},
			// {
			//   fullname: 'Dịch vụ nước Đồng Nai',
			//   code: 'DNIWACO',
			//   isHidden: false,
			//   default: false,
			//   gateway: 'ESTIO',
			//   disable: false,
			// },
			// {
			//   fullname: 'Cấp nước Cần Thơ',
			//   code: 'CTWACO',
			//   isHidden: false,
			//   default: false,
			//   gateway: 'ESTIO',
			//   disable: false,
			// },
			// {
			//   fullname: 'Cấp nước Tiền Giang',
			//   code: 'TGGWACO',
			//   isHidden: false,
			//   default: false,
			//   gateway: 'ESTIO',
			//   disable: false,
			// },
			{
				fullname: 'Cấp nước Thừa Thiên - Huế',
				code: 'NHUE',
				isHidden: false,
				default: false,
				gateway: 'ESTIO',
				disable: false
			},
			{
				fullname: 'Cấp nước Cà Mau',
				code: 'NCM',
				isHidden: false,
				default: false,
				gateway: 'ESTIO',
				disable: false
			}
			// {
			//   // fullname: 'Cấp nước & Môi trường Đô thị Đồng Tháp',
			//   fullname: 'Cấp nước Đồng Tháp',
			//   code: 'NDT',
			//   isHidden: false,
			//   default: false,
			//   gateway: 'ESTIO',
			//   disable: false,
			// }
		]
	},
	// POWEOCB: {
	//   isHidden: false,
	//   default: false,
	//   disable: false,
	//   title: 'Điện – Power bill',
	//   subTitle: 'Điện',
	//   items: [
	//     {
	//       fullname: 'Điện lực TPHCM – HCM EVN',
	//       code: 'EVNHCM',
	//       isHidden: false,
	//       gateway: 'OCB',
	//       default: true,
	//       disable: false,
	//     },

	//     {
	//       fullname: 'Điện lực Miền Nam',
	//       code: 'EVNMN',
	//       isHidden: false,
	//       default: false,
	//       gateway: 'OCB',
	//       disable: false,
	//     },
	//     {
	//       fullname: 'Điện lực các tỉnh khác',
	//       code: 'VNPAYEVN',
	//       isHidden: false,
	//       default: false,
	//       gateway: 'OCB',
	//       disable: false,
	//     },
	//     {
	//       fullname: 'Điện lực Hà Nội – Ha Noi EVN',
	//       code: 'EVNHANOI',
	//       isHidden: false,
	//       default: false,
	//       gateway: 'OCB',
	//       disable: false,
	//     },
	//     {
	//       fullname: 'Điện lực Miền Trung',
	//       code: 'EVN-MIENTRUNG',
	//       isHidden: false,
	//       default: false,
	//       gateway: 'OCB',
	//       disable: false,
	//     }
	//   ]
	// },
	ADSL: {
		isHidden: false,
		default: false,
		disable: false,
		title: 'ADSL – Internet ADSL bill',
		subTitle: 'Internet',
		items: [
			{
				code: 'FPT',
				fullname: 'FPT HCM',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'SST',
				fullname: 'SST Nam Sài Gòn',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VIETTEL',
				fullname: 'VIETTEL',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HCM',
				fullname: 'VNPT TP. HCM',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-ANGIANG',
				fullname: 'VNPT An Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BACGIANG',
				fullname: 'VNPT Bắc Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BACKAN',
				fullname: 'VNPT Bắc Kạn',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BACLIEU',
				fullname: 'VNPT Bạc Liêu',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BACNINH',
				fullname: 'VNPT Bắc Ninh',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BENTRE',
				fullname: 'VNPT Bến Tre',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BINHDINH',
				fullname: 'VNPT Bình Định',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BINHDUONG',
				fullname: 'VNPT Bình Dương',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BINHPHUOC',
				fullname: 'VNPT Bình Phước',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BINHTHUAN',
				fullname: 'VNPT Bình Thuận',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-CAMAU',
				fullname: 'VNPT Cà Mau',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-CANTHO',
				fullname: 'VNPT Cần Thơ',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-CAOBANG',
				fullname: 'VNPT Cao Bằng',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-DAKLAK',
				fullname: 'VNPT Đắk Lắk',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-DAKNONG',
				fullname: 'VNPT Đắk Nông',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-DANANG',
				fullname: 'VNPT Đà Nẵng',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-DIENBIEN',
				fullname: 'VNPT Điện Biên',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-DONGNAI',
				fullname: 'VNPT Đồng Nai',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-DONGTHAP',
				fullname: 'VNPT Đồng Tháp',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-GIALAI',
				fullname: 'VNPT Gia Lai',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HAGIANG',
				fullname: 'VNPT Hà Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HAIDUONG',
				fullname: 'VNPT Hải Dương',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HANAM',
				fullname: 'VNPT Hà Nam',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HATINH',
				fullname: 'VNPT Hà Tĩnh',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HAUGIANG',
				fullname: 'VNPT Hậu Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HOABINH',
				fullname: 'VNPT Hòa Bình',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HP',
				fullname: 'VNPT Hải Phòng',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HUE',
				fullname: 'VNPT Thừa Thiên Huế',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HUNGYEN',
				fullname: 'VNPT Hưng Yên',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-KHANHHOA',
				fullname: 'VNPT Khánh Hoà',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-KIENGIANG',
				fullname: 'VNPT Kiên Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-KONTUM',
				fullname: 'VNPT Kon Tum',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-LAICHAU',
				fullname: 'VNPT Lai Châu',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-LAMDONG',
				fullname: 'VNPT Lâm Đồng',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-LANGSON',
				fullname: 'VNPT Lạng Sơn',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-LAOCAI',
				fullname: 'VNPT Lào Cai',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-LONGAN',
				fullname: 'VNPT Long An',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-NAMDINH',
				fullname: 'VNPT Nam Định',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-NGHEAN',
				fullname: 'VNPT Nghệ An',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-NINHBINH',
				fullname: 'VNPT Ninh Bình',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-NINHTHUAN',
				fullname: 'VNPT Ninh Thuận',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-PHUTHO',
				fullname: 'VNPT Phú Thọ',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-PHUYEN',
				fullname: 'VNPT Phú Yên',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-QUANGBINH',
				fullname: 'VNPT Quảng Bình',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-QUANGNAM',
				fullname: 'VNPT Quảng Nam',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-QUANGNGAI',
				fullname: 'VNPT Quảng Ngãi',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-QUANGNINH',
				fullname: 'VNPT Quảng Ninh',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-QUANGTRI',
				fullname: 'VNPT Quảng Trị',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-SOCTRANG',
				fullname: 'VNPT Sóc Trăng',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-SONLA',
				fullname: 'VNPT Sơn La',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-TAYNINH',
				fullname: 'VNPT Tây Ninh',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-THAIBINH',
				fullname: 'VNPT Thái Bình',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-THAINGUYEN',
				fullname: 'VNPT Thái Nguyên',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-THANHHOA',
				fullname: 'VNPT Thanh Hóa',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-TIENGIANG',
				fullname: 'VNPT Tiền Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-TRAVINH',
				fullname: 'VNPT Trà Vinh',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-TUYENQUANG',
				fullname: 'VNPT Tuyên Quang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-VINHLONG',
				fullname: 'VNPT Vĩnh Long',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-VINHPHUC',
				fullname: 'VNPT Vĩnh Phúc',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-VUNGTAU',
				fullname: 'VNPT Bà Rịa Vũng Tàu',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-YENBAI',
				fullname: 'VNPT Yên Bái',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			}
		]
	},
	TIVI: {
		isHidden: false,
		default: false,
		disable: false,
		title: 'Truyền hình cáp - Television bill',
		subTitle: 'Truyền hình',
		items: [
			{
				code: 'ANVIEN',
				fullname: 'An Viên',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VIETTEL',
				fullname: 'Viettel Cáp',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'HCATV',
				// fullname: 'Truyền hình cáp Hà Nội (HCATV)',
				fullname: 'Hà Nội Cáp',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'KPLUS',
				fullname: 'K+',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'SCTVHCM',
				fullname: 'SCTV Cáp',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VTC',
				fullname: 'VTC',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VTVC',
				fullname: 'VTV Cáp',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HCM',
				fullname: 'VNPT TP. HCM',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HANOI',
				fullname: 'VNPT Hà Nội',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-ANGIANG',
				fullname: 'VNPT An Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BACGIANG',
				fullname: 'VNPT Bắc Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BACKAN',
				fullname: 'VNPT Bắc Kạn',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BACLIEU',
				fullname: 'VNPT Bạc Liêu',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BACNINH',
				fullname: 'VNPT Bắc Ninh',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BENTRE',
				fullname: 'VNPT Bến Tre',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BINHDINH',
				fullname: 'VNPT Bình Định',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BINHDUONG',
				fullname: 'VNPT Bình Dương',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BINHPHUOC',
				fullname: 'VNPT Bình Phước',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-BINHTHUAN',
				fullname: 'VNPT Bình Thuận',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-CAMAU',
				fullname: 'VNPT Cà Mau',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-CANTHO',
				fullname: 'VNPT Cần Thơ',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-CAOBANG',
				fullname: 'VNPT Cao Bằng',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-DAKLAK',
				fullname: 'VNPT Đắk Lắk',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-DAKNONG',
				fullname: 'VNPT Đắk Nông',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-DANANG',
				fullname: 'VNPT Đà Nẵng',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-DIENBIEN',
				fullname: 'VNPT Điện Biên',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-DONGNAI',
				fullname: 'VNPT Đồng Nai',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-DONGTHAP',
				fullname: 'VNPT Đồng Tháp',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-GIALAI',
				fullname: 'VNPT Gia Lai',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HAGIANG',
				fullname: 'VNPT Hà Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HAIDUONG',
				fullname: 'VNPT Hải Dương',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HANAM',
				fullname: 'VNPT Hà Nam',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HATINH',
				fullname: 'VNPT Hà Tĩnh',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HAUGIANG',
				fullname: 'VNPT Hậu Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},

			{
				code: 'VNPT-HOABINH',
				fullname: 'VNPT Hòa Bình',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HP',
				fullname: 'VNPT Hải Phòng',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HUE',
				fullname: 'VNPT Thừa Thiên Huế',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-HUNGYEN',
				fullname: 'VNPT Hưng Yên',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-KHANHHOA',
				fullname: 'VNPT Khánh Hoà',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-KIENGIANG',
				fullname: 'VNPT Kiên Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-KONTUM',
				fullname: 'VNPT Kon Tum',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-LAICHAU',
				fullname: 'VNPT Lai Châu',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-LAMDONG',
				fullname: 'VNPT Lâm Đồng',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-LANGSON',
				fullname: 'VNPT Lạng Sơn',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-LAOCAI',
				fullname: 'VNPT Lào Cai',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-LONGAN',
				fullname: 'VNPT Long An',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-NAMDINH',
				fullname: 'VNPT Nam Định',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-NGHEAN',
				fullname: 'VNPT Nghệ An',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-NINHBINH',
				fullname: 'VNPT Ninh Bình',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-NINHTHUAN',
				fullname: 'VNPT Ninh Thuận',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-PHUTHO',
				fullname: 'VNPT Phú Thọ',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-PHUYEN',
				fullname: 'VNPT Phú Yên',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-QUANGBINH',
				fullname: 'VNPT Quảng Bình',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-QUANGNAM',
				fullname: 'VNPT Quảng Nam',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-QUANGNGAI',
				fullname: 'VNPT Quảng Ngãi',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-QUANGNINH',
				fullname: 'VNPT Quảng Ninh',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-QUANGTRI',
				fullname: 'VNPT Quảng Trị',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-SOCTRANG',
				fullname: 'VNPT Sóc Trăng',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-SONLA',
				fullname: 'VNPT Sơn La',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-TAYNINH',
				fullname: 'VNPT Tây Ninh',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-THAIBINH',
				fullname: 'VNPT Thái Bình',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-THAINGUYEN',
				fullname: 'VNPT Thái Nguyên',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-THANHHOA',
				fullname: 'VNPT Thanh Hóa',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-TIENGIANG',
				fullname: 'VNPT Tiền Giang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-TRAVINH',
				fullname: 'VNPT Trà Vinh',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-TUYENQUANG',
				fullname: 'VNPT Tuyên Quang',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-VINHLONG',
				fullname: 'VNPT Vĩnh Long',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-VINHPHUC',
				fullname: 'VNPT Vĩnh Phúc',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-VUNGTAU',
				fullname: 'VNPT Bà Rịa Vũng Tàu',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				code: 'VNPT-YENBAI',
				fullname: 'VNPT Yên Bái',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			}
		]
	},
	PPMB: {
		isHidden: false,
		default: false,
		disable: false,
		title: 'Điện thoại trả sau – Postpaid mobile bill',
		subTitle: 'Điện thoại trả sau',
		items: [
			{
				fullname: 'Mobifone',
				code: 'MOBI',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				fullname: 'Viettel',
				code: 'VIETTEL',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			// {
			//   fullname: 'SPT Bưu chính Viễn thông Sài Gòn',
			//   code: 'SPT-HCM',
			//   isHidden: false,
			//   default: false,gateway:'OCB',
			//   disable: false,
			// },
			{
				fullname: 'Vinaphone',
				code: 'VINA',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			}
			// {
			//   fullname: 'SFone Nội Thành',
			//   code: 'SFONE_HOI_THANH_POSTPAID',
			//   isHidden: false,
			//   default: false,gateway:'OCB',
			//   disable: false,
			// },
			// {
			//   fullname: 'Vinaphone HCM Nội Thành',
			//   code: 'VINAPHONE_HCM_NOI_THANH_POSTPAID',
			//   isHidden: false,
			//   default: false,gateway:'OCB',
			//   disable: false,
			// }
		]
	},
	ATIC: {
		isHidden: false,
		default: false,
		disable: false,
		title: 'Vé máy bay',
		subTitle: 'Vé máy bay',
		items: [
			{
				fullname: 'Jetstar',
				code: 'JETSTAR_AIRLATER',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			},
			{
				fullname: 'Vietnam Airlines',
				code: 'VIETNAM_AIRLINES',
				isHidden: false,
				default: false,
				gateway: 'OCB',
				disable: false
			}
			// {
			//   fullname: 'SDKVNPAY',
			//   code: 'SDKVNPAY',
			//   isHidden: false,
			//   default: false,gateway:'OCB',
			//   disable: false
			// },
			// {
			//   fullname: 'Vé máy bay khác',
			//   code: 'VEMB',
			//   isHidden: false,
			//   default: false,gateway:'OCB',
			//   disable: false
			// }
		]
	},
	FTEL: {
		isHidden: false,
		default: false,
		disable: false,
		title: 'Điện thoại cố định – Home phone bill',
		subTitle: 'Điện thoại cố định',
		items: [{
			code: 'SPT-HCM',
			fullname: 'SPT Sài Gòn',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'SST',
			fullname: 'SST Nam Sài Gòn',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VIETTEL',
			fullname: 'Viettel',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-HCM',
			fullname: 'VNPT TP. HCM',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-HANOI',
			fullname: 'VNPT Hà Nội',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-ANGIANG',
			fullname: 'VNPT An Giang',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-BACGIANG',
			fullname: 'VNPT Bắc Giang',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-BACKAN',
			fullname: 'VNPT Bắc Kạn',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-BACLIEU',
			fullname: 'VNPT Bạc Liêu',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-BACNINH',
			fullname: 'VNPT Bắc Ninh',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-BENTRE',
			fullname: 'VNPT Bến Tre',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-BINHDINH',
			fullname: 'VNPT Bình Định',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-BINHDUONG',
			fullname: 'VNPT Bình Dương',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-BINHPHUOC',
			fullname: 'VNPT Bình Phước',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-BINHTHUAN',
			fullname: 'VNPT Bình Thuận',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-CAMAU',
			fullname: 'VNPT Cà Mau',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-CANTHO',
			fullname: 'VNPT Cần Thơ',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-CAOBANG',
			fullname: 'VNPT Cao Bằng',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-DAKLAK',
			fullname: 'VNPT Đắk Lắk',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-DAKNONG',
			fullname: 'VNPT Đắk Nông',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-DANANG',
			fullname: 'VNPT Đà Nẵng',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-DIENBIEN',
			fullname: 'VNPT Điện Biên',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-DONGNAI',
			fullname: 'VNPT Đồng Nai',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-DONGTHAP',
			fullname: 'VNPT Đồng Tháp',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-GIALAI',
			fullname: 'VNPT Gia Lai',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-HAGIANG',
			fullname: 'VNPT Hà Giang',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-HAIDUONG',
			fullname: 'VNPT Hải Dương',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-HANAM',
			fullname: 'VNPT Hà Nam',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},

		{
			code: 'VNPT-HATINH',
			fullname: 'VNPT Hà Tĩnh',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-HAUGIANG',
			fullname: 'VNPT Hậu Giang',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-HOABINH',
			fullname: 'VNPT Hòa Bình',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-HP',
			fullname: 'VNPT Hải Phòng',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-HUE',
			fullname: 'VNPT Thừa Thiên Huế',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-HUNGYEN',
			fullname: 'VNPT Hưng Yên',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-KHANHHOA',
			fullname: 'VNPT Khánh Hoà',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-KIENGIANG',
			fullname: 'VNPT Kiên Giang',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-KONTUM',
			fullname: 'VNPT Kon Tum',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-LAICHAU',
			fullname: 'VNPT Lai Châu',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-LAMDONG',
			fullname: 'VNPT Lâm Đồng',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-LANGSON',
			fullname: 'VNPT Lạng Sơn',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-LAOCAI',
			fullname: 'VNPT Lào Cai',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-LONGAN',
			fullname: 'VNPT Long An',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-NAMDINH',
			fullname: 'VNPT Nam Định',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-NGHEAN',
			fullname: 'VNPT Nghệ An',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-NINHBINH',
			fullname: 'VNPT Ninh Bình',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-NINHTHUAN',
			fullname: 'VNPT Ninh Thuận',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-PHUTHO',
			fullname: 'VNPT Phú Thọ',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-PHUYEN',
			fullname: 'VNPT Phú Yên',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-QUANGBINH',
			fullname: 'VNPT Quảng Bình',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-QUANGNAM',
			fullname: 'VNPT Quảng Nam',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-QUANGNGAI',
			fullname: 'VNPT Quảng Ngãi',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-QUANGNINH',
			fullname: 'VNPT Quảng Ninh',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-QUANGTRI',
			fullname: 'VNPT Quảng Trị',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-SOCTRANG',
			fullname: 'VNPT Sóc Trăng',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-SONLA',
			fullname: 'VNPT Sơn La',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-TAYNINH',
			fullname: 'VNPT Tây Ninh',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-THAIBINH',
			fullname: 'VNPT Thái Bình',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-THAINGUYEN',
			fullname: 'VNPT Thái Nguyên',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-THANHHOA',
			fullname: 'VNPT Thanh Hóa',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-TIENGIANG',
			fullname: 'VNPT Tiền Giang',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-TRAVINH',
			fullname: 'VNPT Trà Vinh',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-TUYENQUANG',
			fullname: 'VNPT Tuyên Quang',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-VINHLONG',
			fullname: 'VNPT Vĩnh Long',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-VINHPHUC',
			fullname: 'VNPT Vĩnh Phúc',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-VUNGTAU',
			fullname: 'VNPT Bà Rịa Vũng Tàu',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		},
		{
			code: 'VNPT-YENBAI',
			fullname: 'VNPT Yên Bái',
			isHidden: false,
			default: false,
			gateway: 'OCB',
			disable: false
		}]
	},
	LLNE: {
		isHidden: false,
		default: false,
		disable: false,
		title: 'Leased Line – Leased Line bill',
		subTitle: 'Leased Line',
		items: []
	},
	PSTN: {
		isHidden: false,
		default: false,
		disable: false,
		title: 'Hóa đơn PSTN – PSTN bill',
		subTitle: 'Hóa đơn PSTN',
		items: []
	},
	TAICHINH: {
		isHidden: false,
		default: false,
		disable: false,
		title: 'Tài chính',
		subTitle: 'Tài chính',
		items: []
	},
	BAOHIEM: {
		isHidden: false,
		default: false,
		disable: false,
		title: 'Bảo hiểm',
		subTitle: 'Bảo hiểm',
		items: []
	},
	TUITION: {
		isHidden: true,
		default: false,
		disable: true,
		title: 'Học phí',
		subTitle: 'Học phí',
		items: []
	},
	OTHER: {
		isHidden: false,
		default: false,
		disable: false,
		title: 'Thanh Toán Đại Lý BambooAirways',
		items: []
	}
};
