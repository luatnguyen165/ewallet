module.exports = {
	STATE: {
		PENDING: 'PENDING',
		EXPIRED: 'EXPIRED',
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED'
	},
	SALT: 'PAyMEntCoDePAYME@2021',
	SERVICE_TYPE: 'PAYMENT_CODE',
	TAGS: {
		RECEIVE: 'RECEIVE',
		SEND: 'SEND',
		PAYMENT_CODE: 'PAYMENT_CODE'
	},
	PAYMENT_STATE: {
		SUCCEEDED: 'SUCCEEDED',
		REQUIRE_PAYMENT: 'REQUIRE_PAYMENT',
		NOT_ENOUGHT_BALANCE: 'NOT_ENOUGHT_BALANCE',
		OVER_LIMIT: 'OVER_LIMIT'
	}
};
