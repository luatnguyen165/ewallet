module.exports = [
	{
		id: '0',
		en: 'Vietnam Public Joint Stock Commercial Bank (PVComBank)',
		vi: 'TMCP Đại Chúng Việt Nam (PVComBank)',
		shortName: 'PVComBank',
		bankNameTCB: 'DAI CHUNG VN (PVComBank)',
		swiftCode: 'WBVNVNVX',
		aliasIdPVComBank: 1,
		card: { // su dung cho man hinh lay ds cac NH duoc phep lien ket vi
			atm: {
				prefix: '970412',
				length: 16
			}
		},
		link: {
			gateway: 'PVCBANK'
		}
	},
	{
		id: '1',
		en: 'Asia Commercial Bank (ACB)',
		vi: 'TMCP Á Châu (ACB)',
		shortName: 'ACB',
		bankNameTCB: 'A CHAU (ACB)-307',
		swiftCode: 'ASCBVNVX',
		aliasIdPVComBank: 2,
		card: {
			atm: {
				prefix: '970416',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '2',
		en: 'Bank for Foreign Trade of Vietnam (VietcomBank)',
		vi: 'TMCP Ngoại Thương Việt Nam  (VietcomBank)',
		shortName: 'VietcomBank',
		bankNameTCB: 'NGOAI THUONG VN (VCB)-203',
		swiftCode: 'BFTVVNVX',
		aliasIdPVComBank: 4,
		card: {
			atm: {
				prefix: '970436',
				length: 19
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '3',
		en: 'Vietnam Bank for Industry and Trade (VietinBank)',
		vi: 'TMCP Công Thương Việt Nam (VietinBank)',
		shortName: 'VietinBank',
		bankNameTCB: 'CONG THUONG VN (VIETINBANK)-201',
		swiftCode: 'ICBVVNVX',
		aliasIdPVComBank: 46,
		card: {
			atm: {
				prefix: '970415',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '4',
		en: 'Vietnam Technological And Commercial Joint Stock Bank (Techcombank)',
		vi: 'TMCP Kỹ Thương Việt Nam (Techcombank)',
		shortName: 'TechcomBank',
		bankNameTCB: '',
		swiftCode: 'VTCBVNVX',
		aliasIdPVComBank: 5,
		card: {
			atm: {
				prefix: '970407',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '5',
		en: 'Bank for Investment & Dof Vietnam (BIDV)',
		vi: 'TMCP Đầu Tư Và Phát Triển Việt Nam (BIDV)',
		shortName: 'BIDV',
		bankNameTCB: 'DAU TU VA PHAT TRIEN VN (BIDV)-202',
		swiftCode: 'BIDVVNVX',
		aliasIdPVComBank: 6,
		card: {
			atm: {
				prefix: '970418',
				length: 16
			}
		},
		link: {
			gateway: 'BIDV'
		}
	},
	{
		id: '6',
		en: 'Vietnam Maritime Commercial Joint Stock Bank (MaritimeBank)',
		vi: 'TMCP Hàng Hải Việt Nam (MaritimeBank)',
		shortName: 'MaritimeBank',
		bankNameTCB: 'HANG HAI (MARITIMEBANK-MSB)-302',
		swiftCode: 'MCOBVNVX',
		aliasIdPVComBank: 19,
		card: {
			atm: {
				prefix: '970426',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '7',
		en: 'Vietnam Prosperity Bank (VPBank)',
		vi: 'TMCP Việt Nam Thịnh Vượng (VPBank)',
		shortName: 'VPBank',
		bankNameTCB: 'VIET NAM THINH VUONG (VPBANK)-309',
		swiftCode: 'VPBKVNVX',
		aliasIdPVComBank: 9,
		card: {
			atm: {
				prefix: '970432',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '8',
		en: 'Vietnam Bank For Agriculture and Rural Development (Agribank)',
		vi: 'Ngân Hàng Nông Nghiệp Và Phát Triển Nông Thôn Việt Nam (Agribank)',
		shortName: 'Agribank',
		bankNameTCB: 'NONG NGHIEP VA PTNT VN (AGRIBANK)-204',
		swiftCode: 'VBAAVNVX',
		aliasIdPVComBank: 7,
		card: {
			atm: {
				prefix: '970405',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '9',
		en: 'Vietnam Export Import Commercial Joint Stock Bank (Eximbank)',
		vi: 'TMCP Xuất nhập khẩu Việt Nam (Eximbank)',
		shortName: 'EximBank',
		bankNameTCB: 'XUAT NHAP KHAU (EXIMBANK)-305',
		swiftCode: 'EBVIVNVX',
		aliasIdPVComBank: 13,
		card: {
			atm: {
				prefix: '970431',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '10',
		en: 'Saigon Thuong Tin Commercial Joint Stock Bank (Sacombank)',
		vi: 'TMCP Sài Gòn Thương Tín (Sacombank)',
		shortName: 'Sacombank',
		bankNameTCB: 'SAI GON THUONG TIN (SACOMBANK)-303',
		swiftCode: 'SGTTVNVX',
		aliasIdPVComBank: 12,
		card: {
			atm: {
				prefix: '970403',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	// {
	//   id: '11',
	//   en: 'DongA Bank',
	//   vi: 'TMCP Đông Á (DongA Bank)',
	//   shortName: 'DongABank',
	//   bankNameTCB: 'DONG A (DAB)-304',
	//   swiftCode: 'EACBVNVX',
	//   aliasIdPVComBank: 20,
	//   card: {
	//     atm: {
	//       prefix: '970406',
	//       length: 16
	//     }
	//   },
	//   link: {
	//     gateway: 'NAPAS'
	//   }
	// },
	{
		id: '12',
		en: 'North Asia Commercial Joint Stock Bank (NASB)',
		vi: 'TMCP Bắc Á (NASB)',
		shortName: 'BacABank',
		bankNameTCB: 'BAC A (NASB)-313',
		swiftCode: 'NASCVNVX',
		aliasIdPVComBank: 28,
		card: {
			atm: {
				prefix: '970409',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	// {
	//   id: '13',
	//   en: 'Australia and New Zealand Banking (ANZ Bank)',
	//   vi: 'TNHH một thành viên ANZ Việt Nam (ANZ Bank)',
	//   bankNameTCB: 'ANZ VN-602',
	//   swiftCode: 'ANZBVNVX'
	// },
	// {
	//   id: '14',
	//   en: 'Southern Commercial Joint Stock Bank (Phuong Nam Bank)',
	//   vi: 'TMCP Phương Nam (Phuong Nam Bank)',
	//   bankNameTCB: '',
	//   swiftCode: 'PNBKVNVX'
	// },
	{
		id: '15',
		en: 'Vietnam International Commercial Joint Stock Bank (VIB)',
		vi: 'TMCP Quốc tế Việt Nam (VIB)',
		shortName: 'VIB',
		bankNameTCB: 'QUOC TE (VIB)-314',
		swiftCode: 'VNIBVNVX',
		aliasIdPVComBank: 3,
		card: {
			atm: {
				prefix: '970441',
				length: 19
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '16',
		en: 'Vietnam Asia Commercial Joint Stock Bank (VietABank)',
		vi: 'TMCP Việt Á (VietABank)',
		shortName: 'VietABank',
		bankNameTCB: 'VIET A (VIETABANK)-355',
		swiftCode: 'VNACVNVX',
		aliasIdPVComBank: 23,
		card: {
			atm: {
				prefix: '970427',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '17',
		en: 'Tien Phong Commercial Joint Stock Bank (TP Bank)',
		vi: 'TMCP Tiên Phong (TP Bank)',
		shortName: 'TPBank',
		bankNameTCB: 'TIEN PHONG (TIENPHONGBANK)-358',
		swiftCode: 'TPBVVNVX',
		aliasIdPVComBank: 8,
		card: {
			atm: {
				prefix: '970423',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '18',
		en: 'Military Commercial Joint Stock Bank (MB Bank)',
		vi: 'TMCP Quân đội (MB Bank)',
		shortName: 'MBBank',
		bankNameTCB: 'QUAN DOI (MB)-311',
		swiftCode: 'MSCBVNVX',
		aliasIdPVComBank: 11,
		card: {
			atm: {
				prefix: '970422',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '19',
		en: 'OceanBank',
		vi: 'TM TNHH Một Thành Viên Đại Dương (OceanBank)',
		bankNameTCB: 'DAI DUONG (OCEAN BANK)-319',
		shortName: 'OceanBank',
		swiftCode: 'OJBAVNVX',
		aliasIdPVComBank: 14,
		card: {
			atm: {
				prefix: '970414',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '20',
		en: 'Petrolimex Group Commercial Joint Stock Bank (PG Bank)',
		vi: 'TMCP Xăng dầu Petrolimex (PG Bank)',
		shortName: 'PGBank',
		bankNameTCB: 'XANG DAU PETROLIMEX (PGBANK)-341',
		swiftCode: 'PGBLVNVX',
		aliasIdPVComBank: 27,
		card: {
			atm: {
				prefix: '970430',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '21',
		en: 'Lien Viet Post Joint Stock Commercial Bank (LienVietPostBank)',
		vi: 'TMCP Bưu Điện Liên Việt (LienVietPostBank)',
		shortName: 'LienVietPostBank',
		bankNameTCB: 'BUU DIEN LIEN VIET (LPB)-357',
		swiftCode: 'LVBKVNVX',
		aliasIdPVComBank: 17,
		card: {
			atm: {
				prefix: '970449',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	// {
	//   id: '22',
	//   en: 'HSBC Bank (Vietnam) Ltd',
	//   vi: 'TNHH một thành viên HSBC (HSBC)',
	//   bankNameTCB: 'HSBC-617',
	//   swiftCode: 'HSBCVNVX'
	// },
	// {
	//   id: '23',
	//   en: 'Mekong Housing Bank (MHB Bank)',
	//   vi: 'Phát triển nhà đồng bằng sông Cửu Long (MHB Bank)',
	//   bankNameTCB: '',
	//   swiftCode: 'MHBBVNVX'
	// },
	{
		id: '24',
		en: 'Southeast Asia Commercial Joint Stock Bank (SeABank)',
		vi: 'TMCP Đông Nam Á (SeABank)',
		shortName: 'SeaBank',
		bankNameTCB: 'DONG NAM A (SEA BANK)-317',
		swiftCode: 'SEAVVNVX',
		aliasIdPVComBank: 10,
		card: {
			atm: {
				prefix: '970440',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '25',
		en: 'An Binh Commercial Joint Stock Bank (ABBank)',
		vi: 'TMCP An Bình (ABBank)',
		shortName: 'ABBank',
		bankNameTCB: 'AN BINH (AB BANK)-323',
		swiftCode: 'ABBKVNVX',
		aliasIdPVComBank: 16,
		card: {
			atm: {
				prefix: '970425',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	// {
	//   id: '26',
	//   en: 'CITIBANK N.A.',
	//   vi: 'Citibank Việt Nam (CITIBANK)',
	//   bankNameTCB: 'CITIBANK HO CHI MINH-654',
	//   swiftCode: 'CITIVNVX'
	// },
	{
		id: '27',
		en: 'HoChiMinh City Development Joint Stock Commercial Bank (HDBank)',
		vi: 'TMCP Phát triển Thành phố Hồ Chí Minh (HDBank)',
		shortName: 'HDBank',
		bankNameTCB: 'PHAT TRIEN TP HCM (HDBANK)-321',
		swiftCode: 'HDBCVNVX',
		aliasIdPVComBank: 22,
		card: {
			atm: {
				prefix: '970437',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '28',
		en: 'Global Petro Bank (GBBank)',
		vi: 'TMCP Dầu Khí Toàn Cầu (GBBank)',
		shortName: 'GBBank',
		bankNameTCB: 'DAU KHI TOAN CAU (GPBANK)-320',
		swiftCode: 'GBNKVNVX',
		aliasIdPVComBank: 26,
		card: {
			atm: {
				prefix: '970408',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '29',
		en: 'Orient Commercial Joint Stock Bank (OCB)',
		vi: 'TMCP Phương Đông (OCB)',
		shortName: 'OCB',
		bankNameTCB: 'PHUONG DONG (OCB)-333',
		swiftCode: 'ORCOVNVX',
		aliasIdPVComBank: 24,
		card: {
			atm: {
				prefix: '970448',
				length: 16
			}
		},
		link: {
			gateway: 'OCB'
		}
	},
	{
		id: '30',
		en: 'Saigon – Hanoi Commercial Joint Stock Bank (SHB)',
		vi: 'TMCP Sài Gòn – Hà Nội (SHB)',
		shortName: 'SHB',
		bankNameTCB: 'SAI GON - HA NOI (SHB)-348',
		swiftCode: 'SHBAVNVX',
		aliasIdPVComBank: 18,
		card: {
			atm: {
				prefix: '970443',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '31',
		en: 'Nam A Commercial Joint Stock Bank',
		vi: 'TMCP Nam Á (NamA Bank)',
		shortName: 'NamABank',
		bankNameTCB: 'NAM A-306',
		swiftCode: 'NAMAVNVX',
		aliasIdPVComBank: 39,
		card: {
			atm: {
				prefix: '970428',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '32',
		en: 'Saigon Bank For Industry And Trade (Saigon Bank)',
		vi: 'TMCP Sài Gòn Công Thương (Saigon Bank)',
		shortName: 'SaigonBank',
		bankNameTCB: 'SAI GON CONG THUONG-308',
		swiftCode: 'SBITVNVX',
		aliasIdPVComBank: 25,
		card: {
			atm: {
				prefix: '970400',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '33',
		en: 'Saigon Commercial Bank (SCB)',
		vi: 'TMCP Sài Gòn (SCB)',
		shortName: 'SCB',
		bankNameTCB: 'SAI GON (SCB)-334',
		swiftCode: 'SACLVNVX',
		aliasIdPVComBank: 33,
		card: {
			atm: {
				prefix: '970429',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	// {
	//   id: '34',
	//   en: 'Vietnam Construction Joint Stock Commercial Bank (VNCB)',
	//   vi: 'thương mại TNHH MTV Xây dựng Việt Nam (VNCB)',
	//   bankNameTCB: 'XAY DUNG VIET NAM (VNCB)-339',
	//   swiftCode: 'GTBAVNVX'
	// },
	{
		id: '35',
		en: 'Kien Long Commercial Joint Stock Bank (Kienlongbank)',
		vi: 'TMCP Kiên Long (Kienlongbank)',
		shortName: 'KienLongBank',
		bankNameTCB: 'KIEN LONG (KIENLONGBANK)-353',
		swiftCode: 'KLBKVNVX',
		aliasIdPVComBank: 34,
		card: {
			atm: {
				prefix: '970452',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '36',
		en: 'SHINHAN Bank',
		vi: 'Shinhan (SHINHAN Bank)',
		shortName: 'ShinhanBank',
		bankNameTCB: 'SHINHAN VIET NAM-616',
		swiftCode: 'SHBKVNVX',
		aliasIdPVComBank: 30,
		card: {
			atm: {
				prefix: '970424',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '37',
		en: 'Baoviet Joint Stock Commercial Bank',
		vi: 'TMCP Bảo Việt (Baoviet Bank)',
		shortName: 'BaoVietBank',
		bankNameTCB: 'BAO VIET (BAOVIETBANK)-359',
		swiftCode: 'BVBVVNVX',
		aliasIdPVComBank: 21,
		card: {
			atm: {
				prefix: '970438',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '38',
		en: 'Viet Nam Thuong Tin Commercial Joint Stock Bank',
		vi: 'TMCP Việt Nam Thương Tín (Vietbank)',
		shortName: 'VietBank',
		bankNameTCB: 'VIET NAM THUONG TIN (VIETBANK)-356',
		swiftCode: 'VNTTVNVX',
		aliasIdPVComBank: 29,
		card: {
			atm: {
				prefix: '970433',
				length: 16
			}
		},
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '40',
		card: {
			atm: {
				prefix: '970457',
				length: 16
			}
		},
		en: 'WOORI BANK',
		vi: 'Ngân hàng TNHH MTV Woori Việt Nam',
		shortName: 'WooriBank',
		bankNameTCB: 'WOORI BANK',
		swiftCode: 'HVBKVNVX',
		aliasIdPVComBank: 36,
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '41',
		card: {
			atm: {
				prefix: '970458',
				length: 16
			}
		},
		isActive: true,
		en: 'United Overseas Bank',
		vi: 'Ngân hàng TNHH MTV United Overseas Bank',
		shortName: 'UOB',
		bankNameTCB: 'United Overseas Bank',
		swiftCode: 'UOVBVNVX',
		aliasIdPVComBank: 36,
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '42',
		card: {
			atm: {
				prefix: '970454',
				length: 16
			}
		},
		isActive: true,
		en: 'Viet Capital Bank',
		vi: 'Ngân hàng bản việt (VietCapital Bank)',
		shortName: 'VCCB',
		bankNameTCB: 'Viet Capital Bank',
		swiftCode: 'VCBCVNVX',
		aliasIdPVComBank: 36,
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '43',
		card: {
			atm: {
				prefix: '970419',
				length: 16
			}
		},
		isActive: true,
		en: 'National Citizen Bank',
		vi: 'Ngân hàng TMCP Quốc Dân (NCB)',
		shortName: 'NVB',
		bankNameTCB: 'Ngân hàng TMCP Quốc Dân (NCB)',
		swiftCode: 'NVBAVNVX',
		aliasIdPVComBank: 36,
		link: {
			gateway: 'NAPAS'
		}
	},
	{
		id: '45',
		card: {
			atm: {
				prefix: '970421',
				length: 16
			}
		},
		isActive: true,
		en: 'Vietnam - Russia Joint Venture Bank',
		vi: 'Ngân hàng Liên doanh Việt - Nga',
		shortName: 'VRB',
		bankNameTCB: 'Vietnam - Russia Joint Venture Bank',
		swiftCode: 'UOVBVNVX',
		aliasIdPVComBank: 36,
		link: {
			gateway: 'VRBAVNVX'
		}
	},
	{
		id: '46',
		card: {
			atm: {
				prefix: '970434',
				length: 16
			}
		},
		isActive: true,
		en: 'Indovina Bank LTD',
		vi: 'Ngân hàng TNHH Indovina',
		shortName: 'IVB',
		bankNameTCB: 'Ngân hàng TNHH Indovina',
		swiftCode: 'IABBVNVX',
		aliasIdPVComBank: 36,
		link: {
			gateway: 'IABBVNVX'
		}
	}
	// {
	//   id: '39',
	//   en: 'Napas Smart Link Bank',
	//   vi: 'Napas Smart Link Bank',
	//   shortName: 'SMLIB',
	//   bankNameTCB: '',
	//   swiftCode: 'SHBKVNVX',
	//   aliasIdPVComBank: 30,
	//   card: {
	//     atm: {
	//       prefix: '970402',
	//       length: 16
	//     }
	//   },
	//   link: {
	//     gateway: 'NAPAS'
	//   }
	// }
];
