const _ = require('lodash');
const Moment = require('moment-timezone');

const ErrorCodeConstant = require('../constants/errorCode.constant');
const FrontendConstant = require('../constants/frontent.constant');
const BillSupplier = require('../constants/billSupplier.constant');
const HistoryConstant = require('../constants/history.constant');

module.exports = async function (ctx) {
	try {
		const { dataHistory, isPushNotification } = ctx.params;
		let { serviceName } = dataHistory;
		if (!serviceName) {
			serviceName = await this.GetServiceName(dataHistory);
		}

		if (_.isNil(dataHistory.isVisible)) {
			dataHistory.isVisible = true;
		}
		dataHistory['service.name'] = _.get(serviceName, 'data.serviceName', 'Hệ thống');
		dataHistory.method = {};

		if (!dataHistory.publishedAt) {
			dataHistory.publishedAt = Moment().utc();
		}
		const serviceType = _.get(dataHistory, 'service.type', null);
		if (_.get(dataHistory, 'service.code', null) === null && serviceType !== FrontendConstant.SERVICES.SOCIAL_PAYMENT) {
			dataHistory['service.code'] = serviceType;
		}

		if (_.get(dataHistory, 'service.method', null) === null) {
			const service = await this.MethodService(null, { ...dataHistory.service, accountId: dataHistory.accountId }, dataHistory);
			if (_.get(service, 'data.method', null) !== null) {
				_.set(dataHistory, 'service.method', _.get(service, 'method', ''));
			}
		}

		const createdHistory = await this.broker.call('v1.eWalletHistoryModel.create', [dataHistory]);
		if (!_.isObject(createdHistory) || createdHistory.id < 1) {
			return {
				code: ErrorCodeConstant.HISTORY_SERVICE.CREATE_FAILED,
				message: this.__(ErrorCodeConstant.HISTORY_SERVICE.CREATE_FAILED.toString()),
				data: null
			};
		}

		if (isPushNotification === true) {
			const notifyObj = {
				accountId: createdHistory.accountId,
				message: createdHistory.description,
				extraData: createdHistory,
				title: `${createdHistory.service.name}`
			};
			await this.broker.call('v1.ewalletNotification.createNotification', {
				params: notifyObj
			});
		}

		return {
			code: ErrorCodeConstant.HISTORY_SERVICE.CREATE_SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_SERVICE.CREATE_SUCCEEDED.toString()),
			data: createdHistory
		};
	} catch (error) {
		this.logger.info(`[eWalletHistory] Create History Error: ${error}`);
		return {
			code: ErrorCodeConstant.HISTORY_SERVICE.CREATE_FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_SERVICE.CREATE_FAILED.toString()),
			data: null
		};
	}
};
