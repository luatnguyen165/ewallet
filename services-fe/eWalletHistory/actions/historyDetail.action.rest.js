const { MoleculerError } = require('moleculer').Errors;
const _ = require('lodash');
const AsyncForEach = require('await-async-foreach');
const ErrorCodeConstant = require('../constants/errorCode.constant');
const GeneralConstant = require('../constants/general.constant');

const DETAIL_HISTORY_METHOD_NAME = {
	BILL: 'GetBillDetail',
	MOBILE_CARD: 'GetMobileCardDetail',
	MOBILE_TOPUP: 'GetMobileTopupDetail',
	ISEC: 'GetIsecDetail',
	DEPOSIT: 'GetDepositDetail',
	WITHDRAW: 'GetWithdrawDetail',
	REFUND_MONEY: 'GetRefundMoneyDetail',
	TRANSFER_MONEY: 'GetTransferMoneyDetail',
	SOCIAL_PAYMENT: 'GetSocialPaymentDetail',
	SALARY: 'GetSalaryDetail',
	OPEN_EWALLET_PAYMENT: 'GetOpenEwalletPaymentDetail',
	WALLET_QR: 'GetWalletQRDetail',
	WALLET_PAY: 'GetWalletPayDetail',
	PAY_QRCODE: 'GetPayQrCodeDetail',
	CREDIT_STATEMENT: 'GetPayCreditStatementDetail',
	ADVANCE_MONEY: 'GetAdvanceMoneyDetail'
};

module.exports = async function (ctx) {
	try {
		const payload = _.get(ctx, 'params.body', {});
		const authInfo = _.get(ctx, 'meta.auth.data', {});

		this.logger.info(`[eWalletHistory] Get Detail History Payload: ${JSON.stringify(payload)}`);

		const { language } = ctx.meta;
		this.setLocale(language || 'vi');

		// authInfo.accountId = 566980335;
		const { transaction } = payload;
		const { accountId } = authInfo;
		if (!accountId) {
			return {
				code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.AUTH_ERROR,
				message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.AUTH_ERROR.toString())
			};
		}

		const detailHistory = await this.GetDetailHistory(transaction, accountId);
		return detailHistory;
	} catch (err) {
		this.logger.info(`[ewalletHistory] Get Detail History Error: ${err}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED.toString()),
			data: {}
		};
	}
};
