const { MoleculerError } = require('moleculer').Errors;
const _ = require('lodash');
const AsyncForEach = require('await-async-foreach');
const ErrorCodeConstant = require('../constants/errorCode.constant');
const GeneralConstant = require('../constants/general.constant');

const DETAIL_HISTORY_METHOD_NAME = {
	BILL: 'GetBillDetail',
	MOBILE_CARD: 'GetMobileCardDetail',
	MOBILE_TOPUP: 'GetMobileTopupDetail',
	ISEC: 'GetIsecDetail',
	DEPOSIT: 'GetDepositDetail',
	WITHDRAW: 'GetWithdrawDetail',
	REFUND_MONEY: 'GetRefundMoneyDetail',
	TRANSFER_MONEY: 'GetTransferMoneyDetail',
	SOCIAL_PAYMENT: 'GetSocialPaymentDetail',
	SALARY: 'GetSalaryDetail',
	OPEN_EWALLET_PAYMENT: 'GetOpenEwalletPaymentDetail',
	WALLET_QR: 'GetWalletQRDetail',
	WALLET_PAY: 'GetWalletPayDetail',
	PAY_QRCODE: 'GetPayQrCodeDetail',
	CREDIT_STATEMENT: 'GetPayCreditStatementDetail',
	ADVANCE_MONEY: 'GetAdvanceMoneyDetail'
};

module.exports = async function (ctx) {
	try {
		const payload = _.get(ctx, 'params.body', {});
		const authInfo = _.get(ctx, 'meta.auth.data', {});

		this.logger.info(`[eWalletHistory] Get Detail History: ${JSON.stringify(payload)}`);

		const { language } = ctx.meta;
		this.setLocale(language || 'vi');

		const { transaction } = payload;
		let { accountId } = authInfo;
		if (!accountId) accountId = payload.accountId;
		if (!accountId) {
			return {
				code: ErrorCodeConstant.GET_DETAIL_HISTORY_SUCCEEDED,
				message: this.__(ErrorCodeConstant.GET_DETAIL_HISTORY_SUCCEEDED.toString())
			};
		}
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': transaction, accountId }]);
		this.logger.info(`[eWalletHistory] history: ${JSON.stringify(history)}`);

		if (_.get(history, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.GET_DETAIL_HISTORY_SUCCEEDED,
				message: this.__(ErrorCodeConstant.GET_DETAIL_HISTORY_SUCCEEDED.toString())
			};
		}

		let detailHistory;
		const serviceType = _.get(history, 'service.type', '');
		if (_.includes(GeneralConstant.SERVICE_TYPE, serviceType)) {
			try {
				const method = DETAIL_HISTORY_METHOD_NAME[serviceType];
				detailHistory = await this[method](ctx, history);
			} catch (error) {
				this.logger.info(`[eWalletHistory] Get Detail: ${error}`);
			}
		} else detailHistory = await this.GetDefaultDetail(ctx, history);
		return {
			code: ErrorCodeConstant.GET_DETAIL_HISTORY_SUCCEEDED,
			message: this.__(ErrorCodeConstant.GET_DETAIL_HISTORY_SUCCEEDED.toString()),
			data: detailHistory
		};
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`[eWalletHistory] Get History Error: ${err.message}`, ErrorCodeConstant.GET_DETAIL_HISTORY_FAILED);
	}
};
