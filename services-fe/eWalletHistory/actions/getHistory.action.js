const { MoleculerError } = require('moleculer').Errors;
const _ = require('lodash');
const AsyncForEach = require('await-async-foreach');
const GeneralConstant = require('../constants/general.constant');
const ErrorCodeConstant = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	try {
		const payload = _.get(ctx, 'params.body', {});
		const authInfo = _.get(ctx, 'meta.auth.data', {});

		this.logger.info(`[eWalletHistory] Get History: ${JSON.stringify(payload)}`);

		const { language } = ctx.meta;
		this.setLocale(language || 'vi');

		const where = _.clone(payload.filter);
		if (where.transaction) {
			where['service.transaction'] = where.transaction;
		}
		where.accountId = authInfo.accountId;
		if (_.isArray(where.state)) where.state = { $in: where.state };
		delete where.transaction;
		const { paging } = payload;

		this.logger.info(`[eWalletHistory] where: ${JSON.stringify(where)}`);

		const historyList = await this.broker.call('v1.eWalletHistoryModel.findMany', [
			where,
			'-_id service.transaction service.type service.code amount fee total createdAt state changed description payment balance accountId',
			{
				start: paging.start,
				limit: paging.limit,
				sort: {
					createdAt: -1
				}
			}
		]);
		await AsyncForEach(historyList, async (history) => {
			try {
				const paymentMethod = await this.GetPaymentMethod(_.get(history, 'payment.method', null));
				if (_.get(paymentMethod, 'succeeded', false)) history.paymentMethod = paymentMethod.data.paymentMethod;
			} catch (error) {
				this.logger.info('[eWalletHistory] Get Payment Method: ', error);
			}
			try {
				const serviceName = await this.GetServiceName(_.get(history, 'service.type', null), _.get(history, 'service.code', null));
				if (_.get(serviceName, 'succeeded', false)) history.serviceName = serviceName.data.serviceName;
			} catch (error) {
				this.logger.info('[eWalletHistory] Get Service Name: ', error);
			}
			history.transaction = _.get(history, 'service.transaction');
			history.changed = GeneralConstant.changed[history.changed] || history.changed;
			if (_.get(payload, 'option.isDetail', false) === true) {
				try {
					const params = ctx;
					_.set(params.params.body.transaction, history.transaction);
					const detail = await this.broker.call('v1.eWalletHistory.getDetailHistory', { body: { transaction: history.transaction, accountId: authInfo.accountId } });
					if (detail.code === ErrorCodeConstant.GET_DETAIL_HISTORY_SUCCEEDED) history.detail = detail.data;
				} catch (error) {
					this.broker.call(`[eWalletHistory] Get History Detail: ${JSON.stringify(error)}`);
				}
			}
			delete history.payment;
			delete history.service;
		});

		// this.logger.info(`[eWalletHistory] History List: ${historyList.length}, ${JSON.stringify(historyList)}`);
		return {
			code: ErrorCodeConstant.GET_LIST_HISTORY_SUCCEEDED,
			message: this.__(ErrorCodeConstant.GET_LIST_HISTORY_SUCCEEDED.toString()),
			data: historyList
		};
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`[eWalletHistory] Get History Error: ${err.message}`, 99);
	}
};
