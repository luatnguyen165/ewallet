const _ = require('lodash');
const errorCodeConstant = require('../constants/errorCode.constant');

const FrontendConstant = require('../constants/frontent.constant');

const stateUpdatePublishedAt = [FrontendConstant.HISTORY_STATE.SUCCEEDED, FrontendConstant.HISTORY_STATE.PENDING, FrontendConstant.HISTORY_STATE.FAILED];

module.exports = async function (ctx) {
	try {
		const { fields, updateInfo, isPushNotification = false } = ctx.params;
		if (!_.isObject(fields)) return null;
		if (_.isEmpty(fields)) return null;

		const stateHistory = _.get(updateInfo, 'state', '') || '';
		if (stateHistory !== '' && _.includes(stateUpdatePublishedAt, stateHistory)) {
			updateInfo.publishedAt = new Date().toISOString();
			const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [fields]);
			if (_.get(history, 'changed', '') === '') {
				history.changed = _.get(updateInfo, 'changed', null);
			}
			if (_.get(history, 'id', null) !== null && _.get(history, 'service.method', null) === null) {
				const service = await this.MethodService(history.id, { ...history.service, accountId: history.accountId }, history);
				if (_.get(service, 'data.method', null) !== null && _.get(updateInfo, 'service.method', null) === null) {
					updateInfo['service.method'] = _.get(service, 'method', '');
				}
			}
		}

		const updatedHistory = await this.broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [fields, updateInfo, { new: true }]);
		if (!_.isObject(updatedHistory) || updatedHistory.id < 1) {
			return {
				code: errorCodeConstant.HISTORY_SERVICE.UPDATE_FAILED,
				message: this.__(errorCodeConstant.HISTORY_SERVICE.UPDATE_FAILED.toString()),
				data: {}
			};
		}

		if (isPushNotification === true) {
			updatedHistory.clientId = updateInfo.clientId;
			const notifyObj = {
				accountId: updatedHistory.accountId,
				message: updatedHistory.description,
				extraData: updatedHistory,
				title: `${updatedHistory.service.name}`
			};
			await this.broker.call('v1.ewalletNotification.createNotification', {
				params: notifyObj
			});
		}

		return {
			code: errorCodeConstant.HISTORY_SERVICE.UPDATE_SUCCEEDED,
			message: this.__(errorCodeConstant.HISTORY_SERVICE.UPDATE_SUCCEEDED.toString()),
			data: updatedHistory
		};
	} catch (error) {
		this.logger.info(`[HistoryService] Update Error: ${error}`);
		return {
			code: errorCodeConstant.HISTORY_SERVICE.UPDATE_FAILED,
			message: this.__(errorCodeConstant.HISTORY_SERVICE.UPDATE_FAILED.toString()),
			data: {}
		};
	}
};
