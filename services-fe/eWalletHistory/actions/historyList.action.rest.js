const _ = require('lodash');
const AsyncForEach = require('await-async-foreach');
const moment = require('moment');
const numeral = require('numeral');

const ErrorCodeConstant = require('../constants/errorCode.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	try {
		const payload = _.get(ctx, 'params.body', {});
		const authInfo = _.get(ctx, 'meta.auth.data', {});
		const { filter, paging = { start: 0, limit: 10 } } = payload;

		const { language } = ctx.meta;
		this.setLocale(language || 'vi');

		// authInfo.accountId = 566980335;
		const where = {
			accountId: authInfo.accountId,
			state: {
				$in: [GeneralConstant.STATE.SUCCEEDED]
			},
			changed: {
				$in: ['+', '-']
			}
		};
		if (filter.startDate || filter.endDate) where.createdAt = {};
		if (filter.startDate) where.createdAt.$gte = filter.startDate;
		if (filter.endDate) where.createdAt.$lte = filter.endDate;
		if (filter.type) where['service.type'] = filter.type;

		this.logger.info(`[eWalletHistory] where: ${JSON.stringify(where)}`);

		const historyList = await this.broker.call('v1.eWalletHistoryModel.findMany', [
			where,
			'-_id',
			{
				start: paging.start,
				limit: paging.limit,
				sort: {
					publishedAt: -1
				}
			}
		]);

		const data = [];
		await AsyncForEach(historyList, async (history) => {
			const temp = {
				block: {
					date: history.publishedAt ? moment(history.publishedAt).format('HH:mm') : moment(history.updatedAt).format('HH:mm')
				},
				historyId: history.id,
				publishedAt: history.publishedAt,
				transaction: _.get(history, 'service.transaction', null)
			};
			let formattedMoney = Math.ceil(history.total);
			formattedMoney = history.changed === '+' ? formattedMoney : formattedMoney * -1;
			temp.block.total = `${numeral(formattedMoney).format('0,0')} đ`;
			temp.block.paymentMethod = _.get(history, 'payment.description', null);
			temp.block.method = _.get(history, 'payment.method', null);
			let serviceName = _.get(history, 'service.name', null);
			if (!serviceName) {
				try {
					const getServiceName = await this.GetServiceName(history);
					if (_.get(serviceName, 'succeeded', false) === true) serviceName = getServiceName.data.serviceName;
				} catch (error) {
					this.logger.info('[eWalletHistory] Get Service Name: ', error);
				}
			}
			temp.block.title = serviceName;
			temp.block.icon = this.GetIcon(history);

			if (_.get(payload, 'option.isDetail', false) === true) {
				try {
					const detail = await this.GetDetailHistory(history.transaction, authInfo.accountId);
					if (detail.code === ErrorCodeConstant.GET_DETAIL_HISTORY_SUCCEEDED) temp.detail = detail.data;
				} catch (error) {
					this.broker.call(`[eWalletHistory] Get History Detail: ${JSON.stringify(error)}`);
				}
			}
			data.push(temp);
		});
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_LIST.SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_LIST.SUCCEEDED.toString()),
			data
		};
	} catch (error) {
		this.logger.info(`[ewalletHistory] Get History List Error: ${error}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_LIST.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_LIST.FAILED.toString()),
			data: []
		};
	}
};
