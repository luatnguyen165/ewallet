const _ = require('lodash');
const Moment = require('moment');
const Numeral = require('numeral');
const Decimal = require('decimal.js');
const MobileConstant = require('../constants/mobile.constant');
const GeneralConstant = require('../constants/general.constant');
const GateDecoder = require('../helpers/gateDecoder.helper');
const ErrorCodeConstant = require('../constants/errorCode.constant');

const Private = {
	blockWallet() {
		return [{ leftTitle: 'Phương thức', rightTitle: GeneralConstant.ITEM_METHOD.PAYME }];
	},
	blockCreditBalance() {
		return [{ leftTitle: 'Phương thức', rightTitle: GeneralConstant.ITEM_METHOD.CREDIT_BALANCE }];
	},

	blockTransactionEnd(history) {
		return [
			{ leftTitle: 'Phí giao dịch', rightTitle: history.fee ? `${Numeral(history.fee).format('0,0')} đ` : 'Miễn phí' },
			{ leftTitle: 'Tổng thanh toán', rightTitle: `${Numeral(history.total).format('0,0')} đ`, rightColor: '#ec2a2a' }
		];
	},

	transactionBlock(history) {
		const serviceType = _.get(history, 'service.type', '');

		const transactionDetail = [
			{
				leftTitle: 'Mã giao dịch',
				rightTitle: _.get(history, 'service.transaction', '')
			},
			{
				leftTitle: 'Thời gian giao dịch',
				rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('hh:mm DD/MM/YYYY')
			}
		];

		if (_.get(history, 'payment.method', '') === GeneralConstant.PAYMENT_TYPE.WALLET) {
			return _.concat(transactionDetail, Private.blockWallet(history), Private.blockTransactionEnd(history));
		}
		if (_.get(history, 'payment.method', '') === GeneralConstant.PAYMENT_TYPE.CREDIT_BALANCE) {
			return _.concat(transactionDetail, Private.blockCreditBalance(), Private.blockTransactionEnd(history));
		}
		let description = GeneralConstant.ITEM_METHOD.PAYME;
		// rỗng + transport từ ví hiển thị số dư ví
		if (!serviceType) {
			switch (_.get(history, 'method.group', null)) {
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_BANK:
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_GATEWAY:
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_BANK_OCBBANK:
					description = GeneralConstant.ITEM_METHOD.LINKED;
					break;
				case GeneralConstant.PAYMENT_METHOD_GROUP.GATEWAY:
					description = GeneralConstant.ITEM_METHOD.ATM;
					break;
				case GeneralConstant.PAYMENT_METHOD_GROUP.DEPOSIT_BANK_MANUAL:
					description = GeneralConstant.ITEM_METHOD.TRANSFER_BANK;
					break;
				default:
					break;
			}
		}

		const paymentMethod = _.get(history, 'payment.method', null) || _.get(history, 'transport.method', null);

		if (serviceType) {
			switch (paymentMethod) {
				case GeneralConstant.PAYMENT_TYPE.LINKED:
					description = GeneralConstant.ITEM_METHOD.LINKED;
					break;
				case GeneralConstant.PAYMENT_TYPE.BANK_CARD:
					description = GeneralConstant.ITEM_METHOD.ATM;
					break;
				case GeneralConstant.PAYMENT_TYPE.BANK_ACCOUNT:
					description = GeneralConstant.ITEM_METHOD.ACCOUNT;
					break;
				case GeneralConstant.PAYMENT_TYPE.PAYME_CREDIT:
					if (_.get(history, 'service.type', null) !== GeneralConstant.SERVICE_TYPE.ADVANCE_MONEY) {
						description = GeneralConstant.ITEM_METHOD.PAYME_CREDIT;
					}
					break;
				default:
					break;
			}
		}

		let cardType = 'tài khoản';
		if (paymentMethod === GeneralConstant.PAYMENT_TYPE.BANK_CARD) {
			cardType = 'thẻ';
		}
		if (history.state !== GeneralConstant.STATE.PENDING) {
			transactionDetail.push(
				{ leftTitle: 'Phương thức', rightTitle: description },
				{ leftTitle: `Số ${cardType}`, rightTitle: _.get(history, 'payment.description', '') }
			);
		}
		return _.concat(transactionDetail, Private.blockTransactionEnd(history));
	},

	infoTopupBlock(history, card) {
		const supplier = MobileConstant.CARD_SUPPLIER_NAME[(_.get(card, 'supplier', '')).toUpperCase()] || '';
		let cardType = GeneralConstant.SERVICE_NAME.CARD_GAME;
		if (_.includes([
			MobileConstant.CARD_SUPPLIER.VIETTEL,
			MobileConstant.CARD_SUPPLIER.MOBI,
			MobileConstant.CARD_SUPPLIER.VINA,
			MobileConstant.CARD_SUPPLIER.BEELINE,
			MobileConstant.CARD_SUPPLIER.VN_MOBI
		], card.supplier)
		) {
			cardType = GeneralConstant.SERVICE_NAME.MOBILE_CARD;
		}
		if (_.get(history, 'service.state') !== GeneralConstant.STATE.SUCCEEDED) {
			return [
				{ leftTitle: 'Thanh Toán', rightTitle: cardType },
				{ leftTitle: 'Nhà cung cấp', rightTitle: supplier === MobileConstant.CARD_SUPPLIER_NAME.GATE ? 'Thẻ đa năng Gate' : supplier },
				{ leftTitle: 'Chiết khấu', rightTitle: `${_.toNumber(new Decimal(100).mul(new Decimal(card.discount).div(card.amount)))}%` }
			];
		}
		let type = 'thẻ';
		if (cardType === GeneralConstant.SERVICE_NAME.MOBILE_CARD) {
			type = 'Pin';
		}
		const result = [
			{ leftTitle: 'Thanh Toán', rightTitle: cardType },
			{ leftTitle: 'Nhà cung cấp', rightTitle: supplier === MobileConstant.CARD_SUPPLIER_NAME.GATE ? 'Thẻ đa năng Gate' : supplier },
			{ leftTitle: 'Chiết khấu', rightTitle: `${_.toNumber(new Decimal(100).mul(new Decimal(card.discount).div(card.amount)))}%` },
			// ví mỗi lần chỉ mua dc 1 thẻ cào
			{ leftTitle: `Mã ${type}`, rightTitle: GateDecoder.decrypt(_.get(card, 'cardInfo[0].pin', '')) },
			{ leftTitle: 'Số seri', rightTitle: _.get(card, 'cardInfo[0].serial', '') }
		];
		return result;
	}
};

module.exports = async function (history) {
	// eslint-disable-next-line no-useless-catch
	try {
		const card = await this.broker.call('v1.eWalletMobileCardModel.findOne', [{ transaction: _.get(history, 'service.transaction', false) }]);
		if (_.isNil(card)) {
			// load default
			return await this.defaultDetail(history);
		}
		let cardType = GeneralConstant.SERVICE_NAME.CARD_GAME;
		let icon = GeneralConstant.DISPLAY_ICON.GAME_CARD;
		if (_.includes([
			MobileConstant.CARD_SUPPLIER.VIETTEL,
			MobileConstant.CARD_SUPPLIER.MOBI,
			MobileConstant.CARD_SUPPLIER.VINA,
			MobileConstant.CARD_SUPPLIER.BEELINE,
			MobileConstant.CARD_SUPPLIER.VN_MOBI
		], card.supplier)
		) {
			cardType = GeneralConstant.SERVICE_NAME.MOBILE_CARD;
			icon = GeneralConstant.DISPLAY_ICON.BUY_CARD;
		}
		const item = {
			blockInfo: [
				{
					icon,
					title: `${cardType} ${_.get(GeneralConstant.STATE_PAYMENT, `${_.get(history, 'service.state', '')}`, '')}`,
					description: `${Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: [],
			info: [],
			action: [],
			webview: []
		};
		// if (serviceType !== GeneralConstant.SERVICE_TYPE.TOPUP) {
		//   return item;
		// }
		item.transaction = Private.transactionBlock(history);

		item.info = Private.infoTopupBlock(history, card);
		const action = {
			type: 'openPopup',
			data: {
				popupId: 131,
				params:
				{
					seri: _.get(card, 'cardInfo[0].serial', ''),
					pin: GateDecoder.decrypt(_.get(card, 'cardInfo[0].pin', '')),
					supplier: MobileConstant.CARD_SUPPLIER_NAME[(_.get(card, 'supplier', '')).toUpperCase()]
				}
			}
		};
		if (_.get(history, 'service.state', '') === GeneralConstant.STATE.SUCCEEDED) {
			item.action = [{
				rightColor: '#00be00',
				rightTitle: 'Sao chép',
				rightFirstIcon: 'icon_copy_green',
				rightAction: JSON.stringify(action)
			}];
		}

		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED.toString()),
			data: item
		};
	} catch (error) {
		this.logger.info(`[ewalletHistory] Get Detail History Method -> Mobile Card Error: ${error}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED.toString()),
			data: null
		};
	}
};
