const _ = require('lodash');

const Moment = require('moment');
const Numeral = require('numeral');
const ErrorCodeConstant = require('../constants/errorCode.constant');
const GeneralConstant = require('../constants/general.constant');

const Private = {
	blockCreditBalance() {
		return [{ leftTitle: 'Phương thức', rightTitle: GeneralConstant.ITEM_METHOD.PAYME }];
	},

	blockTransaction(history) {
		const transactionDetail = [
			{
				leftTitle: 'Mã giao dịch',
				rightTitle: _.get(history, 'service.transaction', '')
			},
			{
				leftTitle: 'Thời gian giao dịch',
				rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
			}
		];

		return _.concat(transactionDetail, Private.blockCreditBalance());
	},

	itemInfo(packageInfo) {
		const result = [
			{ leftTitle: 'Người chuyển', rightTitle: packageInfo.supplierName }
		];

		result.push({ leftTitle: 'Nội dung', rightTitle: 'Ứng tiền ví tín dụng' });
		return result;
	}
};

module.exports = async function (history) {
	try {
		const item = {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.ADVANCE_MONEY_CREDIT_BALANCE,
					title: `${GeneralConstant.SERVICE_NAME.ADVANCE_MONEY} ${_.get(GeneralConstant.STATE_PAYMENT, `${_.get(history, 'service.state', '')}`, '')}`,
					description: `${Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: Private.blockTransaction(history),
			info: [],
			action: [],
			webview: []
		};
		const creditBalanceHis = await this.broker.call('v1.creditBalanceHistoryModel.findOne', [{ id: _.get(history, 'service.data.creditBalanceId', 0) }]);
		if (_.get(creditBalanceHis, 'id', null) !== null) {
			const packageInfo = await this.broker.call('v1.creditBalancePackageModel.findOne', [{ id: creditBalanceHis.packageId }]);
			item.info = Private.itemInfo(packageInfo);
		}
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED.toString()),
			data: item
		};
	} catch (error) {
		this.logger.info(`[ewalletHistory] Get Detail History Method -> Advance Money Error: ${error}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED.toString()),
			data: null
		};
	}
};
