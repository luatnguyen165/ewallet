const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;

const ErrorCodeConstant = require('../constants/errorCode.constant');
const GeneralConstant = require('../constants/general.constant.js');

const DETAIL_HISTORY_METHOD_NAME = {
	BILL: 'GetBillDetail',
	MOBILE_CARD: 'GetMobileCardDetail',
	MOBILE_TOPUP: 'GetMobileTopupDetail',
	ISEC: 'GetIsecDetail',
	DEPOSIT: 'GetDepositDetail',
	WITHDRAW: 'GetWithdrawDetail',
	REFUND_MONEY: 'GetRefundMoneyDetail',
	TRANSFER_MONEY: 'GetTransferMoneyDetail',
	SOCIAL_PAYMENT: 'GetSocialPaymentDetail',
	SALARY: 'GetSalaryDetail',
	OPEN_EWALLET_PAYMENT: 'GetOpenEwalletPaymentDetail',
	WALLET_QR: 'GetWalletQRDetail',
	WALLET_PAY: 'GetWalletPayDetail',
	PAY_QRCODE: 'GetPayQrCodeDetail',
	CREDIT_STATEMENT: 'GetPayCreditStatementDetail',
	ADVANCE_MONEY: 'GetAdvanceMoneyDetail'
};

module.exports = async function (history) {
	const response = {
		code: ErrorCodeConstant.GET_DETAIL_FROM_NAVIGATE_FAILED,
		data: {
			message: this.__(ErrorCodeConstant.GET_DETAIL_FROM_NAVIGATE_FAILED.toString())
		}
	};
	try {
		let detailHistory;
		const serviceType = _.get(history, 'service.type', '');
		if (_.includes(GeneralConstant.SERVICE_TYPE, serviceType)) {
			console.log('serviceType', serviceType);
			detailHistory = await this[DETAIL_HISTORY_METHOD_NAME[serviceType]](history);
		} else detailHistory = await this.GetDefaultDetail(history);
		this.logger.info(`[eWalletHistory] Navigate History Detail Method: ${detailHistory}`);
		response.code = ErrorCodeConstant.GET_DETAIL_FROM_NAVIGATE_SUCCEEDED;
		response.message = this.__(ErrorCodeConstant.GET_DETAIL_FROM_NAVIGATE_FAILED.toString());
		response.data = detailHistory;
		return response;
	} catch (err) {
		throw new MoleculerError(err);
	};
};
