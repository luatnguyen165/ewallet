const _ = require('lodash');

const ErrorCodeConstant = require('../constants/errorCode.constant');
const FrontendConstant = require('../constants/frontent.constant');
const HistoryConstant = require('../constants/history.constant');

module.exports = async function (historyId, service, history = {}) {
	try {
		let serviceType = null;
		const serviceData = !_.isEmpty(service.data) ? service.data : {};
		let method = null;
		let data = {
			...serviceData,
			resolveType: serviceData.resolveType || 'BasicObject'
		};
		const serviceCode = _.get(service, 'code', null) || _.get(service, 'type', null);
		if (!serviceCode) {
			return {
				code: ErrorCodeConstant.METHOD_SERVICE.FAILED,
				message: this.__(ErrorCodeConstant.METHOD_SERVICE.FAILED.toString()),
				data: {}
			};
		}
		switch (serviceCode) {
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT:
			case FrontendConstant.SERVICES.SOCIAL_DONATE_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_DONATE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_REQUEST_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_SEND_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_SEND_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_PAYME_RECEIVE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_NAPAS_RECEIVE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_DONATE_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_DONATE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_REQUEST_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_SEND_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_SEND_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_PAYME_RECEIVE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_NAPAS_RECEIVE_MONEY:
				serviceType = HistoryConstant.SERVICE_TYPE.SOCIAL_PAYMENT;
				if (history.changed === '-') {
					method = HistoryConstant.SERVICE_METHOD.TRANSFER_BANK;
				}
				if (history.changed === '+') {
					method = HistoryConstant.SERVICE_METHOD.RECEIVE_MONEY;
				}
				data = {
					resolveType: FrontendConstant.RESOLVE_TYPE.SOCIAL_PAYMENT,
					...serviceData
				};
				break;
			case FrontendConstant.SERVICES.WITHDRAW:
			case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_PVCBANK:
			case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_GATEWAY:
			case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_OCBBANK:
			case FrontendConstant.SERVICES.WITHDRAW_BANK_MANUAL:
				serviceType = HistoryConstant.SERVICE_TYPE.WITHDRAW;
				method = HistoryConstant.SERVICE_METHOD.WITHDRAW;

				data = {
					resolveType: FrontendConstant.RESOLVE_TYPE.WITHDRAW,
					...serviceData
				};
				break;
			//
			case FrontendConstant.SERVICES.WITHDRAW_BANK_GATEWAY:
				if (_.get(history, 'extraData.type', 'WITHDRAW') === 'TRANSFER') {
					serviceType = HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY;
					method = HistoryConstant.SERVICE_METHOD.TRANSFER_BANK;
					data = {
						resolveType: FrontendConstant.RESOLVE_TYPE.TRANSFER_MONEY,
						...serviceData
					};
				} else {
					serviceType = HistoryConstant.SERVICE_TYPE.WITHDRAW;
					method = HistoryConstant.SERVICE_METHOD.WITHDRAW;
					data = {
						resolveType: FrontendConstant.RESOLVE_TYPE.WITHDRAW,
						...serviceData
					};
				}

				break;
			//
			case FrontendConstant.SERVICES.DEPOSIT:
			case FrontendConstant.SERVICES.DEPOSIT_BANK_MANUAL:
			case FrontendConstant.SERVICES.DEPOSIT_PVCBANK:
			case HistoryConstant.SERVICE_TYPE.SALARY:
				serviceType = HistoryConstant.SERVICE_TYPE.DEPOSIT;
				method = HistoryConstant.SERVICE_METHOD.DEPOSIT;

				data = {
					resolveType: FrontendConstant.RESOLVE_TYPE.DEPOSIT,
					...serviceData
				};
				break;
			case FrontendConstant.SERVICES.BILL:
				serviceType = HistoryConstant.SERVICE_TYPE.BILL;
				method = HistoryConstant.SERVICE_METHOD.PAYMENT;

				data = {
					resolveType: FrontendConstant.RESOLVE_TYPE.BILL,
					...serviceData
				};
				break;
			case FrontendConstant.SERVICES.LINKED:
				serviceType = HistoryConstant.SERVICE_TYPE.LINKED;
				break;
			case FrontendConstant.SERVICES.MOBILE_CARD:
				serviceType = HistoryConstant.SERVICE_TYPE.MOBILE_CARD;
				method = HistoryConstant.SERVICE_METHOD.PAYMENT;

				data = {
					resolveType: FrontendConstant.RESOLVE_TYPE.CARD,
					...serviceData
				};
				break;
			case FrontendConstant.SERVICES.MOBILE_TOPUP:
				serviceType = HistoryConstant.SERVICE_TYPE.MOBILE_TOPUP;
				method = HistoryConstant.SERVICE_METHOD.PAYMENT;

				data = {
					resolveType: FrontendConstant.RESOLVE_TYPE.TOPUP,
					...serviceData
				};
				break;
			case FrontendConstant.SERVICES.TRANSFER_PAYME:
				serviceType = HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY;

				if (history.changed === '+') {
					method = HistoryConstant.SERVICE_METHOD.RECEIVE_MONEY;
				}
				if (history.changed === '-') {
					method = HistoryConstant.SERVICE_METHOD.TRANSFER_WALLET;
				}
				if (_.includes(history.tags, FrontendConstant.TAGS.WITHDRAW)) {
					method = HistoryConstant.SERVICE_METHOD.TRANSFER_BANK;
				}
				data = {
					resolveType: FrontendConstant.RESOLVE_TYPE.TRANSFER_MONEY,
					...serviceData
				};
				break;
			case FrontendConstant.SERVICES.TRANSFER_MONEY:
				serviceType = HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY;
				method = HistoryConstant.SERVICE_METHOD.TRANSFER_WALLET;
				if (history.changed === '+') {
					method = HistoryConstant.SERVICE_METHOD.RECEIVE_MONEY;
				}
				if (_.includes(history.tags, FrontendConstant.TAGS.WITHDRAW)) {
					method = HistoryConstant.SERVICE_METHOD.TRANSFER_BANK;
				}
				data = {
					resolveType: FrontendConstant.RESOLVE_TYPE.TRANSFER_MONEY,
					...serviceData
				};
				break;
			case FrontendConstant.SERVICES.OPEN_EWALLET_PAYMENT:
				serviceType = HistoryConstant.SERVICE_TYPE.OPEN_EWALLET_PAYMENT;
				method = HistoryConstant.SERVICE_METHOD.PAYMENT;
				data = {
					resolveType: FrontendConstant.RESOLVE_TYPE.OPEN_EWALLET_PAYMENT,
					...serviceData
				};
				break;
			case FrontendConstant.SERVICES.REFUND_MONEY:
				serviceType = HistoryConstant.SERVICE_TYPE.REFUND_MONEY;
				method = HistoryConstant.SERVICE_METHOD.REFUND_MONEY;

				data = {
					resolveType: FrontendConstant.RESOLVE_TYPE.REFUND,
					...serviceData
				};
				break;
			case FrontendConstant.SERVICES.ISEC:
			case FrontendConstant.SERVICES.ISEC_REDEEM:
			case FrontendConstant.SERVICES.ISEC_SCRATCH:
			case FrontendConstant.SERVICES.ISEC_SAVE:
			case FrontendConstant.SERVICES.ISEC_SEND:
			case FrontendConstant.SERVICES.ISEC_RECEIVED:
			case FrontendConstant.SERVICES.ISEC_DONATED:
				serviceType = HistoryConstant.SERVICE_TYPE.ISEC;
				if (history.changed === '-') {
					method = HistoryConstant.SERVICE_METHOD.PAYMENT;
				}
				if (history.changed === '+') {
					method = HistoryConstant.SERVICE_METHOD.RECEIVE_MONEY;
				}

				data = {
					resolveType: FrontendConstant.RESOLVE_TYPE.ISEC,
					...serviceData
				};
				break;

			case FrontendConstant.SERVICES.CREDIT_SETTLEMENT:
				method = HistoryConstant.SERVICE_METHOD.PAYMENT;
				data = {
					resolveType: 'PaymeObject',
					...serviceData
				};
				break;
			case FrontendConstant.SERVICES.ADVANCE_MONEY:
				method = HistoryConstant.SERVICE_METHOD.RECEIVE_MONEY;
				data = {
					resolveType: 'PaymeCreditObject',
					...serviceData
				};
				break;
			case FrontendConstant.SERVICES.ADVANCE_SALARY:
				method = HistoryConstant.SERVICE_METHOD.RECEIVE_MONEY;
				data = {
					resolveType: 'AdvanceSalaryObject',
					...serviceData
				};
				break;
			case FrontendConstant.SERVICES.PAY_QRCODE:
				method = HistoryConstant.SERVICE_METHOD.PAYMENT;
				break;
			default:
				serviceType = HistoryConstant.SERVICE_TYPE.BASIC;
				break;
		}

		return {
			code: ErrorCodeConstant.METHOD_SERVICE.SUCCEEDED,
			message: this.__(ErrorCodeConstant.METHOD_SERVICE.SUCCEEDED.toString()),
			data: {
				id: service.id,
				name: service.name,
				state: service.state,
				code: serviceCode,
				transaction: service.transaction,
				type: service.type || serviceType,
				data,
				method
			}
		};
	} catch (error) {
		this.logger.info(`[HistoryService] Get Method Error: ${error}`);
		return {
			code: ErrorCodeConstant.METHOD_SERVICE.FAILED,
			message: this.__(ErrorCodeConstant.METHOD_SERVICE.FAILED.toString()),
			data: {}
		};
	}
};
