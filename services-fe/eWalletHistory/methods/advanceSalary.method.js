const _ = require('lodash');
const Moment = require('moment');
const Numeral = require('numeral');

const GeneralConstant = require('../constants/general.constant');
const ErrorCodeConstant = require('../constants/errorCode.constant');

const Private = {
	blockTransaction(history) {
		return [
			{
				leftTitle: 'Mã giao dịch',
				rightTitle: _.get(history, 'service.transaction', '')
			},
			{
				leftTitle: 'Thời gian giao dịch',
				rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
			},
			{ leftTitle: 'Phương thức', rightTitle: GeneralConstant.ITEM_METHOD.PAYME }
		];
	},

	transferPay(history, transfer) {
		return {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.TRANSFER_MONEY,
					title: `${GeneralConstant.SERVICE_NAME.TRANSFER_MONEY} thành công`,
					description: Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0'),
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: [
				{
					leftTitle: 'Mã giao dịch',
					rightTitle: _.get(history, 'service.transaction', '')
				},
				{
					leftTitle: 'Thời gian giao dịch',
					rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
				},
				{
					leftTitle: 'Phí giao dịch',
					rightTitle: 'Miễn phí'
				},
				{
					leftTitle: 'Tổng thanh toán',
					rightTitle: `${Numeral(Math.ceil(_.toNumber(history.total))).format('0,0')} đ`
				}
			],
			info: [
				{
					leftTitle: 'Chuyến đến',
					rightTitle: GeneralConstant.ITEM_METHOD.PAYME
				},
				{
					leftTitle: 'Người nhận',
					rightTitle: _.get(transfer, 'methodData.accountInfo.fullname', '')
				},
				{
					leftTitle: 'Ghi chú',
					rightTitle: _.get(transfer, 'note', '')
				}
			],
			action: [],
			webview: []
		};
	},

	async itemInfo(history, advanceSalary) {
		return [
			{ leftTitle: 'Người gửi', rightTitle: _.get(advanceSalary, 'merchantName', '') },
			{ leftTitle: 'Ghi chú', rightTitle: _.get(advanceSalary, 'dueDate.title', '') }
		];
	}
};

module.exports = async function (history) {
	try {
		const advanceSalary = await this.broker.call('v1.advanceSalaryModel.findOne', [{ transaction: _.get(history, 'service.transaction', '') }]);
		if (history.changed === '-') {
			const transfer = await TransportModel.findOne({ transaction: _.get(history, 'service.transaction', '') });
			return Private.transferPay(history, transfer);
		}
		const item = {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.RECEIVE_MONEY,
					title: `${GeneralConstant.SERVICE_NAME.RECEIVE_MONEY} ${_.get(GeneralConstant.STATE_PAYMENT, `${_.get(history, 'service.state', '')}`, '')}`,
					description: `${Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: Private.blockTransaction(history),
			info: Private.itemInfo(history, advanceSalary),
			action: [],
			webview: []
		};
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED.toString()),
			data: item
		};
	} catch (error) {
		this.logger.info(`[ewalletHistory] Get Detail History Method -> Advance Salary Error: ${error}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED.toString()),
			data: null
		};
	}
};
