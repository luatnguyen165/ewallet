const _ = require('lodash');
const Moment = require('moment');
const Numeral = require('numeral');

const GeneralConstant = require('../constants/general.constant');
const ErrorCodeConstant = require('../constants/errorCode.constant');

const Private = {

	blockTransactionEnd(history) {
		return [
			{ leftTitle: 'Phí giao dịch', rightTitle: history.fee ? `${Numeral(history.fee).format('0,0')} đ` : 'Miễn phí' },
			{ leftTitle: 'Tổng thanh toán', rightTitle: `${Numeral(history.total).format('0,0')} đ`, rightColor: '#ec2a2a' }
		];
	},

	transactionBlock(history) {
		const serviceType = _.get(history, 'service.type', '');

		const transactionDetail = [
			{
				leftTitle: 'Mã giao dịch',
				rightTitle: _.get(history, 'service.transaction', '')
			},
			{
				leftTitle: 'Thời gian giao dịch',
				rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
			}
		];

		if (_.get(history, 'service.state') !== GeneralConstant.STATE.SUCCEEDED) {
			return _.concat(transactionDetail, Private.blockTransactionEnd(history));
		}

		let description = '';
		// rỗng + transport từ ví hiển thị số dư ví
		if (!serviceType) {
			switch (_.get(history, 'method.group', null)) {
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_BANK:
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_GATEWAY:
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_BANK_OCBBANK:
					description = GeneralConstant.ITEM_METHOD.LINKED;
					break;
				case GeneralConstant.PAYMENT_METHOD_GROUP.GATEWAY:
					description = GeneralConstant.ITEM_METHOD.ATM;
					break;
				case GeneralConstant.PAYMENT_METHOD_GROUP.DEPOSIT_BANK_MANUAL:
					description = GeneralConstant.ITEM_METHOD.TRANSFER_BANK;
					break;
				default:
					break;
			}
		}
		// rut về đâu là lấy transport
		const transportMethod = _.get(history, 'transport.method', null);

		if (serviceType) {
			switch (transportMethod) {
				case GeneralConstant.PAYMENT_TYPE.LINKED:
					description = GeneralConstant.ITEM_METHOD.LINKED;
					break;
				case GeneralConstant.PAYMENT_TYPE.BANK_CARD:
					description = GeneralConstant.ITEM_METHOD.ATM;
					break;
				case GeneralConstant.PAYMENT_TYPE.BANK_ACCOUNT:
					description = GeneralConstant.ITEM_METHOD.ACCOUNT;
					break;
				case GeneralConstant.PAYMENT_TYPE.PAYME_CREDIT:
					if (_.get(history, 'service.type', null) !== GeneralConstant.SERVICE_TYPE.ADVANCE_MONEY) {
						description = GeneralConstant.ITEM_METHOD.PAYME_CREDIT;
					}
					break;
				default:
					break;
			}
		}

		let cardType = 'tài khoản';
		if (transportMethod === GeneralConstant.PAYMENT_TYPE.BANK_CARD) {
			cardType = 'thẻ';
		}
		if (history.state !== GeneralConstant.STATE.PENDING) {
			transactionDetail.push(
				{ leftTitle: 'Phương thức', rightTitle: description },
				{ leftTitle: `Số ${cardType}`, rightTitle: _.get(history, 'transport.description', '') }
			);
		}
		return _.concat(transactionDetail, Private.blockTransactionEnd(history));
	}
};

module.exports = async function (history) {
	try {
		const item = {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.WITHDRAW,
					title: `${GeneralConstant.SERVICE_NAME.WITHDRAW} ${_.get(GeneralConstant.STATE_PAYMENT, `${_.get(history, 'service.state', '')}`, '')}`,
					description: `${Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: [],
			info: [],
			action: [],
			webview: []
		};
		item.transaction = Private.transactionBlock(history);

		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED.toString()),
			data: item
		};
	} catch (error) {
		this.logger.info(`[ewalletHistory] Get Detail History Method -> Withdraw Error: ${error}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED.toString()),
			data: null
		};
	}
};
