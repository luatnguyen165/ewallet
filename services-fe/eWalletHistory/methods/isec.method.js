const _ = require('lodash');

const Moment = require('moment');
const Numeral = require('numeral');
const Decimal = require('decimal.js');

const GeneralConstant = require('../constants/general.constant');
const ErrorCodeConstant = require('../constants/errorCode.constant');

const Private = {
	async depositISEC(history, iSec) {
		return {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.RECEIVE_MONEY,
					title: `${GeneralConstant.SERVICE_NAME.RECEIVE_MONEY} thành công`,
					description: `${Numeral(Math.ceil(_.toNumber(history.total))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: [
				{
					leftTitle: 'Mã giao dịch',
					rightTitle: _.get(history, 'service.transaction', '')
				},
				{
					leftTitle: 'Thời gian giao dịch',
					rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
				},
				{
					leftTitle: 'Phương thức',
					rightTitle: 'ISec'
				}
			],
			info: [{
				leftTitle: 'Số iSec',
				rightTitle: `${_.get(iSec, 'iSec.hint', '')}-XXXXXXX`
			},
			{
				leftTitle: 'Ghi chú',
				rightTitle: 'Nhận tiền iSec'
			}],
			action: [],
			webview: []
		};
	},
	cancelISEC(history) {
		return {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.BONUS,
					title: `${GeneralConstant.SERVICE_NAME.CANCEL_ISEC} thành công`,
					description: `${Numeral(Math.ceil(_.toNumber(history.total))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: [
				{
					leftTitle: 'Mã giao dịch',
					rightTitle: _.get(history, 'service.transaction', '')
				},
				{
					leftTitle: 'Thời gian giao dịch',
					rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
				},
				{
					leftTitle: 'Lý do',
					rightTitle: history.description
				}
			],
			info: [],
			action: [],
			webview: []
		};
	},
	blockWallet() {
		return [{ leftTitle: 'Phương thức', rightTitle: GeneralConstant.ITEM_METHOD.PAYME }];
	},
	blockCreditBalance() {
		return [{ leftTitle: 'Phương thức', rightTitle: GeneralConstant.ITEM_METHOD.CREDIT_BALANCE }];
	},

	blockTransactionEnd(history) {
		return [
			{ leftTitle: 'Phí giao dịch', rightTitle: history.fee ? `${Numeral(history.fee).format('0,0')} đ` : 'Miễn phí' },
			{ leftTitle: 'Tổng thanh toán', rightTitle: `${Numeral(history.total).format('0,0')} đ`, rightColor: '#ec2a2a' }
		];
	},

	transactionBlock(history) {
		const serviceType = _.get(history, 'service.type', '');

		const transactionDetail = [
			{
				leftTitle: 'Mã giao dịch',
				rightTitle: _.get(history, 'service.transaction', '')
			},
			{
				leftTitle: 'Thời gian giao dịch',
				rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
			}
		];

		if (_.get(history, 'payment.method', '') === GeneralConstant.PAYMENT_TYPE.WALLET) {
			return _.concat(transactionDetail, Private.blockWallet(history), Private.blockTransactionEnd(history));
		}

		if (_.get(history, 'payment.method', '') === GeneralConstant.PAYMENT_TYPE.CREDIT_BALANCE) {
			return _.concat(transactionDetail, Private.blockCreditBalance(), Private.blockTransactionEnd(history));
		}

		let description = GeneralConstant.ITEM_METHOD.PAYME;
		// rỗng + transport từ ví hiển thị số dư ví
		if (!serviceType) {
			switch (_.get(history, 'method.group', null)) {
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_BANK:
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_GATEWAY:
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_BANK_OCBBANK:
					description = GeneralConstant.ITEM_METHOD.LINKED;
					break;
				case GeneralConstant.PAYMENT_METHOD_GROUP.GATEWAY:
					description = GeneralConstant.ITEM_METHOD.ATM;
					break;
				case GeneralConstant.PAYMENT_METHOD_GROUP.DEPOSIT_BANK_MANUAL:
					description = GeneralConstant.ITEM_METHOD.TRANSFER_BANK;
					break;
				default:
					break;
			}
		}

		const paymentMethod = _.get(history, 'payment.method', null) || _.get(history, 'transport.method', null);

		if (serviceType) {
			switch (paymentMethod) {
				case GeneralConstant.PAYMENT_TYPE.LINKED:
					description = GeneralConstant.ITEM_METHOD.LINKED;
					break;
				case GeneralConstant.PAYMENT_TYPE.BANK_CARD:
					description = GeneralConstant.ITEM_METHOD.ATM;
					break;
				case GeneralConstant.PAYMENT_TYPE.BANK_ACCOUNT:
					description = GeneralConstant.ITEM_METHOD.ACCOUNT;
					break;
				case GeneralConstant.PAYMENT_TYPE.PAYME_CREDIT:
					if (_.get(history, 'service.type', null) !== GeneralConstant.SERVICE_TYPE.ADVANCE_MONEY) {
						description = GeneralConstant.ITEM_METHOD.PAYME_CREDIT;
					}
					break;
				default:
					break;
			}
		}
		if (description === GeneralConstant.ITEM_METHOD.PAYME) {
			return _.concat(transactionDetail, Private.blockWallet(history), Private.blockTransactionEnd(history));
		}

		let cardType = 'tài khoản';
		if (paymentMethod === GeneralConstant.PAYMENT_TYPE.BANK_CARD) {
			cardType = 'thẻ';
		}
		if (history.state !== GeneralConstant.HISTORY_STATE.PENDING) {
			transactionDetail.push(
				{ leftTitle: 'Phương thức', rightTitle: description },
				{ leftTitle: `Số ${cardType}`, rightTitle: _.get(history, 'payment.description', '') }
			);
		}

		return _.concat(transactionDetail, Private.blockTransactionEnd(history));
	},

	infoTopupBlock(iSec) {
		// TODO: check iSec BULk
		const reulst = [
			{ leftTitle: 'Thanh Toán', rightTitle: GeneralConstant.SERVICE_NAME.ISEC },
			{ leftTitle: 'Hoàn tiền', rightTitle: `${_.ceil(_.toNumber(new Decimal(100).sub(new Decimal(iSec.total).mul(100).div(iSec.amount))))}%` }
		];
		const iSecHint = _.get(iSec, 'iSec.hint', '') || '';

		if (iSecHint !== '') {
			reulst.push({ leftTitle: 'Mã iSec', rightTitle: `${_.get(iSec, 'iSec.hint', '')}-XXXXXXX` });
		}
		return reulst;
	}
};

module.exports = async function (history) {
	try {
		const iSec = await this.broker.call('v1.eWalletIsecModel.findOne', [{ transaction: _.get(history, 'service.transaction', '') }]);
		if (_.isNil(history.changed) || _.isNil(iSec)) {
			return await this.defaultDetail(history);
		}
		// huy iSEc
		if (history.changed === '+' && _.get(history, 'service.method', '') === GeneralConstant.SERVICE_TYPE.REFUND_MONEY) {
			return Private.cancelISEC(history, iSec);
		}
		// nap isec
		if (history.changed === '+' && _.get(history, 'service.method', '') === GeneralConstant.SERVICE_TYPE.RECEIVE_MONEY) {
			return Private.depositISEC(history, iSec);
		}

		const item = {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.ISEC,
					// iSec lấy state ngoài... state trong dịch vụ
					title: `${GeneralConstant.SERVICE_NAME.ISEC} ${_.get(GeneralConstant.STATE_PAYMENT, `${_.get(history, 'state', '')}`, '')}`,
					description: `${Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: [],
			info: [],
			action: [],
			webview: []
		};
		item.transaction = Private.transactionBlock(history);

		item.info = Private.infoTopupBlock(iSec || {});
		if (_.get(iSec, 'iSec.hint', null) !== null) {
			item.action = [{
				rightColor: '#00be00',
				rightTitle: 'Xem mã iSec',
				rightFirstIcon: 'iconEyeView',
				rightAction: JSON.stringify({ type: 'openScreen', data: { screenName: 'IsecCodeInfo', params: { iSecId: iSec.id, bGoBackOnBackPress: true } } })
			}];
		}
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED.toString()),
			data: item
		};
	} catch (error) {
		this.logger.info(`[ewalletHistory] Get Detail History Method -> ISec Error: ${error}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED.toString()),
			data: null
		};
	}
};
