const _ = require('lodash');

const FrontendConstant = require('../constants/frontent.constant');
const HistoryConstant = require('../constants/history.constant');

module.exports = function (data = {}) {
	let icon = null;
	const serviceCode = _.get(data, 'service.type', null) || _.get(data, 'service.code', '');
	const { changed } = data;
	switch (serviceCode) {
		case FrontendConstant.SERVICES.SOCIAL_DONATE_MONEY_LINK:
		case FrontendConstant.SERVICES.SOCIAL_PAYMENT_DONATE_MONEY_LINK:
		case FrontendConstant.SERVICES.SOCIAL_PAYME_RECEIVE_MONEY:
		case FrontendConstant.SERVICES.SOCIAL_PAYMENT_PAYME_RECEIVE_MONEY:
			icon = HistoryConstant.DISPLAY_ICON.RECEIVE_MONEY;
			break;
		case FrontendConstant.SERVICES.SOCIAL_DONATE_MONEY:
		case FrontendConstant.SERVICES.SOCIAL_PAYMENT_DONATE_MONEY:
		case FrontendConstant.SERVICES.SOCIAL_SEND_MONEY_LINK:
		case FrontendConstant.SERVICES.SOCIAL_PAYMENT_SEND_MONEY_LINK:
		case FrontendConstant.SERVICES.SOCIAL_SEND_MONEY:
		case FrontendConstant.SERVICES.SOCIAL_PAYMENT_SEND_MONEY:
			icon = HistoryConstant.DISPLAY_ICON.CALL_PAYMENT;
			break;
		case FrontendConstant.SERVICES.SOCIAL_NAPAS_RECEIVE_MONEY:
		case FrontendConstant.SERVICES.SOCIAL_PAYMENT_NAPAS_RECEIVE_MONEY:
		case FrontendConstant.SERVICES.SOCIAL_PAYMENT:
		case FrontendConstant.SERVICES.SOCIAL_REQUEST_MONEY_LINK:
		case FrontendConstant.SERVICES.SOCIAL_PAYMENT_REQUEST_MONEY_LINK:
			icon = HistoryConstant.DISPLAY_ICON.CALL_PAYMENT;
			if (changed === '+') {
				icon = HistoryConstant.DISPLAY_ICON.RECEIVE_MONEY;
				break;
			}
			break;
		case HistoryConstant.SERVICE_TYPE.WITHDRAW:
		case FrontendConstant.SERVICES.WITHDRAW_BANK_GATEWAY:
		case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_PVCBANK:
		case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_GATEWAY:
		case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_OCBBANK:
			if (_.get(data, 'extraData.type', FrontendConstant.SERVICES.WITHDRAW) === 'TRANSFER') {
				icon = HistoryConstant.DISPLAY_ICON.TRANSFER_MONEY;
			}
			icon = HistoryConstant.DISPLAY_ICON.WITHDRAW;
			break;
		case FrontendConstant.SERVICES.DEPOSIT:
		case FrontendConstant.SERVICES.DEPOSIT_BANK_MANUAL:
		case FrontendConstant.SERVICES.DEPOSIT_PVCBANK:
			icon = HistoryConstant.DISPLAY_ICON.DEPOSIT;
			break;
		case FrontendConstant.SERVICES.BILL:
			icon = _.find(HistoryConstant.DISPLAY_ICON_BILL, (k, v) => _.includes(data.tags, v)) || HistoryConstant.DISPLAY_ICON.BILL;
			// icon = HistoryConstant.DISPLAY_ICON.BILL;
			break;
		case FrontendConstant.SERVICES.ISEC:
		case FrontendConstant.SERVICES.ISEC_REDEEM:
		case FrontendConstant.SERVICES.CANCEL_ISEC:
		case FrontendConstant.SERVICES.ISEC_SAVE:
		case FrontendConstant.SERVICES.ISEC_SCRATCH:
		case FrontendConstant.SERVICES.ISEC_DONATED:
		case FrontendConstant.SERVICES.ISEC_RECEIVED:
		case FrontendConstant.SERVICES.ISEC_SEND:
			icon = HistoryConstant.DISPLAY_ICON.ISEC;
			if (changed === '+') {
				icon = HistoryConstant.DISPLAY_ICON.RECEIVE_MONEY;
			}
			if (serviceCode === FrontendConstant.SERVICES.CANCEL_ISEC || _.includes(data.tags, FrontendConstant.TAGS.CASHBACK_ISEC)) {
				icon = HistoryConstant.DISPLAY_ICON.BONUS;
			}
			// if (serviceCode === FrontendConstant.SERVICES.ISEC_SAVE) {
			//   icon = HistoryConstant.DISPLAY_ICON.ISEC;
			// }
			// if (serviceCode === FrontendConstant.SERVICES.ISEC_REDEEM || serviceCode === FrontendConstant.SERVICES.ISEC_SCRATCH) {
			//   icon = HistoryConstant.DISPLAY_ICON.DEPOSIT;
			// }
			break;
		case FrontendConstant.SERVICES.MOBILE_CARD:
			icon = HistoryConstant.DISPLAY_ICON.BUY_CARD;
			if (_.includes(_.get(data, 'service.name', ''), 'mã game')
			) {
				icon = HistoryConstant.DISPLAY_ICON.GAME_CARD;
			}
			break;
		case FrontendConstant.SERVICES.MOBILE_TOPUP:
			icon = HistoryConstant.DISPLAY_ICON.TOPUP_PHONE;
			break;
		case FrontendConstant.SERVICES.TRANSFER_PAYME:
		case HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY:
			if (changed === '+') {
				icon = HistoryConstant.DISPLAY_ICON.RECEIVE_MONEY;
				break;
			}
			icon = HistoryConstant.DISPLAY_ICON.TRANSFER_MONEY;
			break;
		case FrontendConstant.SERVICES.REFUND_MONEY:
			icon = HistoryConstant.DISPLAY_ICON.BONUS;
			break;
		case FrontendConstant.SERVICES.ADD_MONEY:
			if (changed === '+') {
				icon = HistoryConstant.DISPLAY_ICON.DEPOSIT;
				if (_.includes(_.get(data, 'tags', []), FrontendConstant.TAGS.SALARY)) {
					// nhan tien
					icon = HistoryConstant.DISPLAY_ICON.RECEIVE_MONEY;
				}
			}
			if (changed === '-') {
				icon = HistoryConstant.DISPLAY_ICON.WITHDRAW;
				break;
			}
			break;
		case FrontendConstant.SERVICES.SALARY:
			icon = HistoryConstant.DISPLAY_ICON.RECEIVE_MONEY;
			break;
		case FrontendConstant.SERVICES.LOGIN_TRUST_DEVICE:
			icon = HistoryConstant.DISPLAY_ICON.EKYC;
			break;
		case HistoryConstant.SERVICE_TYPE.ADVANCE_MONEY:
			icon = HistoryConstant.DISPLAY_ICON.ADVANCE_MONEY_CREDIT_BALANCE;
			break;
		case HistoryConstant.SERVICE_TYPE.CREDIT_SETTLEMENT:
		case HistoryConstant.SERVICE_TYPE.CREDIT_STATEMENT:
			icon = HistoryConstant.DISPLAY_ICON.CREDIT_STATEMENT_CREDIT_BALANCE;
			break;
		case HistoryConstant.SERVICE_TYPE.ADVANCE_SALARY:
		case HistoryConstant.SERVICE_TYPE.ADVANCE_SALARY_SETTLEMENT:
			icon = HistoryConstant.DISPLAY_ICON.ADVANCE_SALARY;
			break;
		case HistoryConstant.SERVICE_TYPE.FAST_LOAN:
			icon = HistoryConstant.DISPLAY_ICON.FAST_LOAN;
			break;
		case HistoryConstant.SERVICE_TYPE.OPEN_EWALLET_PAYMENT:
			icon = HistoryConstant.DISPLAY_ICON.OPEN_EWALLET_PAYMENT;
			break;
		case HistoryConstant.SERVICE_TYPE.WALLET_PAY:
		case HistoryConstant.SERVICE_TYPE.PAY_QRCODE:
			icon = HistoryConstant.DISPLAY_ICON.OPEN_EWALLET_PAYMENT;
			break;
		case HistoryConstant.SERVICE_TYPE.WALLET_QR:
			icon = HistoryConstant.DISPLAY_ICON.OPEN_EWALLET_PAYMENT;
			break;
		default:
			icon = HistoryConstant.DISPLAY_ICON.SYSTEM;
			break;
	}
	return icon;
};
