const _ = require('lodash');

const Moment = require('moment');
const Numeral = require('numeral');
const GeneralConstant = require('../constants/general.constant');
const ErrorCodeConstant = require('../constants/errorCode.constant');

const Private = {
	blockWallet() {
		return [{ leftTitle: 'Phương thức', rightTitle: GeneralConstant.ITEM_METHOD.PAYME }];
	},
	blockCreditBalance() {
		return [{ leftTitle: 'Phương thức', rightTitle: GeneralConstant.ITEM_METHOD.CREDIT_BALANCE }];
	},
	blockTransactionEnd(history) {
		return [
			{ leftTitle: 'Phí giao dịch', rightTitle: history.fee ? `${Numeral(history.fee).format('0,0')} đ` : 'Miễn phí' },
			{ leftTitle: 'Tổng thanh toán', rightTitle: `${Numeral(history.total).format('0,0')} đ`, rightColor: '#ec2a2a' }
		];
	},
	blockTransaction(history) {
		const serviceType = _.get(history, 'service.type', '');

		const transactionDetail = [
			{
				leftTitle: 'Mã giao dịch',
				rightTitle: _.get(history, 'service.transaction', '')
			},
			{
				leftTitle: 'Thời gian giao dịch',
				rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
			}
		];

		if (_.get(history, 'payment.method', '') === GeneralConstant.PAYMENT_TYPE.WALLET) {
			return _.concat(transactionDetail, Private.blockWallet(history), Private.blockTransactionEnd(history));
		}

		if (_.get(history, 'payment.method', '') === GeneralConstant.PAYMENT_TYPE.CREDIT_BALANCE) {
			return _.concat(transactionDetail, Private.blockCreditBalance(), Private.blockTransactionEnd(history));
		}

		let description = GeneralConstant.ITEM_METHOD.PAYME;
		// rỗng + transport từ ví hiển thị số dư ví
		if (!serviceType) {
			switch (_.get(history, 'method.group', null)) {
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_BANK:
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_GATEWAY:
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_BANK_OCBBANK:
					description = GeneralConstant.ITEM_METHOD.LINKED;
					break;
				case GeneralConstant.PAYMENT_METHOD_GROUP.GATEWAY:
					description = GeneralConstant.ITEM_METHOD.ATM;
					break;
				case GeneralConstant.PAYMENT_METHOD_GROUP.DEPOSIT_BANK_MANUAL:
					description = GeneralConstant.ITEM_METHOD.TRANSFER_BANK;
					break;
				default:
					break;
			}
		}

		const paymentMethod = _.get(history, 'payment.method', null) || _.get(history, 'transport.method', null);

		if (serviceType) {
			switch (paymentMethod) {
				case GeneralConstant.PAYMENT_TYPE.LINKED:
					description = GeneralConstant.ITEM_METHOD.LINKED;
					break;
				case GeneralConstant.PAYMENT_TYPE.BANK_CARD:
					description = GeneralConstant.ITEM_METHOD.ATM;
					break;
				case GeneralConstant.PAYMENT_TYPE.BANK_ACCOUNT:
					description = GeneralConstant.ITEM_METHOD.ACCOUNT;
					break;
				case GeneralConstant.PAYMENT_TYPE.PAYME_CREDIT:
					if (_.get(history, 'service.type', null) !== GeneralConstant.SERVICE_TYPE.ADVANCE_MONEY) {
						description = GeneralConstant.ITEM_METHOD.PAYME_CREDIT;
					}
					break;
				default:
					break;
			}
		}
		let cardType = 'tài khoản';
		if (paymentMethod === GeneralConstant.PAYMENT_TYPE.BANK_CARD) {
			cardType = 'thẻ';
		}
		if (history.state !== GeneralConstant.STATE.PENDING) {
			transactionDetail.push(
				{ leftTitle: 'Phương thức', rightTitle: description },
				{ leftTitle: `Số ${cardType}`, rightTitle: _.get(history, 'payment.description', '') }
			);
		}
		return _.concat(transactionDetail, Private.blockTransactionEnd(history));
	},

	itemInfo(history, info) {
		const result = [
			{ leftTitle: 'Người nhận', rightTitle: _.get(info, 'qrCodeInfo.merchantName', '') }
		];
		const description = _.get(info, 'qrCodeInfo.description', '') || '';
		if (description !== '') {
			result.push({ leftTitle: 'Nội dung', rightTitle: description });
		}
		return result;
	}
};

module.exports = async function (history) {
	try {
		const qrPayInfo = await this.broker.call('v1.eWalletPayQRCodeModel.findOne', [{ transaction: _.get(history, 'service.transaction', '') }]);
		const item = {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.OPEN_EWALLET_PAYMENT,
					title: `${GeneralConstant.SERVICE_NAME.PAYMENT} ${_.get(GeneralConstant.STATE_PAYMENT, `${_.get(history, 'service.state', '')}`, '')}`,
					description: `${Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: Private.blockTransaction(history),
			info: Private.itemInfo(history, qrPayInfo),
			action: [],
			webview: []
		};
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED.toString()),
			data: item
		};
	} catch (error) {
		this.logger.info(`[ewalletHistory] Get Detail History Method -> Pay QRCode Error: ${error}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED.toString()),
			data: null
		};
	}
};
