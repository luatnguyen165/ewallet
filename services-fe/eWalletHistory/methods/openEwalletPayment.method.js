const _ = require('lodash');

const Moment = require('moment');
const Numeral = require('numeral');
const GeneralConstant = require('../constants/general.constant');
const ErrorCodeConstant = require('../constants/errorCode.constant');

const Private = {
	blockWallet() {
		return [{ leftTitle: 'Phương thức', rightTitle: GeneralConstant.ITEM_METHOD.PAYME }];
	},
	blockBankQR() {
		return [{ leftTitle: 'Phương thức', rightTitle: GeneralConstant.ITEM_METHOD.QR_PAY }];
	},

	blockTransactionEnd(history) {
		return [
			{ leftTitle: 'Phí giao dịch', rightTitle: history.fee ? `${Numeral(history.fee).format('0,0')} đ` : 'Miễn phí' },
			{ leftTitle: 'Tổng thanh toán', rightTitle: `${Numeral(history.total).format('0,0')} đ`, rightColor: '#ec2a2a' }
		];
	},
	transactionBlock(history) {
		const serviceType = _.get(history, 'service.type', '');

		const transactionDetail = [
			{
				leftTitle: 'Mã giao dịch',
				rightTitle: _.get(history, 'service.transaction', '')
			},
			{
				leftTitle: 'Thời gian giao dịch',
				rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
			}
		];

		if (_.get(history, 'payment.method', '') === GeneralConstant.PAYMENT_TYPE.WALLET) {
			return _.concat(transactionDetail, Private.blockWallet(history), Private.blockTransactionEnd(history));
		}

		if (_.get(history, 'payment.method', '') === GeneralConstant.PAYMENT_TYPE.BANK_QR_CODE_PG) {
			return _.concat(transactionDetail, Private.blockBankQR(history), Private.blockTransactionEnd(history));
		}

		let description = GeneralConstant.ITEM_METHOD.PAYME;

		const paymentMethod = _.get(history, 'payment.method', null) || _.get(history, 'transport.method', null);

		if (serviceType) {
			switch (paymentMethod) {
				case GeneralConstant.PAYMENT_TYPE.LINKED:
					description = GeneralConstant.ITEM_METHOD.LINKED;
					break;
				case GeneralConstant.PAYMENT_TYPE.BANK_CARD:
				case GeneralConstant.PAYMENT_TYPE.BANK_CARD_PG:
					description = GeneralConstant.ITEM_METHOD.ATM;
					break;
				case GeneralConstant.PAYMENT_TYPE.CREDIT_CARD_PG:
					description = GeneralConstant.ITEM_METHOD.CREDIT_CARD;
					break;
				case GeneralConstant.PAYMENT_TYPE.BANK_ACCOUNT:
					description = GeneralConstant.ITEM_METHOD.ACCOUNT;
					break;
				case GeneralConstant.PAYMENT_TYPE.BANK_QR_CODE_PG:
					description = GeneralConstant.ITEM_METHOD.QR_PAY;
					break;
				default:
					break;
			}
		}
		let cardType = 'tài khoản';
		if (paymentMethod === GeneralConstant.PAYMENT_TYPE.BANK_CARD) {
			cardType = 'thẻ';
		}
		if (history.state !== GeneralConstant.STATE.PENDING) {
			transactionDetail.push(
				{ leftTitle: 'Phương thức', rightTitle: description },
				{ leftTitle: `Số ${cardType}`, rightTitle: _.get(history, 'payment.description', '') }
			);
		}
		return _.concat(transactionDetail, Private.blockTransactionEnd(history));
	},
	infoPaymentBlock(info, appInfo) {
		const reulst = [
			{ leftTitle: 'Thanh Toán', rightTitle: 'Dịch vụ' },
			{ leftTitle: 'Người nhận', rightTitle: `${_.get(appInfo, 'merchantName', null)}` },
			{ leftTitle: 'Mã đơn hàng', rightTitle: `${info.orderId}` }
		];
		const note = _.get(info, 'note', '') || '';
		if (note !== '') {
			reulst.push({ leftTitle: 'Nội dung', rightTitle: `${info.note}` });
		}

		return reulst;
	}
};

module.exports = async function (history) {
	try {
		const paymentInfo = await this.broker.call('v1.eWalletOpenEwalletPaymentModel.findOne', [{ transaction: _.get(history, 'service.transaction', null) }]);
		const item = {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.OPEN_EWALLET_PAYMENT,
					title: `Thanh toán ${_.get(GeneralConstant.STATE_PAYMENT, `${_.get(history, 'service.state', '')}`, '')}`,
					description: `${Numeral(Math.ceil(_.toNumber(history.total))).format('0,0')} đ`,
					iconResult: GeneralConstant.DISPLAY_ICON.OPEN_EWALLET_PAYMENT
				}
			],
			transaction: [],
			info: [],
			action: [],
			webview: []
		};
		item.transaction = Private.transactionBlock(history);

		const appInfo = await this.broker.call('v1.ewalletAppInfoModel.findOne', [{ id: history.appId }]);
		item.info = Private.infoPaymentBlock(paymentInfo, appInfo);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED.toString()),
			data: item
		};
	} catch (error) {
		this.logger.info(`[ewalletHistory] Get Detail History Method -> Open Ewallet Payment Error: ${error}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED.toString()),
			data: null
		};
	}
};
