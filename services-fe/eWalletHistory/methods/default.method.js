const _ = require('lodash');

const Moment = require('moment');
const Numeral = require('numeral');

const GeneralConstant = require('../constants/general.constant');
const ErrorCodeConstant = require('../constants/errorCode.constant');

const Private = {

	blockTransaction(history) {
		if (history.changed === '+') {
			const result = [
				{
					leftTitle: 'Mã giao dịch',
					rightTitle: _.get(history, 'service.transaction', '')
				},
				{
					leftTitle: 'Thời gian giao dịch',
					rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
				}];
			if (history.state !== GeneralConstant.STATE.PENDING) {
				_.concat(result,
					{
						leftTitle: 'Phương thức', rightTitle: GeneralConstant.ITEM_METHOD.PAYME
					});
			}
		}
		return [
			{
				leftTitle: 'Mã giao dịch',
				rightTitle: _.get(history, 'service.transaction', '')
			},
			{
				leftTitle: 'Thời gian giao dịch',
				rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
			},
			{ leftTitle: 'Phí giao dịch', rightTitle: history.fee ? `${Numeral(history.fee).format('0,0')} đ` : 'Miễn phí' },
			{ leftTitle: 'Tổng thanh toán', rightTitle: `${Numeral(history.total).format('0,0')} đ`, rightColor: '#ec2a2a' }
		];
	}
};

module.exports = async function (history) {
	try {
		console.log('default');
		const item = {
			blockInfo: [
				{
					icon: history.changed === '+' ? GeneralConstant.DISPLAY_ICON.RECEIVE_MONEY : GeneralConstant.DISPLAY_ICON.OPEN_EWALLET_PAYMENT,
					title: `${history.changed === '+' ? GeneralConstant.SERVICE_NAME.RECEIVE_MONEY : GeneralConstant.SERVICE_NAME.PAYMENT} ${_.get(GeneralConstant.STATE_PAYMENT, `${_.get(history, 'service.state', '')}`, '')}`,
					description: `${Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: Private.blockTransaction(history),
			info: [],
			action: [],
			webview: []
		};
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED.toString()),
			data: item
		};
	} catch (error) {
		this.logger.info(`[ewalletHistory] Get Detail History Method -> Default Error: ${error}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED.toString()),
			data: null
		};
	}
};
