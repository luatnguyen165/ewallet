const _ = require('lodash');

const Moment = require('moment');
const Numeral = require('numeral');
const ErrorCodeConstant = require('../constants/errorCode.constant');
const GeneralConstant = require('../constants/general.constant');

const Private = {
	blockTransaction(history) {
		return [
			{
				leftTitle: 'Mã giao dịch',
				rightTitle: _.get(history, 'service.transaction', '')
			},
			{
				leftTitle: 'Thời gian giao dịch',
				rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
			},
			{ leftTitle: 'Phí giao dịch', rightTitle: history.fee ? `${Numeral(history.fee).format('0,0')} đ` : 'Miễn phí' },
			{ leftTitle: 'Tổng thanh toán', rightTitle: `${Numeral(history.total).format('0,0')} đ`, rightColor: '#ec2a2a' }
		];
	},

	transferReceive(history, transfer, account) {
		const result = {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.RECEIVE_MONEY,
					title: `${GeneralConstant.SERVICE_NAME.RECEIVE_MONEY} thành công`,
					description: `${Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: [
				{
					leftTitle: 'Mã giao dịch',
					rightTitle: _.get(history, 'service.transaction', '')
				},
				{
					leftTitle: 'Thời gian giao dịch',
					rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
				},
				{
					leftTitle: 'Phương thức',
					rightTitle: GeneralConstant.ITEM_METHOD.PAYME
				}
			],
			info: [{
				leftTitle: 'Người gửi',
				rightTitle: _.get(transfer, 'senderInfo.fullname', '')
			}
			],
			action: [],
			webview: []
		};
		const accountReceive = _.get(account, 'phone', '') || '';
		if (accountReceive !== '') {
			result.info.push({ leftTitle: 'Tài khoản', rightTitle: `0${(_.get(account, 'phone', '')).substr(2)}` });
		}
		const note = _.get(transfer, 'note', '') || '';
		if (note !== '') {
			result.info.push({ leftTitle: 'Ghi chú', rightTitle: _.get(transfer, 'note', '') || '' });
		}
		const reply = _.get(transfer, 'reply', '') || '';
		if (reply !== '') {
			result.info.push({ leftTitle: 'Ghi chú thanh toán', rightTitle: _.get(transfer, 'reply', '') });
		}
		return result;
	},

	async itemInfo(ctx, history, transfer) {
		const serviceType = _.get(history, 'service.type', '');
		let description = '';
		// rỗng + transport từ ví hiển thị số dư ví
		if (!serviceType) {
			switch (_.get(history, 'method.group', null)) {
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_BANK:
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_GATEWAY:
				case GeneralConstant.PAYMENT_METHOD_GROUP.LINKED_BANK_OCBBANK:
					description = GeneralConstant.ITEM_METHOD.LINKED;
					break;
				case GeneralConstant.PAYMENT_METHOD_GROUP.GATEWAY:
					description = GeneralConstant.ITEM_METHOD.ATM;
					break;
				case GeneralConstant.PAYMENT_METHOD_GROUP.DEPOSIT_BANK_MANUAL:
					description = GeneralConstant.ITEM_METHOD.TRANSFER_BANK;
					break;
				default:
					break;
			}
		}
		// rut về đâu là lấy transport
		const transportMethod = _.get(history, 'transport.method', null);

		if (serviceType) {
			switch (transportMethod) {
				case GeneralConstant.PAYMENT_TYPE.LINKED:
					description = GeneralConstant.ITEM_METHOD.LINKED;
					break;
				case GeneralConstant.PAYMENT_TYPE.BANK_CARD:
					description = GeneralConstant.ITEM_METHOD.ATM;
					break;
				case GeneralConstant.PAYMENT_TYPE.BANK_ACCOUNT:
					description = GeneralConstant.ITEM_METHOD.ACCOUNT;
					break;
				case GeneralConstant.PAYMENT_TYPE.PAYME_CREDIT:
					if (_.get(history, 'service.type', null) !== GeneralConstant.SERVICE_TYPE.ADVANCE_MONEY) {
						description = GeneralConstant.ITEM_METHOD.PAYME_CREDIT;
					}
					break;
				default:
					break;
			}
		}
		if (_.isEmpty(description)) {
			const reply = _.get(transfer, 'reply', '') || '';
			const result = [
				{ leftTitle: 'Chuyển đến', rightTitle: GeneralConstant.ITEM_METHOD.PAYME },
				{ leftTitle: 'Người nhận', rightTitle: _.get(transfer, 'receiverInfo.fullname', '') }
			];
			const note = _.get(transfer, 'note', '') || '';
			if (note !== '') {
				result.push({ leftTitle: 'Ghi chú', rightTitle: _.get(transfer, 'note', '') || '' });
			}

			if (reply !== '') {
				result.push({ leftTitle: 'Ghi chú thanh toán', rightTitle: _.get(transfer, 'reply', '') });
			}
			return result;
		}

		if (history.state !== GeneralConstant.STATE.PENDING) {
			const transport = await this.broker.call('v1.eWalletTransportModel.findOne', [{ id: _.get(transfer, 'transport.transportId', '') }]);
			const bank = await this.broker.call('v1.eWalletBankCodeModel.findOne', [{ swiftCode: _.get(transport, 'methodData.bankInfo.swiftCode', '') }]);
			const cardNumber = _.get(transport, 'methodData.bankInfo.cardNumber', '') || '';
			const accountNumber = _.get(transport, 'methodData.bankInfo.bankAccountNumber', '') || '';
			const cardHolder = _.get(transport, 'methodData.bankInfo.cardHolder', '') || '';
			const bankAccountName = _.get(transport, 'methodData.bankInfo.bankAccountName', '') || '';
			return [
				{ leftTitle: 'Chuyển đến', rightTitle: description },
				{ leftTitle: 'Ngân hàng', rightTitle: _.toUpper(_.get(bank, 'shortName', '')) },
				{
					leftTitle: `Số ${cardNumber !== '' ? 'thẻ' : 'tài khoản'}`,
					rightTitle: cardNumber !== '' ? cardNumber : accountNumber
				},
				{ leftTitle: 'Chủ tài khoản', rightTitle: cardHolder !== '' ? cardHolder : bankAccountName },
				{ leftTitle: 'Ghi chú', rightTitle: _.get(transfer, 'note', '') }
			];
		}
		return [];
	}
};

module.exports = async function (history) {
	try {
		const transfer = await this.broker.call('v1.eWalletTransferMoneyModel.findOne', [{ transaction: _.get(history, 'service.transaction', '') }]);
		if (history.changed === '+') {
			const account = await this.broker.call('v1.eWalletAccountModel.findOne', [{ id: _.get(transfer, 'senderInfo.accountId', '') }]);
			return Private.transferReceive(history, transfer, account);
		}
		const item = {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.TRANSFER_MONEY,
					title: `${GeneralConstant.SERVICE_NAME.TRANSFER_MONEY} ${_.get(GeneralConstant.STATE_PAYMENT, `${_.get(history, 'service.state', '')}`, '')}`,
					description: `${Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: Private.blockTransaction(history),
			info: Private.itemInfo(ctx, history, transfer),
			action: [],
			webview: []
		};
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED.toString()),
			data: item
		};
	} catch (error) {
		this.logger.info(`[ewalletHistory] Get Detail History Method -> Transfer Money Error: ${error}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED.toString()),
			data: null
		};
	}
};
