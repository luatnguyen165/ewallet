const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;

const ErrorCodeConstant = require('../constants/errorCode.constant');
const GeneralConstant = require('../constants/general.constant.js');

module.exports = async function (method) {
	const response = {
		code: ErrorCodeConstant.GET_PAYMENT_METHOD.FAILED,
		succeeded: false,
		data: {
			message: this.__(ErrorCodeConstant.GET_PAYMENT_METHOD.FAILED.toString())
		}
	};
	try {
		const paymentMethod = GeneralConstant.PAYMENT_METHOD[method];
		if (!paymentMethod) {
			return response;
		}
		response.data.paymentMethod = paymentMethod;
		response.code = ErrorCodeConstant.GET_PAYMENT_METHOD.SUCCEEDED;
		response.succeeded = true;
		response.data.message = this.__(ErrorCodeConstant.GET_PAYMENT_METHOD.SUCCEEDED.toString());
		return response;
	} catch (err) {
		throw new MoleculerError(err);
	};
};
