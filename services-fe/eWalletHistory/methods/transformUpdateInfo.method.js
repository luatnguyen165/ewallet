const BankCode = require('../constants/bankCode.constant');
const HistoryConstant = require('../constants/history.constant');

module.exports = async function (updateInfo, filter) {
	const historyInfo = await this.broker.call('v1.eWalletHistoryModel.findOne', [filter, '-_id']);
	// Check update service name
	if (_.has(historyInfo, 'id') && _.isEmpty(_.get(historyInfo, 'service.name', ''))) {
		let serviceName = await this.GetServiceName(historyInfo);
		serviceName = serviceName.data.serviceName;
		const serviceId = _.get(historyInfo, 'service.id', null);
		const serviceType = _.get(historyInfo, 'service.type', null);

		const paymentMethod = _.get(updateInfo, 'payment.method', null);
		const transportMethod = _.get(updateInfo, 'transport.method', null);
		const bankInfo = _.get(updateInfo, 'transport.transport.bankInfo', {}) || {};
		const bankFullInfo = bankInfo.swiftCode ? BankCode.find((value) => value.swiftCode === bankInfo.swiftCode) || {} : {};

		const transportInfo = {
			accountName: (_.get(updateInfo, 'transport.transport.accountInfo.fullname', '') || '').split(' ').pop(),
			cardHolder: (bankInfo.cardHolder || '').split(' ').pop(),
			bankAccountName: (bankInfo.bankAccountName || '').split(' ').pop()
		};
		let socialLink = {};
		if (serviceType === HistoryConstant.SERVICE_TYPE.SOCIAL_PAYMENT) {
			socialLink = await this.broker.call('v1.ewalletSocialLinkModel.findOne', [{
				transaction: historyInfo.transaction,
				accountId: historyInfo.accountId
			}]);
		}

		if (!_.isNil(serviceName)) {
			if (_.isObject(updateInfo.service)) {
				_.set(updateInfo, 'service.name', serviceName);
			} else {
				updateInfo['service.name'] = serviceName;
			}
		}
	}

	return updateInfo;
};
