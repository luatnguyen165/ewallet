const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;

const ErrorCodeConstant = require('../constants/errorCode.constant');
const GeneralConstant = require('../constants/general.constant');
const FrontendConstant = require('../constants/frontent.constant');
const BillSupplier = require('../constants/billSupplier.constant');
const HistoryConstant = require('../constants/history.constant');

module.exports = async function (history) {
	const response = {
		code: ErrorCodeConstant.GET_SERVICE_NAME.FAILED,
		message: this.__(ErrorCodeConstant.GET_SERVICE_NAME.FAILED.toString()),
		data: {}
	};
	try {
		const serviceCode = _.get(history, 'service.code', null) || _.get(history, 'service.type', null);
		console.log('serviceCode', serviceCode);
		if (!serviceCode) return response;
		let serviceName = _.get(history, 'service.name', null);

		if (!serviceName) {
			switch (serviceCode) {
				case FrontendConstant.SERVICES.PAYMENT_CODE:
					serviceName = 'Thanh toán thành công';
					break;
				case FrontendConstant.SERVICES.FAST_LOAN:
					serviceName = 'Vay nhanh';
					break;
				case FrontendConstant.SERVICES.ADVANCE_SALARY_SETTLEMENT:
					serviceName = 'Hoàn ứng thành công';
					break;
				case FrontendConstant.SERVICES.SALARY:
					serviceName = `Nhận tiền từ ${_.get(history, 'transport.description')}`;
					break;
				case FrontendConstant.SERVICES.ADVANCE_SALARY:
					serviceName = 'Nhận tiền từ CTY CỔ PHẦN CÔNG NGHỆ PAYME';
					break;
				case FrontendConstant.SERVICES.ADVANCE_MONEY:
					serviceName = 'Ứng tiền';
					break;
				case FrontendConstant.SERVICES.CREDIT_STATEMENT:
					serviceName = 'Thanh toán tín dụng';
					break;
				case FrontendConstant.SERVICES.CREDIT_SETTLEMENT:
					serviceName = 'Thanh toán tín dụng';
					break;
				case FrontendConstant.SERVICES.DEPOSIT:
					serviceName = 'Nạp tiền';
					break;
				case FrontendConstant.SERVICES.TRANSFER_MONEY:
					serviceName = 'Chuyển tiền';
					if (history.changed === '-') serviceName = `Chuyển tiền đến ${_.get(history,'transport',null)}`;
					if (history.changed === '+') serviceName = 'Nhận tiền';
					break;
				case FrontendConstant.SERVICES.WITHDRAW:
					serviceName = 'Rút tiền';
					break;
				case FrontendConstant.SERVICES.OPEN_EWALLET_PAYMENT:
					serviceName = 'Thanh toán OpenEWallet';
					break;
				case FrontendConstant.SERVICES.BILL: {
					const typeService = _.get(history, 'service.data.type', '');
					const supplierFullInfo = BillSupplier[typeService] || {};
					serviceName = `Thanh toán hóa đơn ${(_.get(supplierFullInfo, 'subTitle', '').toLowerCase())}`;
				}
					break;
				case FrontendConstant.SERVICES.MOBILE_CARD: {
					const typeService = _.get(history, 'service.data.type', '');
					let cardType = HistoryConstant.ITEM_SERVICE.CARD_GAME;
					if (_.includes([
						FrontendConstant.CARD_SUPPLIER.VIETTEL,
						FrontendConstant.CARD_SUPPLIER.MOBI,
						FrontendConstant.CARD_SUPPLIER.VINA,
						FrontendConstant.CARD_SUPPLIER.BEELINE,
						FrontendConstant.CARD_SUPPLIER.VN_MOBI
					], typeService)
					) {
						cardType = HistoryConstant.ITEM_SERVICE.CARD;
					}
					serviceName = `Thanh toán ${cardType}`;
				}
					break;
				case FrontendConstant.SERVICES.MOBILE_TOPUP: {
					serviceName = 'Thanh toán Nạp tiền điện thoại';
					break;
				}
				case FrontendConstant.SERVICES.REFUND_MONEY:
					serviceName = 'Hoàn tiền giao dịch';
					break;
				case FrontendConstant.SERVICES.ISEC:
					serviceName = 'Phát hành iSec';
					if (history.changed === '+') {
						serviceName = 'Nhận tiền iSec';
					}
					break;
				// case FrontendConstant.SERVICES.ISEC_REDEEM:
				//   break;
				case FrontendConstant.SERVICES.ISEC_BULK:
					serviceName = 'Thanh toán Phát hành iSec số lượng lớn';
					break;
				case FrontendConstant.SERVICES.SOCIAL_PAYMENT:
					serviceName = 'Nhận tiền qua PayME Link';
					if (history.changed === '-') { serviceName = 'Chuyển tiền qua PayME Link'; }
					break;
				default:
					serviceName = 'Hệ thống';
			}
		}

		response.data.serviceName = serviceName;
		response.message = this.__(ErrorCodeConstant.GET_SERVICE_NAME.SUCCEEDED.toString());
		response.code = ErrorCodeConstant.GET_SERVICE_NAME.SUCCEEDED;
		return response;
	} catch (err) {
		this.logger.info(`[HistoryService] Get Service Name Method Error: ${err}`);
		return response;
	}
};
