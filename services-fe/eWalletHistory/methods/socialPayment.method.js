const _ = require('lodash');

const Moment = require('moment');
const Numeral = require('numeral');
const CryptoJS = require('crypto-js');
const GeneralConstant = require('../constants/general.constant');
const ErrorCodeConstant = require('../constants/errorCode.constant');

const Private = {

	blockTransaction(history) {
		return [
			{
				leftTitle: 'Mã giao dịch',
				rightTitle: _.get(history, 'service.transaction', '')
			},
			{
				leftTitle: 'Thời gian giao dịch',
				rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
			},
			{ leftTitle: 'Phí giao dịch', rightTitle: history.fee ? `${Numeral(history.fee).format('0,0')} đ` : 'Miễn phí' },
			{ leftTitle: 'Tổng thanh toán', rightTitle: `${Numeral(history.total).format('0,0')} đ`, rightColor: '#ec2a2a' }
		];
	},

	transferReceive(history, socialLink) {
		const result = {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.RECEIVE_MONEY,
					title: `${GeneralConstant.SERVICE_NAME.RECEIVE_MONEY} thành công`,
					description: `${Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: [
				{
					leftTitle: 'Mã giao dịch',
					rightTitle: _.get(history, 'service.transaction', '')
				},
				{
					leftTitle: 'Thời gian giao dịch',
					rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
				},
				{
					leftTitle: 'Phương thức',
					rightTitle: GeneralConstant.ITEM_METHOD.PAYME_LINK
				}
			],
			info: [{
				leftTitle: 'Liên kết',
				rightTitle: 'Hiển thị',
				rightFirstIcon: 'iconEyeView',
				rightColor: '#00be00',
				rightAction: `${JSON.stringify({ type: 'openPopup', data: { popupId: 132, params: { changed: history.changed, SocialPaymentObjectDetails: socialLink } } })}`
			}],
			action: [],
			webview: []
		};
		const note = _.get(socialLink, 'note', '') || '';
		if (note !== '') {
			result.info.push({ leftTitle: 'Ghi chú', rightTitle: _.get(socialLink, 'note', '') || '' });
		}

		const reply = _.get(socialLink, 'reply', '') || '';
		if (reply !== '') {
			result.info.push({ leftTitle: 'Ghi chú thanh toán', rightTitle: _.get(socialLink, 'reply', '') });
		}
		const userPayment = _.get(socialLink, 'transferInfo.senderInfo.fullname', '') || '';
		if (userPayment !== '') {
			result.info.push({ leftTitle: 'Người thanh toán', rightTitle: _.get(socialLink, 'transferInfo.senderInfo.fullname', '') });
		}
		return result;
	},

	async transferLink(history, socialLink) {
		const bytes = CryptoJS.AES.decrypt(_.toString(_.get(socialLink, 'password', '')), GeneralConstant.SOCIAL_LINK_PASSWORD_SALT);
		const password = bytes.toString(CryptoJS.enc.Utf8) || '';
		const result = {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.CALL_PAYMENT,
					title: `${GeneralConstant.SERVICE_NAME.LINK_TRANSFER_MONEY} thành công`,
					description: `${Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: Private.blockTransaction(history),
			info: [
				{
					leftTitle: 'Chuyển đến',
					rightTitle: 'Link (không cần tài khoản)'
				},
				{
					leftTitle: 'Liên kết nhận tiền',
					rightTitle: 'Hiển thị',
					rightFirstIcon: 'iconEyeView',
					rightColor: '#00be00',
					rightAction: `${JSON.stringify({ type: 'openPopup', data: { popupId: 132, params: { changed: history.changed, SocialPaymentObjectDetails: socialLink } } })}`
				}],
			action: [{
				rightTitle: 'Chia sẻ link nhận tiền',
				rightFirstIcon: 'icon_share_link',
				rightColor: '#00be00',
				rightAction: `${JSON.stringify({ type: 'share', data: { linkInfo: socialLink } })}`
			}],
			webview: []
		};
		const note = _.get(socialLink, 'note', '') || '';
		if (note !== '') {
			result.info.push({ leftTitle: 'Nội dung', rightTitle: _.get(socialLink, 'note', '') || '' });
		}
		if (password) {
			result.info.push({
				leftTitle: 'Mã bảo mật',
				rightTitle: password,
				rightFirstIcon: 'icon_copy_green',
				rightAction: `${JSON.stringify({ type: 'copy', data: { text: `${password}` } })}`
			});
		}

		return result;
	},

	async itemInfo(history, socialLink) {
		const result = [
			{ leftTitle: 'Chuyển đến', rightTitle: GeneralConstant.ITEM_METHOD.PAYME_LINK },
			{
				leftTitle: 'Liên kết',
				rightTitle: 'Hiển thị',
				rightFirstIcon: 'iconEyeView',
				rightColor: '#00be00',
				rightAction: `${JSON.stringify({ type: 'openPopup', data: { popupId: 132, params: { changed: history.changed, SocialPaymentObjectDetails: socialLink } } })}`
			}
		];
		const note = _.get(socialLink, 'note', '') || '';
		if (note !== '') {
			result.push({ leftTitle: 'Ghi chú', rightTitle: _.get(socialLink, 'note', '') || '' });
		}
		const reply = _.get(socialLink, 'reply', '') || '';
		if (reply !== '') {
			result.push({ leftTitle: 'Ghi chú thanh toán', rightTitle: _.get(socialLink, 'reply', '') });
		}
		return result;
	}
};

module.exports = async function (history) {
	try {
		const socialLink = await this.broker.call('v1.eWalletSocialLinkModel.findOne', [{ transaction: _.get(history, 'service.transaction', '') }]);
		if (history.changed === '+') {
			return Private.transferReceive(history, socialLink);
		}
		// link chuyển tiền
		if (_.get(history, 'changed', '') === '-' && _.includes(history.tags, 'SEND_MONEY') && _.get(history, 'transport.accountId', 0) === 0) {
			// nạp tiền từ insight
			return await Private.transferLink(history, socialLink);
		}
		const item = {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.CALL_PAYMENT,
					title: `${GeneralConstant.SERVICE_NAME.TRANSFER_MONEY} ${_.get(GeneralConstant.STATE_PAYMENT, `${_.get(history, 'service.state', '')}`, '')}`,
					description: `${Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: Private.blockTransaction(history),
			info: Private.itemInfo(history, socialLink),
			action: [],
			webview: []
		};
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED.toString()),
			data: item
		};
	} catch (error) {
		this.logger.info(`[ewalletHistory] Get Detail History Method -> Social Payment Error: ${error}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED.toString()),
			data: null
		};
	}
};
