const _ = require('lodash');

const ErrorCodeConstant = require('../constants/errorCode.constant');
const GeneralConstant = require('../constants/general.constant');

const DETAIL_HISTORY_METHOD_NAME = {
	BILL: 'GetBillDetail',
	MOBILE_CARD: 'GetMobileCardDetail',
	MOBILE_TOPUP: 'GetMobileTopupDetail',
	ISEC: 'GetIsecDetail',
	DEPOSIT: 'GetDepositDetail',
	WITHDRAW: 'GetWithdrawDetail',
	REFUND_MONEY: 'GetRefundMoneyDetail',
	TRANSFER_MONEY: 'GetTransferMoneyDetail',
	SOCIAL_PAYMENT: 'GetSocialPaymentDetail',
	SALARY: 'GetSalaryDetail',
	OPEN_EWALLET_PAYMENT: 'GetOpenEwalletPaymentDetail',
	WALLET_QR: 'GetWalletQRDetail',
	WALLET_PAY: 'GetWalletPayDetail',
	PAY_QRCODE: 'GetPayQrCodeDetail',
	CREDIT_STATEMENT: 'GetPayCreditStatementDetail',
	ADVANCE_MONEY: 'GetAdvanceMoneyDetail'
};

module.exports = async function (transaction, accountId) {
	try {
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': transaction, accountId }]);
		this.logger.info(`[eWalletHistory] history: ${JSON.stringify(history)}`);

		if (_.get(history, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.HISTORY_NOT_FOUND,
				message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.HISTORY_NOT_FOUND.toString())
			};
		}

		let detailHistory;
		const serviceType = _.get(history, 'service.type', '');
		if (_.includes(GeneralConstant.SERVICE_TYPE, serviceType)) {
			try {
				const method = DETAIL_HISTORY_METHOD_NAME[serviceType];
				detailHistory = await this[method](history);
			} catch (error) {
				this.logger.info(`[eWalletHistory] Get Detail: ${error}`);
			}
		} else detailHistory = await this.GetDefaultDetail(history);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED.toString()),
			data: detailHistory
		};
	} catch (err) {
		this.logger.info(`[ewalletHistory] Get Detail History Method Error: ${err}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED.toString()),
			data: null
		};
	}
};
