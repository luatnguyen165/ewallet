const _ = require('lodash');

const Moment = require('moment');
const Numeral = require('numeral');
const GeneralConstant = require('../constants/general.constant');
const ErrorCodeConstant = require('../constants/errorCode.constant');

module.exports = async function (history) {
	try {
		const addMoney = await this.broker.call('v1.eWalletAddMoneyModel.findOne', [{ transaction: _.get(history, 'service.transaction', '') }]);
		if (_.isNil(history.changed) || _.isNil(addMoney)) {
			return await this.defaultDetail(history);
		}
		const item = {
			blockInfo: [
				{
					icon: GeneralConstant.DISPLAY_ICON.RECEIVE_MONEY,
					title: `${GeneralConstant.SERVICE_NAME.RECEIVE_MONEY} ${_.get(GeneralConstant.STATE_PAYMENT, `${_.get(history, 'service.state', '')}`, '')}`,
					description: `${Numeral(Math.ceil(_.toNumber(history.amount))).format('0,0')} đ`,
					iconResult: _.get(GeneralConstant.DISPLAY_ICON, `PAYMENT_${history.state}`, GeneralConstant.DISPLAY_ICON.PAYMENT_PENDING)
				}
			],
			transaction: [
				{
					leftTitle: 'Mã giao dịch',
					rightTitle: _.get(history, 'service.transaction', '')
				},
				{
					leftTitle: 'Thời gian giao dịch',
					rightTitle: Moment((_.get(history, 'publishedAt', history.updatedAt))).format('HH:mm DD/MM/YYYY')
				},
				{ leftTitle: 'Phương thức', rightTitle: GeneralConstant.ITEM_METHOD.PAYME }
			],
			info: [
				{
					leftTitle: 'Người gửi',
					rightTitle: _.get(addMoney, 'sender', '')
				},
				{
					leftTitle: 'Ghi chú',
					rightTitle: _.get(addMoney, 'description', '')
				}
			],
			action: [],
			webview: []
		};
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.SUCCEEDED.toString()),
			data: item
		};
	} catch (error) {
		this.logger.info(`[ewalletHistory] Get Detail History Method -> Salary Error: ${error}`);
		return {
			code: ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED,
			message: this.__(ErrorCodeConstant.HISTORY_QUERY.HISTORY_DETAIL.FAILED.toString()),
			data: null
		};
	}
};
