const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const Moment = require('moment');

const BankLinkConstant = require('../constants/bankLink.constant');
const ResponseCode = require('../constants/code.constant');
const GeneralConstant = require('../constants/general.constant');

const CodeConstant = require('../constants/code.constant');

const { LinkedModel, BankCodeModel } = require('../constants/general.constant').MODEL;

module.exports = {
	handlerError(ctx, err) {
		return {
			code: err.code,
			message: err.message
		};
	},
	handlerErrorLinked(ctx, err) {
		const Logger = ctx.broker.logger.info;

		Logger(`[LINKED_HOOKS] => [handlerErrorLinked]: ${err}`);
		const response = {
			code: ResponseCode.LINKED_FAILED,
			message: 'Liên kết thất bại. Vui lòng thử lại sau (-1)'
		};
		try {
			if (err.name === 'MoleculerError') {
				response.code = err.code;
				response.message = err.message;
				const ttl = _.get(err, 'data.ttl', false);
				if (ttl !== false) {
					response.remainingLockedTime = ttl;
				}
			}
		} catch (error) {
			Logger(`[LINKED_HOOKS] => [handlerErrorLinked] => [Exception]: ${err}`);
		}
		return response;
	}
};
