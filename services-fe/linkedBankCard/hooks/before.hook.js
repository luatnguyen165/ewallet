const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const Moment = require('moment');

const BankLinkConstant = require('../constants/bankLink.constant');
const ResponseCode = require('../constants/code.constant');
const GeneralConstant = require('../constants/general.constant');

const CodeConstant = require('../constants/code.constant');

const {
	LinkedModel, BankCodeModel, AccountModel, AppInfoModel, ConnectedUserModel
} = require('../constants/general.constant').MODEL;

module.exports = {
	async hardCode(ctx) {
		const Broker = ctx.broker;
		const Logger = ctx.broker.logger.info;
		try {
			// HardCode for test
			const payload = ctx.params.body;
			const authInfo = _.get(ctx, 'meta.auth.walletAuthInfo', {});

			if (_.isEmpty(authInfo) === true) return false;
			if (!!process.env.ENV_NAME === false || _.toUpper(process.env.ENV_NAME) !== 'SANDBOX') return false;

			const listActionValid = [
				'linked', 'verifyOTP', 'unlinked',
				'linked_BE', 'verifyOTP_BE', 'unlinked_BE'
			];

			const rawNameAction = _.get(ctx, 'action.rawName', '');
			if (_.includes(listActionValid, rawNameAction) === false) return false;

			let supplier = null;
			let isLinkedAccount = false;
			let cardForHardCode = _.get(payload, 'linkInfo.card.cardNumber', null);

			if (_.isEmpty(payload.linkInfo) === false) {
				const bankInfo = await Broker.call(BankCodeModel.findOne, [{ swiftCode: payload.swiftCode }]);
				if (_.has(bankInfo, 'id') === false) return false;
				isLinkedAccount = _.toUpper(_.keys(payload.linkInfo)) === BankLinkConstant.LINKED_METHOD.ACCOUNT;
				switch (_.get(bankInfo, 'link.gateway', null)) {
					case BankLinkConstant.LINKED_GATEWAY.OCB:
						supplier = BankLinkConstant.LINKED_SUPPLIER.OCBBANK;
						break;
					case BankLinkConstant.LINKED_GATEWAY.BIDV:
						supplier = BankLinkConstant.LINKED_SUPPLIER.BIDVBANK;
						break;
					case BankLinkConstant.LINKED_GATEWAY.PVCBANK:
						supplier = BankLinkConstant.LINKED_SUPPLIER.PVCOMBANK;
						break;
					case BankLinkConstant.LINKED_GATEWAY.VIETINBANK:
						supplier = BankLinkConstant.LINKED_SUPPLIER.VIETINBANK;
						break;
					default:
						supplier = BankLinkConstant.LINKED_SUPPLIER.NAPAS_GATEWAY;
						break;
				}
			} else {
				let findCondition = null;
				if (!!payload.linkedId === true) {
					findCondition = { id: payload.linkedId };
				} else {
					findCondition = {
						accountId: authInfo.accountId,
						'cardInfo.swiftCode': payload.swiftCode,
						'linkedInfo.transactionId': payload.transaction
					};
				}
				const linkedInfo = await Broker.call(LinkedModel.findOne, [findCondition]);
				if (_.get(linkedInfo, 'id', false) === false) {
					return false;
				}
				supplier = linkedInfo.supplier;
				cardForHardCode = linkedInfo.cardInfo.cardNumber;
				isLinkedAccount = linkedInfo.type === BankLinkConstant.LINKED_METHOD.ACCOUNT;
			}

			switch (supplier) {
				case BankLinkConstant.LINKED_SUPPLIER.OCBBANK: {
					if (isLinkedAccount === true) {
						_.set(authInfo, 'fullname', 'GB NAME 1-803585');
						_.set(authInfo, 'kyc.identifyNumber', 'LEGAL ID-803585');
						_.set(authInfo, 'phone', '0953208227');
					} else {
						_.set(authInfo, 'fullname', 'Thời Phúc Phần');
						_.set(authInfo, 'kyc.identifyNumber', '070187001822');
						_.set(authInfo, 'phone', '0963566079');
					}
					break;
				}
				case BankLinkConstant.LINKED_SUPPLIER.BIDVBANK: {
					_.set(authInfo, 'fullname', 'Phạm Tuấn');
					_.set(authInfo, 'kyc.identifyNumber', '301300493');
					_.set(authInfo, 'phone', '84906328126');
					break;
				}
				case BankLinkConstant.LINKED_SUPPLIER.PVCOMBANK: {
					_.set(authInfo, 'fullname', 'Trương Tuấn Cường');
					_.set(authInfo, 'kyc.identifyNumber', '875662323245');
					_.set(authInfo, 'phone', '84920000005');
					break;
				}
				case BankLinkConstant.LINKED_SUPPLIER.VIETINBANK: {
					if (cardForHardCode === '9704150100005662') {
						_.set(authInfo, 'fullname', 'Trần Công Khanh');
						_.set(authInfo, 'kyc.identifyNumber', '013369393');
						_.set(authInfo, 'phone', '0986697989');
					} else if (cardForHardCode === '9704150100005654') {
						_.set(authInfo, 'fullname', 'Trần Thị Hương');
						_.set(authInfo, 'kyc.identifyNumber', '012959232');
						_.set(authInfo, 'phone', '0979596803');
					}

					break;
				}
				case BankLinkConstant.LINKED_SUPPLIER.NAPAS_GATEWAY: {
					if (cardForHardCode === '9704000000000018')	_.set(authInfo, 'fullname', 'Nguyễn Văn A');
					else if (cardForHardCode === '9704150100005662')	_.set(authInfo, 'fullname', 'Trần Công Khanh');
					else if (cardForHardCode === '9704125600182054')	_.set(authInfo, 'fullname', 'Trần Tuấn Cường');
					break;
				}
				default:
					return false;
			}

			return true;
		} catch (error) {
			console.log('error :>> ', error);
			return false;
		}
	},
	async hardCodeVersion2(ctx) {
		const Broker = ctx.broker;
		const Logger = ctx.broker.logger.info;
		try {
			// HardCode for test
			const payload = ctx.params.body;
			const authInfo = _.get(ctx, 'meta.auth.walletAuthInfo', {});

			if (_.isEmpty(authInfo) === true) return false;
			if (!!process.env.ENV_NAME === false || _.toUpper(process.env.ENV_NAME) !== 'SANDBOX') return false;

			// const listActionValid = ['linkedSDK'];
			// const rawNameAction = _.get(ctx, 'action.rawName', '');
			// if (_.includes(listActionValid, rawNameAction) === false) return false;

			let number = _.get(payload, 'linkInfo.card.cardNumber', null) || _.get(payload, 'linkInfo.account.accountNumber', null);
			if (payload.linkedId) {
				const linkedInfo = await this.broker.call('v1.eWalletLinkedModel.findOne', [{ id: payload.linkedId }]);
				if (_.get(linkedInfo, 'id', false) === false) return false;
				number = _.get(linkedInfo, 'cardInfo.cardNumber', null) || _.get(linkedInfo, 'cardInfo.accountNumber', null);
			}

			const cardInfo = GeneralConstant.CARD[`${number}`];
			if (cardInfo) {
				_.set(authInfo, 'fullname', cardInfo.fullname);
				_.set(authInfo, 'kyc.identifyNumber', cardInfo.identifyNumber);
				_.set(authInfo, 'phone', cardInfo.phone);
			}
			Logger(`[HARD_CODE_CARD_VERSION_2] :>> ${JSON.stringify({ card: number, info: cardInfo, payload })}`);
			return true;
		} catch (error) {
			console.log('error :>> ', error);
			return false;
		}
	},
	async getUserInfo(ctx) {
		const Broker = ctx.broker;
		const Logger = ctx.broker.logger.info;

		const payload = _.get(ctx, 'params.body', {});
		const query = _.get(ctx, 'params.query', {});

		let authInfo = _.get(ctx, 'meta.auth.data', {});
		if (_.isEmpty(authInfo)) {
			authInfo = this.GetAuthInfo(ctx.meta);
		}
		Logger(`authInfo :>> ${JSON.stringify(authInfo)}`);
		const listActionValid = ['linked_BE', 'unlinked_BE', 'getListLinked_BE'];
		const rawNameAction = _.get(ctx, 'action.rawName', '');

		if (_.includes(listActionValid, rawNameAction) === false) {
			authInfo.sourceConnected = _.isEmpty(authInfo) === true ? 'WAP' : 'FE';
			_.set(ctx, 'meta.auth.walletAuthInfo', authInfo);
			return true;
		}

		let responseCode = null;
		let message = '';

		switch (rawNameAction) {
			case 'linked_BE':
				responseCode = CodeConstant.LINKED_FAILED;
				message = 'Liên kết thất bại. Vui lòng thử lại sau.';
				break;
			case 'verifyOTP_BE':
				responseCode = CodeConstant.VERIFY_LINKED_FAILED;
				message = 'Xác thực liên kết thất bại. Vui lòng thử lại sau.';
				break;
			case 'unlinked_BE':
				responseCode = CodeConstant.UNLINKED_FAILED;
				message = 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.';
				break;
			case 'getListLinked_BE':
				responseCode = CodeConstant.GET_LINKED_LIST_FAILED;
				message = 'Lấy danh sách thẻ liên kết thất bại. Vui lòng thử lại sau.';
				break;
			default:
				return false;
		}

		try {
			// HardCode for test
			let phoneUser = _.get(payload, 'phone', false) || _.get(query, 'phone', false);
			if (!!phoneUser === false) throw new MoleculerError('Không tìm thấy thông tin phone user. Vui lòng kiểm tra lại.', responseCode);

			const checkPhone = this.ValidatePhone(phoneUser);
			if (checkPhone.isValid === false) throw new MoleculerError('Số điện thoại không hợp lệ. Vui lòng kiểm tra lại.', responseCode);

			phoneUser = checkPhone.phone;
			let findCondition = {
				phone: phoneUser
			};
			const accountInfo = await Broker.call(AccountModel.findOne, [findCondition]);
			if (_.get(accountInfo, 'id', false) === false) {
				Logger(`[BEFORE_HOOK] => [getUserInfo] => [findCondition] => ${JSON.stringify(findCondition)}`);
				throw new MoleculerError('Không tìm thấy thông tin tài khoản Ví.', responseCode);
			}

			if (_.get(accountInfo, 'kyc.state', '') !== 'APPROVED') {
				Logger(`[BEFORE_HOOK] => [getUserInfo] => [findCondition] => ${JSON.stringify(findCondition)}`);
				throw new MoleculerError('Tài khoản Ví chưa được định danh. Vui lòng kiểm tra lại.', responseCode);
			}

			let appInfo = null;
			if (_.get(ctx, 'meta.source', 'API') !== 'INSIGHT') {
				findCondition = {
					merchantId: _.get(authInfo, 'merchantId', '')
				};

				// Lấy APP mới nhất
				appInfo = await Broker.call(AppInfoModel.findOne, [findCondition, '-_id', {
					sort: {
						createdAt: 1
					}
				}]);

				if (_.get(appInfo, 'id', false) === false) {
					Logger(`[BEFORE_HOOK] => [getUserInfo] => [findCondition] => ${JSON.stringify(findCondition)}`);
					throw new MoleculerError('Không tìm thấy thông tin APP', responseCode);
				}

				findCondition = {
					accountId: accountInfo.id,
					phone: phoneUser,
					appId: appInfo.id
				};
				const userConnected = await Broker.call(ConnectedUserModel.findOne, [findCondition]);
				if (_.get(userConnected, 'id', false) === false) {
					Logger(`[BEFORE_HOOK] => [getUserInfo] => [findCondition] => ${JSON.stringify(findCondition)}`);
					throw new MoleculerError('Số điện thoại chưa được liên kết.', responseCode);
				}
			}

			const walletAuthInfo = {
				sourceConnected: 'BE',
				merchantId: _.get(authInfo, 'merchantId', null),
				accountId: accountInfo.id,
				phone: accountInfo.phone,
				appId: _.get(appInfo, 'id', null),
				fullname: accountInfo.fullname,
				avatar: _.get(accountInfo, 'avatar', null),
				kyc: {
					kycId: _.get(accountInfo, 'kyc.kycId', null),
					state: _.get(accountInfo, 'kyc.state', null),
					identifyNumber: _.get(accountInfo, 'kyc.identifyNumber', null)
				},
				isVerifiedEmail: _.get(accountInfo, 'isVerifiedEmail', false),
				email: _.get(accountInfo, 'email', null),
				accountGroupId: _.get(accountInfo, 'accountGroupId', null),
				accountType: _.get(accountInfo, 'accountType', 'PERSONAL')
			};
			_.set(ctx, 'meta.auth.walletAuthInfo', walletAuthInfo);

			return true;
		} catch (error) {
			if (error.name === 'MoleculerError') throw error;
			Logger(`[BEFORE_HOOK] => [getUserInfo] => [Exception] => ${error}`);
			throw new MoleculerError(message, responseCode);
		}
	},
	async checkLinkedLocked(ctx) {
		const Broker = ctx.broker;
		const Logger = ctx.broker.logger.info;
		try {
			const RedisService = this.RedisCache();

			const payload = _.get(ctx, 'params.body', null);
			const authInfo = _.get(ctx, 'meta.auth.walletAuthInfo', { accountId: 0 });
			if (!!payload.swiftCode === false) return false;

			const bankInfo = await Broker.call(BankCodeModel.findOne, [{ swiftCode: payload.swiftCode }]);
			if (_.get(bankInfo, 'id', false) === false) return false;

			const linkedGateway = _.get(bankInfo, 'link.gateway', BankLinkConstant.LINKED_GATEWAY.NAPAS);
			if (linkedGateway === BankLinkConstant.LINKED_GATEWAY.NAPAS) return false;

			let supplier = null;
			switch (linkedGateway) {
				case BankLinkConstant.LINKED_GATEWAY.BIDV:
					supplier = BankLinkConstant.LINKED_SUPPLIER.BIDVBANK;
					break;
				case BankLinkConstant.LINKED_GATEWAY.OCB:
					supplier = BankLinkConstant.LINKED_SUPPLIER.OCBBANK;
					break;
				case BankLinkConstant.LINKED_GATEWAY.PVCBANK:
					supplier = BankLinkConstant.LINKED_SUPPLIER.PVCOMBANK;
					break;
				case BankLinkConstant.LINKED_GATEWAY.VIETINBANK:
					supplier = BankLinkConstant.LINKED_SUPPLIER.VIETINBANK;
					break;
				default:
					return false;
			}
			const keyRedis = `LOCK_LINKED_${supplier}_${authInfo.accountId}`;
			let countOTPFailed = await RedisService.get({ key: keyRedis }) || 0;

			if (!!countOTPFailed === false || countOTPFailed < 3) return false;
			countOTPFailed = _.toNumber(countOTPFailed);

			const ttl = await RedisService.getTTL({ key: keyRedis });
			const momentTime = Moment.duration(ttl, 'seconds');
			const hours = momentTime.hours();
			const minutes = momentTime.minutes();
			const seconds = momentTime.seconds();

			let parseTime = null;
			if (hours > 0) {
				parseTime = `${hours} giờ ${minutes} phút ${seconds} giây`;
			} else if (minutes > 0) {
				parseTime = `${minutes} phút ${seconds} giây`;
			} else parseTime = `${seconds} giây`;

			// Chặn không cho call api liên kết.
			const message = `Tài khoản của bạn bị tạm khóa liên kết thẻ ${bankInfo.shortName}. Vui lòng thử lại sau ${parseTime}.`;
			throw new MoleculerError(message, ResponseCode.LINKED_TEMP_LOCK, null, { ttl });
		} catch (error) {
			if (error.name === 'MoleculerError') throw error;
			Logger(`[BEFORE_HOOK] => [checkLinkedLocked] => [Exception]: ${error}`);
			return false;
		}
	}
};
