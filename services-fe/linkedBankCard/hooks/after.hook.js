const _ = require('lodash');
const Moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;

const BankLinkConstant = require('../constants/bankLink.constant');
const ResponseCode = require('../constants/code.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = {
	async handlerLockLinked(ctx, response) {
		const Broker = ctx.broker;
		const Logger = ctx.broker.logger.info;
		try {
			const { LinkedModel, HistoryModel } = GeneralConstant.MODEL;
			const RedisService = this.RedisCache();

			const lockInfo = _.get(ctx, 'meta.lockInfo', {});
			if (_.isEmpty(lockInfo) === true) return response;

			const { keyRedis, linkedId } = lockInfo;

			if (response.code !== ResponseCode.VERIFY_LINKED_NOT_MATCH) {
				if (response.code === ResponseCode.VERIFY_LINKED_SUCCESSED) {
					await RedisService.delete({ key: keyRedis });
				}
				return response;
			}

			let countOTPFailed = await RedisService.get({ key: keyRedis }) || 1;
			countOTPFailed = _.toNumber(countOTPFailed) || 1;

			if (countOTPFailed < 3) {
				await RedisService.set({ key: keyRedis, value: countOTPFailed + 1, ttl: 300 });
				return response;
			}

			const ttl = await RedisService.getTTL({ key: keyRedis });
			const momentTime = Moment.duration(ttl, 'seconds');
			const hours = momentTime.hours();
			const minutes = momentTime.minutes();
			const seconds = momentTime.seconds();

			let parseTime = null;
			if (hours > 0) {
				parseTime = `${hours} giờ ${minutes} phút ${seconds} giây`;
			} else if (minutes > 0) {
				parseTime = `${minutes} phút ${seconds} giây`;
			} else parseTime = `${seconds} giây`;

			response.code = ResponseCode.VERIFY_LINKED_TEMP_LOCKED;
			response.message = `Bạn đã nhập sai OTP ${countOTPFailed} lần liên tiếp. Vui liên kết lại sau ${parseTime}!`;
			response.remainingLockedTime = ttl || 0;

			const conditionUpdate = { id: linkedId };
			const conditionUpdateHistory = {
				'service.id': linkedId,
				'service.type': BankLinkConstant.HISTORY_SERVICE.LINKED
			};
			const fieldUpdate = {
				state: BankLinkConstant.LINKED_STATE.FAILED
			};
			const fieldUpdateHistory = {
				state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
				'service.state': BankLinkConstant.LINKED_PAYMENT_STATE.FAILED
			};
			await Broker.call(LinkedModel.updateOne, [conditionUpdate, fieldUpdate]);
			await Broker.call(HistoryModel.updateOne, [conditionUpdateHistory, fieldUpdateHistory]);
		} catch (error) {
			// if (error.name === 'MoleculerError') throw error;
			Logger(`[AFTER_HOOKS] => [checkLinkedLocked] => [Error]: ${error}`);
		}
		return response;
	}
};
