const _ = require('lodash');
const BankLinkConstant = require('./constants/bankLink.constant');
// Hooks
const BeforeHooks = require('./hooks/before.hook');
const AfterHooks = require('./hooks/after.hook');
const ErrorHooks = require('./hooks/error.hook');

// Library
const PVComBankLibrary = require('./libraries/pvcomBank.library');
const VietinBankLibrary = require('./libraries/vietinBank.library');
const OCBBankLibrary = require('./libraries/ocbBank.library');
const BIDVBankLibrary = require('./libraries/bidvBank.library');
const NapasLibrary = require('./libraries/napas.library');
const WalletLibrary = require('./libraries/wallet.library');
const PGLibrary = require('./libraries/gateway.library');
const SacombankLibrary = require('./libraries/sacombank.library');

module.exports = {
	name: 'ewalletBankLink',
	version: 1,
	// mixins: [moleculerI18n],
	// i18n: {
	// 	directory: path.join(__dirname, 'locales'),
	// 	locales: ['vi', 'en'],
	// 	defaultLocale: 'vi'
	// },
	hooks: {
		before: {
			'*': [
				BeforeHooks.getUserInfo, // ==> Trường kết nối server to server thì nhảy vào đây để kiếm auth user
				// BeforeHooks.hardCode, // ==> HardCode Info User test liên kêt => NHỚ REMOVE
				BeforeHooks.hardCodeVersion2 // ==> HardCode Info User test liên kêt => NHỚ REMOVE
			]
		},
		error: {
			'*': ErrorHooks.handlerError
		}
	},
	/**
	 * Settings
	 */
	settings: {},
	/**
	 * Dependencies
	 */
	dependencies: [],
	/**
	 * Actions
	 */
	actions: {
		linked: {
			rest: {
				method: 'POST',
				fullPath: '/fe/ewallet/banking/link',
				auth: {
					strategies: ['Ewallet'],
					mode: 'optional'
				}
			},
			params: {
				body: {
					$$type: 'object',
					token: 'string|optional',
					swiftCode: 'string|optional',
					amount: 'number|optional',
					env: {
						optional: true,
						type: 'enum',
						values: _.values(BankLinkConstant.ENV_NAME)
					},
					linkInfo: {
						type: 'object',
						strict: 'remove',
						props: {
							card: {
								$$type: 'object|optional',
								cardNumber: 'string|optional',
								cardHolder: 'string|optional',
								issuedAt: 'string|optional',
								expiredAt: 'string|optional'
							},
							account: {
								$$type: 'object|optional',
								accountNumber: 'string'
							},
							credit: {
								type: 'object',
								optional: true,
								strict: 'remove',
								props: {
									transaction: 'string|optional',
									referenceId: 'string|optional',
									cardNumber: 'string|optional',
									cardHolder: 'string|optional',
									expiredAt: 'string|optional',
									cvv: 'string|optional'
								}
							}
						},
						minProps: 1,
						maxProps: 1
					},
					redirectUrl: 'string|optional',
					clientId: 'string'
				}
			},
			hooks: {
				before: BeforeHooks.checkLinkedLocked,
				error: ErrorHooks.handlerErrorLinked
			},
			handler: require('./actions/linked.action')
		},
		linked_BE: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/banking/link',
				auth: false
				// auth: {
				// 	strategies: ['Default'],
				// 	mode: 'required'
				// }
			},
			params: {
				body: {
					$$type: 'object',
					swiftCode: 'string|optional',
					amount: 'number|optional',
					phone: 'string',
					ipnUrl: 'string|optional',
					env: {
						optional: true,
						type: 'enum',
						values: _.values(BankLinkConstant.ENV_NAME)
					},
					linkInfo: {
						type: 'object',
						strict: 'remove',
						props: {
							card: {
								$$type: 'object|optional',
								cardNumber: 'string|optional',
								cardHolder: 'string|optional',
								issuedAt: 'string|optional',
								expiredAt: 'string|optional'
							},
							account: {
								$$type: 'object|optional',
								accountNumber: 'string'
							},
							credit: {
								type: 'object',
								optional: true,
								strict: 'remove',
								props: {
									transaction: 'string|optional',
									referenceId: 'string|optional',
									cardNumber: 'string|optional',
									cardHolder: 'string|optional',
									expiredAt: 'string|optional',
									cvv: 'string|optional'
								}
							}
						},
						minProps: 1,
						maxProps: 1
					},
					redirectUrl: 'string|optional',
					clientId: 'string|optional'
				}
			},
			hooks: {
				before: BeforeHooks.checkLinkedLocked,
				error: ErrorHooks.handlerErrorLinked
			},
			handler: require('./actions/linked.action')
		},
		linkedSDK: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/sdk/link',
				auth: {
					strategies: ['Ewallet'],
					mode: 'optional'
				}
			},
			params: {
				body: {
					$$type: 'object',
					token: 'string|optional',
					swiftCode: 'string|optional',
					amount: 'number|optional',
					env: {
						optional: true,
						type: 'enum',
						values: _.values(BankLinkConstant.ENV_NAME)
					},
					linkInfo: {
						type: 'object',
						strict: 'remove',
						props: {
							card: {
								$$type: 'object|optional',
								cardNumber: 'string|optional',
								cardHolder: 'string|optional',
								issuedAt: 'string|optional',
								expiredAt: 'string|optional'
							},
							account: {
								$$type: 'object|optional',
								accountNumber: 'string'
							},
							credit: {
								type: 'object',
								optional: true,
								strict: 'remove',
								props: {
									transaction: 'string|optional',
									referenceId: 'string|optional',
									cardNumber: 'string|optional',
									cardHolder: 'string|optional',
									expiredAt: 'string|optional',
									cvv: 'string|optional'
								}
							}
						},
						minProps: 1,
						maxProps: 1
					},
					redirectUrl: 'string|optional',
					clientId: 'string'
				}
			},
			hooks: {
				before: BeforeHooks.checkLinkedLocked,
				error: ErrorHooks.handlerErrorLinked
			},
			timeout: 2 * 60 * 1000,
			handler: require('./actions/linkedSDK.action')
		},
		verifyOTP: {
			rest: {
				method: 'POST',
				fullPath: '/fe/ewallet/banking/link/verifyOTP',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					transaction: 'string',
					otp: 'string'
				}
			},
			hooks: {
				after: AfterHooks.handlerLockLinked
			},
			handler: require('./actions/verifyLinked.action')
		},
		verifyOTP_BE: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/banking/link/verifyOTP',
				auth: false
				// auth: {
				// 	strategies: ['Default'],
				// 	mode: 'required'
				// }
			},
			params: {
				body: {
					$$type: 'object',
					transaction: 'string',
					otp: 'string'
				}
			},
			hooks: {
				after: AfterHooks.handlerLockLinked
			},
			handler: require('./actions/verifyLinked.action')
		},
		verifyOtpSDK: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/sdk/link/verifyOTP',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					transaction: 'string',
					otp: 'string'
				}
			},
			hooks: {
				after: AfterHooks.handlerLockLinked
			},
			timeout: 2 * 60 * 1000,
			handler: require('./actions/verifyLinkedSDK.action')
		},
		unlinked: {
			rest: {
				method: 'POST',
				fullPath: '/fe/ewallet/banking/unlink',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					linkedId: 'number',
					version: 'string|optional',
					clientId: 'string'
				}
			},
			handler: require('./actions/unlinked.action')
		},
		unlinked_BE: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/banking/unlink',
				auth: false
				// auth: {
				// 	strategies: ['Default'],
				// 	mode: 'required'
				// }
			},
			params: {
				body: {
					$$type: 'object',
					phone: 'string',
					linkedId: 'number',
					version: 'string|optional'
				}
			},
			handler: require('./actions/unlinked.action')
		},
		unlinkedSDK: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/sdk/unlink',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					linkedId: 'number',
					password: 'string|optional',
					version: 'string|optional',
					clientId: 'string'
				}
			},
			timeout: 2 * 60 * 1000,
			handler: require('./actions/unlinkedSDK.action')
		},
		authCreditSDK: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/sdk/credit/authentication',
				auth: {
					strategies: ['Ewallet'],
					mode: 'optional'
				}
			},
			params: {
				body: {
					$$type: 'object',
					linkedId: 'string|optional',
					cardNumber: 'string|optional',
					expiredAt: 'string|optional',
					language: 'string|min:2|optional|default:vi',
					clientId: 'string|optional'
				}
			},
			handler: require('./actions/authenticationCreditSDK.action')
		},

		getListLinked: {
			rest: {
				method: 'GET',
				fullPath: '/fe/ewallet/banking/linked',
				auth: {
					strategies: ['Ewallet'],
					mode: 'optional'
				}
			},
			query: {
				linkedId: 'string|optional'
			},
			handler: require('./actions/getLinkedInfo.action')
		},
		getListLinked_BE: {
			rest: {
				method: 'GET',
				fullPath: '/be/ewallet/banking/linked',
				auth: false
				// auth: {
				// 	strategies: ['Default'],
				// 	mode: 'required'
				// }
			},
			query: {
				phone: 'string'
			},
			handler: require('./actions/getLinkedInfo.action')
		},
		getListLinkedSDK: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/sdk/linked',
				auth: {
					strategies: ['Ewallet'],
					mode: 'optional'
				}
			},
			params: {
				body: {
					$$type: 'object|optional',
					phone: 'string|optional',
					linkedId: 'string|optional'
				}
			},
			timeout: 2 * 60 * 1000,
			handler: require('./actions/getLinkedInfo.action')
		},
		ipnLinkedNapas: {
			rest: {
				method: 'POST',
				fullPath: '/ewallet/banking/napas/ipn',
				auth: false,
				security: false
			},
			params: {
				body: {
					$$type: 'object'
				}
			},
			handler: require('./actions/ipnLinkedNapas.action')
		},
		ipnLinkedBIDV: {
			rest: {
				method: 'POST',
				fullPath: '/ewallet/banking/bidv/ipn',
				auth: false,
				security: false
			},
			params: {
				body: {
					$$type: 'object'
				}
			},
			timeout: 2 * 60 * 1000,
			handler: require('./actions/ipnLinkedBIDV.action')
		},
		ipnLinkedCredit: {
			rest: {
				method: 'POST',
				fullPath: '/ewallet/banking/credit/ipn',
				auth: false
			},
			params: {
				body: {
					$$type: 'object'
				}
			},
			timeout: 2 * 60 * 1000,
			handler: require('./actions/ipnLinkedCredit.action')
		},
		processLinkCredit: {
			params: {
				paymentId: 'string',
				cardId: 'string'
			},
			handler: require('./actions/handlerLinkCredit.action')
		}

	},
	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		GetAuthInfo: require('./methods/getAuthInfo.method'),
		Helper: require('./methods/helper.method'),
		RedisCache: require('./methods/redisCache.method'),
		ValidatePhone: require('./methods/validatePhone.method'),
		PasswordHelper: require('./methods/hashPassword.method'),
		RedisCheckUnlink: require('./methods/redisLockFailedPassword.method')
	},

	created() {
		try {
			_.set(this, 'PVComBankLibs', PVComBankLibrary({ broker: this.broker, logger: this.logger.info, redis: this.RedisCache }));
			_.set(this, 'VietinBankLibs', VietinBankLibrary({ broker: this.broker, logger: this.logger.info, redis: this.RedisCache }));
			_.set(this, 'OCBBankLibs', OCBBankLibrary({ broker: this.broker, logger: this.logger.info, redis: this.RedisCache }));
			_.set(this, 'BIDVBankLibs', BIDVBankLibrary({ broker: this.broker, logger: this.logger.info, redis: this.RedisCache }));
			_.set(this, 'NapasLibs', NapasLibrary({ broker: this.broker, logger: this.logger.info, redis: this.RedisCache }));
			_.set(this, 'WalletLibs', WalletLibrary({ broker: this.broker, logger: this.logger.info, redis: this.RedisCache }));
			_.set(this, 'PGLibs', PGLibrary({ broker: this.broker, logger: this.logger.info, helper: this.Helper }));
			_.set(this, 'SacomBankLibs', SacombankLibrary({ broker: this.broker, logger: this.logger.info, helper: this.Helper }));
		} catch (error) {
			this.logger.info(`[Generate_Library_Error] => ${error}`);
		}
	}
};
