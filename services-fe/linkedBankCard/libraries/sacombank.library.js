const _ = require('lodash');
const BankLinkConstant = require('../constants/bankLink.constant');
const ResponseCode = require('../constants/code.constant');

function SacombankLibrary(config = {}) {
	const Broker = config.broker;
	const Logger = config.logger;
	return {
		async UnLinked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.UNLINKED_FAILED,
				state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
				message: 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.',
				transaction: null,
				supplierResponse: null
			};
			const body = {
				account_id: _.toString(params.accountId),
				card_id: params.cardId,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			try {
				Logger(`[SACOMBANK_LIBRARY] => [Unlinked] => [Params] : ${JSON.stringify(body)}`);
				const resultUnLink = await Broker.call('v1.sacomBank.unlink', body);
				Logger(`[SACOMBANK_LIBRARY] => [Unlinked] => [Response] : ${JSON.stringify(resultUnLink)}`);
				if (_.get(resultUnLink, 'code', 1) !== 166040) {
					response.message = _.get(resultUnLink, 'message', 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.');
					return response;
				}
				response.code = ResponseCode.UNLINKED_SUCCESSED;
				response.message = 'Huỷ liên kết thẻ thành công';
				response.state = BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[SACOMBANK_LIBRARY] => [Unlinked] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}

	};
}
module.exports = SacombankLibrary;
