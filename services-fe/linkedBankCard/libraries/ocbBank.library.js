/* eslint-disable no-unused-expressions */
const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');
const Axios = require('axios');
const Qs = require('qs');
const Jose = require('jose');
const Md5 = require('md5');

const BankLinkConstant = require('../constants/bankLink.constant');
const ResponseCode = require('../constants/code.constant');

function OCBBankLibrary(config = {}) {
	const Broker = config.broker;
	const Logger = config.logger;
	const RedisService = config.redis();

	const OCBConfig = {
		url: process.env.OCB_URL,
		clientId: process.env.OCB_CLIENT_ID,
		clientSecret: process.env.OCB_CLIENT_SECRECT,
		username: process.env.OCB_USER_NAME,
		password: process.env.OCB_PASSWORD,
		debitAccount: process.env.OCB_DEBIT_ACCOUNT,
		privateKey: process.env.OCB_PRIVATE_KEY,
		partnerCode: process.env.OCB_PARTNER_CODE,
		xClientCertificate: process.env.OCB_CLIENT_CERTIFICATE
	};

	const { JWK, JWS, errors } = Jose;

	async function PostForm(url, form) {
		try {
			const response = await Axios.post(url, Qs.stringify(form));
			return response;
		} catch (error) {
			return error;
		}
	}

	async function GetToken(scope = 'OCB') {
		const response = {
			isSuccess: false,
			message: 'Lấy thông tin kết nối thất bai.',
			accessToken: null
		};

		const urlRequest = `${OCBConfig.url}/ocb-oauth-provider/oauth2/token`;
		const objForm = {
			grant_type: 'password',
			username: OCBConfig.username,
			password: OCBConfig.password,
			client_id: OCBConfig.clientId,
			client_secret: OCBConfig.clientSecret,
			scope
		};
		let resultGetToken = null;
		try {
			resultGetToken = await PostForm(urlRequest, objForm);
			const accessToken = _.get(resultGetToken, 'data.access_token', null);
			if (resultGetToken.status !== 200 || !!accessToken === false) {
				Logger(`[OCBBANK_LIBRARY] => [GetToken] => [Params] : ${JSON.stringify(objForm)}`);
				Logger(`[OCBBANK_LIBRARY] => [GetToken] => [Response] : ${resultGetToken}`);
				return response;
			}

			response.isSuccess = true;
			response.accessToken = accessToken;
			response.message = 'Lấy thông tin kết nối thành công.';
		} catch (err) {
			Logger(`[OCBBANK_LIBRARY] => [GetToken] => [Error] : ${err}`);
			Logger(`[OCBBANK_LIBRARY] => [GetToken] => [Params] : ${JSON.stringify(objForm)}`);
			Logger(`[OCBBANK_LIBRARY] => [GetToken] => [Response] : ${resultGetToken}`);
		}
		return response;
	}

	async function Sign(obj) {
		try {
			const key = Jose.JWK.asKey(OCBConfig.privateKey);
			const payload = JSON.stringify(obj);
			const jwsData = JWS.sign(payload, key, { alg: 'RS256' });
			const xSignature = jwsData.split('.').pop();
			return xSignature;
		} catch (error) {
			Logger(`[OCBBANK_LIBRARY] => [Sign] => [Params] : ${JSON.stringify(obj)}`);
			Logger(`[OCBBANK_LIBRARY] => [Sign] => [Error] : ${error}`);
			return false;
		}
	}

	async function PostRequest(url, payload, headers = {}) {
		const response = {
			isSuccess: false,
			message: 'Kết nối với nhà cung cấp thất bại.',
			data: null
		};
		let resultRequest = null;
		let urlRequest = null;
		let paramsRequest = null;
		try {
			const resultGetToken = await GetToken();
			if (resultGetToken.isSuccess === false) {
				response.message = resultGetToken.message;
				response.data = resultGetToken;
				return response;
			}
			const { accessToken } = resultGetToken;

			urlRequest = url;
			paramsRequest = {
				...payload,
				trace: {
					clientTransId: Md5(Moment().format('YYYYMMDDHHmmssSSS') + _.random(111111, 999999)),
					clientTimestamp: Moment().format('YYYYMMDDHHmmssSSS')
				}
			};

			const XSignature = await Sign(paramsRequest);
			if (!!XSignature === false) {
				response.message = 'Khởi tạo thông tin kết nối thất bại.';
				return response;
			}

			resultRequest = await Axios.post(urlRequest, paramsRequest, {
				headers: _.merge({
					Authorization: `Bearer ${accessToken}`,
					'X-Client-Certificate': OCBConfig.xClientCertificate,
					'X-Signature': XSignature,
					'X-IBM-Client-Id': OCBConfig.clientId,
					'X-IBM-Client-Secret': OCBConfig.clientSecret
				}, headers)
			});

			Logger(`[OCBBANK_LIBRARY] => [PostRequest] => [Params] : ${JSON.stringify(paramsRequest)}`);
			Logger(`[OCBBANK_LIBRARY] => [PostRequest] => [Response] : ${resultRequest}`);

			response.isSuccess = true;
			response.message = 'Thành công.';
			response.data = {
				status: _.get(resultRequest, 'status', null),
				message: _.get(resultRequest, 'statusText', null),
				transaction: _.get(resultRequest, 'data.trace.bankRefNo', null),
				extraData: {
					dataRequest: _.get(resultRequest, 'config.data', null),
					ewallet: _.get(resultRequest, 'data.data.ewallet', null),
					trace: _.get(resultRequest, 'data.trace', null)
				}
			};
			return response;
		} catch (error) {
			if (_.get(error, 'response', null) !== null) {
				response.data = {
					status: _.get(error, 'response.status', null),
					message: _.get(error, 'response.statusText', null),
					extraData: {
						dataRequest: _.get(error, 'response.config.data', null),
						messageDetail: _.get(error, 'response.data.moreInformation', null),
						error: _.get(error, 'response.data.error', null)
					}
				};
			}
			if (error.code === 'ECONNABORTED') {
				response.data = error;
			}
			Logger(`[OCBBANK_LIBRARY] => [PostRequest] => [Error] : ${error.message}`);
			Logger(`[OCBBANK_LIBRARY] => [PostRequest] => [Params] : ${JSON.stringify(paramsRequest)}`);
			return response;
		}
	}

	return {
		async Linked(params = {}) {
			const response = {
				code: ResponseCode.LINKED_FAILED,
				message: 'Liên kết OCB thất bại. Vui lòng thử lại sau.',
				transaction: null,
				state: BankLinkConstant.LINKED_STATE.FAILED,
				html: null,
				supplierResponse: null
			};

			const urlRequest = `${OCBConfig.url}/v1/ewallet/create-ewallet-link`;
			const paramsRequest = {
				data: {
					ewallet: {
						accountNumber: params.accountNumber || '',
						cardNumber: params.cardNumber || '',
						accountType: params.type,
						fullName: params.fullname,
						mobilePhone: params.phone,
						identificationNumber: params.identifyNumber,
						identificationType: 'CMND',
						partnerCode: OCBConfig.partnerCode
					}
				}
			};

			try {
				const resultRequest = await PostRequest(urlRequest, paramsRequest);
				response.supplierResponse = JSON.stringify(resultRequest);

				const responseStatus = _.get(resultRequest, 'data.status', -1);
				if (resultRequest.isSuccess === false || responseStatus !== 200) {
					Logger(`[OCBBANK_LIBRARY] => [Linked] => [Params] : ${JSON.stringify(paramsRequest)}`);
					Logger(`[OCBBANK_LIBRARY] => [Linked] => [Response] : ${JSON.stringify(resultRequest)}`);

					const errorCode = _.toNumber(_.get(resultRequest, 'data.extraData.error.code', 0));
					const message = BankLinkConstant.LINKED_OCB_MESSAGE[errorCode] || response.message;
					response.message = `[OCB] ${message}`;
					return response;
				}
				response.transaction = _.get(resultRequest, 'data.transaction', null);
				response.code = ResponseCode.LINKED_REQUIRE_OTP;
				response.message = 'Liên kết cần xác thực bằng mã OTP ngân hàng.';
				response.state = BankLinkConstant.LINKED_STATE.REQUIRED_OTP;
				return response;
			} catch (error) {
				Logger(`[OCBBANK_LIBRARY] => [Linked: ${urlRequest}] => [Error] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
			}
			return response;
		},
		async UnLinked(params = {}) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.UNLINKED_FAILED,
				state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
				message: 'Hủy liên kết thẻ OCB thất bại. Vui lòng thử lại sau.',
				supplierResponse: null
			};

			const urlRequest = `${OCBConfig.url}/v1/ewallet/terminate-ewallet-link`;
			const paramsRequest = {
				data: {
					ewallet: {
						mobilePhone: params.phone,
						ewalletLinkId: params.cardId,
						partnerCode: OCBConfig.partnerCode
					}
				}
			};

			try {
				const resultRequest = await PostRequest(urlRequest, paramsRequest);
				response.supplierResponse = JSON.stringify(resultRequest);

				const responseStatus = _.get(resultRequest, 'data.status', -1);

				if (resultRequest.isSuccess === false || responseStatus !== 200) {
					Logger(`[OCBBANK_LIBRARY] => [Unlinked] => [Params] : ${JSON.stringify(paramsRequest)}`);
					Logger(`[OCBBANK_LIBRARY] => [Unlinked] => [Response] : ${JSON.stringify(resultRequest)}`);
					return response;
				}
				response.code = ResponseCode.UNLINKED_SUCCESSED;
				response.transaction = _.get(resultRequest, 'data.data.trace.bankRefNo', null);
				response.message = 'Huỷ liên kết thẻ thành công';
				response.state = BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[OCBBANK_LIBRARY] => [Unlinked: ${urlRequest}] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async VerifyLinked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.VERIFY_LINKED_FAILED,
				state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
				message: 'Xác thực OTP thất bại. Vui lòng thử lại sau.',
				cardId: null,
				supplierResponse: null
			};
			const urlRequest = `${OCBConfig.url}/v1/ewallet/authorization-ewallet-link`;
			const paramsRequest = {
				data: {
					ewallet: {
						bankRefNo: params.transaction,
						OTPCode: params.otp,
						partnerCode: OCBConfig.partnerCode
					}
				}
			};

			try {
				const resultRequest = await PostRequest(urlRequest, paramsRequest);
				response.supplierResponse = JSON.stringify(resultRequest);

				const responseStatus = _.get(resultRequest, 'data.status', -1);
				if (resultRequest.isSuccess === false || responseStatus !== 200) {
					Logger(`[OCBBANK_LIBRARY] => [VerifyLinked] => [Params] : ${JSON.stringify(paramsRequest)}`);
					Logger(`[OCBBANK_LIBRARY] => [VerifyLinked] => [Response] : ${JSON.stringify(resultRequest)}`);

					const errorCode = _.get(resultRequest, 'data.extraData.error.code', 0);

					response.message = BankLinkConstant.LINKED_OCB_MESSAGE[_.toNumber(errorCode)];
					if (_.includes(['41723', '41724', '41725', '41726'], errorCode) === true) {
						// Sai OTP
						response.code = ResponseCode.VERIFY_LINKED_NOT_MATCH;
						response.message = 'Mã OTP không đúng. Vui lòng nhập lại.';
						response.state = BankLinkConstant.LINKED_PAYMENT_STATE.INVALID_OTP;
						return response;
					}
					return response;
				}
				response.code = ResponseCode.VERIFY_LINKED_SUCCESSED;
				response.state = BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED;
				response.message = 'Xác thực OTP thành công.';
				response.cardId = _.get(resultRequest, 'data.extraData.ewallet.ewalletLinkId', null);
				response.identificationNumber = _.get(resultRequest, 'data.extraData.ewallet.identificationNumber', null);
				return response;
			} catch (error) {
				Logger(`[OCBBANK_LIBRARY] => [VerifyLinked: ${urlRequest}] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}
	};
}
module.exports = OCBBankLibrary;
