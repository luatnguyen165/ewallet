const Request = require('request');
const _ = require('lodash');
const ShortId = require('shortid');
const Moment = require('moment');
const BankLinkConstant = require('../constants/bankLink.constant');
const ResponseCode = require('../constants/code.constant');
const GeneralConstant = require('../constants/general.constant');

function PGLibrary(config = {}) {
	const inpUrl = process.env.GAPI_URL;

	const { hiddenNumber } = config.helper();
	const Broker = config.broker;
	const Logger = config.logger;

	const {
		createLinkedOrder, refundCredit, unlinkCredit, linkedCredit, processLinkCredit
	} = GeneralConstant.SERVICE.PAYMENT_GATEWAY;
	const { PaymentPGModel } = GeneralConstant.MODEL;
	function parseHtml(link) {
		return `<html xmlns="http://www.w3.org/1999/xhtml">
			<head>  
			<title>Liên kết thẻ Credit</title>  
			<meta http-equiv="refresh" content="0;URL=${link}" />
			</head>
			<body></body>
			</html>`;
	}

	// const RedisService = config.redis();
	return {
		async CreateLinkedOrder(params) {
			const response = {
				code: ResponseCode.LINKED_FAILED,
				state: BankLinkConstant.LINKED_STATE.FAILED,
				message: 'Liên kết thẻ Credit thất bại. Vui lòng thử lại sau.',
				transaction: null,
				html: null,
				supplierResponse: null
			};
			const paramsRequest = {
				accountId: _.toNumber(params.accountId),
				referId: params.requestId,
				referType: 'SDK',
				partnerTransaction: params.transaction,
				amount: params.amount,
				description: 'Liên kết thẻ Credit',
				payMethod: 'CREDIT',
				ipnUrl: params.ipnUrl,
				redirectUrl: params.redirectUrl || process.env.HOME_URL
			};
			let resultRequest = null;
			try {
				resultRequest = await Broker.call(createLinkedOrder, paramsRequest);
				response.supplierResponse = JSON.stringify(resultRequest);

				const bodyResponse = resultRequest;
				if (bodyResponse.code === 105000) {
					let base64Trans = JSON.stringify({
						merchantId: params.merchantId,
						accountId: _.get(params, 'authInfo.accountId', null),
						linkedId: params.linkedId,
						transaction: params.transaction
					});
					base64Trans = Buffer.from(base64Trans).toString('base64');
					console.log('token :>> ', base64Trans);
					response.code = ResponseCode.LINKED_REQUIRED_VERIFY;
					response.transaction = _.get(bodyResponse, 'data.orderId', null);
					response.message = 'Liên kết cần xác thực bằng form ngân hàng.';
					response.state = BankLinkConstant.LINKED_STATE.REQUIRED_VERIFY;
					// response.html = parseHtml(`${_.get(bodyResponse, 'data.url', null)}`);
					response.html = parseHtml(`${process.env.WAP_URL}/bank-link/${base64Trans}`);
					return response;
				}
				Logger(`[PG_LIBRARY] => [CreateLinkedOrder] => [Params && Response ] : ${JSON.stringify({ paramsRequest, resultRequest })}`);
				response.message = _.get(bodyResponse, 'data.message', 'Liên kết thất bại. Vui lòng thử lại sau (E014).');
				return response;
			} catch (error) {
				Logger(`[PG_LIBRARY] => [CreateLinkedOrder] => [Exception] : ${error}`);
				Logger(`[PG_LIBRARY] => [CreateLinkedOrder] => [Params && Response ] : ${JSON.stringify({ paramsRequest, resultRequest })}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async Linked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.LINKED_FAILED,
				state: BankLinkConstant.LINKED_STATE.FAILED,
				message: 'Liên kết Credit thất bại. Vui lòng thử lại sau.',
				transaction: null,
				html: null,
				supplierResponse: null
			};
			const paramsRequest = {
				body: {
					transaction: params.transaction,
					payCode: 'CREDIT',
					payData: {
						credit: {
							redirectUrl: params.redirectUrl,
							saveCard: true,
							cardNumber: params.cardNumber,
							cardHolder: params.cardHolder,
							expiredAt: params.expiredAt,
							cvv: params.cvv
						}
					},
					language: 'vi',
					note: 'Liên kết thẻ Credit'
				}
			};

			if (_.isEmpty(params.referenceId) === false) _.set(paramsRequest, 'body.payData.credit.referenceId', params.referenceId);
			if (_.isEmpty(params.billInfo) === false) _.set(paramsRequest, 'body.payData.credit.billInfo', params.billInfo);
			let resultRequest = null;
			try {
				resultRequest = await Broker.call(linkedCredit, paramsRequest, {
					meta: {
						auth: {
							security: {}
						}
					}
				});
				response.supplierResponse = { resultRequest: JSON.stringify(resultRequest) };

				const bodyResponse = resultRequest;
				if (bodyResponse.code === 129008) {
					const cardId = _.get(bodyResponse, 'data.cardId', false);
					if (!!cardId === true) {
						const resultHandlerLinked = await Broker.call(processLinkCredit, {
							cardId,
							paymentId: _.get(bodyResponse, 'data.paymentId', null)
						});
						if (resultHandlerLinked.code === ResponseCode.IPN_SUCCEEDED) {
							response.supplierResponse = JSON.stringify({ paramsRequest, resultRequest, resultHandlerLinked });
							response.code = ResponseCode.LINKED_SUCCESSED;
							response.transaction = _.get(bodyResponse, 'data.transaction', null);
							response.message = 'Liên kết thẻ Credit thành công.';
							response.state = BankLinkConstant.LINKED_STATE.LINKED;
							response.html = '';
						} else {
							response.code = ResponseCode.LINKED_FAILED;
							response.transaction = _.get(bodyResponse, 'data.transaction', null);
							response.message = 'Xử lý liên kết thất bại. Vui lòng liên hệ CSKH để được hỗ trợ.';
							response.state = BankLinkConstant.LINKED_STATE.FAILED;
							response.supplierResponse = JSON.stringify({ paramsRequest, resultRequest, resultHandlerLinked });
							response.html = '';
						}
					} else {
						response.supplierResponse = JSON.stringify({ ...resultRequest, data: { ...resultRequest.data, form: 'Đã bị ẩn vì quá dài' } });
						response.code = ResponseCode.LINKED_REQUIRED_VERIFY;
						response.transaction = _.get(bodyResponse, 'data.transaction', null);
						response.message = 'Liên kết cần xác thực bằng form ngân hàng.';
						response.state = BankLinkConstant.LINKED_STATE.REQUIRED_VERIFY;
						response.html = _.get(bodyResponse, 'data.form', null);
						response.paymentId = _.get(bodyResponse, 'data.paymentId', null);
					}
					return response;
				}

				_.set(paramsRequest, 'body.payData.credit.cardNumber', hiddenNumber(params.cardNumber, 'CARD'));
				_.set(paramsRequest, 'body.payData.credit.cvv', 'XXX');

				Logger(`[PG_LIBRARY] => [Linked] => [Params && Response] : ${JSON.stringify({ paramsRequest, resultRequest })}`);
				response.supplierResponse = JSON.stringify(response.supplierResponse);
				response.message = _.get(bodyResponse, 'message', 'Liên kết thất bại. Vui lòng thử lại sau (E016).');
				return response;
			} catch (error) {
				Logger(`[PG_LIBRARY] => [Linked] => [Exception] : ${error}`);
				Logger(`[PG_LIBRARY] => [Linked] => [Params && Response] : ${JSON.stringify({ paramsRequest, resultRequest })}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message, data: _.get(error, 'data', null) });
				return response;
			}
		},
		async LinkedDirect(params = {}) {
			// Response:  code - message - state - transaction - html
			let resultRequest = null;
			let paramsRequest = null;
			const response = {
				code: ResponseCode.LINKED_FAILED,
				state: BankLinkConstant.LINKED_STATE.FAILED,
				message: 'Liên kết Credit thất bại. Vui lòng thử lại sau.',
				transaction: null,
				html: null,
				supplierResponse: null
			};
			try {
				paramsRequest = {
					accountId: _.toNumber(params.accountId),
					referId: params.requestId,
					referType: 'SDK',
					partnerTransaction: params.transaction,
					amount: params.amount,
					description: 'Liên kết thẻ Credit',
					payMethod: 'CREDIT',
					ipnUrl: params.ipnUrl,
					redirectUrl: params.redirectUrl || process.env.HOME_URL
				};

				const resultCreate = await Broker.call(createLinkedOrder, paramsRequest, { timeout: 60 * 1000 });
				if (resultCreate.code !== 105000) {
					Logger(`[PG_LIBRARY] ==> Create_Order_Failed : ${JSON.stringify({ paramsRequest, resultCreate })}`);
					return response;
				}

				await Broker.call('v1.eWalletLinkedModel.updateOne', [
					{ id: params.linkedId },
					{
						state: 'REQUIRED_VERIFY',
						'linkedInfo.transactionId': _.get(resultCreate, 'data.orderId', null)
					}
				]);

				paramsRequest = {
					body: {
						transaction: _.get(resultCreate, 'data.orderId', null),
						payCode: 'CREDIT',
						payData: {
							credit: {
								redirectUrl: params.redirectUrl,
								saveCard: true,
								cardNumber: params.cardNumber,
								cardHolder: params.cardHolder,
								expiredAt: params.expiredAt,
								cvv: params.cvv
							}
						},
						language: 'vi',
						note: 'Liên kết thẻ Credit'
					}
				};

				if (_.isEmpty(params.referenceId) === false) _.set(paramsRequest, 'body.payData.credit.referenceId', params.referenceId);
				if (_.isEmpty(params.billInfo) === false) _.set(paramsRequest, 'body.payData.credit.billInfo', params.billInfo);

				resultRequest = await Broker.call(linkedCredit, paramsRequest, {
					meta: {
						auth: {
							security: {}
						}
					}
				});
				response.supplierResponse = { resultRequest: JSON.stringify(resultRequest) };
				// {"cardId":"768","paymentId":"RYLA4AV7CKNR"}}
				const bodyResponse = resultRequest;
				if (bodyResponse.code === 129008) {
					const cardId = _.get(bodyResponse, 'data.cardId', false);
					if (!!cardId === true) {
						const resultHandlerLinked = await Broker.call(processLinkCredit, {
							cardId,
							paymentId: _.get(bodyResponse, 'data.paymentId', null)
						});
						if (resultHandlerLinked.code === ResponseCode.IPN_SUCCEEDED) {
							response.supplierResponse = JSON.stringify({ paramsRequest, resultRequest, resultHandlerLinked });
							response.code = ResponseCode.LINKED_SUCCESSED;
							response.transaction = _.get(bodyResponse, 'data.transaction', null);
							response.message = 'Liên kết thẻ Credit thành công.';
							response.state = BankLinkConstant.LINKED_STATE.LINKED;
							response.html = '';
						} else {
							response.code = ResponseCode.LINKED_FAILED;
							response.transaction = _.get(bodyResponse, 'data.transaction', null);
							response.message = 'Xử lý liên kết thất bại. Vui lòng liên hệ CSKH để được hỗ trợ.';
							response.state = BankLinkConstant.LINKED_STATE.FAILED;
							response.supplierResponse = JSON.stringify({ paramsRequest, resultRequest, resultHandlerLinked });
							response.html = '';
						}
					} else {
						response.supplierResponse = JSON.stringify({ ...resultRequest, data: { ...resultRequest.data, form: 'Đã bị ẩn vì quá dài' } });
						response.code = ResponseCode.LINKED_REQUIRED_VERIFY;
						response.transaction = _.get(bodyResponse, 'data.transaction', null);
						response.message = 'Liên kết cần xác thực bằng form ngân hàng.';
						response.state = BankLinkConstant.LINKED_STATE.REQUIRED_VERIFY;
						response.html = _.get(bodyResponse, 'data.form', null);
						response.paymentId = _.get(bodyResponse, 'data.paymentId', null);
					}
					return response;
				}

				_.set(paramsRequest, 'body.payData.credit.cardNumber', hiddenNumber(params.cardNumber, 'CARD'));
				_.set(paramsRequest, 'body.payData.credit.cvv', 'XXX');

				Logger(`[PG_LIBRARY] => [Linked] => [Params && Response] : ${JSON.stringify({ paramsRequest, resultRequest })}`);
				response.supplierResponse = JSON.stringify(response.supplierResponse);
				response.message = _.get(bodyResponse, 'message', 'Liên kết thất bại. Vui lòng thử lại sau (E016).');
				return response;
			} catch (error) {
				Logger(`[PG_LIBRARY] => [Linked] => [Exception] : ${error}`);
				Logger(`[PG_LIBRARY] => [Linked] => [Params && Response] : ${JSON.stringify({ paramsRequest, resultRequest })}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message, data: _.get(error, 'data', null) });
				return response;
			}
		},

		async UnLinked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.UNLINKED_FAILED,
				state: BankLinkConstant.LINKED_STATE.FAILED,
				message: 'Liên kết Credit thất bại. Vui lòng thử lại sau.',
				supplierResponse: null
			};
			const paramsRequest = {
				body: {
					cardId: params.cardId,
					language: 'vi'
				}
			};
			try {
				const resultRequest = await Broker.call(unlinkCredit, paramsRequest, {
					meta: {
						auth: {
							credentials: {
								accountId: process.env.PG_WALLET_ACCOUNT_ID
							}
						}
					}
				});
				response.supplierResponse = JSON.stringify(resultRequest);
				if (resultRequest.code !== 129200) {
					Logger(`[PG_LIBRARY] => [Unlinked] => [ Params && Response ] : ${JSON.stringify({ paramsRequest, resultRequest })}`);
					response.message = _.get(resultRequest, 'data.message', 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.');
					return response;
				}
				response.code = ResponseCode.UNLINKED_SUCCESSED;
				response.message = 'Huỷ liên kết thẻ thành công';
				response.state = BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[PG_LIBRARY] => [Unlinked] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async RefundCredit(params) {
			const response = {
				code: ResponseCode.REFUND_CREDIT_FAILED,
				message: 'Hoàn tiền giao dịch thẻ liên kết thất bại.',
				supplierResponse: null
			};
			try {
				const partnerTransaction = `${params.transaction}_${ShortId.generate()}`;
				const paramsRefund = {
					body: {
						reason: 'Hủy giao dịch xác thực liên kết thẻ Credit.',
						amount: params.amount,
						transaction: params.transaction,
						partnerTransaction,
						lang: 'vi'
					}
				};
				const resultRefund = await Broker.call(refundCredit, paramsRefund, {
					meta: {
						security: {
							credentials: {
								accountId: params.accountId
							}
						}
					}
				});
				response.supplierResponse = JSON.stringify(resultRefund);
				if (resultRefund.code !== 105003) {
					Logger(`[PG_LIBRARY] => [RefundCredit] => [paramsRefund && resultRefund] => ${JSON.stringify({ paramsRefund, resultRefund })}`);
					return response;
				}
				response.code = ResponseCode.REFUND_CREDIT_SUCCESSED;
				response.message = 'Hủy giao dịch liên kết thẻ thành công.';
			} catch (error) {
				Logger(`[PG_LIBRARY] => [RefundCredit] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
			}
			return response;
		}
	};
}
module.exports = PGLibrary;
