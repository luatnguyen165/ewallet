const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');
const BankLinkConstant = require('../constants/bankLink.constant');
const ResponseCode = require('../constants/code.constant');

function BIDVBankLibrary(config = {}) {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.BIDVBANK_TOKEN;
	const defaultNotifyUrl = process.env.HOME_URL;
	const inpUrl = process.env.GAPI_URL;

	const Broker = config.broker;
	const Logger = config.logger;
	const RedisService = config.redis();

	async function PostRequest(urlRequest, body, headers = {}) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({
					url: urlRequest,
					timeout: 50000,
					body: JSON.stringify(body),
					headers: {
						Authorization,
						...headers
					}
				}, (err, httpResponse, response) => {
					if (err) {
						reject(err);
					}
					resolve(httpResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}

	function parseHtml(link) {
		return `<html xmlns="http://www.w3.org/1999/xhtml">
			<head>  
			<title>Liên kết ngân hàng BIDV</title>  
			<meta http-equiv="refresh" content="0;URL=${link}"/>
			</head>
			<body></body>
			</html>`;
	}

	return {
		async Linked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.LINKED_FAILED,
				state: BankLinkConstant.LINKED_STATE.FAILED,
				message: 'Liên kết VietinBank thất bại. Vui lòng thử lại sau.',
				transaction: null,
				html: null,
				supplierResponse: null
			};
			const body = {
				trans_id: params.transaction,
				trans_desc: 'Lien ket ngan hang BIDV',
				account_id: params.accountId,
				account_name: params.fullname,
				return_url: !_.isNil(params.redirectUrl) ? params.redirectUrl : `${defaultNotifyUrl}`,
				notify_url: `${inpUrl}/ewallet/banking/bidv/ipn`,
				sign_type: 'RSA',
				sign: 'xxxxxxxx',
				phone: params.phone,
				payerIdentity: params.identifyNumber
			};

			let urlRequest = null;
			if (params.version === 2) {
				body.payerDebit = params.payerDebit;
				body.payerDebitType = params.payerDebitType;
				urlRequest = `${url}/v2/bidv/wallet/link`;
			} else {
				urlRequest = `${url}/v1/bidv/wallet/link`;
			}

			try {
				const result = await PostRequest(urlRequest, body);
				Logger(`[BIDVBANK_LIBRARY] => [Linked] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
				Logger(`[BIDVBANK_LIBRARY] => [Linked] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				response.supplierResponse = _.get(result, 'body', result);
				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Liên kết thất bại. Vui lòng thử lại sau (E013).');
					if (response.message === 'ESOCKETTIMEDOUT') {
						response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					}
					return response;
				}

				response.transaction = _.get(bodyResponse, 'data.trans_id', '');
				if (params.version === 2) {
					response.code = ResponseCode.LINKED_REQUIRE_OTP;
					response.message = 'Liên kết cần xác thực bằng mã OTP ngân hàng.';
					response.state = BankLinkConstant.LINKED_STATE.REQUIRED_OTP;
				} else {
					response.code = ResponseCode.LINKED_REQUIRED_VERIFY;
					response.message = 'Liên kết cần xác thực bằng form của ngân hàng.';
					response.state = BankLinkConstant.LINKED_STATE.REQUIRED_VERIFY;
					response.html = parseHtml(_.get(bodyResponse, 'data.redirect_url', null));
				}

				return response;
			} catch (error) {
				Logger(`[BIDVBANK_LIBRARY] => [Linked: ${urlRequest}] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async UnLinked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.UNLINKED_FAILED,
				state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
				message: 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.',
				supplierResponse: null
			};

			const body = {
				trans_id: params.transaction,
				trans_desc: 'Huy lien ket ngan hang BIDV',
				type: 0,
				account_id: _.toString(params.accountId),
				account_name: params.accountName,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};

			let urlRequest = null;
			if (params.version === 2) {
				urlRequest = `${url}/v2/bidv/wallet/unlink`;
			} else {
				urlRequest = `${url}/v1/bidv/wallet/unlink`;
			}

			try {
				const result = await PostRequest(urlRequest, body);
				Logger(`[BIDVBANK_LIBRARY] => [Unlinked] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
				Logger(`[BIDVBANK_LIBRARY] => [Unlinked] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				response.supplierResponse = _.get(result, 'body', result);
				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.');
					return response;
				}
				response.code = ResponseCode.UNLINKED_SUCCESSED;
				response.message = 'Huỷ liên kết thẻ thành công';
				response.state = BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[BIDVBANK_LIBRARY] => [Unlinked: ${urlRequest}] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async VerifyLinked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.VERIFY_LINKED_FAILED,
				state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
				message: 'Xác thực OTP thất bại. Vui lòng thử lại sau.'
			};
			const urlRequest = `${url}/v2/bidv/wallet/link/check_otp`;
			const body = {
				trans_id: params.transaction,
				otp_number: _.toString(params.otp),
				account_id: _.toNumber(params.accountId),
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};

			try {
				const result = await PostRequest(urlRequest, body);
				Logger(`[BIDVBANK_LIBRARY] => [VerifyLinked] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
				Logger(`[BIDVBANK_LIBRARY] => [VerifyLinked] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				response.supplierResponse = _.get(result, 'body', result);
				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code === 1000) {
					response.code = ResponseCode.VERIFY_LINKED_SUCCESSED;
					response.cardId = _.toString(body.account_id);
					response.message = 'Xác thực liên kết thành công.';
					response.state = BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED;
					return response;
				}
				if (bodyResponse.code === 2054) {
					response.code = ResponseCode.VERIFY_LINKED_FAILED;
					response.state = BankLinkConstant.LINKED_STATE.FAILED;
					response.message = _.get(bodyResponse, 'data.message', 'Quá thời gian xác thực OTP.');
					return response;
				}

				response.code = ResponseCode.VERIFY_LINKED_NOT_MATCH;
				response.message = _.get(bodyResponse, 'data.message', 'Mã OTP không chính xác.');
				response.state = BankLinkConstant.LINKED_PAYMENT_STATE.INVALID_OTP;
				return response;
			} catch (error) {
				Logger(`[BIDVBANK_LIBRARY] => [VerifyLinked: ${urlRequest}] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}

	};
}
module.exports = BIDVBankLibrary;
