const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');
const BankLinkConstant = require('../constants/bankLink.constant');
const ResponseCode = require('../constants/code.constant');

function PVcomBankLibrary(config = {}) {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.PVCOMBANK_TOKEN;

	const { test } = process.env;
	const Broker = config.broker;
	const Logger = config.logger;
	const RedisService = config.redis();

	async function PostRequest(urlRequest, body, headers = {}) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({
					url: urlRequest,
					timeout: 50000,
					body: JSON.stringify(body),
					headers: {
						Authorization,
						...headers
					}
				}, (err, httpResponse, response) => {
					if (err) {
						reject(err);
					}
					resolve(httpResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}
	return {
		async Linked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.LINKED_FAILED,
				state: BankLinkConstant.LINKED_STATE.FAILED,
				message: 'Liên kết PVComBank thất bại. Vui lòng thử lại sau.',
				transaction: null,
				html: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/card/link`;
			const body = {
				trans_id: params.transaction,
				request_id: params.requestId,
				bank_code: 'PVCB',
				card_number: _.trim(params.cardNumber),
				card_holder: _.trim(params.cardHolder).toUpperCase(),
				issue_date: params.issuedAt,
				expire_date: !!params.expiredAt === true ? params.expiredAt : '',
				account_id: _.toString(params.accountId),
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			Logger(`[PVCOMBANK_LIBRARY] => [Linked] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
			try {
				const result = await PostRequest(urlRequest, body);
				Logger(`[PVCOMBANK_LIBRARY] => [Linked] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				response.supplierResponse = _.get(result, 'body', result);

				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Liên kết thất bại. Vui lòng thử lại sau (E018).');
					if (response.message === 'ESOCKETTIMEDOUT') {
						response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					}
					return response;
				}
				response.code = ResponseCode.LINKED_REQUIRE_OTP;
				response.transaction = _.get(bodyResponse, 'data.trans_id', '');
				response.message = 'Liên kết cần xác thực bằng mã OTP ngân hàng.';
				response.state = BankLinkConstant.LINKED_STATE.REQUIRED_OTP;

				return response;
			} catch (error) {
				Logger(`[PVCOMBANK_LIBRARY] => [Linked] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				if (error.message === 'ESOCKETTIMEDOUT') {
					response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
				}
				return response;
			}
		},
		async UnLinked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.UNLINKED_FAILED,
				state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
				message: 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.',
				transaction: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/card/un_link`;
			const body = {
				trans_id: Math.round(Math.random() * 100000000000).toString(),
				request_id: Math.round(Math.random() * 100000000000).toString(),
				card_id: _.toString(params.cardId),
				account_id: _.toString(params.accountId),
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			Logger(`[PVCOMBANK_LIBRARY] => [Unlinked] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
			try {
				const result = await PostRequest(urlRequest, body);
				Logger(`[PVCOMBANK_LIBRARY] => [Unlinked] => [Response] : ${JSON.stringify({ urlRequest, result })}`);
				response.supplierResponse = _.get(result, 'body', result);

				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.');
					return response;
				}
				response.code = ResponseCode.UNLINKED_SUCCESSED;
				response.transaction = body.trans_id;
				response.message = 'Huỷ liên kết thẻ thành công';
				response.state = BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[PVCOMBANK_LIBRARY] => [Unlinked] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async VerifyLinked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.VERIFY_LINKED_FAILED,
				state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
				message: 'Xác thực OTP thất bại. Vui lòng thử lại sau.',
				transaction: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/card/verify_link`;
			const body = {
				trans_id: _.toString(params.transaction),
				request_id: Math.round(Math.random() * 100000000000).toString(),
				otp: _.toString(params.otp),
				card_number: params.cardNumber,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			Logger(`[PVCOMBANK_LIBRARY] => [VerifyLinked] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
			try {
				const result = await PostRequest(urlRequest, body);
				Logger(`[PVCOMBANK_LIBRARY] => [VerifyLinked] => [Response] : ${JSON.stringify({ urlRequest, result })}`);
				response.supplierResponse = _.get(result, 'body', result);

				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code === 1000) {
					response.code = ResponseCode.VERIFY_LINKED_SUCCESSED;
					response.cardId = _.get(bodyResponse, 'data.card_id', null);
					response.message = 'Xác thực liên kết thẻ thành công';
					response.state = BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED;
				} else if (bodyResponse.code === 1008) {
					// Khóa OTP khí quá 3 lần nhập sai.
					response.code = ResponseCode.VERIFY_LINKED_NOT_MATCH;
					response.message = _.get(bodyResponse, 'data.message', 'Mã OTP không chính xác');
					response.state = BankLinkConstant.LINKED_STATE.INVALID_OTP;
				} else {
					response.message = _.get(bodyResponse, 'data.message', 'Xác thực OTP thất bại. Vui lòng thử lại sau.');
				}
				return response;
			} catch (error) {
				Logger(`[PVCOMBANK_LIBRARY] => [VerifyLinked] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}

	};
}
module.exports = PVcomBankLibrary;
