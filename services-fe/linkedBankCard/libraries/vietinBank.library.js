const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');
const BankLinkConstant = require('../constants/bankLink.constant');
const ResponseCode = require('../constants/code.constant');

function VietinBankLibrary(config = {}) {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.VIETINBANK_TOKEN;

	const Broker = config.broker;
	const Logger = config.logger;
	const RedisService = config.redis();

	async function PostRequest(urlRequest, body, headers = {}) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({
					url: urlRequest,
					timeout: 50000,
					body: JSON.stringify(body),
					headers: {
						Authorization,
						...headers
					}
				}, (err, httpResponse, response) => {
					if (err) {
						reject(err);
					}
					resolve(httpResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}
	return {
		async Linked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.LINKED_FAILED,
				state: BankLinkConstant.LINKED_STATE.FAILED,
				message: 'Liên kết VietinBank thất bại. Vui lòng thử lại sau.',
				transaction: null,
				html: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/wallet/vietin/card/link`;
			const body = {
				trans_id: _.toString(params.transaction),
				account_id: _.toString(params.accountId),
				card_num: _.toString(params.cardNumber),
				card_holder_name: _.toString(params.cardHolder),
				card_issue_date: params.issuedAt,
				customer_phone: _.toString(params.phone),
				customer_id: _.toString(params.customerId),
				channel: 'MOBILE',
				language: 'vi',
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			try {
				const result = await PostRequest(urlRequest, body);

				response.supplierResponse = result.body || result;
				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 2053) {
					Logger(`[VIETINBANK_LIBRARY] => [Linked: ${urlRequest}] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
					Logger(`[VIETINBANK_LIBRARY] => [Linked: ${urlRequest}] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

					response.message = _.get(bodyResponse, 'data.message', 'Liên kết thất bại. Vui lòng thử lại sau (E019).');
					if (response.message === 'ESOCKETTIMEDOUT') {
						response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					}
					return response;
				}
				response.code = ResponseCode.LINKED_REQUIRE_OTP;
				response.transaction = _.get(bodyResponse, 'data.trans_id', body.trans_id);
				response.message = 'Liên kết cần xác thực bằng mã OTP ngân hàng.';
				response.state = BankLinkConstant.LINKED_STATE.REQUIRED_OTP;

				return response;
			} catch (error) {
				Logger(`[VIETINBANK_LIBRARY] => [Linked: ${urlRequest}] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async UnLinked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.UNLINKED_FAILED,
				state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
				message: 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.',
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/wallet/vietin/card/un_link`;
			const body = {
				account_id: _.toString(params.accountId),
				card_id: _.toNumber(params.cardId),
				channel: 'MOBILE',
				language: 'vi',
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};

			try {
				const result = await PostRequest(urlRequest, body);
				response.supplierResponse = _.get(result, 'body', result);

				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					Logger(`[VIETINBANK_LIBRARY] => [Unlinked] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
					Logger(`[VIETINBANK_LIBRARY] => [Unlinked] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

					response.message = _.get(bodyResponse, 'data.message', 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.');
					return response;
				}
				response.code = ResponseCode.UNLINKED_SUCCESSED;
				response.transaction = body.trans_id;
				response.message = 'Huỷ liên kết thẻ thành công';
				response.state = BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[VIETINBANK_LIBRARY] => [Unlinked: ${urlRequest}] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async VerifyLinked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.VERIFY_LINKED_FAILED,
				state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
				message: 'Xác thực OTP thất bại. Vui lòng thử lại sau.',
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/wallet/vietin/card/link/verify_otp`;
			const body = {
				connector_trans_id: _.toString(params.transaction),
				otp: _.toString(params.otp),
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};

			try {
				const result = await PostRequest(urlRequest, body);
				response.supplierResponse = _.get(result, 'body', result);

				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					Logger(`[VIETINBANK_LIBRARY] => [VerifyLinked] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
					Logger(`[VIETINBANK_LIBRARY] => [VerifyLinked] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

					const message = _.get(bodyResponse, 'data.message', 'Xác thực thất bại.');
					if (_.includes(message, 'OTP') === true) {
						// Khóa OTP khi nhập sai 3 lần
						response.code = ResponseCode.VERIFY_LINKED_NOT_MATCH;
						response.message = 'Mã OTP không đúng. Vui lòng nhập lại.';
						response.state = BankLinkConstant.LINKED_PAYMENT_STATE.INVALID_OTP;
						return response;
					}
					return response;
				}
				response.code = ResponseCode.VERIFY_LINKED_SUCCESSED;
				response.cardId = _.get(bodyResponse, 'data.card_id', null);
				response.message = 'Xác thực liên kết thành công.';
				response.state = BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED;
				return response;
			} catch (error) {
				Logger(`[VIETINBANK_LIBRARY] => [VerifyLinked: ${urlRequest}] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}

	};
}
module.exports = VietinBankLibrary;
