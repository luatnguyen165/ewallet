const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');
const BankLinkConstant = require('../constants/bankLink.constant');
const ResponseCode = require('../constants/code.constant');

function WalletLibrary(config = {}) {
	const url = process.env.WALLET_URL;
	const Authorization = process.env.WALLET_TOKEN;

	const Broker = config.broker;
	const Logger = config.logger;
	// const RedisService = config.redis();

	async function PostRequest(urlRequest, body, headers = {}) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({
					url: urlRequest,
					timeout: 50000,
					body: JSON.stringify(body),
					headers: {
						Authorization,
						...headers
					}
				}, (err, httpResponse, response) => {
					if (err) {
						reject(err);
					}
					resolve(httpResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}
	return {
		async Deposit(params) {
			const response = {
				successed: false,
				message: 'Nạp tiền vào Ví thất bại.',
				balance: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/Wallet/Deposit`;
			const body = {
				accountId: params.accountId,
				amount: params.amount,
				description: params.description,
				referData: params.referData
			};

			try {
				const result = await PostRequest(urlRequest, body);
				response.supplierResponse = _.get(result, 'body', JSON.stringify(result));

				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					Logger(`[WALLET_LIBRARY] => [Deposit] => [Params] : ${body}`);
					Logger(`[WALLET_LIBRARY] => [Deposit] => [Response] : ${result}`);
					response.message = _.get(bodyResponse, 'data.message', response.message);
					return response;
				}
				response.successed = true;
				response.message = 'Nạp tiền vào ví PayME thành công.';
				response.balance = _.get(bodyResponse, 'data.balance', null);
				return response;
			} catch (error) {
				Logger(`[WALLET_LIBRARY] => [WALLET_LIBRARY: ${urlRequest}] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}
	};
}
module.exports = WalletLibrary;
