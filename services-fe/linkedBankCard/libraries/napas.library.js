const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');
const BankLinkConstant = require('../constants/bankLink.constant');
const ResponseCode = require('../constants/code.constant');

function NapasLibrary(config = {}) {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.NAPAS_TOKEN;
	const defaultNotifyUrl = process.env.HOME_URL;
	const inpUrl = process.env.GAPI_URL;

	const Broker = config.broker;
	const Logger = config.logger;
	// const RedisService = config.redis();

	async function PostRequest(urlRequest, body, headers = {}) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({
					url: urlRequest,
					timeout: 50000,
					body: JSON.stringify(body),
					headers: {
						Authorization,
						...headers
					}
				}, (err, httpResponse, response) => {
					if (err) {
						reject(err);
					}
					resolve(httpResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}
	return {
		async Linked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.LINKED_FAILED,
				state: BankLinkConstant.LINKED_STATE.FAILED,
				message: 'Liên kết VietinBank thất bại. Vui lòng thử lại sau.',
				transaction: null,
				html: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/global/card/link`;
			const body = {
				trans_id: _.toString(params.transaction),
				card_number: _.trim(params.cardNumber),
				card_holder: _.trim(params.cardHolder).toUpperCase(),
				issue_date: _.trim(params.issuedAt),
				account_id: _.toString(params.accountId),
				amount: params.amount,
				return_url: !_.isNil(params.redirectUrl) ? params.redirectUrl : `${defaultNotifyUrl}`, // redirect sau khi user nhập otp liên kết thẻ thành công
				notify_url: `${inpUrl}/ewallet/banking/napas/ipn`,
				client_ip: params.clientIp || 'NONE',
				device_id: _.trim(params.clientId),
				env: _.trim(params.envName),
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};

			try {
				const result = await PostRequest(urlRequest, body);
				response.supplierResponse = _.get(result, 'body', result);

				const bodyResponse = JSON.parse(result.body);

				Logger(`[NAPAS_LIBRARY] => [Linked] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
				Logger(`[NAPAS_LIBRARY] => [Linked] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				if (bodyResponse.code === 1000) {
					response.code = ResponseCode.LINKED_REQUIRED_VERIFY;
					response.transaction = _.get(bodyResponse, 'data.order_id', null);
					response.message = 'Liên kết cần xác thực bằng form ngân hàng.';
					response.state = BankLinkConstant.LINKED_STATE.REQUIRED_VERIFY;
					response.html = _.get(bodyResponse, 'data.data', null);

					return response;
				}

				response.message = _.get(bodyResponse, 'data.message', 'Liên kết thất bại. Vui lòng thử lại sau (E017).');
				if (response.message === 'ESOCKETTIMEDOUT') {
					response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
				}

				if (bodyResponse.code === 2052) {
					// thẻ đã được liên kết => tạm thời chưa xử lý
					return response;
				}

				return response;
			} catch (error) {
				Logger(`[NAPAS_LIBRARY] => [Linked] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async UnLinked(params) {
			// Response:  code - message - state - transaction - html
			const response = {
				code: ResponseCode.UNLINKED_FAILED,
				state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
				message: 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.',
				transaction: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/global/token/delete`;
			const body = {
				trans_id: Math.round(Math.random() * 100000000000).toString(),
				account_id: _.toString(params.accountId),
				card_id: _.toString(params.cardId),
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			try {
				const result = await PostRequest(urlRequest, body);
				response.supplierResponse = _.get(result, 'body', result);

				const bodyResponse = JSON.parse(result.body);
				Logger(`[NAPAS_LIBRARY] => [Unlinked] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
				Logger(`[NAPAS_LIBRARY] => [Unlinked] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.');
					return response;
				}
				response.code = ResponseCode.UNLINKED_SUCCESSED;
				response.message = 'Huỷ liên kết thẻ thành công';
				response.state = BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[NAPAS_LIBRARY] => [Unlinked] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}

	};
}
module.exports = NapasLibrary;
