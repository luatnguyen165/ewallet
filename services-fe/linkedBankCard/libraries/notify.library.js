const Request = require('request');

const Private = {
	async requestPost(requestLink, body, headers = { Authorization: '' }) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({ url: requestLink, body: JSON.stringify(body), headers }, (err, httpResponse, bodyResponse) => {
					if (err) return reject(err);
					return resolve(bodyResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}
};

class NotifyService {
	constructor(url, authorization, envName) {
		this.url = url;
		this.authorization = authorization;
		this.envName = envName;
	}

	async SendAlertNotify(pushMessage, buttons, receiverList = []) {
		const response = {
			code: -1,
			data: {}
		};
		try {
			if (this.envName) {
				pushMessage = `[${this.envName}] - ${pushMessage}`;
			}
			const payload = {
				type: 'telegram',
				receiver: receiverList,
				message: [
					{
						type: 'text',
						content: pushMessage
					}
				]
			};
			if (buttons) {
				payload.message[0].buttons = buttons;
			}
			const result = await Private.requestPost(`${this.url}notification`, payload, { Authorization: this.authorization });
			const json = JSON.parse(result);
			if (json.code === 1000) {
				response.code = 1;
			} else {
				response.code = -2;
			}
			response.data = json.data;
			return response;
		} catch (error) {
			response.data = error;
		}

		return response;
	}

	async SendExceptionNotify(pushMessage) {
		const response = {
			code: -1,
			data: {}
		};
		try {
			let receiverList = ['api'];
			if (this.envName) {
				pushMessage = `[${this.envName}] - ${pushMessage}`;
				if (this.envName === 'Sandbox') {
					receiverList = ['AEX'];
				}
			}
			const payload = {
				type: 'telegram',
				receiver: receiverList,
				message: [
					{
						type: 'text',
						content: pushMessage
					}
				]
			};
			const result = await Private.requestPost(`${this.url}notification`, payload, { Authorization: this.authorization });
			const json = JSON.parse(result);
			if (json.code === 1000) {
				response.code = 1;
			} else {
				response.code = -2;
			}
			response.data = json.data;
			return response;
		} catch (error) {
			response.data = error;
		}
		return response;
	}
}

const notifyService = new NotifyService(
	process.env.NOTIFY_URL,
	process.env.NOTIFY_TOKEN,
	process.env.ENV_NAME
);

module.exports = notifyService;
