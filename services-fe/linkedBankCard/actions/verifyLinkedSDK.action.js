// Libraries
const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const shortid = require('shortid');
const Numeral = require('numeral');
const Moment = require('moment');

// Constants
const GeneralConstant = require('../constants/general.constant');
const ResponseCode = require('../constants/code.constant');
const BankLinkConstant = require('../constants/bankLink.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	const { removeUnicode, hiddenNumber } = this.Helper();

	const response = {
		code: ResponseCode.VERIFY_LINKED_FAILED,
		message: 'Xác thực liên kết thất bai. Vui lòng thử lại sau.'
	};
	// MODEL
	const {
		LinkedModel, HistoryModel
	} = GeneralConstant.MODEL;
	// SERVICE
	const { UUID, PAYMENT_METHOD } = GeneralConstant.SERVICE;

	try {
		const payload = _.get(ctx, 'params.body', {});
		const authInfo = _.get(ctx, 'meta.auth.walletAuthInfo', {});

		Logger(`[VERIFY_LINK] => [payload] ===> ${JSON.stringify(payload)}`);
		Logger(`[VERIFY_LINK] => [credentials] ===> ${JSON.stringify(_.get(ctx, 'meta.auth.credentials', {}))}`);
		Logger(`[VERIFY_LINK] => [walletAuthInfo] ===> ${JSON.stringify(authInfo)}`);

		const { transaction, otp } = payload;

		const findCondition = {
			transaction: payload.transaction,
			state: BankLinkConstant.LINKED_STATE.REQUIRED_OTP
		};

		const linkedInfo = await Broker.call(LinkedModel.findOne, [findCondition]);
		if (_.get(linkedInfo, 'id', false) === false) {
			Logger(`[VERIFY_BANK] => [findCondition]: ${JSON.stringify(findCondition)}`);
			response.message = 'Không tìm thấy thông tin liên kết. Vui lòng kiểm tra lại.';
			return response;
		}

		// findCondition = {
		// 	accountId: linkedInfo.accountId,
		// 	state: BankLinkConstant.LINKED_STATE.LINKED
		// };
		// // block 1 user - nhiều thẻ 1 ngân hàng
		// const checkLinked = await Broker.call(LinkedModel.findOne, [findCondition]);
		// if (_.get(checkLinked, 'id', false) !== false) {
		// 	const cardNumberInfo = _.get(linkedInfo, 'cardInfo.cardNumber', '') || '';
		// 	if (!!cardNumberInfo === true) {
		// 		response.message = `Tài khoản của bạn đã được liên kết ngân hàng này, số thẻ ${hiddenNumber(checkLinked.cardInfo.cardNumber, 'CARD')}`;
		// 	} else {
		// 		response.message = `Tài khoản của bạn đã được liên kết ngân hàng này, số tài khoản ${hiddenNumber(checkLinked.cardInfo.accountNumber, 'ACCOUNT')}`;
		// 	}
		// 	return response;
		// }

		if (linkedInfo.state === BankLinkConstant.LINKED_STATE.FAILED) {
			response.message = 'Thông tin thẻ liên kết không hợp lê. Vui lòng kiểm tra lại.';
			return response;
		}

		if (linkedInfo.state === BankLinkConstant.LINKED_STATE.UNLINK) {
			response.message = 'Thẻ liên kết đã bị hủy trước đó.';
			return response;
		}

		if (linkedInfo.state === BankLinkConstant.LINKED_STATE.LOCKED) {
			response.message = 'Thể liên kết đang bị khóa. Vui lòng thử lại sau.';
			return response;
		}

		if (linkedInfo.state === BankLinkConstant.LINKED_STATE.LINKED) {
			response.message = 'Thẻ đã được liên kết trước đó. Vui lòng kiểm ta lại.';
			return response;
		}

		const linkedSupplier = linkedInfo.supplier;
		const swiftCode = _.get(linkedInfo, 'cardInfo.swiftCode', '');
		let routeName = null;
		let bankName = null;
		let resultRequest = null;
		const paramsRequest = {
			transaction: _.get(linkedInfo, 'linkedInfo.transactionId', false),
			otp,
			accountId: linkedInfo.accountId,
			cardNumber: linkedInfo.cardInfo.cardNumber
		};

		switch (linkedSupplier) {
			case BankLinkConstant.LINKED_SUPPLIER.PVCOMBANK: {
				routeName = 'PVComBank';
				bankName = 'PVComBank';
				resultRequest = await this.PVComBankLibs.VerifyLinked(paramsRequest);
				break;
			}
			case BankLinkConstant.LINKED_SUPPLIER.VIETINBANK: {
				routeName = 'VietinBank';
				bankName = 'VietinBank';
				resultRequest = await this.VietinBankLibs.VerifyLinked(paramsRequest);
				break;
			}
			case BankLinkConstant.LINKED_SUPPLIER.BIDVBANK: {
				routeName = 'BIDV';
				bankName = 'BIDV';
				resultRequest = await this.BIDVBankLibs.VerifyLinked(paramsRequest);
				break;
			}
			case BankLinkConstant.LINKED_SUPPLIER.OCBBANK: {
				routeName = 'OCBBank';
				bankName = 'OCB';
				resultRequest = await this.OCBBankLibs.VerifyLinked(paramsRequest);
				break;
			}
			default:
				response.message = 'Ngân hàng liên kết chưa hỗ trợ phương thức xác thực này';
				return response;
		}
		Logger(`[VERIFY_BANK] => [resultRequest]: ${JSON.stringify(resultRequest)}`);

		response.code = resultRequest.code;
		response.message = resultRequest.message;

		let conditionUpdate = {};
		let fieldUpdate = {};

		let conditionUpdateHistory = {};
		let fieldUpdateHistory = {};
		let isRegisterPaymentMethod = false;
		let paymentDesc = null;

		switch (resultRequest.code) {
			case ResponseCode.VERIFY_LINKED_SUCCESSED: {
				// OTP thành công => Xử lý liên kết
				isRegisterPaymentMethod = true;

				const number = linkedInfo.type === BankLinkConstant.LINKED_TYPE.ACCOUNT ? _.get(linkedInfo, 'cardInfo.accountNumber', null) : _.get(linkedInfo, 'cardInfo.cardNumber', null);
				paymentDesc = bankName && number ? `${bankName}-${number.substr(-4, 4)}` : '';

				conditionUpdate = { id: linkedInfo.id };
				conditionUpdateHistory = {
					'service.id': linkedInfo.id,
					'service.type': BankLinkConstant.HISTORY_SERVICE.LINKED
				};

				fieldUpdate = {
					state: BankLinkConstant.LINKED_STATE.LINKED,
					linkedAt: new Date().toISOString(),
					'linkedInfo.cardId': _.toString(_.get(resultRequest, 'cardId', null)),
					'cardInfo.methodName': paymentDesc,
					'linkedInfo.identificationNumber': _.get(resultRequest, 'identificationNumber', null),
					$push: {
						supplierResponsed: _.get(resultRequest, 'supplierResponse', null)
					}
				};

				fieldUpdateHistory = {
					state: BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED,
					'service.state': BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED
				};
				break;
			}
			case ResponseCode.VERIFY_LINKED_NOT_MATCH: {
				// Trường hợp OTP bị tạch => Nhập lại.
				conditionUpdate = { id: linkedInfo.id };
				fieldUpdate = {
					$push: {
						supplierResponsed: _.get(resultRequest, 'supplierResponse', null)
					}
				};
				break;
			}
			case ResponseCode.VERIFY_LINKED_FAILED: {
				// Trường hợp bị khóa hoặc thất bại.
				conditionUpdate = { id: linkedInfo.id };
				conditionUpdateHistory = {
					'service.id': linkedInfo.id,
					'service.type': BankLinkConstant.HISTORY_SERVICE.LINKED
				};
				fieldUpdate = {
					state: BankLinkConstant.LINKED_STATE.FAILED,
					$push: {
						supplierResponsed: _.get(resultRequest, 'supplierResponse', null)
					}
				};
				fieldUpdateHistory = {
					state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
					'service.state': BankLinkConstant.LINKED_PAYMENT_STATE.FAILED
				};

				break;
			}
			default:
				break;
		}

		let resultUpdate = null;

		if (_.isEmpty(conditionUpdate) === false && _.isEmpty(fieldUpdate) === false) {
			resultUpdate = await Broker.call(LinkedModel.updateOne, [conditionUpdate, fieldUpdate]);
			if (!_.isObject(resultUpdate) || resultUpdate.nModified < 1) {
				Logger(`[VERIFY_LINK] => [conditionUpdate & fieldUpdate]: ${JSON.stringify({ conditionUpdate, fieldUpdate })}`);
				return response;
			}
		}

		if (_.isEmpty(conditionUpdateHistory) === false && _.isEmpty(fieldUpdateHistory) === false) {
			resultUpdate = await Broker.call(HistoryModel.updateOne, [conditionUpdateHistory, fieldUpdateHistory]);
			if (!_.isObject(resultUpdate) || resultUpdate.nModified < 1) {
				Logger(`[VERIFY_LINK] => [conditionUpdateHistory & fieldUpdateHistory]: ${JSON.stringify({ conditionUpdateHistory, fieldUpdateHistory })}`);
				return response;
			}
		}

		if (isRegisterPaymentMethod === true) {
			// Đăng kí PTTT => Call action làm sau
			const registerCode = `${BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK}_${swiftCode}_${linkedInfo.id}`;
			const paramsRegister = {
				registerCode,
				accountId: linkedInfo.accountId,
				group: BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK,
				title: bankName,
				description: 'Tài khoản liên kết',
				isActive: true,
				paymentInfo: {
					swiftCode,
					linkedId: linkedInfo.id,
					cardNumber: _.get(linkedInfo, 'cardInfo.cardNumber', '') || '',
					accountNumber: _.get(linkedInfo, 'cardInfo.accountNumber', '') || '',
					cardHolder: _.get(linkedInfo, 'cardInfo.cardHolder', '') || '',
					routeName,
					bankName,
					methodName: paymentDesc
				},
				isWithdrawable: true
			};
			try {
				await Broker.call(PAYMENT_METHOD.registerPaymentMethod, paramsRegister);
			} catch (error) {
				Logger(`[VERIFY_LINK] => [RegisterPaymentMethod] => [Error]: ${error.message}`);
				Logger(`[VERIFY_LINK] => [RegisterPaymentMethod] => [paramsRegister]: ${JSON.stringify(paramsRegister)}`);
			}
		}
		_.set(ctx, 'meta.lockInfo', { linkedId: linkedInfo.id, keyRedis: `LOCK_LINKED_${linkedSupplier}_${linkedInfo.accountId}` });
		return response;
	} catch (error) {
		if (error.name === 'MoleculerError') {
			response.code = error.code;
			response.message = error.message;
		}
		Logger(`[VERIFY_BANK] => [ERROR]: ${error}`);
		return response;
	}
};
