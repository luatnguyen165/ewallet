const _ = require('lodash');
const Moment = require('moment');

// Constants
const GeneralConstant = require('../constants/general.constant');
const ResponseCode = require('../constants/code.constant');
const BankLinkConstant = require('../constants/bankLink.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	const response = {
		code: ResponseCode.IPN_FAILED,
		message: 'Liên kết thất bại.',
		extraData: null
	};
	// MODEL
	const {
		LinkedModel, HistoryModel, PaymentPGModel
	} = GeneralConstant.MODEL;
	// SERVICE
	const { PAYMENT_METHOD } = GeneralConstant.SERVICE;

	try {
		// { trans_id: "123456" (mã trans id của dịch vụ trả về khi link), is_success: true (true: thành công, false: thất bại), more_info:  thông tin tài khoản/thẻ }
		const payload = _.get(ctx, 'params.body', {});
		response.extraData = JSON.stringify(payload);
		Logger(`[PROCESS_LINKED_CREDIT] => [PAYLOAD]: ${JSON.stringify(payload)}}`);

		if (payload.state === 'CANCELED_SUCCEEDED') {
			console.log('[PROCESS_LINKED_CREDIT] => [Cancel_Success] :>> ', 'Hủy giao dịch liên kết thành công');
			response.message = 'Ghi nhận hủy giao dịch thành công';
			response.code = ResponseCode.IPN_SUCCEEDED;
			return response;
		}

		const findCondition = {
			transaction: payload.partnerTransaction,
			'linkedInfo.transactionId': payload.transaction,
			type: BankLinkConstant.LINKED_TYPE.CREDIT_CARD,
			state: BankLinkConstant.LINKED_STATE.REQUIRED_VERIFY
		};

		let linkedInfo = await Broker.call(LinkedModel.findOne, [findCondition]);
		if (_.get(linkedInfo, 'id', false) === false) {
			response.message = 'Không tìm thấy thông tin liên kết.';
			return response;
		}

		if (!!payload.cardId === false) {
			await Broker.call(LinkedModel.updateOne, [{ id: linkedInfo.id }, {
				state: BankLinkConstant.LINKED_STATE.FAILED,
				$push: {
					supplierResponsed: JSON.stringify(payload)
				}
			}]);
			response.message = 'Không tìm thấy thông tin cardId';
			return response;
		}

		const state = _.get(payload, 'state', BankLinkConstant.LINKED_PAYMENT_STATE.FAILED);
		if (state !== BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED) {
			const arrNotifyState = ['AUTHORIZED'];

			if (_.includes(arrNotifyState, state) === true) {
				// const alertMsg = `❗Thanh thanh toán bằng thẻ tín dụng đã được ghi nhận xử lý
				// \nTransaction: ${JSON.stringify(payload.partnerTransaction)},
				// \nPartner Transaction: ${JSON.stringify(payload.transaction)}
				// \nLinkedId: ${linkedInfo.id}`;
				// NotifyService.SendAlertNotify(alertMsg, [], ['api']);
				response.message = 'Đã nhận yêu cầu liên kết thẻ.';
				return response;
			}

			await Broker.call(LinkedModel.updateOne, [{ id: linkedInfo.id }, {
				state: BankLinkConstant.LINKED_STATE.FAILED,
				$push: {
					supplierResponsed: JSON.stringify(payload)
				}
			}]);
			response.message = 'Liên kết thất bại. Giao dịch không ở trạng thái thành công.';
			return response;
		}

		const paymentInfo = await Broker.call(PaymentPGModel.findOne, [{ paymentId: payload.paymentId }]);
		const issuer = _.toUpper(_.get(paymentInfo, 'issuer.name', 'VISA'));
		const payCode = _.get(paymentInfo, 'paymentInfo.payCode', 'CREDIT');
		const fieldUpdatedLinked = {
			'linkedInfo.cardId': _.toString(payload.cardId),
			'linkedInfo.issuer': issuer,
			'linkedInfo.payCode': payCode,
			state: BankLinkConstant.LINKED_STATE.LINKED,
			linkedAt: new Date().toISOString(),
			$push: {
				supplierResponsed: JSON.stringify(payload)
			}
		};

		linkedInfo = await Broker.call(LinkedModel.findOneAndUpdate, [{ id: linkedInfo.id }, fieldUpdatedLinked, { new: true }]);
		if (_.get(linkedInfo, 'id', false) === false) {
			response.message = 'Cập nhật liên kết thất bại. Vui long thử lại sau.';
			return response;
		}

		const cardNumber = _.get(linkedInfo, 'cardInfo.cardNumber', null);
		const methodName = cardNumber && `${issuer}-${cardNumber.substr(-4, 4)}`;
		const registerCode = `${BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK}_CREDIT_CARD_${linkedInfo.id}`;
		const paramsRegister = {
			registerCode,
			accountId: linkedInfo.accountId,
			group: BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK,
			title: _.get(linkedInfo, 'linkedInfo.issuer', 'VISA'),
			description: 'Tài khoản liên kết',
			isActive: true,
			paymentInfo: {
				linkedId: linkedInfo.id,
				cardNumber: _.get(linkedInfo, 'cardInfo.cardNumber', '') || '',
				accountNumber: _.get(linkedInfo, 'cardInfo.accountNumber', '') || '',
				cardHolder: _.get(linkedInfo, 'cardInfo.cardHolder', '') || '',
				methodName
			},
			isWithdrawable: false
		};
		try {
			await Broker.call(PAYMENT_METHOD.registerPaymentMethod, paramsRegister);
		} catch (error) {
			Logger(`[PROCESS_LINKED_CREDIT] => [RegisterPaymentMethod] => [Error]: ${error.message}`);
			Logger(`[PROCESS_LINKED_CREDIT] => [RegisterPaymentMethod] => [paramsRegister]: ${JSON.stringify(paramsRegister)}`);
		}

		//  Hủy giao dịch liên kết
		const paramsRefund = {
			amount: paymentInfo.amount || 1000,
			transaction: payload.transaction || paymentInfo.transactionId
		};
		let resultRefund = null;
		try {
			resultRefund = await this.PGLibs.RefundCredit(paramsRefund);
			Logger(`[PROCESS_LINKED_CREDIT] => [Refund Credit] => [paramsRefund]: ${JSON.stringify(paramsRefund)}`);
			Logger(`[PROCESS_LINKED_CREDIT] => [Refund Credit] => [resultRefund]: ${JSON.stringify(resultRefund)}`);
		} catch (error) {
			Logger(`[PROCESS_LINKED_CREDIT] => [Refund Credit] => [Error]: ${error.message}`);
		}

		response.code = ResponseCode.IPN_SUCCEEDED;
		response.message = 'Liên kết thành công.';
		return response;
	} catch (error) {
		Logger(`[PROCESS_LINKED_CREDIT] => [EXCEPTION]: ${error}}`);
		response.message = error.message || 'Liên kết thất bại.';
		return response;
	}
};
