// Libraries
const _ = require('lodash');
const Moment = require('moment');

// Constants
const GeneralConstant = require('../constants/general.constant');
const ResponseCode = require('../constants/code.constant');
const BankLinkConstant = require('../constants/bankLink.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	const { hiddenNumber } = this.Helper();
	const response = {
		code: ResponseCode.LINKED_FAILED,
		message: 'Lấy danh sách liên kết thất bại. Vui lòng thử lại sau.'
	};
	// MODEL
	const { LinkedModel } = GeneralConstant.MODEL;

	try {
		const authInfo = _.get(ctx, 'meta.auth.walletAuthInfo', {});
		const query = _.get(ctx, 'params.query', null) || _.get(ctx, 'params.body', null) || {};

		Logger(`[GET_LINKED_LIST] => [query] ===> ${JSON.stringify(query)}`);
		Logger(`[GET_LINKED_LIST] => [credentials] ===> ${JSON.stringify(_.get(ctx, 'meta.auth.credentials', {}))}`);
		Logger(`[GET_LINKED_LIST] => [walletAuthInfo] ===> ${JSON.stringify(authInfo)}`);

		let findCondition = null;

		if (_.isEmpty(authInfo) === true) {
			if (_.isEmpty(query) === true || !!query.linkedId === false) {
				response.message = 'Không tìm thấy thông tin tài khoản Ví PayME.';
				return response;
			}
			findCondition = {
				id: query.linkedId
				// state: BankLinkConstant.LINKED_STATE.REQUIRED_VERIFY
			};
			Logger(`[GET_LINKED_LIST] => [findCondition_1]: ${JSON.stringify(findCondition)}}`);

			const linkedInfo = await Broker.call(LinkedModel.findOne, [findCondition]);
			if (_.get(linkedInfo, 'id', false) === false) {
				response.message = 'Không tìm thấy thông tin liên kết.';
				return response;
			}
			return {
				code: ResponseCode.GET_LINKED_LIST_SUCCESSED,
				message: 'Lấy thông tin thẻ liên kết thành công.',
				data: {
					linkedId: linkedInfo.id,
					accountId: linkedInfo.accountId,
					transaction: `${_.get(linkedInfo, 'linkedInfo.transactionId', null)}_${linkedInfo.accountId}`,
					type: linkedInfo.type,
					state: linkedInfo.state
				}
			};
		}

		findCondition = {
			accountId: authInfo.accountId
		};
		if (!!query.linkedId === true) {
			findCondition.id = query.linkedId;
		} else {
			findCondition.state = 'LINKED';
		}

		Logger(`[GET_LINKED_LIST] => [findCondition_2]: ${JSON.stringify(findCondition)}}`);
		const listLinkedInfo = await Broker.call(LinkedModel.find, [findCondition, '-_id', { sort: { createdAt: -1 } }]);
		if (_.isEmpty(listLinkedInfo) === true) {
			return {
				code: ResponseCode.GET_LINKED_LIST_SUCCESSED,
				message: 'Lấy thông tin thẻ liên kết thành công.',
				data: {
					totalLinked: 0,
					linkedList: []
				}
			};
		}

		return {
			code: ResponseCode.GET_LINKED_LIST_SUCCESSED,
			message: 'Lấy thông tin thẻ liên kết thành công.',
			data: {
				totalLinked: listLinkedInfo.length,
				linkedList: listLinkedInfo.map((item) => ({
					linkedId: _.get(item, 'id', null),
					linkedAt: _.get(item, 'createdAt', null) ? Moment(_.get(item, 'createdAt', null)).format('DD/MM/YYYY') : '',
					type: _.get(item, 'type', null),
					issuer: _.toUpper(_.get(item, 'linkedInfo.issuer', '') || _.get(item, 'cardInfo.bankName', null) || ''),
					swiftCode: _.get(item, 'cardInfo.swiftCode', null),
					cardNumber: hiddenNumber(_.get(item, 'cardInfo.cardNumber', null), 'CARD') || '',
					accountNumber: hiddenNumber(_.get(item, 'cardInfo.accountNumber', null), 'ACCOUNT') || '',
					cardHolder: _.toUpper(_.get(item, 'cardInfo.cardHolder', '')),
					issuedAt: _.get(item, 'cardInfo.issuedAt', null) ? Moment(item.cardInfo.issuedAt, 'YYYY/MM').format('MM/YYYY') : '',
					expiredAt: _.get(item, 'cardInfo.expiredAt', null) ? Moment(item.cardInfo.expiredAt, 'YYYY/MM').format('MM/YYYY') : ''
				}))
			}
		};
	} catch (error) {
		Logger(`[GET_LINKED_LIST] => [ERROR]: ${error}}`);
		return {
			code: ResponseCode.GET_LINKED_LIST_FAILED,
			message: 'Lấy thông tin thẻ liên kết thất bại.'
		};
	}
};
