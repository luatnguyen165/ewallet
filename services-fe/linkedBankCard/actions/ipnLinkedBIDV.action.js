const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const shortid = require('shortid');
const Numeral = require('numeral');
const Moment = require('moment');

// Constants
const GeneralConstant = require('../constants/general.constant');
const ResponseCode = require('../constants/code.constant');
const BankLinkConstant = require('../constants/bankLink.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	const response = {
		code: ResponseCode.IPN_FAILED,
		message: 'Liên kết thất bại.',
		extraData: null
	};
	// MODEL
	const {
		LinkedModel, HistoryModel
	} = GeneralConstant.MODEL;
	// SERVICE
	const { UUID, PAYMENT_METHOD } = GeneralConstant.SERVICE;

	try {
		// { trans_id: "123456" (mã trans id của dịch vụ trả về khi link), is_success: true (true: thành công, false: thất bại), more_info:  thông tin tài khoản/thẻ }
		const payload = _.get(ctx, 'params.body', {});
		response.extraData = JSON.stringify(payload);
		Logger(`[IPN_LINKED_BIDV] => [payload]: ${JSON.stringify(payload)}}`);
		if (!!payload.is_success === false) {
			return response;
		}
		const bidvSwiftCode = 'BIDVVNVX';
		let condition = {
			'linkedInfo.transactionId': payload.trans_id,
			'cardInfo.swiftCode': bidvSwiftCode,
			type: BankLinkConstant.LINKED_TYPE.BANKING,
			state: BankLinkConstant.LINKED_STATE.REQUIRED_VERIFY
		};
		let linkedInfo = await Broker.call(LinkedModel.findOne, [condition]);
		if (_.get(linkedInfo, 'id', false) === false) {
			Logger(`[IPN_LINKED_BIDV] => [FIND_LINKED] => ${JSON.stringify({ condition })}`);
			response.message = 'Không tìm thấy thông tin liên kết';
			return response;
		}

		condition = {
			id: linkedInfo.id
		};
		let filedUpdate = {
			'linkedInfo.cardId': _.toString(linkedInfo.accountId),
			'cardInfo.accountNumber': _.toString(payload.more_info),
			state: BankLinkConstant.LINKED_STATE.LINKED,
			linkedAt: new Date().toISOString(),
			$push: {
				supplierResponsed: JSON.stringify(payload)
			}
		};

		linkedInfo = await Broker.call(LinkedModel.findOneAndUpdate, [condition, filedUpdate, { new: true }]);
		if (_.get(linkedInfo, 'id', false) === false) {
			Logger(`[IPN_LINKED_BIDV] => [UPDATE_LINKED_ERROR]: ${JSON.stringify({ condition, filedUpdate })}`);
			response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E001).';
			return response;
		}

		condition = {
			'service.code': BankLinkConstant.LINKED_STATE.LINKED,
			'service.id': linkedInfo.id
		};
		filedUpdate = {
			'service.state': BankLinkConstant.LINKED_STATE.LINKED,
			publishedAt: Moment().utc()
		};

		const historyInfo = await Broker.call(HistoryModel.updateOne, [condition, filedUpdate]);
		if (_.get(historyInfo, 'id', false) === false) {
			Logger(`[IPN_LINKED_BIDV] => [UPDATE_HISTORY_ERROR] => ${JSON.stringify({ condition, filedUpdate })}`);
			// response.message = 'Không tìm tháy lịch sử giao dịch.';
			// return response;
		}
		const paramsRegister = {
			registerCode: `${BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK}_NAPAS_${linkedInfo.id}`,
			accountId: linkedInfo.accountId,
			group: BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK,
			title: _.get(linkedInfo, 'cardInfo.bankName', ''),
			titleLinked: 'Tài khoản liên kết',
			isActive: true,
			paymentInfo: {
				swiftCode: _.get(linkedInfo, 'cardInfo.swiftCode', 'NONE'),
				linkedId: linkedInfo.id,
				cardNumber: _.get(linkedInfo, 'cardInfo.cardNumber', '') || '',
				accountNumber: _.get(linkedInfo, 'cardInfo.accountNumber', '') || '',
				cardHolder: _.get(linkedInfo, 'cardInfo.cardHolder', '') || '',
				routeName: 'BIDV',
				bankName: _.get(linkedInfo, 'cardInfo.bankName', '')
			},
			isWithdrawable: true
		};
		let resultCreateMethod = null;
		try {
			resultCreateMethod = await Broker.call(PAYMENT_METHOD.registerPaymentMethod, { paramsRegister });
		} catch (error) {
			Logger(`[IPN_LINKED_BIDV] => [REGISTER_PAYMENT_METHOD_ERROR]: ${JSON.stringify(paramsRegister)}`);
			Logger(`[IPN_LINKED_BIDV] => [REGISTER_PAYMENT_METHOD_ERROR]: ${JSON.stringify(error.message)}`);
		}

		response.code = ResponseCode.IPN_SUCCEEDED;
		response.message = 'Liên kết thành công.';
		return response;
	} catch (error) {
		Logger(`[IPN_LINKED_BIDV] => [ERROR]: ${error}}`);
		response.message = error.message || 'Liên kết thất bại.';
		return response;
	}
};
