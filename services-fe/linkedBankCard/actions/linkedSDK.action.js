// Libraries
const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const shortid = require('shortid');
const Numeral = require('numeral');
const Moment = require('moment');
const MomentTimezone = require('moment-timezone');

// Constants
const GeneralConstant = require('../constants/general.constant');
const ResponseCode = require('../constants/code.constant');
const BankLinkConstant = require('../constants/bankLink.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	const { removeUnicode, hiddenNumber, getRecipientName } = this.Helper();

	const response = {
		code: ResponseCode.LINKED_FAILED,
		message: 'Liên kết thất bại. Vui lòng thử lại sau (E001).'
	};
	// MODEL
	const {
		LinkedModel, BankCodeModel, SettingModel, KYCModel, HistoryModel, AccountModel
	} = GeneralConstant.MODEL;
	// SERVICE
	const { UUID, PAYMENT_METHOD } = GeneralConstant.SERVICE;

	try {
		const payload = _.get(ctx, 'params.body', {});
		let authInfo = _.get(ctx, 'meta.auth.walletAuthInfo', {});

		const {
			swiftCode, amount, linkInfo: linkedData
		} = payload;

		Logger(`[LINKED_BANK_SDK] => [payload] ===> ${JSON.stringify(payload)}`);
		Logger(`[LINKED_BANK_SDK] => [credentials] ===> ${JSON.stringify(_.get(ctx, 'meta.auth.credentials', {}))}`);
		Logger(`[LINKED_BANK_SDK] => [walletAuthInfo] ===> ${JSON.stringify(authInfo)}`);

		let isCreatedLinked = true;
		let	isLinkedCreditDirect = false;

		let linkedInfo = null;
		let bankInfo = null;
		let linkedGateway = null;
		let linkedType = null;
		let version = null;

		let findCondition = null;

		let issuedAt = _.get(linkedData, 'card.issuedAt', null);
		let expiredAt = _.get(linkedData, 'card.expiredAt', null) || _.get(linkedData, 'credit.expiredAt', null);
		issuedAt = issuedAt && MomentTimezone(Moment(issuedAt, 'MM/YYYY')).tz('Asia/Ho_Chi_Minh').format('YYYY/MM');
		expiredAt = expiredAt && MomentTimezone(Moment(expiredAt, 'MM/YYYY')).tz('Asia/Ho_Chi_Minh').format('YYYY/MM');

		let paymeAccountName = _.trim(authInfo.fullname).toUpperCase();
		let cardHolderName = _.trim(removeUnicode(_.get(linkedData, 'card.cardHolder', null) || _.get(linkedData, 'credit.cardHolder', null)));

		const cardNumber = _.get(linkedData, 'card.cardNumber', null) || _.get(linkedData, 'credit.cardNumber', null);
		const accountNumber = _.get(linkedData, 'account.accountNumber', '');

		const description = 'Liên kết thẻ vào ví PayME.';
		const billInfo = {};

		const tags = [];
		const linkedMethod = _.toUpper(_.keys(payload.linkInfo));
		let hiddenCard = null;

		if (linkedMethod === BankLinkConstant.LINKED_METHOD.CREDIT) {
		// 	Thẻ credit nè
			linkedType = BankLinkConstant.LINKED_TYPE.CREDIT_CARD;
			linkedGateway = BankLinkConstant.LINKED_GATEWAY.PG;
			tags.push(BankLinkConstant.LINKED_TAG.LINKED_CREDIT);
			const creditInfo = _.get(linkedData, 'credit', {});

			if (!!authInfo.accountId === false) {
			// Webview gọi tới nên không có auth đâu nha => Call action thanh toán kèm kèm xử lý liên kết nếu có.
				isCreatedLinked = false;
				isCreatedLinked = false;
				let decodeBase64Trans = payload.token;
				if (!!decodeBase64Trans === false) {
					response.message = 'Liên kết thất bại. Mã giao dịch không tồn tại (E001).';
					return response;
				}

				try {
					decodeBase64Trans = Buffer.from(decodeBase64Trans, 'base64').toString();
					decodeBase64Trans = JSON.parse(decodeBase64Trans);
				} catch (error) {
					Logger(`[BANK_LINK] => [DECODE_BASE64_TRANS_ERROR]: ${JSON.stringify({ transaction: creditInfo.transaction })}`);
					response.message = 'Liên kết thất bại. Mã giao dịch không tồn tại (E001).';
					return response;
				}
				Logger(`[BANK_LINK] => [decodeBase64Trans]: ${JSON.stringify(decodeBase64Trans)}`);
				findCondition = {
					id: decodeBase64Trans.accountId
				};
				const accountInfo = await Broker.call(AccountModel.findOne, [findCondition]);
				if (_.get(accountInfo, 'id', false) === false) {
					response.message = 'Không tìm thấy thông tin tài khoản Ví (E001).';
					return response;
				}
				authInfo = accountInfo;
				authInfo.accountId = accountInfo.id;
				paymeAccountName = _.trim(removeUnicode(authInfo.fullname)).toUpperCase();

				findCondition = {
					id: decodeBase64Trans.linkedId,
					accountId: accountInfo.id,
					transaction: decodeBase64Trans.transaction
				};

				linkedInfo = await Broker.call(LinkedModel.findOne, [findCondition]);
				if (_.get(linkedInfo, 'id', false) === false) {
					Logger(`[LINKED_BANK_SDK] => [findCondition]: ${JSON.stringify({ findCondition })}`);
					response.message = 'Liên kết thất bại. Không tìm thấy thông tin liên kết (E001).';
					return response;
				}

				if (linkedInfo.state === BankLinkConstant.LINKED_STATE.LINKED) {
					response.message = `Tài khoản của bạn đã được liên kết với thẻ ${_.get(linkedInfo, 'cardInfo.cardNumber', null)}.`;
					return response;
				}

				if (linkedInfo.state !== BankLinkConstant.LINKED_STATE.REQUIRED_VERIFY) {
					response.message = 'Liên kết thất bại. Không tìm thấy thông tin liên kết (E002).';
					return response;
				}
			} else {
			// Call từ API đến => Call CTT tạo order trước rồi tính tiếp :)))
				if (_.isEmpty(creditInfo) === false) {
					// liên kết thẻ tín dụng theo đường trực tiếp => truyền thông tin thẻ qua đường API
					isLinkedCreditDirect = true;
					// throw new MoleculerError('Liên kết thẻ tin dụng trực tiếp đang được phát triển. Vui lòng thử lại sau.', ResponseCode.LINKED_FAILED);
				} else {
					// liên kết thẻ tín dụng thông qua Webview => User sẽ nhập thẻ trên UI của Webview
				}

				if (_.isNil(amount) === true) {
					response.message = 'Liên kết thất bại. Số tiền xác thực không được để trống.';
					return response;
				}
			}

			if (isCreatedLinked === false || isLinkedCreditDirect === true) {
				if (!!cardNumber === false) {
					response.message = 'Liên kết thất bại. Số thẻ không được để trống.';
					return response;
				}

				if (!!expiredAt === false) {
					response.message = 'Liên kết thất bại. Ngày hết hạn thẻ không được để trống.';
					return response;
				}

				if (!!creditInfo.cvv === false) {
					response.message = 'Liên kết thất bại. Mã bảo mật thẻ để trống.';
					return response;
				}

				if (!!cardHolderName === false) {
					response.message = 'Liên kết thất bại. Tên chủ thẻ không được để trống.';
					return response;
				}

				// Ktra 1 thẻ nhiều user đã liên kết hay không không
				hiddenCard = hiddenNumber(cardNumber, 'CARD');
				findCondition = {
					'cardInfo.cardNumber': hiddenCard,
					state: BankLinkConstant.LINKED_STATE.LINKED,
					type: BankLinkConstant.LINKED_TYPE.CREDIT_CARD
				};

				const checkCreditLinked = await Broker.call(LinkedModel.findOne, [findCondition]);
				if (_.get(checkCreditLinked, 'id', false) !== false) {
					response.message = `Số thẻ ${hiddenCard} đã được liên kết với tài khoản khác.`;
					return response;
				}
			}
		} else {
			// Trường hợp liên kết sài thẻ nội địa
			if (_.isEmpty(authInfo) === true) {
				response.message = 'Không tìm thấy thông tin tài khoản Ví PayME.';
				return response;
			}

			if (_.isEmpty(swiftCode) === true) {
				response.message = 'Thiểu thông swiftCode ngân hàng liên kết.';
				return response;
			}

			findCondition = {
				accountId: authInfo.accountId,
				'cardInfo.swiftCode': swiftCode,
				state: BankLinkConstant.LINKED_STATE.LINKED
			};
			linkedInfo = await Broker.call(LinkedModel.findOne, [findCondition]);

			if (_.get(linkedInfo, 'id', false) !== false) {
				const linkedCardNumber = _.get(linkedInfo, 'cardInfo.cardNumber', false);
				const linkedAccountNumber = _.get(linkedInfo, 'cardInfo.accountNumber', false);
				if (!!cardNumber === true) response.message = `Tài khoản của bạn đã được liên kết ngân hàng này, số thẻ ${hiddenNumber(linkedCardNumber, 'CARD')}.`;
				else response.message = `Tài khoản của bạn đã được liên kết ngân hàng này, số tài khoản ${hiddenNumber(linkedAccountNumber, 'ACCOUNT')}.`;
				return response;
			}

			// Lấy danh sách ngân hàng
			const listBankSupport = await Broker.call(BankCodeModel.find, [{ isActive: true }]);

			// danh sach swiftCode ngân hàng chạy riêng không qua cổng NAPAS
			const listBankDirect = _.compact(listBankSupport.map((item) => (item.link.gateway !== 'NAPAS' ? item.swiftCode : null)));
			this.logger.info(`[DEBUG]: ${JSON.stringify({ swiftCode, listBankDirect })}`);
			const linkedBankInfo = _.find(listBankSupport, { swiftCode });
			if (_.get(linkedBankInfo, 'id', false) === false) {
				response.message = 'Ngân hàng liên kết chưa được hỗ trợ. Vui lòng thử lại sau.';
				return response;
			}

			// Lấy cổng chạy của các kênh theo config db - Kiểm tra điều kiện mở rộng chạy qua các kênh (nếu có)
			if (_.trim(paymeAccountName) !== 'GB NAME 1-803585' || (cardHolderName && cardHolderName !== 'GB NAME 1-803585')) {
				cardHolderName = _.trim(removeUnicode(cardHolderName)).toUpperCase();
				paymeAccountName = _.trim(removeUnicode(authInfo.fullname)).toUpperCase();
			}

			linkedGateway = _.get(linkedBankInfo, 'link.gateway', BankLinkConstant.LINKED_GATEWAY.NAPAS);
			switch (linkedGateway) {
				case BankLinkConstant.LINKED_GATEWAY.PVCBANK: {
					if (linkedMethod === BankLinkConstant.LINKED_METHOD.CARD) {
						// card number
						if (_.isEmpty(cardNumber) === true) {
							response.message = 'Liên kết ngân hàng PVComBank thất bại. Thiếu thông tin số thẻ.';
							return response;
						}
						if (_.isEmpty(issuedAt) === true) {
							response.message = 'Liên kết ngân hàng PVComBank thất bại. Thiếu thông tin ngày phát hành thẻ.';
							return response;
						}
					} else {
						// account bank
						response.message = `Ngân hàng ${linkedBankInfo.shortName} chưa hỗ trợ phương thức liên kết bằng số tài khoản ngân hàng.`;
						return response;
					}
					linkedType = BankLinkConstant.LINKED_TYPE.BANKING;
					tags.push(BankLinkConstant.LINKED_TAG.LINKED_PVCBANK);
					break;
				}
				case BankLinkConstant.LINKED_GATEWAY.VIETINBANK: {
					if (linkedMethod === BankLinkConstant.LINKED_METHOD.CARD) {
						// card number
						if (_.isEmpty(cardNumber) === true) {
							response.message = 'Liên kết ngân hàng VietinBank thất bại. Thiếu thông tin số thẻ.';
							return response;
						}
						if (_.isEmpty(issuedAt) === true) {
							response.message = 'Liên kết ngân hàng VietinBank thất bại. Thiếu thông tin ngày phát hành thẻ.';
							return response;
						}
					} else {
						// account bank
						response.message = `Ngân hàng ${linkedBankInfo.shortName} chưa hỗ trợ phương thức liên kết bằng số tài khoản ngân hàng.`;
						return response;
					}
					linkedType = BankLinkConstant.LINKED_TYPE.BANKING;
					tags.push(BankLinkConstant.LINKED_TAG.LINKED_VIETINBANK);
					break;
				}
				case BankLinkConstant.LINKED_GATEWAY.OCB: {
				// Cổng OCB
					const kycInfo = authInfo.kyc;
					if (_.isNull(kycInfo.kycId)) {
						response.message = 'Không tìm thấy thông tin định danh. Vui lòng liên hệ CSKH!';
						return response;
					}
					const kycDetails = await Broker.call(KYCModel.findOne, [{ id: kycInfo.kycId }]);
					if (_.get(kycDetails, 'id', null) === null) {
						response.message = 'Không tìm thấy thông tin định danh. Vui lòng liên hệ CSKH!';
						return response;
					}
					if (_.get(kycDetails, 'type', '') !== 'CMND') {
						response.message = 'Liên kết OCB cần thông tin số CMND để liên kết. Vui lòng xác thực định danh bằng số CMND. Xin trân trọng cảm ơn.';
						return response;
					}

					if (linkedMethod === BankLinkConstant.LINKED_METHOD.CARD && _.isEmpty(cardNumber) === true) {
						response.message = 'Liên kết ngân hàng OCB thất bại. Thiếu thông tin số thẻ.';
						return response;
					}
					if (linkedMethod === BankLinkConstant.LINKED_METHOD.ACCOUNT && _.isEmpty(accountNumber) === true) {
						response.message = 'Liên kết ngân hàng BIDV thất bại. Thiếu thông tin số tài khoản.';
						return response;
					}

					linkedType = linkedMethod === BankLinkConstant.LINKED_METHOD.CARD ? BankLinkConstant.LINKED_TYPE.BANKING : BankLinkConstant.LINKED_TYPE.ACCOUNT;
					tags.push(BankLinkConstant.LINKED_TAG.LINKED_OCBBANK);
					break;
				}
				case BankLinkConstant.LINKED_GATEWAY.BIDV: {
				// Liên kêt chạy cổng BIDV - Lấy trong config trong setting đê biets chạy version nào
					const checkIdentifyNumber = _.get(authInfo, 'kyc.identifyNumber', '');
					if (!!checkIdentifyNumber === false) {
						response.message = 'Không tìm thấy thông tin định danh. Vui lòng liên hệ CSKH!';
						return response;
					}

					let configBIDV = _.get(
						await Broker.call(SettingModel.findOne, [{ key: 'linked.bidv' }]),
						'value',
						JSON.stringify({ version: 2 })
					);
					try {
						configBIDV = JSON.parse(configBIDV);
						version = _.get(configBIDV, 'version', 2);
					} catch (error) {
						version = 2;
					}

					if (version === 2) {
						if (linkedMethod === BankLinkConstant.LINKED_METHOD.CARD && _.isEmpty(cardNumber) === true) {
							response.message = 'Liên kết ngân hàng BIDV thất bại. Thiếu thông tin số thẻ.';
							return response;
						}
						if (linkedMethod === BankLinkConstant.LINKED_METHOD.ACCOUNT && _.isEmpty(accountNumber) === true) {
							response.message = 'Liên kết ngân hàng BIDV thất bại. Thiếu thông tin số tài khoản.';
							return response;
						}
						linkedType = linkedMethod === BankLinkConstant.LINKED_METHOD.CARD ? BankLinkConstant.LINKED_TYPE.BANKING : BankLinkConstant.LINKED_TYPE.ACCOUNT;
					} else {
						bankInfo = {
							swiftCode: BankLinkConstant.SWIFT_CODE.BIDV,
							name: 'BIDV'
						};
						linkedType = BankLinkConstant.LINKED_TYPE.BANKING;
					}
					tags.push(BankLinkConstant.LINKED_TAG.LINKED_BIDVBANK);
					break;
				}
				default: {
					// Liên kêt chạy Cổng Napas

					if (linkedMethod === BankLinkConstant.LINKED_METHOD.ACCOUNT) {
						// account bank
						response.message = `Ngân hàng ${linkedBankInfo.shortName} chưa hỗ trợ phương thức liên kết bằng số tài khoản qua cổng Napas.`;
						return response;
					}
					// card number
					if (_.isEmpty(cardNumber) === true) {
						response.message = `Liên kết ngân hàng ${linkedBankInfo.shortName} thất bại. Thiếu thông tin số thẻ.`;
						return response;
					}

					if (_.isEmpty(cardHolderName) === true) {
						response.message = `Liên kết ngân hàng ${linkedBankInfo.shortName} thất bại. Thiếu thông tin tên chủ thẻ.`;
						return response;
					}

					if (_.isEmpty(issuedAt) === true) {
						response.message = `Liên kết ngân hàng ${linkedBankInfo.shortName} thất bại. Thiếu thông tin ngày phát hành thẻ.`;
						return response;
					}

					if (_.isNil(amount) === true) {
						response.message = `Liên kết ngân hàng ${linkedBankInfo.shortName} thất bại. Thiếu thông tin số tiền nạp để xác thực giao dịch.`;
						return response;
					}

					// lấy amount trong setting trường hợp chạy napas
					let limitAmountConfig = _.get(
						await Broker.call(SettingModel.findOne, [{ key: 'limit.param.amount.deposit' }]),
						'value',
						JSON.stringify({ min: 20000, money: 100000000 })
					);
					try {
						limitAmountConfig = JSON.parse(limitAmountConfig);
					} catch (error) {
						limitAmountConfig = { min: 20000, money: 100000000 };
					}

					if (amount < limitAmountConfig.min) {
						response.message = `Số tiền nạp không được nhỏ hơn ${Numeral(limitAmountConfig.min).format('0,0')}đ`;
						return response;
					}
					if (amount > limitAmountConfig.max) {
						response.message = `Số tiền nạp không vượt quá ${Numeral(limitAmountConfig.max).format('0,0')}đ`;
						return response;
					}

					// Lấy tên chủ the liên kết.
					const paramsRequest = {
						swiftCode,
						type: 'CARD',
						number: cardNumber
					};

					const resultRecipientName = await getRecipientName(paramsRequest);
					this.logger.info(`[DEBUG] Get Recepient SDK::: ${JSON.stringify(resultRecipientName)}`);
					if (!!resultRecipientName === false) {
						Logger(`[LINKED_BANK_SDK] => [Get_Name_Recipient_Error]: ${JSON.stringify({ paramsRequest, resultRecipientName })}`);
						response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E003)';
						return response;
					}
					cardHolderName = _.trim(removeUnicode(cardHolderName)).toUpperCase();

					linkedType = BankLinkConstant.LINKED_TYPE.ATM;
					tags.push(BankLinkConstant.LINKED_TAG.LINKED_NAPAS);
					break;
				}
			}

			// Lấy danh sách ngân hàng
			// BIDV chạy v2 - swiftCode không phải BIDV - BIDV chạy cổng NAPAS thì lao vào đoạn code If ở dưới.
			// BIDV v1 sẽ mở webview để nhập số thẻ, v2 thì sẽ nhập số thẻ trên UI client;
			if (version === 2 || swiftCode !== BankLinkConstant.SWIFT_CODE.BIDV || _.includes(listBankDirect, BankLinkConstant.SWIFT_CODE.BIDV) === false) {
				const checkLinkedCondition = {
					'cardInfo.swiftCode': swiftCode,
					state: BankLinkConstant.LINKED_STATE.LINKED
				};
				if (linkedType === BankLinkConstant.LINKED_TYPE_SUPPLIER.ACCOUNT) {
					checkLinkedCondition['cardInfo.accountNumber'] = accountNumber;
				} else {
					checkLinkedCondition['cardInfo.cardNumber'] = cardNumber;
				}

				Logger(`[LINKED_BANK_SDK] => [checkLinkedInfo]: ${JSON.stringify(checkLinkedCondition)}`);
				const checkLinkedInfo = await Broker.call(LinkedModel.findOne, [checkLinkedCondition]);
				if (_.get(checkLinkedInfo, 'id', false) !== false) {
					response.message = 'Số thẻ này này đã được liên kết với tài khoản Ví khác.';
					return response;
				}

				if (linkedMethod === BankLinkConstant.LINKED_METHOD.CARD) {
					let prefix = cardNumber;
					if (prefix === '') {
						response.message = 'Không tìm thấy số thẻ. Vui lòng kiểm tra lại thông tin';
						return response;
					}
					prefix = prefix.substr(0, 6);
					const findBankLinked = _.find(listBankSupport, (item) => _.get(item, 'card.atm.prefix', '') === prefix);
					if (_.isEmpty(findBankLinked) === true) {
						response.message = 'Ngân hàng chưa được hổ trợ. Vui lòng thử lại sau.';
						return response;
					}
					bankInfo = {
						cardNumber: _.get(linkedData, 'card.cardNumber', null),
						swiftCode: findBankLinked.swiftCode,
						name: findBankLinked.shortName
					};
				}
			}
		}

		// Kiểm tra tài khoản liên kết phải trùng vs tên Ví.
		if ((linkedMethod === BankLinkConstant.LINKED_METHOD.CARD
			|| (linkedMethod === BankLinkConstant.LINKED_METHOD.CREDIT && isCreatedLinked === false))
			&& paymeAccountName !== cardHolderName) {
			const splitToString1 = paymeAccountName.split(' ');
			const splitToString2 = cardHolderName.split(' ');

			let isShortCheck = false;
			if (splitToString1 && splitToString2 && splitToString2.length >= 2 && splitToString1.length >= 2) {
				if (splitToString2[0] === splitToString1[0] && splitToString2[splitToString2.length - 1] === splitToString1[splitToString1.length - 1]) {
					isShortCheck = true;
				}
			}
			if (isShortCheck === false) {
				const startNumCheckPayME = splitToString1.length - 2;
				const startNumCheckCardHolder = splitToString2.length - 2;
				if ((splitToString1[startNumCheckPayME + 1] !== splitToString2[startNumCheckCardHolder + 1])
						|| (splitToString1[startNumCheckPayME] !== splitToString2[startNumCheckCardHolder])) {
					response.message = 'Tên chủ thẻ phải trùng tên ví';
					return response;
				}
			}
		}

		// Tạo data trước khi call liên kết (credit - napas) => Dùng chung khúc này hishi :)))
		let transaction = null;
		let linkedId = null;

		let linkedCreated = linkedInfo;
		let historyCreated = null;

		if (isCreatedLinked === true) {
			try {
				transaction = await Broker.call(UUID.pick, { prefix: 'LINKED' });
				linkedId = await Broker.call(UUID.pick, { prefix: 'LINKED' });
			} catch (error) {
				Logger(`[LINKED_BANK_SDK] => [UUID] => [ERROR]: ${error}`);
				response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E004)';
				return response;
			}

			if (!!transaction === false || !!linkedId === false) {
				Logger(`[LINKED_BANK_SDK] => [GET_UUID_ERROR]: ${JSON.stringify({ transaction, linkedId })}`);
				response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E005)';
				return response;
			}

			const dataLinked = {
				id: linkedId,
				appId: authInfo.appId,
				accountId: authInfo.accountId,
				transaction,
				requestId: shortid.generate(),
				cardInfo: {
					bankCode: linkedGateway,
					bankName: _.get(bankInfo, 'name', linkedGateway),
					swiftCode,
					accountNumber: _.get(linkedData, 'account.accountNumber', null),
					cardHolder: cardHolderName || paymeAccountName,
					cardNumber: _.get(linkedData, 'card.cardNumber', null),
					issuedAt,
					expiredAt,
					version

				},
				type: linkedType,
				state: BankLinkConstant.LINKED_STATE.NEW
			};

			try {
				linkedCreated = await Broker.call(LinkedModel.create, [dataLinked]);
			} catch (error) {
				Logger(`[LINKED_BANK_SDK] => [CREATE_LINKED_ERROR_2]: ${error}`);
				Logger(`[LINKED_BANK_SDK] => [CREATE_LINKED_ERROR_1]: ${JSON.stringify(dataLinked)}`);
				response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E006)';
				return response;
			}
			if (_.get(linkedCreated, 'id', false) === false) {
				Logger(`[LINKED_BANK_SDK] => [CREATE_LINKED_ERROR]: ${JSON.stringify(linkedCreated)}`);
				response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E007)';
				return response;
			}

			const dataHistory = {
				accountId: linkedCreated.accountId,
				appId: authInfo.appId,
				service: {
					id: linkedCreated.id,
					name: 'Liên kết thẻ vào Ví PayME',
					type: BankLinkConstant.HISTORY_SERVICE.LINKED,
					transaction: linkedCreated.transaction,
					state: BankLinkConstant.LINKED_PAYMENT_STATE.PENDING
				},
				amount: _.get(payload, 'amount', 0) || 0,
				total: _.get(payload, 'amount', 0) || 0,
				fee: 0,
				state: BankLinkConstant.LINKED_PAYMENT_STATE.PENDING,
				description,
				tags: [BankLinkConstant.LINKED_TAG.LINKED, ...tags]
			};

			try {
				historyCreated = await Broker.call(HistoryModel.create, [dataHistory]);
			} catch (error) {
				Logger(`[LINKED_BANK_SDK] => [CREATE_HISTORY_ERROR_1] ===> ${JSON.stringify(dataHistory)}`);
				Logger(`[LINKED_BANK_SDK] => [CREATE_HISTORY_ERROR_2] ===> ${error}`);
				response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E008)';
				return response;
			}

			if (_.get(historyCreated, 'id', false) === false) {
				Logger(`[LINKED_BANK_SDK] => [CREATE_HISTORY_ERROR] ===> ${JSON.stringify(dataHistory)}`);
				response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E009)';
				return response;
			}
		}
		// Dành cho Credit Card Liên kết trực tiếp
		if (isLinkedCreditDirect && isCreatedLinked === true) {
			const fieldUpdate = {
				'cardInfo.cardNumber': hiddenCard,
				'cardInfo.expiredAt': expiredAt

			};
			linkedInfo = await Broker.call(LinkedModel.findOneAndUpdate, [{ id: linkedCreated.id }, fieldUpdate, { new: true }]);
			if (_.get(linkedInfo, 'id', false) === false) {
				response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E002).';
				return response;
			}
			// Tạo billInfo liên két
			const kycInfo = await Broker.call(KYCModel.findOne, [{ accountId: linkedInfo.accountId || authInfo.accountId }]);
			if (_.get(kycInfo, 'id', false) !== false) {
				let firstName = '';
				let lastName = '';
				const arrName = _.split(kycInfo.fullname, ' ');
				if (_.isEmpty(arrName) === false) {
					firstName = arrName.pop();
					lastName = arrName.join(' ');
					lastName = _.isEmpty(lastName) ? firstName : lastName;
				}
				billInfo.country = 'VN';
				if (_.isEmpty(firstName) === false) billInfo.firstName = firstName;
				if (_.isEmpty(lastName) === false) billInfo.lastName = lastName;

				let address = _.get(kycInfo, 'address.street', null);
				if (_.isEmpty(address) === true) address = _.get(kycInfo, 'address', null);
				if (_.isEmpty(address) === false && _.isString(address) === true) billInfo.address = address;

				const ward = _.get(kycInfo, 'address.ward.title', null);
				const district = _.get(kycInfo, 'address.district.title', null);
				if (!!ward && !!district) {
					billInfo.locality = `${ward} - ${district}`;
				} else if (ward) {
					billInfo.locality = ward;
				} else if (district) {
					billInfo.locality = district;
				}

				const city = _.get(kycInfo, 'address.city.title', null);
				if (city) {
					billInfo.administrativeArea = city;
					if (city.includes('Thành phố')) billInfo.administrativeArea = _.trim(_.replace(city, 'Thành phố', ''));
					if (city.includes('Tỉnh')) billInfo.administrativeArea = _.trim(_.replace(city, 'Tỉnh', ''));
				}

				if (_.isEmpty(kycInfo.email) === false) billInfo.email = kycInfo.email;
				if (_.isEmpty(authInfo.phone) === false) billInfo.phone = authInfo.phone;
			}
		}

		let supplier = null;
		let paramsRequest = null;
		let linkedResponsed = null;
		switch (linkedGateway) {
			case BankLinkConstant.LINKED_GATEWAY.PVCBANK: {
				supplier = BankLinkConstant.LINKED_SUPPLIER.PVCOMBANK;
				paramsRequest = {
					transaction,
					requestId: linkedCreated.requestId,
					cardNumber: _.trim(payload.linkInfo.card.cardNumber),
					cardHolder: cardHolderName,
					issuedAt,
					expiredAt: !!expiredAt === true ? expiredAt : '',
					accountId: authInfo.accountId
				};
				linkedResponsed = await this.PVComBankLibs.Linked(paramsRequest);
				break;
			}
			case BankLinkConstant.LINKED_GATEWAY.VIETINBANK: {
				supplier = BankLinkConstant.LINKED_SUPPLIER.VIETINBANK;
				paramsRequest = {
					transaction,
					accountId: authInfo.accountId,
					cardNumber,
					cardHolder: cardHolderName,
					issuedAt,
					phone: _.replace(authInfo.phone, '84', '0'),
					customerId: authInfo.kyc.identifyNumber
				};
				linkedResponsed = await this.VietinBankLibs.Linked(paramsRequest);
				break;
			}
			case BankLinkConstant.LINKED_GATEWAY.OCB: {
				supplier = BankLinkConstant.LINKED_SUPPLIER.OCBBANK;
				paramsRequest = {
					fullname: paymeAccountName,
					phone: _.replace(authInfo.phone, '84', '0'),
					identifyNumber: authInfo.kyc.identifyNumber
				};

				if (linkedMethod === BankLinkConstant.LINKED_METHOD.CARD) {
				// card number
					paramsRequest.type = 'CARD';
					paramsRequest.cardNumber = cardNumber;
				} else {
				// account bank
					paramsRequest.type = 'ACCOUNT';
					paramsRequest.accountNumber = accountNumber;
				}
				linkedResponsed = await this.OCBBankLibs.Linked(paramsRequest);
				break;
			}
			case BankLinkConstant.LINKED_GATEWAY.BIDV: {
				supplier = BankLinkConstant.LINKED_SUPPLIER.BIDVBANK;
				paramsRequest = {
					transaction,
					accountId: authInfo.accountId,
					fullname: paymeAccountName,
					redirectUrl: payload.redirectUrl,
					phone: _.replace(authInfo.phone, '84', '0'),
					identifyNumber: authInfo.kyc.identifyNumber,
					version
				};

				if (version === 2) {
					if (linkedMethod === BankLinkConstant.LINKED_METHOD.CARD) {
						paramsRequest.payerDebit = cardNumber;
					} else {
						paramsRequest.payerDebit = accountNumber;
					}
				}
				linkedResponsed = await this.BIDVBankLibs.Linked(paramsRequest);
				break;
			}
			case BankLinkConstant.LINKED_GATEWAY.PG: {
				supplier = BankLinkConstant.LINKED_SUPPLIER.PG;
				if (isLinkedCreditDirect === false) {
					// Đường liên kết mở webview của để tiến hành authen và liên kết.
					if (isCreatedLinked === true) {
						// Call CTT tạo order => trả webview cho client mở
						paramsRequest = {
							linkedId: linkedCreated.id,
							requestId: linkedCreated.requestId,
							amount,
							transaction,
							authInfo,
							accountId: process.env.PG_WALLET_ACCOUNT_ID,
							redirectUrl: payload.redirectUrl
						};
						linkedResponsed = await this.PGLibs.CreateLinkedOrder(paramsRequest);
					} else {
						// Thực hiện liên kết từ webview trả ở trên
						paramsRequest = {
							transaction: _.get(linkedCreated, 'linkedInfo.transactionId', null),
							redirectUrl: _.get(linkedCreated, 'linkedInfo.redirectUrl', 'https://payme.vn/web'),
							cardNumber,
							cardHolder: cardHolderName,
							cvv: _.get(linkedData, 'credit.cvv', ''),
							expiredAt: MomentTimezone(Moment(expiredAt, 'YYYY/MM')).tz('Asia/Ho_Chi_Minh').format('MM/YY'),
							referenceId: payload.referenceId,
							billInfo
						};
						linkedResponsed = await this.PGLibs.Linked(paramsRequest);
						_.set(paramsRequest, 'cardNumber', hiddenNumber(paramsRequest.cardNumber, 'CARD'));
						_.set(paramsRequest, 'cvv', 'XXX');
					}
				} else {
					// Đường liên kết client tự authen và call api liên kết. (Client tự lực cánh sinh)
					paramsRequest = {
						linkedId: linkedCreated.id,
						accountId: process.env.PG_WALLET_ACCOUNT_ID,
						amount: payload.amount || 1000,
						authInfo,
						requestId: linkedCreated.requestId,
						transaction: linkedCreated.transaction,
						cardNumber,
						cardHolder: cardHolderName,
						cvv: _.get(linkedData, 'credit.cvv', ''),
						expiredAt: MomentTimezone(Moment(expiredAt, 'YYYY/MM')).tz('Asia/Ho_Chi_Minh').format('MM/YY'),
						referenceId: _.get(linkedData, 'credit.referenceId', '') || '',
						billInfo,
						redirectUrl: payload.redirectUrl || 'https://payme.vn/web'
					};
					linkedResponsed = await this.PGLibs.LinkedDirect(paramsRequest);
					_.set(paramsRequest, 'cardNumber', hiddenNumber(paramsRequest.cardNumber, 'CARD'));
					_.set(paramsRequest, 'cvv', 'XXX');

					// throw new MoleculerError('Tính năng đang được phát triển. Vui lòng thử lại sau.', ResponseCode.LINKED_FAILED);
				}
				break;
			}
			default: {
			// Cổng Napas
				supplier = BankLinkConstant.LINKED_SUPPLIER.NAPAS_GATEWAY;
				// Note:  Tính phí nạp khi liên kết => viết action làm sau
				paramsRequest = {
					cardNumber,
					cardHolder: cardHolderName,
					issuedAt: MomentTimezone(Moment(issuedAt, 'YYYY/MM')).tz('Asia/Ho_Chi_Minh').format('MM/YY'),
					expiredAt,
					accountId: authInfo.accountId,
					amount,
					clientIp: '',
					clientId: payload.clientId,
					transaction: _.toString(transaction),
					envName: payload.envName ? payload.envName : 'MobileApp',
					redirectUrl: payload.redirectUrl
				};
				linkedResponsed = await this.NapasLibs.Linked(paramsRequest);
				break;
			}
		}
		// Response:  code - message - state - transaction - html
		Logger(`[LINKED_BANK_SDK] => [${linkedGateway}] => [paramsRequest] ===> ${JSON.stringify(paramsRequest)}`);
		Logger(`[LINKED_BANK_SDK] => [${linkedGateway}] => [linkedResponsed] ===> ${JSON.stringify({ ...linkedResponsed, html: 'Đã ẩn HTML vì dài quá' })}`);

		switch (linkedResponsed.code) {
			case ResponseCode.LINKED_SUCCESSED: {
				// Liên kết thành công => Credit 2ds sẽ linked thành công luôn nhé hihi :>))
				response.code = linkedResponsed.code;
				response.message = linkedResponsed.message || 'Liên kết thành công.';
				response.data = {
					linkedId: linkedCreated.id
				};
				break;
			}
			case ResponseCode.LINKED_REQUIRED_VERIFY:
			case ResponseCode.LINKED_REQUIRE_OTP: {
				// Liên kết chờ OTP ngân hàng hoặc form của bank
				const updateCondition = { id: linkedCreated.id };
				const updateData = {
					linkedInfo: {
						transactionId: linkedResponsed.transaction,
						paymentId: linkedResponsed.paymentId
					},
					state: linkedResponsed.state,
					$push: {
						supplierResponsed: linkedResponsed.supplierResponse
					},
					supplier
				};
				if (isCreatedLinked === true) _.set(updateData, 'linkedInfo.redirectUrl', payload.redirectUrl);
				const updateResult = await Broker.call(LinkedModel.updateOne, [updateCondition, updateData]);
				if (!_.isObject(updateResult) || updateResult.nModified < 1) {
					Logger(`[LINKED_BANK_SDK] => [UPDATE_LINKED_ERROR] ===> ${JSON.stringify({ updateCondition, updateData })}`);
					response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E011)';
					return response;
				}

				response.code = linkedResponsed.code;
				response.message = linkedResponsed.message;
				response.data = {
					linkedId: linkedCreated.id,
					transaction: linkedCreated.transaction,
					html: _.get(linkedResponsed, 'html', null)
				};
				break;
			}
			default: {
				response.message = linkedResponsed.message || 'Liên kết thất bại. Vui lòng thử lại sau (E012)';
				const updateResult = await Broker.call(LinkedModel.updateOne, [
					{ id: linkedCreated.id },
					{
						state: linkedResponsed.state,
						supplier,
						$push: {
							supplierResponsed: linkedResponsed.supplierResponse
						}
					}]);
				break;
			}
		}

		return response;
	} catch (error) {
		if (error.name === 'MoleculerError') {
			response.code = error.code;
			response.message = error.message;
		}
		Logger(`[LINKED_BANK_SDK] => [ERROR] ===> ${error}`);
		return response;
	}
};
