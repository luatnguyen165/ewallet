// Libraries
const _ = require('lodash');

// Constants
const GeneralConstant = require('../constants/general.constant');
const ResponseCode = require('../constants/code.constant');
const BankLinkConstant = require('../constants/bankLink.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	const { comparePassword } = this.PasswordHelper();
	const { removeUnicode, hiddenCardNumber } = this.Helper();
	const RedisCache = this.RedisCache();
	const response = {
		code: ResponseCode.UNLINKED_FAILED,
		message: 'Hủy liên kết thất bai. Vui lòng thử lại sau.'
	};
	// MODEL
	const {
		LinkedModel, HistoryModel, AccountModel
	} = GeneralConstant.MODEL;
	// SERVICE
	const { UUID, PAYMENT_METHOD } = GeneralConstant.SERVICE;

	try {
		const payload = _.get(ctx, 'params.body', {});
		const authInfo = _.get(ctx, 'meta.auth.walletAuthInfo', {});

		const {
			linkedId, password, version, clientId
		} = payload;

		Logger(`[UNLINKED_BANK_SDK] => [payload] ===> ${JSON.stringify(payload)}`);
		Logger(`[UNLINKED_BANK_SDK] => [credentials] ===> ${JSON.stringify(_.get(ctx, 'meta.auth.credentials', {}))}`);
		Logger(`[UNLINKED_BANK_SDK] => [walletAuthInfo] ===> ${JSON.stringify(authInfo)}`);

		if (password && authInfo.accountId) {
			const accountInfo = await this.broker.call(AccountModel.findOne, [{ id: authInfo.accountId }]);
			if (_.get(accountInfo, 'id', false) === false) {
				response.message = 'Không tìm tháy thông tin tài khoản Ví';
				return response;
			}

			const RedisWrongPass = await this.RedisCheckUnlink({ accountId: accountInfo.id });

			const resultCheck = await RedisWrongPass.Check();
			if (resultCheck.successed !== true) {
				if (resultCheck.state === 'LOCKED') response.code = ResponseCode.UNLINKED_ACCOUNT_LOCKED;
				else if (resultCheck.state === 'TEMP_LOCKED') response.code = ResponseCode.UNLINKED_ACCOUNT_TEMP_LOCKED;

				response.remainingTime = resultCheck.ttl;
				response.message = resultCheck.message;
				return response;
			}

			if (comparePassword(password, accountInfo.password) === false) {
				const resultLock = await RedisWrongPass.Locked();
				if (resultCheck.successed !== true) {
					if (resultCheck.state === 'LOCKED') response.code = ResponseCode.UNLINKED_ACCOUNT_LOCKED;
					else if (resultCheck.state === 'TEMP_LOCKED') response.code = ResponseCode.UNLINKED_ACCOUNT_TEMP_LOCKED;

					response.remainingTime = resultCheck.ttl;
					response.message = resultCheck.message;
					return response;
				}
				response.code = ResponseCode.UNLINKED_ACCOUNT_PASSWORD_NOT_MATCH;
				response.message = `PayME PIN chưa chính xác.\n Lưu ý: nhập sai ${resultLock.lockTimes} lần nữa tài khoản của bạn sẽ bị khóa.`;

				return response;
			}
			await RedisWrongPass.Delete();
		}

		const findCondition = {
			id: linkedId,
			accountId: authInfo.accountId,
			state: BankLinkConstant.LINKED_STATE.LINKED
		};

		const linkedInfo = await Broker.call(LinkedModel.findOne, [findCondition]);
		if (_.get(linkedInfo, 'id', false) === false) {
			Logger(`[UNLINKED_BANK_SDK] => [findCondition]: ${JSON.stringify({ findCondition })}`);
			response.message = 'Không tìm thấy thông tin liên kết. Vui lòng kiểm tra lại.';
			return response;
		}

		const linkedSupplier = linkedInfo.supplier;

		let resultRequest = null;
		let paramsRequest = null;
		switch (linkedSupplier) {
			case BankLinkConstant.LINKED_SUPPLIER.PVCOMBANK: {
				paramsRequest = {
					accountId: authInfo.accountId,
					cardId: linkedInfo.linkedInfo.cardId
				};
				resultRequest = await this.PVComBankLibs.UnLinked(paramsRequest);
				break;
			}
			case BankLinkConstant.LINKED_SUPPLIER.VIETINBANK: {
				paramsRequest = {
					accountId: authInfo.accountId,
					cardId: linkedInfo.linkedInfo.cardId
				};
				resultRequest = await this.VietinBankLibs.UnLinked(paramsRequest);
				break;
			}
			case BankLinkConstant.LINKED_SUPPLIER.BIDVBANK: {
				paramsRequest = {
					transaction: linkedInfo.linkedInfo.transactionId,
					accountId: authInfo.accountId,
					accountName: _.trim(removeUnicode(authInfo.fullname)).toUpperCase(),
					version: _.get(linkedInfo, 'cardInfo.version', 1)
				};
				resultRequest = await this.BIDVBankLibs.UnLinked(paramsRequest);
				break;
			}
			case BankLinkConstant.LINKED_SUPPLIER.OCBBANK: {
				paramsRequest = {
					phone: _.replace(authInfo.phone, '84', '0'),
					cardId: linkedInfo.linkedInfo.cardId
				};
				resultRequest = await this.OCBBankLibs.UnLinked(paramsRequest);
				break;
			}
			case BankLinkConstant.LINKED_SUPPLIER.NAPAS_GATEWAY: {
				paramsRequest = {
					accountId: authInfo.accountId,
					cardId: linkedInfo.linkedInfo.cardId
				};
				resultRequest = await this.NapasLibs.UnLinked(paramsRequest);
				break;
			}
			case BankLinkConstant.LINKED_SUPPLIER.PG: {
				paramsRequest = {
					accountId: authInfo.accountId,
					cardId: linkedInfo.linkedInfo.cardId
				};
				resultRequest = await this.PGLibs.UnLinked(paramsRequest);
				break;
			}
			case BankLinkConstant.LINKED_SUPPLIER.SACOMBANK: {
				paramsRequest = {
					accountId: authInfo.accountId,
					cardId: linkedInfo.linkedInfo.cardId
				};
				resultRequest = await this.SacomBankLibs.UnLinked(paramsRequest);
				break;
			}
			default:
				response.message = 'Hủy liên kết thất bại. Không tìm thấy thông tin ngân hàng liên kêt.';
				return response;
		}
		Logger(`[UNLINKED_BANK_SDK] => [resultRequest]: ${JSON.stringify(resultRequest)}`);

		response.code = resultRequest.code;
		response.message = resultRequest.message;

		let conditionUpdate = {};
		let fieldUpdate = {};

		let conditionUpdateHistory = {};
		let fieldUpdateHistory = {};
		let isUnRegisterPaymentMethod = false;

		if (resultRequest.code === ResponseCode.UNLINKED_SUCCESSED) {
			isUnRegisterPaymentMethod = true;

			conditionUpdate = { id: linkedInfo.id };
			conditionUpdateHistory = {
				'service.id': linkedInfo.id,
				'service.type': BankLinkConstant.HISTORY_SERVICE.LINKED
			};

			fieldUpdate = {
				state: BankLinkConstant.LINKED_STATE.UNLINK,
				$push: {
					supplierResponsed: _.get(resultRequest, 'supplierResponse', null)
				}
			};
			fieldUpdateHistory = {
				state: BankLinkConstant.LINKED_STATE.UNLINK,
				'service.state': BankLinkConstant.LINKED_STATE.UNLINK
			};
		} else {
			conditionUpdate = { id: linkedInfo.id };
			fieldUpdate = {
				$push: {
					supplierResponsed: _.get(resultRequest, 'supplierResponse', null)
				}
			};
		}

		let resultUpdate = null;

		if (_.isEmpty(conditionUpdate) === false && _.isEmpty(fieldUpdate) === false) {
			resultUpdate = await Broker.call(LinkedModel.updateOne, [conditionUpdate, fieldUpdate]);
			if (!_.isObject(resultUpdate) || resultUpdate.nModified < 1) {
				Logger(`[UNLINKED_BANK_SDK] => [conditionUpdate & fieldUpdate]: ${JSON.stringify({ conditionUpdate, fieldUpdate })}`);
				return response;
			}
		}

		if (_.isEmpty(conditionUpdateHistory) === false && _.isEmpty(fieldUpdateHistory) === false) {
			resultUpdate = await Broker.call(HistoryModel.updateOne, [conditionUpdateHistory, fieldUpdateHistory]);
			if (!_.isObject(resultUpdate) || resultUpdate.nModified < 1) {
				Logger(`[UNLINKED_BANK_SDK] => [conditionUpdateHistory & fieldUpdateHistory]: ${JSON.stringify({ conditionUpdateHistory, fieldUpdateHistory })}`);
				return response;
			}
		}

		if (isUnRegisterPaymentMethod === true) {
			// Hủy đăng kí PTTT => Call action làm sau
			let registerCode = `${BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK}_NAPAS_${linkedInfo.id}`;
			if (linkedSupplier === BankLinkConstant.LINKED_SUPPLIER.PG) {
				registerCode = `${BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK}_CREDIT_CARD_${linkedInfo.id}`;
			} else if (linkedSupplier !== BankLinkConstant.LINKED_SUPPLIER.NAPAS_GATEWAY) {
				registerCode = `${BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK}_${_.get(linkedInfo, 'cardInfo.swiftCode', 'NONE')}_${linkedInfo.id}`;
			}
			try {
				await Broker.call(PAYMENT_METHOD.unregisterPaymentMethod, { registerCode });
			} catch (error) {
				Logger(`[UNLINKED_BANK_SDK] => [UnRegisterPaymentMethod] => [Error]: ${registerCode}`);
			}
		}

		return response;
	} catch (error) {
		if (error.name === 'MoleculerError') {
			response.code = error.code;
			response.message = error.message;
		}
		Logger(`[UNLINKED_BANK_SDK] => [ERROR]: ${error}`);
		return response;
	}
};
