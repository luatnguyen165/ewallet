// Libraries
const _ = require('lodash');
const Decimal = require('decimal.js');
const { MoleculerError } = require('moleculer').Errors;
const ShortId = require('shortid');
const Numeral = require('numeral');
const Moment = require('moment');

// Constants
const GeneralConstant = require('../constants/general.constant');
const ResponseCode = require('../constants/code.constant');
const BankLinkConstant = require('../constants/bankLink.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	const { removeUnicode, hiddenNumber } = this.Helper();

	const response = {
		code: ResponseCode.IPN_FAILED,
		message: 'Liên kết thất bại.',
		extraData: null
	};
	// MODEL
	const {
		LinkedModel, HistoryModel, DepositModel, PaymentEwalletModel
	} = GeneralConstant.MODEL;
	// SERVICE
	const { UUID, PAYMENT_METHOD } = GeneralConstant.SERVICE;
	let linkedInfo = null;
	const payload = _.get(ctx, 'params.body', {});
	try {
		// Example Payload: { trans_id: '58579971930', order_id: '58579971930KAI04MOC1', card_id: 286, amount: 2000, bank_code: 'SMLIB' }

		response.extraData = JSON.stringify(payload);
		Logger(`[IPN_LINKED_NAPAS] => [payload]: ${JSON.stringify(payload)}}`);

		if (!!payload.card_id === false) {
			response.message = 'Có có thông tin card_id. Vui lòng kiểm tra lại.';
			return response;
		}

		let condition = {
			transaction: payload.trans_id,
			'linkedInfo.transactionId': payload.order_id,
			type: BankLinkConstant.LINKED_TYPE.ATM,
			state: BankLinkConstant.LINKED_STATE.REQUIRED_VERIFY
		};
		linkedInfo = await Broker.call(LinkedModel.findOne, [condition]);
		if (_.get(linkedInfo, 'id', false) === false) {
			Logger(`[IPN_LINKED_NAPAS] => [FIND_LINKED] => ${JSON.stringify({ condition })}`);
			response.message = 'Không tìm thấy thông tin liên kết';
			return response;
		}

		condition = {
			id: linkedInfo.id
		};
		let filedUpdate = {
			'linkedInfo.cardId': _.toString(payload.card_id),
			'cardInfo.bankCode': payload.bank_code,
			state: BankLinkConstant.LINKED_STATE.LINKED,
			linkedAt: new Date().toISOString(),
			$push: {
				supplierResponsed: JSON.stringify(payload)
			}
		};

		linkedInfo = await Broker.call(LinkedModel.findOneAndUpdate, [condition, filedUpdate, { new: true }]);
		if (_.get(linkedInfo, 'id', false) === false) {
			Logger(`[IPN_LINKED_NAPAS] => [UPDATE_LINKED_ERROR]: ${JSON.stringify({ condition, filedUpdate })}`);
			response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E001).';
			return response;
		}

		// History
		condition = {
			'service.type': BankLinkConstant.SERVICES.LINKED,
			'service.id': linkedInfo.id
		};

		let historyInfo = await Broker.call(HistoryModel.findOne, [condition]);
		if (_.get(historyInfo, 'id', false) === false) {
			Logger(`[IPN_LINKED_NAPAS] => [FIND_HISTORY] => ${JSON.stringify({ condition })}`);
			response.message = 'Không tìm tháy lịch sử giao dịch.';
			return response;
		}

		const extraFee = new Decimal(historyInfo.total).minus(new Decimal(payload.amount));
		const fee = new Decimal(historyInfo.fee).add(extraFee);
		const totalReceive = new Decimal(historyInfo.amount).minus(extraFee);

		condition = { id: historyInfo.id };
		filedUpdate = {
			fee: _.toNumber(fee),
			amount: _.toNumber(totalReceive),
			'service.state': BankLinkConstant.LINKED_STATE.LINKED,
			state: BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED
		};

		historyInfo = await Broker.call(HistoryModel.findOneAndUpdate, [condition, filedUpdate], { new: true });
		if (_.get(historyInfo, 'id', false) === false) {
			Logger(`[IPN_LINKED_NAPAS] => [UPDATE_HISTORY_ERROR_1] => ${JSON.stringify({ condition, filedUpdate })}`);
			response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E002).';
			return response;
		}

		// Đăng  kí paymentMethod
		const bankName = _.get(linkedInfo, 'cardInfo.bankName', '');
		const cardNumber = _.get(linkedInfo, 'cardInfo.cardNumber', '');

		const paymentDesc = bankName && cardNumber ? `${bankName}-${cardNumber.substr(-4, 4)}` : '';
		const description = `Bạn đã nạp thành công ${Numeral(historyInfo.amount).format(0, 0)}đ ${paymentDesc ? `từ ${paymentDesc}` : ''}.`;

		const paramsRegister = {
			registerCode: `${BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK}_NAPAS_${linkedInfo.id}`,
			accountId: linkedInfo.accountId,
			group: BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK,
			title: _.get(linkedInfo, 'cardInfo.bankName', ''),
			description: 'Tài khoản liên kết',
			isActive: true,
			paymentInfo: {
				methodName: paymentDesc,
				swiftCode: _.get(linkedInfo, 'cardInfo.swiftCode', 'NONE'),
				linkedId: linkedInfo.id,
				cardNumber,
				accountNumber: _.get(linkedInfo, 'cardInfo.accountNumber', '') || '',
				cardHolder: _.get(linkedInfo, 'cardInfo.cardHolder', '') || '',
				routeName: payload.bank_code || '',
				bankName
			},
			isWithdrawable: true
		};
		let resultCreateMethod = null;
		try {
			resultCreateMethod = await Broker.call(PAYMENT_METHOD.registerPaymentMethod, paramsRegister);
		} catch (error) {
			Logger(`[IPN_LINKED_NAPAS] => [REGISTER_PAYMENT_METHOD_ERROR]: ${JSON.stringify(paramsRegister)}`);
			Logger(`[IPN_LINKED_NAPAS] => [REGISTER_PAYMENT_METHOD_ERROR]: ${JSON.stringify(error.message)}`);
		}

		// Xử lý nạp tiền vào ví
		let transaction = null;
		try {
			transaction = await Broker.call(UUID.pick, { prefix: 'DEPOSIT' });
		} catch (error) {
			transaction = ShortId.generate();
		}

		if (!!transaction === false) {
			response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E003).';
			return response;
		}

		const depositData = {
			accountId: linkedInfo.accountId,
			transaction,
			appId: historyInfo.appId,
			supplierTransaction: payload.order_id, // tranh nạp tiên dup
			amount: historyInfo.amount,
			fee: historyInfo.fee,
			total: historyInfo.total,
			description,
			state: BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED,
			$push: {
				supplierResponsed: JSON.stringify(payload)
			},
			method: {
				id: _.get(resultCreateMethod, 'paymentMethodId', 0),
				group: BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_GATEWAY,
				description: 'Tài khoản liên kết.',
				bankInfo: paramsRegister.paymentInfo
			}
		};

		let depositCreated = null;
		try {
			depositCreated = await Broker.call(DepositModel.create, [depositData]);
		} catch (error) {
			Logger(`[IPN_LINKED_NAPAS] => [CREATE_DEPOSIT_ERROR]: ${JSON.stringify(depositData)}`);
			Logger(`[IPN_LINKED_NAPAS] => [CREATE_DEPOSIT_ERROR]: ${error}`);
			response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E004)';
			return response;
		}
		if (_.get(depositCreated, 'id', false) === false) {
			Logger(`[IPN_LINKED_NAPAS] => [CREATE_DEPOSIT_ERROR_2]: ${JSON.stringify(depositData)}`);
			response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E005)';
			return response;
		}
		// Tạo Record Payment
		const paymentData = {
			accountId: historyInfo.accountId,
			appId: historyInfo.appId,
			transaction,
			amount: historyInfo.total,
			state: BankLinkConstant.LINKED_PAYMENT_STATE.PENDING,
			method: 'LINKED',
			service: {
				code: 'DEPOSIT',
				type: 'DEPOSIT',
				transaction
			},
			description: 'Nạp tiền vào ví PayME'
		};

		let paymentCreated = null;
		try {
			paymentCreated = await Broker.call(PaymentEwalletModel.create, [paymentData]);
		} catch (error) {
			Logger(`[IPN_LINKED_NAPAS] => [CREATE_PAYMENT_ERROR]: ${JSON.stringify(paymentData)}`);
			Logger(`[IPN_LINKED_NAPAS] => [CREATE_PAYMENT_ERROR]: ${error}`);
			response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E006)';
			return response;
		}
		if (_.get(paymentCreated, 'id', false) === false) {
			Logger(`[IPN_LINKED_NAPAS] => [CREATE_PAYMENT_ERROR_2]: ${JSON.stringify(paymentData)}`);
			response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E007)';
			return response;
		}
		// Tạo history nạp tiền vào ví
		const dataHistory = {
			accountId: linkedInfo.accountId,
			appId: historyInfo.appId,
			service: {
				type: 'DEPOSIT',
				id: depositCreated.id,
				transaction: depositCreated.transaction,
				method: 'DEPOSIT',
				state: BankLinkConstant.LINKED_PAYMENT_STATE.PENDING,
				name: 'Nạp tiền bằng Tài khoản liên kết'
			},
			amount: historyInfo.amount,
			total: historyInfo.total,
			fee: historyInfo.fee,
			state: BankLinkConstant.LINKED_PAYMENT_STATE.PENDING,
			description,
			changed: '+',
			tags: ['DEPOSIT'],
			payment: {
				id: paymentCreated.id,
				transaction,
				state: paymentCreated.state,
				method: 'LINKED',
				description
			}
		};

		let historyDepositCreated = null;
		try {
			historyDepositCreated = await Broker.call(HistoryModel.create, [dataHistory]);
		} catch (error) {
			Logger(`[IPN_LINKED_NAPAS] => [CREATE_HISTORY_DEPOSIT_ERROR]: ${JSON.stringify(dataHistory)}`);
			Logger(`[IPN_LINKED_NAPAS] => [CREATE_HISTORY_DEPOSIT_ERROR]: ${error}`);
			response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E008)';
			return response;
		}
		if (_.get(historyDepositCreated, 'id', false) === false) {
			Logger(`[IPN_LINKED_NAPAS] => [CREATE_HISTORY_DEPOSIT_ERROR_2]: ${JSON.stringify(dataHistory)}`);
			response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E009)';
			return response;
		}

		// Call Service nạp tiền vào Ví
		const paramsRequest = {
			accountId: linkedInfo.accountId,
			amount: _.toNumber(totalReceive),
			description: 'Nạp tiền vào ví PayME.',
			referData: historyDepositCreated
		};
		// WalletLibs
		let resultDeposit = null;
		try {
			resultDeposit = await this.WalletLibs.Deposit(paramsRequest);
		} catch (error) {
			Logger(`[IPN_LINKED_NAPAS] => [DEPOSIT_WALLET_ERROR]: ${JSON.stringify(paramsRequest)}`);
			Logger(`[IPN_LINKED_NAPAS] => [DEPOSIT_WALLET_ERROR]: ${error}`);
			response.message = 'Liên kết thất bại. Vui lòng thử lại sau (E010)';
			return response;
		}
		if (resultDeposit.successed === false) {
			Logger(`[IPN_LINKED_NAPAS] => [DEPOSIT_WALLET_ERROR]: ${JSON.stringify({ paramsRequest, resultDeposit })}`);
			await Broker.call(DepositModel.updateOne, [{ id: depositCreated.id },
				{
					state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
					$push: {
						supplierResponsed: resultDeposit.supplierResponse
					}
				}
			]);
			await Broker.call(HistoryModel.updateOne, [{ id: historyDepositCreated.id }, {
				state: BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
				'service.state': BankLinkConstant.LINKED_PAYMENT_STATE.FAILED,
				publishedAt: new Date()
			}]);
			response.message = 'Nạp tiền vào ví PayME thất bại.';
			return response;
		}
		let resultUpdate = null;

		resultUpdate = await Broker.call(DepositModel.updateOne, [{ id: depositCreated.id },
			{
				state: BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED,
				$push: {
					supplierResponsed: resultDeposit.supplierResponse
				}
			}
		]);
		if (!_.isObject(resultUpdate) || resultUpdate.nModified < 1) {
			Logger(`[IPN_LINKED_NAPAS] => [UPDATE_DEPOSIT_ERROR]: ${JSON.stringify({ id: depositCreated.id })}`);
		}

		resultUpdate = await Broker.call(PaymentEwalletModel.updateOne, [{ id: paymentCreated.id }, { state: BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED }]);
		if (!_.isObject(resultUpdate) || resultUpdate.nModified < 1) {
			Logger(`[IPN_LINKED_NAPAS] => [UPDATE_PAYMENT_ERROR]: ${JSON.stringify({ id: paymentCreated.id })}`);
		}

		resultUpdate = await Broker.call(HistoryModel.updateOne, [{ id: historyDepositCreated.id },
			{
				state: BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED,
				'service.state': BankLinkConstant.LINKED_PAYMENT_STATE.SUCCEEDED,
				balance: resultDeposit.balance,
				publishedAt: new Date()
			}
		]);
		if (!_.isObject(resultUpdate) || resultUpdate.nModified < 1) {
			Logger(`[IPN_LINKED_NAPAS] => [UPDATE_HISTORY_ERROR_2]: ${JSON.stringify({ id: historyDepositCreated.id })}`);
		}

		await Broker.call(LinkedModel.updateOne, [
			{
				id: linkedInfo.id
			},
			{
				depositTransaction: transaction,
				'cardInfo.methodName': paymentDesc
			}
		]);

		response.code = ResponseCode.IPN_SUCCEEDED;
		response.message = 'Liên kết thành công.';
		return response;
	} catch (error) {
		Logger(`[IPN_LINKED_NAPAS] => [ERROR]: ${error}}`);
		response.message = error.message || 'Liên kết thất bại.';
		return response;
	} finally {
		// IPN cho Merchant khi liên kết thành công;
		console.log('response :>> ', response);
		const phone = _.get(linkedInfo, 'linkedInfo.phone', null);
		const merchantId = _.get(linkedInfo, 'linkedInfo.merchantId', null);
		const ipnUrl = _.get(linkedInfo, 'linkedInfo.ipnUrl', null);
		if (merchantId && ipnUrl) {
			try {
				const paramsRequest = {
					ipnUrl,
					merchantId: _.toNumber(merchantId),
					ipnData: {
						linkedId: linkedInfo.id,
						phone,
						walletAccountId: _.get(linkedInfo, 'accountId', null),
						state: response.code === 1000 ? 'SUCCEEDED' : 'FAILED',
						amount: _.get(payload, 'amount', null),
						transaction: linkedInfo.transaction,
						cardInfo: {
							swiftCode: _.get(linkedInfo, 'cardInfo.swiftCode', null),
							cardHolder: _.get(linkedInfo, 'cardInfo.cardHolder', null),
							cardNumber: hiddenNumber(_.get(linkedInfo, 'cardInfo.cardNumber', null), 'CARD') || null,
							issuedAt: _.get(linkedInfo, 'cardInfo.issuedAt', null)
						}
					}
				};

				this.logger.info(`[IPN_LINKED_MERCHANT]:::[REQUEST] ${JSON.stringify(paramsRequest)}`);
				const ipn = await this.broker.call('v1.ipnMerchantSDK.ipnLinked', paramsRequest, { timeout: 90 * 1000 });
				this.logger.info(`[IPN_LINKED_MERCHANT]:::[RESPONSE] ${JSON.stringify(ipn)}`);
			} catch (error) {
				console.error('[IPN_LINKED_MERCHANT][ERROR]', error);
				this.logger.info(`[IPN_LINKED_MERCHANT][ERROR]::: ${JSON.stringify(String(error))}`);
			}
		}
	}
};
