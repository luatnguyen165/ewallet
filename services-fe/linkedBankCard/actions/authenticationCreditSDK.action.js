// Libraries
const _ = require('lodash');
const Moment = require('moment');

// Constants
const GeneralConstant = require('../constants/general.constant');
const ResponseCode = require('../constants/code.constant');
const BankLinkConstant = require('../constants/bankLink.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;

	const { hiddenNumber } = this.Helper();
	const response = {
		code: ResponseCode.AUTH_CREDIT_FAILED,
		message: 'Xác thực thông tin thẻ thất bại (E001).'
	};
	// MODEL
	const { LinkedModel } = GeneralConstant.MODEL;

	try {
		const payload = _.get(ctx, 'params.body', {});
		this.logger.info(`[AuthCreditSKD] Payload >>> ${JSON.stringify(payload)} `);
		let linkedInfo = null;
		const paramsRequest = {};

		if (payload.linkedId) {
			const findCondition = { id: payload.linkedId };
			linkedInfo = await Broker.call(LinkedModel.findOne, [findCondition]);
			if (_.get(linkedInfo, 'id', false) === false) {
				this.logger.info(`[AuthCreditSKD]: findLinked: >>> ${JSON.stringify({ findCondition })}`);
				response.message = 'Xác thực thông tin thẻ thất bại. Không tìm thấy thông tin liên kết (E001).';
				return response;
			}

			paramsRequest.transaction = _.get(linkedInfo, 'linkedInfo.transactionId', null);
			paramsRequest.cardId = _.get(linkedInfo, 'linkedInfo.cardId', null);
		} else {
			paramsRequest.cardNumber = payload.cardNumber;
			paramsRequest.expiredAt = payload.expiredAt;
		}

		this.logger.info(`[AuthCreditSKD]: Request >>> ${JSON.stringify(paramsRequest)}`);
		const resultAuth = await Broker.call('v1.payment.creditAuthentication', { body: { ...paramsRequest } }, {
			timeout: 90 * 1000,
			meta: {
				auth: {
					credentials: {
						accountId: process.env.PG_WALLET_ACCOUNT_ID
					}
				}
			}
		});
		this.logger.info(`[AuthCreditSKD]: Response >>> ${JSON.stringify(resultAuth)}`);

		if (resultAuth.code !== 129020) {
			response.message = 'Xác thực thông tin thẻ thất bại. Vui lòng thử lại sau.';
			return response;
		}

		response.code = ResponseCode.AUTH_CREDIT_SUCCESSED;
		response.message = resultAuth.message;
		response.data = resultAuth.data;
		return response;
	} catch (error) {
		console.error('[AuthCreditSKD]: Exception >>>', error);
		this.logger.info(`[AuthCreditSKD]: Exception >>>  ${String(error)}}`);
	}
	return response;
};
