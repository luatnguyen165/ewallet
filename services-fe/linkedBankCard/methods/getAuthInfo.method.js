const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;

// Method lấy thông tin một giao dịch
module.exports = function (meta) {
	const credentials = {
		merchantId: null,
		accountId: null,
		storeId: null,
		appId: null
	};
	try {
		console.log('meta :>> ', meta);
		if (!_.isEmpty(_.get(meta, 'security.credentials', {}))) {
			const security = meta.security.credentials;
			credentials.merchantId = _.get(security, 'merchantId', null);
			credentials.appId = _.get(security, 'appId', null);
			credentials.storeId = _.get(security, 'storeId', null);
			credentials.accountId = _.get(security, 'accountId', null);
		}

		if (!_.isEmpty(_.get(meta, 'auth.credentials', {}))) {
			const auth = meta.auth.credentials;
			credentials.merchantId = _.get(auth, 'merchantId', _.get(meta, 'security.credentials.merchantId', null));
			credentials.appId = _.get(auth, 'appId', _.get(meta, 'security.credentials.appId', null));
			credentials.storeId = _.get(auth, 'storeId', _.get(meta, 'security.credentials.storeId', null));
			credentials.accountId = _.get(auth, 'accountId', _.get(meta, 'security.credentials.accountId', null));
		}
		return credentials;
	} catch (err) {
		if (err.name === 'MoleculerError') {
			throw err;
		}
		throw new MoleculerError(err);
	}
};
