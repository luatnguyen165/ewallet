const _ = require('lodash');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;

function getTime(times, lang = 'vi') {
	let resultText = '';
	const { hours, minutes, seconds } = times;

	if (lang === 'vi') {
		if (hours > 0) {
			resultText = `${hours} giờ ${minutes} phút ${seconds} giây`;
		} else if (minutes > 0) {
			resultText = `${minutes} phút ${seconds} giây`;
		} else resultText = `${seconds} giây`;
	} else if (hours > 0) {
		resultText = `${hours} hour(s) ${minutes} minute(s) ${seconds} second(s)`;
	} else if (minutes > 0) {
		resultText = `${minutes} minute(s) ${seconds} second(s)`;
	} else resultText = `${seconds} second(s)`;

	return resultText;
}

module.exports = async function (params = {}) {
	// 1: tai khoản bị khóa - 2 tài khoản tạm khóa
	const RedisCache = this.RedisCache();
	const Logger = this.logger;

	const keyConfig = 'wallet.config.unlinkWrongTimes';
	const keyRedis = `PAYME_PIN_${params.accountId}`;

	let settingConfig = await this.broker.call('v1.eWalletSettingModel.findOne', [{ key: keyConfig }]);
	settingConfig = _.get(settingConfig, 'value', JSON.stringify({ lockedTimes: 3, minuteLocked: 15 }));

	if (_.isEmpty(settingConfig) === false) {
		try {
			settingConfig = JSON.parse(settingConfig);
		} catch (error) {
			settingConfig = {
				lockedTimes: 3,
				minuteLocked: 15
			};
		}
	}

	async function Check() {
		const result = {
			successed: false,
			message: '',
			state: ''
		};

		try {
			let times = await RedisCache.get({ key: keyRedis }) || 1;
			times = _.toNumber(times);
			if (times >= _.toNumber(settingConfig.lockedTimes)) {
				const ttl = await RedisCache.pttl({ key: keyRedis }) / 1000;
				const momentTime = moment.duration(ttl, 'seconds');

				const time = {
					hours: momentTime.hours(),
					minutes: momentTime.minutes(),
					seconds: momentTime.seconds()
				};
				result.state = 'TEMP_LOCKED';
				result.ttl = ttl;
				result.message = `Tạm khóa PayME PIN. Bạn đã nhập sai PayME PIN ${settingConfig.lockedTimes} lần.\n Thử lại sau ${getTime(time)}.`;

				return result;
			}
			result.successed = true;

			return result;
		} catch (err) {
			// return result;
			Logger.info(`[REDIS]::: Check Unlink Wrong Password :=> ${String(err)}`);
		}
		return result;
	}

	async function Locked() {
		const result = {
			successed: false,
			message: ''
		};

		try {
			let times = await RedisCache.get({ key: keyRedis }) || 1;
			times = _.toNumber(times);
			if (times < _.toNumber(settingConfig.lockedTimes)) {
				await RedisCache.set({ key: keyRedis, value: times + 1, ttl: _.toNumber(settingConfig.minuteLocked) * 60 });
			}
			result.successed = true;
			result.lockTimes = settingConfig.lockedTimes - times;
			return result;
		} catch (err) {
			// return result;
			Logger.info(`[REDIS]::: Check Unlink Wrong Password :=> ${String(err)}`);
		}
	}

	async function Delete() {
		try {
			await RedisCache.delete({ key: keyRedis });
			return true;
		} catch (err) {
			// return result;
			Logger.info(`[REDIS]::: Check Unlink Wrong Password :=> ${String(err)}`);
		}
	}

	return {
		Check,
		Locked,
		Delete
	};
};
