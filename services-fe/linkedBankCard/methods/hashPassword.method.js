const sha256 = require('sha256');
const GeneralConstant = require('../constants/general.constant');

module.exports = function () {
	const salt = GeneralConstant.SALT_PASSWORD;

	function hashPassword(password) {
		return sha256(password + salt);
	}

	function comparePassword(password, hash) {
		return sha256(password + salt) === hash;
	}

	return {
		hashPassword,
		comparePassword
	};
};
