const _ = require('lodash');
const GeneralConstant = require('../constants/general.constant');
const BankLinkConstant = require('../constants/bankLink.constant');

module.exports = function () {
	const Broker = this.broker;
	const Logger = this.logger.info;

	const ENV = process.env.ENV_NAME || 'SANDBOX';
	const { getRecipientByCard, getRecipientByAccount } = GeneralConstant.SERVICE.BANK;
	return {
		hiddenNumber(number, type = 'CARD') {
			try {
				let numberResponse = null;
				if (!!number === false) {
					return numberResponse;
				}
				if (type === 'CARD') {
					const lengthNeedToReplace = number.length - 8;
					const regex = new RegExp(`(\\d{4})\\d{${lengthNeedToReplace}}`, 'gi');
					numberResponse = number.replace(regex, `$1${'*'.repeat(lengthNeedToReplace)}`);
				} else {
					const lengthNeedToReplace = number.length - 4;
					const regex = new RegExp(`(\\d{0})\\d{${lengthNeedToReplace}}`, 'gi');
					numberResponse = number.replace(regex, `$1${'*'.repeat(lengthNeedToReplace)}`);
				}
				return numberResponse;
			} catch (error) {
				return number;
			}
		},
		removeUnicode(alias) {
			let str = alias;
			if (alias) {
				str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
				str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
				str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
				str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
				str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
				str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
				str = str.replace(/đ/g, 'd');
				str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
				str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
				str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
				str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
				str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
				str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
				str = str.replace(/Đ/g, 'D');
				str = str.replace(':', '-');
				str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ''); // huyền, sắc, hỏi, ngã, nặng
				str = str.replace(/\u02C6|\u0306|\u031B/g, ''); // mũ â (ê), mũ ă, mũ ơ (ư)
				str = str.replace(/[^A-Za-z0-9\s]/gi, '');
				return str;
			}
			return '';
		},
		async getRecipientName(params = {}) {
			try {
				let name = false;
				const {
					swiftCode, type, number, defaultName
				} = params;
				if (_.toUpper(ENV) === 'SANDBOX') {
					switch (_.toString(number)) {
						case '9704480248306775': {
							name = 'THOI THUC PHAN';
							break;
						}
						case '0011100003367009': {
							name = 'GB NAME 1-803585';
							break;
						}
						case '9704125600182062':
						case '9704125600182054': {
							name = 'TRUONG TUAN CUONG';
							break;
						}
						case '9704000000000018': {
							name = 'NGUYEN VAN A';
							break;
						}
						case '9704150100005654': {
							name = 'TRAN THI HUONG';
							break;
						}
						default:
							break;
					}
				} else {
					let actionBroker = null;
					const paramsRequest = {
						swiftCode
					};
					if (type === 'CARD') {
						actionBroker = getRecipientByCard;
						paramsRequest.cardNumber = number;
					} else {
						name = defaultName;
						actionBroker = getRecipientByAccount;
						paramsRequest.bankAccountNumber = number;
					}

					const resultRequest = await Broker.call(actionBroker, paramsRequest);
					Logger(`[LINKED_BANK] => [METHOD: getRecipientName] => ${JSON.stringify({ paramsRequest, resultRequest })}}`);

					if (resultRequest.code === 107001) {
						name = _.get(resultRequest, 'data.fullname', false);
					}
					return name;
				}
				return name;
			} catch (error) {
				Logger(`[LINKED_BANK] => [METHOD: getRecipientName] => [ERROR]: ${error}`);
				return false;
			}
		}
	};
};
