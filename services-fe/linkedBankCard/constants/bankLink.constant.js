module.exports = {
	SWIFT_CODE: {
		BIDV: 'BIDVVNVX',
		OCB: 'ORCOVNVX',
		PVCOMBANK: 'WBVNVNVX',
		SAIGON_BANK: 'SBITVNVX',
		VIETINBANK: 'ICBVVNVX'
	},
	PAYMENT_METHOD_GROUP: {
		PAYME: 'PAYME',
		EWALLET: 'EWALLET',
		LINKED_BANK: 'LINKED_BANK',
		LINKED_CREDIT: 'LINKED_CREDIT',
		GATEWAY: 'GATEWAY',
		LINKED_GATEWAY: 'LINKED_GATEWAY',
		LINKED_BANK_OCBBANK: 'LINKED_BANK_OCBBANK',
		ISEC: 'ISEC',
		DEPOSIT_BANK_MANUAL: 'DEPOSIT_BANK_MANUAL',
		SSCC: 'SSCC',
		VNPAY: 'VNPAY',
		PAYME_CREDIT: 'PAYME_CREDIT',
		OTHER: 'OTHER'
	},

	LINKED_SUPPLIER: {
		NAPAS_GATEWAY: 'NAPAS_GATEWAY',
		PVCOMBANK: 'PVCOMBANK',
		OCBBANK: 'OCBBANK',
		BIDVBANK: 'BIDVBANK',
		PG: 'PG',
		VIETINBANK: 'VIETINBANK',
		SACOMBANK: 'SACOMBANK'
	},
	LINKED_TYPE: {
		// Chạy cổng napas
		ATM: 'ATM',
		// Chạy đường trực tiếp qua bank
		BANKING: 'BANKING',
		// Chạy liên kết đường accountNumber của bank
		ACCOUNT: 'ACCOUNT',
		// Chạy qua CTT
		CREDIT_CARD: 'CREDIT_CARD'
	},
	LINKED_STATE: {
		NEW: 'NEW',
		LINKED: 'LINKED',
		LOCKED: 'LOCKED',
		UNLINK: 'UNLINK',
		FAILED: 'FAILED',
		REQUIRED_OTP: 'REQUIRED_OTP',
		REQUIRED_VERIFY: 'REQUIRED_VERIFY',
		INVALID_OTP: 'INVALID_OTP',
		OTP_RETRY_TIMES_OVER: 'OTP_RETRY_TIMES_OVER'
	},
	LINKED_TYPE_SUPPLIER: {
		CARD: 'CARD',
		ACCOUNT: 'ACCOUNT'
	},
	SERVICES: {
		LINKED: 'LINKED' // Lien ket the
	},
	LINKED_TAG: {
		LINKED: 'LINKED',
		LINKED_PVCBANK: 'LINKED_PVCBANK',
		LINKED_OCBBANK: 'LINKED_OCBBANK',
		LINKED_BIDVBANK: 'LINKED_BIDVBANK',
		LINKED_VIETINBANK: 'LINKED_VIETINBANK',
		LINKED_NAPAS: 'LINKED_NAPAS',
		LINKED_CREDIT: 'LINKED_CREDIT',
		UNLINK: 'UNLINK'
	},
	ACCOUNT_KYC_STATE: {
		PENDING: 'PENDING',
		REJECTED: 'REJECTED',
		APPROVED: 'APPROVED',
		CANCELED: 'CANCELED',
		BANNED: 'BANNED',
		null: null
	},
	ENV_NAME: {
		MobileApp: 'MobileApp',
		WebApp: 'WebApp'
	},
	LINKED_GATEWAY: {
		NAPAS: 'NAPAS',
		OCB: 'OCB',
		BIDV: 'BIDV',
		VIETINBANK: 'VIETIN',
		PVCBANK: 'PVCBANK',
		PG: 'PG'
	},

	LINKED_METHOD: {
		CREDIT: 'CREDIT',
		CARD: 'CARD',
		ACCOUNT: 'ACCOUNT'
	},

	HISTORY_SERVICE: {
		LINKED: 'LINKED'
	},
	LINKED_PAYMENT_STATE: {
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED',
		PENDING: 'PENDING',
		REFUNDED: 'REFUNDED',
		REQUIRED_VERIFY: 'REQUIRED_VERIFY',
		BALANCE_NOT_ENOUGHT: 'BALANCE_NOT_ENOUGHT',
		REQUIRED_TRANSFER: 'REQUIRED_TRANSFER',
		INVALID_PARAMS: 'INVALID_PARAMS',
		OTP_RETRY_TIMES_OVER: 'OTP_RETRY_TIMES_OVER',
		INVALID_OTP: 'INVALID_OTP',
		INVALID_SECURITY_CODE: 'INVALID_SECURITY_CODE'
	},
	LINKED_OCB_MESSAGE: {
		0: 'Không nhận được kết quả từ phía ngân hàng. Vui lòng thử lại sau ít phút.',
		400: 'Thông tin thẻ không đúng. Vui lòng thử lại!',
		406: 'Đã có lỗi xảy ra. Vui lòng thử lại sau ít phút.',
		504: 'Không nhận được kết quả từ phía ngân hàng. Vui lòng thử lại sau ít phút.',
		41701: 'Có lỗi xảy ra từ phía ngân hàng. Vui lòng thử lại sau ít phút.',
		41704: 'Có lỗi xảy ra từ phía ngân hàng. Vui lòng thử lại sau ít phút.',
		41706: 'Số tiền giao dịch vượt giới hạn cho phép của 1 ngày!',
		41707: 'Số tiền giao dịch vượt giới hạn cho phép!',
		41708: 'Số tiền giao dịch nhỏ hơn hạn mức tối thiểu. Vui lòng thử lại.',
		41709: 'Giao dịch lỗi! Vui lòng kiểm tra lại thông tin.',
		41714: 'Tài khoản không đủ số dư!',
		41715: 'Giao dịch lỗi! Vui lòng thử lại sau ít phút!',
		41716: 'Tài khoản bị khoá! Vui lòng kiểm tra lại thông tin!',
		41721: 'Thẻ đã được liên kết với tài khoản khác!',
		41727: 'Không tìm thấy tài khoản liên kết ví!',
		41730: 'Không hỗ trợ loại xác thực!',
		41731: 'Giao dịch không hợp lệ!',
		41732: 'Tài khoản không hợp lệ, hoặc đang bị khoá/ bị huỷ. Vui lòng kiểm tra lại!',
		41742: 'Thẻ /tài khoản không hợp lệ, hoặc bị khoá, bị huỷ!',
		41743: 'Số thẻ (hoặc tài khoản) không trùng với tên người dùng. Vui lòng kiểm tra lại!',
		41744: 'Thông tin số giấy tờ (CMND/CCCD) không tồn tại bên ngân hàng OCB. Vui lòng kiểm tra lại thông tin.',
		41745: 'Thông tin thẻ không đúng. Vui lòng kiểm tra lại thông tin!',
		41746: 'Gói tài khoản OMNI OCB không hợp lệ. Vui lòng liên hệ với ngân hàng OCB. Xin cảm ơn.',
		41747: 'Tài khoản ngân hàng chưa đăng kí OMNI OCB. Vui lòng liên hệ với ngân hàng OCB để liên kết ví. Xin cảm ơn.',
		41748: 'Tài khoản OMNI OCB không hợp lệ. Vui lòng liên hệ với ngân hàng OCB. Xin cảm ơn.',
		41749: 'Số điện thoại không hợp lệ!',
		41754: 'Số thẻ không hợp lệ.',
		// OTP Sai
		41723: 'Lỗi khi tạo OTP. Vui lòng thử lại!',
		41724: 'Mã OTP không đúng!',
		41725: 'Lỗi khi liên kết ví. Vui lòng thử lại sau!',
		41726: 'OTP đã hết hạn!'
	},
	VN_PHONE_PREFIX: {
		VIETTEL: ['086', '096', '097', '098', '032', '033', '034', '035', '036', '037', '038', '039'],
		MOBIFONE: ['089', '090', '093', '070', '079', '077', '076', '078'],
		VINAPHONE: ['088', '091', '094', '083', '084', '085', '081', '082'],
		VIETNAMOBILE: ['092', '056', '058', '052'],
		GMOBILE: ['099', '059'],
		SFone: ['095'],
		ITELECOM: ['087']
	}
};
