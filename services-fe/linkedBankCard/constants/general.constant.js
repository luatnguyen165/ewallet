/* eslint-disable quote-props */
module.exports = {
	SALT_PASSWORD: '(*&^$#$#@$@!@#',
	MODEL: {
		KYCModel: {
			create: 'v1.eWalletKycModel.create',
			find: 'v1.eWalletKycModel.findMany',
			findOne: 'v1.eWalletKycModel.findOne',
			updateOne: 'v1.eWalletKycModel.updateOne',
			findOneAndUpdate: 'v1.eWalletKycModel.findOneAndUpdate'
		},
		AccountModel: {
			create: 'v1.eWalletAccountModel.create',
			find: 'v1.eWalletAccountModel.findMany',
			findOne: 'v1.eWalletAccountModel.findOne',
			updateOne: 'v1.eWalletAccountModel.updateOne',
			findOneAndUpdate: 'v1.eWalletAccountModel.findOneAndUpdate'
		},
		AppInfoModel: {
			create: 'v1.ewalletAppInfoModel.create',
			find: 'v1.ewalletAppInfoModel.findMany',
			findOne: 'v1.ewalletAppInfoModel.findOne',
			updateOne: 'v1.ewalletAppInfoModel.updateOne',
			findOneAndUpdate: 'v1.ewalletAppInfoModel.findOneAndUpdate'
		},
		ConnectedUserModel: {
			create: 'v1.eWalletConnectedUserModel.create',
			find: 'v1.eWalletConnectedUserModel.findMany',
			findOne: 'v1.eWalletConnectedUserModel.findOne',
			updateOne: 'v1.eWalletConnectedUserModel.updateOne',
			findOneAndUpdate: 'v1.eWalletConnectedUserModel.findOneAndUpdate'
		},
		LinkedModel: {
			create: 'v1.eWalletLinkedModel.create',
			find: 'v1.eWalletLinkedModel.findMany',
			findOne: 'v1.eWalletLinkedModel.findOne',
			updateOne: 'v1.eWalletLinkedModel.updateOne',
			findOneAndUpdate: 'v1.eWalletLinkedModel.findOneAndUpdate'
		},
		HistoryModel: {
			create: 'v1.eWalletHistoryModel.create',
			find: 'v1.eWalletHistoryModel.findMany',
			findOne: 'v1.eWalletHistoryModel.findOne',
			updateOne: 'v1.eWalletHistoryModel.updateOne',
			findOneAndUpdate: 'v1.eWalletHistoryModel.findOneAndUpdate'
		},
		BankCodeModel: {
			create: 'v1.eWalletBankCodeModel.create',
			find: 'v1.eWalletBankCodeModel.findMany',
			findOne: 'v1.eWalletBankCodeModel.findOne',
			updateOne: 'v1.eWalletBankCodeModel.updateOne',
			findOneAndUpdate: 'v1.eWalletBankCodeModel.findOneAndUpdate'
		},
		SettingModel: {
			create: 'v1.eWalletSettingModel.create',
			find: 'v1.eWalletSettingModel.findMany',
			findOne: 'v1.eWalletSettingModel.findOne',
			updateOne: 'v1.eWalletSettingModel.updateOne',
			findOneAndUpdate: 'v1.eWalletSettingModel.findOneAndUpdate'
		},
		DepositModel: {
			create: 'v1.eWalletDepositModel.create',
			find: 'v1.eWalletDepositModel.findMany',
			findOne: 'v1.eWalletDepositModel.findOne',
			updateOne: 'v1.eWalletDepositModel.updateOne',
			findOneAndUpdate: 'v1.eWalletDepositModel.findOneAndUpdate'
		},
		// eWalletPaymentModel
		PaymentEwalletModel: {
			create: 'v1.eWalletPaymentModel.create',
			find: 'v1.eWalletPaymentModel.findMany',
			findOne: 'v1.eWalletPaymentModel.findOne',
			updateOne: 'v1.eWalletPaymentModel.updateOne',
			findOneAndUpdate: 'v1.eWalletPaymentModel.findOneAndUpdate'
		},
		PaymentPGModel: {
			create: 'v1.paymentModel.create',
			find: 'v1.paymentModel.findMany',
			findOne: 'v1.paymentModel.findOne',
			updateOne: 'v1.paymentModel.updateOne',
			findOneAndUpdate: 'v1.paymentModel.findOneAndUpdate'
		}
	},
	SERVICE: {
		UUID: {
			pick: 'v1.walletUuid.pick'
		},
		BANK: {
			getRecipientByCard: 'v1.bank.getRecipientByCard',
			getRecipientByAccount: 'v1.bank.getRecipientByAccount'
		},
		PAYMENT_METHOD: {
			registerPaymentMethod: 'v1.eWalletPaymentMethod.registerMethod',
			unregisterPaymentMethod: 'v1.eWalletPaymentMethod.unregisterMethod'
		},
		PAYMENT_GATEWAY: {
			createLinkedOrder: 'v1.order.createWeb',
			linkedCredit: 'v1.payment.fePayment',
			unlinkCredit: 'v1.payment.creditUnlinked',
			refundCredit: 'v1.order.refund',
			processLinkCredit: 'v1.ewalletBankLink.processLinkCredit'
		}
	},
	CARD: {
		// PVCombank
		'9704125600182062': {
			fullname: 'Trương Tuấn Cường',
			phone: '0920000005',
			identifyNumber: '875662323245'
		},
		'9704125600182054': {
			fullname: 'Trương Tuấn Cường',
			phone: '0372041227',
			identifyNumber: '78888888888'
		},
		// OCB
		'0037100003375007': {
			fullname: 'Trần Thị Ngọc Thắm',
			phone: '0797134732',
			identifyNumber: '301572672'
		},
		'0011100003367009': {
			fullname: 'GB NAME 1-803585',
			phone: '0953208227',
			identifyNumber: 'LEGAL ID-803585'
		},
		'9704480248306775': {
			fullname: 'Thời Phúc Phần',
			phone: '0963566079',
			identifyNumber: '070187001822'
		},
		'0111100012326002': {
			fullname: 'Nguyễn Hữu Sang',
			phone: '0963566079',
			identifyNumber: '165121121'
		},
		// Vietin
		'9704150260000867': {
			fullname: 'CUSTOMMER NAME',
			phone: '0349637486',
			identifyNumber: '1401795952'
		},
		'9704150100005662': {
			fullname: 'Trần Công Khanh',
			phone: '0986697989',
			identifyNumber: '013369393'
		},
		'9704150100005654': {
			fullname: 'Trần Thị Hương',
			phone: '0979596803',
			identifyNumber: '012959232'
		},
		// BIDV
		'12010002100520': {
			fullname: 'Phạm Tuấn',
			phone: '0906328126',
			identifyNumber: '301300493'
		},
		'12010002101037': {
			fullname: 'Phan Thị Châu Giang',
			phone: '0902539889',
			identifyNumber: '001198001552'
		},
		'65210000459899': {
			fullname: 'Nguyễn Phước Vĩnh Thái',
			phone: '0906820686',
			identifyNumber: '280830670'
		}
	}
};
