const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const _ = require('lodash');
const generalConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	transactionId: {
		type: String,
		required: true
	},
	paymentId: {
		type: String,
		default: null
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	total: {
		type: Number,
		required: true,
		default: 0
	},
	fee: {
		type: Number,
		required: true,
		default: 0
	},
	state: {
		type: String,
		enum: _.values(generalConstant.STATE)
	},
	description: {
		type: String,
		default: null
	},
	creditInfo: {
		linkedId: {
			type: Number, // id tới bảng linked Bank
			required: true
		},
		cardNumber: {
			type: String
		},
		expiredAt: {
			type: String,
			default: null
		},
		description: {
			type: String
		},
		methodId: {
			type: Number
		}
	},
	supplierTransaction: {
		type: String,
		default: null
	},
	extraData: mongoose.Schema.Types.Mixed,
	supplierResponsed: mongoose.Schema.Types.Mixed,
	isChecked: {
		type: Boolean,
		default: false
	}
}, {
	collection: 'Payment_LinkedCreditCardPG',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ state: 1 });
/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}_id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
