const DbService = require('moleculer-db');
const moleculerI18n = require('moleculer-i18n-js');
const path = require('path');
const _ = require('lodash');

// Library
const PVComBankLibrary = require('./libraries/pvcomBank.library');
const VietinBankLibrary = require('./libraries/vietinBank.library');
const OCBBankLibrary = require('./libraries/ocbBank.library');
const BIDVBankLibrary = require('./libraries/bidvBank.library');
const NapasLibrary = require('./libraries/napas.library');
const WalletLibrary = require('./libraries/wallet.library');

module.exports = {
	name: 'eWalletDeposit',

	version: 1,

	mixins: [moleculerI18n, DbService],
	i18n: {
		directory: path.join(__dirname, 'locales'),
		locales: ['vi', 'en'],
		defaultLocale: 'vi'
	},
	/**
   * Settings
   */
	settings: {
	},

	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		depositBE: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/deposit/linked',
				auth: false
			},
			params: {
				body: {
					$$type: 'object',
					phone: 'string',
					linkedId: 'number',
					amount: 'number',
					envName: 'string|optional',
					ipnUrl: 'string|optional'

				}
			},
			handler: require('./actions/deposit.action.rest')
		},
		verifyDepositBE: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/deposit/verifyLinked',
				auth: false
			},
			params: {
				body: {
					$$type: 'object',
					otp: 'string',
					phone: 'string',
					linkedId: 'number',
					transaction: 'string'

				}
			},
			timeout: 50000,
			handler: require('./actions/verifyDeposit.action.rest')
		},
		ipnDepositBE:{
			rest:{
				method: 'POST',
				fullPath: '/ewallet/deposit/napas/linked/ipn',
				auth: false,
				security: false
			},
			params: {},
			timeout: 50000,
			handler: require('./actions/ipnNapasLinked.action.rest')
		},
		test: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/test',
				auth: false,
				security: false
			},
			params: {
				body: {
					$$type: 'object',
					number: 'string'
				}
			},
			timeout: 50000,
			handler: require('./actions/test.action.rest')
		},
		depositEWallet: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/sdk/deposit',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					amount: 'number',
					transaction: 'string|optional',
					method: {
						type: 'object',
						strict: 'remove',
						props: {
							bankCard: {
								$$type: 'object|optional',
								cardNumber: 'string',
								// cardHolder: 'string',
								issuedAt: 'string|optional',
								expiredAt: 'string|optional',
								notifyUrl: 'string|optional',
								envName: 'string|optional'
							},
							linked: {
								$$type: 'object|optional',
								linkedId: 'number',
								otp: 'string|optional',
								notifyUrl: 'string|optional',
								envName: 'string|optional'
							}
						},
						minProps: 1,
						maxProps: 1
					}
				}
			},
			timeout: 50000,
			handler: require('./actions/depositEwallet.action.rest')
		},
		ipnDepositEwallet: {
			params: {
				accountId: 'number',
				state: 'string',
				transaction: 'string',
				method: 'string',
				payment: 'object'
			},
			timeout: 50000,
			handler: require('./actions/ipnDeposit.action')
		}
	},

	/**
   * Events
   */
	events: {

	},

	/**
   * Methods
   */
	methods: {
		GetAuthInfo: require('./methods/getAuthInfo.method'),
		RedisCache: require('./methods/redisCache.method'),
		CalcFee: require('./methods/calcFee.method'),
		PayNapas: require('./methods/payNapas.method'),
		PayOCB: require('./methods/payOCB.method'),
		PayBIDV: require('./methods/payBIDV.method'),
		PayPVCBank: require('./methods/payPVC.method'),
		PayVietin: require('./methods/payVietin.method'),
		CardDescription: require('./methods/cardDescription.method'),
		PhoneUtility: require('./methods/phoneUtility.method'),
		HiddenCardNumber: require('./methods/hiddenCardNumber.method')
	},
	/**
   * Service created lifecycle event handler
   */
	created() {
		try {
			_.set(this, 'PVComBankLibs', PVComBankLibrary({ logger: this.logger.info }));
			_.set(this, 'VietinBankLibs', VietinBankLibrary({ logger: this.logger.info }));
			_.set(this, 'OCBBankLibs', OCBBankLibrary({ logger: this.logger.info }));
			_.set(this, 'BIDVBankLibs', BIDVBankLibrary({ logger: this.logger.info }));
			_.set(this, 'NapasLibs', NapasLibrary({ logger: this.logger.info }));
			_.set(this, 'WalletLibs', WalletLibrary({ logger: this.logger.info }));
		} catch (error) {
			this.logger.info(`[Generate_Library_Error] => ${error}`);
		}
	},

	/**
   * Service started lifecycle event handler
   */
	async started() {
	},

	/**
   * Service stopped lifecycle event handler
   */

	// async stopped() {},

	async afterConnected() {
		this.logger.info('Connected successfully...');
	}
};
