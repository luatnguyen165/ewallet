const _ = require('lodash');
const moment = require('moment');
const Decimal = require('decimal.js');
const { MoleculerError } = require('moleculer').Errors;
const ErrorCode = require('../constants/errorCode.constant');
const DepositConstant = require('../constants/deposit.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	try {
		const {
			phone,
			linkedId
		} = _.get(ctx, 'params.body');
		const payload = _.get(ctx, 'params.body');

		const authInfo = this.GetAuthInfo(ctx.meta);

		Logger(`[EWALLET_DEPOSIT] => [payload] => ${JSON.stringify(_.get(ctx, 'params.body'))}`);

		const eWalletAppInfo = await this.broker.call('v1.ewalletAppInfoModel.findOne', [{
			merchantId: authInfo.merchantId
		}]);

		if (_.get(eWalletAppInfo, 'id', null) === null) {
			return {
				code: ErrorCode.VERIFY_DEPOSIT_CODE.FAILED,
				message: 'Không tìm thấy thông tin kết nối'
			};
		}

		const checkPhone = this.PhoneUtility(phone);
		if (!checkPhone.isValid) {
			return {
				code: ErrorCode.VERIFY_DEPOSIT_CODE.FAILED,
				message: 'Số điện thoại không hợp lệ'
			};
		}
		const { phoneFormatted } = checkPhone;

		const connectedUserInfo = await this.broker.call('v1.eWalletConnectedUserModel.findOne', [{
			phone: phoneFormatted,
			appId: eWalletAppInfo.id
		}]);

		if (_.get(connectedUserInfo, 'id', null) === null) {
			return {
				code: ErrorCode.VERIFY_DEPOSIT_CODE.FAILED,
				message: 'Số điện thoại này chưa liên kết với Ví PayME'
			};
		}

		const accountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [{
			phone: phoneFormatted
		}]);

		if (_.get(connectedUserInfo, 'id', null) === null) {
			return {
				code: ErrorCode.VERIFY_DEPOSIT_CODE.FAILED,
				message: 'Số điện thoại này chưa liên kết với Ví PayME'
			};
		}

		const dataDeposit = {
			accountId: accountInfo.id,
			transaction: payload.transaction,
			appId: eWalletAppInfo.id,
			state: DepositConstant.DEPOSIT_STATE.PENDING
		};

		let depositCreated = await this.broker.call('v1.eWalletDepositModel.findOne', [{ ...dataDeposit }]);

		if (_.get(depositCreated, 'id', null) === null) {
			return {
				code: ErrorCode.VERIFY_DEPOSIT_CODE.FAILED,
				message: 'Xác thực nạp tiền thất bại. Giao dịch này không tồn tại hoặc đã hoàn thành trước đó. (1)'
			};
		}

		// Xet linkedId
		const linkedInfo = await this.broker.call('v1.eWalletLinkedModel.findOne', [{
			id: _.get(depositCreated, 'extraData.linkedId', 0),
			accountId: accountInfo.id
		}]);

		if (_.get(linkedInfo, 'id', null) === null) {
			return {
				code: ErrorCode.VERIFY_DEPOSIT_CODE.FAILED,
				message: 'LinkedId sai hoặc không tồn tại (1)'
			};
		}

		if (linkedInfo.supplier === DepositConstant.SUPPLIER.NAPAS_GATEWAY) {
			return {
				code: ErrorCode.VERIFY_DEPOSIT_CODE.FAILED,
				message: 'Thẻ liên kết Napas không hỗ trợ xác thực OTP. Vui lòng kiểm tra lại thông tin thẻ'
			};
		}

		const paymentMethodData = await this.broker.call('v1.eWalletPaymentMethodModel.findOne', [{
			'paymentInfo.linkedId': linkedId
		}]);

		if (_.get(paymentMethodData, 'id', null) === null) {
			return {
				code: ErrorCode.VERIFY_DEPOSIT_CODE.FAILED,
				message: 'LinkedId sai hoặc không tồn tại (2)'
			};
		}

		if (_.get(paymentMethodData, 'group', '') === DepositConstant.PAYMENT_METHOD_GROUP.LINKED_CREDIT) {
			return {
				code: ErrorCode.VERIFY_DEPOSIT_CODE.FAILED,
				message: 'Thẻ không có quyền nạp tiền'
			};
		}

		const paymentData = {
			accountId: accountInfo.id,
			appId: eWalletAppInfo.id,
			transaction: payload.transaction,
			state: DepositConstant.DEPOSIT_STATE.PENDING
		};

		const paymentCreated = await this.broker.call('v1.eWalletPaymentModel.findOne', [paymentData]);

		if (_.get(paymentCreated, 'id', null) === null) {
			return {
				code: ErrorCode.VERIFY_DEPOSIT_CODE.FAILED,
				message: 'Xác thực nạp tiền thất bại. Giao dịch này không tồn tại hoặc đã hoàn thành trước đó. (2)'
			};
		}

		const dataHistory = {
			accountId: accountInfo.id,
			appId: eWalletAppInfo.id,
			'service.type': 'DEPOSIT',
			'service.id': depositCreated.id,
			'service.transaction': depositCreated.transaction,
			'service.state': DepositConstant.DEPOSIT_STATE.PENDING,
			state: DepositConstant.DEPOSIT_STATE.PENDING
		};
		// console.log(dataHistory);

		let historyDepositCreated = await Broker.call('v1.eWalletHistoryModel.findOne', [dataHistory]);
		if (_.get(historyDepositCreated, 'id', null) === null) {
			return {
				code: ErrorCode.VERIFY_DEPOSIT_CODE.FAILED,
				message: 'Xác thực nạp tiền thất bại. Giao dịch này không tồn tại hoặc đã hoàn thành trước đó. (3)'
			};
		}

		// call supplier
		let supplierResponse = null;

		switch (linkedInfo.supplier) {
			case DepositConstant.SUPPLIER.BIDVBANK: {
				let version = _.get(linkedInfo, 'cardInfo.version', 2);

				if (_.isNil(version)) {
					version = 1;
				}

				const params = {
					accountId: accountInfo.id,
					transaction: payload.transaction,
					otp: payload.otp,
					cardId: _.get(linkedInfo, 'linkedInfo.cardId', null),
					version
				};
				supplierResponse = await this.PayBIDV().VerifyPay(params);
				break;
			}
			case DepositConstant.SUPPLIER.OCBBANK: {
				const params = {
					accountId: accountInfo.id,
					transaction: payload.transaction,
					otp: payload.otp,
					amount: depositCreated.total,
					phone: accountInfo.phone,
					cardId: _.get(linkedInfo, 'linkedInfo.cardId', null)
				};
				supplierResponse = await this.PayOCB().VerifyPay(params);
				break;
			}
			case DepositConstant.SUPPLIER.PVCOMBANK: {
				const params = {
					accountId: accountInfo.id,
					transaction: payload.transaction,
					otp: payload.otp,
					cardId: _.get(linkedInfo, 'linkedInfo.cardId', null)
				};
				supplierResponse = await this.PayPVCBank().VerifyPay(params);
				break;
			}
			case DepositConstant.SUPPLIER.VIETINBANK: {
				const params = {
					accountId: accountInfo.id,
					transaction: payload.transaction,
					otp: payload.otp,
					cardId: _.get(linkedInfo, 'linkedInfo.cardId', null)
				};
				supplierResponse = await this.PayVietin().VerifyPay(params);
				break;
			}

			default:
				break;
		}

		if (supplierResponse.state === DepositConstant.DEPOSIT_STATE.INVALID_OTP) {
			return {
				code: ErrorCode.VERIFY_DEPOSIT_CODE.INVALID_OTP,
				message: 'Mã OTP không đúng. Vui lòng kiểm tra lại'
			};
		}

		const response = {
			code: ErrorCode.VERIFY_DEPOSIT_CODE.FAILED,
			message: 'Thanh toán thất bại. Vui lòng thử lại sau'
		};
		let depositDataUpdate = {
			payment: {
				id: paymentCreated.id,
				transaction: payload.transaction,
				method: 'LINKED',
				state: supplierResponse.state,
				payment: _.get(paymentMethodData, 'paymentInfo', {})
			}
		};
		let updateHistoryData = {};

		// Xet state cua supplier de tra ve kq cho dung
		if (supplierResponse.state === GeneralConstant.SUPPLIER_STATE.FAILED) {
			updateHistoryData = {
				...updateHistoryData,
				state: DepositConstant.DEPOSIT_STATE.FAILED,
				'service.state': DepositConstant.DEPOSIT_STATE.FAILED,
				'service.data.state': DepositConstant.DEPOSIT_STATE.FAILED,
				'payment.state': DepositConstant.DEPOSIT_STATE.FAILED,
				publishedAt: new Date().toISOString()
			};

			depositDataUpdate = {
				...depositDataUpdate,
				state: DepositConstant.DEPOSIT_STATE.FAILED
			};
		} else if (supplierResponse.state === GeneralConstant.SUPPLIER_STATE.SUCCEEDED) {
			updateHistoryData = {
				...updateHistoryData,
				state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
				'service.state': DepositConstant.DEPOSIT_STATE.SUCCEEDED,
				'service.data.state': DepositConstant.DEPOSIT_STATE.SUCCEEDED,
				'payment.state': DepositConstant.DEPOSIT_STATE.SUCCEEDED,
				publishedAt: new Date().toISOString()
			};
			depositDataUpdate = {
				...depositDataUpdate,
				state: DepositConstant.DEPOSIT_STATE.SUCCEEDED
			};

			const paramsRequest = {
				accountId: linkedInfo.accountId,
				amount: depositCreated.amount,
				description: 'Nạp tiền vào ví PayME.',
				referData: depositCreated
			};
			// WalletLibs
			let resultDeposit = null;
			try {
				Logger(`[DEPOSIT] => [DEPOSIT_WALLET_PARAMS]: ${JSON.stringify(paramsRequest)}`);
				resultDeposit = await this.WalletLibs.Deposit(paramsRequest);
				Logger(`[DEPOSIT] => [DEPOSIT_WALLET]: ${JSON.stringify(resultDeposit)}`);

				updateHistoryData = {
					...updateHistoryData,
					balance: resultDeposit.balance
				};
			} catch (error) {
				depositDataUpdate = {
					...depositDataUpdate,
					state: DepositConstant.DEPOSIT_STATE.FAILED
				};
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E06)';
				Logger(`[DEPOSIT] => [DEPOSIT_WALLET_ERROR]: ${error}`);
			}

			if (resultDeposit.succeeded === false) {
				updateHistoryData = {
					...updateHistoryData,
					state: DepositConstant.DEPOSIT_STATE.FAILED,
					'service.state': DepositConstant.DEPOSIT_STATE.FAILED,
					'service.data.state': DepositConstant.DEPOSIT_STATE.FAILED,
					publishedAt: new Date().toISOString()
				};
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E07)';
				depositDataUpdate = {
					...depositDataUpdate,
					state: DepositConstant.DEPOSIT_STATE.FAILED
				};
			}

			response.code = ErrorCode.VERIFY_DEPOSIT_CODE.SUCCEED;
			response.data = {
				transaction: depositCreated.transaction
			};
			response.message = 'Nạp tiền thành công';
		}

		depositCreated = await Broker.call('v1.eWalletDepositModel.findOneAndUpdate', [
			{ id: depositCreated.id },
			depositDataUpdate
		]);

		historyDepositCreated = await Broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [
			{ id: historyDepositCreated.id }, updateHistoryData]);

		return response;
	} catch (error) {
		console.log(error);
		Logger('[OPENEWALLET] >>> [DEPOSIT] >> [ERROR] >>', JSON.stringify(error));

		return {
			code: ErrorCode.VERIFY_DEPOSIT_CODE.FAILED,
			message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(E00)'
		};
	}
};
