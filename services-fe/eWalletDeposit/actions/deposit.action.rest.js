const _ = require('lodash');
const moment = require('moment');
const Decimal = require('decimal.js');
const { MoleculerError } = require('moleculer').Errors;
const ErrorCode = require('../constants/errorCode.constant');
const DepositConstant = require('../constants/deposit.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	try {
		const {
			phone,
			linkedId,
			amount
		} = _.get(ctx, 'params.body');
		const payload = _.get(ctx, 'params.body');
		const authInfo = this.GetAuthInfo(ctx.meta);

		Logger(`[EWALLET_DEPOSIT] => [payload] => ${JSON.stringify(_.get(ctx, 'params.body'))}`);

		const eWalletAppInfo = await this.broker.call('v1.ewalletAppInfoModel.findOne', [{
			merchantId: authInfo.merchantId
		}]);

		if (_.get(eWalletAppInfo, 'id', null) === null) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Không tìm thấy thông tin kết nối'
			};
		}

		const checkPhone = this.PhoneUtility(phone);
		if (!checkPhone.isValid) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Số điện thoại không hợp lệ'
			};
		}
		const { phoneFormatted } = checkPhone;

		const connectedUserInfo = await this.broker.call('v1.eWalletConnectedUserModel.findOne', [{
			phone: phoneFormatted,
			appId: eWalletAppInfo.id
		}]);

		if (_.get(connectedUserInfo, 'id', null) === null) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Số điện thoại này chưa liên kết với Ví PayME'
			};
		}

		const accountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [{
			phone: phoneFormatted
		}]);

		if (_.get(connectedUserInfo, 'id', null) === null) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Số điện thoại này chưa liên kết với Ví PayME'
			};
		}

		let settingKey = null;
		// Xet linkedId
		const linkedInfo = await this.broker.call('v1.eWalletLinkedModel.findOne', [{
			id: linkedId,
			accountId: accountInfo.id
		}]);

		if (_.get(linkedInfo, 'id', null) === null) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'LinkedId sai hoặc không tồn tại (1)'
			};
		}

		if (linkedInfo.supplier === DepositConstant.SUPPLIER.NAPAS_GATEWAY) {
			settingKey = 'NAPAS_LINKED';
		} else {
			settingKey = 'LINKED';
		}

		const paymentMethodData = await this.broker.call('v1.eWalletPaymentMethodModel.findOne', [{
			'paymentInfo.linkedId': linkedId
		}]);

		if (_.get(paymentMethodData, 'id', null) === null) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'LinkedId sai hoặc không tồn tại (2)'
			};
		}

		if (_.get(paymentMethodData, 'group', '') === DepositConstant.PAYMENT_METHOD_GROUP.LINKED_CREDIT) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Thẻ không có quyền nạp tiền'
			};
		}

		let valueDepositConfig = await Broker.call('v1.eWalletSettingModel.findOne', [{
			key: 'fee.deposit.config'
		}]);

		valueDepositConfig = valueDepositConfig.value;

		const calFeeResult = await this.CalcFee({
			accountId: accountInfo.id,
			amount,
			feeConfig: valueDepositConfig,
			settingKey: DepositConstant.FEE_SETTING_KEY[settingKey],
			chargeFeeKey: `DepositChargeFee_${accountInfo.id}`,
			filter: {
				accountId: accountInfo.id,
				appId: eWalletAppInfo.id,
				state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
				'method.group': { $ne: 'CREDIT_CARD' }
			}
		});
		if (_.isNumber(_.get(calFeeResult, 'fee')) === false) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(E02)'
			};
		}

		const { fee } = calFeeResult;
		const totalPay = _.toNumber((new Decimal(amount)).add(new Decimal(fee)));

		let transaction = null;
		try {
			transaction = await this.broker.call('v1.walletUuid.pick', { prefix: 'DEPOSIT' });
		} catch (error) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Có lỗi xảy ra khi nạp tiền. Vui lòng thử lại sau.'
			};
		}

		if (_.isNull(transaction)) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(E01)'
			};
		}

		const dataDeposit = {
			accountId: linkedInfo.accountId,
			transaction,
			appId: eWalletAppInfo.id,
			amount,
			fee,
			total: totalPay,
			description: 'Nap tien vao Vi PayME',
			state: DepositConstant.DEPOSIT_STATE.PENDING,
			method: {
				id: _.get(paymentMethodData, 'id', 0),
				group: DepositConstant.PAYMENT_METHOD_GROUP.LINKED_GATEWAY,
				description: 'Tài khoản liên kết.',
				bankInfo: _.get(paymentMethodData, 'paymentInfo', {})
			},
			extraData: {
				linkedId: linkedInfo.id,
				ipnUrl: _.get(payload, 'ipnUrl', null),
				merchantId: authInfo.merchantId
			}
		};

		let depositCreated = await this.broker.call('v1.eWalletDepositModel.create', [{ ...dataDeposit }]);

		if (_.get(depositCreated, 'id', null) === null) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(E03)'
			};
		}

		const paymentData = {
			accountId: accountInfo.id,
			appId: eWalletAppInfo.id,
			transaction,
			amount: totalPay,
			state: DepositConstant.DEPOSIT_STATE.PENDING,
			method: 'LINKED',
			service: {
				code: 'DEPOSIT',
				type: 'DEPOSIT',
				transaction
			},
			description: 'Nạp tiền vào ví PayME'
		};

		let paymentCreated = null;
		try {
			paymentCreated = await this.broker.call('v1.eWalletPaymentModel.create', [paymentData]);
		} catch (error) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(E04)'
			};
		}
		if (_.get(paymentCreated, 'id', null) === null) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(E05)'
			};
		}

		const cardDescription = await this.CardDescription(_.get(paymentMethodData, 'paymentInfo', {}));
		const dataHistory = {
			accountId: linkedInfo.accountId,
			appId: eWalletAppInfo.id,
			service: {
				type: 'DEPOSIT',
				code: 'DEPOSIT',
				id: depositCreated.id,
				transaction: depositCreated.transaction,
				method: 'DEPOSIT',
				state: DepositConstant.DEPOSIT_STATE.PENDING,
				name: 'Nạp tiền bằng Tài khoản liên kết',
				data: {
					id: depositCreated.id,
					resolveType: 'DepositObject',
					state: DepositConstant.DEPOSIT_STATE.PENDING,
					transaction,
					accountId: accountInfo.id
				}
			},
			amount,
			total: totalPay,
			fee,
			state: DepositConstant.DEPOSIT_STATE.PENDING,
			description: 'Nap tien vao Vi PayME',
			changed: '+',
			tags: ['DEPOSIT'],
			payment: {
				id: paymentCreated.id,
				transaction,
				method: 'LINKED',
				state: DepositConstant.DEPOSIT_STATE.PENDING,
				description: cardDescription
			}
		};

		let historyDepositCreated = null;
		try {
			historyDepositCreated = await Broker.call('v1.eWalletHistoryModel.create', [dataHistory]);
		} catch (error) {
			Logger(`[DEPOSIT_EWALLET] => [CREATE_HISTORY_DEPOSIT_ERROR]: ${error}`);
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(E04)'
			};
		}
		if (_.get(historyDepositCreated, 'id', null) === null) {
			Logger(`[DEPOSIT_EWALLET] => [CREATE_HISTORY_DEPOSIT_ERROR_2]: ${JSON.stringify(dataHistory)}`);
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(E05)'
			};
		}

		// call supplier
		let supplierResponse = null;

		switch (linkedInfo.supplier) {
			case DepositConstant.SUPPLIER.NAPAS_GATEWAY: {
				_.set(linkedInfo, 'cardInfo.linkedId', linkedInfo.id);
				const params = {
					accountId: accountInfo.id,
					transaction,
					paymentId: paymentCreated.id,
					amount: totalPay,
					cardId: _.get(linkedInfo, 'linkedInfo.cardId', null),
					cardInfo: _.get(linkedInfo, 'cardInfo', {}),
					envName: _.get(payload, 'envName', 'MobileApp'),
					redirectUrl: _.get(payload.payment, 'linked.redirectUrl', '')
				};
				supplierResponse = await this.PayNapas(params);
				break;
			}
			case DepositConstant.SUPPLIER.BIDVBANK: {
				let version = _.get(linkedInfo, 'cardInfo.version', 2);

				if (_.isNil(version)) {
					version = 1;
				}

				const params = {
					accountId: accountInfo.id,
					transaction,
					paymentId: paymentCreated.id,
					amount: totalPay,
					cardId: _.get(linkedInfo, 'linkedInfo.cardId', null),
					cardInfo: _.get(paymentMethodData, 'paymentInfo', {}),
					version
				};
				supplierResponse = await this.PayBIDV().Pay(params);
				break;
			}
			case DepositConstant.SUPPLIER.OCBBANK: {
				const params = {
					accountId: accountInfo.id,
					transaction,
					paymentId: paymentCreated.id,
					amount,
					phone: accountInfo.phone,
					cardInfo: _.get(paymentMethodData, 'paymentInfo', {}),
					cardId: _.get(linkedInfo, 'linkedInfo.cardId', null)
				};
				supplierResponse = await this.PayOCB().Pay(params);
				break;
			}
			case DepositConstant.SUPPLIER.PVCOMBANK: {
				const params = {
					accountId: accountInfo.id,
					transaction,
					paymentId: paymentCreated.id,
					cardInfo: _.get(paymentMethodData, 'paymentInfo', {}),
					amount,
					cardId: _.get(linkedInfo, 'linkedInfo.cardId', null)
				};
				supplierResponse = await this.PayPVCBank().Pay(params);
				break;
			}
			case DepositConstant.SUPPLIER.VIETINBANK: {
				const params = {
					accountId: accountInfo.id,
					transaction,
					paymentId: paymentCreated.id,
					amount,
					cardInfo: _.get(paymentMethodData, 'paymentInfo', {}),
					cardId: _.get(linkedInfo, 'linkedInfo.cardId', null)
				};
				supplierResponse = await this.PayVietin().Pay(params);
				break;
			}

			default:
				break;
		}

		// Logic of Payment (Update Record)
		const updatePaymentData = {
			supplierTransaction: supplierResponse.supplierTransaction,
			methodData: {
				resolveType: DepositConstant.RESOLVE_TYPE.LINKED,
				linkedId: linkedInfo.id,
				paymentLinkedId: supplierResponse.paymentLinkedId,
				bankInfo: _.get(linkedInfo, 'cardInfo', {})
			}
		};
		let updateHistoryData = {};
		if (_.includes([DepositConstant.DEPOSIT_STATE.SUCCEEDED,
			DepositConstant.DEPOSIT_STATE.FAILED], supplierResponse.state)) {
			updatePaymentData.state = supplierResponse.state;

			updateHistoryData['payment.state'] = supplierResponse.state;
		}

		const checkPaymentUpdate = await Broker.call('v1.eWalletPaymentModel.updateOne', [
			{ id: paymentCreated.id }, updatePaymentData]);

		if (_.get(checkPaymentUpdate, 'nModified', 0) < 1) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(E06)'
			};
		}

		// Truong hop Require_OTP
		if (supplierResponse.state === DepositConstant.DEPOSIT_STATE.REQUIRED_OTP) {
			return {
				code: ErrorCode.DEPOSIT_CODE.REQUIRE_OTP,
				message: _.get(supplierResponse, 'message', 'Nạp tiền cần xác thực OTP'),
				data: { transaction }
			};
		}

		// Truong hop Require_Verify
		if (supplierResponse.state === DepositConstant.DEPOSIT_STATE.REQUIRED_VERIFY) {
			return {
				code: ErrorCode.DEPOSIT_CODE.REQUIRE_VERIFY,
				message: _.get(supplierResponse, 'message', 'Nạp tiền cần xác thực từ ngân hàng'),
				data: {
					html: _.get(supplierResponse, 'html', null), 
					transaction 
				}
			};
		}


		// Cap nhat lai trang thai Failed, succeed o dich vu Deposit
		let depositDataUpdate = {
			payment: {
				id: paymentCreated.id,
				transaction,
				method: 'LINKED',
				state: supplierResponse.state,
				description: cardDescription,
				payment: _.get(paymentMethodData, 'paymentInfo', {})
			}
		};
		const response = {
			code: ErrorCode.DEPOSIT_CODE.FAILED,
			message: _.get(supplierResponse, 'message', 'Nạp tiền thất bại'),
			data: { transaction }
		};
		if (supplierResponse.state === DepositConstant.DEPOSIT_STATE.FAILED) {
			updateHistoryData = {
				...updateHistoryData,
				state: DepositConstant.DEPOSIT_STATE.FAILED,
				'service.state': DepositConstant.DEPOSIT_STATE.FAILED,
				'service.data.state': DepositConstant.DEPOSIT_STATE.FAILED,
				publishedAt: new Date().toISOString()
			};

			depositDataUpdate = {
				...depositDataUpdate,
				state: DepositConstant.DEPOSIT_STATE.FAILED
			};
		} else if (supplierResponse.state === DepositConstant.DEPOSIT_STATE.SUCCEEDED) {
			updateHistoryData = {
				...updateHistoryData,
				state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
				'service.state': DepositConstant.DEPOSIT_STATE.SUCCEEDED,
				'service.data.state': DepositConstant.DEPOSIT_STATE.SUCCEEDED,
				publishedAt: new Date().toISOString()
			};
			depositDataUpdate = {
				...depositDataUpdate,
				state: DepositConstant.DEPOSIT_STATE.SUCCEEDED
			};

			const paramsRequest = {
				accountId: linkedInfo.accountId,
				amount,
				description: 'Nạp tiền vào ví PayME.',
				referData: depositCreated
			};
			// WalletLibs
			let resultDeposit = null;
			try {
				Logger(`[DEPOSIT] => [DEPOSIT_WALLET_PARAMS]: ${JSON.stringify(paramsRequest)}`);
				resultDeposit = await this.WalletLibs.Deposit(paramsRequest);
				Logger(`[DEPOSIT] => [DEPOSIT_WALLET]: ${JSON.stringify(resultDeposit)}`);

				updateHistoryData = {
					...updateHistoryData,
					balance: resultDeposit.balance
				};
			} catch (error) {
				depositDataUpdate = {
					...depositDataUpdate,
					state: DepositConstant.DEPOSIT_STATE.FAILED
				};
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E06)';
				Logger(`[DEPOSIT] => [DEPOSIT_WALLET_ERROR]: ${error}`);
			}

			// Neu kq tra ra la false
			if (resultDeposit.succeeded === false) {
				updateHistoryData = {
					...updateHistoryData,
					state: DepositConstant.DEPOSIT_STATE.FAILED,
					'service.state': DepositConstant.DEPOSIT_STATE.FAILED,
					'service.data.state': DepositConstant.DEPOSIT_STATE.FAILED
				};
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E07)';
				depositDataUpdate = {
					...depositDataUpdate,
					state: DepositConstant.DEPOSIT_STATE.FAILED
				};
			} else {
				// Neu kq la true thi tra ve response code SUCCEED
				response.data = { transaction };
				response.code = ErrorCode.DEPOSIT_CODE.SUCCEED;
				response.message = 'Nạp tiền thành công';
			}
		}

		depositCreated = await Broker.call('v1.eWalletDepositModel.findOneAndUpdate', [
			{ id: depositCreated.id },
			depositDataUpdate
		]);

		historyDepositCreated = await Broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [
			{ id: historyDepositCreated.id }, updateHistoryData]);

		return response;
	} catch (error) {
		console.log(error);
		Logger('[OPENEWALLET] >>> [DEPOSIT] >> [ERROR] >>', JSON.stringify(error));

		return {
			code: ErrorCode.DEPOSIT_CODE.FAILED,
			message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(E00)'
		};
	}
};
