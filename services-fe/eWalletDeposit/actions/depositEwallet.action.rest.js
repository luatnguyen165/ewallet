const _ = require('lodash');
const Decimal = require('decimal.js');
const numeral = require('numeral');
const ErrorCode = require('../constants/errorCode.constant');

const AccountConstant = require('../constants/account.constant');
const DepositConstant = require('../constants/deposit.constant');
const LinkCardConstant = require('../constants/linkCard.constant');
const PaymentConstant = require('../constants/payment.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	const authInfo = _.get(ctx, 'meta.auth.data', {});
	const payload = _.get(ctx, 'params.body', {});
	const response = {
		code: ErrorCode.DEPOSIT_EWALLET.FAILED,
		message: 'Nạp tiền thất bại. Vui lòng thử lại sau. (ERR_01)',
		data: {}
	};

	try {
		if (_.get(authInfo, 'kyc.state', null) !== AccountConstant.KYC_STATE.APPROVED) {
			response.message = 'Tài khoản chưa định danh. Vui lòng thực hiện định danh.';
			return response;
		}

		const limitDepositConfig = await Broker.call('v1.eWalletSettingModel.findOne', [{
			key: DepositConstant.SEETING_KEY.LIMIT_AMOUNT_SETTING_KEY
		}]);

		let limitDepositValue = limitDepositConfig.value;
		if (_.get(limitDepositValue, 'min', false) === false) limitDepositValue = { min: 10000, max: 100000000 };

		if (payload.amount < limitDepositValue.min) {
			response.message = `Số tiền nạp không được nhỏ hơn ${numeral(limitDepositValue.min).format('0,0')} đ`;
			return response;
		}
		if (payload.amount > limitDepositValue.max) {
			response.message = `Số tiền nạp không vượt quá ${numeral(limitDepositValue.max).format('0,0')} đ`;
			return response;
		}

		// Setting Key using for calc Fee
		let settingGetFeeKey = null;
		let bankInfo = {};
		const paymentKey = _.keys(payload.method)[0];
		let paymentMethodData = {};

		/* If User Using linked Card to Deposit */
		// Check its own logic and get the fee deposit key
		if (paymentKey === DepositConstant.DEPOSIT_PAYLOAD_KEY.LINKED) {
			const linkedId = _.get(payload, 'method.linked.linkedId', 0);
			const linkedInfo = await Broker.call('v1.eWalletLinkedModel.findOne', [{
				id: linkedId,
				accountId: authInfo.accountId,
				state: LinkCardConstant.STATE.LINKED
			}, '-_id id cardInfo supplier']);

			if (_.get(linkedInfo, 'id', null) === null) {
				response.message = 'Không tồn tại thẻ liên kết này. (ERR_01)';
				return response;
			}

			/** Credit Card */
			if (_.get(linkedInfo, 'supplier', null) === LinkCardConstant.SUPPLIER.PG) {
				response.message = 'Thẻ không có quyền nạp tiền';
				return response;
			}

			const paymentMethodInfo = await Broker.call('v1.eWalletPaymentMethodModel.findOne', [{
				accountId: authInfo.accountId,
				'paymentInfo.linkedId': linkedId
			}, '-_id id']);

			if (_.get(paymentMethodInfo, 'id', null) === null) {
				response.message = 'Không tồn tại thẻ liên kết này. (ERR_02)';
				return response;
			}

			if (linkedInfo.supplier === LinkCardConstant.SUPPLIER.NAPAS_GATEWAY) {
				settingGetFeeKey = 'NAPAS_LINKED';
			} else {
				settingGetFeeKey = 'LINKED';
			}

			bankInfo = linkedInfo.cardInfo;
			paymentMethodData = {
				id: paymentMethodInfo.id,
				group: DepositConstant.PAYMENT_METHOD_GROUP.LINKED_GATEWAY,
				description: 'Tài khoản liên kết.',
				bankInfo: _.get(paymentMethodData, 'paymentInfo', {})
			};
		}

		/* If User Using Bank Card to Deposit */
		// Check its own logic and get the fee deposit key
		if (paymentKey === DepositConstant.DEPOSIT_PAYLOAD_KEY.BANK_CARD) {
			settingGetFeeKey = 'NAPAS';
			bankInfo = _.get(payload, 'method.bankCard', {});
			const prefix = bankInfo.cardNumber.substring(0, 6);

			const bankCodeSupportList = await Broker.call('v1.eWalletBankCodeModel.findMany',
				[{ isActive: true }]);
			_.forEach(bankCodeSupportList, (bankConfig) => {
				if (_.get(bankConfig, 'card.atm.prefix', false) && _.isEqual(bankConfig.card.atm.prefix, prefix)) {
					bankInfo.swiftCode = bankConfig.swiftCode;
					bankInfo.bankName = bankConfig.vi;
					bankInfo.shortName = bankConfig.shortName;
				}
			});

			if (_.get(bankInfo, 'swiftCode', null) === null) {
				response.code = ErrorCode.DEPOSIT_EWALLET.FAILED;
				response.message = 'Ngân Hàng chưa được hỗ trợ';
				return response;
			}

			const accountNameSplited = _.split(_.toUpper(authInfo.fullname), ' ');
			let cardHolderNameDetect = null;
			try {
				const data = await Broker.call('v1.utility.detectBank', {
					body: {
						cardNumber: bankInfo.cardNumber
					}
				});

				console.log(data);
				cardHolderNameDetect = _.get(data, 'data.fullname', '');
			} catch (err) {
				Logger('[DEPOSIT] -> [BANKCARD] -> [DETECT] -> :: ', err);
				response.code = ErrorCode.DEPOSIT_EWALLET.CARDNAME_DONT_MATCH;
				response.message = 'Tên chủ thẻ phải trùng tên Ví (E01)';
			}

			const cardHolderNameSplited = _.split(cardHolderNameDetect, ' ');
			const accountNameSplitedLength = accountNameSplited.length;
			const cardHolderNameSplitedLength = cardHolderNameSplited.length;

			let isShortCheck = false;
			if (accountNameSplited && cardHolderNameSplited
				&& accountNameSplitedLength >= 2 && cardHolderNameSplitedLength >= 2) {
				// Short Check
				if (accountNameSplited[0] === cardHolderNameSplited[0]
					&& accountNameSplited[accountNameSplitedLength - 1] === cardHolderNameSplited[cardHolderNameSplitedLength - 1]) {
					isShortCheck = true;
				}
			}
			if (isShortCheck === false) {
				const startNumCheckAccountName = accountNameSplitedLength - 2;
				const startNumCheckCardHolder = cardHolderNameSplitedLength - 2;

				if ((accountNameSplited[startNumCheckAccountName + 1] !== cardHolderNameSplited[startNumCheckCardHolder + 1])
						|| (accountNameSplited[startNumCheckAccountName] !== cardHolderNameSplited[startNumCheckCardHolder])) {
					response.code = ErrorCode.DEPOSIT_EWALLET.CARDNAME_DONT_MATCH;
					response.message = 'Tên chủ thẻ phải trùng tên Ví (E02)';
					return response;
				}
			}

			paymentMethodData = {
				id: 5,
				bankInfo,
				description: 'Thẻ ATM nội địa',
				group: 'GATEWAY'
			};
		}

		// STEP
		// CALC specialFee For Special Bank
		let fee = 0;
		try {
			const swiftCode = _.get(bankInfo, 'swiftCode', '');
			let feeConfigBank = await Broker.call('v1.eWalletSettingModel.findOne', [{
				key: 'fee.deposit.config.byBank'
			}]);

			feeConfigBank = feeConfigBank.value;
			if (feeConfigBank) {
				feeConfigBank = JSON.parse(feeConfigBank);
			}

			if (feeConfigBank && feeConfigBank.listBank) {
				const overAmount = new Decimal(payload.amount);

				const targetConfig = _.find(feeConfigBank.listBank, (value) => value.swiftCode === swiftCode) || {};
				if (targetConfig && targetConfig.feeRate) {
					fee = overAmount.times(targetConfig.feeRate).div(new Decimal('100')).ceil();
					fee = _.toNumber(fee);
				}
			}
		} catch (exeption) {}

		if (fee === 0) {
			let valueDepositConfig = await Broker.call('v1.eWalletSettingModel.findOne', [{
				key: 'fee.deposit.config'
			}]);

			valueDepositConfig = valueDepositConfig.value;

			const calFeeResult = await this.CalcFee({
				accountId: authInfo.accountId,
				amount: payload.amount,
				feeConfig: valueDepositConfig,
				settingKey: DepositConstant.FEE_SETTING_KEY[settingGetFeeKey],
				chargeFeeKey: `DepositChargeFee_${authInfo.accountId}`,
				filter: {
					accountId: authInfo.accountId,
					state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
					'method.group': { $ne: 'CREDIT_CARD' }
				}
			});
			if (_.isNumber(_.get(calFeeResult, 'fee')) === false) {
				response.code = ErrorCode.DEPOSIT_EWALLET.CALC_FEE_FAILED;
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E02)';
				return response;
			}

			fee = calFeeResult.fee;
		}
		const totalPay = _.toNumber((new Decimal(payload.amount)).add(new Decimal(fee)));

		// This variable help this transaction is Verify or create Transaction
		const isVerify = paymentKey === DepositConstant.DEPOSIT_PAYLOAD_KEY.LINKED && !_.isNil(_.get(payload, 'transaction'));
		let depositInfo = null;
		let historyInfo = null;

		/* In case of this transaction need to verify OTP */
		if (isVerify) {
			/* Find Deposit Info based on transaction */
			const transactionDeposit = _.get(payload, 'transaction', null);
			depositInfo = await Broker.call('v1.eWalletDepositModel.findOne',
				[
					{
						accountId: authInfo.accountId,
						transaction: transactionDeposit,
						state: DepositConstant.DEPOSIT_STATE.PENDING
					}, '-_id id transaction'
				]);

			if (_.get(depositInfo, 'id', null) === null) {
				response.message = 'Không tìm thấy lệnh nạp tiền, vui lòng thử lại sau.(E01)';
				return response;
			}

			historyInfo = await Broker.call('v1.eWalletHistoryModel.findOne',
				[
					{
						accountId: authInfo.accountId,
						state: PaymentConstant.STATE.PENDING,
						'service.type': DepositConstant.DEPOSIT_SERVICE_TYPE,
						'service.id': depositInfo.id,
						'service.transaction': depositInfo.transaction
					}, '-_id id'
				]);

			if (_.get(historyInfo, 'id', null) === null) {
				response.message = 'Không tìm thấy lệnh nạp tiền, vui lòng thử lại sau.(E02)';
				return response;
			}
		} else { /* In case of create new Transaction */
			let transaction = null;
			try {
				transaction = await Broker.call('v1.walletUuid.pick', { prefix: 'DEPOSIT' });
			} catch (error) {
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E03)';
				return response;
			}

			if (_.isNull(transaction)) {
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E04)';
				return response;
			}

			const dataDeposit = {
				accountId: authInfo.accountId,
				transaction,
				appId: authInfo.appId,
				amount: payload.amount,
				fee,
				total: totalPay,
				description: 'Nap tien vao Vi PayME',
				state: DepositConstant.DEPOSIT_STATE.PENDING,
				method: paymentMethodData
			};

			depositInfo = await Broker.call('v1.eWalletDepositModel.create', [dataDeposit]);

			if (_.get(depositInfo, 'id', null) === null) {
				response.code = ErrorCode.DEPOSIT_EWALLET.FAILED;
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E05)';
				return response;
			}

			let serviceName = 'Nạp tiền bằng Tài khoản liên kết';
			if (paymentKey === DepositConstant.DEPOSIT_PAYLOAD_KEY.BANK_CARD) {
				serviceName = 'Nạp tiền bằng Thẻ ATM nội địa';
			}
			const dataHistory = {
				accountId: authInfo.accountId,
				appId: authInfo.appId,
				service: {
					type: DepositConstant.DEPOSIT_SERVICE_TYPE,
					code: DepositConstant.DEPOSIT_SERVICE_CODE,
					id: depositInfo.id,
					transaction: depositInfo.transaction,
					method: DepositConstant.DEPOSIT_SERVICE_TYPE,
					state: DepositConstant.DEPOSIT_STATE.PENDING,
					name: serviceName,
					data: {
						resolveType: 'DepositObject',
						id: depositInfo.id,
						state: DepositConstant.DEPOSIT_STATE.PENDING,
						transaction: depositInfo.transaction,
						accountId: authInfo.accountId
					}
				},
				amount: payload.amount,
				total: totalPay,
				fee,
				state: DepositConstant.DEPOSIT_STATE.PENDING,
				description: 'Nap tien vao Vi PayME',
				tags: [DepositConstant.DEPOSIT_SERVICE_TYPE]
			};

			historyInfo = await Broker.call('v1.eWalletHistoryModel.create', [dataHistory]);

			if (_.get(historyInfo, 'id', null) === null) {
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E06)';
				return response;
			}
		}

		// Call Payment
		const paramsPayment = {
			accountId: authInfo.accountId,
			appId: authInfo.appId,
			amount: totalPay,
			description: 'Nạp tiền vào ví PayME',
			ip: _.get(ctx, 'meta.clientIp', null),
			service: {
				type: DepositConstant.DEPOSIT_SERVICE_CODE,
				code: DepositConstant.DEPOSIT_SERVICE_TYPE,
				transaction: depositInfo.transaction
			},
			method: _.get(payload, 'method', {})
		};

		console.log(paramsPayment);

		/** Example return
		 * @returns
		 * @example
		 *  {
		  		state: PaymentConstant.STATE.FAILED,
		  		message: 'Thanh toán thất bại',
				payment: {
					id: null,
					transaction: null,
					method: null,
					state: null,
					description: ''
				},
				data: {
					html: ....
				}
			}
		*/
		const paymentResult = await Broker.call('v1.eWalletPayment.pay', paramsPayment);

		console.log(paymentResult);
		// STEP x : Based on data of Payment, handle it appropriately

		/* -- Wrong Bank OTP Case -- */
		if (_.includes([PaymentConstant.STATE.INVALID_OTP], paymentResult.state)) {
			response.code = ErrorCode.DEPOSIT_EWALLET.WRONG_OTP;
			response.message = _.get(paymentResult, 'message', 'Mã OTP không chính xác');
			return response;
		}
		/* -- NEED VERIFY Case -- */
		const NEED_VERIFY_STATE = [PaymentConstant.STATE.REQUIRE_OTP,
			PaymentConstant.STATE.REQUIRE_VERIFY, PaymentConstant.STATE.PENDING];
		if (_.includes(NEED_VERIFY_STATE, paymentResult.state)) {
			// Update Into Payment and History
			const updateDepositData = {
				payment: paymentResult.payment
			};

			const updateDepositResult = await Broker.call('v1.eWalletDepositModel.updateOne', [{
				id: depositInfo.id
			}, updateDepositData]);

			if (updateDepositResult.nModified < 1) {
				response.code = ErrorCode.DEPOSIT_EWALLET.FAILED;
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E07)';
				return response;
			}

			const updateHistoryData = {
				payment: paymentResult.payment
			};

			const updateHistoryResult = await Broker.call('v1.eWalletHistoryModel.updateOne', [{
				id: historyInfo.id
			}, updateHistoryData]);

			if (updateHistoryResult.nModified < 1) {
				response.code = ErrorCode.DEPOSIT_EWALLET.FAILED;
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E08)';
				return response;
			}

			if (paymentResult.state === PaymentConstant.STATE.REQUIRE_OTP) {
				response.code = ErrorCode.DEPOSIT_EWALLET.REQUIRE_OTP;
			}

			if (paymentResult.state === PaymentConstant.STATE.REQUIRE_VERIFY) {
				response.code = ErrorCode.DEPOSIT_EWALLET.REQUIRE_VERIFY;
			}

			response.message = 'Thanh toán cần xác thực từ ngân hàng';
			response.data = {
				transaction: depositInfo.transaction,
				html: _.get(paymentResult, 'data.html', null)
			};
			return response;
		}

		/* -- FAILED Case -- */
		if (paymentResult.state === PaymentConstant.STATE.FAILED) {
			const updateDepositData = {
				payment: paymentResult.payment,
				state: PaymentConstant.STATE.FAILED
			};

			const updateDepositResult = await Broker.call('v1.eWalletDepositModel.updateOne', [{
				id: depositInfo.id
			}, updateDepositData]);

			if (updateDepositResult.nModified < 1) {
				response.code = ErrorCode.DEPOSIT_EWALLET.FAILED;
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E08)';
				return response;
			}

			const updateHistoryData = {
				state: PaymentConstant.STATE.FAILED,
				payment: paymentResult.payment,
				'service.name': `Nạp tiền từ ${_.get(paymentResult, 'payment.description', '')} thất bại`,
				'service.state': PaymentConstant.STATE.FAILED,
				'service.data.state': PaymentConstant.STATE.FAILED
			};
			const updateHistoryResult = await Broker.call('v1.eWalletHistoryModel.updateOne', [{
				id: historyInfo.id
			}, updateHistoryData]);

			if (updateHistoryResult.nModified < 1) {
				response.code = ErrorCode.DEPOSIT_EWALLET.FAILED;
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E09)';
				return response;
			}

			response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.';
			return response;
		}

		/* -- SUCCEEDED Case -- */
		if (paymentResult.state === PaymentConstant.STATE.SUCCEEDED) {
			// WalletLibs
			let resultDeposit = null;
			const paramsRequest = {
				accountId: authInfo.accountId,
				amount: payload.amount,
				description: 'Nạp tiền vào ví PayME.',
				referData: depositInfo
			};
			try {
				Logger(`[DEPOSIT] -> [CALL_WALLET] -> [PAYLOAD] -> :: ${JSON.stringify(paramsRequest)}`);
				resultDeposit = await this.WalletLibs.Deposit(paramsRequest);
				Logger(`[DEPOSIT] -> [CALL_WALLET] -> [RESPONSE] -> :: ${JSON.stringify(resultDeposit)}`);
			} catch (error) {
				response.code = ErrorCode.DEPOSIT_EWALLET.FAILED;
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E10)';
				return response;
			}

			const updateDepositData = {
				payment: paymentResult.payment,
				state: PaymentConstant.STATE.SUCCEEDED
			};

			const updateDepositResult = await Broker.call('v1.eWalletDepositModel.updateOne', [{
				id: depositInfo.id
			}, updateDepositData]);

			if (updateDepositResult.nModified < 1) {
				response.code = ErrorCode.DEPOSIT_EWALLET.FAILED;
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E11)';
				return response;
			}

			const updateHistoryData = {
				changed: '+',
				state: PaymentConstant.STATE.SUCCEEDED,
				payment: paymentResult.payment,
				balance: resultDeposit.balance,
				'service.name': `Nạp tiền từ ${_.get(paymentResult, 'payment.description', '')} thành công`,
				'service.state': PaymentConstant.STATE.FAILED,
				'service.data.state': PaymentConstant.STATE.FAILED
			};

			const updateHistoryResult = await Broker.call('v1.eWalletHistoryModel.updateOne', [{
				id: historyInfo.id
			}, updateHistoryData]);

			if (updateHistoryResult.nModified < 1) {
				response.code = ErrorCode.DEPOSIT_EWALLET.FAILED;
				response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E12)';
				return response;
			}

			response.code = ErrorCode.DEPOSIT_EWALLET.SUCCEEDED;
			response.message = 'Nạp tiền thành công';
			response.data = {
				historyId: historyInfo.id,
				description: _.get(paymentResult, 'payment.description', ''),
				fee,
				transaction: depositInfo.transaction
			};

			return response;
		}
	} catch (error) {
		Logger('[DEPOSIT] -> [EXCEPTION] ->', JSON.stringify(error));
		response.code = ErrorCode.DEPOSIT_EWALLET.FAILED;
		response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E00)';
	}
	return response;
};
