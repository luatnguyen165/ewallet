const _ = require('lodash');
const AwaitAsyncForEach = require('await-async-foreach');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	try {
		const payload = ctx.params.body;
		const banks = await Broker.call('v1.utility.getBankCode', { body: { language: 'vi' } });
		if (banks.code !== 113100) {
			return {
				code: 1001,
				message: 'Không lấy được danh sách ngân hàng'
			};
		}

		// console.log(banks);
		const listSwiftCode = _.map(banks.data, 'swiftCode');

		const startTime = Date.now();

		const result = [];
		await AwaitAsyncForEach(listSwiftCode, async (swiftCode) => {
			try {
				const data = await Broker.call('v1.utility.detectBank', {
					body: {
						cardNumber: payload.number,
						swiftCode
					}
				});

				if (_.get(data, 'data.fullname', '') !== '') {
					result.push(data);
				}
			} catch (err) {
				console.log(err);
			}
		}, 'parallel', 5);
		const endTime = Date.now();

		const timeWork = endTime - startTime;
		return {
			code: 1000,
			timeWork,
			data: result
		};
	} catch (error) {
		console.log(error);
		Logger('[TEST] >>> [DETECT BANK] >> [ERROR] >>', error);
	}
};
