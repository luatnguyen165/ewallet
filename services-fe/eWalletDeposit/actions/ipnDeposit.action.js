const _ = require('lodash');
const PaymentConstant = require('../constants/payment.constant');
const DepositConstant = require('../constants/deposit.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;

	const payload = _.get(ctx, 'params', {});
	try {
		if (payload.state === PaymentConstant.STATE.SUCCEEDED) {
			const updatedDepositInfo = await Broker.call('v1.eWalletDepositModel.findOneAndUpdate', [{
				accountId: payload.accountId,
				transaction: payload.transaction,
				state: PaymentConstant.STATE.PENDING
			}, {
				state: PaymentConstant.STATE.SUCCEEDED,
				'payment.state': PaymentConstant.STATE.SUCCEEDED
			}, { new: true }]);

			let resultDeposit = null;
			const paramsRequest = {
				accountId: updatedDepositInfo.accountId,
				amount: updatedDepositInfo.amount,
				description: 'Nạp tiền vào ví PayME.',
				referData: updatedDepositInfo
			};
			try {
				Logger(`[DEPOSIT] -> [CALL_WALLET] -> [PAYLOAD] -> :: ${JSON.stringify(paramsRequest)}`);
				resultDeposit = await this.WalletLibs.Deposit(paramsRequest);
				Logger(`[DEPOSIT] -> [CALL_WALLET] -> [RESPONSE] -> :: ${JSON.stringify(resultDeposit)}`);
			} catch (error) {
				Logger(`[DEPOSIT] -> [CALL_WALLET] -> [EXCEPTION] -> :: ${JSON.stringify(resultDeposit)}`);
			}

			await Broker.call('v1.eWalletHistoryModel.updateOne', [{
				accountId: payload.accountId,
				'service.id': updatedDepositInfo.id,
				'service.type': DepositConstant.DEPOSIT_SERVICE_TYPE,
				'service.transaction': payload.transaction
			}, {
				changed: '+',
				state: PaymentConstant.STATE.SUCCEEDED,
				balance: resultDeposit.balance,
				'service.name': `Nạp tiền từ ${_.get(payload, 'payment.description', '')} thành công`,
				'service.state': PaymentConstant.STATE.SUCCEEDED,
				'service.data.state': PaymentConstant.STATE.SUCCEEDED,
				'payment.state': PaymentConstant.STATE.SUCCEEDED
			}]);
		}

		if (payload.state === PaymentConstant.STATE.FAILED) {
			await Broker.call('v1.eWalletDepositModel.updateOne', [{
				transaction: payload.transaction,
				state: PaymentConstant.STATE.PENDING
			}, {
				state: PaymentConstant.STATE.FAILED,
				'payment.state': PaymentConstant.STATE.FAILED
			}]);

			await Broker.call('v1.eWalletHistoryModel.updateOne', [{
				accountId: payload.accountId,
				'service.type': DepositConstant.DEPOSIT_SERVICE_TYPE,
				'service.transaction': payload.transaction
			}, {
				changed: '+',
				state: PaymentConstant.STATE.FAILED,
				'service.name': `Nạp tiền từ ${_.get(payload, 'payment.description', '')} thất bại`,
				'service.state': PaymentConstant.STATE.FAILED,
				'service.data.state': PaymentConstant.STATE.FAILED,
				'payment.state': PaymentConstant.STATE.FAILED
			}]);
		}
	} catch (e) {
		Logger('[DEPOSIT] -> [IPN] -> [EXCEPTION] -> :: ', e);
	}
};
