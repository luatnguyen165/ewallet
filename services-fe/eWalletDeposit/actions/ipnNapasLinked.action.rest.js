const _ = require('lodash');
const moment = require('moment');

const DepositConstant = require('../constants/deposit.constant');
const ErrorCode = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;
	try {
		const payload = _.get(ctx, 'params.body', {});

		Logger(`[DEPOSIT_BE] -> [IPN] -> [PAYLOAD] :: ${JSON.stringify(payload)}`);

		// Find Payment Linked Napas
		const paymentData = await Broker.call('v1.eWalletPaymentLinkedNapasModel.findOne', [
			{
				transactionId: payload.trans_id,
				supplierTransaction: payload.order_id,
				state: DepositConstant.DEPOSIT_STATE.PENDING
			},
			'-_id id accountId bankInfo supplierResponsed']);

		if (_.get(paymentData, 'id', null) === null) {
			return {
				code: ErrorCode.IPN_CODE.FAILED,
				message: 'Không tìm thấy giao dịch'
			};
		}

		// Find In Payment Model
		const paymentInfo = await Broker.call('v1.eWalletPaymentModel.findOne', [{
			accountId: paymentData.accountId,
			transaction: payload.trans_id,
			state: DepositConstant.DEPOSIT_STATE.PENDING
		}]);

		if (_.get(paymentInfo, 'id', null) === null) {
			return {
				code: ErrorCode.IPN_CODE.FAILED,
				message: 'Không tìm thấy thông tin nạp tiền'
			};
		}

		// Update Succeeded payment
		let { supplierResponsed } = paymentData;
		if (_.isArray(supplierResponsed)) {
			supplierResponsed.push(payload);
		} else {
			supplierResponsed = [JSON.stringify(payload)];
		}

		await Broker.call('v1.eWalletPaymentLinkedNapasModel.updateOne',
			[
				{
					id: paymentData.id
				},
				{
					state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
					supplierResponsed
				}
			]);

		// Update Payment
		const filedsNeedUpdate = {
			state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
			method: DepositConstant.METHOD_CODE.LINKED,
			supplierTransaction: payload.order_id
		};

		await Broker.call('v1.eWalletPaymentModel.updateOne', [
			{ id: paymentInfo.id },
			filedsNeedUpdate
		]);

		const bankInfo = _.get(paymentData, 'bankInfo', {}); // paymentLinked.bankInfo;
		const paymentDescription = await this.CardDescription(bankInfo);

		const paymentObj = {
			id: paymentInfo.id,
			transaction: payload.trans_id,
			method: DepositConstant.METHOD_CODE.LINKED,
			state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
			description: paymentDescription,
			payment: bankInfo
		};

		const depositInfo = await Broker.call('v1.eWalletDepositModel.findOneAndUpdate',
			[{
				accountId: paymentInfo.accountId,
				transaction: payload.trans_id,
				state: DepositConstant.DEPOSIT_STATE.PENDING
			},
			{
				state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
				payment: paymentObj
			}, { new: true }]);

		const paramsRequest = {
			accountId: depositInfo.accountId,
			amount: depositInfo.amount,
			description: 'Nạp tiền vào ví PayME.',
			referData: depositInfo
		};
		// WalletLibs
		let resultDeposit = null;
		try {
			Logger(`[DEPOSIT] => [DEPOSIT_WALLET_PARAMS]: ${JSON.stringify(paramsRequest)}`);
			resultDeposit = await this.WalletLibs.Deposit(paramsRequest);
			Logger(`[DEPOSIT] => [DEPOSIT_WALLET]: ${JSON.stringify(resultDeposit)}`);
		} catch (error) {
			Logger(`[DEPOSIT] => [DEPOSIT_WALLET_ERROR]: ${error}`);

			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Nạp tiền thất bại (EXCEPTION_WALLET_1)'
			};
		}

		if (resultDeposit.successed === false) {
			return {
				code: ErrorCode.DEPOSIT_CODE.FAILED,
				message: 'Nạp tiền thất bại (EXCEPTION_WALLET_2)'
			};
		}
		const updateHistoryData = {
			changed: '+',
			method: DepositConstant.METHOD_CODE.LINKED,
			'service.name': `Nạp tiền từ ${paymentDescription}`,
			'service.state': DepositConstant.DEPOSIT_STATE.SUCCEEDED,
			'service.data.state': DepositConstant.DEPOSIT_STATE.SUCCEEDED,
			state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
			'payment.state': DepositConstant.DEPOSIT_STATE.SUCCEEDED,
			balance: resultDeposit.balance,
			publishedAt: new Date().toISOString()
		};

		const updateHistoryResult = await Broker.call('v1.eWalletHistoryModel.updateOne', [
			{
				accountId: depositInfo.accountId,
				state: DepositConstant.DEPOSIT_STATE.PENDING,
				'service.id': depositInfo.id,
				'service.type': 'DEPOSIT',
				'service.transaction': depositInfo.transaction
			},
			updateHistoryData
		]);

		if (updateHistoryResult.nModified < 1) {
			Logger('[DEPOSIT_BE] -> [IPN] ->[UPDATE_FAILED]');
			return {
				code: ErrorCode.IPN_CODE.FAILED,
				message: 'Thanh toán thành công'
			};
		}

		// IPN to Merchant
		if (_.get(depositInfo, 'extraData.ipnUrl', null) !== null) {
			const accountInfo = await Broker.call('v1.eWalletAccountModel.findOne', [{
				id: depositInfo.accountId
			}, '-_id phone']);

			const phone = _.get(accountInfo, 'phone', null);
			const merchantId = _.get(depositInfo, 'extraData.merchantId', null);
			const ipnUrl = _.get(depositInfo, 'extraData.ipnUrl', null);

			const issuedAt = _.get(bankInfo, 'issuedAt', null);
			const formatDate = moment(issuedAt).format('YYYY/MM').toString();
			console.log(bankInfo);
			const hiddenCardNumber = this.HiddenCardNumber(_.get(bankInfo, 'cardNumber', null), 'CARD');
			const ipnData = {
				linkedId: _.get(depositInfo, 'extraData.linkedId', null),
				phone,
				walletAccountId: depositInfo.accountId,
				transaction: depositInfo.transaction,
				state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
				amount: depositInfo.amount,
				fee: depositInfo.fee,
				total: depositInfo.total,
				cardInfo: {
					swiftCode: _.get(bankInfo, 'swiftCode', null),
					cardHolder: _.get(bankInfo, 'cardHolder', null),
					cardNumber: hiddenCardNumber,
					issuedAt: formatDate
				}
			};

			try {
				const paramsIPN = {
					ipnUrl,
					merchantId: _.toNumber(merchantId),
					ipnData
				};

				Logger(`[IPN_DEPOSIT_LINKED] -> [PAYLOAD] -> ${JSON.stringify(paramsIPN)}`);
				const ipn = await Broker.call('v1.ipnMerchantSDK.ipnPayment', paramsIPN, { timeout: 90 * 1000 });
				Logger(`[IPN_DEPOSIT_LINKED] -> [RESPONSE] -> ${JSON.stringify(ipn)}`);
			} catch (error) {
				console.error('[IPN_DEPOSIT_LINKED] -> [EXCEPTION] -> :: ', error);
				Logger(`[IPN_DEPOSIT_LINKED]-> [EXCEPTION] -> :: ${error}`);
			}
		}
		return {
			code: ErrorCode.IPN_CODE.SUCCEEDED,
			message: 'Thanh toán thành công'
		};
	} catch (error) {
		console.log(error);
		Logger('[DEPOSIT_BE] -> [IPN] -> [NAPAS_CARD] -> [EXCEPTION] :: ', error);
	}

	return {
		code: ErrorCode.IPN_CODE.FAILED,
		message: 'Thanh toán thất bại (EXCEPTION)'
	};
};
