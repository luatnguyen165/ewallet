const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');
const DepositConstant = require('../constants/deposit.constant');
const GeneralConstant = require('../constants/general.constant');

function PVcomBankLibrary(config = {}) {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.PVCOMBANK_TOKEN;

	const Logger = config.logger;

	async function PostRequest(urlRequest, body, headers = {}) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({
					url: urlRequest,
					timeout: 50000,
					body: JSON.stringify(body),
					headers: {
						Authorization,
						...headers
					}
				}, (err, httpResponse, response) => {
					if (err) {
						reject(err);
					}
					resolve(httpResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}
	return {
		async Pay(payload) {
			// Response:  code - message - state - transaction - html
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Nạp tiền PVComBank thất bại. Vui lòng thử lại sau.',
				transaction: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/card/topup`;
			const body = {
				trans_id: payload.transaction,
				request_id: Math.round(Math.random() * 100000000000).toString(),
				card_id: payload.cardId,
				amount: payload.amount,
				account_id: _.toString(payload.accountId),
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			Logger(`[PVCOMBANK_LIBRARY] => [Deposit] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
			try {
				const result = await PostRequest(urlRequest, body);
				Logger(`[PVCOMBANK_LIBRARY] => [Deposit] => [Response] : ${JSON.stringify({ urlRequest, result: _.get(result, 'body' , '') })}`);

				response.supplierResponse = _.get(result, 'body', result);

				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Thanh toán thất bại. Vui lòng thử lại sau.');
					if (response.message === 'ESOCKETTIMEDOUT') {
						response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					}
					return response;
				}
				response.transaction = _.get(bodyResponse, 'data.trans_id', '');
				if (_.get(bodyResponse, 'data.otp_required', false) === true) {
					response.message = _.get(bodyResponse, 'data.message', 'Thanh toán cần xác thực OTP từ ngân hàng');
					response.state = GeneralConstant.SUPPLIER_STATE.REQUIRED_OTP;
					return response;
				}
				response.message = 'Thanh toán thành công.';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[PVCOMBANK_LIBRARY] => [Deposit] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				if (error.message === 'ESOCKETTIMEDOUT') {
					response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
				}
				return response;
			}
		},
		async VerifyPay(payload) {
			// Response:  code - message - state - transaction - html
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Xác thực nạp tiền thất bại. Vui lòng thử lại sau.',
				transaction: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/card/verify_trans`;
			const body = {
				trans_id: payload.transaction,
				request_id: Math.round(Math.random() * 100000000000).toString(),
				otp: payload.otp,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			Logger(`[PVCOMBANK_LIBRARY] => [VerifyPay] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
			try {
				const result = await PostRequest(urlRequest, body);
				Logger(`[PVCOMBANK_LIBRARY] => [VerifyPay] => [Response] : ${JSON.stringify({ urlRequest, result: _.get(result, 'body' , '') })}`);
				response.supplierResponse = _.get(result, 'body', result);

				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code === 1008) {
					response.state = GeneralConstant.SUPPLIER_STATE.INVALID_OTP;
					response.message = _.get(bodyResponse, 'data.message', 'Mã OTP không đúng. Vui lòng thử lại');
					return response;
				}
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Xác thực nạp tiền thất bại. Vui lòng thử lại sau.');
					return response;
				}
				response.transaction = body.trans_id;
				response.message = 'Xác thực thanh toán thành công';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[PVCOMBANK_LIBRARY] => [Deposit] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}
	};
}
module.exports = PVcomBankLibrary;
