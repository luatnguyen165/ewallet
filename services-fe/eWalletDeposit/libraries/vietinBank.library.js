const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');
const DepositConstant = require('../constants/deposit.constant');
const GeneralConstant = require('../constants/general.constant')

function VietinBankLibrary(config = {}) {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.VIETINBANK_TOKEN;

	const Broker = config.broker;
	const Logger = config.logger;

	async function PostRequest(urlRequest, body, headers = {}) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({
					url: urlRequest,
					timeout: 50000,
					body: JSON.stringify(body),
					headers: {
						Authorization,
						...headers
					}
				}, (err, httpResponse, response) => {
					if (err) {
						reject(err);
					}
					resolve(httpResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}
	return {
		async Pay(payload) {
			// Response:  code - message - state - transaction - html
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Nạp tiền VietinBank thất bại. Vui lòng thử lại sau.',
				transaction: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/wallet/vietin/token/pay`;
			const body = {
				trans_id: _.toString(payload.transaction),
				amount: payload.amount,
				account_id: _.toString(payload.accountId),
				card_id: _.toNumber(payload.cardId),
				trans_desc: 'Nap tien vao vi PayME',
				channel: 'MOBILE',
				language: 'vi',
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			try {
				const result = await PostRequest(urlRequest, body);

				Logger(`[VIETINBANK_LIBRARY] => [Deposit: ${urlRequest}] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
				Logger(`[VIETINBANK_LIBRARY] => [Deposit: ${urlRequest}] => [Response] : ${JSON.stringify({ urlRequest, result: _.get(result, 'body' , '') })}`);
				response.supplierResponse = result.body || result;
				const bodyResponse = JSON.parse(result.body);

				response.transaction = _.get(bodyResponse, 'data.trans_id', body.trans_id);
				if (bodyResponse.code === 2053) {
					response.state = GeneralConstant.SUPPLIER_STATE.REQUIRED_OTP;
					response.message = _.get(bodyResponse, 'data.message', 'Thanh toán cần xác thực OTP');
					return response;
				}

				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Thanh toán thất bại. Vui lòng thử lại sau.');
					if (response.message === 'ESOCKETTIMEDOUT') {
						response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					}
					return response;
				}
				response.message = 'Thanh toán thành công.';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[VIETINBANK_LIBRARY] => [Deposit: ${urlRequest}] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async VerifyPay(payload) {
			// Response:  code - message - state - transaction - html
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Xác thực thanh toán thất bại. Vui lòng thử lại sau.',
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/wallet/vietin/token/pay/verify_otp`;
			const body = {
				connector_trans_id: payload.transaction,
				otp: payload.otp,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};

			try {
				const result = await PostRequest(urlRequest, body);
				response.supplierResponse = _.get(result, 'body', result);

				const bodyResponse = JSON.parse(result.body);

				Logger(`[VIETINBANK_LIBRARY] => [VerifyDeposit] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
				Logger(`[VIETINBANK_LIBRARY] => [VerifyDeposit] => [Response] : ${JSON.stringify({ urlRequest, result: _.get(result, 'body', '') })}`);

				// if (bodyResponse.code === 1001) {
				// 	response.state = GeneralConstant.SUPPLIER_STATE.INVALID_OTP;
				// 	return response;
				// }
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.');
					return response;
				}
				response.transaction = body.trans_id;
				response.message = 'Xác thực thanh toán thành công';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[VIETINBANK_LIBRARY] => [VerifyDeposit: ${urlRequest}] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}

	};
}
module.exports = VietinBankLibrary;
