const Request = require('request');
const _ = require('lodash');
const GeneralConstant = require('../constants/general.constant');

function NapasLibrary(config = {}) {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.NAPAS_TOKEN;
	const defaultNotifyUrl = process.env.HOME_URL;
	const inpUrl = process.env.GAPI_URL;

	const Broker = config.broker;
	const Logger = config.logger;
	// const RedisService = config.redis();

	async function PostRequest(urlRequest, body, headers = {}) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({
					url: urlRequest,
					timeout: 50000,
					body: JSON.stringify(body),
					headers: {
						Authorization,
						...headers
					}
				}, (err, httpResponse, response) => {
					if (err) {
						reject(err);
					}
					resolve(httpResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}
	return {
		async Pay(payload) {
			// Response:  code - message - state - transaction - html
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Thanh toán Napas thất bại. Vui lòng thử lại sau.',
				transaction: null,
				html: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/global/token/pay`;

			let redirectUrl = _.get(payload, 'redirectUrl', '') || '';
			if (redirectUrl === '') {
				redirectUrl = 'https://payme.vn/web';
			}

			const body = {
				amount: payload.amount,
				account_id: _.toString(payload.accountId),
				trans_id: payload.transaction,
				card_id: _.get(payload, 'cardId', ''),
				subject: 'Thanh Toán Dịch vụ',
				return_url: redirectUrl, // redirect sau khi user thanh toán thành công
				notify_url: `${inpUrl}/ewallet/deposit/napas/linked/ipn`,
				client_ip: '',
				device_id: '',
				env: _.get(payload, 'envName', 'MobileApp'),
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};

			try {
				const result = await PostRequest(urlRequest, body);
				response.supplierResponse = _.get(result, 'body', result);

				const bodyResponse = JSON.parse(result.body);

				Logger(`[NAPAS_LIBRARY] => [Deposit] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
				Logger(`[NAPAS_LIBRARY] => [Deposit] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				if (bodyResponse.code === 1000) {
					response.transaction = _.get(bodyResponse, 'data.order_id', null);
					response.message = 'Thanh toán cần xác thực bằng form ngân hàng.';
					response.state = GeneralConstant.SUPPLIER_STATE.REQUIRED_VERIFY;
					response.html = _.get(bodyResponse, 'data.data', null);

					return response;
				}

				response.message = _.get(bodyResponse, 'data.message', 'Thanh toán thất bại. Vui lòng thử lại sau');
				if (response.message === 'ESOCKETTIMEDOUT') {
					response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
				}

				return response;
			} catch (error) {
				Logger(`[NAPAS_LIBRARY] => [Deposit] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}
	};
}
module.exports = NapasLibrary;
