const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');
const DepositConstant = require('../constants/deposit.constant');
const GeneralConstant = require('../constants/general.constant');
const ResponseCode = require('../constants/errorCode.constant');

function BIDVBankLibrary(config = {}) {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.BIDVBANK_TOKEN;

	const Logger = config.logger;

	async function PostRequest(urlRequest, body, headers = {}) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({
					url: urlRequest,
					timeout: 50000,
					body: JSON.stringify(body),
					headers: {
						Authorization,
						...headers
					}
				}, (err, httpResponse, response) => {
					if (err) {
						reject(err);
					}
					resolve(httpResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}

	return {
		async Pay(payload) {
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Nạp tiền BIDV thất bại. Vui lòng thử lại sau.',
				transaction: null,
				supplierResponse: null
			};

			const params = {
				trans_id: payload.transaction,
				trans_desc: payload.description || 'Nap tien vao vi PayME',
				amount: payload.amount,
				account_id: payload.accountId,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
				// version: payload.version
			};

			let urlRequest = null;
			if (payload.version === 2) {
				urlRequest = `${url}/v2/bidv/wallet/bank_to_wallet`;
			} else {
				urlRequest = `${url}/v1/bidv/wallet/bank_to_wallet`;
			}

			try {
				const result = await PostRequest(urlRequest, params);
				Logger(`[BIDVBANK] => [DEPOSIT] => [Params] : ${JSON.stringify({ urlRequest, params })}`);
				Logger(`[BIDVBANK] => [DEPOSIT] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				response.supplierResponse = _.get(result, 'body', result);
				const bodyResponse = JSON.parse(result.body);
				response.transaction = _.get(bodyResponse, 'data.trans_id', '');
				if (bodyResponse.code === 2053) {
					response.state = GeneralConstant.SUPPLIER_STATE.REQUIRED_OTP;
					response.message = _.get(bodyResponse, 'data.message', 'Thanh toán cần xác thực OTP');
					return response;
				}
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Thanh toán thất bại. Vui lòng thử lại sau.');
					if (response.message === 'ESOCKETTIMEDOUT') {
						response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					}
					return response;
				}

				response.message = 'Nạp tiền thành công.';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[BIDVBANK_LIBRARY] => [Deposit: ${urlRequest}] => [Error] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async VerifyPay(payload) {
			// Response:  code - message - state - transaction - html
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Xác thực thanh táon thất bại. Vui lòng thử lại sau',
				supplierResponse: null
			};

			const params = {
				trans_id: payload.transaction,
				otp_number: payload.otp,
				account_id: payload.accountId,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
				// version: payload.version
			};

			let urlRequest = null;
			if (payload.version === 2) {
				urlRequest = `${url}/v2/bidv/wallet/bank_to_wallet/check_otp`;
			} else {
				urlRequest = `${url}/v1/bidv/wallet/check_otp`;
			}

			try {
				const result = await PostRequest(urlRequest, params);
				Logger(`[BIDVBANK] => [VerifyPay] => [Params] : ${JSON.stringify({ urlRequest, params })}`);
				Logger(`[BIDVBANK] => [VerifyPay] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				response.supplierResponse = _.get(result, 'body', result);
				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Xác thực thanh toán thất bại. Vui lòng thử lại sau');
					return response;
				}
				response.message = 'Nạp tiền thành công';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[BIDVBANK_LIBRARY] => [Deposit: ${urlRequest}] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}

	};
}
module.exports = BIDVBankLibrary;
