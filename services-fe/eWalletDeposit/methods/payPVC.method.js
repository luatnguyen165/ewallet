const _ = require('lodash');
const moment = require('moment');
const DepositConstant = require('../constants/deposit.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = function () {
	// Payload
	const Logger = this.logger.info;
	const Broker = this.broker;
	const PVCLib = this.PVComBankLibs;
	const RedisCache = this.RedisCache();

	return {

		async Pay(payload) {
			const response = {
				state: DepositConstant.DEPOSIT_STATE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(PVC_ERR)'
			};
			try {
				const created = await Broker.call('v1.eWalletPaymentLinkedPVCModel.create', [{
					accountId: payload.accountId,
					paymentId: payload.paymentId,
					transactionId: payload.transaction,
					bankInfo: payload.cardInfo,
					amount: payload.amount,
					description: 'Thanh toán từ PVCBank',
					state: DepositConstant.DEPOSIT_STATE.PENDING
				}]);

				if (_.get(created, 'id', null) === null) {
					response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(PVC_ERR_01)';
					return response;
				}

				const paramsSend = {
					transaction: payload.transaction,
					cardId: payload.cardId,
					amount: payload.amount,
					accountId: payload.accountId
				};

				const supplierResponse = await PVCLib.Pay(paramsSend);

				const dataUpdate = {
					supplierResponsed: [supplierResponse.supplierResponse],
					supplierTransaction: _.get(supplierResponse, 'transaction', null)
				};

				if (supplierResponse.state === DepositConstant.DEPOSIT_STATE.FAILED) {
					dataUpdate.state = DepositConstant.DEPOSIT_STATE.FAILED;
					response.state = DepositConstant.DEPOSIT_STATE.FAILED;
				}

				if (supplierResponse.state === DepositConstant.DEPOSIT_STATE.REQUIRED_OTP) {
					response.state = DepositConstant.DEPOSIT_STATE.REQUIRED_OTP;
				}
				if (supplierResponse.state === DepositConstant.DEPOSIT_STATE.SUCCEEDED) {
					dataUpdate.state = DepositConstant.DEPOSIT_STATE.SUCCEEDED;
					response.state = DepositConstant.DEPOSIT_STATE.SUCCEEDED;
				}

				const checkUpdate = await Broker.call('v1.eWalletPaymentLinkedPVCModel.updateOne', [{
					id: created.id
				}, {
					...dataUpdate
				}]);

				if (checkUpdate.nModified < 1) {
					response.state = DepositConstant.DEPOSIT_STATE.FAILED;
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau (PVC_ERR_01)'
					return response;
				}

				response.paymentLinkedId = created.id;
				response.supplierTransaction = _.get(supplierResponse, 'transaction', null);
				response.message = _.get(supplierResponse, 'message', null) || 'Thanh toán thành công';

				return response;
			} catch (err) {
				Logger('[OPENEWALLET] >>> [DEPOSIT] >> [PVC_SUPPLIER] >>', JSON.stringify(err));

				return response;
			}
		},

		async VerifyPay(payload) {
			const response = {
				state: DepositConstant.DEPOSIT_STATE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(PVC_VER_ERR)'
			};
			try {
				const created = await Broker.call('v1.eWalletPaymentLinkedPVCModel.findOne', [{
					transactionId: payload.transaction,
					state: DepositConstant.DEPOSIT_STATE.PENDING
				}]);

				if (_.get(created, 'id', null) === null) {
					response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(PVC_VER_ERR_01)';
					return response;
				}

				const paramsSend = {
					transaction: created.supplierTransaction,
					otp: payload.otp
				};

				const pvcResponse = await PVCLib.VerifyPay(paramsSend);

				const supplierResponsed = _.get(created, 'supplierResponsed', []);
				supplierResponsed.push(JSON.stringify(pvcResponse.supplierResponse));

				// Sai ma OTP
				if (pvcResponse.state === GeneralConstant.SUPPLIER_STATE.INVALID_OTP) {
					let countOTPFailed = await RedisCache.get(`BankTransactionFailedOTP-${payload.accountId}`);
					countOTPFailed = _.toNumber(countOTPFailed) || 0;
					if (countOTPFailed >= 3) {
						await Broker.call('v1.eWalletPaymentLinkedPVCModel.updateOne', [{
							id: created.id
						}, {
							state: DepositConstant.DEPOSIT_STATE.FAILED,
							supplierResponsed
						}]);
						const ttl = await RedisCache.getTTL(`BankTransactionFailedOTP-${payload.accountId}`);
						let d = moment.duration(ttl, 'seconds').format('m:ss');
						if (d < 60) d = `${d} giây`;
						else {
							d = d.split(':');
							if (d[1] !== '00') d = `${d[0]} phút ${d[1]} giây`;
							else d = `${d[0]} phút`;
						}
						response.state = DepositConstant.DEPOSIT_STATE.FAILED;
						response.message = `Bạn đã nhập sai OTP quá 3 lần liên tiếp, vui lòng thử lại sau ${d} hoặc liên hệ bộ phận CSKH để được hỗ trợ!`;
						return response;
					}
					await RedisCache.set(`BankTransactionFailedOTP-${payload.accountId}`, countOTPFailed + 1, 300);
					response.state = DepositConstant.DEPOSIT_STATE.INVALID_OTP;
					response.message = _.get(pvcResponse, 'data.message', 'Mã OTP không chính xác');
					return response;
				}

				// Thanh toan Failed
				if (pvcResponse.state === GeneralConstant.SUPPLIER_STATE.FAILED) {
					const t = Math.round(_.toNumber(payload.total)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
					const alertMsg = `❗Thanh toán từ PVCBank thất bại
					  \nTopup Response: ${JSON.stringify(pvcResponse.original)}
					  \nID: ${created.id} - Amount: ${t} - AccountId: ${payload.accountId}`;
					// NotifyService.SendAlertNotify(alertMsg, [], ['api']);

					await Broker.call('v1.eWalletPaymentLinkedPVCModel.updateOne', [{
						id: created.id

					}, {
						state: DepositConstant.DEPOSIT_STATE.FAILED,
						supplierResponsed
					}]);

					await RedisCache.delete(`OCBBankTransactionFailedOTP-${payload.accountId}`);
					response.state = DepositConstant.DEPOSIT_STATE.FAILED;
					response.message = _.get(pvcResponse, 'data.message', 'Thanh toán thất bại!');
					return response;
				}

				await Broker.call('v1.eWalletPaymentLinkedPVCModel.updateOne', [{
					id: created.id
				}, {
					state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
					supplierResponsed,
					supplierTransaction: _.get(pvcResponse, 'transaction', null)
				}]);

				response.supplierTransaction = _.get(pvcResponse, 'transaction', null);
				response.state = pvcResponse.state;
				response.message = _.get(pvcResponse, 'message', null) || 'Thanh toán thành công';

				return response;
			} catch (err) {
				Logger('[OPENEWALLET] >>> [DEPOSIT] >> [PVC_SUPPLIER] >>', JSON.stringify(err));

				return response;
			}
		}

	};
};
