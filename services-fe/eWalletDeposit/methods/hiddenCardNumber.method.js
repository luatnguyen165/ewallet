module.exports = function (number, type = 'CARD') {
	try {
		let numberResponse = null;
		if (!!number === false) {
			return numberResponse;
		}
		if (type === 'CARD') {
			const lengthNeedToReplace = number.length - 8;
			const regex = new RegExp(`(\\d{4})\\d{${lengthNeedToReplace}}`, 'gi');
			numberResponse = number.replace(regex, `$1${'*'.repeat(lengthNeedToReplace)}`);
		} else {
			const lengthNeedToReplace = number.length - 4;
			const regex = new RegExp(`(\\d{0})\\d{${lengthNeedToReplace}}`, 'gi');
			numberResponse = number.replace(regex, `$1${'*'.repeat(lengthNeedToReplace)}`);
		}
		return numberResponse;
	} catch (error) {
		return number;
	}
};
