const _ = require('lodash');
const moment = require('moment');
const DepositConstant = require('../constants/deposit.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = function () {
	// Payload
	const Logger = this.logger.info;
	const Broker = this.broker;
	const OcbLib = this.OCBBankLibs;
	const RedisCache = this.RedisCache();

	return {
		async Pay(payload) {
			const response = {
				state: DepositConstant.DEPOSIT_STATE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(OCB_ERR)'
			};
			try {
				const created = await Broker.call('v1.eWalletPaymentLinkedOCBModel.create', [{
					accountId: payload.accountId,
					paymentId: payload.paymentId,
					transactionId: payload.transaction,
					bankInfo: payload.cardInfo,
					amount: payload.amount,
					description: 'Thanh toán từ OCBBank',
					state: DepositConstant.DEPOSIT_STATE.PENDING
				}]);

				if (_.get(created, 'id', null) === null) {
					response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(OCB_ERR_01)';
					return response;
				}

				const vanillaPhoneFormat = payload.phone.replace(/^84/, 0);
				const paramsSend = {
					phone: vanillaPhoneFormat,
					amount: payload.amount,
					cardId: payload.cardId
				};

				/**
				* Response of libs:
				state:  in  DepositConstant.DEPOSIT_STATE,
				message: message of supplier
				*/

				const supplierResponse = await OcbLib.Pay(paramsSend);

				const dataUpdate = {
					supplierResponsed: [supplierResponse.supplierResponse],
					supplierTransaction: _.get(supplierResponse, 'transaction', null)
				};

				// 3 case of OCB require OTP, FAILED and SUCCEEDED
				response.state = DepositConstant.DEPOSIT_STATE.REQUIRED_OTP;
				if (supplierResponse.state === DepositConstant.DEPOSIT_STATE.FAILED) {
					dataUpdate.state = DepositConstant.DEPOSIT_STATE.FAILED;
					response.state = DepositConstant.DEPOSIT_STATE.FAILED;
				}
				if (supplierResponse.state === DepositConstant.DEPOSIT_STATE.SUCCEEDED) {
					dataUpdate.state = DepositConstant.DEPOSIT_STATE.SUCCEEDED;
					response.state = DepositConstant.DEPOSIT_STATE.SUCCEEDED;
				}
				await Broker.call('v1.eWalletPaymentLinkedOCBModel.updateOne', [{
					id: created.id
				}, {
					...dataUpdate
				}]);

				response.supplierTransaction = _.get(supplierResponse, 'transaction', null);
				response.message = _.get(supplierResponse, 'message', null) || 'Tạo yêu cầu thanh toán thành công';

				return response;
			} catch (err) {
				console.log(err)
				Logger('[OPENEWALLET] >>> [DEPOSIT] >> [OCB_SUPPLIER] >>', err);

				return response;
			}
		},

		async VerifyPay(payload) {
			const response = {
				state: DepositConstant.DEPOSIT_STATE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(OCB_VER_ERR)'
			};
			try {
				const created = await Broker.call('v1.eWalletPaymentLinkedOCBModel.findOne', [{
					transactionId: payload.transaction,
					state: DepositConstant.DEPOSIT_STATE.PENDING
				}]);

				if (_.get(created, 'id', null) === null) {
					response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(OCB_VER_ERR_01)';
					return response;
				}

				const vanillaPhoneFormat = payload.phone.replace(/^84/, 0);
				const paramsSend = {
					transaction: created.supplierTransaction,
					cardId: payload.cardId,
					phone: vanillaPhoneFormat,
					amount: payload.amount,
					otp: payload.otp
				};

				const ocbResponse = await OcbLib.VerifyPay(paramsSend);

				const supplierResponsed = _.get(created, 'supplierResponsed', []);
				supplierResponsed.push(JSON.stringify(ocbResponse.supplierResponse));
				// Sai ma OTP
				if (ocbResponse.state === GeneralConstant.SUPPLIER_STATE.INVALID_OTP) {
					let countOTPFailed = await RedisCache.get(`BankTransactionFailedOTP-${payload.accountId}`);
					countOTPFailed = _.toNumber(countOTPFailed) || 0;
					if (countOTPFailed >= 3) {
						await Broker.call('v1.eWalletPaymentLinkedOCBModel.updateOne', [{
							id: created.id

						}, {	state: DepositConstant.DEPOSIT_STATE.FAILED }]);
						const ttl = await RedisCache.getTTL(`BankTransactionFailedOTP-${payload.accountId}`);
						let d = moment.duration(ttl, 'seconds').format('m:ss');
						if (d < 60) d = `${d} giây`;
						else {
							d = d.split(':');
							if (d[1] !== '00') d = `${d[0]} phút ${d[1]} giây`;
							else d = `${d[0]} phút`;
						}
						response.state = DepositConstant.DEPOSIT_STATE.FAILED;
						response.message = `Bạn đã nhập sai OTP quá 3 lần liên tiếp, vui lòng thử lại sau ${d} hoặc liên hệ bộ phận CSKH để được hỗ trợ!`;
						return response;
					}
					await RedisCache.set(`BankTransactionFailedOTP-${payload.accountId}`, countOTPFailed + 1, 300);
					response.state = DepositConstant.DEPOSIT_STATE.INVALID_OTP;
					response.message = _.get(ocbResponse, 'data.message', 'Thanh toán thất bại');
					return response;
				}


				// Thanh toan Failed
				if (ocbResponse.state ===GeneralConstant.SUPPLIER_STATE.FAILED) {
					const t = Math.round(_.toNumber(payload.total)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
					const alertMsg = `❗Thanh toán từ OCBBank thất bại
					  \nTopup Response: ${JSON.stringify(ocbResponse.original)}
					  \nID: ${created.id} - Amount: ${t} - AccountId: ${payload.accountId}`;
					// NotifyService.SendAlertNotify(alertMsg, [], ['api']);

					await Broker.call('v1.eWalletPaymentLinkedOCBModel.updateOne', [{
						id: created.id

					}, { state: DepositConstant.DEPOSIT_STATE.FAILED, supplierResponsed }]);

					await RedisCache.delete(`OCBBankTransactionFailedOTP-${payload.accountId}`);
					response.state = DepositConstant.DEPOSIT_STATE.FAILED;
					response.message = _.get(ocbResponse, 'data.message', 'Thanh toán thất bại!');
					return response;
				}

				await Broker.call('v1.eWalletPaymentLinkedOCBModel.updateOne', [{
					id: created.id
				}, {
					bankTransactionId: _.get(ocbResponse, 'bankTransactionId', null),
					supplierResponsed,
					state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
					clientTransId: _.get(ocbResponse, 'clientTransId', null),
					supplierTransaction: _.get(ocbResponse, 'transaction', null)
				}]);

				response.supplierTransaction = _.get(ocbResponse, 'transaction', null);
				response.state = DepositConstant.DEPOSIT_STATE.SUCCEEDED;
				response.message = _.get(ocbResponse, 'message', null) || 'Thanh toán thành công';
				response.paymentLinkedId = created.id;

				return response;
			} catch (err) {
				console.log(err)
				Logger('[OPENEWALLET] >>> [DEPOSIT] >> [OCB_SUPPLIER] >>', err);

				return response;
			}
		}

	};
};
