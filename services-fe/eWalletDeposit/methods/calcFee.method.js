const _ = require('lodash');
const Decimal = require('decimal.js');
const Moment = require('moment');

const ModuleConstant = {
	DUTY_FREE_SETTING_KEY: 'duty.free.list'
};

module.exports = async function (params) {
	try {
		const Broker = this.broker;
		const RedisCache = this.RedisCache();
		// duty free
		let valueDutyFreeConfig = await Broker.call('v1.eWalletSettingModel.findOne', [{
			key: ModuleConstant.DUTY_FREE_SETTING_KEY
		}]);

		if (!_.isNil(valueDutyFreeConfig)) {
			valueDutyFreeConfig = valueDutyFreeConfig.value;

			if (_.isString(valueDutyFreeConfig)) {
				valueDutyFreeConfig = JSON.parse(valueDutyFreeConfig);
			}
			valueDutyFreeConfig = valueDutyFreeConfig || {};
			const dutyFreeAccountList = _.get(valueDutyFreeConfig, 'accountId', []);
			if (_.includes(dutyFreeAccountList, params.accountId)) {
				return {
					fee: 0
				};
			}
		}

		const amount = new Decimal(params.amount);
		let fee = new Decimal(0);
		// get config
		let feeConfig = await Broker.call('v1.eWalletSettingModel.findOne', [{
			key: params.settingKey
		}]);
		if (_.isString(feeConfig.value)) {
			feeConfig = JSON.parse(feeConfig.value);
		}
		feeConfig = feeConfig || {};
		let paramsConfig = params.feeConfig;
		if (_.isString(paramsConfig)) {
			paramsConfig = JSON.parse(paramsConfig);
		}
		paramsConfig = paramsConfig || {};
		feeConfig = { ...paramsConfig, ...feeConfig };
		if (_.isNumber(feeConfig.freeLimit) === false || !feeConfig.dayRange || feeConfig.dayRange < 1) {
			return {
				fee: 0
			};
		}

		// load charge fee flag from redis, if not exists, count from db and set to redis for caching
		const now = Moment();
		const chargeFeeKey = `PerDays_${params.chargeFeeKey}`;
		await RedisCache.delete(chargeFeeKey);

		const dayOfMonth = now.date();
		const startOfRangeDay = Math.ceil(dayOfMonth / feeConfig.dayRange) * feeConfig.dayRange - feeConfig.dayRange + 1;
		const startOfRangeMoment = now.clone().set('date', startOfRangeDay).startOf('day');
		const endOfRangeMoment = Moment.min(startOfRangeMoment.clone().add(feeConfig.dayRange - 1, 'days'), now.clone().endOf('month')).endOf('day');

		_.set(params.filter, 'createdAt.$gte', { $dateFromString: { dateString: startOfRangeMoment.toDate().toISOString() } });
		_.set(params.filter, 'createdAt.$lte', { $dateFromString: { dateString: endOfRangeMoment.toDate().toISOString() } });

		let statictis = await this.broker.call('v1.eWalletDepositModel.aggregate', [[{
			$match: params.filter
		},
		{
			$group: {
				_id: null,
				sumAmount: { $sum: '$amount' },
				count: { $sum: 1 }
			}
		},
		{
			$project: {
				_id: 0,
				sumAmount: '$sumAmount',
				count: '$count'
			}
		}]]);

		if (_.get(statictis, 'length', 0) <= 0) {
			statictis = {
				sumAmount: 0,
				count: 0
			};
		} else {
			statictis = _.get(statictis, '[0]', {});
		}
		if (
			!statictis
      || _.isNumber(statictis.sumAmount) === false
      || _.isNumber(statictis.count) === false
		) {
			return null;
		}
		// chargeFee = await params.model.countDocuments(params.filter);
		// if (_.isNumber(chargeFee) === false) {
		//   return null;
		// }
		const reachedAmount = new Decimal(statictis.sumAmount).add(amount);
		if (feeConfig.moneyLimit >= 0 && reachedAmount.gt(new Decimal(feeConfig.moneyLimit)) === true) {
			const overAmount = Decimal.min(reachedAmount.minus(new Decimal(feeConfig.moneyLimit)), amount);
			if (feeConfig.feeRate) {
				const feeRate = new Decimal(feeConfig.feeRate);
				fee = overAmount.times(feeRate).div(new Decimal('100')).ceil();
			}
			fee = fee.add(new Decimal(feeConfig.fee || 0));

			if (feeConfig.minFee) {
				const minFee = new Decimal(feeConfig.minFee);
				if (fee.lt(minFee)) fee = minFee;
			}
			if (feeConfig.maxFee) {
				const maxFee = new Decimal(feeConfig.maxFee);
				if (fee.gt(maxFee)) fee = maxFee;
			}
		} else if (feeConfig.freeLimit >= 0 && statictis.count >= feeConfig.freeLimit) {
		// const cacheTimeout = now.clone().endOf('day').unix() - now.unix();
		// if (cacheTimeout > 0) {
		//   await RedisService.set(chargeFeeKey, chargeFee, cacheTimeout);
		// }
			if (feeConfig.feeRate) {
				const feeRate = new Decimal(feeConfig.feeRate);
				fee = amount.times(feeRate).div(new Decimal('100')).ceil();
			}
			fee = fee.add(new Decimal(feeConfig.fee || 0));

			if (feeConfig.minFee) {
				const minFee = new Decimal(feeConfig.minFee);
				if (fee.lt(minFee)) fee = minFee;
			}
			if (feeConfig.maxFee) {
				const maxFee = new Decimal(feeConfig.maxFee);
				if (fee.gt(maxFee)) fee = maxFee;
			}
		}

		fee = _.toNumber(fee);

		return {
			fee
		};
	} catch (e) {
		this.logger.info('[CALC_FEE] => [EXCEPTION] => ', e);
		console.log(e);
	}
};
