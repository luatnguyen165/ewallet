const _ = require('lodash');
const moment = require('moment');
const DepositConstant = require('../constants/deposit.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = function (logger, payload) {
	// Payload
	const Logger = this.logger.info;
	const Broker = this.broker;
	const BIDVLib = this.BIDVBankLibs;
	const RedisCache = this.RedisCache();

	return {

		async Pay() {
			const response = {
				state: DepositConstant.DEPOSIT_STATE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(BIDV_ERR)'
			};
			try {
				const created = await Broker.call('v1.eWalletPaymentLinkedBIDVModel.create', [{
					accountId: payload.accountId,
					paymentId: payload.paymentId,
					transactionId: payload.transaction,
					bankInfo: payload.cardInfo,
					amount: payload.amount,
					version: payload.version,
					description: 'Thanh toán từ BIDVBank',
					state: DepositConstant.DEPOSIT_STATE.PENDING
				}]);

				if (_.get(created, 'id', null) === null) {
					response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(BIDV_ERR_01)';
					return response;
				}

				const paramsSend = {
					transaction: payload.transaction,
					description: 'Nap tien vao vi PayME',
					amount: payload.amount,
					accountId: payload.accountId,
					version: payload.version
				};

				const supplierResponse = await BIDVLib.Pay(paramsSend);

				const dataUpdate = {
					supplierResponsed: [supplierResponse.supplierResponse],
					supplierTransaction: _.get(supplierResponse, 'transaction', null)
				};

				if (supplierResponse.state === DepositConstant.DEPOSIT_STATE.REQUIRED_OTP) {
					response.state = DepositConstant.DEPOSIT_STATE.REQUIRED_OTP;
				}
				if (supplierResponse.state === DepositConstant.DEPOSIT_STATE.FAILED) {
					// response.state = DepositConstant.DEPOSIT_STATE.SUCCEEDED;
					dataUpdate.state = DepositConstant.DEPOSIT_STATE.FAILED;
					response.state = DepositConstant.DEPOSIT_STATE.FAILED;
				}
				if (supplierResponse.state === DepositConstant.DEPOSIT_STATE.SUCCEEDED) {
					dataUpdate.state = DepositConstant.DEPOSIT_STATE.SUCCEEDED;
					response.state = DepositConstant.DEPOSIT_STATE.SUCCEEDED;
				}

				const checkUpdate = await Broker.call('v1.eWalletPaymentLinkedBIDVModel.updateOne', [{
					id: created.id
				}, {
					...dataUpdate
				}]);

				if (_.get(checkUpdate, 'nModified', 0) < 1) {
					response.state = DepositConstant.DEPOSIT_STATE.FAILED;
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.';
					return response;
				}

				response.supplierTransaction = _.get(supplierResponse, 'transaction', null);
				response.message = _.get(supplierResponse, 'message', null) || 'Thanh toán thành công';

				return response;
			} catch (err) {
				Logger('[OPENEWALLET] >>> [DEPOSIT] >> [BIDV_SUPPLIER] >>', err);

				return response;
			}
		},

		async VerifyPay() {
			const response = {
				state: DepositConstant.DEPOSIT_STATE.FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(BIDV_VER_ERR)'
			};
			try {
				const created = await Broker.call('v1.eWalletPaymentLinkedBIDVModel.findOne', [{
					transactionId: payload.transaction,
					state: DepositConstant.DEPOSIT_STATE.PENDING
				}]);

				if (_.get(created, 'id', null) === null) {
					response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(BIDV_VER_ERR_01)';
					return response;
				}

				const paramsSend = {
					transaction: created.supplierTransaction,
					otp: payload.otp,
					accountId: created.accountId,
					version: payload.version
				};

				const bidvResponse = await BIDVLib.VerifyPay(paramsSend);

				const supplierResponsed = _.get(created, 'supplierResponsed', []);
				supplierResponsed.push(JSON.stringify(bidvResponse.supplierResponse));

				// Sai ma OTP
				if (bidvResponse.state === GeneralConstant.SUPPLIER_STATE.INVALID_OTP) {
					let countOTPFailed = await RedisCache.get(`BankTransactionFailedOTP-${payload.accountId}`);
					countOTPFailed = _.toNumber(countOTPFailed) || 0;
					if (countOTPFailed >= 3) {
						await this.broker.call('v1.eWalletPaymentLinkedBIDVModel.updateOne', [{
							id: created.id
						}, { state: DepositConstant.DEPOSIT_STATE.FAILED }]);
						const ttl = await RedisCache.getTTL(`BankTransactionFailedOTP-${payload.accountId}`);
						let d = moment.duration(ttl, 'seconds').format('m:ss');
						if (d < 60) d = `${d} giây`;
						else {
							d = d.split(':');
							if (d[1] !== '00') d = `${d[0]} phút ${d[1]} giây`;
							else d = `${d[0]} phút`;
						}
						response.state = DepositConstant.DEPOSIT_STATE.FAILED;
						response.message = `Bạn đã nhập sai OTP quá 3 lần liên tiếp, vui lòng thử lại sau ${d} hoặc liên hệ bộ phận CSKH để được hỗ trợ!`;
						return response;
					}
					await RedisCache.set(`BankTransactionFailedOTP-${payload.accountId}`, countOTPFailed + 1, 300);
					response.state = DepositConstant.DEPOSIT_STATE.INVALID_OTP;
					response.message = _.get(bidvResponse, 'data.message', 'Mã OTP không chính xác');
					return response;
				}

				// Thanh toan Failed
				if (bidvResponse.state === GeneralConstant.SUPPLIER_STATE.FAILED) {
					const t = Math.round(_.toNumber(payload.total)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
					const alertMsg = `❗Thanh toán từ BIDVBank thất bại
					  \nTopup Response: ${JSON.stringify(bidvResponse.original)}
					  \nID: ${created.id} - Amount: ${t} - AccountId: ${payload.accountId}`;
					// NotifyService.SendAlertNotify(alertMsg, [], ['api']);

					await Broker.call('v1.eWalletPaymentLinkedOCBModel.updateOne', [{
						id: created.id

					}, { state: DepositConstant.DEPOSIT_STATE.FAILED, supplierResponsed }]);

					await RedisCache.delete(`OCBBankTransactionFailedOTP-${payload.accountId}`);
					response.state = DepositConstant.DEPOSIT_STATE.FAILED;
					response.message = _.get(bidvResponse, 'data.message', 'Thanh toán thất bại!');
					return response;
				}

				await Broker.call('v1.eWalletPaymentLinkedBIDVModel.updateOne', [{
					id: created.id
				}, {
					state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
					supplierResponsed,
					supplierTransaction: _.get(bidvResponse, 'transaction', null)
				}]);

				response.supplierTransaction = _.get(bidvResponse, 'transaction', null);
				response.state = bidvResponse.state;
				response.message = _.get(bidvResponse, 'message', null) || 'Thanh toán thành công';
				response.paymentLinkedId = created.id;

				return response;
			} catch (err) {
				Logger('[OPENEWALLET] >>> [DEPOSIT] >> [BIDV_SUPPLIER] >>', JSON.stringify(err));

				return response;
			}
		}

	};
};
