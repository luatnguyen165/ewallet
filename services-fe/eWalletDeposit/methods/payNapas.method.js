const _ = require('lodash');
const DepositConstant = require('../constants/deposit.constant');

module.exports = async function (payload) {
	// Payload
	const Logger = this.logger.info;
	const Broker = this.broker;
	const NapasLib = this.NapasLibs;
	const RedisCache = this.RedisCache();

	const response = {
		state: DepositConstant.DEPOSIT_STATE.FAILED,
		message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(NAPAS_ERR)'
	};
	try {
		const created = await Broker.call('v1.eWalletPaymentLinkedNapasModel.create', [{
			accountId: payload.accountId,
			paymentId: payload.paymentId,
			transactionId: payload.transaction,
			bankInfo: payload.cardInfo,
			amount: payload.amount,
			description: 'Thanh toán từ Napas',
			state: DepositConstant.DEPOSIT_STATE.PENDING
		}]);

		if (_.get(created, 'id', null) === null) {
			response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(NAPAS_ERR_01)';
			return response;
		}

		const paramsSend = {
			amount: payload.amount,
			transaction: payload.transaction,
			accountId: payload.accountId,
			redirectUrl: '',
			cardId: payload.cardId,
			env: _.get(payload, 'envName', 'MobileApp')
		};

		const supplierResponse = await NapasLib.Pay(paramsSend);

		await Broker.call('v1.eWalletPaymentLinkedNapasModel.updateOne', [{
			id: created.id
		}, {
			supplierResponsed: [supplierResponse.supplierResponse],
			supplierTransaction: _.get(supplierResponse, 'transaction', null)
		}]);

		response.supplierTransaction = _.get(supplierResponse, 'transaction', null);
		response.state = supplierResponse.state;
		response.message = _.get(supplierResponse, 'message', null) || 'Tạo yêu cầu thanh toán thành công';
		response.html = _.get(supplierResponse, 'html', null);
		response.paymentLinkedId = created.id;

		return response;
	} catch (err) {
		Logger('[OPENEWALLET] -> [DEPOSIT] -> [NAPAS_SUPPLIER] -> :: ', err);
		response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau';
		return response;
	}
};
