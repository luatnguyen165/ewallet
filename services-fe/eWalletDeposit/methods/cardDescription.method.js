const _ = require('lodash');

module.exports = async function (data = {}) {
	let description = '';
	let number = _.get(data, 'cardNumber', '') || '';
	if (number === '') {
		number = _.get(data, 'accountNumber', '') || '';
	}

	if (number === '') {
		number = _.get(data, 'bankAccountNumber', '') || '';
	}
	const bankCodeConfig = await this.broker.call('v1.eWalletBankCodeModel.findOne',
		[{ swiftCode: data.swiftCode }]);

	const bankName = _.get(bankCodeConfig, 'shortName', '');

	description = `${bankName}-${number.substr(-4, 4)}`;

	return description;
};
