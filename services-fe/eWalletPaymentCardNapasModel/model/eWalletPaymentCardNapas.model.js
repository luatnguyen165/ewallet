const GeneralConstant = require('../constants/general.constant');
const { values } = require('lodash');

const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

const MongooseSchema = mongoose.Schema;

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	transactionId: {
		type: String,
		required: true
	},
	paymentId: {
		type: String,
		default: null
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	state: {
		type: String,
		enum: values(GeneralConstant.STATE)
	},
	description: {
		type: String,
		default: null
	},
	bankInfo: {
		bankCode: {
			type: String,
			default: null
		},
		cardNumber: {
			type: String
		},
		cardHolder: {
			type: String,
			default: null,
			required: true
		},
		issuedAt: {
			type: Date,
			default: null
		},
		swiftCode: {
			type: String,
			default: null
		},
		shortName: {
			type: String,
			default: null
		}
	},
	supplierTransaction: {
		type: String,
		default: null
	},
	extraData: MongooseSchema.Types.Mixed,
	supplierResponsed: MongooseSchema.Types.Mixed,
	isChecked: {
		type: Boolean,
		default: false
	}
}, {
	collection: 'Payment_GatewayNapas',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ state: 1 });

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});
module.exports = mongoose.model(Schema.options.collection, Schema);