module.exports = {
	SUPPLIER: {
		BS: 'BS',
		BIDV: 'BIDV',
		OCB: 'OCB',
		PG: 'PG'
	},
	STATE: {
		NEW: 'NEW',
		TRANSFERING: 'TRANSFERING',
		TRANSFERED: 'TRANSFERED',
		FAILED: 'FAILED'
	}
};
