const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const generalConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	// Phuong thuc nguoi dung da dung de chuyen (rut ve the lien ket hay ve the noi dia)
	method: {
		type: String,
		default: null
	},
	serviceTransaction: {
		type: String,
		default: null
	},
	// Transaction ben minh
	transaction: {
		type: String,
		required: true
	},
	// Transaction ben ngan hang
	supplierTransaction: {
		type: String,
		default: null
	},
	amount: {
		type: Number,
		default: 0
	},
	total: {
		type: Number,
		default: 0
	},
	exchangeRate: {
		type: Number,
		default: 0
	},
	fee: {
		type: Number,
		default: 0
	},
	taxAmount: {
		type: Number,
		default: 0
	},
	bankTransactionId: {
		type: String,
		default: null
	},
	state: {
		type: String,
		enum: _.values(generalConstant.STATE),
		required: true
	},
	supplier: {
		type: String,
		enum: _.values(generalConstant.SUPPLIER),
		required: true
	},
	// Cập nhật các trạng thái nạp tiên
	stateAt: [Object],
	from: {
		bankAccountId: {
			type: Number
		},
		swiftCode: {
			type: String
		},
		bankInfo: Object
	},
	to: {
		cardNumber: {
			type: String
		},
		accountNumber: {
			type: String
		},
		fullname: {
			type: String
		},
		swiftCode: {
			type: String
		}
	},
	supplierResponsed: [String],
	description: {
		type: String,
		default: null
	},
	clientTransId: { // danh rieng cho OCB
		type: String,
		default: null
	}
}, {
	collection: 'TransferBank',
	versionKey: false,
	timestamps: true
});

Schema.index({ transaction: 1 }, { sparse: true, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
