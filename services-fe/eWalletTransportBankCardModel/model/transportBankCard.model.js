const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const TransportConstant = require('../constants/transport.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	transportId: {
		type: Number,
		required: true
	},
	appId: {
		type: Number,
		default: 1
	},
	transaction: {
		type: String,
		required: true
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	service: {
		code: {
			type: String,
			default: null
		},
		type: {
			type: String,
			default: null
		},
		transaction: {
			type: String,
			required: true
		}
	},
	state: {
		type: String,
		enum: _.values(TransportConstant.STATE),
		default: TransportConstant.STATE.PENDING
	},
	description: {
		type: String,
		default: null
	},
	note: {
		type: String,
		default: null
	},
	bankInfo: {
		swiftCode: String,
		bankName: String,
		cardNumber: String,
		cardHolder: String
	},
	supplierResponsed: Object
}, {
	collection: 'TransportBankCard',
	versionKey: false,
	timestamps: true
});

/*
  | ==========================================================
  | Plugins
  | ==========================================================
  */

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
  | ==========================================================
  | Methods
  | ==========================================================
  */

/*
  | ==========================================================
  | HOOKS
  | ==========================================================
  */

module.exports = mongoose.model(Schema.options.collection, Schema);
