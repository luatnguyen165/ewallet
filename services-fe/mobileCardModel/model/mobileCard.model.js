const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		default: null
	},
	amount: {
		type: Number,
		required: true
	},
	total: {
		type: Number,
		default: null
	},
	discount: {
		type: Number,
		default: 0
	},
	fee: {
		type: Number,
		default: 0
	},
	quantity: {
		type: Number,
		default: 1
	},
	supplier: {
		type: String,
		enum: _.values(GeneralConstant.CARD_SUPPLIER),
		default: null,
		required: true
	},
	transaction: {
		type: String,
		required: true,
		unique: true
	},
	description: {
		type: String,
		default: null
	},
	partnerTransaction: {
		type: String,
		default: null
	},
	cardInfo: [{
		amount: Number,
		total: Number,
		discount: {
			type: Number,
			default: 0
		},
		serial: {
			type: String
		},
		pin: {
			type: String
		},
		expiredAt: {
			type: Date
		},
		saleDate: {
			type: Date
		},
		state: {
			type: String,
			enum: _.values(GeneralConstant.CARD_ITEM_STATE),
			default: GeneralConstant.CARD_ITEM_STATE.PENDING
		}
	}],
	state: {
		type: String,
		enum: _.values(GeneralConstant.CARD_TRANSACTION_STATE),
		default: null
	},
	payment: Object, // Phương thức thanh toán
	supplierResponsed: Object,
	extraData: Object,
	isRefunded: {
		type: Boolean,
		default: false
	}
}, {
	collection: 'Service_MobileCard',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
