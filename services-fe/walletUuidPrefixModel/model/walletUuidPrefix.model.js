const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const walletUUIDConstant = require('../constants/walletUuidPrefix.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	prefix: {
		type: String,
		required: true
	},
	description: {
		type: String,
		default: null
	},
	length: {
		type: Number,
		default: null
	},
	type: {
		type: String,
		enum: _.values(walletUUIDConstant.PREFIX_TYPE)
	},
	isActive: {
		type: Boolean,
		default: true
	}
}, {
	collection: 'Service_UuidPrefixConfig',
	versionKey: false,
	timestamps: true
});

Schema.index({ uuid: 1, type: 1 }, { index: true });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
