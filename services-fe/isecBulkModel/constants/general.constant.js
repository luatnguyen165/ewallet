module.exports = {
	ISEC_SOURCE: {
		SAVED: 'SAVED',
		TRANSFERRED: 'TRANSFERRED',
		CREATED: 'CREATED'
	},
	ISEC_STATE: {
		REQUESTING: 'REQUESTING',
		PENDING: 'PENDING',
		DONATED: 'DONATE',
		USED: 'USED',
		DEPOSIT: 'DEPOSIT',
		LOCKED: 'LOCKED',
		CANCELED: 'CANCELED',
		FAILED: 'FAILED',
		EXPIRED: 'EXPIRED'
	},
	ISEC_BULKID_STATE: {
		NEW: 'NEW',
		CREATING: 'CREATING',
		CREATED: 'CREATED',
		FAILED: 'FAILED'
	}
};
