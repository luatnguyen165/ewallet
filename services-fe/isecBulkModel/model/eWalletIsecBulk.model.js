const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		default: null
	},
	partnerTransaction: { // gateTransactionId cũ, sửa lại vì DMS cũng dùng cho nhìu đối tác khác      type: String
		type: String
	},
	bulkId: {
		type: String,
		required: true,
		unique: true
	},
	orderId: {
		type: Number,
		default: null
	},
	destinationAccountId: {
		type: Number
	},
	quantity: {
		type: Number,
		required: true
	},
	quantityCreated: {
		type: Number,
		default: 0
	},
	amount: {
		type: Number,
		required: true
	},
	total: {
		type: Number,
		default: 0
	},
	fee: {
		type: Number,
		default: 0
	},
	callbackUrl: {
		type: String
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.ISEC_BULKID_STATE),
		default: GeneralConstant.ISEC_BULKID_STATE.CREATING,
		required: true
	}
}, {
	collection: 'Service_ISecBulk',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ destinationAccountId: 1 }, { sparse: false, unique: false });
Schema.index({ bulkId: 1 }, { sparse: false, unique: true });
Schema.index({
	quantityCreated: 1, bulkId: 1, state: 1, accountId: 1, id: 1, partnerTransaction: 1
}, { index: true });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
