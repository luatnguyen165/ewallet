const numeral = require('numeral');
const _ = require('lodash');
const GeneralConstant = require('../constants/general.constant');

const ErrorCode = require('../constants/errorCode.constant');

module.exports = async function (payload) {
	const Broker = this.broker;
	const Logger = this.logger.info;
	const response = {
		state: GeneralConstant.PAYMENT_STATE.FAILED,
		message: 'Rút tiền thất bại (PAY_ERR)'
	};
	try {
		const { accountInfo } = payload;

		const paymentData = {
			accountId: accountInfo.id,
			appId: payload.appId,
			transaction: payload.transaction,
			amount: payload.amount,
			state: GeneralConstant.PAYMENT_STATE.PENDING,
			method: 'WALLET',
			service: {
				code: 'WITHDRAW',
				type: 'WITHDRAW',
				transaction: payload.transaction
			},
			description: 'Rút tiền từ ví PayME'
		};

		let paymentCreated = null;
		try {
			paymentCreated = await Broker.call('v1.eWalletPaymentModel.create', [paymentData]);
		} catch (error) {
			response.message = 'Rút tiền thất bại. Vui lòng thử lại sau (PAY_ERR_01)';
			return response;
		}

		let limitSetting = await Broker.call('v1.eWalletSettingModel.findOne', [{
			key: 'limit.param.amount.withdraw'
		}]);

		limitSetting = limitSetting.value;

		if (_.get(limitSetting, 'min', false) === false) {
			limitSetting = { min: 0, max: 100000000 };
		}
		if (_.includes([GeneralConstant.ACCOUNT_TYPE.PERSONAL], accountInfo.accountType)) {
			if (payload.amount < limitSetting.min) {
				response.message = `Số tiền thanh toán không được nhỏ hơn ${numeral(limitSetting.min).format('0,0')} đ`;
				return response;
			}
			if (payload.amount > limitSetting.max) {
				response.message = `Số tiền thanh toán không vượt quá ${numeral(limitSetting.max).format('0,0')} đ`;
				return response;
			}
		}

		const paymentPayMEData = {
			accountId: accountInfo.id,
			transactionId: payload.transaction,
			paymentId: paymentCreated.id, // Add later
			amount: payload.amount,
			state: GeneralConstant.PAYMENT_STATE.PENDING,
			description: null, // Add later
			balance: {
				before: {
					cash: payload.walletBalance
				},
				after: {
					cash: payload.walletBalance
				}
			}
		};

		let paymentPayMECreated = null;
		try {
			paymentPayMECreated = await Broker.call('v1.eWalletPaymentPayMEModel.create', [paymentPayMEData]);
		} catch (error) {
			response.message = 'Rút tiền thất bại. Vui lòng thử lại sau (PAY_ERR_02)';
			return response;
		}

		let paymentDataUpdate = {
			methodData: {
				resolveType: 'PaymentWalletObject',
				paymentWalletId: paymentPayMECreated.id,
				accountInfo: {
					accountId: _.get(accountInfo, 'id', null),
					fullname: _.get(accountInfo, 'fullname', null)
				}
			}
		};

		// GOI qua Wallet tien hanh Rut
		let withdrawWallet = null;
		try {
			const paramsWithdraw = {
				accountId: accountInfo.id,
				amount: payload.amount,
				description: 'Rut tien tu vi PayME',
				referData: paymentPayMECreated
			};
			withdrawWallet = await this.WalletLibs.Withdraw(paramsWithdraw);
		} catch (error) {
			response.message = 'Rút tiền thất bại. Vui lòng thử lại sau (PAY_ERR_03)';
			Logger(`[WITHDRAW] => [WITHDRAW_WALLET_ERROR]: ${error}`);

			paymentDataUpdate = {
				...paymentDataUpdate,
				state: GeneralConstant.PAYMENT_STATE.FAILED
			};
		}

		if (_.get(withdrawWallet, 'succeeded', false) === false) {
			paymentDataUpdate = {
				...paymentDataUpdate,
				state: GeneralConstant.PAYMENT_STATE.FAILED
			};
			response.message = 'Rút tiền thất bại. Vui lòng thử lại sau (PAY_ERR_04)';
		} else {
			paymentDataUpdate = {
				...paymentDataUpdate,
				state: GeneralConstant.PAYMENT_STATE.SUCCEEDED,
				balance: withdrawWallet.balance
			};
			response.state = GeneralConstant.PAYMENT_STATE.SUCCEEDED;
		}

		paymentPayMECreated = await Broker.call('v1.eWalletPaymentPayMEModel.findOneAndUpdate', [{
			id: paymentPayMECreated.id
		}, {
			state: paymentDataUpdate.state
		}, {
			new: true
		}]);

		paymentCreated = await Broker.call('v1.eWalletPaymentModel.findOneAndUpdate',
			[{ id: paymentCreated.id }, { ...paymentDataUpdate }, { new: true }
			]);

		response.payment = {
			id: paymentCreated.id,
			transaction: payload.transaction,
			method: 'WALLET',
			state: paymentCreated.state,
			description: 'Ví PayME'
		};
		response.message = 'Thanh toán thành công';
		response.balance = withdrawWallet.balance;
		return response;
	} catch (e) {
		console.log(e);
		Logger('[WITHDRAW] => [PAYMENT] => [WALLET] => [ERROR] >>', e);
		return response;
	}
};
