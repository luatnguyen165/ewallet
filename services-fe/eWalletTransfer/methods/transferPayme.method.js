const { MoleculerError } = require('moleculer').Errors;
const _ = require('lodash');
const Decimal = require('decimal.js');
const moment = require('moment');

// Constants
const ErrorCodeConstant = require('../constants/errorCode.constant');
const AccountConstant = require('../constants/account.constant');
const GeneralConstant = require('../constants/general.constant');
const DepositConstant = require('../constants/deposit.constant');

module.exports = async function (transaction) {
	try {
		const transferMoneyInfo = await this.broker.call('v1.eWalletTransferMoneyModel.findOne', [{ transaction }]);

		// 	const cardDescription = await this.CardDescription(_.get(paymentMethodData, 'paymentInfo', {}));
		const receiverHistoryObj = {
			accountId: transferMoneyInfo.receiverInfo.accountId,
			service: {
				type: 'RECEIVE_MONEY',
				id: transferMoneyInfo.id,
				transaction: transferMoneyInfo.transaction,
				method: 'DEPOSIT',
				state: DepositConstant.DEPOSIT_STATE.PENDING,
				name: `Nhận tiền từ ví ${transferMoneyInfo.senderInfo.fullname}`,
				data: {
					id: transferMoneyInfo.id,
					resolveType: 'DepositObject',
					state: DepositConstant.DEPOSIT_STATE.PENDING,
					transaction,
					accountId: transferMoneyInfo.receiverInfo.accountId
				}
			},
			amount: transferMoneyInfo.amount,
			total: transferMoneyInfo.amount,
			fee: 0,
			state: DepositConstant.DEPOSIT_STATE.PENDING,
			description: `Nhận tiền từ ví ${transferMoneyInfo.senderInfo.fullname}`,
			changed: '+',
			tags: ['TRANSFER_MONEY', 'RECEIVE_MONEY']
		};

		let historyDepositCreated = null;
		try {
			historyDepositCreated = await this.broker.call('v1.eWalletHistoryModel.create', [receiverHistoryObj]);
		} catch (error) {
			this.logger.info(`[DEPOSIT_EWALLET] => [CREATE_HISTORY_DEPOSIT_ERROR]: ${error}`);
			return {
				code: ErrorCodeConstant.TRANSFER_FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(E04)'
			};
		}
		if (_.get(historyDepositCreated, 'id', null) === null) {
			this.logger.info(`[DEPOSIT_EWALLET] => [CREATE_HISTORY_DEPOSIT_ERROR_2]: ${JSON.stringify(receiverHistoryObj)}`);
			return {
				code: ErrorCodeConstant.TRANSFER_FAILED,
				message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(E05)'
			};
		}

		const paramsRequest = {
			accountId: transferMoneyInfo.receiverInfo.accountId,
			amount: transferMoneyInfo.amount,
			description: 'Nạp tiền vào ví PayME.',
			referData: transferMoneyInfo
		};
		console.log(paramsRequest);

		let depositResponsed = null;
		try {
			this.logger.info(`[DEPOSIT] => [DEPOSIT_WALLET_PARAMS]: ${JSON.stringify(paramsRequest)}`);
			depositResponsed = await this.WalletLibs.Deposit(paramsRequest);
			this.logger.info(`[DEPOSIT] => [DEPOSIT_WALLET]: ${JSON.stringify(depositResponsed)}`);
		} catch (error) {
			historyDepositCreated = await this.broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [{
				id: historyDepositCreated.id
			}, {
				state: DepositConstant.DEPOSIT_STATE.FAILED,
				'service.state': DepositConstant.DEPOSIT_STATE.FAILED,
				'service.data.state': DepositConstant.DEPOSIT_STATE.FAILED
			}, { new: true }]);
			this.logger.info(`[DEPOSIT] => [DEPOSIT_WALLET_ERROR]: ${error}`);
			return {
				state: GeneralConstant.TRANSFER_STATE.FAILED,
				code: ErrorCodeConstant.TRANSFER_FAILED,
				message: this.__(ErrorCodeConstant.TRANSFER_FAILED.toString()),
				data: depositResponsed
			};
		}

		if (depositResponsed.succeeded === false) {
			historyDepositCreated = await this.broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [{
				id: historyDepositCreated.id
			}, {
				state: DepositConstant.DEPOSIT_STATE.FAILED,
				'service.state': DepositConstant.DEPOSIT_STATE.FAILED,
				'service.data.state': DepositConstant.DEPOSIT_STATE.FAILED,
				balance: depositResponsed.balance
			}, { new: true }]);
			return {
				code: ErrorCodeConstant.TRANSFER_FAILED,
				message: this.__(ErrorCodeConstant.TRANSFER_FAILED.toString())
			};
		}
		console.log('depositResponsed', depositResponsed);

		historyDepositCreated = await this.broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [{
			id: historyDepositCreated.id
		}, {
			state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
			'service.state': DepositConstant.DEPOSIT_STATE.SUCCEEDED,
			'service.data.state': DepositConstant.DEPOSIT_STATE.SUCCEEDED
		}, { new: true }]);

		console.log('historyDepositCreated', historyDepositCreated);
		return {
			state: GeneralConstant.TRANSFER_STATE.SUCCEEDED,
			code: ErrorCodeConstant.TRANSFER_SUCCEEDED,
			message: this.__(ErrorCodeConstant.TRANSFER_SUCCEEDED.toString()),
			data: depositResponsed
		};
	} catch (error) {
		this.logger.info(`[eWalletTransfer] transferPayme Error: ${JSON.stringify(error)}`);
		return {
			code: ErrorCodeConstant.INTERNAL_SERVER_ERROR,
			message: this.__(ErrorCodeConstant.INTERNAL_SERVER_ERROR.toString())
		};
	}
};
