const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const Decimal = require('decimal.js');
const Numeral = require('numeral');

const ModelConstant = require('../constants/model.constant');
const AccountConstant = require('../constants/account.constant');
const TransferMoneyConstant = require('../constants/transfer.constant');

module.exports = async function (params = {
	sender: {}, receiver: {}, note: '', amount: 0, ip: '', clientId: '', payment: {}, transport: {}
}) {
	const {
		AccountModel, LinkedModel, HistoryService, TransferMoneyModel
	} = ModelConstant;
	const response = {
		successed: false,
		massage: 'Chuyển tiền thất bại. Vui lòng thử lại sau (E001).'
	};
	let transferMoneyId = null;

	let paramsCreate = {};
	let paramsRequest = {};
	let resultCreate = null;
	let resultUpdate = null;
	let fields = {};
	let updateInfo = {};

	const supplierResponse = { payment: {}, transport: {} };

	try {
		const {
			amount, clientId, sender, receiver, note, ip, payment, transport, description
		} = params;

		const walletCheck = await this.WalletLibs.Information(sender.id);
		if (walletCheck.succeeded === false) {
			this.logger.info(`[TRANSFER_WALLET]:::[CHECK_WALLET]: ${JSON.stringify(walletCheck)}`);
			response.massage = 'Không tìm thấy thông tin tài khoản Ví.';
			return response;
		}

		// Tính Fee người chuyển
		const fee = 0;
		const totalPay = _.toNumber((new Decimal(amount)).add(new Decimal(fee)));
		// Tính Fee người nhận
		const receiveFee = 0;
		const totalReceive = _.toNumber((new Decimal(amount)).minus(new Decimal(receiveFee)));

		if (walletCheck.balance < totalPay) {
			response.massage = 'Số dư Ví không đủ để thực hiện giao dịch.';
			return response;
		}

		const transferUUID = await this.GetUUID('TRANSFER_MONEY');
		if (_.isEmpty(transferUUID) === true) {
			response.massage = 'Chuyển tiền thất bại. Vui lòng thử lại sau (E002).';
			return response;
		}

		paramsCreate = {
			accountId: sender.id,
			supplier: TransferMoneyConstant.SUPPLIER.WALLET,
			appId: sender.appId || 0,
			transaction: transferUUID,
			description,
			amount,
			note,
			senderInfo: {
				accountId: sender.id,
				fullname: sender.fullname
			},
			receiverInfo: {
				accountId: receiver.id,
				fullname: receiver.fullname
			},
			fee,
			total: totalPay,
			state: TransferMoneyConstant.STATE.PENDING
		};
		try {
			resultCreate = await this.broker.call(TransferMoneyModel.create, [paramsCreate]);
			if (_.get(resultCreate, 'id', false) === false) {
				this.logger.info(`[TRANSFER_WALLET]:::[CREATE_TRANSFER_ERROR_1]: ${JSON.stringify(paramsCreate)}`);
				response.massage = 'Chuyển tiền thất bại. Vui lòng thử lại sau (E003).';
				return response;
			}
		} catch (error) {
			this.logger.info(`[TRANSFER_WALLET]:::[CREATE_TRANSFER_ERROR_2]: ${JSON.stringify(paramsCreate)}`);
			response.massage = 'Chuyển tiền thất bại. Vui lòng thử lại sau (E004).';
			return response;
		}
		const transferMoney = resultCreate;
		transferMoneyId = transferMoney.id;

		const sourceName = (_.get(transferMoney, 'senderInfo.fullname') || '').split(/\s+/).pop();
		const targetName = (_.get(transferMoney, 'receiverInfo.fullname') || '').split(/\s+/).pop();

		paramsCreate = {
			dataHistory: {
				accountId: sender.id,
				appId: sender.appId || 0,
				service: {
					type: TransferMoneyConstant.SERVICE_CODE,
					id: transferMoney.id,
					transaction: transferMoney.transaction,
					state: transferMoney.state,
					name: `Chuyển tiền đến ${targetName}`,
					data: {
						resolveType: TransferMoneyConstant.RESOLVE_TYPE,
						transferMoneyId: transferMoney.id,
						transaction: transferMoney.transaction,
						accountId: transferMoney.accountId,
						type: TransferMoneyConstant.TYPE.SEND,
						state: transferMoney.state
					}
				},
				amount: transferMoney.amount,
				fee: transferMoney.fee,
				total: transferMoney.total,
				discount: 0,
				bonus: 0,
				state: TransferMoneyConstant.HISTORY_STATE.PENDING,
				description: 'Chuyển tiền từ ví PayME.',
				changed: '',
				tags: ['TRANSFER_PAYME']
			}
		};
		try {
			resultCreate = await this.broker.call(HistoryService.create, paramsCreate);
			if (_.get(resultCreate, 'code', null) !== 202700) {
				this.logger.info(`[TRANSFER_WALLET] >>> Create_History_Error_1:  ${JSON.stringify({ paramsCreate, resultCreate })}`);
				response.massage = 'Chuyển tiền thất bại. Vui lòng thử lại sau (E005).';
				return response;
			}
		} catch (error) {
			this.logger.info(`[TRANSFER_WALLET]>>> Create_History_Error_2: ${JSON.stringify({ paramsCreate, error: String(error) })}`);
			response.massage = 'Chuyển tiền thất bại. Vui lòng thử lại sau (E006).';
			return response;
		}
		let historyInfo = resultCreate.data;
		paramsRequest = {
			accountId: sender.id,
			appId: sender.appId || 0,
			amount: transferMoney.total,
			ip,
			clientId,
			service: {
				code: TransferMoneyConstant.SERVICE_CODE,
				type: TransferMoneyConstant.SUPPLIER.WALLET,
				transaction: transferMoney.transaction
			},
			method: payment
		};
		let requestPayment = null;
		try {
			requestPayment = await this.broker.call('v1.eWalletPayment.pay', paramsRequest, { timeout: 90 * 1000 });
		} catch (error) {
			this.logger.info(`[TRANSFER_WALLET] >>> Payment_Params :  ${JSON.stringify(paramsRequest)}`);
			this.logger.info(`[TRANSFER_WALLET] >>> Payment_Error :  ${String(error)}`);
		}
		supplierResponse.payment = { request: _.clone(paramsRequest), response: _.clone(requestPayment) };
		const paymentInfo = _.get(requestPayment, 'payment', null);
		if (_.get(requestPayment, 'state', '') !== 'SUCCEEDED') {
			resultUpdate = await this.broker.call(TransferMoneyModel.updateOne, [
				{ id: transferMoney.id },
				{
					state: 'FAILED',
					payment: paymentInfo
				}
			]);
			if (_.get(requestPayment, 'nModified', 0) < 1) {
				this.logger.info(`[TRANSFER_WALLET] >>> Update_Transfer_Error_1 ${JSON.stringify({ transferMoney })}`);
			}
			fields = { id: historyInfo.id };
			updateInfo = {
				state: TransferMoneyConstant.HISTORY_STATE.FAILED,
				'service.state': TransferMoneyConstant.STATE.FAILED,
				'service.data.state': TransferMoneyConstant.STATE.FAILED,
				payment: paymentInfo
			};
			resultUpdate = await this.broker.call(HistoryService.update, { fields, updateInfo }, { timeout: 60 * 1000 });
			if (resultUpdate.code !== 202710) {
				this.logger.info(`[TRANSFER_WALLET]>>> Update_History_Error_1: ${JSON.stringify({ fields, updateInfo })}`);
			}

			response.massage = _.get(requestPayment, 'message', null) || 'Chuyển tiền thất bại. Vui lòng thử lại sau (E007).';
			return response;
		}
		// Thanh toán thành công.
		resultUpdate = await this.broker.call(TransferMoneyModel.updateOne, [
			{ id: transferMoney.id },
			{
				state: TransferMoneyConstant.STATE.SUCCEEDED,
				payment: requestPayment.payment
			}
		]);
		if (_.get(resultUpdate, 'nModified', 0) < 1) {
			this.logger.info(`[TRANSFER_WALLET] >>> Update_Transfer_Error_2 ${JSON.stringify({ transferMoney })}`);
			response.massage = 'Chuyển tiền thất bại. Vui lòng thử lại sau (E008).';
			return response;
		}

		fields = { id: historyInfo.id };
		updateInfo = {
			state: TransferMoneyConstant.HISTORY_STATE.SUCCEEDED,
			'service.state': TransferMoneyConstant.STATE.SUCCEEDED,
			'service.data.state': TransferMoneyConstant.STATE.SUCCEEDED,
			changed: '-',
			balance: _.get(requestPayment, 'data.balance', {}),
			payment: requestPayment.payment,
			publishedAt: new Date()
		};
		resultUpdate = await this.broker.call(HistoryService.update, { fields, updateInfo }, { timeout: 60 * 1000 });
		if (resultUpdate.code !== 202710) {
			this.logger.info(`[TRANSFER_WALLET]>>> Update_History_Error_2: ${JSON.stringify({ fields, updateInfo })}`);
			response.massage = 'Chuyển tiền thất bại. Vui lòng thử lại sau (E009).';
			return response;
		}
		historyInfo = resultUpdate.data;

		paramsRequest = {
			accountId: sender.id,
			appId: sender.appId || 0,
			amount: totalReceive,
			description: 'Chuyen tien tu vi PayME.',
			service: {
				code: TransferMoneyConstant.SERVICE_CODE,
				type: TransferMoneyConstant.SUPPLIER.WALLET,
				transaction: transferMoney.transaction
			},
			transport
		};

		// Call transfer money action
		let requestTransport = null;
		try {
			requestTransport = await this.broker.call('v1.eWalletTransport.transfer', paramsRequest, { timeout: 90 * 1000 });
		} catch (error) {
			this.logger.info(`[TRANSFER_WALLET] >>> Transfer_Params ${JSON.stringify(paramsRequest)}`);
			this.logger.info(`[TRANSFER_WALLET] >>> Transfer_Error ${String(error)}`);
		}

		const transportInfo = _.get(requestTransport, 'transportInfo', null);
		supplierResponse.transport = { request: _.clone(paramsRequest), response: _.clone(requestTransport) };
		if (_.get(requestTransport, 'state', '') !== 'SUCCEEDED') {
			resultUpdate = await this.broker.call(TransferMoneyModel.updateOne, [
				{ id: transferMoney.id },
				{
					state: TransferMoneyConstant.STATE.FAILED,
					transport: transportInfo
				}
			]);
			if (_.get(resultUpdate, 'nModified', 0) < 1) {
				this.logger.info(`[TRANSFER_WALLET]: TransferModel Update Failed => ${JSON.stringify({ transferMoney })}`);
			} else {
				const formatAmount = Numeral(transferMoney.total).format('0,0');
				// Hoàn tiền cho thằng chuyển

				// const refundResponse = await PaymentService.Refund({
				// 	paymentId: _.get(paymentResponse, 'payment.id', null),
				// 	appId: createdTransferMoney.appId,
				// 	amount: createdTransferMoney.total,
				// 	serviceCode: TransferMoneyConstant.SERVICE_CODE,
				// 	serviceId: createdTransferMoney.id,
				// 	description: `Bạn được hoàn ${formatedAmount}đ do chuyển tiền thất bại.`
				// });
				// if (refundResponse.state !== PaymentConstant.STATE.SUCCEEDED) {
				// 	const alertMsg = `Hoàn tiền thất bại cho dịch vụ chuyển tiền (TRANSFER_MONEY - WALLET)
				// 	\ntransferMoneyId: ${createdTransferMoney.id}
				// 	\nrefundResponse: ${JSON.stringify(refundResponse)}`;
				// 	NotifyService.SendAlertNotify(alertMsg, [], ['api']);
				// } else {
				// 	let historySendNotification = refundResponse.history;
				// 	if (_.get(historySendNotification, 'toObject', null)) {
				// 		historySendNotification = historySendNotification.toObject();
				// 	}

				// 	historySendNotification.clientId = params.clientId || null;
				// 	CreateAndPushNotification.pushNotificationCreate(
				// 		createdTransferMoney.accountId,
				// 		`Bạn được hoàn ${formatedAmount}đ do chuyển tiền thất bại.`,
				// 		historySendNotification,
				// 		historySendNotification.service.name
				// 	);
				// 	const pushData = {
				// 		id: historySendNotification.id,
				// 		serviceCode: historySendNotification.service.code,
				// 		serviceId: historySendNotification.service.id,
				// 		accountId: historySendNotification.accountId,
				// 		transaction: historySendNotification.service.transaction,
				// 		state: historySendNotification.state ? historySendNotification.state : historySendNotification.service.state,
				// 		amount: historySendNotification.amount,
				// 		total: historySendNotification.total,
				// 		fee: historySendNotification.fee,
				// 		discount: historySendNotification.discount,
				// 		cashback: historySendNotification.cashback,
				// 		bonus: historySendNotification.bonus,
				// 		changed: historySendNotification.changed,
				// 		description: historySendNotification.description,
				// 		createdAt: historySendNotification.createdAt,
				// 		updatedAt: historySendNotification.updatedAt,
				// 		tags: historySendNotification.tags,
				// 		extraData: historySendNotification.extraData,
				// 		source: historySendNotification.method ? historySendNotification.method.description : 'N/A'
				// 	};
				// 	StreamService.emit('NotifyService', pushData);
				// }
			}

			fields = { id: historyInfo.id };
			updateInfo = {
				state: TransferMoneyConstant.HISTORY_STATE.FAILED,
				'service.state': TransferMoneyConstant.STATE.FAILED,
				'service.data.state': TransferMoneyConstant.STATE.FAILED,
				transport: transportInfo
			};
			resultUpdate = await this.broker.call(HistoryService.update, { fields, updateInfo }, { timeout: 60 * 1000 });
			if (resultUpdate.code !== 202710) {
				this.logger.info(`[TRANSFER_WALLET]:::[UPDATE_HISTORY_ERROR_3]: ${JSON.stringify({ fields, updateInfo })}`);
			}

			response.massage = _.get(requestTransport, 'message', null) || 'Chuyển tiền thất bại. Vui lòng thử lại sau (E010).';
			return response;
		}
		// Transfer Successed
		resultUpdate = await this.broker.call(TransferMoneyModel.updateOne, [
			{ id: transferMoney.id },
			{
				transport: transportInfo
			}
		]);
		if (_.get(resultUpdate, 'nModified', 0) < 1) {
			this.logger.info(`[TRANSFER_WALLET] >>> Update_Transfer_Error_4 ${JSON.stringify({ transferMoney })}`);
		}

		fields = { id: historyInfo.id };
		updateInfo = {
			transport: transportInfo
		};
		resultUpdate = await this.broker.call(HistoryService.update, { fields, updateInfo }, { timeout: 60 * 1000 });
		if (resultUpdate.code !== 202710) {
			this.logger.info(`[TRANSFER_WALLET]>>> Update_History_Error_3: ${JSON.stringify({ fields, updateInfo })}`);
		}
		historyInfo = resultUpdate.data;

		try {
			const senderName = _.upperFirst(_.toLower(sender.fullname.split(/\s+/).pop()));
			paramsCreate = {
				dataHistory: {
					accountId: receiver.id,
					appId: sender.appId,
					service: {
						type: TransferMoneyConstant.SERVICE_CODE,
						id: transferMoney.id,
						transaction: transferMoney.transaction,
						state: TransferMoneyConstant.STATE.SUCCEEDED,
						name: `Nhận tiền từ ${sourceName}`,
						data: {
							resolveType: TransferMoneyConstant.RESOLVE_TYPE,
							transferMoneyId: transferMoney.id,
							transaction: transferMoney.transaction,
							accountId: transferMoney.accountId,
							type: TransferMoneyConstant.TYPE.RECEIVE,
							state: TransferMoneyConstant.STATE.SUCCEEDED
						}
					},
					amount: transferMoney.amount,
					fee: receiveFee,
					discount: 0,
					bonus: 0,
					total: totalReceive,
					balance: requestTransport.balance,
					state: TransferMoneyConstant.HISTORY_STATE.SUCCEEDED,
					description: `Nhận tiền từ ví của ${senderName}.`,
					changed: '+',
					tags: ['RECEIVE_MONEY', 'TRANSFER_PAYME'],
					transport: transportInfo
				}
			};

			resultCreate = await this.broker.call(HistoryService.create, paramsCreate);
			if (_.get(resultCreate, 'code', null) !== 202700) {
				this.logger.info(`[TRANSFER_WALLET] >>> Create_History_Error_3:  ${JSON.stringify({ paramsCreate, resultCreate })}`);
			}
		} catch (error) {
			this.logger.info(`[TRANSFER_WALLET]>>> Create_History_Error_4: ${JSON.stringify({ paramsCreate, error: String(error) })}`);
		}
		const historyReceiver = resultCreate.data;

		response.successed = true;
		response.massage = 'Chuyển tiền thành công.';
	} catch (error) {
		console.error('[TRANSFER_WALLET]:::Exception >>>', error);
		this.logger.info(`[TRANSFER_WALLET]:::Exception >>> ${String(error)}`);
	} finally {
		// Update record supplierResponse for trace logs
		if (response.successed === false) {
			await this.broker.call(TransferMoneyModel.updateOne, [
				{ id: transferMoneyId },
				{
					$push: {
						supplierResponse: {
							payment: JSON.stringify(supplierResponse.payment),
							transport: JSON.stringify(supplierResponse.transport),
							response: JSON.stringify(response)
						}
					}
				}
			], { timeout: 60 * 1000 });
		}
	}
	return response;
};
