const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;

const ModelConstant = require('../constants/model.constant');
const AccountConstant = require('../constants/account.constant');

module.exports = async function (accountId, isTransfer, isCheckLinked = true) {
	const { AccountModel, LinkedModel, HistoryModel } = ModelConstant;

	const result = {
		isValid: false,
		state: 'FAILED'
	};
	try {
		this.logger.info(`[METHOD_CHECK_ACCOUNT]::: accountId: ${accountId}`);
		if (!!accountId === false) {
			throw new MoleculerError('ACCOUNT_NOT_FOUND');
		}
		const accountInfo = await this.broker.call(AccountModel.findOne, [{ id: accountId }]);
		if (_.has(accountInfo, 'id') === false) {
			throw new MoleculerError('ACCOUNT_NOT_FOUND');
		}

		if (accountInfo.isActive === false || accountInfo.state !== 'OPENED') {
			throw new MoleculerError('ACCOUNT_LOCKED');
		}

		if (_.get(accountInfo, 'kyc.state', null) !== AccountConstant.ACCOUNT_KYC_STATE.APPROVED) {
			throw new MoleculerError('ACCOUNT_NOT_KYC');
		}

		if (isCheckLinked) {
			const paramsFind = {
				accountId: accountInfo.id,
				state: 'LINKED'
			};
			const linkedInfo = await this.broker.call(LinkedModel.findOne, [paramsFind]);
			if (_.has(linkedInfo, 'id') === false) throw new MoleculerError('ACCOUNT_NOT_LINKED');
			if (isTransfer) {
				const isBusiness = accountInfo.accountType === AccountConstant.ACCOUNT_TYPE.BUSINESS;
				const isApplyTransfer = _.includes(accountInfo.scope, 'account.bussiness.transferMoney');
				if (isBusiness === true && isApplyTransfer === false) {
					throw new MoleculerError('ACCOUNT_NOT_APPLY_TRANSFER');
				}
			}
		}

		result.isValid = true;
		result.accountInfo = accountInfo;
	} catch (error) {
		if (error.name === 'MoleculerError') {
			result.state = error.message || 'FAILED';
		}
	}
	return result;
};
