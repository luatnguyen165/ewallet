const moleculerI18n = require('moleculer-i18n-js');
const path = require('path');
const _ = require('lodash');

// Library
const WalletLibrary = require('./libraries/wallet.library');

module.exports = {
	name: 'eWalletTransfer',
	version: 1,
	mixins: [moleculerI18n],
	i18n: {
		directory: path.join(__dirname, 'locales'),
		locales: ['vi', 'en'],
		defaultLocale: 'vi'
	},

	/**
	 * Settings
	 */
	settings: {},
	/**
	 * Dependencies
	 */
	dependencies: [],
	/**
	 * Actions
	 */
	actions: {
		transfer: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/payment/transfer',
				auth: false
			},
			params: {
				body: {
					$$type: 'object',
					amount: 'number',
					phone: 'string',
					description: 'string',
					transaction: 'string|optional',
					otp: 'string|optional',
					transport: {
						$$type: 'object',
						wallet: {
							$$type: 'object|optional',
							phone: 'string'
						},
						bankCard: {
							$$type: 'object|optional',
							cardNumber: 'string',
							cardHolder: 'string',
							swiftCode: 'string'
						},
						bankAccount: {
							$$type: 'object|optional',
							accountName: 'string',
							accountNumber: 'string',
							swiftCode: 'string'
						}
					}
				}
			},
			handler: require('./actions/transfer.action')
		},
		verifyTransfer: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/payment/transfer',
				auth: false
			},
			params: {
				body: {
					$$type: 'object',
					phone: 'string',
					transaction: 'string',
					otp: 'string'
				}
			},
			handler: require('./actions/verifyTransfer.action')
		},
		transferSDK: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/sdk/transfer',
				auth: {
					strategies: ['Ewallet'],
					mode: 'optional'
				}
			},
			params: {
				body: {
					$$type: 'object',
					amount: 'number',
					note: 'string',
					description: 'string|optional',
					payment: {
						type: 'object',
						strict: 'remove',
						props: {
							wallet: {
								$$type: 'object|optional',
								requireSecurityCode: 'boolean',
								securityCode: 'string|optional'
							},
							bankCard: {
								$$type: 'object|optional',
								cardNumber: 'string',
								cardHolder: 'string',
								issuedAt: 'string|optional',
								expiredAt: 'string|optional',
								notifyUrl: 'string|optional',
								envName: 'string|optional'
							},
							linked: {
								$$type: 'object|optional',
								linkedId: 'number'
							}
						},
						minProps: 1,
						maxProps: 1
					},
					transport: {
						type: 'object',
						strict: 'remove',
						props: {
							wallet: {
								$$type: 'object|optional',
								accountId: 'number'
							},
							bankCard: {
								$$type: 'object|optional',
								swiftCode: 'string',
								cardNumber: 'string',
								cardHolder: 'string|optional',
								supplier: 'string|optional'
							},
							bankAccount: {
								$$type: 'object|optional',
								swiftCode: 'string',
								bankAccountNumber: 'string',
								bankAccountName: 'string|optional',
								supplier: 'string|optional'
							},
							linked: {
								$$type: 'object|optional',
								linkedId: 'number',
								supplier: 'string|optional'
							}
						},
						minProps: 1,
						maxProps: 1
					},
					clientId: 'string'
				}
			},
			timeout: 3 * 60 * 1000,
			handler: require('./actions/transferSDK.action')
		}
	},
	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		GetAuthInfo: require('./methods/getAuthInfo.method'),
		RedisCache: require('./methods/redisCache.method'),
		AccountValidation: require('./methods/accountValidation.method'),
		GetUUID: require('./methods/getUUID.method'),
		TransferToWallet: require('./methods/transferToWallet.method'),

		PayWallet: require('./methods/payWallet.method'),
		CalcFee: require('./methods/calcFee.method'),
		CardDescription: require('./methods/cardDescription.method'),
		PhoneUtility: require('./methods/phoneUtility.method'),
		TransferPayme: require('./methods/transferPayme.method')
	},
	/**
   * Service created lifecycle event handler
   */
	created() {
		try {
			_.set(this, 'WalletLibs', WalletLibrary({ logger: this.logger.info }));
		} catch (error) {
			this.logger.info(`[Generate_Library_Error] => ${error}`);
		}
	}
};
