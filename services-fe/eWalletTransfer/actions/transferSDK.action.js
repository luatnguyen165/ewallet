const { MoleculerError } = require('moleculer').Errors;
const _ = require('lodash');
const Decimal = require('decimal.js');
const moment = require('moment');

// Constants
const CodeConstant = require('../constants/errorCode.constant');
const ModelConstant = require('../constants/model.constant');
const TransferConstant = require('../constants/transfer.constant');

const ModuleConstant = {
	LIMIT_SETTING_KEY: 'limit.param.amount.withdraw',
	DEFAULT_AMOUNT_MIN: 10000,
	DEFAULT_AMOUNT_MAX: 100000000,
	ALLOWED_TRANSPORT_METHOD: [
		'bankCard',
		'bankAccount',
		'wallet'
	],
	ALLOWED_PAYMENT_METHOD: [
		'wallet'
	]
};

module.exports = async function (ctx) {
	const response = {
		code: CodeConstant.TRANSFER_SDK.FAILED,
		message: ''
	};

	const { AccountModel, LinkedModel, HistoryModel } = ModelConstant;

	const paramsCheck = {};
	let checkAccount = null;
	let resultTransfer = null;

	try {
		const {
			amount, note, description, payment, transport, clientId
		} = _.get(ctx, 'params.body', {});
		const authInfo = this.GetAuthInfo(ctx.meta);
		this.logger.info(`[TRANSFER_SDK]:::Payload >> ${JSON.stringify(ctx.params.body)}`);
		this.logger.info(`[TRANSFER_SDK]:::AuthInfo >> ${JSON.stringify(authInfo)}`);

		// Tạm đóng đợi action getSetting;

		// const withdrawEnable = await this.broker.call('v1.eWalletSetting.getSetting');
		// if (!withdrawEnable) {
		// 	return {
		// 		succeeded: false,
		// 		message: 'Chức năng đang được bảo trì, vui lòng thử lại sau hoặc liên hệ CSKH để được hỗ trợ!'
		// 	};
		// }

		checkAccount = await this.AccountValidation(authInfo.accountId, true);
		if (checkAccount.isValid === false) {
			this.logger.info(`[TRANSFER_SDK]:::[CHECK_ACCOUNT_ERROR_1]: ${JSON.stringify({ accountId: authInfo.accountId })}`);
			throw new MoleculerError(null, CodeConstant.TRANSFER_SDK[checkAccount.state]);
		}
		const { accountInfo } = checkAccount;
		const transportMethod = _.keys(transport)[0];

		const paramsTransfer = {
			sender: { ...checkAccount.accountInfo, appId: _.get(authInfo, 'appId', null) || 0 },
			note,
			description,
			amount,
			ip: _.get(ctx, 'meta.clientIp', null),
			clientId,
			payment,
			transport
		};
		switch (transportMethod) {
			case 'wallet': {
				const receiverAccountId = _.get(transport, 'wallet.accountId', null);
				checkAccount = await this.AccountValidation(receiverAccountId, false, false);
				if (checkAccount.isValid === false) {
					this.logger.info(`[TRANSFER_SDK]:::[CHECK_ACCOUNT_ERROR_2]: ${JSON.stringify({ accountId: receiverAccountId })}`);
					throw new MoleculerError(null, CodeConstant.TRANSFER_SDK[checkAccount.state]);
				}
				if (receiverAccountId === accountInfo.id) {
					throw new MoleculerError(null, CodeConstant.TRANSFER_SDK.ACCOUNT_PAYMENT_AND_TRANSPORT_IS_SAME);
				}
				// Call method transfer wallet
				paramsTransfer.receiver = checkAccount.accountInfo;
				resultTransfer = await this.TransferToWallet(paramsTransfer);
				if (resultTransfer.successed !== true) {
					this.logger.info(`[TRANSFER_SDK]:::[WALLET_ERROR]: ${JSON.stringify(resultTransfer)}`);
					// throw new MoleculerError(null, CodeConstant.TRANSFER_SDK[resultTransfer.state], null, resultTransfer.data || {});
					response.code = CodeConstant.TRANSFER_FAILED;
					response.message = resultTransfer.message;
					return response;
				}
				break;
			}
			case 'bankCard':
			case 'bankAccount': {
				throw new MoleculerError(null, CodeConstant.TRANSFER_SDK.TRANSPORT_METHOD_NOT_SUPPORT);

				// resultTransfer = this.transferToBank();
				// break;
			}

			default:
				throw new MoleculerError(null, CodeConstant.TRANSFER_SDK.TRANSPORT_METHOD_NOT_SUPPORT);
		}

		response.code = CodeConstant.TRANSFER_SDK.SUCCEEDED;
		response.message = this.__(_.toString(response.code));
		response.data = _.get(resultTransfer, 'data', {});
		return response;
	} catch (error) {
		this.logger.info(`[TRANSFER_SDK]:::[ERROR]: ${String(error)}`);
		if (error.name !== 'MoleculerError') {
			console.error('==> [TRANSFER_SDK_EXCEPTION] :::', error);
		}
		response.code = error.code || CodeConstant.TRANSFER_SDK.FAILED;
		response.message = this.__(_.toString(response.code), { ...error.data }) || 'Có lỗi xảy ra. Vui lòng thử lại sau.';
	}
	return response;
};
