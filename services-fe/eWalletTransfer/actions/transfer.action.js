const { MoleculerError } = require('moleculer').Errors;
const _ = require('lodash');
const Decimal = require('decimal.js');
const moment = require('moment');

// Constants
const ErrorCodeConstant = require('../constants/errorCode.constant');
const AccountConstant = require('../constants/account.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	try {
		const authInfo = this.GetAuthInfo(ctx.meta);
		this.logger.info(`[eWalletTransfer] authInfo: ${JSON.stringify(authInfo)}`);
		const payload = _.get(ctx, 'params.body', {});
		this.logger.info(`[eWalletTransfer] payload: ${JSON.stringify(payload)}`);

		const eWalletAppInfo = await this.broker.call('v1.ewalletAppInfoModel.findOne', [{
			merchantId: authInfo.merchantId
		}]);

		if (_.get(eWalletAppInfo, 'id', null) === null) {
			// response.message = 'Không tìm thấy thông tin kết nối';
			return {
				code: ErrorCodeConstant.ACCOUNT_CHECK.APPINFO_NOT_FOUND,
				message: this.__(ErrorCodeConstant.ACCOUNT_CHECK.APPINFO_NOT_FOUND.toString())
			};
		}

		const checkPhone = this.PhoneUtility(payload.phone);
		// console.log('checkPhone', checkPhone);
		if (!checkPhone.isValid) {
			// response.message = 'Số điện thoại không hợp lệ';
			return {
				code: ErrorCodeConstant.ACCOUNT_CHECK.PHONE_NOT_AVAILABLE,
				message: this.__(ErrorCodeConstant.ACCOUNT_CHECK.PHONE_NOT_AVAILABLE.toString())
			};
		}
		const { phoneFormatted } = checkPhone;

		const connectedUserInfo = await this.broker.call('v1.eWalletConnectedUserModel.findOne', [{
			phone: phoneFormatted
			// appId: eWalletAppInfo.id
		}]);

		if (_.get(connectedUserInfo, 'id', null) === null) {
			// response.message = 'Số điện thoại này chưa liên kết với Ví PayME';
			return {
				code: ErrorCodeConstant.ACCOUNT_CHECK.USER_NOT_CONNECTED,
				message: this.__(ErrorCodeConstant.ACCOUNT_CHECK.USER_NOT_CONNECTED.toString())
			};
		}

		const accountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [{
			phone: phoneFormatted
		}]);

		this.logger.info(`[eWalletTransfer] AccountInfo: ${JSON.stringify(accountInfo)}`);

		if (_.get(accountInfo, 'kyc.state', null) !== AccountConstant.ACCOUNT_KYC_STATE.APPROVED) {
			// message: 'Tài khoản chưa định danh. Vui lòng thực hiện định danh.'
			return {
				code: ErrorCodeConstant.ACCOUNT_CHECK.ACCOUNT_NOT_KYC,
				message: this.__(ErrorCodeConstant.ACCOUNT_CHECK.ACCOUNT_NOT_KYC.toString())
			};
		}
		if (accountInfo.accountType === AccountConstant.ACCOUNT_TYPE.BUSINESS && !_.includes(accountInfo.scope, 'account.bussiness.transferMoney')) {
			// message: 'Tài khoản không được cấp quyền rút.'
			return {
				code: ErrorCodeConstant.ACCOUNT_CHECK.TRANSFER_NOT_ALLOW,
				message: this.__(ErrorCodeConstant.ACCOUNT_CHECK.TRANSFER_NOT_ALLOW.toString())
			};
		}

		let transportInput = _.get(payload, 'transport', {});
		if (_.keys(transportInput).length > 1) {
			return {
				code: ErrorCodeConstant.ACCOUNT_CHECK.TRANSFER_METHOD_NOT_SUPPORTED,
				message: this.__(ErrorCodeConstant.ACCOUNT_CHECK.TRANSFER_METHOD_NOT_SUPPORTED.toString())
			};
		}
		transportInput = _.pick(transportInput, GeneralConstant.ALLOWED_TRANSPORT_METHOD);
		if (_.isEmpty(transportInput)) {
			// message: 'Phương thức chuyển tiền không được hỗ trợ.'
			return {
				code: ErrorCodeConstant.ACCOUNT_CHECK.TRANSFER_METHOD_NOT_SUPPORTED,
				message: this.__(ErrorCodeConstant.ACCOUNT_CHECK.TRANSFER_METHOD_NOT_SUPPORTED.toString())
			};
		}

		let walletInfo = null;
		try {
			walletInfo = await this.WalletLibs.Information(accountInfo.id);
		} catch (error) {
			this.logger.info(`[eWalletTransfer] => [SENDER_WALLET_INFORMATION_ERROR]: ${error}`);
			// response.message = 'Rút tiền thất bại. Vui lòng thử lại (E02)';
			return {
				code: ErrorCodeConstant.INTERNAL_SERVER_ERROR,
				message: this.__(ErrorCodeConstant.INTERNAL_SERVER_ERROR.toString())
			};
		}

		let settingKey;
		const createdTransferMoneyObj = {
			accountId: accountInfo.id,
			appId: eWalletAppInfo.id,
			description: payload.description,
			amount: payload.amount,
			note: payload.description,
			senderInfo: {
				accountId: accountInfo.id,
				fullname: accountInfo.fullname
			},
			receiverInfo: {},
			state: GeneralConstant.TRANSFER_STATE.PENDING
		};
		// console.log('_.keys(transportInput)[0]', _.keys(transportInput)[0]);
		if (_.keys(transportInput)[0] === 'wallet') {
			settingKey = 'PAYME';
			const checkReceiverPhone = await this.PhoneUtility(transportInput.wallet.phone);
			// console.log('checkReceiverPhone', checkReceiverPhone);
			if (!checkReceiverPhone.isValid) {
				// response.message = 'Số điện thoại không hợp lệ';
				return {
					code: ErrorCodeConstant.RECEIVER_NOT_FOUND,
					message: this.__(ErrorCodeConstant.RECEIVER_NOT_FOUND.toString())
				};
			}
			const receiverPhoneFormatted = checkReceiverPhone.phoneFormatted;
			if (receiverPhoneFormatted === phoneFormatted) {
				return {
					code: ErrorCodeConstant.ACCOUNT_CHECK.SAME_SENDER_RECEIVER_PHONE,
					message: this.__(ErrorCodeConstant.ACCOUNT_CHECK.SAME_SENDER_RECEIVER_PHONE.toString())
				};
			}
			// console.log('receiverPhoneFormatted', receiverPhoneFormatted);
			const receiverAccountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [{ phone: receiverPhoneFormatted }]);
			// console.log('receiverAccountInfo', receiverAccountInfo);

			if (_.get(receiverAccountInfo, 'id', null) === null) {
				// response.message = 'Số điện thoại này chưa liên kết với Ví PayME';
				return {
					code: ErrorCodeConstant.RECEIVER_NOT_FOUND,
					message: this.__(ErrorCodeConstant.RECEIVER_NOT_FOUND.toString())
				};
			}
			createdTransferMoneyObj.receiverInfo.accountId = receiverAccountInfo.id;
			createdTransferMoneyObj.receiverInfo.fullname = receiverAccountInfo.fullname;
			createdTransferMoneyObj.supplier = GeneralConstant.SUPPLIER.WALLET;
		} else {
			settingKey = 'BANK';
			// console.log('transportInput', transportInput);
			createdTransferMoneyObj.receiverInfo.cardNumber = _.get(transportInput, 'bankCard.cardNumber', null);
			// console.log('createdTransferMoneyObj.receiverInfo.cardNumber', createdTransferMoneyObj.receiverInfo.cardNumber);

			createdTransferMoneyObj.receiverInfo.accountNumber = _.get(transportInput, 'bankAccount.accountNumber', null);
			createdTransferMoneyObj.receiverInfo.swiftCode = _.get(transportInput, 'bankCard.swiftCode', null) || _.get(transportInput, 'bankAccount.swiftCode', null);
			createdTransferMoneyObj.receiverInfo.fullname = _.get(transportInput, 'bankCard.cardHolder', null) || _.get(transportInput, 'bankAccount.accountHolder', null);
			createdTransferMoneyObj.supplier = GeneralConstant.SUPPLIER.NAPAS_GATEWAY;
		}
		let transferMoneyConfig = await this.broker.call('v1.eWalletSettingModel.findOne', [{
			key: 'fee.transferMoney.config'
		}]);

		// console.log('transferMoneyConfig', transferMoneyConfig);

		transferMoneyConfig = transferMoneyConfig.value;
		const calFeeResult = await this.CalcFee({
			accountId: accountInfo.id,
			amount: payload.amount,
			feeConfig: transferMoneyConfig,
			settingKey: GeneralConstant.FEE_SETTING_KEY[settingKey],
			chargeFeeKey: `TransferMoneyChargeFee_${accountInfo.id}`,
			filter: {
				accountId: accountInfo.id,
				state: GeneralConstant.TRANSFER_STATE.SUCCEEDED
			}
		});
		// console.log('settingKey', settingKey);
		const { fee } = calFeeResult;
		const totalPay = _.toNumber((new Decimal(payload.amount)).add(new Decimal(fee)));

		// console.log('calFeeResult', calFeeResult);
		if (_.isNumber(_.get(calFeeResult, 'fee')) === false) {
			// response.message = 'Rút tiền thất bại. Vui lòng thử lại (E01)';
			return {
				code: ErrorCodeConstant.INTERNAL_SERVER_ERROR,
				message: this.__(ErrorCodeConstant.INTERNAL_SERVER_ERROR.toString())
			};
		}
		// console.log('walletInfo', walletInfo);
		if (_.get(walletInfo, 'balance', 0) < totalPay) {
			this.logger.info('[eWalletTransfer] => [SENDER_WALLET_INFORMATION_ERROR]: Số dư của bạn không đủ để thực hiện giao dịch');
			// response.message = 'Số dư của bạn không đủ để thực hiện giao dịch';
			return {
				code: ErrorCodeConstant.BALANCE_NOT_ENOUGH,
				message: this.__(ErrorCodeConstant.BALANCE_NOT_ENOUGH.toString())
			};
		}
		createdTransferMoneyObj.fee = fee;
		createdTransferMoneyObj.total = totalPay;

		let transaction = null;
		try {
			transaction = await this.broker.call('v1.walletUuid.pick', { prefix: 'WITHDRAW' });
		} catch (error) {
			this.logger.info('[eWalletTransfer] => [SENDER_WALLET_INFORMATION_ERROR]: Rút tiền thất bại. Vui lòng thử lại (E03)');
			return {
				code: ErrorCodeConstant.TRANSFER_FAILED,
				message: this.__(ErrorCodeConstant.TRANSFER_FAILED.toString())
			};
		}

		if (_.isNull(transaction)) {
			this.logger.info('[eWalletTransfer] => [SENDER_WALLET_INFORMATION_ERROR]: Rút tiền thất bại. Vui lòng thử lại (E04)');
			return {
				code: ErrorCodeConstant.TRANSFER_FAILED,
				message: this.__(ErrorCodeConstant.TRANSFER_FAILED.toString())
			};
		}

		createdTransferMoneyObj.transaction = transaction;

		let createdTransferMoney = await this.broker.call('v1.eWalletTransferMoneyModel.create', [createdTransferMoneyObj]);

		// console.log('createdTransferMoney', createdTransferMoney);
		const senderHistoryObj = {
			accountId: accountInfo.id,
			appId: eWalletAppInfo.id,
			service: {
				type: 'TRANSFER',
				code: 'TRANSFER_MONEY',
				method: 'PAYME',
				name: 'Chuyển tiền',
				id: createdTransferMoney.id,
				transaction: createdTransferMoney.transaction,
				state: createdTransferMoney.state,
				data: {
					resolveType: GeneralConstant.RESOLVE_TYPE.TRANSFER_MONEY,
					transferId: createdTransferMoney.id,
					transaction: createdTransferMoney.transaction,
					accountId: createdTransferMoney.accountId,
					state: createdTransferMoney.state
				}
			},
			amount: createdTransferMoney.amount,
			fee: createdTransferMoney.fee,
			total: createdTransferMoney.total,
			discount: 0,
			bonus: 0,
			state: GeneralConstant.TRANSFER_STATE.PENDING,
			description: `Chuyển tiền đến ${createdTransferMoney.receiverInfo.fullname}`,
			changed: '-',
			tags: [
				'TRANSFER_MONEY',
				'SENDER'
			]
		};

		let senderHistory = null;
		try {
			senderHistory = await this.broker.call('v1.eWalletHistoryModel.create', [senderHistoryObj]);
		} catch (error) {
			this.logger.info(`[WITHDRAW_EWALLET] => [CREATE_HISTORY_WITHDRAW_ERROR]: ${error}`);
			// response.message = 'Rút tiền thất bại. Vui lòng thử lại (E08)';
			return {
				code: ErrorCodeConstant.TRANSFER_FAILED,
				message: this.__(ErrorCodeConstant.TRANSFER_FAILED.toString())
			};
		}

		// console.log('senderHistory', senderHistory);
		let valueTransferThresholdConfig = await this.broker.call('v1.eWalletSettingModel.findOne', [{
			key: 'transfer.require.verify'
		}]);
		valueTransferThresholdConfig = _.get(valueTransferThresholdConfig, 'value', null);

		try {
			valueTransferThresholdConfig = JSON.parse(valueTransferThresholdConfig);
			const amountVerify = _.get(valueTransferThresholdConfig, 'threshold', 0);

			// console.log('valueTransferThresholdConfig', valueTransferThresholdConfig);
			if (amountVerify < totalPay) {
				const code = _.random(100000, 999999);
				const activeCodeObj = {
					phone: phoneFormatted,
					code,
					source: GeneralConstant.TRANSFER_SOURCE,
					expiredAt: moment(new Date()).add(5, 'minutes')
				};
				const createActiveCode = await this.broker.call('v1.ewalletActiveCodeModel.create', [activeCodeObj]);
				// console.log('createActiveCode', createActiveCode);
				try {
					const smsContent = 'Ma xac thuc PayME cua ban khi chuyen tien la ';
					const paramsSendSMS = {
						phone: phoneFormatted,
						code,
						content: smsContent,
						isFullContent: false
					};
					await this.broker.call('v1.utility.sendSMS', paramsSendSMS);
				} catch (error) {
					this.logger.info(`SEND_SMS_TRANSFER :>>>> ${JSON.stringify(error)}`);
					// response.message = 'Có lỗi xảy ra khi rút tiền. Vui lòng thử lại sau.';
					return {
						code: ErrorCodeConstant.INTERNAL_SERVER_ERROR,
						message: this.__(ErrorCodeConstant.INTERNAL_SERVER_ERROR.toString())
					};
				}

				// response.code = ErrorCodeConstant.REQUIRE_OTP;
				// response.message = 'Rút tiền cần xác thực thông qua OTP';
				return {
					code: ErrorCodeConstant.REQUIRE_OTP,
					message: this.__(ErrorCodeConstant.REQUIRE_OTP.toString()),
					data: {
						transaction
					}
				};
			}
		} catch (e) {
			this.logger.info(`[eWalletTransfer] valueTransferThresholdConfig Error :>>>> ${JSON.stringify(e)}`);
			// response.message = 'Rút tiền thất bại. Vui lòng thử lại (E09)';
			return {
				code: ErrorCodeConstant.INTERNAL_SERVER_ERROR,
				message: this.__(ErrorCodeConstant.INTERNAL_SERVER_ERROR.toString())
			};
		}

		const paymentParams = {
			accountInfo,
			appId: eWalletAppInfo.id,
			transaction,
			amount: totalPay,
			walletBalance: walletInfo.balance
		};
		// console.log('paymentParams', paymentParams);
		const paymentResponsed = await this.PayWallet(paymentParams);

		this.logger.info(`[eWalletTransfer] payment Response: ${JSON.stringify(paymentResponsed)}`);
		if (paymentResponsed.state !== GeneralConstant.PAYMENT_STATE.SUCCEEDED) {
			createdTransferMoney = await this.broker.call('v1.eWalletWithdrawModel.findOneAndUpdate', [
				{ id: createdTransferMoney.id },
				{
					state: GeneralConstant.TRANSFER_STATE.FAILED,
					payment: paymentResponsed.payment
				}
			]);

			senderHistory = await this.broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [
				{ id: senderHistory.id },
				{
					state: GeneralConstant.TRANSFER_STATE.FAILED,
					'service.state': GeneralConstant.TRANSFER_STATE.FAILED,
					'service.data.state': GeneralConstant.TRANSFER_STATE.FAILED,
					payment: paymentResponsed.payment
				}
			]);

			// response.message = _.get(PayWallet, 'message', 'Rút tiền thất bại. Vui lòng thử lại (E10)');
			return {
				code: ErrorCodeConstant.TRANSFER_FAILED,
				message: this.__(ErrorCodeConstant.TRANSFER_FAILED.toString())
			};
		}

		let transferResponse;
		let paramsSend;
		switch (_.keys(transportInput)[0]) {
			case 'wallet':
				// console.log('transaction', transaction);
				transferResponse = await this.TransferPayme(transaction);
				break;
			case 'bankCard':
				paramsSend = {
					transaction: _.get(createdTransferMoney, 'transaction', null), // Transaction khi nguoi dung thuc hien giao dich
					amount: payload.amount,
					bankInfo: {
						method: 'CARD',
						cardNumber: createdTransferMoney.receiverInfo.cardNumber,
						swiftCode: createdTransferMoney.receiverInfo.swiftCode,
						fullname: createdTransferMoney.receiverInfo.fullname
					}
				};
				// console.log('paramsSend', paramsSend);
				transferResponse = await this.broker.call('v1.eWalletWithdraw.bankBalancerTransfer', paramsSend);
				break;
			case 'bankAccount':
				paramsSend = {
					transaction: _.get(createdTransferMoney, 'transaction', null), // Transaction khi nguoi dung thuc hien giao dich
					amount: payload.amount,
					bankInfo: {
						method: 'ACCOUNT',
						accountNumber: createdTransferMoney.receiverInfo.accountNumber,
						swiftCode: createdTransferMoney.receiverInfo.swiftCode,
						fullname: createdTransferMoney.receiverInfo.fullname
					}
				};
				// console.log('paramsSend', paramsSend);
				transferResponse = await this.broker.call('v1.eWalletWithdraw.bankBalancerTransfer', {
					paramsSend
				});
				break;
			default:
				break;
		}

		// console.log('transferResponse', transferResponse);

		if (_.get(transferResponse, 'state', null) !== GeneralConstant.TRANSFER_STATE.SUCCEEDED) {
			const refundObj = {
				paymentId: paymentResponsed.payment.id, // Transaction khi nguoi dung thuc hien giao dich
				appId: eWalletAppInfo.id,
				amount: totalPay,
				description: 'Hoàn tiền khi chuyển ví thất bại',
				serviceCode: 'TRANSFER_MONEY',
				serviceId: createdTransferMoney.id
			};
			const refundResponsed = await this.broker.call('v1.eWalletWithdraw.refund', refundObj);
			createdTransferMoney = await this.broker.call('v1.eWalletTransferMoneyModel.findOneAndUpdate', [{
				id: createdTransferMoney.id
			}, {
				state: GeneralConstant.TRANSFER_STATE.FAILED
			}]);

			senderHistory = await this.broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [
				{ id: senderHistory.id },
				{
					state: GeneralConstant.TRANSFER_STATE.FAILED,
					'service.state': GeneralConstant.TRANSFER_STATE.FAILED,
					'service.data.state': GeneralConstant.TRANSFER_STATE.FAILED,
					changed: '-',
					balance: paymentResponsed.balance,
					payment: paymentResponsed.payment,
					publishedAt: new Date().toISOString()
				},
				{ new: true }
			]);
			this.logger.info(`[eWalletTransfer] Refund: ${JSON.stringify(refundResponsed)}`);
			return {
				code: ErrorCodeConstant.TRANSFER_FAILED,
				message: this.__(ErrorCodeConstant.TRANSFER_FAILED.toString())
			};
		}
		createdTransferMoney = await this.broker.call('v1.eWalletTransferMoneyModel.findOneAndUpdate', [{
			id: createdTransferMoney.id
		}, {
			state: GeneralConstant.TRANSFER_STATE.SUCCEEDED,
			payment: paymentResponsed.payment
		}]);

		senderHistory = await this.broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [
			{ id: senderHistory.id },
			{
				state: GeneralConstant.TRANSFER_STATE.SUCCEEDED,
				'service.state': GeneralConstant.TRANSFER_STATE.SUCCEEDED,
				'service.data.state': GeneralConstant.TRANSFER_STATE.SUCCEEDED,
				changed: '-',
				balance: paymentResponsed.balance,
				payment: paymentResponsed.payment,
				publishedAt: new Date().toISOString()
			},
			{ new: true }
		]);

		return {
			code: ErrorCodeConstant.TRANSFER_SUCCEEDED,
			message: this.__(ErrorCodeConstant.TRANSFER_SUCCEEDED.toString()),
			data: {
				transaction
			}
		};
	} catch (error) {
		this.logger.info(`[eWalletTransfer] Transfer Error: ${error}`);
		return {
			code: ErrorCodeConstant.INTERNAL_SERVER_ERROR,
			message: this.__(ErrorCodeConstant.INTERNAL_SERVER_ERROR.toString())
		};
	}
};
