module.exports = {
	AccountModel: {
		findOne: 'v1.eWalletAccountModel.findOne',
		findMany: 'v1.eWalletAccountModel.findMany',
		findOneAndUpdate: 'v1.eWalletAccountModel.findOneAndUpdate',
		create: 'v1.eWalletAccountModel.create',
		updateOne: 'v1.eWalletAccountModel.updateOne',
		delete: 'v1.eWalletAccountModel.delete',
		deleteMany: 'v1.eWalletAccountModel.deleteMany'
	},
	LinkedModel: {
		findOne: 'v1.eWalletLinkedModel.findOne',
		findMany: 'v1.eWalletLinkedModel.findMany',
		findOneAndUpdate: 'v1.eWalletLinkedModel.findOneAndUpdate',
		create: 'v1.eWalletLinkedModel.create',
		updateOne: 'v1.eWalletLinkedModel.updateOne',
		delete: 'v1.eWalletLinkedModel.delete',
		deleteMany: 'v1.eWalletLinkedModel.deleteMany'
	},
	TransportModel: {
		findOne: 'v1.eWalletTransportModel.findOne',
		findMany: 'v1.eWalletTransportModel.findMany',
		findOneAndUpdate: 'v1.eWalletTransportModel.findOneAndUpdate',
		create: 'v1.eWalletTransportModel.create',
		updateOne: 'v1.eWalletTransportModel.updateOne',
		delete: 'v1.eWalletTransportModel.delete',
		deleteMany: 'v1.eWalletTransportModel.deleteMany'
	},
	TransportWalletModel: {
		findOne: 'v1.eWalletTransportWalletModel.findOne',
		findMany: 'v1.eWalletTransportWalletModel.findMany',
		findOneAndUpdate: 'v1.eWalletTransportWalletModel.findOneAndUpdate',
		create: 'v1.eWalletTransportWalletModel.create',
		updateOne: 'v1.eWalletTransportWalletModel.updateOne',
		delete: 'v1.eWalletTransportWalletModel.delete',
		deleteMany: 'v1.eWalletTransportWalletModel.deleteMany'
	},
	TransferMoneyModel: {
		findOne: 'v1.eWalletTransferMoneyModel.findOne',
		findMany: 'v1.eWalletTransferMoneyModel.findMany',
		findOneAndUpdate: 'v1.eWalletTransferMoneyModel.findOneAndUpdate',
		create: 'v1.eWalletTransferMoneyModel.create',
		updateOne: 'v1.eWalletTransferMoneyModel.updateOne',
		delete: 'v1.eWalletTransferMoneyModel.delete',
		deleteMany: 'v1.eWalletTransferMoneyModel.deleteMany'
	},
	History: {
		findOne: 'v1.eWalletHistoryModel.findOne',
		findMany: 'v1.eWalletHistoryModel.findMany',
		findOneAndUpdate: 'v1.eWalletHistoryModel.findOneAndUpdate',
		create: 'v1.eWalletHistory.create',
		updateOne: 'v1.eWalletHistoryModel.updateOne',
		delete: 'v1.eWalletHistoryModel.delete',
		deleteMany: 'v1.eWalletHistoryModel.deleteMany'
	},
	HistoryService: {
		create: 'v1.eWalletHistory.create',
		update: 'v1.eWalletHistory.update'
	},
	KYCModel: {
		findOne: 'v1.eWalletKycModel.findOne',
		findMany: 'v1.eWalletKycModel.findMany',
		findOneAndUpdate: 'v1.eWalletKycModel.findOneAndUpdate',
		create: 'v1.eWalletKycModel.create',
		updateOne: 'v1.eWalletKycModel.updateOne',
		delete: 'v1.eWalletKycModel.delete',
		deleteMany: 'v1.eWalletKycModel.deleteMany'
	}

};
