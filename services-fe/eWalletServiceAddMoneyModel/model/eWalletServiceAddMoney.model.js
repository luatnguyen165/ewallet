const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const generalConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
    required: true
	},
  phone: {
    type: String,
    default: null
  },
  appId: {
    type: Number,
    default: null
  },
  
  description: {
    type: String
  },
  reason: {
    type: String
  },
  submittedAccountId: {
    type: Number,
    required: true
  },
  amount: {
    type: Number,
    required: true
  },
  fee: {
    type: Number,
    default: 0
  },
  total: {
    type: Number,
    required: true
  },
  transaction: {
    type: String,
    required: true
  },
  sender: {
    type: String
  },
  bankTransaction: {
    type: String,
    default: null
  },
  updatedAccountId: {
    type: Number,
    default: null
  },
  state: {
    type: String,
    enum: _.values(generalConstant.DEPOSIT_SYSTEM_STATE),
    default: generalConstant.DEPOSIT_SYSTEM_STATE.PENDING
  },
  type: {
    type: String,
    enum: _.values(generalConstant.SERVICE_ADD_MONEY_TYPE)
  },
  extraData: Object,
  supplierResponsed: [String]
}, {
	collection: 'Service_AddMoney',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: true, unique: false });
Schema.index({ state: 1 });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
