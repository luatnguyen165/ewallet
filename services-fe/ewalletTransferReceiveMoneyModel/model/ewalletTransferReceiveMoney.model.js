const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const FrontendConstant = require('../constants/frontend.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	paccountId: {
		type: Number,
		default: null
	},
	transferSendId: {
		type: Number,
		default: null
	},
	amount: {
		type: Number,
		required: true
	},
	total: {
		type: Number,
		default: null
	},
	discount: {
		type: Number,
		default: 0
	},
	fee: {
		type: Number,
		default: 0
	},
	transaction: {
		type: String,
		required: true,
		unique: true
	},
	note: {
		type: String,
		default: null
	},
	description: {
		type: String,
		default: null
	},
	receiverInfo: { // thong tin ng nhận tiền
		accountId: {
			type: Number,
			default: 0
		},
		fullname: String,
		phone: {
			type: String,
			default: null
		}
	},
	senderInfo: { // thong tin ng gui tiền
		accountId: {
			type: Number,
			required: true
		},
		fullname: String,
		phone: {
			type: String,
			default: null
		}
	},
	state: {
		type: String,
		enum: _.values(FrontendConstant.TRANSFER_PAYME_STATE),
		default: null
	},
	submittedAccountId: {
		type: String,
		default: null
	},
	supplierResponsed: mongoose.Schema.Types.Mixed,
	extraData: Object
}, {
	collection: 'Service_TransferReceiveMoney',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ id: 1, transaction: 1 });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
