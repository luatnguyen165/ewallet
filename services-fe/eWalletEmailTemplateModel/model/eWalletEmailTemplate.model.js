const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	shortName: {
		type: String,
		unique: true,
		required: true
	},
	title: {
		type: String,
		default: null
	},
	content: {
		type: String,
		default: null
	},
	description: {
		type: String,
		default: null
	},
	type: {
		type: String,
		enum: _.values(GeneralConstant.EMAIL_TEMPLATE_TYPE)
	},
	projectName: {
		type: String,
		default: null
	}
}, {
	collection: 'EmailTemplate',
	versionKey: false,
	timestamps: true
});

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
