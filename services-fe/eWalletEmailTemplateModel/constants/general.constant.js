module.exports = {
	EMAIL_TEMPLATE_TYPE: {
		SMS: 'SMS',
		EMAIL: 'EMAIL',
		NOTIFY: 'NOTIFY'
	}
};
