const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;

module.exports = async function (ctx) {
	const payload = ctx.params;
	const { accountId } = payload;
	try {
		// console.log(payload)
		let alias = this.GetShortName(payload.fullname);

		// console.log('asjasjksa', alias);
		if (_.isNil(alias)) {
			throw new MoleculerError('Create Alias Error: Get ShortName failed', 500);
		}

		const randomNumber = Math.floor(Math.random() * 1000);
		alias += randomNumber.toString();

		const aProfileAlias = await this.broker.call('v1.eWalletProfileAliasModel.findOne', [
			{
				accountId
			}
		]);

		// console.log(`${accountId} == > ${JSON.stringify(aProfileAlias)}`);

		let isUpdate = true;
		if (!aProfileAlias) {
			isUpdate = false;
		}

		// for (let i = 0; i < 3; i++) {
		// thu lai 3 lan
		const aExisted = await this.broker.call('v1.eWalletProfileAliasModel.findMany', [
			{
				alias
			}
		]);

		if (aExisted && aExisted.length > 0) {
			const randomNumber2 = Math.floor(Math.random() * 1000);
			alias += randomNumber2.toString();
		}
		if (isUpdate) {
			if (aProfileAlias.updateTimes === 0) {
				const updateAlias = await this.broker.call('v1.eWalletProfileAliasModel.updateMany', [
					{
						accountId
					},
					{
						alias
					}
				]);
				if (updateAlias && updateAlias.nModified >= 1) {
					await this.broker.call('v1.eWalletPageModel.updateOne', [
						{
							accountId
						},
						{
							alias
						}
					]);
					return true;
				}
			} else {
				throw new MoleculerError(`Tạo alias skip ! - old alias ${aProfileAlias.alias} - ${accountId}`, 500);
			}
		} else {
			const docProfileAlias = await this.broker.call('v1.eWalletProfileAliasModel.create', [
				{
					accountId,
					alias
				}
			]);
			if (_.isObject(docProfileAlias) && docProfileAlias.id > 1) {
				await this.broker.call('v1.eWalletPageModel.updateOne', [
					{
						accountId
					},
					{
						alias
					}
				]);

				return true;
			}
		}
	} catch (e) {
		console.log(e);
		this.logger.info('[ALIAS] -> [CREATE] -> [EXCEPTION] :: ', e);
		throw new MoleculerError(`Tạo alias lỗi ! - ${payload.fullname} - ${accountId}`, 500);
	}
};
