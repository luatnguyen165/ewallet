const moleculerI18n = require('moleculer-i18n-js');
const path = require('path');

module.exports = {
	name: 'profileAlias',

	version: 1,

	mixins: [moleculerI18n],

	i18n: {
		directory: path.join(__dirname, 'locales'),
		locales: ['vi', 'en'],
		defaultLocale: 'vi'
	},

	/**
	 * Settings
	 */
	settings: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		createByName: {
			params: {
				accountId: 'number',
				fullname: 'string'
			},
			handler: require('./actions/createByName.action')
		}
	},

	async started() {
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		RemoveUnicode: require('./methods/removeUnicode.method'),
		GetShortName: require('./methods/getShortName.method')
	}
};
