module.exports = function (fullname) {

	try{
		const fullnameNoUnicode = this.RemoveUnicode(fullname);
		const data = fullnameNoUnicode.toLowerCase().split(' ');
		// console.log(JSON.stringify(data));
		let aliasName = data[data.length - 1];
		for (let i = 0; i < data.length - 1; i++) {
			aliasName += data[i][0];
		}
		return aliasName;
	} catch (err) {
		
		console.log(`Profile Alias Error : ${err}`);
	}
	return null;
};
