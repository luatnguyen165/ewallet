const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	name: {
		type: String,
		default: null
	},
	shortName: {
		type: String,
		default: null
	},
	showName: { // tên hiển thị
		type: String,
		default: null
	},
	service: {
		type: String,
		default: null
	},
	logo: {
		type: String,
		default: null
	},
	isActive: {
		type: Boolean,
		default: true
	},
	description: {
		type: String,
		default: null
	},
	configs: { // is JSON
		type: Object,
		default: null
	}
}, {
	collection: 'Issuer',
	versionKey: false,
	timestamps: true
});

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
