const _ = require('lodash');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	name: String,
	description: {
		type: String,
		default: null
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.PACKAGE_STATE)
	},
	amountLimit: {
		min: {
			type: Number,
			default: 0
		},
		max: {
			type: Number,
			default: 0
		},
		fixed: {
			type: Number,
			default: null
		}
	},
	supplierInfo: {
		// ref tới bảng supplier
		id: {
			type: Number,
			required: true
		},
		name: {
			type: String
		},
		logo: {
			type: String
		},
		title: {
			type: String
		}
	},
	// Thời gian cấp tín dụng -  tính theo số ngày
	approvedAt: Date,
	term: {
		min: Number,
		max: Number,
		title: String
	},
	interestTitle: String,
	interestRate: [{
		term: Number,
		rate: Number,
		title: String,
		type: {
			type: String,
			enum: _.values(GeneralConstant.PACKAGE_TERM_TYPE)
		}
	}],
	disbursement: String,
	promotionUrl: String,
	conditionUrl: String,
	appType: {
		type: String,
		enum: _.values(GeneralConstant.APPTYPE)
	},
	redirectUrl: {
		type: String,
		default: null
	},
	// Nhóm đối tượng sử dụng gói
	// group: {
	//   type: {
	//     type: String,
	//     enum: _.values(FastLoanConstant.PACKAGE_GROUP)
	//   },
	//   values: Array
	// },
	// // Độ tuổi
	// age: {
	//   from: {
	//     type: Number
	//   },
	//   to: {
	//     type: Number
	//   }
	// },
	// Thông tin giấy tờ sao kê
	statementInfo: {
		type: Array
	}
	// Nhóm gói vay
	// groupLevel: {
	//   type: String,
	//   enum: _.values(FastLoanConstant.PACKAGE_LEVEL)
	// },
	// Đường dẫn hợp đồng/điều khoản
}, {
	collection: 'Service_FastLoanPackageV2',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
