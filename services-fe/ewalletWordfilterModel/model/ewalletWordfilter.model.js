const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const { WORD_FILTER_TYPE } = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	title: {
		type: String
	},
	description: {
		type: String
	},
	isActive: {
		type: Boolean
	},
	file: {
		type: String
	},
	fileName: {
		type: String
	},
	type: {
		type: String,
		enum: Object.values(WORD_FILTER_TYPE)
	}
}, {
	collection: 'Wordfilter',
	versionKey: false,
	timestamps: true
});

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
