const _ = require('lodash');

const BankServiceConstant = require('../constants/bankService.constant');

const TelegramGroup = process.env.NOTIFY_TELEGRAM;

module.exports = async function () {
	const FastTransfer = async (payload) => {
		const response = {
			state: BankServiceConstant.STATE.FAILED,
			message: null
		};

		try {
			// validate data
			let description = _.get(payload, 'description', '') || '';
			if (description === '') {
				description = 'Chuyen tien tu tai khoan PayME toi OCB';
			}
			const ocbParams = {
				sourceAccount: payload.sourceAccountNumber,
				type: payload.method,
				amount: payload.amount,
				description,
				accountNumber: _.get(payload, 'accountNumber', '') || '',
				cardNumber: _.get(payload, 'cardNumber', '') || '',
				bankCode: payload.bankCode
			};
			this.logger.info(`[ewalletTransfer] OCB Supplier >> Fast Transfer >> ocbParams: ${JSON.stringify(ocbParams)}`);
			const ocbResponsed = await this.OCBBankLibs.Transfer247(ocbParams);
			this.logger.info(`[ewalletTransfer] OCB Supplier >> Fast Transfer >> ocbResponsed: ${JSON.stringify(ocbResponsed)}`);
			const supplierResponsed = JSON.stringify(ocbResponsed.original);

			// another error
			try {
				const supplierTransaction = _.get(ocbResponsed, 'original.data.transaction.bankTransactionId', null);
				if (ocbResponsed.code !== 1) {
					if (ocbResponsed.code === -2) {
						const alertMessage = `OCB - #${payload.serviceTransaction} Tạo lệnh chuyển tiền cho ${payload.cardNumber || payload.accountNumber} -  ${supplierTransaction} PENDING chờ xử lý !!`;
						await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
					}
					// code return is -99
					if (ocbResponsed.code !== -2) {
						const alertMessage = `OCB - #${payload.serviceTransaction} Tạo lệnh chuyển tiền cho ${payload.cardNumber || payload.accountNumber} -  ${supplierTransaction} thất bại`;
						await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
						response.message = _.get(ocbResponsed, 'data.message', 'Thanh toán thất bại');
						response.supplierResponsed = supplierResponsed;
						return response;
					}
				} else {
					const t = Math.round(payload.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
					const alertMessage = `OCB - #${payload.serviceTransaction} Tạo lệnh chuyển tiền cho ${payload.cardNumber || payload.accountNumber} -  ${supplierTransaction} thành công`;
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
				}
			} catch (exception) {
				this.logger.info(exception);
			}

			response.clientTransId = _.get(ocbResponsed, 'original.trace.clientTransId', null);
			response.supplierTransaction = _.get(ocbResponsed, 'original.trace.bankRefNo', null);
			response.supplierResponsed = supplierResponsed;
			response.fee = _.get(ocbResponsed, 'data.transaction.transactionFee', 0) || 0;
			response.taxAmount = _.get(ocbResponsed, 'data.transaction.taxAmount', 0) || 0;
			response.total = _.get(ocbResponsed, 'data.transaction.totalDebitedAmount', payload.amount) || payload.amount;
			response.bankTransactionId = _.get(ocbResponsed, 'data.transaction.bankTransactionId', null);
			response.state = BankServiceConstant.STATE.TRANSFERED;
			response.message = 'Thanh toán thành công';
			return response;
		} catch (error) {
			this.logger.info(`[OCBSupplierMethod] FastTransfer Error ${error}`);
			response.state = BankServiceConstant.STATE.FAILED;
			response.message = 'Thanh toán thất bại';
			return response;
		}
	};

	const DomesticTransfer = async (payload) => {
		const response = {
			state: BankServiceConstant.STATE.FAILED,
			message: null
		};

		try {
			// validate data
			let description = _.get(payload, 'description', '') || '';
			if (description === '') {
				description = 'Chuyen tien tu tai khoan PayME toi OCB';
			}
			const ocbParams = {
				sourceAccountNumber: payload.sourceAccountNumber,
				amount: payload.amount,
				description,
				accountNumber: payload.accountNumber
			};
			this.logger.info(`[ewalletTransfer] OCB Supplier >> DomesticTransfer >> ocbParams: ${JSON.stringify(ocbParams)}`);
			const ocbResponsed = await this.OCBBankLibs.TransferToOCB(ocbParams);
			this.logger.info(`[ewalletTransfer] OCB Supplier >> DomesticTransfer >> ocbResponsed: ${JSON.stringify(ocbResponsed)}`);

			const supplierResponsed = JSON.stringify(ocbResponsed.original);
			// another error
			if (ocbResponsed.code !== 1) {
				if (ocbResponsed.code === -2) {
					const t = Math.round(payload.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
					const alertMsg = `❗Chuyển tiền từ Ngân hàng -> Ngân hàng bằng OCBBank bị TIMEOUT
			  \nParam: ${JSON.stringify(payload)}
			  \nAmount: ${t}
			  \nOCB Response: ${JSON.stringify(ocbResponsed.original)}`;
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
				}
				if (ocbResponsed.code !== -2) {
					const t = Math.round(payload.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
					const alertMsg = `❗Chuyển tiền từ Ngân hàng -> Ngân hàng bằng OCBBank thất bại
			  \nParam: ${JSON.stringify(payload)}
			  \nAmount: ${t}
			  \nOCB Response: ${JSON.stringify(ocbResponsed.original)}`;
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });

					response.message = _.get(ocbResponsed, 'data.message', 'Thanh toán thất bại');
					response.supplierResponsed = supplierResponsed;
					return response;
				}
			} else {
				const supplierTransaction = _.get(ocbResponsed, 'original.data.transaction.bankTransactionId', null);
				const alertMsg = `OCB - #${payload.serviceTransaction} Tạo lệnh chuyển tiền inside ${payload.cardNumber || payload.accountNumber} -  ${supplierTransaction} thành công`;
				await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
			}

			response.clientTransId = _.get(ocbResponsed, 'original.trace.clientTransId', null);
			response.supplierTransaction = _.get(ocbResponsed, 'original.trace.bankRefNo', null);
			response.supplierResponsed = supplierResponsed;
			response.fee = _.get(ocbResponsed, 'data.transaction.transactionFee', 0) || 0;
			response.taxAmount = _.get(ocbResponsed, 'data.transaction.taxAmount', 0) || 0;
			response.total = _.get(ocbResponsed, 'data.transaction.totalDebitedAmount', payload.amount) || payload.amount;
			response.state = BankServiceConstant.STATE.TRANSFERED;
			response.message = 'Thanh toán thành công';
			return response;
		} catch (error) {
			this.logger.info(`[OCBSupplierMethod] DomesticTransfer Error ${error}`);
			response.state = BankServiceConstant.STATE.FAILED;
			response.message = 'Thanh toán thất bại';
			return response;
		}
	};

	return {
		DomesticTransfer,
		FastTransfer
	};
};
