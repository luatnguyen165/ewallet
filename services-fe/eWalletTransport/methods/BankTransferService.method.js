const _ = require('lodash');

const TransportConstant = require('../constants/transport.constant');
const BankServiceConstant = require('../constants/bankService.constant');

const TelegramGroup = process.env.NOTIFY_TELEGRAM;
const CONSTANTS = {
	KEY_CONFIG: 'cash.source.supplier',
	UUID_PREFIX: 'TRANSFER_BANK_TO_BANK',
	KEY_AMOUNT_OCB: 'min.amount.ocb.banktobank',
	KEY_AMOUNT_OTHER: 'min.amount.transfer.banktobank',
	RANDOM: 'bank.transfer.rate'
};

const Private = {
	// data is json object
	randomBankTransfer(data) {
		if (!_.isObject(data)) {
			return '';
		}
		const allKeys = _.keys(data);
		const randomValue = _.random(0, 999);
		let temp = 0;
		for (let i = 0; i < allKeys.length; i += 1) {
			temp += (data[allKeys[i]] * 10);
			if ((randomValue - temp) < 0) {
				return _.toUpper(allKeys[i]);
			}
		}
		return '';
	}
};
module.exports = async function BankTransferService() {
	const Transfer = async (payload) => {
		const response = {
			code: -1,
			state: BankServiceConstant.STATE.FAILED,
			message: null
		};

		try {
			const { bankInfo } = payload;

			const BankCodeConfig = await this.broker.call('v1.eWalletBankCodeModel.findMany', [{ isActive: true }]);
			const bankTransferInfo = _.find(BankCodeConfig, { swiftCode: bankInfo.swiftCode });
			if (_.isNil(bankTransferInfo)) {
				response.message = 'Ngân hàng không đươc hỗ trợ';
				return response;
			}
			const bankTransferCode = _.get(bankTransferInfo, 'card.atm.prefix', null);

			if (_.isNull(bankTransferCode)) {
				response.message = 'Không tìm thấy mã ngân hàng';
				return response;
			}

			let { supplier } = payload;
			if (_.isNil(supplier)) {
				let dataRandomSupplier = await this.broker.call('v1.eWalletSetting.getSettingConfig', { body: { keys: [CONSTANTS.RANDOM] } });

				if (_.get(dataRandomSupplier, 'code', null) === 202500 && !_.isEmpty(dataRandomSupplier.data) && dataRandomSupplier.data[0]) {
					dataRandomSupplier = dataRandomSupplier.data[0].value;
					const JsonData = JSON.parse(dataRandomSupplier);
					supplier = Private.randomBankTransfer(JsonData);
				} else {
					supplier = '';
				}
			}

			// supplier la OCB thi chan 20000, khong thi lay mac dinh tu trong setting (co the 10000)
			if (supplier === BankServiceConstant.SUPPLIER.OCB) {
				let AMOUNT_MIN = await this.broker.call('v1.eWalletSetting.getSettingConfig', { body: { keys: [CONSTANTS.RANDOM] } });

				if (_.get(AMOUNT_MIN, 'code', null) === 202500 && !_.isEmpty(AMOUNT_MIN.data) && AMOUNT_MIN.data[0]) {
					AMOUNT_MIN = _.toNumber(AMOUNT_MIN.data[0].value);
				} else AMOUNT_MIN = 20000;
				if (payload.amount < AMOUNT_MIN) {
					const t = Math.round(AMOUNT_MIN).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
					response.message = `Số tiền tối thiểu là ${t} đ`;
					return response;
				}
			} else {
				let AMOUNT_MIN = await this.broker.call('v1.eWalletSetting.getSettingConfig', { body: { keys: [CONSTANTS.KEY_AMOUNT_OTHER] } });

				if (_.get(AMOUNT_MIN, 'code', null) === 202500 && !_.isEmpty(AMOUNT_MIN.data) && AMOUNT_MIN.data[0]) {
					AMOUNT_MIN = _.toNumber(AMOUNT_MIN.data[0].value);
				} else AMOUNT_MIN = 10000;
				if (payload.amount < AMOUNT_MIN) {
					const t = Math.round(AMOUNT_MIN).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
					response.message = `Số tiền tối thiểu là ${t} đ`;
					return response;
				}
			}

			const { transaction } = payload;
			// const uuidService = new UuidService(CONSTANTS.UUID_PREFIX);
			// const uuidData = await uuidService.getUUID(1, payload);
			// if (uuidData.code === 1 || uuidData.data.length > 0) {
			//   transaction = uuidData.data.uuid[0];
			// }
			// if (_.isNull(transaction)) {
			//   const alertMsg = `❗Chuyển tiền từ Ngân hàng -> Ngân hàng thất bại vì không tạo được UUID
			//   \nParam: ${JSON.stringify(payload)}`;
			//   // NotifyService.SendAlertNotify(alertMsg, [], ['api']);

			//   response.message = 'Tạo lệnh chuyển tiền thất bại. Vui lòng thử lại';
			//   return response;
			// }

			let bankAccountSource = null;
			// Tim tai khoan ngan hang nguon tien
			if (supplier === BankServiceConstant.SUPPLIER.OCB) {
				bankAccountSource = await this.broker.call('v1.eWalletSourceBankAccountModel.findOne', [{
					sourceSupplier: supplier,
					isActive: true
				}]);
				if (_.get(bankAccountSource, 'id', null) === null) {
					response.message = 'Không tìm thấy nguồn tiền!';
					return response;
				}
			}
			const createTranferObj = {
				serviceTransaction: payload.transaction,
				transaction,
				method: payload.method,
				amount: payload.amount,
				state: BankServiceConstant.STATE.NEW,
				supplier,
				// Cập nhật các trạng thái nạp tiên
				stateAt: [{
					state: BankServiceConstant.STATE.NEW,
					createdAt: new Date().toISOString()
				}],
				from: {
					bankAccountId: _.get(bankAccountSource, 'id', null),
					swiftCode: _.get(bankAccountSource, 'swiftCode', null),
					bankInfo: bankAccountSource
				},
				to: bankInfo
			};

			const createTransfer = await this.broker.call('v1.eWalletTransferBankModel.create', [createTranferObj]);

			if (_.get(createTransfer, 'id', null) === null) {
				const alertMsg = `❗Chuyển tiền từ Ngân hàng -> Ngân hàng thất bại vì không tạo được Record
		  \nParam: ${JSON.stringify(payload)}`;
				// NotifyService.SendAlertNotify(alertMsg, [], ['api']);
				response.message = 'Không thể tạo giao dịch!';
				return response;
			}

			let result = {};
			switch (supplier) {
				// OCB
				case BankServiceConstant.SUPPLIER.OCB: {
					const OCBSupplier = await this.OCBSupplier();
					if (bankInfo.swiftCode === 'ORCOVNVX') {
						const paramsSend = {
							sourceAccountNumber: bankAccountSource.accountNumber,
							accountNumber: bankInfo.accountNumber,
							amount: payload.amount,
							description: payload.description,
							serviceTransaction: payload.transaction
						};
						result = await OCBSupplier.DomesticTransfer(paramsSend);
					} else {
						const paramsSend = {
							sourceAccountNumber: bankAccountSource.accountNumber,
							method: bankInfo.method,
							amount: payload.amount,
							cardNumber: bankInfo.cardNumber,
							accountNumber: bankInfo.accountNumber,
							description: payload.description,
							bankCode: bankTransferCode,
							serviceTransaction: payload.transaction
						};
						result = await OCBSupplier.FastTransfer(paramsSend);
					}
					break;
				}

				// // BIDV
				case BankServiceConstant.SUPPLIER.BIDV: {
					// Chuyen den ngan hang BIDV
					const BIDVSupplier = await this.BIDVSupplier();
					if (bankInfo.swiftCode === 'BIDVVNVX') {
						const paramsSend = {
							transaction,
							cardNumber: bankInfo.cardNumber,
							accountNumber: bankInfo.accountNumber,
							amount: payload.amount,
							description: payload.description,
							swiftCode: bankInfo.swiftCode
						};
						result = await BIDVSupplier.DomesticTransfer(paramsSend);
					} else {
						const paramsSend = {
							transaction,
							cardNumber: bankInfo.cardNumber,
							accountNumber: bankInfo.accountNumber,
							amount: payload.amount,
							description: payload.description,
							bankCode: bankTransferCode,
							swiftCode: bankInfo.swiftCode
						};
						result = await BIDVSupplier.OtherBankTransfer(paramsSend);
					}
					break;
				}
				// PG
				case BankServiceConstant.SUPPLIER.PG: {
					const PGSupplier = await this.PGSupplier();
					const paramsSend = {
						transaction,
						amount: payload.amount,
						description: payload.description,
						accountNumber: bankInfo.cardNumber || bankInfo.accountNumber,
						accountName: bankInfo.fullname,
						swiftCode: bankInfo.swiftCode
					};
					result = await PGSupplier.Transfer(paramsSend);
					break;
				}
				default: break;
			}

			const { stateAt } = createTransfer;
			stateAt.push({
				state: result.state,
				createdAt: new Date().toISOString()
			});
			// Cap nhat lai vao bang
			const updated = await this.broker.call('v1.eWalletTransferBankModel.findOneAndUpdate', [{
				id: createTransfer.id
			}, {
				stateAt,
				description: _.get(result, 'description', 'Chuyen tien tu ngan hang den ngan hang'),
				clientTransId: _.get(result, 'clientTransId', null),
				state: result.state,
				supplierTransaction: _.get(result, 'supplierTransaction', null),
				bankTransactionId: _.get(result, 'bankTransactionId', null),
				total: _.get(result, 'total', payload.amount) || payload.amount,
				exchangeRate: _.get(result, 'exchangeRate', 0) || 0,
				fee: _.get(result, 'fee', 0) || 0,
				taxAmount: _.get(result, 'taxAmount', 0) || 0,
				supplierResponsed: [result.supplierResponsed]
			}, {
				new: true
			}]);

			if (!_.isObject(updated)) {
				const alertMsg = `❗Chuyển tiền từ Ngân hàng -> Ngân hàng Cập nhật thông tin thất bại
		  \nParam: ${JSON.stringify(payload)}
		  \nResult:  ${JSON.stringify(result)}`;
				await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });

				response.message = 'Tạo lệnh chuyển tiền thất bại. Vui lòng thử lại';
				return response;
			}

			if (result.state === BankServiceConstant.STATE.TRANSFERED) {
				response.code = 1;
			}
			if (result.state === BankServiceConstant.STATE.TRANSFERING) {
				response.code = -2;
			}
			response.message = _.get(result, 'message', '');
			response.state = result.state;
			response.data = {
				message: _.get(result, 'message', '')
			};
			response.original = result.original;
			return response;
		} catch (error) {
			this.logger.info(`[BankTransferService] Transfer Error: ${error}`);
			response.message = 'Đã có lỗi xảy ra';
			return response;
		}
	};
	const GetBankName = async (payload) => {
		const response = {
			code: -1,
			state: BankServiceConstant.STATE.FAILED,
			message: null
		};

		const BankCodeConfig = await this.broker.call('v1.eWalletBankCodeModel.findMany', [{ isActive: true }]);
		const bankInfo = _.find(BankCodeConfig, { swiftCode: payload.swiftCode });
		if (_.isNil(bankInfo)) {
			response.message = 'Ngân hàng không đươc hỗ trợ';
			return response;
		}

		const getBankCode = _.get(bankInfo, 'card.atm.prefix', '');
		let { supplier } = payload;
		if (_.isNil(supplier)) {
			let dataRandomSupplier = await this.broker.call('v1.eWalletSetting.getSettingConfig', { body: { keys: [CONSTANTS.RANDOM] } });

			if (_.get(dataRandomSupplier, 'code', null) === 202500 && !_.isEmpty(dataRandomSupplier.data) && dataRandomSupplier.data[0]) {
				dataRandomSupplier = dataRandomSupplier.data[0].value;
				const JsonData = JSON.parse(dataRandomSupplier);
				supplier = Private.randomBankTransfer(JsonData);
			}
		}

		let result = null;
		switch (supplier) {
			// BIDV
			case BankServiceConstant.SUPPLIER.BIDV: {
				// Chuyen den ngan hang BIDV
				if (bankInfo.swiftCode === 'BIDVVNVX') {
					const paramsSend = {
						cardNumber: payload.cardNumber,
						accountNumber: payload.bankAccountNumber,
						swiftCode: payload.swiftCode
					};
					result = await this.BIDVBankLibrary.GetBankNameDomestic(paramsSend);
				} else {
					const paramsSend = {
						cardNumber: payload.cardNumber,
						accountNumber: payload.bankAccountNumber,
						bankCode: getBankCode,
						swiftCode: payload.swiftCode
					};
					result = await this.BIDVBankLibrary.GetBankName247(paramsSend);
				}
				break;
			}
			case BankServiceConstant.SUPPLIER.PG: {
				const paramsSend = {
					cardNumber: payload.cardNumber || payload.bankAccountNumber,
					swiftCode: payload.swiftCode
				};
				result = await this.broker.call('v1.utility.detectBankAccount', { body: paramsSend });
				if (result.code === 113400) {
					result.state = BankServiceConstant.GET_NAME_STATE.SUCCEEDED;
				}
				// Chuyen den ngan hang BIDV
				break;
			}
			case BankServiceConstant.SUPPLIER.OCB: {
				const isGetCardName = !_.isNil(payload.cardNumber);
				if (isGetCardName) {
					const paramsSend = {
						cardNumber: payload.cardNumber,
						swiftCode: payload.swiftCode
					};
					result = await this.BSLibs.GetBankCardName(paramsSend);
				} else {
					const paramsSend = {
						bankAccountNumber: payload.bankAccountNumber,
						swiftCode: payload.swiftCode
					};
					result = await this.BSLibs.GetBankAccountName(paramsSend);
				}

				break;
			}
			default: break;
		}

		response.data = {
			message: _.get(result, 'message', null),
			fullname: _.get(result, 'fullname', null)
		};
		response.state = _.get(result, 'state', BankServiceConstant.GET_NAME_STATE.FAILED);
		response.code = result.state === BankServiceConstant.GET_NAME_STATE.SUCCEEDED ? 1 : -1;
		response.original = _.get(result, 'supplierResponsed', null);
		return response;
	};
	const GetSupplier = async function () {
		let result = null;
		let JsonData;
		let dataRandomSupplier = await this.broker.call('v1.eWalletSetting.getSettingConfig', { body: { keys: [CONSTANTS.RANDOM] } });

		if (_.get(dataRandomSupplier, 'code', null) === 202500 && !_.isEmpty(dataRandomSupplier.data) && dataRandomSupplier.data[0]) {
			dataRandomSupplier = dataRandomSupplier.data[0].value;
			JsonData = JSON.parse(dataRandomSupplier);
		}
		const supplier = Private.randomBankTransfer(JsonData);

		if (supplier !== '') { result = supplier; }
		return result;
	};
	return {
		Transfer,
		GetBankName,
		GetSupplier
	};
};
