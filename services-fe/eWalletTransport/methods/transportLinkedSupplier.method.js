const _ = require('lodash');

const TransportConstant = require('../constants/transport.constant');

const TelegramGroup = process.env.NOTIFY_TELEGRAM;

module.exports = async function () {
	const BIDVSupplier = async (payload) => {
		const response = {
			code: -99,
			data: null,
			original: null
		};

		try {
			const { transportInfo } = payload;
			const { linkedInfo } = payload;

			const accountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [{ id: linkedInfo.accountId }]);
			if (_.get(accountInfo, 'id', 0) < 1) {
				response.state = TransportConstant.STATE.FAILED;
				response.message = 'Không tìm thấy thông tin tài khoản.';
				return response;
			}

			let bankDescription = this.RemoveUnicode(transportInfo.note || '');
			bankDescription = _.trim(bankDescription).substr(0, 160);
			bankDescription = _.replace(bankDescription, /:/g, '-');
			bankDescription = _.replace(bankDescription, /[^a-zA-Z0-9\s-]/g, '');

			const bidvParams = {
				trans_id: _.toString(transportInfo.transaction),
				trans_desc: bankDescription,
				amount: _.toNumber(transportInfo.amount),
				account_id: _.toString(transportInfo.accountId),
				sign_type: 'RSA',
				sign: 'xxxxxxxx',
				version: _.get(linkedInfo, 'cardInfo.version', null)
			};
			this.logger.info(`[eWalletTransport] Transport Linked Supplier Method >> BIDVSupplier >> bidvParams: ${JSON.stringify(bidvParams)}`);
			const bidvWithdrawResponse = await this.BIDVBankLibs.Transfer(bidvParams);
			this.logger.info(`[eWalletTransport] Transport Linked Supplier Method >> BIDVSupplier >> bidvWithdrawResponse: ${JSON.stringify(bidvWithdrawResponse)}`);

			response.code = bidvWithdrawResponse.code;
			response.original = JSON.stringify(bidvWithdrawResponse.original);
			response.data = bidvWithdrawResponse.data;

			return response;
		} catch (error) {
			this.logger.info(`[TransportLinkedSupplierMethod] BIDVSupplier Error ${error}`);
			response.data = {
				message: error.message
			};
			return response;
		}
	};
	const VietinbankSupplier = async (payload) => {
		const response = {
			code: -99,
			data: null,
			original: null
		};

		try {
			const { transportInfo } = payload;
			const { linkedInfo } = payload;

			// "trans_id": "123",
			// "account_id": "123456",
			// "card_id": 4,
			// "amount": 10000,
			// "channel": "MOBILE",
			// "language": "vi",
			// "trans_desc": "thanh toán học phí",
			// "sign_type": "RSA",
			// "sign": "xxxxxxxx"

			const cardId = _.get(linkedInfo, 'linkedInfo.cardId', null);
			const paramsRequest = {
				trans_id: transportInfo.transaction,
				account_id: _.toString(transportInfo.accountId),
				card_id: cardId,
				amount: _.toNumber(transportInfo.amount),
				channel: 'MOBILE',
				language: 'vi',
				trans_desc: 'Rút tiền từ Ví PayME.',
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			this.logger.info(`[eWalletTransport] Transport Linked Supplier Method >> ViettinbankSupplier >> paramsRequest: ${JSON.stringify(paramsRequest)}`);
			const result = await this.VietinBankLibs.Withdraw(paramsRequest);
			this.logger.info(`[eWalletTransport] Transport Linked Supplier Method >> ViettinbankSupplier >> result: ${JSON.stringify(result)}`);

			if (result.code !== 1) {
				const message = _.get(result, 'data.message', 'Lỗi hệ thống!');
				const alertMessage = `Vietin -⚠️⚠️⚠️ #${transportInfo.transaction} Tạo lệnh chuyển tiền cho card_id ${cardId} thất bại. \nReason ${message} ⚠️⚠️⚠️`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[eWalletTransport] Transport Linked Supplier Method >> ViettinbankSupplier >> Alert Telegram Error: ${alertMessage} ${JSON.stringify(error)}`);
				}
				// code return is -1
				response.message = _.get(result, 'data.message', 'Thanh toán thất bại');
				response.supplierResponsed = result;
				return response;
			}
			const alertMessage = `PG - #${transportInfo.transaction} Tạo lệnh chuyển tiền cho card_id ${cardId} thành công.`;
			try {
				await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
			} catch (error) {
				this.logger.info(`[eWalletTransport] Transport Linked Supplier Method >> ViettinbankSupplier >> Alert Telegram Error: ${alertMessage} ${JSON.stringify(error)}`);
			}

			response.code = result.code;
			response.original = result.original;
			response.data = result.data;

			return response;
		} catch (error) {
			this.logger.info(`[TransportLinkedSupplierMethod] VietinbankSupplier Error ${error}`);
			response.data = {
				message: error.message
			};
			return response;
		}
	};
	const SacomBankSupplier = async (payload) => {
		const response = {
			code: -99,
			data: null,
			original: null
		};

		try {
			const { transportInfo } = payload;
			const { linkedInfo } = payload;

			const paramsSend = {
				trans_id: transportInfo.transaction,
				trans_desc: payload.description || 'Rut tien tu PayME ve the Sacombank',
				amount: transportInfo.amount,
				account_id: transportInfo.accountId,
				card_id: _.get(linkedInfo, 'linkedInfo.cardId', null),
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			this.logger.info('[WITHDRAW] => [LINKED] => [SACOMBANK] => [PAYLOAD] ', JSON.stringify(paramsSend));
			const sacomBankResponse = await this.broker.call('v1.sacomBank.walletToBank', paramsSend);
			this.logger.info('[WITHDRAW] => [LINKED] => [SACOMBANK] => [RESPONSE] ', JSON.stringify(sacomBankResponse));

			if (_.get(sacomBankResponse, 'code', 1) === 166050) {
				sacomBankResponse.code = 1;
			} else sacomBankResponse.code = -1;

			response.code = sacomBankResponse.code;
			response.original = sacomBankResponse;
			response.data = _.get(sacomBankResponse, 'data', {});

			return response;
		} catch (error) {
			this.logger.info(`[TransportLinkedSupplierMethod] SacomBankSupplier Error ${error}`);
			response.data = {
				message: error.message
			};
			return response;
		}
	};
	const MBBankSupplier = async (payload) => {
		const response = {
			code: -1,
			data: null,
			original: null,
			message: 'Rút tiền thất bại.'
		};

		try {
			const { transportInfo } = payload;

			const actionBroker = 'v1.mb.cashout';
			const paramsRequest = {
				accountId: transportInfo.accountId,
				merchantId: process.env.WALLET_MERCHANT_ID,
				transactionId: _.toString(transportInfo.transaction),
				amount: _.toNumber(transportInfo.amount)
			};

			this.logger.info('===> [TRANSPORT_LINKED]:::[MBBANK]:::[PARAMS] :>> ', JSON.stringify({ actionBroker, paramsRequest }));
			const result = await this.broker.call(actionBroker, paramsRequest, { timeout: 2 * 60 * 1000 });
			this.logger.info('===> [TRANSPORT_LINKED]:::[MBBANK]:::[RESPONSE] :>> ', JSON.stringify({ actionBroker, result }));

			response.original = result;

			if (result.code !== 174300) {
				const realMessage = _.get(result, 'data.message', 'Lỗi hệ thống!');
				const alertMessage = `Vietin -⚠️⚠️⚠️ #${transportInfo.transaction} Tạo lệnh chuyển tiền cho accountId ${transportInfo.accountId} thất bại. \nReason ${realMessage} ⚠️⚠️⚠️`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[eWalletTransport] Transport Linked Supplier Method >> ViettinbankSupplier >> Alert Telegram Error: ${alertMessage} ${JSON.stringify(error)}`);
				}
				// code return is -1
				response.message = _.get(result, 'message', 'Thanh toán thất bại');
				return response;
			}

			const alertMessage = `PG - #${transportInfo.transaction} Tạo lệnh chuyển tiền cho accountId ${transportInfo.accountId} thành công.`;
			try {
				await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
			} catch (error) {
				this.logger.info(`[eWalletTransport] Transport Linked Supplier Method >> ViettinbankSupplier >> Alert Telegram Error: ${alertMessage} ${JSON.stringify(error)}`);
			}

			response.code = 1;
			// response.original = result.original;
			response.data = result.data;
			response.referenceTransaction = _.get(result, 'data.paymentId', null);
			return response;
		} catch (error) {
			this.logger.info(`[TransportLinkedSupplierMethod] MBBankSupplier Error ${error}`);
			response.data = {
				message: 'Rút tiền thất bại. Vui lòng thử lại sau (E001).'
			};
			return response;
		}
	};
	const AgribankSupplier = async (payload) => {
		const response = {
			code: -99,
			data: null,
			original: null
		};

		try {
			const { transportInfo } = payload;

			const paramsSend = {
				transactionId: transportInfo.transaction,
				amount: transportInfo.amount,
				accountId: transportInfo.accountId,
				merchantId: process.env.WALLET_MERCHANT_ID
			};

			this.logger.info('[WITHDRAW] => [LINKED] => [AGRIBANK] => [PAYLOAD] ', JSON.stringify(paramsSend));
			const agribankResponse = await this.broker.call('v1.agribank.wallet2Bank', paramsSend);
			this.logger.info('[WITHDRAW] => [LINKED] => [AGRIBANK] => [RESPONSE] ', JSON.stringify(agribankResponse));

			if (_.get(agribankResponse, 'code', 1) === 170040) {
				agribankResponse.code = 1;
			} else agribankResponse.code = -1;

			response.code = agribankResponse.code;
			response.original = agribankResponse;
			response.data = _.get(agribankResponse, 'data', {});
			response.referenceTransaction = _.get(agribankResponse, 'data.supplierTransId', null);
			return response;
		} catch (error) {
			this.logger.info(`[TransportLinkedSupplierMethod] AgribankSupplier Error ${error}`);
			response.data = {
				message: error.message
			};
			return response;
		}
	};
	return {
		BIDVSupplier,
		VietinbankSupplier,
		SacomBankSupplier,
		MBBankSupplier,
		AgribankSupplier
	};
};
