/* eslint-disable object-curly-newline */
const _ = require('lodash');
const { customAlphabet } = require('nanoid');

module.exports = async function (prefix) {
	const nanoId = customAlphabet('0123456789', 11);
	let UUID = null;
	try {
		UUID = await this.broker.call('v1.walletUuid.pick', { prefix });
		if (!!UUID === false) {
			throw new Error('GEN UUID Error');
		}
	} catch (error) {
		UUID = nanoId();
	}
	return UUID;
};
