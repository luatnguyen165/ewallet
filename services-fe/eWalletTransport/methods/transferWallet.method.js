const _ = require('lodash');
const { customAlphabet } = require('nanoid');

const TransportConstant = require('../constants/transport.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = async function (transportInfo, receiverInfo) {
	const transportDescription = 'Số dư ví PayME';
	const { TransportModel, TransportWalletModel, AccountModel } = GeneralConstant.MODEL;

	const response = {
		state: TransportConstant.STATE.FAILED,
		message: 'Giao dịch không thành công. Vui lòng thử lại sau.'
	};

	let paramsCreate = {};
	let paramsWhere = {};
	let parasUpdate = {};
	try {
		const accountReceiverInfo = await this.broker.call(AccountModel.findOne, [{ id: receiverInfo.accountId }]);
		if (_.has(accountReceiverInfo, 'id') === false) {
			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Không tìm thấy thông tin tài khoản người nhận.';
			return response;
		}
		if (_.get(accountReceiverInfo, 'kyc.state', null) !== 'APPROVED') {
			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Tài khoản ví PayME nhận tiền chưa thực hiện định danh.';
			return response;
		}

		response.accountInfo = {
			accountId: accountReceiverInfo.id,
			fullname: accountReceiverInfo.fullname
		};
		response.transportInfo = {
			accountId: transportInfo.accountId,
			transportId: transportInfo.id,
			transaction: transportInfo.transaction,
			state: TransportConstant.STATE.FAILED,
			method: TransportConstant.METHOD.WALLET,
			description: transportDescription
		};

		paramsCreate = {
			accountId: accountReceiverInfo.id,
			transportId: transportInfo.id,
			appId: transportInfo.appId,
			transaction: transportInfo.transaction,
			amount: transportInfo.amount,
			service: transportInfo.service,
			state: TransportConstant.STATE.SUCCEEDED,
			description: transportInfo.description,
			note: transportInfo.note
		};

		const transportWallet = await this.broker.call(TransportWalletModel.create, [paramsCreate]);
		if (_.get(transportWallet, 'id', false) === false) {
			this.logger.info(`[TRANSPORT_WALLET]:::[CREATE_ERROR]: ${JSON.stringify({ action: TransportWalletModel.create, params: paramsCreate })}`);
			await this.broker.call(TransportModel.updateOne, [
				{ id: transportInfo.id },
				{
					state: TransportConstant.STATE.FAILED,
					method: TransportConstant.METHOD.WALLET
				}
			]);

			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Giao dịch không thành công. Vui lòng thử lại sau (E001);';
			return response;
		}

		paramsWhere = { id: transportInfo.id };
		parasUpdate = {
			description: transportDescription,
			state: TransportConstant.STATE.SUCCEEDED,
			method: TransportConstant.METHOD.WALLET,
			methodData: {
				resolveType: 'TransportWalletObject',
				transportWalletId: transportWallet.id,
				accountInfo: {
					accountId: transportInfo.accountId,
					fullname: transportInfo.fullname
				}
			}
		};

		let resultUpdate = await this.broker.call(TransportModel.updateOne, [paramsWhere, parasUpdate]);
		if (_.get(resultUpdate, 'nModified', 0) < 1) {
			this.logger.info(`[TRANSPORT_WALLET]:::[UPDATE_ERROR]: ${JSON.stringify(
				{
					action: TransportModel.updateOne,
					condition: paramsWhere,
					update: parasUpdate
				}
			)}`);
			await this.broker.call(TransportWalletModel.updateOne, [{ id: transportWallet.id }, { state: TransportConstant.STATE.FAILED }]);

			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Giao dịch không thành công. Vui lòng thử lại sau (E002);';
			return response;
		}

		const paramsRequest = {
			accountId: accountReceiverInfo.id,
			amount: transportInfo.amount,
			description: 'Nạp tiền vào ví PayME.',
			referData: transportInfo
		};
		this.logger.info(`[TRANSPORT_WALLET] => [DEPOSIT_PARAMS]: ${JSON.stringify(paramsRequest)}`);
		const depositResult = await this.WalletLibs.Deposit(paramsRequest);
		this.logger.info(`[TRANSPORT_WALLET] => [DEPOSIT_RESPONSE]: ${JSON.stringify(depositResult)}`);

		if (depositResult.succeeded === false) {
			this.logger.info(`[TRANSPORT_WALLET] => [DEPOSIT_WALLET_ERROR]: ${JSON.stringify({ paramsRequest, depositResult })}`);

			await this.broker.call(TransportModel.updateOne, [{ id: transportInfo.id }, { state: TransportConstant.STATE.FAILED }]);
			await this.broker.call(TransportWalletModel.updateOne, [{ id: transportWallet.id }, { state: TransportConstant.STATE.FAILED }]);

			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.';
			return response;
		}

		const balance = _.get(depositResult, 'balance', null);

		resultUpdate = await this.broker.call(TransportModel.updateOne, [{ id: transportInfo.id }, { 'methodData.balance': balance }]);
		if (_.get(resultUpdate, 'nModified', 0) < 1) {
			this.logger.info(`[TRANSPORT_WALLET]:::[UPDATE_ERROR_1]: ${JSON.stringify(
				{
					action: TransportModel.updateOne,
					condition: { id: transportInfo.id },
					update: { 'methodData.balance': balance }
				}
			)}`);
		}

		resultUpdate = await this.broker.call(TransportWalletModel.updateOne, [{ id: transportWallet.id }, { balance }]);
		if (_.get(resultUpdate, 'nModified', 0) < 1) {
			this.logger.info(`[TRANSPORT_WALLET]:::[UPDATE_ERROR_2]: ${JSON.stringify(
				{
					action: TransportWalletModel.updateOne,
					condition: { id: transportWallet.id },
					update: { balance }
				}
			)}`);
		}

		response.state = TransportConstant.STATE.SUCCEEDED;
		response.transportInfo.state = TransportConstant.STATE.SUCCEEDED;
		response.message = 'Chuyển tiếp tiền thành công.';
		response.balance = depositResult.balance;
		return response;
	} catch (error) {
		this.logger.info(`[TRANSPORT_WALLET]:::[EXCEPTION]: ${String(error)}`);
	}
	return response;
};
