const _ = require('lodash');

const PaymentConstant = require('../constants/payment.constant');

module.exports = async function (method, data = {}) {
	let description = '';
	let number = _.get(data, 'cardNumber', '') || '';
	if (number === '') {
		number = _.get(data, 'accountNumber', '') || '';
	}

	// fix cua Hau
	if (number === '') {
		number = _.get(data, 'bankAccountNumber', '') || '';
	}
	switch (method) {
		case PaymentConstant.PAYMENT_TYPE.WALLET:
			description = 'Số dư ví PayME';
			break;

		case PaymentConstant.PAYMENT_TYPE.BANK_CARD:
		case PaymentConstant.PAYMENT_TYPE.BANK_ACCOUNT:
		case PaymentConstant.PAYMENT_TYPE.BANK_TRANSFER:
		case PaymentConstant.PAYMENT_TYPE.LINKED: {
			const BankCodeConfig = await this.broker.call('v1.eWalletBankCodeModel.findOne', [{ isActive: true, swiftCode: data.swiftCode }]);
			const bankName = _.get(BankCodeConfig, 'shortName', '');

			description = `${bankName}-${number.substr(-4, 4)}`;
			break;
		}
		case PaymentConstant.PAYMENT_TYPE.BANK_QR_CODE:
			description = data.description;
			break;
		default:
			break;
	}
	return description;
};
