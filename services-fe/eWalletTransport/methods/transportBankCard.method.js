const _ = require('lodash');

const BankServiceConstant = require('../constants/bankService.constant');
const TransportConstant = require('../constants/transport.constant');

const TelegramGroup = process.env.NOTIFY_TELEGRAM;
const ModuleConstant = {
	METHOD_RESOLVE_TYPE: 'TransportBankCardObject'
};

module.exports = async function (payload) {
	const response = {
		method: TransportConstant.METHOD.BANK_CARD,
		state: null,
		message: null,
		bankInfo: {
			swiftCode: null,
			cardNumber: null,
			cardHolder: null,
			bankName: null
		}
	};

	try {
		const { transportInfo } = payload;
		const { bankCard } = payload;

		const cardNumberPrefix = bankCard.cardNumber.substr(0, 6);
		let bankInfo = null;
		const BankCodeConfig = await this.broker.call('v1.eWalletBankCodeModel.findMany', [{ isActive: true }]);
		_.find(BankCodeConfig, (bankConfig) => {
			if (_.get(bankConfig, 'card.atm.prefix', false) && _.get(bankConfig, 'card.atm.prefix', null) === cardNumberPrefix) {
				bankInfo = {
					cardNumber: bankCard.cardNumber,
					cardHolder: bankCard.cardHolder,
					swiftCode: bankConfig.swiftCode,
					bankName: bankConfig.vi
				};
				return true;
			}
			return false;
		});

		if (_.isEmpty(bankInfo) === true) {
			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Ngân hàng chưa được hổ trợ.';
			return response;
		}
		const transportDescription = await this.BankDescriptionService(TransportConstant.METHOD.BANK_CARD, bankInfo);
		response.bankInfo = {
			swiftCode: bankInfo.swiftCode,
			cardNumber: bankInfo.cardNumber,
			cardHolder: bankInfo.cardHolder,
			bankName: bankInfo.bankName
		};
		response.transportInfo = {
			accountId: transportInfo.accountId,
			transportId: transportInfo.id,
			transaction: transportInfo.transaction,
			state: TransportConstant.STATE.FAILED,
			method: TransportConstant.METHOD.BANK_CARD,
			description: transportDescription
		};

		const createdTransportBankCard = await this.broker.call('v1.eWalletTransportBankCardModel.create', [{
			accountId: transportInfo.accountId,
			transportId: transportInfo.id,
			appId: transportInfo.appId,
			transaction: transportInfo.transaction,
			amount: transportInfo.amount,
			service: transportInfo.service,
			state: TransportConstant.STATE.SUCCEEDED,
			description: transportInfo.description,
			bankInfo,
			note: transportInfo.note
		}]);
		if (_.get(createdTransportBankCard, 'id', 0) < 1) {
			const updateTransportResponse = await this.broker.call('v1.eWalletTransportModel.findOneAndUpdate', [{
				id: transportInfo.id
			}, {
				$set: {
					state: TransportConstant.STATE.FAILED,
					method: TransportConstant.METHOD.BANK_CARD
				}
			}, {
				new: true
			}]);
			if (_.get(updateTransportResponse, 'id', 0) < 1) {
				const alertMsg = `Cập nhật Transport khi tạo TransportBankCard thất bại thất bại:\ntransportId: ${transportInfo.id}`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
				}
			}
			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Giao dịch không thành công. Vui lòng thử lại sau.';
			return response;
		}

		let updateTransportResponse = await this.broker.call('v1.eWalletTransportModel.findOneAndUpdate', [{
			id: transportInfo.id
		}, {
			$set: {
				description: transportDescription,
				state: TransportConstant.STATE.SUCCEEDED,
				method: TransportConstant.METHOD.BANK_CARD,
				methodData: {
					resolveType: ModuleConstant.METHOD_RESOLVE_TYPE,
					transportBankCardId: createdTransportBankCard.id,
					bankInfo
				}
			}
		}, {
			new: true
		}]);
		if (_.get(updateTransportResponse, 'id', 0) < 1) {
			const updateTransportBankCardResponse = await this.broker.call('v1.eWalletTransportBankCardModel.findOneAndUpdate', [{
				id: createdTransportBankCard.id
			}, {
				$set: { state: TransportConstant.STATE.FAILED }
			}, {
				new: true
			}]);
			if (_.get(updateTransportBankCardResponse, 'id', 0) < 1) {
				const alertMsg = `Cập nhật TransportBankCard khi cập nhật Transport thất bại thất bại:
        \ntransportId: ${transportInfo.id}
        \ntransportBankCardId: ${createdTransportBankCard.id}`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
				}
			}
			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Giao dịch không thành công. Vui lòng thử lại sau.';
			return response;
		}

		let bankDescription = this.RemoveUnicode(transportInfo.note || '');
		bankDescription = _.trim(bankDescription).substr(0, 160);
		bankDescription = _.replace(bankDescription, /:/g, '-');
		bankDescription = _.replace(bankDescription, /[^a-zA-Z0-9\s-]/g, '');
		// const bankService = new BankConnectorService(ocbBankServiceConfig.url, ocbBankServiceConfig.authorization, PayMEConfig.EnvTag);

		const BankTransferService = await this.BankTransferService();
		const transportNapasResponse = await BankTransferService.Transfer({
			transaction: _.get(transportInfo, 'transaction', null),
			amount: transportInfo.amount,
			supplier: payload.supplier,
			bankInfo: {
				method: 'CARD',
				cardNumber: bankInfo.cardNumber,
				swiftCode: bankInfo.swiftCode,
				fullname: bankInfo.cardHolder
			},
			description: bankDescription
		});

		// const transportNapasResponse = await bankTransfer.Transfer(dataBankService);
		// const bankService = new BankConnectorService(ocbBankServiceConfig.url, ocbBankServiceConfig.authorization, PayMEConfig.EnvTag);
		// const transportNapasResponse = await bankService.TransferCard({
		//   transaction: _.get(transportInfo, 'transaction', null),
		//   amount: transportInfo.amount,
		//   description: bankDescription,
		//   cardNumber: bankInfo.cardNumber,
		//   swiftCode: bankInfo.swiftCode
		// });

		const supplierResponsed = [JSON.stringify(transportNapasResponse.original)];
		let updateTransportBankCardResponse = await this.broker.call('v1.eWalletTransportBankCardModel.findOneAndUpdate', [{
			id: createdTransportBankCard.id
		}, {
			$set: {
				supplierResponsed
			}
		}, {
			new: true
		}]);
		if (_.get(updateTransportBankCardResponse, 'id', 0) < 1) {
			const alertMsg = `Cập nhật TransportBankCard supplierResponsed thất bại:
      \ntransportId: ${transportInfo.id}
      \ntransportBankCardId: ${createdTransportBankCard.id}
      \nsupplierResponsed: ${supplierResponsed}`;
			try {
				await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
			} catch (error) {
				this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
			}
		}

		if (transportNapasResponse.code !== 1) {
			let stateUpdate = TransportConstant.STATE.FAILED;
			// Truong hop bi timeout
			if (transportNapasResponse.code === -2) {
				stateUpdate = TransportConstant.STATE.TIME_OUT;
			}

			updateTransportResponse = await this.broker.call('v1.eWalletTransportModel.findOneAndUpdate', [{
				id: transportInfo.id
			}, {
				$set: {
					state: stateUpdate,
					method: TransportConstant.METHOD.BANK_CARD
				}
			}, {
				new: true
			}]);
			if (_.get(updateTransportResponse, 'id', 0) < 1) {
				const alertMsg = `Cập nhật Transport khi transport thất bại thất bại:
        \ntransportId: ${transportInfo.id}`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
				}
			}

			updateTransportBankCardResponse = await this.broker.call('v1.eWalletTransportBankCardModel.findOneAndUpdate', [{
				id: createdTransportBankCard.id
			}, {
				$set: {
					state: stateUpdate
				}
			}, {
				new: true
			}]);
			if (_.get(updateTransportBankCardResponse, 'id', 0) < 1) {
				const alertMsg = `Cập nhật TransportBankCard khi transport thất bại thất bại:
        \ntransportId: ${transportInfo.id}
        \ntransportBankCard: ${createdTransportBankCard.id}`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
				}
			}

			response.state = stateUpdate;
			response.message = _.get(transportNapasResponse, 'message', 'Giao dịch không thành công. Vui lòng thử lại sau.');

			if (response.message === 'ECONNABORTED') {
				response.message = 'Giao dịch vẫn đang được thực hiện. Vui lòng không thực hiện lại để tránh bị trùng.';
			}
			// response.message = 'Giao dịch không thành công. Vui lòng thử lại sau.';
			return response;
		}

		response.state = TransportConstant.STATE.SUCCEEDED;
		response.transportInfo.state = TransportConstant.STATE.SUCCEEDED;
		response.message = 'Chuyển tiếp tiền thành công.';
		return response;
	} catch (error) {
		response.state = TransportConstant.STATE.FAILED;
		response.message = 'Chuyển tiếp tiền thất bại.';
		return response;
	}
};
