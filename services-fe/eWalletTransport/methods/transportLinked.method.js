const _ = require('lodash');

const FrontendConstant = require('../constants/frontend.constant');
const TransportConstant = require('../constants/transport.constant');

const TelegramGroup = process.env.NOTIFY_TELEGRAM;
const ModuleConstant = {
	METHOD_RESOLVE_TYPE: 'TransportLinkedObject'
};

module.exports = async function (params) {
	const response = {
		method: TransportConstant.METHOD.LINKED,
		state: null,
		message: null,
		linkedId: null,
		bankInfo: null
	};

	try {
		const payload = params;
		const { transportInfo } = payload;
		const { linked } = payload;

		const linkedInfo = await this.broker.call('v1.eWalletLinkedModel.findOne', [{
			accountId: transportInfo.accountId,
			id: linked.linkedId,
			state: FrontendConstant.LINKED_STATE.LINKED
		}]);
		if (_.get(linkedInfo, 'id', 0) < 1) {
			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Không tìm thấy thông tin liên kết thẻ/tài khoản ngân hàng.';
			return response;
		}

		const accountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [{
			id: linkedInfo.accountId
		}]);
		if (_.get(accountInfo, 'id', 0) < 1) {
			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Không tìm thấy thông tin tài khoản.';
			return response;
		}

		const BankCodeConfig = await this.broker.call('v1.eWalletBankCodeModel.findMany', [{ isActive: true }]);
		const matchedBank = BankCodeConfig.find((bank) => bank.swiftCode === linkedInfo.cardInfo.swiftCode);
		if (!matchedBank) {
			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Không tìm thấy thông tin ngân hàng.';
			return response;
		}

		const bankInfo = {
			swiftCode: _.get(linkedInfo, 'cardInfo.swiftCode'),
			cardNumber: _.get(linkedInfo, 'cardInfo.cardNumber'),
			cardHolder: _.get(linkedInfo, 'cardInfo.cardHolder'),
			bankAccountNumber: _.get(linkedInfo, 'cardInfo.accountNumber')
		};
		const transportDescription = await this.BankDescriptionService(TransportConstant.METHOD.LINKED, bankInfo);
		response.linkedId = linkedInfo.id;
		response.bankInfo = bankInfo;
		response.accountInfo = {
			accountId: accountInfo.id,
			fullname: accountInfo.fullname
		};
		response.transportInfo = {
			accountId: transportInfo.accountId,
			transportId: transportInfo.id,
			transaction: transportInfo.transaction,
			state: TransportConstant.STATE.FAILED,
			method: TransportConstant.METHOD.LINKED,
			description: transportDescription
		};
		const createdTransportLinked = await this.broker.call('v1.eWalletTransportLinkedModel.create', [{
			accountId: transportInfo.accountId,
			transportId: transportInfo.id,
			linkedId: linkedInfo.id,
			appId: transportInfo.appId,
			transaction: transportInfo.transaction,
			amount: transportInfo.amount,
			service: transportInfo.service,
			state: TransportConstant.STATE.SUCCEEDED,
			description: transportInfo.description,
			bankInfo,
			note: transportInfo.note
		}]);
		if (_.get(createdTransportLinked, 'id', 0) < 1) {
			const updateTransportResponse = await this.broker.call('v1.eWalletTransportModel.findOneAndUpdate', [{
				id: transportInfo.id
			}, {
				$set: {
					state: TransportConstant.STATE.FAILED,
					method: TransportConstant.METHOD.LINKED
				}
			}, {
				new: true
			}]);
			if (_.get(updateTransportResponse, 'id', 0) < 1) {
				const alertMsg = `Cập nhật Transport khi tạo TransportLinked thất bại thất bại:
        \ntransportId: ${transportInfo.id}`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
				}
			}

			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Giao dịch không thành công. Vui lòng thử lại sau.';
			return response;
		}

		let updateTransportResponse = await this.broker.call('v1.eWalletTransportModel.findOneAndUpdate', [{
			id: transportInfo.id
		}, {
			$set: {
				description: transportDescription,
				state: TransportConstant.STATE.SUCCEEDED,
				method: TransportConstant.METHOD.LINKED,
				methodData: {
					resolveType: ModuleConstant.METHOD_RESOLVE_TYPE,
					linkedId: linkedInfo.id,
					transportLinkedId: createdTransportLinked.id,
					bankInfo
				}
			}
		}, {
			new: true
		}]);
		if (_.get(updateTransportResponse, 'id', 0) < 1) {
			const updateTransportLinkedResponse = await this.broker.call('v1.eWalletTransportLinkedModel.findOneAndUpdate', [{
				id: createdTransportLinked.id
			}, {
				$set: { state: TransportConstant.STATE.FAILED }
			}, {
				new: true
			}]);
			if (_.get(updateTransportLinkedResponse, 'id', 0) < 1) {
				const alertMsg = `Cập nhật TransportLinked khi cập nhật Transport thất bại thất bại:
        \ntransportId: ${transportInfo.id}
        \ntransportLinkedId: ${createdTransportLinked.id}`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
				}
			}

			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Giao dịch không thành công. Vui lòng thử lại sau';
			return response;
		}

		let supplierTransportResponse = {};
		const TransportLinkedSupplier = await this.TransportLinkedSupplier();
		switch (linkedInfo.supplier) {
			// case TransportConstant.SUPPLIER.NAPAS_GATEWAY:
			//   supplierTransportResponse = await require('./supplier/BSModule')({
			//     transportInfo,
			//     linkedInfo
			//   });
			//   break;
			// case TransportConstant.SUPPLIER.PVCOMBANK:
			//   supplierTransportResponse = await require('./supplier/PVComBankModule')({
			//     transportInfo,
			//     linkedInfo
			//   });
			//   break;
			// case TransportConstant.SUPPLIER.OCBBANK:
			//   supplierTransportResponse = await require('./supplier/OCBBankModule')({
			//     transportInfo,
			//     linkedInfo
			//   });
			//   break;

			case TransportConstant.SUPPLIER.BIDVBANK:
				supplierTransportResponse = await TransportLinkedSupplier.BIDVSupplier({
					transportInfo,
					linkedInfo
				});
				break;
			case TransportConstant.SUPPLIER.VIETINBANK:
				supplierTransportResponse = await TransportLinkedSupplier.VietinbankSupplier({
					transportInfo,
					linkedInfo
				});
				break;
			case TransportConstant.SUPPLIER.SACOMBANK:
				supplierTransportResponse = await TransportLinkedSupplier.SacomBankSupplier({
					transportInfo,
					linkedInfo
				});
				break;
			case TransportConstant.SUPPLIER.MBBANK:
				supplierTransportResponse = await TransportLinkedSupplier.MBBankSupplier({
					transportInfo,
					linkedInfo
				});
				break;
			case TransportConstant.SUPPLIER.AGRIBANK:
				supplierTransportResponse = await TransportLinkedSupplier.AgribankSupplier({
					transportInfo,
					linkedInfo
				});
				break;
			default: {
				let bankDescription = this.RemoveUnicode(transportInfo.note || '');
				bankDescription = _.trim(bankDescription).substr(0, 160);
				bankDescription = _.replace(bankDescription, /:/g, '-');
				bankDescription = _.replace(bankDescription, /[^a-zA-Z0-9\s-]/g, '');
				const BankTransferService = await this.BankTransferService();
				const transferObj = {
					transaction: _.get(transportInfo, 'transaction', null),
					amount: transportInfo.amount,
					bankInfo: {
						method: bankInfo.bankAccountNumber ? 'ACCOUNT' : 'CARD',
						accountNumber: bankInfo.bankAccountNumber,
						cardNumber: bankInfo.cardNumber,
						swiftCode: bankInfo.swiftCode,
						fullname: bankInfo.cardHolder
					},
					description: bankDescription
				};
				supplierTransportResponse = await BankTransferService.Transfer(transferObj);
				break;
			}
		}

		const supplierResponsed = [JSON.stringify(supplierTransportResponse.original)];
		let updateTransportLinkedResponse = await this.broker.call('v1.eWalletTransportLinkedModel.findOneAndUpdate', [{
			id: createdTransportLinked.id
		}, {
			$set: {
				supplier: linkedInfo.supplier,
				supplierResponsed,
				referenceTransaction: _.get(supplierTransportResponse, 'referenceTransaction', null)
			}
		}, {
			new: true
		}]);
		if (_.get(updateTransportLinkedResponse, 'id', 0) < 1) {
			const alertMsg = `Cập nhật TransportLinked supplierResponsed thất bại:
      \ntransportId: ${transportInfo.id}
      \ntransportLinkedId: ${createdTransportLinked.id}
      \nsupplierResponsed: ${supplierResponsed}`;
			try {
				await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
			} catch (error) {
				this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
			}
		}

		if (supplierTransportResponse.code !== 1) {
			let stateUpdate = TransportConstant.STATE.FAILED;
			// Truong hop bi timeout
			if (supplierTransportResponse.code === -2) {
				stateUpdate = TransportConstant.STATE.TIME_OUT;
			}

			let alertMsg = `Transport linked thất bại:
      \ntransportId: ${transportInfo.id}
      \ntransportLinkedId: ${createdTransportLinked.id}
      \nsupplierCode: ${linkedInfo.supplier}
      \nsupplierResponsed: ${supplierResponsed}`;
			try {
				await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
			} catch (error) {
				this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
			}
			updateTransportResponse = await this.broker.call('v1.eWalletTransportModel.findOneAndUpdate', [{
				id: transportInfo.id
			}, {
				$set: {
					state: stateUpdate,
					method: TransportConstant.METHOD.LINKED
				}
			}, {
				new: true
			}]);
			if (_.get(updateTransportResponse, 'id', 0) < 1) {
				alertMsg = `Cập nhật Transport khi transport thất bại thất bại:
        \ntransportId: ${transportInfo.id}
        \ntransportLinkedId: ${createdTransportLinked.id}`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
				}
			}

			updateTransportLinkedResponse = await this.broker.call('v1.eWalletTransportLinkedModel.findOneAndUpdate', [{
				id: createdTransportLinked.id
			}, {
				$set: {
					state: stateUpdate
				}
			}, {
				new: true
			}]);
			if (_.get(updateTransportLinkedResponse, 'id', 0) < 1) {
				alertMsg = `Cập nhật TransportLinked khi transport thất bại thất bại:
        \ntransportId: ${transportInfo.id}
        \ntransportLinkedId: ${createdTransportLinked.id}`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
				}
			}
			let responseMessage = 'Giao dịch không thành công. Vui lòng thử lại sau !';
			this.logger.info(`[TransportLinkedMethod] SupplierTransportResponse: ${JSON.stringify(supplierTransportResponse)}`);
			if (payload.supplier === 'BIDV') {
				responseMessage = _.get(supplierTransportResponse, 'data.message');
			}
			if (linkedInfo.supplier !== TransportConstant.SUPPLIER.NAPAS_GATEWAY) {
				responseMessage = _.get(supplierTransportResponse, 'original.data.message')
					|| _.get(supplierTransportResponse, 'original.message')
					|| _.get(supplierTransportResponse, 'data.message')
					|| _.get(supplierTransportResponse, 'message')
					|| responseMessage;
			}

			if (responseMessage === 'ECONNABORTED') {
				responseMessage = 'Giao dịch vẫn đang được thực hiện. Vui lòng không thực hiện lại để tránh bị trùng.';
			}
			response.state = stateUpdate;
			response.message = responseMessage;
			return response;
		}

		response.state = TransportConstant.STATE.SUCCEEDED;
		response.transportInfo.state = TransportConstant.STATE.SUCCEEDED;
		response.message = 'Chuyển tiếp tiền thành công.';
		return response;
	} catch (error) {
		this.logger.info(`[TransportLinkedMethod] Error: ${error}`);
		response.state = TransportConstant.STATE.FAILED;
		response.message = 'Chuyển tiếp tiền thất bại.';
		return response;
	}
};
