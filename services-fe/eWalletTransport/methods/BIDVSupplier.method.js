const _ = require('lodash');

const BankServiceConstant = require('../constants/bankService.constant');

const TelegramGroup = process.env.NOTIFY_TELEGRAM;

module.exports = async function () {
	const DomesticTransfer = async (payload) => {
		const response = {
			state: BankServiceConstant.STATE.FAILED,
			message: null
		};
		try {
			let withdrawEnable = await this.broker.call('v1.eWalletSetting.getSettingConfig', { body: { keys: ['withdraw.config.enable'] } });

			if (_.get(withdrawEnable, 'code', null) === 202500 && !_.isEmpty(withdrawEnable.data) && withdrawEnable.data[0]) {
				withdrawEnable = withdrawEnable.data[0].value;
			}
			if (_.get(withdrawEnable, 'code', null) !== 202500 || !withdrawEnable) {
				response.state = BankServiceConstant.STATE.FAILED;
				response.message = 'Chức năng đang được bảo trì, vui lòng thử lại sau hoặc liên hệ CSKH để được hỗ trợ!';
				return response;
			}
			let description = _.get(payload, 'description', '') || '';
			if (description === '') {
				description = 'Chi ho chuyen tien trong BIDV';
			}
			const transDesc = _.trim(description.length < 32 ? description : description.substring(0, 31));
			const params = {
				trans_id: payload.transaction,
				trans_desc: transDesc,
				card_number: payload.cardNumber || payload.accountNumber,
				amount: payload.amount,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			this.logger.info(`[ewalletTransfer] BIDV Supplier Params: ${JSON.stringify(params)}`);
			const bidvResponsed = await this.BIDVBankLibs.TransferBIDV(params);
			this.logger.info(`[ewalletTransfer] BIDV Supplier bidvResponsed: ${JSON.stringify(bidvResponsed)}`);
			const supplierResponsed = JSON.stringify(bidvResponsed.original);

			// another error
			if (bidvResponsed.code !== 1) {
				try {
					const message = _.get(bidvResponsed, 'data.message', 'Thanh toán thất bại!');
					const statusCode = _.get(bidvResponsed, 'data.status_code', '0');
					const alertMessage = `⛔️⛔️⛔️ #${payload.transaction} Tạo lệnh chuyển tiền cho ${payload.cardNumber || payload.accountNumber} thất bại ! Lý do : ${message} - code : ${statusCode}⚠️⚠️⚠️`;
					// const alertMessage = `#${payload.transaction} Tạo lệnh chuyển tiền cho ${payload.cardNumber || payload.accountNumber} thất bại ! Lý do : ${JSON.stringify(bidvResponsed.original)}`;
					this.logger.info(`[ewalletTransfer] BIDV Supplier Alert Message ${alertMessage}`);
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransfer] BIDV Supplier >> DomesticTransfer >> BidvResponse.code !== 1 NotifyError ${error}`);
				}

				const message = _.get(bidvResponsed, 'data.message', 'Thanh toán thất bại');
				if (message === 'ECONNABORTED') {
					response.state = BankServiceConstant.STATE.TRANSFERING;
				}
				// code return is -99
				// const t = Math.round(payload.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
				// const alertMsg = `❗Chuyển tiền từ Ngân hàng -> Ngân hàng trong ngân hàng BIDV bằng BIDVBank thất bại
				//   \nParam: ${JSON.stringify(params)}
				//   \nAmount: ${t}
				//   \nBIDV Response: ${JSON.stringify(bidvResponsed.original)}`;
				// NotifyService.SendAlertNotify(alertMsg, [], ['api']);
				response.supplierResponsed = supplierResponsed;
				response.message = _.get(bidvResponsed, 'data.message', 'Thanh toán thất bại');
				response.supplierResponsed = supplierResponsed;
				return response;
			}
			try {
				const originalCode = _.get(bidvResponsed, 'original.code', 1000);
				if (originalCode === 1000) {
					const alertMessage = `#${payload.transaction} Tạo lệnh chuyển tiền cho ${payload.cardNumber || payload.accountNumber} thành công`;
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
				} else {
					const alertMessage = `⛔️⛔️⛔️#${payload.transaction} Tạo lệnh chuyển tiền cho ${payload.cardNumber || payload.accountNumber} - Giao dịch pending chờ xử lý !⚠️⚠️⚠️`;
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
				}
			} catch (error) {
				this.logger.info(`[ewalletTransfer] BIDV Supplier >> DomesticTransfer >> BidvResponse.code === 1 NotifyError ${error}`);
			}

			response.original = _.get(bidvResponsed, 'original', {});
			response.supplierTransaction = _.get(bidvResponsed, 'data.trans_id', null);
			response.supplierResponsed = supplierResponsed;
			response.state = BankServiceConstant.STATE.TRANSFERED;
			response.message = 'Thanh toán thành công';
			return response;
		} catch (error) {
			this.logger.info(`[BIDVSupplierMethod] DomesticTransfer Error ${error}`);
			response.state = BankServiceConstant.STATE.FAILED;
			response.message = 'Thanh toán thất bại';
			return response;
		}
	};
	const OtherBankTransfer = async (payload) => {
		const response = {
			state: BankServiceConstant.STATE.FAILED,
			message: null
		};
		// validate params
		try {
			let withdrawEnable = await this.broker.call('v1.eWalletSetting.getSettingConfig', { body: { keys: ['withdraw.config.enable'] } });

			if (_.get(withdrawEnable, 'code', null) === 202500 && !_.isEmpty(withdrawEnable.data) && withdrawEnable.data[0]) {
				withdrawEnable = withdrawEnable.data[0].value;
			}
			if (_.get(withdrawEnable, 'code', null) !== 202500 || !withdrawEnable) {
				response.state = BankServiceConstant.STATE.FAILED;
				response.message = 'Chức năng đang được bảo trì, vui lòng thử lại sau hoặc liên hệ CSKH để được hỗ trợ!';
				return response;
			}
			let description = _.get(payload, 'description', '') || '';
			if (description === '') {
				description = 'Chi ho chuyen tien ngoai BIDV';
			}
			const transDesc = _.trim(description.length < 32 ? description : description.substring(0, 31));
			const params = {
				trans_id: payload.transaction,
				trans_desc: transDesc,
				card_number: payload.cardNumber || payload.accountNumber,
				bank_code: payload.bankCode,
				amount: payload.amount,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			this.logger.info('BIDVTransferTo247AnotherBankRes Params >>>>>>>>>>>>>>>>>>', JSON.stringify(params));
			const bidvResponsed = await this.BIDVBankLibs.Transfer247(params);
			this.logger.info('BIDVTransferTo247AnotherBankRes >>>>>>>>>>>>>>>>>>', JSON.stringify(bidvResponsed));
			const supplierResponsed = JSON.stringify(bidvResponsed.original);
			if (bidvResponsed.code !== 1) {
				try {
					const message = _.get(bidvResponsed, 'data.message', 'Thanh toán thất bại!');
					const statusCode = _.get(bidvResponsed, 'data.status_code', '0');
					const alertMessage = `⚠️⚠️⚠️ #${payload.transaction} Tạo lệnh chuyển tiền cho ${payload.cardNumber || payload.accountNumber} thất bại. \nReason ${message} \nCode ${statusCode}⚠️⚠️⚠️`;
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransfer] BIDV Supplier >> OtherBankTransfer >> BidvResponse.code !== 1 NotifyError ${error}`);
				}
				// NotifyService.SendAlertNotify(alertMsg, [], ['api']);

				const message = _.get(bidvResponsed, 'data.message', 'Thanh toán thất bại!');
				if (message === 'ECONNABORTED') {
					response.state = BankServiceConstant.STATE.TRANSFERING;
				}
				response.supplierResponsed = supplierResponsed;
				response.message = _.get(bidvResponsed, 'data.message', 'Thanh toán thất bại!');
				return response;
			}
			const originalCode = _.get(bidvResponsed, 'original.code', 1000);
			if (originalCode === 1000) {
				const alertMessage = `#${payload.transaction} Tạo lệnh chuyển tiền cho ${payload.cardNumber || payload.accountNumber} thành công`;
				await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
			} else {
				const alertMessage = `⚠️⚠️⚠️#${payload.transaction} Tạo lệnh chuyển tiền cho ${payload.cardNumber || payload.accountNumber} Giao dịch pending chờ xử lý ⛔️⛔️⛔️`;
				await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
			}

			response.original = _.get(bidvResponsed, 'original', {});
			response.supplierTransaction = _.get(bidvResponsed, 'data.trans_id', null);
			response.supplierResponsed = supplierResponsed;
			response.state = BankServiceConstant.STATE.TRANSFERED;
			response.message = 'Thanh toán thành công';
			return response;
		} catch (error) {
			this.logger.info(`[BIDVSupplierMethod] OtherBankTransfer Error ${error}`);
			response.state = BankServiceConstant.STATE.FAILED;
			response.message = 'Thanh toán thất bại';
			return response;
		}
	};

	return {
		DomesticTransfer,
		OtherBankTransfer
	};
};
