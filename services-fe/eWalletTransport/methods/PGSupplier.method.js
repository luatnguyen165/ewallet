const _ = require('lodash');

const BankServiceConstant = require('../constants/bankService.constant');
const TransportConstant = require('../constants/transport.constant');

const TelegramGroup = process.env.NOTIFY_TELEGRAM;
const IPN_URL_PATH = '/v3/Payout/IPN';
const { PAYOUT_IPN_URL } = process.env;

module.exports = async function () {
	const Transfer = async (payload) => {
		const response = {
			state: BankServiceConstant.STATE.FAILED,
			message: null
		};

		try {
			// validate data
			let description = _.get(payload, 'description', '') || '';
			if (description === '') {
				description = 'Chuyen tien tu tai khoan PayME toi Bank';
			}
			const pgParams = {
				partnerTransaction: payload.transaction,
				amount: payload.amount,
				destination: {
					bankAccount: {
						accountNumber: payload.accountNumber,
						accountName: payload.accountName,
						swiftCode: payload.swiftCode
					}
				},
				type: 'PAYMENT',
				content: description,
				ipnUrl: `${PAYOUT_IPN_URL}${IPN_URL_PATH}`
			};
			this.logger.info(`[ewalletTransfer] PG Supplier >> Transfer >> pgParams: ${JSON.stringify(pgParams)}`);

			const pgResponsed = await this.PGLibs.Payout(pgParams);
			this.logger.info(`[ewalletTransfer] PG Supplier >> Transfer >> pgResponsed: ${JSON.stringify(pgResponsed)}`);
			const supplierResponsed = JSON.stringify(pgResponsed.supplierResponse);

			// another error
			if (pgResponsed.state !== TransportConstant.STATE.SUCCEEDED) {
				try {
					const message = _.get(supplierResponsed, 'data.message', 'Thanh toán thất bại!');
					const alertMessage = `PG -⚠️⚠️⚠️ #${payload.transaction} Tạo lệnh chuyển tiền cho ${payload.accountNumber} thất bại. \nReason ${message} ⚠️⚠️⚠️`;
					this.logger.info(`[PGSupplier] PGResponse State !== Succeeded Error ${alertMessage}`);
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransfer] PG Supplier >> Transfer >> pgResponsed.code !== 1 NotifyError ${error}`);
				}
				// code return is -1
				response.message = _.get(pgResponsed, 'data.message', 'Thanh toán thất bại');
				response.supplierResponsed = supplierResponsed;
				return response;
			}
			try {
				const alertMessage = `PG - #${payload.transaction} Tạo lệnh chuyển tiền cho ${payload.accountNumber} thành công.`;
				await this.broker.call('v1.utility.notifyTelegram', { message: alertMessage, buttons: [], receiverList: [TelegramGroup] });
			} catch (error) {
				this.logger.info(`[ewalletTransfer] PG Supplier >> Transfer >> pgResponsed.code === 1 NotifyError ${error}`);
			}

			response.original = _.get(pgResponsed, 'original', {});
			response.supplierTransaction = _.get(pgResponsed, 'transaction', null);
			response.supplierResponsed = supplierResponsed;
			response.state = BankServiceConstant.STATE.TRANSFERED;
			response.message = 'Thanh toán thành công';
			return response;
		} catch (error) {
			this.logger.info(`[PGSupplier] Transfer Error: ${error}`);
			response.state = BankServiceConstant.STATE.FAILED;
			response.message = 'Thanh toán thất bại';
			return response;
		}
	};

	return {
		Transfer
	};
};
