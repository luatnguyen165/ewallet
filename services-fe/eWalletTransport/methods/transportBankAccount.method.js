const _ = require('lodash');

const TransportConstant = require('../constants/transport.constant');

const TelegramGroup = process.env.NOTIFY_TELEGRAM;
const ModuleConstant = {
	METHOD_RESOLVE_TYPE: 'TransportBankAccountObject'
};

module.exports = async function (params) {
	const response = {
		method: TransportConstant.METHOD.BANK_ACCOUNT,
		state: null,
		message: null,
		bankInfo: {
			swiftCode: null,
			bankAccountNumber: null,
			bankAccountName: null,
			bankName: null
		}
	};

	try {
		const payload = params;
		const { transportInfo } = payload;
		const { bankAccount } = payload;

		const BankCodeConfig = await this.broker.call('v1.eWalletBankCodeModel.findMany', [{ isActive: true }]);
		const matchedBank = BankCodeConfig.find((bank) => bank.swiftCode === bankAccount.swiftCode);
		if (_.isEmpty(matchedBank) === true) {
			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Không tìm thấy thông tin ngân hàng.';
			return response;
		}
		const bankInfo = {
			swiftCode: matchedBank.swiftCode,
			bankName: matchedBank.shortName,
			bankAccountNumber: bankAccount.bankAccountNumber,
			bankAccountName: bankAccount.bankAccountName
		};
		const transportDescription = await this.BankDescriptionService(TransportConstant.METHOD.BANK_ACCOUNT, bankInfo);
		response.bankInfo = {
			swiftCode: bankInfo.swiftCode,
			bankAccountNumber: bankInfo.bankAccountNumber,
			bankAccountName: bankInfo.bankAccountName,
			bankName: bankInfo.bankName
		};
		response.transportInfo = {
			accountId: transportInfo.accountId,
			transportId: transportInfo.id,
			transaction: transportInfo.transaction,
			state: TransportConstant.STATE.FAILED,
			method: TransportConstant.METHOD.BANK_ACCOUNT,
			description: transportDescription
		};

		// if (PayMEConfig.EnvTag !== 'sandbox') {
		// if (_.get(bankInfo, 'swiftCode') === 'ORCOVNVX') {
		//   response.state = TransportConstant.STATE.FAILED;
		//   response.message = 'Ngân hàng OCB đang được bảo trì trên hệ thống PayME. Vui lòng thử lại sau.';
		//   return response;
		// }
		// if (_.get(bankInfo, 'swiftCode') === 'WBVNVNVX') {
		//   response.state = TransportConstant.STATE.FAILED;
		//   response.message = 'Ngân hàng PVcomBank đang được bảo trì trên hệ thống PayME. Vui lòng thử lại sau.';
		//   return response;
		// }
		// }

		const createdTransportBankAccount = await this.broker.call('v1.eWalletTransportBankAccountModel.create', [{
			accountId: transportInfo.accountId,
			transportId: transportInfo.id,
			appId: transportInfo.appId,
			transaction: transportInfo.transaction,
			amount: transportInfo.amount,
			service: transportInfo.service,
			state: TransportConstant.STATE.SUCCEEDED,
			description: transportInfo.description,
			bankInfo,
			note: transportInfo.note
		}]);
		if (_.get(createdTransportBankAccount, 'id', 0) < 1) {
			const updateTransportResponse = await this.broker.call('v1.eWalletTransportModel.findOneAndUpdate', [{
				id: transportInfo.id
			}, {
				$set: {
					state: TransportConstant.STATE.FAILED,
					method: TransportConstant.METHOD.BANK_ACCOUNT
				}
			}, {
				new: true
			}]);
			if (_.get(updateTransportResponse, 'id', 0) < 1) {
				const alertMsg = `Cập nhật Transport khi tạo bảng con thất bại thất bại:
        \ntransportId: ${transportInfo.id}
        \ninputBankAccount: ${JSON.stringify(bankAccount)}`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
				}
			}

			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Giao dịch không thành công. Vui lòng thử lại sau.';
			return response;
		}

		let updateTransportResponse = await this.broker.call('v1.eWalletTransportModel.findOneAndUpdate', [{
			id: transportInfo.id
		}, {
			$set: {
				description: transportDescription,
				state: TransportConstant.STATE.SUCCEEDED,
				method: TransportConstant.METHOD.BANK_ACCOUNT,
				methodData: {
					resolveType: ModuleConstant.METHOD_RESOLVE_TYPE,
					transportBankAccountId: createdTransportBankAccount.id,
					bankInfo
				}
			}
		}, {
			new: true
		}]);
		if (_.get(updateTransportResponse, 'id', 0) < 1) {
			const updateTransportBankAccountResponse = await this.broker.call('v1.eWalletTransportBankAccountModel.findOneAndUpdate', [{
				id: createdTransportBankAccount.id
			}, {
				$set: { state: TransportConstant.STATE.FAILED }
			}, {
				new: true
			}]);
			if (_.get(updateTransportBankAccountResponse, 'id', 0) < 1) {
				const alertMsg = `Cập nhật TransportBankAccount thất bại khi cập nhật Transport thất bại thất bại:
        \ntransportId: ${transportInfo.id}
        \ntransportBankAccountId: ${createdTransportBankAccount.id}`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
				}
			}
			response.state = TransportConstant.STATE.FAILED;
			response.message = 'Giao dịch không thành công. Vui lòng thử lại sau.';
			return response;
		}

		let bankDescription = this.RemoveUnicode(transportInfo.note || '');
		bankDescription = _.trim(bankDescription).substr(0, 160);
		bankDescription = _.replace(bankDescription, /:/g, '-');
		bankDescription = _.replace(bankDescription, /[^a-zA-Z0-9\s-]/g, '');
		// const bankService = new BankConnectorService(ocbBankServiceConfig.url, ocbBankServiceConfig.authorization, PayMEConfig.EnvTag);

		const BankTransferService = await this.BankTransferService();
		const transportNapasObj = {
			transaction: _.get(transportInfo, 'transaction', null),
			amount: transportInfo.amount,
			bankInfo: {
				method: 'ACCOUNT',
				accountNumber: bankInfo.bankAccountNumber,
				swiftCode: bankInfo.swiftCode,
				fullname: bankInfo.bankAccountName
			},
			description: bankDescription
		};
		const transportNapasResponse = await BankTransferService.Transfer(transportNapasObj);
		// const bankService = new BankConnectorService(ocbBankServiceConfig.url, ocbBankServiceConfig.authorization, PayMEConfig.EnvTag);
		// const transportNapasResponse = await bankService.TransferBank({
		//   transaction: _.get(transportInfo, 'transaction', null),
		//   amount: transportInfo.amount,
		//   description: bankDescription,
		//   bankAccountNumber: bankInfo.bankAccountNumber,
		//   swiftCode: bankInfo.swiftCode
		// });
		const supplierResponsed = [JSON.stringify(transportNapasResponse.original)];
		let updateTransportBankAccountResponse = await this.broker.call('v1.eWalletTransportBankAccountModel.findOneAndUpdate', [{
			id: createdTransportBankAccount.id
		}, {
			$set: {
				supplierResponsed
			}
		}, {
			new: true
		}]);
		if (_.get(updateTransportBankAccountResponse, 'id', 0) < 1) {
			const alertMsg = `Cập nhật TransportBankAccount supplierResponsed thất bại:
      \ntransportId: ${transportInfo.id}
      \ncreatedTransportBankAccount: ${createdTransportBankAccount.id}
      \nsupplierResponsed: ${supplierResponsed}`;
			try {
				await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
			} catch (error) {
				this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
			}
		}

		if (transportNapasResponse.code !== 1) {
			let stateUpdate = TransportConstant.STATE.FAILED;
			// Truong hop bi timeout
			if (transportNapasResponse.code === -2) {
				stateUpdate = TransportConstant.STATE.TIME_OUT;
			}

			updateTransportResponse = await this.broker.call('v1.eWalletTransportModel.findOneAndUpdate', [{
				id: transportInfo.id
			}, {
				$set: {
					state: stateUpdate,
					method: TransportConstant.METHOD.BANK_ACCOUNT
				}
			}, {
				new: true
			}]);
			if (_.get(updateTransportResponse, 'id', 0) < 1) {
				const alertMsg = `Cập nhật Transport khi transport thất bại thất bại:
        \ntransportId: ${transportInfo.id}`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
				}
			}
			updateTransportBankAccountResponse = await this.broker.call('v1.eWalletTransportBankAccountModel.findOneAndUpdate', [{
				id: createdTransportBankAccount.id
			}, {
				$set: {
					state: stateUpdate
				}
			}, {
				new: true
			}]);
			if (_.get(updateTransportBankAccountResponse, 'id', 0) < 1) {
				const alertMsg = `Cập nhật TransportBankAccount khi transport thất bại thất bại:
        \ntransportId: ${transportInfo.id}
        \ntransportBankAccountId: ${createdTransportBankAccount.id}`;
				try {
					await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
				} catch (error) {
					this.logger.info(`[ewalletTransport] BankCardMethod Notify Error ${alertMsg}: ${error}`);
				}
			}
			response.state = stateUpdate;
			response.message = _.get(transportNapasResponse, 'message', 'Giao dịch không thành công. Vui lòng thử lại sau.');
			if (response.message === 'ECONNABORTED') {
				response.message = 'Giao dịch vẫn đang được thực hiện. Vui lòng không thực hiện lại để tránh bị trùng.';
			}
			return response;
		}

		response.state = TransportConstant.STATE.SUCCEEDED;
		response.transportInfo.state = TransportConstant.STATE.SUCCEEDED;
		response.message = 'Chuyển tiếp tiền thành công.';
		return response;
	} catch (error) {
		response.state = TransportConstant.STATE.FAILED;
		response.message = 'Chuyển tiếp tiền thất bại.';
		return response;
	}
};
