module.exports = {
	LIMIT_SETTING_KEY: 'limit.param.amount.withdraw',
	DEFAULT_AMOUNT_MIN: 10000,
	DEFAULT_AMOUNT_MAX: 100000000,
	ALLOWED_TRANSPORT_METHOD: [
		'bankCard',
		'bankAccount',
		'wallet'
	],
	ALLOWED_PAYMENT_METHOD: [
		'wallet'
	],
	TRANSFER_METHOD: {
		BANK: 'BANK',
		PAYME: 'PAYME'
	},
	SWIFT_CODE: {
		BIDV: 'BIDVVNVX',
		OCB: 'ORCOVNVX',
		PVCOMBANK: 'WBVNVNVX',
		SAIGONBANK: 'SBITVNVX',
		VIETINBANK: 'ICBVVNVX'
	},
	PAYMENT_METHOD_GROUP: {
		PAYME: 'PAYME',
		EWALLET: 'EWALLET',
		LINKED_BANK: 'LINKED_BANK',
		LINKED_CREDIT: 'LINKED_CREDIT',
		GATEWAY: 'GATEWAY',
		LINKED_GATEWAY: 'LINKED_GATEWAY',
		LINKED_BANK_OCBBANK: 'LINKED_BANK_OCBBANK',
		ISEC: 'ISEC',
		DEPOSIT_BANK_MANUAL: 'DEPOSIT_BANK_MANUAL',
		SSCC: 'SSCC',
		VNPAY: 'VNPAY',
		PAYME_CREDIT: 'PAYME_CREDIT',
		OTHER: 'OTHER'
	},
	PAYMENT_TYPE: {
		WALLET: 'WALLET',
		BANK_CARD: 'BANK_CARD',
		BANK_ACCOUNT: 'BANK_ACCOUNT',
		BANK_QR_CODE: 'BANK_QR_CODE',
		BANK_TRANSFER: 'BANK_TRANSFER',
		LINKED: 'LINKED',
		CREDIT_CARD: 'CREDIT_CARD',
		PAYME_CREDIT: 'PAYME_CREDIT',
		PAYME_PG: 'PAYME_PG', // Pttt la PayME ben CTT,
		OPEN_EWALLET_PG: 'OPEN_EWALLET_PG', // OpenEWalletPaymentGateWay
		BANK_CARD_PG: 'BANK_CARD_PG', // Thhanh toan bankCard qua duong CTT(payment Gateway)
		MOMO_PG: 'MOMO_PG', // Thanh toan MOMO qua duong CTT(Payment Gateway)
		CREDIT_CARD_PG: 'CREDIT_CARD_PG', // Thanh toan Credit Card qua duong CTT(Payment Gateway),
		BANK_QR_CODE_PG: 'BANK_QR_CODE_PG',
		ZALOPAY_PG: 'ZALOPAY_PG',
		CREDIT_BALANCE: 'CREDIT_BALANCE'
	},
	SUPPLIER: {
		NAPAS_GATEWAY: 'NAPAS_GATEWAY',
		PVCOMBANK: 'PVCOMBANK',
		OCBBANK: 'OCBBANK',
		BIDVBANK: 'BIDVBANK',
		VIETINBANK: 'VIETINBANK',
		WALLET: 'WALLET'
	},
	LINKED_TYPE: {
		// Chạy cổng napas
		ATM: 'ATM',
		// Chạy đường trực tiếp qua bank
		BANKING: 'BANKING',
		// Chạy liên kết đường accountNumber của bank
		ACCOUNT: 'ACCOUNT',
		// Chạy qua CTT
		CREDIT_CARD: 'CREDIT_CARD'
	},
	LINKED_STATE: {
		NEW: 'NEW',
		LINKED: 'LINKED',
		LOCKED: 'LOCKED',
		UNLINK: 'UNLINK',
		FAILED: 'FAILED',
		REQUIRED_OTP: 'REQUIRED_OTP',
		REQUIRED_VERIFY: 'REQUIRED_VERIFY',
		INVALID_OTP: 'INVALID_OTP',
		OTP_RETRY_TIMES_OVER: 'OTP_RETRY_TIMES_OVER'
	},
	LINKED_TYPE_SUPPLIER: {
		CARD: 'CARD',
		ACCOUNT: 'ACCOUNT'
	},
	SERVICES: {
		LINKED: 'LINKED' // Lien ket the
	},
	LINKED_TAG: {
		LINKED: 'LINKED',
		LINKED_PVCBANK: 'LINKED_PVCBANK',
		LINKED_OCBBANK: 'LINKED_OCBBANK',
		LINKED_BIDVBANK: 'LINKED_BIDVBANK',
		LINKED_VIETINBANK: 'LINKED_VIETINBANK',
		LINKED_NAPAS: 'LINKED_NAPAS',
		LINKED_CREDIT: 'LINKED_CREDIT',
		UNLINK: 'UNLINK'
	},
	RESOLVE_TYPE: {
		TRANSFER_MONEY: 'TransferMoneyObject'
	},
	ACCOUNT_KYC_STATE: {
		PENDING: 'PENDING',
		REJECTED: 'REJECTED',
		APPROVED: 'APPROVED',
		CANCELED: 'CANCELED',
		BANNED: 'BANNED',
		null: null
	},
	ENV_NAME: {
		MobileApp: 'MobileApp',
		WebApp: 'WebApp'
	},
	LINKED_GATEWAY: {
		NAPAS: 'NAPAS',
		OCB: 'OCB',
		BIDV: 'BIDV',
		VIETINBANK: 'VIETINBANK',
		PVCBANK: 'PVCBANK',
		PG: 'PG'
	},
	LINKED_METHOD: {
		CREDIT: 'CREDIT',
		CARD: 'CARD',
		ACCOUNT: 'ACCOUNT'
	},
	WITHDRAW_STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		CANCELED: 'CANCELED',
		FAILED: 'FAILED'
	},
	PAYMENT_STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		CANCELED: 'CANCELED',
		FAILED: 'FAILED'
	},
	TRANSFER_STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED',
		TIME_OUT: 'TIME_OUT',
		REFUNDED: 'REFUNDED',
		CANCELED: 'CANCELED',
		INVALID_PARAMS: 'INVALID_PARAMS'
		// ...
	},
	TRANSPORT_STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED',
		TIME_OUT: 'TIME_OUT',
		REFUNDED: 'REFUNDED',
		CANCELED: 'CANCELED',
		INVALID_PARAMS: 'INVALID_PARAMS'
		// ...
	},
	REFUND_STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED'
	},
	FEE_SETTING_KEY: {
		PAYME: 'transferMoney.payme.config',
		BANK: 'transferMoney.bank.config'
	},
	TRANSFER_SOURCE: 'TRANSFER',
	PHONE_CONSTANT: {
		VIETTEL: ['086', '096', '097', '098', '032', '033', '034', '035', '036', '037', '038', '039'],
		MOBIFONE: ['089', '090', '093', '070', '079', '077', '076', '078'],
		VINAPHONE: ['088', '091', '094', '083', '084', '085', '081', '082'],
		VIETNAMOBILE: ['092', '056', '058', '052'],
		GMOBILE: ['099', '059'],
		SFone: ['095'],
		ITELECOM: ['087']
	},
	ACCOUNT_TYPE: {
		PERSONAL: 'PERSONAL',
		BUSINESS: 'BUSINESS'
	},
	MODEL: {
		AccountModel: {
			findOne: 'v1.eWalletAccountModel.findOne',
			findMany: 'v1.eWalletAccountModel.findMany',
			findOneAndUpdate: 'v1.eWalletAccountModel.findOneAndUpdate',
			create: 'v1.eWalletAccountModel.create',
			updateOne: 'v1.eWalletAccountModel.updateOne',
			delete: 'v1.eWalletAccountModel.delete',
			deleteMany: 'v1.eWalletAccountModel.deleteMany'
		},
		LinkedModel: {
			findOne: 'v1.eWalletLinkedModel.findOne',
			findMany: 'v1.eWalletLinkedModel.findMany',
			findOneAndUpdate: 'v1.eWalletLinkedModel.findOneAndUpdate',
			create: 'v1.eWalletLinkedModel.create',
			updateOne: 'v1.eWalletLinkedModel.updateOne',
			delete: 'v1.eWalletLinkedModel.delete',
			deleteMany: 'v1.eWalletLinkedModel.deleteMany'
		},
		TransportModel: {
			findOne: 'v1.eWalletTransportModel.findOne',
			findMany: 'v1.eWalletTransportModel.findMany',
			findOneAndUpdate: 'v1.eWalletTransportModel.findOneAndUpdate',
			create: 'v1.eWalletTransportModel.create',
			updateOne: 'v1.eWalletTransportModel.updateOne',
			delete: 'v1.eWalletTransportModel.delete',
			deleteMany: 'v1.eWalletTransportModel.deleteMany'
		},
		TransportWalletModel: {
			findOne: 'v1.eWalletTransportWalletModel.findOne',
			findMany: 'v1.eWalletTransportWalletModel.findMany',
			findOneAndUpdate: 'v1.eWalletTransportWalletModel.findOneAndUpdate',
			create: 'v1.eWalletTransportWalletModel.create',
			updateOne: 'v1.eWalletTransportWalletModel.updateOne',
			delete: 'v1.eWalletTransportWalletModel.delete',
			deleteMany: 'v1.eWalletTransportWalletModel.deleteMany'
		},

		HistoryModel: {
			findOne: 'v1.eWalletHistoryModel.findOne',
			findMany: 'v1.eWalletHistoryModel.findMany',
			findOneAndUpdate: 'v1.eWalletHistoryModel.findOneAndUpdate',
			create: 'v1.eWalletHistoryModel.create',
			updateOne: 'v1.eWalletHistoryModel.updateOne',
			delete: 'v1.eWalletHistoryModel.delete',
			deleteMany: 'v1.eWalletHistoryModel.deleteMany'
		},
		KYCModel: {
			findOne: 'v1.eWalletKycModel.findOne',
			findMany: 'v1.eWalletKycModel.findMany',
			findOneAndUpdate: 'v1.eWalletKycModel.findOneAndUpdate',
			create: 'v1.eWalletKycModel.create',
			updateOne: 'v1.eWalletKycModel.updateOne',
			delete: 'v1.eWalletKycModel.delete',
			deleteMany: 'v1.eWalletKycModel.deleteMany'
		}
	},
	SUPPLIER_STATE: {
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED',
		PENDING: 'PENDING',
		REQUIRED_OTP: 'REQUIRED_OTP',
		REQUIRED_VERIFY: 'REQUIRED_VERIFY',
		BALANCE_NOT_ENOUGHT: 'BALANCE_NOT_ENOUGHT',
		REQUIRED_TRANSFER: 'REQUIRED_TRANSFER',
		OTP_RETRY_TIMES_OVER: 'OTP_RETRY_TIMES_OVER',
		INVALID_OTP: 'INVALID_OTP',
		INVALID_SECURITY_CODE: 'INVALID_SECURITY_CODE'
	}
};
