const moleculerI18n = require('moleculer-i18n-js');
const path = require('path');
const _ = require('lodash');

// Library
const WalletLibrary = require('./libraries/wallet.library');
const BSLibrary = require('./libraries/bs.library');
const PGLibrary = require('./libraries/pg.library');
const OCBBankLibrary = require('./libraries/ocbBank.library');
const BIDVBankLibrary = require('./libraries/bidvBank.library');
const VietinBankLibrary = require('./libraries/vietinBank.library');

module.exports = {
	name: 'eWalletTransport',
	version: 1,
	mixins: [moleculerI18n],
	i18n: {
		directory: path.join(__dirname, 'locales'),
		locales: ['vi', 'en'],
		defaultLocale: 'vi'
	},

	/**
	 * Settings
	 */
	settings: {},
	/**
	 * Dependencies
	 */
	dependencies: [],
	/**
	 * Actions
	 */
	actions: {
		transfer: {
			params: {
				accountId: 'number',
				appId: 'number|optional',
				amount: 'number',
				description: 'string',
				service: {
					$$type: 'object',
					code: 'string',
					type: 'string|optional',
					transaction: 'string'
				},
				transport: {
					type: 'object',
					strict: 'remove',
					props: {
						wallet: {
							$$type: 'object|optional',
							accountId: 'number'
						},
						bankCard: {
							$$type: 'object|optional',
							swiftCode: 'string',
							cardNumber: 'string',
							cardHolder: 'string|optional',
							supplier: 'string|optional'
						},
						bankAccount: {
							$$type: 'object|optional',
							swiftCode: 'string',
							bankAccountNumber: 'string',
							bankAccountName: 'string|optional',
							supplier: 'string|optional'
						},
						linked: {
							$$type: 'object|optional',
							linkedId: 'number',
							supplier: 'string|optional'
						}
					},
					minProps: 1,
					maxProps: 1
				}

			},
			timeout: 3 * 60 * 1000,
			handler: require('./actions/transfer.action')
		}
	},
	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		RedisCache: require('./methods/redisCache.method'),
		GetUUID: require('./methods/getUUID.method'),
		CalcFee: require('./methods/calcFee.method'),
		WalletTransfer: require('./methods/transferWallet.method'),
		PayWallet: require('./methods/payWallet.method'),
		CardDescription: require('./methods/cardDescription.method'),
		RemoveUnicode: require('./methods/removeUnicode.method'),
		PhoneUtility: require('./methods/phoneUtility.method'),
		BankTransferService: require('./methods/BankTransferService.method'),
		BIDVSupplier: require('./methods/BIDVSupplier.method'),
		OCBSupplier: require('./methods/OCBSupplier.method'),
		PGSupplier: require('./methods/PGSupplier.method'),
		TransportLinked: require('./methods/transportLinked.method'),
		TransportLinkedSupplier: require('./methods/transportLinkedSupplier.method'),
		TransportBankCard: require('./methods/transportBankCard.method'),
		TransportBankAccount: require('./methods/transportBankAccount.method'),
		BankDescriptionService: require('./methods/bankDescriptionService.method')
	},
	/**
   * Service created lifecycle event handler
   */
	created() {
		try {
			_.set(this, 'OCBBankLibs', OCBBankLibrary({ logger: this.logger.info }));
			_.set(this, 'BIDVBankLibs', BIDVBankLibrary({ logger: this.logger.info }));
			_.set(this, 'WalletLibs', WalletLibrary({ logger: this.logger.info }));
			_.set(this, 'PGLibs', PGLibrary({ logger: this.logger.info }));
			_.set(this, 'BSLibs', BSLibrary({ logger: this.logger.info }));
			_.set(this, 'VietinBankLibs', VietinBankLibrary({ logger: this.logger.info }));
		} catch (error) {
			this.logger.info(`[Generate_Library_Error] => ${error}`);
		}
	}
};
