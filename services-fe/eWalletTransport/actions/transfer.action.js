const _ = require('lodash');
const Decimal = require('decimal.js');
const { MoleculerError } = require('moleculer').Errors;
const moment = require('moment');

// Constants
const ErrorCodeConstant = require('../constants/errorCode.constant');
const AccountConstant = require('../constants/account.constant');
const TransportConstant = require('../constants/transport.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	const {
		AccountModel, HistoryModel, LinkedModel, TransportModel
	} = GeneralConstant.MODEL;

	let response = {
		state: 'FAILED',
		message: 'Chuyển tiền thất bại. Vui lòng thử lại sau.',
		transportInfo: {}
	};

	let paramsCheck = null;
	let paramsCreate = null;
	const resultRequest = null;
	try {
		const payload = _.get(ctx, 'params', {});
		this.logger.info(`[TRANSPORT] Payload >>> ${JSON.stringify(payload)}`);
		paramsCheck = {
			id: payload.accountId
		};
		const accountInfo = await this.broker.call(AccountModel.findOne, [paramsCheck]);
		if (_.has(accountInfo, 'id') === false) {
			throw new MoleculerError('Không tìm thấy thông tin tài khoản.', -1);
		}

		if (_.get(accountInfo, 'kyc.state', null) !== AccountConstant.ACCOUNT_KYC_STATE.APPROVED) {
			throw new MoleculerError('Tài khoản chưa được định danh. Vui lòng thực hiện định danh.', -1);
		}
		paramsCheck = {
			accountId: payload.accountId,
			state: GeneralConstant.LINKED_STATE.LINKED
		};

		const linkedInfo = await this.broker.call(LinkedModel.findOne, [paramsCheck]);
		if (_.has(linkedInfo, 'id') === false) {
			throw new MoleculerError('Tài khoản chưa có nguồn tiền liên kết. Vui lòng thực hiện liên kết nguồn tiền.', -1);
		}

		paramsCheck = {
			'service.code': payload.service.code,
			'service.transaction': payload.service.transaction,
			state: TransportConstant.STATE.SUCCEEDED
		};

		const transportInfo = await this.broker.call(TransportModel.findOne, [paramsCheck]);
		if (_.has(transportInfo, 'id') === true) {
			throw new MoleculerError('Giao dịch đã thành công trước đó.', -1);
		}

		const UUID = await this.GetUUID('TRANSPORT');
		paramsCreate = {
			accountId: payload.accountId,
			appId: payload.appId || 0,
			transaction: UUID,
			amount: payload.amount,
			service: payload.service,
			state: TransportConstant.STATE.PENDING,
			note: payload.description || ''
		};

		const createdTransport = await this.broker.call(TransportModel.create, [paramsCreate]);
		if (_.get(createdTransport, 'id', false) === false) {
			throw new MoleculerError('Khởi tạo thông tin giao dịch thất bại. Vui lòng thử lại sau.', -1);
		}

		const transportMethod = _.keys(payload.transport)[0];
		switch (transportMethod) {
			case 'wallet': {
				const paramsRequest = {
					id: createdTransport.id,
					appId: createdTransport.appId,
					accountId: createdTransport.accountId,
					fullname: accountInfo.fullname,
					amount: createdTransport.amount,
					transaction: createdTransport.transaction,
					description: createdTransport.description,
					note: createdTransport.note,
					service: createdTransport.service
				};

				response = await this.WalletTransfer(paramsRequest, payload.transport.wallet);
				_.set(response, 'transportInfo.transport.accountInfo', _.get(response, 'accountInfo', null));
				break;
			}
			case 'bankCard': {
				response = await this.TransportBankCard({
					transportInfo: createdTransport,
					bankCard: payload.transport.bankCard
				});
				_.set(response, 'transportInfo.transport.bankInfo', _.get(response, 'bankInfo', null));
				break;
			}
			case 'bankAccount': {
				response = this.TransportBankAccount({
					transportInfo: createdTransport,
					bankAccount: payload.transport.bankAccount
				});
				_.set(response, 'transportInfo.transport.bankInfo', _.get(response, 'bankInfo', null));
				break;
			}
			case 'linked': {
				response = await this.TransportLinked({
					transportInfo: createdTransport,
					linked: payload.transport.linked
				});
				_.set(response, 'transportInfo.transport.bankInfo', _.get(response, 'bankInfo', null));
				_.set(response, 'transportInfo.transport.linkedId', _.get(response, 'linkedId', null));
				break;
			}
			default:
				throw new MoleculerError('Không tìm thấy phương thưc giao dịch.', -1);
		}
		return response;
	} catch (error) {
		this.logger.info(`[TRANSPORT_MODULE] Error: ${String(error)}`);
		if (error.name === 'MoleculerError') {
			response.message = error.message;
		}
		return response;
	}
};
