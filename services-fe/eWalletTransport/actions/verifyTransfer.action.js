const { MoleculerError } = require('moleculer').Errors;
const _ = require('lodash');
const Decimal = require('decimal.js');
const moment = require('moment');

// Constants
const ErrorCodeConstant = require('../constants/errorCode.constant');
const AccountConstant = require('../constants/account.constant');
const GeneralConstant = require('../constants/general.constant');
const DepositConstant = require('../constants/deposit.constant');

module.exports = async function (ctx) {
	const response = {
		message: 'Thành công',
		code: 1
	};
	try {
		const { phone, otp, transaction } = _.get(ctx, 'params.body', {});

		const authInfo = this.GetAuthInfo(ctx.meta);

		const eWalletAppInfo = await this.broker.call('v1.ewalletAppInfoModel.findOne', [{
			merchantId: authInfo.merchantId
		}]);

		if (_.get(eWalletAppInfo, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.ACCOUNT_CHECK.APPINFO_NOT_FOUND,
				message: this.__(ErrorCodeConstant.ACCOUNT_CHECK.APPINFO_NOT_FOUND.toString())
			};
		}

		const checkPhone = this.PhoneUtility(phone);
		if (!checkPhone.isValid) {
			return {
				code: ErrorCodeConstant.ACCOUNT_CHECK.USER_NOT_CONNECTED,
				message: this.__(ErrorCodeConstant.ACCOUNT_CHECK.USER_NOT_CONNECTED.toString())
			};
		}
		const { phoneFormatted } = checkPhone;

		const connectedUserInfo = await this.broker.call('v1.eWalletConnectedUserModel.findOne', [{
			phone: phoneFormatted,
			appId: eWalletAppInfo.id
		}]);

		if (_.get(connectedUserInfo, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.ACCOUNT_CHECK.USER_NOT_CONNECTED,
				message: this.__(ErrorCodeConstant.ACCOUNT_CHECK.USER_NOT_CONNECTED.toString())
			};
		}

		const accountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [{
			phone: phoneFormatted
		}]);

		if (_.get(accountInfo, 'kyc.state', null) !== AccountConstant.ACCOUNT_KYC_STATE.APPROVED) {
			// message: 'Tài khoản chưa định danh. Vui lòng thực hiện định danh.'
			return {
				code: ErrorCodeConstant.ACCOUNT_CHECK.ACCOUNT_NOT_KYC,
				message: this.__(ErrorCodeConstant.ACCOUNT_CHECK.ACCOUNT_NOT_KYC.toString())
			};
		}

		const transferObj = {
			accountId: accountInfo.id,
			transaction,
			appId: eWalletAppInfo.id,
			state: GeneralConstant.TRANSFER_STATE.PENDING
		};

		let createdTransferMoney = await this.broker.call('v1.eWalletTransferMoneyModel.findOne', [transferObj]);

		if (_.get(createdTransferMoney, 'id', null) === null) {
			// response.message = 'Xác thực rút tiền thất bại. Giao dịch này không tồn tại hoặc đã hoàn thành trước đó. (1)';
			return {
				code: ErrorCodeConstant.VERIFY.TRANSACTION_NOT_FOUND,
				message: this.__(ErrorCodeConstant.VERIFY.TRANSACTION_NOT_FOUND.toString())
			};
		}

		const dataHistory = {
			accountId: accountInfo.id,
			appId: eWalletAppInfo.id,
			'service.transaction': transaction,
			'service.type': 'TRANSFER',
			state: GeneralConstant.TRANSFER_STATE.PENDING
		};

		let senderHistory = await this.broker.call('v1.eWalletHistoryModel.findOne', [dataHistory]);

		if (_.get(senderHistory, 'id', null) === null) {
			// response.message = 'Xác thực rút tiền thất bại. Giao dịch này không tồn tại hoặc đã hoàn thành trước đó. (2)';
			return {
				code: ErrorCodeConstant.VERIFY.TRANSACTION_NOT_FOUND,
				message: this.__(ErrorCodeConstant.VERIFY.TRANSACTION_NOT_FOUND.toString())
			};
		}

		const RedisCache = this.RedisCache();
		const keyCheck = `CountWrongOTPTransfer-${phoneFormatted}`;

		const countCheckLock = _.toNumber(await RedisCache.get({ key: keyCheck })) || 0;

		if (countCheckLock >= 3) {
			const timeLock = _.toNumber(await RedisCache.ttl({ key: keyCheck })) || 0;

			let resultText = '';
			const momentTime = moment.duration(timeLock, 'seconds');

			const hours = momentTime.hours();
			const minutes = momentTime.minutes();
			const seconds = momentTime.seconds();
			if (hours > 0) {
				resultText = `${hours} giờ ${minutes} phút ${seconds} giây`;
			} else if (minutes > 0) {
				resultText = `${minutes} phút ${seconds} giây`;
			} else resultText = `${seconds} giây`;

			response.message = `SDT đã sai mã xác nhận nhiều lần. Tạm khoá : ${resultText}`;
			return {
				code: ErrorCodeConstant.VERIFY.WRONG_OTP_MANY_TIMES,
				message: this.__(_.replace(ErrorCodeConstant.VERIFY.WRONG_OTP_MANY_TIMES.toString(), '{{time}}', resultText))
			};
		}
		const activeCode = await this.broker.call('v1.ewalletActiveCodeModel.findOne', [{
			phone: phoneFormatted,
			code: otp,
			source: GeneralConstant.TRANSFER_SOURCE
		}]);

		if (_.get(activeCode, 'id', null) === null) {
			await RedisCache.set({ key: keyCheck, value: countCheckLock + 1, ttl: 60 });
			return {
				code: ErrorCodeConstant.VERIFY.OTP_NOT_MATCH,
				message: this.__(ErrorCodeConstant.VERIFY.OTP_NOT_MATCH.toString())
			};
		}

		if (moment(activeCode.expiredAt).isBefore(moment(new Date()))) {
			return {
				code: ErrorCodeConstant.VERIFY.OTP_EXPIRED,
				message: this.__(ErrorCodeConstant.VERIFY.OTP_EXPIRED.toString())
			};
		}
		let walletInfo = null;
		try {
			walletInfo = await this.WalletLibs.Information(accountInfo.id);
		} catch (error) {
			this.logger.info(`[eWalletTransfer] => [SENDER_WALLET_INFORMATION_ERROR]: ${error}`);
			// response.message = 'Rút tiền thất bại. Vui lòng thử lại (E02)';
			return {
				code: ErrorCodeConstant.VERIFY.INTERNAL_SERVER_ERROR,
				message: this.__(ErrorCodeConstant.VERIFY.INTERNAL_SERVER_ERROR.toString())
			};
		}
		const paymentParams = {
			accountInfo,
			appId: eWalletAppInfo.id,
			transaction,
			amount: createdTransferMoney.total,
			walletBalance: walletInfo.balance
		};
		const paymentResponsed = await this.PayWallet(paymentParams);

		this.logger.info(`[eWalletTransfer] payment Response: ${JSON.stringify(paymentResponsed)}`);
		if (paymentResponsed.state !== GeneralConstant.PAYMENT_STATE.SUCCEEDED) {
			createdTransferMoney = await this.broker.call('v1.eWalletTransferMoneyModel.findOneAndUpdate', [
				{ id: createdTransferMoney.id },
				{
					state: GeneralConstant.TRANSFER_STATE.FAILED,
					payment: paymentResponsed.payment
				}
			]);

			senderHistory = await this.broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [
				{ id: senderHistory.id },
				{
					state: GeneralConstant.TRANSFER_STATE.FAILED,
					'service.state': GeneralConstant.TRANSFER_STATE.FAILED,
					'service.data.state': GeneralConstant.TRANSFER_STATE.FAILED,
					payment: paymentResponsed.payment
				}
			]);

			// response.message = _.get(PayWallet, 'message', 'Rút tiền thất bại. Vui lòng thử lại (E10)');
			return {
				code: ErrorCodeConstant.VERIFY.TRANSFER_FAILED,
				message: this.__(ErrorCodeConstant.VERIFY.TRANSFER_FAILED.toString())
			};
		}

		let transferResponse;
		let paramsSend;
		if (_.get(createdTransferMoney, 'receiverInfo.accountId', null) !== null) {
			transferResponse = await this.TransferPayme(transaction);
		} else {
			paramsSend = {
				transaction: _.get(createdTransferMoney, 'transaction', null), // Transaction khi nguoi dung thuc hien giao dich
				amount: createdTransferMoney.amount,
				bankInfo: {
					swiftCode: createdTransferMoney.receiverInfo.swiftCode,
					fullname: createdTransferMoney.receiverInfo.fullname
				}
			};
			if (_.get(createdTransferMoney, 'receiverInfo.cardNumber', null) !== null) {
				paramsSend.bankInfo.method = 'CARD';
				paramsSend.bankInfo.cardNumber = createdTransferMoney.receiverInfo.cardNumber;
			} else {
				paramsSend.bankInfo.method = 'ACCOUNT';
				paramsSend.bankInfo.accountNumber = createdTransferMoney.receiverInfo.accountNumber;
			}
			transferResponse = await this.broker.call('v1.eWalletWithdraw.bankBalancerTransfer', paramsSend);
		}

		if (_.get(transferResponse, 'state', null) !== GeneralConstant.TRANSFER_STATE.SUCCEEDED) {
			const refundObj = {
				paymentId: paymentResponsed.payment.id, // Transaction khi nguoi dung thuc hien giao dich
				appId: eWalletAppInfo.id,
				amount: createdTransferMoney.total,
				description: 'Hoàn tiền khi chuyển ví thất bại',
				serviceCode: 'TRANSFER_MONEY',
				serviceId: createdTransferMoney.id
			};
			const refundResponsed = await this.broker.call('v1.eWalletWithdraw.refund', refundObj);
			createdTransferMoney = await this.broker.call('v1.eWalletTransferMoneyModel.findOneAndUpdate', [{
				id: createdTransferMoney.id
			}, {
				state: GeneralConstant.TRANSFER_STATE.FAILED
			}]);

			senderHistory = await this.broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [
				{ id: senderHistory.id },
				{
					state: GeneralConstant.TRANSFER_STATE.FAILED,
					'service.state': GeneralConstant.TRANSFER_STATE.FAILED,
					'service.data.state': GeneralConstant.TRANSFER_STATE.FAILED,
					changed: '-',
					balance: paymentResponsed.balance,
					payment: paymentResponsed.payment,
					publishedAt: new Date().toISOString()
				},
				{ new: true }
			]);
			this.logger.info(`[eWalletTransfer] Refund: ${JSON.stringify(refundResponsed)}`);
			return {
				code: ErrorCodeConstant.VERIFY.TRANSFER_FAILED,
				message: this.__(ErrorCodeConstant.VERIFY.TRANSFER_FAILED.toString())
			};
		}
		createdTransferMoney = await this.broker.call('v1.eWalletTransferMoneyModel.findOneAndUpdate', [{
			id: createdTransferMoney.id
		}, {
			state: GeneralConstant.TRANSFER_STATE.SUCCEEDED,
			payment: paymentResponsed.payment
		}]);

		senderHistory = await this.broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [
			{ id: senderHistory.id },
			{
				state: GeneralConstant.TRANSFER_STATE.SUCCEEDED,
				'service.state': GeneralConstant.TRANSFER_STATE.SUCCEEDED,
				'service.data.state': GeneralConstant.TRANSFER_STATE.SUCCEEDED,
				changed: '-',
				balance: paymentResponsed.balance,
				payment: paymentResponsed.payment,
				publishedAt: new Date().toISOString()
			},
			{ new: true }
		]);

		return {
			code: ErrorCodeConstant.VERIFY.TRANSFER_SUCCEEDED,
			message: this.__(ErrorCodeConstant.VERIFY.TRANSFER_SUCCEEDED.toString()),
			data: {
				transaction
			}
		};
	} catch (error) {
		this.logger.info(`[eWalletTransfer] Transfer Error: ${error}`);
		return {
			code: ErrorCodeConstant.VERIFY.INTERNAL_SERVER_ERROR,
			message: this.__(ErrorCodeConstant.VERIFY.INTERNAL_SERVER_ERROR.toString())
		};
	}
};
