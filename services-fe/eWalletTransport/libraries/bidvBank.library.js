const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');

const GeneralConstant = require('../constants/general.constant');
const BankServiceConstant = require('../constants/bankService.constant');

function BIDVBankLibrary(config = {}) {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.BIDVBANK_TOKEN;

	const Logger = config.logger;

	async function PostRequest(urlRequest, body, headers = {}) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({
					url: urlRequest,
					timeout: 50000,
					body: JSON.stringify(body),
					headers: {
						Authorization,
						...headers
					}
				}, (err, httpResponse, response) => {
					if (err) {
						reject(err);
					}
					resolve(httpResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}

	return {
		async Withdraw(payload) {
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Rút tiền về tài khoản BIDV thất bại. Vui lòng thử lại sau',
				transaction: null,
				supplierResponse: null
			};

			const params = {
				trans_id: payload.transaction,
				trans_desc: payload.description || 'Rut tien ve Bank',
				amount: payload.amount,
				account_id: payload.accountId,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};

			let urlRequest = null;
			if (payload.version === 2) {
				urlRequest = `${url}/v2/bidv/wallet/wallet_to_bank`;
			} else {
				urlRequest = `${url}/v1/bidv/wallet/wallet_to_bank`;
			}

			try {
				const result = await PostRequest(urlRequest, params);
				Logger(`[BIDVBANK] => [DEPOSIT] => [Params] : ${JSON.stringify({ urlRequest, params })}`);
				Logger(`[BIDVBANK] => [DEPOSIT] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				response.supplierResponse = _.get(result, 'body', result);
				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Rút tiền thất bại. Vui lòng thử lại sau');
					if (response.message === 'ESOCKETTIMEDOUT') {
						response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					}
					return response;
				}

				response.transaction = _.get(bodyResponse, 'data.trans_id', '');
				response.message = 'Nạp tiền thành công.';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[BIDVBANK_LIBRARY] => [Deposit: ${urlRequest}] => [Error] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async Transfer(params) {
			const result = {
				code: -1,
				data: {},
				original: null
			};
			// {
			// "trans_id": "123",
			// "trans_desc": "Rut tien ve bank",
			// "amount": 10000,
			// "account_id": 123456,
			//   "sign_type": "RSA",
			// "sign": "xxxxxxxx"
			// }
			try {
				const { version } = params;
				delete params.version;

				let responseRaw;
				if (version === 2) responseRaw = await this.PostApi('v2/bidv/wallet/wallet_to_bank', params);
				else responseRaw = await this.PostApi('v1/bidv/wallet/wallet_to_bank', params);
				const response = responseRaw.data;
				if (response.code !== 1000 && response.code !== 2055) {
					if (response.code === 2053) {
						response.code = -2;
					}
					result.data = response.data;
					result.original = response;
					return result;
				}
				result.code = 1;
				result.data = response.data;
				result.original = response;
				return result;
			} catch (err) {
				Logger(`[BIDV Bank Libs] Transfer Error ${err}`);
			}
			return result;
		},
		async Transfer247(payload) {
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Rút tiền về tài khoản liên kết thất bại. Vui lòng thử lại sau',
				transaction: null,
				supplierResponse: null
			};

			const params = {
				trans_id: payload.transaction,
				trans_desc: payload.description || 'Rut tien ve tai khoan ngan hang',
				card_number: payload.cardNumber || payload.accountNumber,
				bank_code: payload.bankCode,
				amount: payload.amount,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};

			const urlRequest = `${url}/v1/bidv/wallet/tranfer_to_bank_247`;

			try {
				const result = await PostRequest(urlRequest, params);
				response.supplierResponse = _.get(result, 'body', result);
				Logger(`[BIDVBANK] => [TRANSFER_247] => [Params] : ${JSON.stringify({ urlRequest, params })}`);
				Logger(`[BIDVBANK] => [TRANSFER_247] => [Response] : ${JSON.stringify({ urlRequest, params: _.get(result, 'body', result) })}`);

				const bodyResponse = JSON.parse(result.body);
				Logger(bodyResponse);
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Rút tiền thất bại. Vui lòng thử lại sau');
					if (response.message === 'ESOCKETTIMEDOUT') {
						response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					}
					return response;
				}

				response.transaction = _.get(bodyResponse, 'data.trans_id', '');
				response.message = 'Rút tiền thành công.';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[BIDVBANK_LIBRARY] => [TRANSFER_247: ${urlRequest}] => [Error] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async TransferBIDV(payload) {
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Rút tiền về tài khoản liên kết thất bại. Vui lòng thử lại sau',
				transaction: null,
				supplierResponse: null
			};

			const params = {
				trans_id: payload.transaction,
				trans_desc: payload.description,
				card_number: payload.cardNumber || payload.accountNumber,
				amount: payload.amount,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};

			const urlRequest = `${url}/v1/bidv/wallet/tranfer_to_bank_bidv`;

			try {
				const result = await PostRequest(urlRequest, params);
				Logger(`[BIDVBANK] => [TRANSFER_TO_BIDV] => [Params] : ${JSON.stringify({ urlRequest, params })}`);
				Logger(`[BIDVBANK] => [TRANSFER_TO_BIDV] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				response.supplierResponse = _.get(result, 'body', result);
				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Rút tiền thất bại. Vui lòng thử lại sau');
					if (response.message === 'ESOCKETTIMEDOUT') {
						response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					}
					return response;
				}

				response.transaction = _.get(bodyResponse, 'data.trans_id', '');
				response.message = 'Rút tiền thành công.';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[BIDVBANK_LIBRARY] => [TRANSFER_TO_BIDV: ${urlRequest}] => [Error] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async GetBankNameDomestic(payload) {
			const response = {
				state: BankServiceConstant.GET_NAME_STATE.FAILED,
				message: null
			};
			// const example = {
			//   cardNumber: '19910000015543'
			// };
			try {
				const params = {
					cardNumber: payload.cardNumber || payload.accountNumber
				};
				Logger('[BIDV Bank Libs] GetBankNameDomestic Payload: ', JSON.stringify(params));
				const responseRaw = await PostRequest('v1/bidv/wallet/get_name_bidv', params);
				const bidvResponsed = responseRaw.data;
				Logger('[BIDV Bank Libs] GetBankNameDomestic Response:', JSON.stringify(bidvResponsed));
				if (bidvResponsed.code !== 1000) {
					response.message = _.get(bidvResponsed, 'data.message', null) || 'Lấy thông tin thất bại';
					return response;
				}
				response.fullname = _.get(bidvResponsed, 'data.card_name', null);
				response.state = BankServiceConstant.GET_NAME_STATE.SUCCEEDED;
				return response;
			} catch (err) {
				Logger(err);
				response.state = BankServiceConstant.STATE.FAILED;
				response.message = 'Lấy thông tin thất bại';
				return response;
			}
		},
		async GetBankName247(payload) {
			const response = {
				state: BankServiceConstant.STATE.FAILED,
				message: null
			};

			// validate params
			try {
				const params = {
					cardNumber: payload.cardNumber || payload.accountNumber,
					bankCode: payload.bankCode
				};

				Logger('[BIDV Bank Libs] GetBankName247 Payload: ', JSON.stringify(params));
				const responseRaw = await this.PostApi('v1/bidv/wallet/get_name_247', params);
				const bidvResponsed = responseRaw.data;
				Logger('[BIDV Bank Libs] GetBankName247 Response:', JSON.stringify(bidvResponsed));
				if (response.code !== 1000) {
					response.message = _.get(bidvResponsed, 'data.message', null) || 'Lấy thông tin thất bại';
					return response;
				}
				response.fullname = _.get(bidvResponsed, 'data.card_name', null);
				response.state = BankServiceConstant.GET_NAME_STATE.SUCCEEDED;
				return response;
			} catch (error) {
				Logger(error);
				response.state = BankServiceConstant.STATE.FAILED;
				response.message = 'Lấy thông tin thất bại';
				return response;
			}
		}
	};
}
module.exports = BIDVBankLibrary;
