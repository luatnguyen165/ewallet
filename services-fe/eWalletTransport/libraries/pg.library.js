const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const Moment = require('moment');
const MeAPI = require('./crypto.library');
const GeneralConstant = require('../constants/general.constant');

const RequestFunctions = {
	async requestPost(body, accessToken, headers = {}, url) {
		const result = {
			code: -1,
			data: {},
			original: null
		};
		try {
			const uri = `${process.env.GAPI_URL}${url}`;

			console.log(`PG - PayMEService --- REQUEST --- url = ${uri} with params = ${JSON.stringify(body)}`);
			const meApi = new MeAPI({
				url: process.env.GAPI_URL,
				isSecurity: 'true',
				privateKey: process.env.PAYOUT_PRIVATE_KEY,
				publicKey: process.env.PAYOUT_PUBLIC_KEY,
				'x-api-client': process.env.PAYOUT_CLIENT_APP
			});

			const response = await meApi.Post(url, body, '', headers);
			// console.log(`PG - PayMEService --- RESPONSE --- url = ${uri}  with data = ${JSON.stringify(response)}`);
			if (response.code !== 1) {
				return response;
			}
			result.code = response.code;
			result.data = response.data;
			result.original = response.original;
			return result;
		} catch (error) {
			throw new MoleculerError(error.message);
		}
	}
};

function PayMeLibrary(config = {}) {
	/**
	 * PayOut
	 */
	const Logger = config.logger;

	return {
		async Payout(payload) {
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Rút tiền về tài khoản ngân hàng thất bại. Vui lòng thử lại sau',
				transaction: null,
				supplierResponse: null
			};
			try {
				// const params = {
				// 	partnerTransaction: payload.transaction,
				// 	amount: payload.amount,
				// 	destination: {
				// 		bankAccount: {
				// 			accountNumber: payload.accountNumber,
				// 			accountName: payload.accountName,
				// 			swiftCode: payload.swiftCode
				// 		}
				// 	},
				// 	type: 'PAYMENT',
				// 	content: payload.description || 'Rut tien ve the ngan hang',
				// 	ipnUrl: `${process.env.PAYME_URL}/v3/Payout/IPN`
				// };
				const uri = '/payout';
				const urlRequest = `${process.env.GAPI_URL}${uri}`;

				Logger(`[PAYOUT] => [Params] : ${JSON.stringify({ urlRequest, payload })}`);
				const result = await RequestFunctions.requestPost(payload, '', {}, uri);
				Logger(`[PAYOUT] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				if (_.get(result, 'data.code', 1) === 132000) {
					response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;
				}
				response.transaction = _.get(result, 'data.data.transactionId', null);
				response.supplierResponse = JSON.stringify(result);
				return response;
			} catch (err) {
				response.code = err.code;
				response.data.message = `[PayME] ${err.message}`;
				response.original = err.data;

				return response;

				// throw new MoleculerError(`v1.payME.library.PG error: ${err}`, 500);
			}
		}
	};
}

module.exports = PayMeLibrary;
