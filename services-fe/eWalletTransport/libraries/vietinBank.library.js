const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');
const GeneralConstant = require('../constants/general.constant');

function VietinBankLibrary(config = {}) {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.VIETINBANK_TOKEN;

	const Broker = config.broker;
	const Logger = config.logger;

	async function PostRequest(urlRequest, body, headers = {}) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({
					url: urlRequest,
					timeout: 50000,
					body: JSON.stringify(body),
					headers: {
						Authorization,
						...headers
					}
				}, (err, httpResponse, response) => {
					if (err) {
						reject(err);
					}
					resolve(httpResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}
	return {
		async Withdraw(payload) {
			// Response:  code - message - state - transaction - html
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Rút tiền về VietinBank thất bại. Vui lòng thử lại sau.',
				transaction: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/wallet/vietin/token/withdraw`;
			const body = {
				trans_id: payload.transaction,
				account_id: _.toString(payload.accountId),
				card_id: _.parseInt(_.get(payload, 'cardId', 0)),
				amount: payload.amount,
				channel: 'MOBILE',
				language: 'vi',
				trans_desc: 'Rút tiền từ Ví PayME.',
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			try {
				const result = await PostRequest(urlRequest, body);

				Logger(`[VIETINBANK_LIBRARY] => [Withdraw: ${urlRequest}] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
				Logger(`[VIETINBANK_LIBRARY] => [Withdraw: ${urlRequest}] => [Response] : ${JSON.stringify({ urlRequest, result })}`);
				response.supplierResponse = result.body || result;
				const bodyResponse = JSON.parse(result.body);

				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Rút tiền thất bại. Vui lòng thử lại sau.');
					if (response.message === 'ESOCKETTIMEDOUT') {
						response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					}
					return response;
				}
				response.transaction = _.get(bodyResponse, 'data.trans_id', body.trans_id);
				response.message = 'Thanh toán thành công.';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[VIETINBANK_LIBRARY] => [Deposit: ${urlRequest}] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}

	};
}
module.exports = VietinBankLibrary;
