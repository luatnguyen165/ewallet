const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const _ = require('lodash');

const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		default: null
	},
	isBroadcast: {
		type: Boolean,
		default: false
	},
	type: {
		type: String
	},
	isRead: {
		type: Boolean,
		default: false
	},
	isDeleted: {
		type: Boolean,
		default: false
	},
	message: {
		type: String,
		required: true
	},
	extraData: {
		type: Object,
		default: {}
	},
	notiType: {
		type: String,
		enum: _.values(GeneralConstant.TYPE)
	},
	content: {
		url: String,
		redirectSchema: mongoose.Schema.Types.Mixed,
		title: String
	}
}, {
	collection: 'Notification',
	versionKey: false,
	timestamps: true
});

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.index({ accountId: 1 }, { sparse: false, unique: false });

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
