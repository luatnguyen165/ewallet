module.exports = {
	TYPE: {
		PERSONAL: 'PERSONAL',
		EVENT: 'EVENT',
		SYSTEM: 'SYSTEM',
		ALL: 'ALL',
		NEWS: 'NEWS'
	},
	INSIGHT_TYPE: {
		EVENT: 'EVENT',
		SYSTEM: 'SYSTEM',
		NEWS: 'NEWS'
	},
	ACTION_TYPE: {
		OPEN_SCREEN: 'openScreen',
		OPEN_POPUP: 'openPopUp',
		COPY: 'copy',
		WEB_BROWSER: 'webBrowser',
		SHARE: 'share',
		TOPUP: 'topup'
	},
	SCREEN: {
		HOME: {
			id: 1,
			screenName: 'Home'
		}
	},
	TAGS: {
		AIZEN: 'AIZEN'
	}
};
