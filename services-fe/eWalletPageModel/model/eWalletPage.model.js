const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const generalConstant = require('../constants/general.constant');
const _ = require('lodash');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	alias: {
		type: String,
		required: true,
		unique: true
	},
	title: {
		type: String,
		required: true
	},
	image: {
		avatar: {
			type: String,
			default: null
		},
		cover: {
			type: String,
			default: null
		}
	},
	state: {
		type: String,
		enum: _.values(generalConstant.STATE),
		default: generalConstant.STATE.NEW
	},
	statistics: {
		aliasUpdateTimes: {
			type: Number,
			default: 0
		},
		aliasUpdatedAt: {
			type: Date,
			default: null
		},
		//
		titleUpdateTimes: {
			type: Number,
			default: 0
		},
		titleUpdatedAt: {
			type: Date,
			default: null
		},
		//
		coverUpdateTimes: {
			type: Number,
			default: 0
		},
		coverUpdatedAt: {
			type: Date,
			default: null
		}
	},
	rejectReason: {
		alias: { type: String, default: null },
		title: { type: String, default: null },
		image: { type: String, default: null }
	}
}, {
	collection: 'Page',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ alias: 1 }, { sparse: false, unique: true });;

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}_id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
