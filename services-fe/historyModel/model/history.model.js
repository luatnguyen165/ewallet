const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const generalConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	partnerId: {
		type: Number,
		default: null
	},
	appId: {
		type: Number,
		default: null
	},
	accountId: {
		type: Number,
		required: true
	},
	service: {
		code: {
			type: String
		},
		type: {
			type: String
		},
		name: {
			type: String,
			default: null
		},
		id: { // ID tới record trong bảng service
			type: Number
		},
		serviceAt: { // thời gian xem tạo service để refund
			type: Date,
			// default: moment(new Date()).utc()
			default: null
		},
		method: {
			type: String,
			default: null
		},
		serviceMethod: {
			id: { // ID tới record trong bảng Payment_Method
				type: Number
			},
			registerId: {
				type: String
			},
			group: {
				type: String
			},
			info: {
				type: Object
			},
			description: {
				type: String
			}
		},
		transaction: {
			type: String
		},
		state: { // serviceState
			type: String
		},
		data: {
			type: Object
		}
	},
	method: {
		id: { // ID tới record trong bảng Payment_Method
			type: Number
		},
		registerId: {
			type: String
		},
		group: {
			type: String
		},
		info: {
			type: Object
		},
		description: {
			type: String
		},
		bankInfo: {
			type: Object
		}
	},
	payment: {
		id: {
			type: Number,
			default: 0
		},
		transaction: {
			type: String,
			default: null
		},
		method: {
			type: String,
			default: null
		},
		state: {
			type: String,
			default: null
		},
		description: {
			type: String,
			default: null
		},
		extraData: {
			// nếu payment có extra thì buộc có resolve type
			resolveType: {
				type: String
			},
			// value này chỉ có khi payment là từ tín dụng và có type là ExtraPaymentPaymeCredit
			paymeCreditNo: {
				type: Number
			},
			packageId: {
				type: Number
			}
		}
	},
	transport: {
		accountId: { type: Number },
		transportId: { type: Number },
		transaction: { type: String },
		state: { type: String },
		method: { type: String },
		description: { type: String }
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	fee: {
		type: Number,
		default: 0
	},
	discount: {
		type: Number,
		default: 0
	},
	bonus: {
		type: Number,
		default: 0
	},
	cashback: {
		type: Number,
		default: 0
	},
	total: {
		type: Number,
		default: 0
	},
	state: { // transactionState
		type: String,
		enum: _.values(generalConstant.HISTORY_STATE),
		default: null
	},
	description: {
		type: String,
		default: null
	},
	changed: {
		type: String,
		enum: ['+', '-', '', null],
		default: ''
	},
	tags: [String],
	extraData: Object,
	isVisible: {
		type: Boolean,
		default: true
	},
	via: {
		type: String,
		default: null
	},
	balance: {
		before: { // số dư ví trước khi giao dịch
			type: Number,
			default: null
		},
		after: { // số dư ví sau khi giao dịch
			type: Number,
			default: null
		}
	},
	publishedAt: {
		type: Date,
		default: null
	}
}, {
	collection: 'History',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ 'service.code': 1 }, { sparse: true, unique: false });
Schema.index({ 'service.state': 1 });
Schema.index({ state: 1 });
Schema.index({ 'service.type': 1 });
Schema.index({ 'service.transaction': 1 });
Schema.index({ changed: 1 });
Schema.index({ tag: 1 });
Schema.index({ id: 1 });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
