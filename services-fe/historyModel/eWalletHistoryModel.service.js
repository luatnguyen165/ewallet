const DbService = require('moleculer-db');
const MongooseAdapter = require('moleculer-db-adapter-mongoose');
const MongooseAction = require('moleculer-db-adapter-mongoose-action');
const _ = require('lodash');
const historyModel = require('./model/history.model');

module.exports = {
	name: 'eWalletHistoryModel',

	version: 1,

	mixins: [DbService],

	adapter: new MongooseAdapter(process.env.FE_MONGO_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		keepAlive: true

	}),

	model: historyModel,

	/**
   * Settings
   */
	settings: {
	},

	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: MongooseAction({
		distinct: {
			rest: false,
			cache: false,
			params: {
				fieldName: 'string',
				query: {
					$$type: 'object|optional|default:{}'
				}
			},
			handler: async (ctx) => {
				const { fieldName, query } = ctx.params;
				const result = await historyModel.distinct(fieldName, query);
				return result;
			}
		}
	}),

	/**
   * Events
   */
	events: {

	},

	/**
   * Methods
   */
	methods: {
	},

	/**
   * Service created lifecycle event handler
   */
	created() {

	},

	/**
   * Service started lifecycle event handler
   */
	started() { },

	/**
   * Service stopped lifecycle event handler
   */

	// async stopped() {},

	async afterConnected() {
		this.logger.info('Connected successfully...');
	}
};
