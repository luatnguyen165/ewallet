module.exports = {
	PHONE_CONSTANT: {
		VIETTEL: ['086', '096', '097', '098', '032', '033', '034', '035', '036', '037', '038', '039'],
		MOBIFONE: ['089', '090', '093', '070', '079', '077', '076', '078'],
		VINAPHONE: ['088', '091', '094', '083', '084', '085', '081', '082'],
		VIETNAMOBILE: ['092', '056', '058', '052'],
		GMOBILE: ['099', '059'],
		SFone: ['095'],
		ITELECOM: ['087']
	},
	ACCOUNT_TYPE: {
		PERSONAL: 'PERSONAL',
		BUSINESS: 'BUSINESS'
	},
	SUPPLIER_STATE :{
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		CANCELED: 'CANCELED',
		FAILED: 'FAILED',
		TIMEOUT: 'TIMEOUT'
	},
	OCB_MESSAGE: {
		0: 'Không nhận được kết quả từ phía ngân hàng. Vui lòng thử lại sau ít phút.',
		400: 'Thông tin thẻ không đúng. Vui lòng thử lại!',
		406: 'Đã có lỗi xảy ra. Vui lòng thử lại sau ít phút.',
		504: 'Không nhận được kết quả từ phía ngân hàng. Vui lòng thử lại sau ít phút.',
		41701: 'Có lỗi xảy ra từ phía ngân hàng. Vui lòng thử lại sau ít phút.',
		41704: 'Có lỗi xảy ra từ phía ngân hàng. Vui lòng thử lại sau ít phút.',
		41706: 'Số tiền giao dịch vượt giới hạn cho phép của 1 ngày!',
		41707: 'Số tiền giao dịch vượt giới hạn cho phép!',
		41708: 'Số tiền giao dịch nhỏ hơn hạn mức tối thiểu. Vui lòng thử lại.',
		41709: 'Giao dịch lỗi! Vui lòng kiểm tra lại thông tin.',
		41714: 'Tài khoản không đủ số dư!',
		41715: 'Giao dịch lỗi! Vui lòng thử lại sau ít phút!',
		41716: 'Tài khoản bị khoá! Vui lòng kiểm tra lại thông tin!',
		41721: 'Thẻ đã được liên kết với tài khoản khác!',
		41727: 'Không tìm thấy tài khoản liên kết ví!',
		41730: 'Không hỗ trợ loại xác thực!',
		41731: 'Giao dịch không hợp lệ!',
		41732: 'Tài khoản không hợp lệ, hoặc đang bị khoá/ bị huỷ. Vui lòng kiểm tra lại!',
		41742: 'Thẻ /tài khoản không hợp lệ, hoặc bị khoá, bị huỷ!',
		41743: 'Số thẻ (hoặc tài khoản) không trùng với tên người dùng. Vui lòng kiểm tra lại!',
		41744: 'Thông tin số giấy tờ (CMND/CCCD) không tồn tại bên ngân hàng OCB. Vui lòng kiểm tra lại thông tin.',
		41745: 'Thông tin thẻ không đúng. Vui lòng kiểm tra lại thông tin!',
		41746: 'Gói tài khoản OMNI OCB không hợp lệ. Vui lòng liên hệ với ngân hàng OCB. Xin cảm ơn.',
		41747: 'Tài khoản ngân hàng chưa đăng kí OMNI OCB. Vui lòng liên hệ với ngân hàng OCB để liên kết ví. Xin cảm ơn.',
		41748: 'Tài khoản OMNI OCB không hợp lệ. Vui lòng liên hệ với ngân hàng OCB. Xin cảm ơn.',
		41749: 'Số điện thoại không hợp lệ!',
		41754: 'Số thẻ không hợp lệ.',
		// OTP Sai
		41723: 'Lỗi khi tạo OTP. Vui lòng thử lại!',
		41724: 'Mã OTP không đúng!',
		41725: 'Lỗi khi liên kết ví. Vui lòng thử lại sau!',
		41726: 'OTP đã hết hạn!'
	},
	TRANSPORT_STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED',
		TIME_OUT: 'TIME_OUT',
		REFUNDED: 'REFUNDED',
		CANCELED: 'CANCELED',
		INVALID_PARAMS: 'INVALID_PARAMS'
		// ...
	},
	REFUND_STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED',
		TIME_OUT: 'TIME_OUT',
		REFUNDED: 'REFUNDED',
		CANCELED: 'CANCELED',
		INVALID_PARAMS: 'INVALID_PARAMS'
		// ...
	},
	PAYMENT_STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED',
		TIME_OUT: 'TIME_OUT',
		REFUNDED: 'REFUNDED',
		CANCELED: 'CANCELED',
		INVALID_PARAMS: 'INVALID_PARAMS'
		// ...
	}
};
