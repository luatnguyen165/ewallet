const DbService = require('moleculer-db');
const moleculerI18n = require('moleculer-i18n-js');
const path = require('path');
const _ = require('lodash');

// Library
const PVComBankLibrary = require('./libraries/pvcomBank.library');
const VietinBankLibrary = require('./libraries/vietinBank.library');
const OCBBankLibrary = require('./libraries/ocbBank.library');
const BIDVBankLibrary = require('./libraries/bidvBank.library');
const WalletLibrary = require('./libraries/wallet.library');
const PGLibrary = require('./libraries/pg.library');
const BSLibrary = require('./libraries/bs.library');

module.exports = {
	name: 'eWalletWithdraw',

	version: 1,

	mixins: [moleculerI18n, DbService],
	i18n: {
		directory: path.join(__dirname, 'locales'),
		locales: ['vi', 'en'],
		defaultLocale: 'vi'
	},
	/**
   * Settings
   */
	settings: {
	},

	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		withdrawBE: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/withdraw/linked',
				auth: false
			},
			params: {
				body: {
					$$type: 'object',
					phone: 'string',
					linkedId: 'number',
					amount: 'number'

				}
			},
			timeout: 30000,
			handler: require('./actions/withdraw.action.rest')
		},
		verifyWithdrawBE: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/withdraw/verifyLinked',
				auth: false
			},
			params: {
				body: {
					$$type: 'object',
					otp: 'string',
					phone: 'string',
					// linkedId: 'number',
					transaction: 'string'
				}
			},
			timeout: 30000,
			handler: require('./actions/verifyWithdraw.action.rest')
		},
		transportLinked: {
			params: {
				linkedId: 'number',
				accountId: 'number',
				transaction: 'string',
				amount: 'number',
				appId: 'number',
				phone: 'string|optional'
			},
			timeout: 30000,
			handler: require('./actions/transportLinked.action')
		},
		bankBalancerTransfer: {
			params: {
				transaction: 'string', // Transaction khi nguoi dung thuc hien giao dich
				amount: 'number',
				description: 'string|optional',
				bankInfo: {
					$$type: 'object',
					method: 'string',
					cardNumber: 'string|optional',
					accountNumber: 'string|optional',
					fullname: 'string|optional',
					swiftCode: 'string|optional'
				}
			},
			timeout: 30000,
			handler: require('./actions/bankBalancerTransfer.action')
		},
		refund: {
			params: {
				paymentId: 'number', // Transaction khi nguoi dung thuc hien giao dich
				appId: 'number',
				amount: 'number',
				description: 'string|optional',
				serviceCode: 'string',
				serviceId: 'number'
			},
			timeout: 30000,
			handler: require('./actions/refund.action')
		},
		withdrawSDK: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/sdk/withdraw',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					remember: 'boolean|default:false',
					amount: 'number',
					payment: {
						$$type: 'object',
						wallet: {
							$$type: 'object|optional',
							securityCode: 'string'
						}
					},
					transport: {
						$$type: 'object',
						bankAccount: {
							$$type: 'object|optional',
							bankAccountNumber: 'string',
							bankAccountName: 'string',
							swiftCode: 'string'
						},
						bankCard: {
							$$type: 'object|optional',
							cardNumber: 'string',
							cardHolder: 'string',
							swiftCode: 'string'
						},
						linked: {
							$$type: 'object|optional',
							linkedId: 'number'
						}
					}
				}
			},
			handler: require('./actions/withdrawSDK.action.rest')
		}
	},

	/**
		* Events
		*/
	events: {

	},

	/**
	* Methods
	*/
	methods: {
		GetAuthInfo: require('./methods/getAuthInfo.method'),
		RedisCache: require('./methods/redisCache.method'),
		PayWallet: require('./methods/payWallet.method'),
		CalcFee: require('./methods/calcFee.method'),
		CardDescription: require('./methods/cardDescription.method'),
		PhoneUtility: require('./methods/phoneUtility.method'),
		CheckLimitWithdrawOCB: require('./methods/checkLimitWithdrawOCB.method'),
		BankBalancerSupplierSelect: require('./methods/bankBalancerSupplierSelect.method'),
		BankTransferService: require('./methods/BankTransferService.method'),
		RemoveUnicode: require('./methods/removeUnicode.method')
	},
	/**
	* Service created lifecycle event handler
	*/
	created() {
		try {
			_.set(this, 'PVComBankLibs', PVComBankLibrary({ logger: this.logger.info }));
			_.set(this, 'VietinBankLibs', VietinBankLibrary({ logger: this.logger.info }));
			_.set(this, 'OCBBankLibs', OCBBankLibrary({ logger: this.logger.info }));
			_.set(this, 'BIDVBankLibs', BIDVBankLibrary({ logger: this.logger.info }));
			_.set(this, 'WalletLibs', WalletLibrary({ logger: this.logger.info }));
			_.set(this, 'PGLibs', PGLibrary({ logger: this.logger.info }));
			_.set(this, 'BSLibs', BSLibrary({ logger: this.logger.info }));
		} catch (error) {
			this.logger.info(`[Generate_Library_Error] => ${error}`);
		}
	},

	/**
   * Service started lifecycle event handler
   */
	async started() {
	},

	/**
   * Service stopped lifecycle event handler
   */

	// async stopped() {},

	async afterConnected() {
		this.logger.info('Connected successfully...');
	}
};
