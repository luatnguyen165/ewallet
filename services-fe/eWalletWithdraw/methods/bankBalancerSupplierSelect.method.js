const _ = require('lodash');

module.exports = function (data) {
	if (!_.isObject(data)) {
		return '';
	}
	const allKeys = _.keys(data);
	const randomValue = _.random(0, 999);
	let temp = 0;
	// eslint-disable-next-line no-plusplus
	for (let i = 0; i < allKeys.length; i++) {
		temp += (data[allKeys[i]] * 10);
		if ((randomValue - temp) < 0) {
			return _.toUpper(allKeys[i]);
		}
	}
	return '';
};
