const moment = require('moment');
const _ = require('lodash');
const numeral = require('numeral');
const WithdrawConstant = require('../constants/withdraw.constant');

const ModuleConstant = {
	LIMIT_AMOUNT_IN_1_DAY: 100000000
};
const response = {
	state: WithdrawConstant.WITHDRAW_STATE.SUCCEEDED,
	message: 'Thành công'
};

module.exports = async function (accountId, amount) {
	const currentDay = moment();
	const startOfDay = currentDay.clone().startOf('day').toDate();
	const endOfDay = currentDay.clone().endOf('day').toDate();
	const sumAmountTransaction = await this.broker.call('v1.eWalletTransportLinkedModel.aggregate', [[
		{
			$match: {
				accountId,
				state: WithdrawConstant.WITHDRAW_STATE.SUCCEEDED,
				'bankInfo.swiftCode': 'ORCOVNVX',
				createdAt: {
					$gte: startOfDay,
					$lte: endOfDay
				}
			}
		},
		{
			$group: {
				_id: null,
				amount: { $sum: '$amount' }
			}
		},
		{
			$project: {
				_id: 0,
				amount: '$amount'
			}
		}
	]]);

	let totalInCurrentDay = _.get(sumAmountTransaction, '[0]', null);
	// Neu ngay hom nay chua co giao dich -> amount la 0
	if (_.isNull(totalInCurrentDay)) {
		totalInCurrentDay = { amount: 0 };
	}

	if (totalInCurrentDay.amount + _.toNumber(amount) > ModuleConstant.LIMIT_AMOUNT_IN_1_DAY) {
		response.state = WithdrawConstant.WITHDRAW_STATE.FAILED;
		response.message = `Không thể giao dịch quá ${numeral(ModuleConstant.LIMIT_AMOUNT_IN_1_DAY).format('0,0')} VND/ngày.`;
		return response;
	}

	return response;
};
