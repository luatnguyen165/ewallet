const _ = require('lodash');

const TransportConstant = require('../constants/transport.constant');
const BankServiceConstant = require('../constants/bankService.constant');

const CONSTANTS = {
	KEY_CONFIG: 'cash.source.supplier',
	UUID_PREFIX: 'TRANSFER_BANK_TO_BANK',
	KEY_AMOUNT_OCB: 'min.amount.ocb.banktobank',
	KEY_AMOUNT_OTHER: 'min.amount.transfer.banktobank',
	RANDOM: 'bank.transfer.rate'
};

const Private = {
	// data is json object
	randomBankTransfer(data) {
		if (!_.isObject(data)) {
			return '';
		}
		const allKeys = _.keys(data);
		const randomValue = _.random(0, 999);
		let temp = 0;
		for (let i = 0; i < allKeys.length; i += 1) {
			temp += (data[allKeys[i]] * 10);
			if ((randomValue - temp) < 0) {
				return _.toUpper(allKeys[i]);
			}
		}
		return '';
	}
};
module.exports = async function BankTransferService() {
	const GetBankName = async (payload) => {
		const response = {
			code: -1,
			state: BankServiceConstant.STATE.FAILED,
			message: null
		};

		const BankCodeConfig = await this.broker.call('v1.eWalletBankCodeModel.findMany', [{ isActive: true }]);
		const bankInfo = _.find(BankCodeConfig, { swiftCode: payload.swiftCode });
		if (_.isNil(bankInfo)) {
			response.message = 'Ngân hàng không đươc hỗ trợ';
			return response;
		}

		const getBankCode = _.get(bankInfo, 'card.atm.prefix', '');
		let { supplier } = payload;
		if (_.isNil(supplier)) {
			let dataRandomSupplier = await this.broker.call('v1.eWalletSetting.getSettingConfig', { body: { keys: [CONSTANTS.RANDOM] } });

			if (_.get(dataRandomSupplier, 'code', null) === 202500 && !_.isEmpty(dataRandomSupplier.data) && dataRandomSupplier.data[0]) {
				dataRandomSupplier = dataRandomSupplier.data[0].value;
				const JsonData = JSON.parse(dataRandomSupplier);
				supplier = Private.randomBankTransfer(JsonData);
			}
		}

		if (_.isNil(supplier)) {
			supplier = BankServiceConstant.SUPPLIER.BS;
		}
		let result = null;
		switch (supplier) {
			// BIDV
			case BankServiceConstant.SUPPLIER.BIDV: {
				// Chuyen den ngan hang BIDV
				if (bankInfo.swiftCode === 'BIDVVNVX') {
					const paramsSend = {
						cardNumber: payload.cardNumber,
						accountNumber: payload.bankAccountNumber,
						swiftCode: payload.swiftCode
					};
					result = await this.BIDVBankLibrary.GetBankNameDomestic(paramsSend);
				} else {
					const paramsSend = {
						cardNumber: payload.cardNumber,
						accountNumber: payload.bankAccountNumber,
						bankCode: getBankCode,
						swiftCode: payload.swiftCode
					};
					result = await this.BIDVBankLibrary.GetBankName247(paramsSend);
				}
				break;
			}
			case BankServiceConstant.SUPPLIER.PG: {
				const paramsSend = {
					cardNumber: payload.cardNumber || payload.bankAccountNumber,
					swiftCode: payload.swiftCode
				};
				result = await this.broker.call('v1.utility.detectBankAccount', { body: paramsSend });
				if (result.code === 113400) {
					result.state = BankServiceConstant.GET_NAME_STATE.SUCCEEDED;
				}
				// Chuyen den ngan hang BIDV
				break;
			}

			// BS
			case BankServiceConstant.SUPPLIER.OCB:
			case BankServiceConstant.SUPPLIER.BS: {
				const isGetCardName = !_.isNil(payload.cardNumber);
				if (isGetCardName) {
					const paramsSend = {
						cardNumber: payload.cardNumber,
						swiftCode: payload.swiftCode
					};
					result = await this.BSLibs.GetBankCardName(paramsSend);
				} else {
					const paramsSend = {
						bankAccountNumber: payload.bankAccountNumber,
						swiftCode: payload.swiftCode
					};
					result = await this.BSLibs.GetBankAccountName(paramsSend);
				}

				break;
			}
			default: break;
		}

		response.data = {
			message: _.get(result, 'message', null),
			fullname: _.get(result, 'fullname', null)
		};
		response.state = _.get(result, 'state', BankServiceConstant.GET_NAME_STATE.FAILED);
		response.code = result.state === BankServiceConstant.GET_NAME_STATE.SUCCEEDED ? 1 : -1;
		response.original = _.get(result, 'supplierResponsed', null);
		return response;
	};
	const GetSupplier = async () => {
		let result = null;

		let JsonData;
		let dataRandomSupplier = await this.broker.call('v1.eWalletSetting.getSettingConfig', { body: { keys: [CONSTANTS.RANDOM] } });

		if (_.get(dataRandomSupplier, 'code', null) === 202500 && !_.isEmpty(dataRandomSupplier.data) && dataRandomSupplier.data[0]) {
			dataRandomSupplier = dataRandomSupplier.data[0].value;
			JsonData = JSON.parse(dataRandomSupplier);
		}
		const supplier = Private.randomBankTransfer(JsonData);

		if (supplier !== '') { result = supplier; }
		return result;
	};
	return {
		GetBankName,
		GetSupplier
	};
};
