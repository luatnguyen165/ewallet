/* eslint-disable object-curly-newline */
const _ = require('lodash');
const PhoneConstant = require('../constants/general.constant');

module.exports = function (phone) {
	const phoneFormat = phone.replace(/[^+0-9]/g, '').replace(/^84/, '0');
	const result = {
		phone: phoneFormat,
		telco: null,
		isValid: false
	};
	if ((result.phone).length !== 10 || _.isNaN(result.phone) === true) {
		return result;
	}
	const telco = _.findKey(PhoneConstant.PHONE_CONSTANT, (v) => _.includes(v, (_.replace(result.phone, /^84/, '0')).substring(0, 3)) || null);
	if (!telco) {
		return result;
	}
	result.telco = telco.toLowerCase();
	const phoneFormatted = phone.replace(/[^+0-9]/g, '').replace(/^00/, '+').replace(/^0/, '84');
	result.phoneFormatted = phoneFormatted;
	result.isValid = true;
	return result;
};
