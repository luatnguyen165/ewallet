const _ = require('lodash');
const moment = require('moment');
const Numeral = require('numeral');
const Decimal = require('decimal.js');
const { MoleculerError } = require('moleculer').Errors;
const ErrorCode = require('../constants/errorCode.constant');
const WithdrawConstant = require('../constants/withdraw.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	const response = {
		code: ErrorCode.WITHDRAW_CODE.FAILED,
		message: 'Rút tiền thất bại. Vui lòng thử lại sau.(E00)'
	};
	try {
		const {
			phone,
			linkedId,
			amount
		} = _.get(ctx, 'params.body');
		const payload = _.get(ctx, 'params.body');

		const authInfo = this.GetAuthInfo(ctx.meta);

		Logger(`[EWALLET_WITHDRAW] => [payload] => ${JSON.stringify(_.get(ctx, 'params.body'))}`);

		const eWalletAppInfo = await Broker.call('v1.ewalletAppInfoModel.findOne', [{
			merchantId: authInfo.merchantId
		}]);

		if (_.get(eWalletAppInfo, 'id', null) === null) {
			response.message = 'Không tìm thấy thông tin kết nối';
			return response;
		}

		const checkPhone = this.PhoneUtility(phone);
		if (!checkPhone.isValid) {
			response.message = 'Số điện thoại không hợp lệ';
			return response;
		}
		const { phoneFormatted } = checkPhone;

		const connectedUserInfo = await Broker.call('v1.eWalletConnectedUserModel.findOne', [{
			phone: phoneFormatted,
			appId: eWalletAppInfo.id
		}]);

		if (_.get(connectedUserInfo, 'id', null) === null) {
			response.message = 'Số điện thoại này chưa liên kết với Ví PayME';
			return response;
		}

		const accountInfo = await Broker.call('v1.eWalletAccountModel.findOne', [{
			phone: phoneFormatted
		}]);

		if (_.get(connectedUserInfo, 'id', null) === null) {
			response.message = 'Số điện thoại chưa tồn tại ở PayME';
			return response;
		}

		let settingKey = null;
		// Xet linkedId
		const linkedInfo = await Broker.call('v1.eWalletLinkedModel.findOne', [{
			id: linkedId,
			accountId: accountInfo.id,
			state: WithdrawConstant.LINKED_STATE.LINKED
		}]);

		if (_.get(linkedInfo, 'id', null) === null) {
			response.message = 'LinkedId sai hoặc không tồn tại (1)';
			return response;
		}

		if (linkedInfo.supplier === WithdrawConstant.SUPPLIER.NAPAS_GATEWAY) {
			settingKey = 'NAPAS_LINKED';
		} else {
			settingKey = 'LINKED';
		}

		let valueWithdrawConfig = await Broker.call('v1.eWalletSettingModel.findOne', [{
			key: 'fee.withdraw.config'
		}]);

		valueWithdrawConfig = valueWithdrawConfig.value;

		const calFeeResult = await this.CalcFee({
			accountId: accountInfo.id,
			amount,
			feeConfig: valueWithdrawConfig,
			settingKey: WithdrawConstant.FEE_SETTING_KEY[settingKey],
			chargeFeeKey: `TransferMoneyWithdrawChargeFee_${accountInfo.id}`,
			filter: {
				accountId: accountInfo.id,
				state: WithdrawConstant.WITHDRAW_STATE.SUCCEEDED
			}
		});
		if (_.isNumber(_.get(calFeeResult, 'fee')) === false) {
			response.message = 'Rút tiền thất bại. Vui lòng thử lại (E01)';
			return response;
		}

		const { fee } = calFeeResult;
		const totalPay = _.toNumber((new Decimal(amount)).add(new Decimal(fee)));

		let walletInfo = null;
		try {
			walletInfo = await this.WalletLibs.Information(accountInfo.id);
		} catch (error) {
			Logger(`[WITHDRAW] => [DEPOSIT_WALLET_ERROR]: ${error}`);
			response.message = 'Rút tiền thất bại. Vui lòng thử lại (E02)';
			return response;
		}

		if (_.get(walletInfo, 'balance', 0) < totalPay) {
			response.message = 'Số dư của bạn không đủ để thực hiện giao dịch';
			return response;
		}

		let transaction = null;
		try {
			transaction = await this.broker.call('v1.walletUuid.pick', { prefix: 'WITHDRAW' });
		} catch (error) {
			response.message = 'Rút tiền thất bại. Vui lòng thử lại (E03)';
			return response;
		}

		if (_.isNull(transaction)) {
			response.message = 'Rút tiền thất bại. Vui lòng thử lại (E04)';
			return response;
		}

		let withdrawId = null;
		try {
			withdrawId = await this.broker.call('v1.walletUuid.pick', { prefix: 'WITHDRAW' });
		} catch (error) {
			response.message = 'Rút tiền thất bại. Vui lòng thử lại (E05)';
			return response;
		}

		if (_.isNull(withdrawId)) {
			response.message = 'Rút tiền thất bại. Vui lòng thử lại (E06)';
			return response;
		}

		const dataWithdraw = {
			id: withdrawId,
			accountId: accountInfo.id,
			appId: eWalletAppInfo.id,
			transaction,
			description: null,
			amount: payload.amount,
			fee,
			total: totalPay,
			state: WithdrawConstant.WITHDRAW_STATE.PENDING,
			extraData: {
				linkedId: linkedInfo.id
			}
		};

		let withdrawCreated = null;

		try {
			withdrawCreated = await Broker.call('v1.eWalletWithdrawModel.create', [{ ...dataWithdraw }]);
		} catch (e) {
			response.message = 'Rút tiền thất bại. Vui lòng thử lại (E07)';
			return response;
		}

		const dataHistory = {
			accountId: accountInfo.id,
			appId: eWalletAppInfo.id,
			service: {
				type: 'WITHDRAW',
				code: 'WITHDRAW_BANK_LINKED_GATEWAY',
				method: 'LINKED',
				name: 'Rút tiền về tài khoản liên kết',
				id: withdrawCreated.id,
				transaction: withdrawCreated.transaction,
				state: withdrawCreated.state,
				data: {
					resolveType: WithdrawConstant.RESOLVE_TYPE.WITHDRAW,
					withdrawId: withdrawCreated.id,
					transaction: withdrawCreated.transaction,
					accountId: withdrawCreated.accountId,
					state: withdrawCreated.state
				}
			},
			amount: withdrawCreated.amount,
			fee: withdrawCreated.fee,
			total: withdrawCreated.total,
			discount: 0,
			bonus: 0,
			state: WithdrawConstant.WITHDRAW_STATE.PENDING,
			description: 'Rút tiền về ví PayME.',
			changed: '',
			tags: [
				'WITHDRAW'
			]
		};

		let historyCreated = null;
		try {
			historyCreated = await Broker.call('v1.eWalletHistoryModel.create', [dataHistory]);
		} catch (error) {
			Logger(`[WITHDRAW_EWALLET] => [CREATE_HISTORY_WITHDRAW_ERROR]: ${error}`);
			response.message = 'Rút tiền thất bại. Vui lòng thử lại (E08)';
			return response;
		}

		let valueWithdrawThresholdConfig = await Broker.call('v1.eWalletSettingModel.findOne', [{
			key: 'withdraw.require.verify'
		}]);
		valueWithdrawThresholdConfig = _.get(valueWithdrawThresholdConfig, 'value', null);

		try {
			valueWithdrawThresholdConfig = JSON.parse(valueWithdrawThresholdConfig);
			const amountVerify = _.get(valueWithdrawThresholdConfig, 'threshold', 0);

			if (amountVerify < totalPay) {
				const code = _.random(100000, 999999);
				const activeCodeObj = {
					phone: phoneFormatted,
					code,
					source: WithdrawConstant.WITHDRAW_SOURCE,
					expiredAt: moment(new Date()).add(5, 'minutes')
				};
				const createActiveCode = await Broker.call('v1.ewalletActiveCodeModel.create', [activeCodeObj]);

				try {
					const smsContent = 'Ma xac thuc PayME cua ban khi rut tien la ';
					const paramsSendSMS = {
						phone: phoneFormatted,
						code,
						content: smsContent,
						isFullContent: false
					};
					await this.broker.call('v1.utility.sendSMS', paramsSendSMS);
				} catch (error) {
					Logger('SEND_SMS_WITHDRAW :>>>>', error);
					response.message = 'Có lỗi xảy ra khi rút tiền. Vui lòng thử lại sau.';
					return response;
				}

				response.code = ErrorCode.WITHDRAW_CODE.REQUIRE_OTP;
				response.message = 'Rút tiền cần xác thực thông qua OTP';
				response.data = {
					transaction
				};
				return response;
			}
		} catch (e) {
			console.log(e);
			response.message = 'Rút tiền thất bại. Vui lòng thử lại (E09)';
			return response;
		}
		const paymentParams = {
			accountInfo,
			appId: eWalletAppInfo.id,
			transaction,
			amount: totalPay,
			walletBalance: walletInfo.balance
		};

		const PayWallet = await this.PayWallet(paymentParams);

		if (PayWallet.state !== WithdrawConstant.PAYMENT_STATE.SUCCEEDED) {
			withdrawCreated = await Broker.call('v1.eWalletWithdrawModel.updateOne', [
				{ id: withdrawCreated.id },
				{
					state: WithdrawConstant.WITHDRAW_STATE.FAILED,
					payment: PayWallet.payment
				}
			]);

			historyCreated = await Broker.call('v1.eWalletHistoryModel.updateOne', [
				{ id: historyCreated.id },
				{
					state: WithdrawConstant.WITHDRAW_STATE.FAILED,
					'service.state': WithdrawConstant.WITHDRAW_STATE.FAILED,
					'service.data.state': WithdrawConstant.WITHDRAW_STATE.FAILED,
					payment: PayWallet.payment
				}
			]);

			response.message = _.get(PayWallet, 'message', 'Rút tiền thất bại. Vui lòng thử lại (E10)');
			return response;
		}

		// Withdraw Succeed, Dich vu cung succeed
		withdrawCreated = await Broker.call('v1.eWalletWithdrawModel.findOneAndUpdate', [
			{ id: withdrawCreated.id },
			{
				state: WithdrawConstant.WITHDRAW_STATE.SUCCEEDED,
				payment: PayWallet.payment
			}, { new: true }
		]);

		historyCreated = await Broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [
			{ id: historyCreated.id },
			{
				'service.state': WithdrawConstant.WITHDRAW_STATE.SUCCEEDED,
				'service.data.state': WithdrawConstant.WITHDRAW_STATE.SUCCEEDED,
				changed: '-',
				balance: PayWallet.balance,
				payment: PayWallet.payment,
				publishedAt: new Date().toISOString()
			},
			{ new: true }
		]);

		// Goi Transport
		let transportResponse = null;
		try {
			transportResponse = await Broker.call('v1.eWalletWithdraw.transportLinked', {
				linkedId: linkedInfo.id,
				amount,
				phone: accountInfo.phone,
				accountId: accountInfo.id,
				appId: eWalletAppInfo.id,
				transaction: withdrawCreated.transaction
			});
		} catch (e) {
			console.log(e);
		}

		let transportUpdateState = WithdrawConstant.TRANSPORT_STATE.FAILED;
		if (transportResponse.state === GeneralConstant.TRANSPORT_STATE.FAILED) {
			const formatedAmount = Numeral(withdrawCreated.total).format('0,0');
			const paramsRefund = {
				paymentId: PayWallet.payment.id,
				appId: eWalletAppInfo.id,
				amount,
				serviceCode: 'WITHDRAW',
				serviceId: withdrawCreated.id,
				description: `Bạn được hoàn ${formatedAmount}đ do rút tiền thất bại.`
			};
			// Goi action hoan tien
			const refundWithdraw = await Broker.call('v1.eWalletWithdraw.refund', {
				...paramsRefund
			});

			if (refundWithdraw.state !== GeneralConstant.REFUND_STATE.SUCCEEDED) {
				try {
					const alertMsg = `Hoàn tiền thất bại cho dịch vụ rút tiền
					\nwithdrawId: ${withdrawCreated.id}
					\nrefundResponse: ${JSON.stringify(refundWithdraw)}`;
					Broker.call('v1.utility.notifyTelegram', {
						message: alertMsg,
						buttons: [],
						receiverList: ['api']
					});
				} catch (e) {
					Logger('NOTIFY_FAILED');
				}
				// console.log(refundWithdraw);
			}
			response.message = _.get(transportResponse, 'message', 'Rút tiền thất bại');
		} else {
			response.code = ErrorCode.WITHDRAW_CODE.SUCCEED;
			response.message = 'Rút tiền thành công';
			transportUpdateState = WithdrawConstant.TRANSPORT_STATE.SUCCEEDED;
		}

		const serviceName = `Rút tiền về ${_.get(transportResponse, 'transportInfo.description', '')}`;

		withdrawCreated = await Broker.call('v1.eWalletWithdrawModel.findOneAndUpdate', [
			{ id: withdrawCreated.id },
			{
				state: transportUpdateState,
				transport: transportResponse.transportInfo
			}
		]);

		historyCreated = await Broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [
			{ id: historyCreated.id },
			{
				state: transportUpdateState,
				'service.state': transportUpdateState,
				'service.data.state': transportUpdateState,
				'service.name': serviceName,
				transport: transportResponse.transportInfo
			}
		]);

		try {
			const alertMsg = `[S2S] Tạo lệnh thanh toán Rút tiền từ ${accountInfo.fullname} thành công
			\n transaction: ${transaction}
			\n logs: ${JSON.stringify({
		accountId: authInfo.fullname,
		amount: withdrawCreated.amount,
		total: withdrawCreated.total,
		fee: withdrawCreated.fee,
		discount: historyCreated.discount,
		cashback: historyCreated.cashback
	})}`;
			const nameGroupNoti = process.env.TRANSACTION_NOTIFY_GROUP;
			Broker.call('v1.utility.notifyTelegram', {
				message: alertMsg,
				buttons: [],
				receiverList: [nameGroupNoti]
			});
		} catch (e) {
			Logger('NOTIFY_FAILED');
		}

		response.data = {
			transaction
		};
		// succeed cap nhat
		return response;
	} catch (error) {
		console.log(error);
		Logger('[OPENEWALLET] >>> [WITHDRAW] >> [ERROR] >>', JSON.stringify(error));

		return {
			code: ErrorCode.WITHDRAW_CODE.FAILED,
			message: 'Rút tiền thất bại. Vui lòng thử lại sau.(E00)'
		};
	}
};
