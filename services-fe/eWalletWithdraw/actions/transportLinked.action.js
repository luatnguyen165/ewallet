const _ = require('lodash');
const WithdrawConstant = require('../constants/withdraw.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;

	const payload = ctx.params;

	const response = {
		state: WithdrawConstant.TRANSPORT_STATE.FAILED,
		message: 'Rút tiền thất bại. Vui lòng thử lại sau.(ERR_TRSP_00)'
	};

	try {
		const linkedInfo = await Broker.call('v1.eWalletLinkedModel.findOne', [{
			id: payload.linkedId,
			accountId: payload.accountId
		}]);

		const bankInfo = _.get(linkedInfo, 'cardInfo', {});
		_.set(bankInfo, 'bankAccountNumber', bankInfo.accountNumber);
		let transportTransaction = null;
		try {
			transportTransaction = await Broker.call('v1.walletUuid.pick', { prefix: 'TRANSPORT' });
		} catch (error) {
			response.message = 'Rút tiền thất bại. Vui lòng thử lại (ERR_TRSP_01)';
			return response;
		}

		const transportDescription = await this.CardDescription(_.get(linkedInfo, 'cardInfo', {}));

		const dataTransport = {
			accountId: payload.accountId,
			appId: payload.appId,
			transaction: transportTransaction,
			amount: payload.amount,
			description: transportDescription,
			method: 'LINKED',
			service: {
				code: 'WITHDRAW',
				type: 'WITHDRAW',
				transaction: payload.transaction
			},
			state: WithdrawConstant.WITHDRAW_STATE.PENDING
		};
		let transportCreated = null;
		try {
			transportCreated = await Broker.call('v1.eWalletTransportModel.create', [dataTransport]);
		} catch (e) {
			console.log(e);
			response.message = 'Rút tiền thất bại. Vui lòng thử lại (ERR_TRSP_02)';
			return response;
		}
		// const cardDescription = await this.CardDescription(_.get(linkedInfo, 'cardInfo', {}));

		const dataTransportLinked = {
			accountId: transportCreated.accountId,
			transportId: transportCreated.id,
			linkedId: linkedInfo.id,
			appId: transportCreated.appId,
			transaction: transportCreated.transaction,
			amount: transportCreated.amount,
			service: transportCreated.service,
			state: WithdrawConstant.WITHDRAW_STATE.PENDING,
			description: transportCreated.description,
			bankInfo,
			note: transportCreated.note
		};

		let transportLinkedCreated = null;
		try {
			transportLinkedCreated = await Broker.call('v1.eWalletTransportLinkedModel.create',
				[dataTransportLinked]);
		} catch (e) {
			response.message = 'Rút tiền thất bại. Vui lòng thử lại (ERR_TRSP_03)';
			return response;
		}

		let supplierTransportResponse = {};
		let paramsSend = {};
		switch (linkedInfo.supplier) {
			case WithdrawConstant.SUPPLIER.NAPAS_GATEWAY:
				if (bankInfo.cardNumber) {
					paramsSend = {
						transaction: _.get(transportCreated, 'transaction', null), // Transaction khi nguoi dung thuc hien giao dich
						amount: transportCreated.amount,
						bankInfo: {
							method: 'CARD',
							cardNumber: bankInfo.cardNumber,
							swiftCode: bankInfo.swiftCode,
							fullname: bankInfo.cardHolder
						}
					};
				} else {
					paramsSend = {
						transaction: _.get(transportCreated, 'transaction', null), // Transaction khi nguoi dung thuc hien giao dich
						amount: transportCreated.amount,
						bankInfo: {
							method: 'ACCOUNT',
							accountNumber: bankInfo.bankAccountNumber,
							fullname: bankInfo.cardHolder || bankInfo.bankAccountName,
							swiftCode: bankInfo.swiftCode
						}
					};
				}
				supplierTransportResponse = await Broker.call('v1.eWalletWithdraw.bankBalancerTransfer',
					{ ...paramsSend });
				break;
			case WithdrawConstant.SUPPLIER.PVCOMBANK:
				paramsSend = {
					transaction: transportTransaction,
					amount: payload.amount,
					accountId: payload.accountId,
					cardId: _.get(linkedInfo, 'linkedInfo.cardId', null)
				};
				supplierTransportResponse = await this.PVComBankLibs.Withdraw(paramsSend);
				break;
			case WithdrawConstant.SUPPLIER.OCBBANK:

				// eslint-disable-next-line no-case-declarations
				const checkLimit1Day = await this.CheckLimitWithdrawOCB(payload.accountId, transportCreated.amount);

				if (checkLimit1Day.state === WithdrawConstant.WITHDRAW_STATE.SUCCEEDED) {
					paramsSend = {
						cardId: _.get(linkedInfo, 'linkedInfo.cardId', null),
						phone: _.replace(payload.phone, /^84/, '0'),
						amount: transportCreated.amount
					};
					supplierTransportResponse = await this.OCBBankLibs.Withdraw(paramsSend);
				} else {
					supplierTransportResponse = {
						state: GeneralConstant.SUPPLIER_STATE.FAILED,
						message: _.get(checkLimit1Day, 'message', 'Vượt quá giới hạn rút 1 ngày thẻ liên kết OCB')
					};
				}
				break;

			case WithdrawConstant.SUPPLIER.BIDVBANK:
				paramsSend = {
					transaction: transportTransaction,
					amount: transportCreated.amount,
					accountId: payload.accountId,
					version: _.get(linkedInfo, 'cardInfo.version', 1) || 1
				};
				supplierTransportResponse = await this.BIDVLibs.Withdraw(paramsSend);
				break;
			case WithdrawConstant.SUPPLIER.VIETINBANK:
				paramsSend = {
					transaction: transportTransaction,
					amount: transportCreated.amount,
					accountId: payload.accountId,
					cardId: _.get(linkedInfo, 'linkedInfo.cardId', null)
				};
				supplierTransportResponse = await this.VietinBankLibs.Withdraw(paramsSend);
				break;
			default:
				break;
		}

		const supplierResponsed = [supplierTransportResponse.supplierResponse];

		const dataUpdateLinkedTransport = {
			supplier: linkedInfo.supplier,
			supplierResponsed,
			referenceTransaction: _.get(supplierTransportResponse, 'referenceTransaction', null),
			state: WithdrawConstant.WITHDRAW_STATE.SUCCEEDED
		};
		const dataTransportUpdate = {
			state: WithdrawConstant.WITHDRAW_STATE.SUCCEEDED
		};
		// fail thi hoan tien
		if (supplierTransportResponse.state === GeneralConstant.SUPPLIER_STATE.FAILED) {
			dataUpdateLinkedTransport.state = WithdrawConstant.WITHDRAW_STATE.FAILED;
			dataTransportUpdate.state = WithdrawConstant.WITHDRAW_STATE.FAILED;
		}

		transportLinkedCreated = await Broker.call('v1.eWalletTransportLinkedModel.findOneAndUpdate', [{
			id: transportLinkedCreated.id
		}, { ...dataUpdateLinkedTransport }, { new: true }]);

		transportCreated = await Broker.call('v1.eWalletTransportModel.findOneAndUpdate',
			[{ id: transportCreated.id }, { ...dataTransportUpdate }, { new: true }]);

		response.transportInfo = {
			accountId: transportCreated.accountId,
			transportId: transportCreated.id,
			transaction: transportCreated.transaction,
			state: transportCreated.state,
			method: 'LINKED',
			description: transportDescription
		};

		response.state = transportCreated.state;
		response.message = _.get(supplierTransportResponse, 'message', 'Rút tiền thành công');
		return response;
	} catch (e) {
		console.log(e);
		Logger('[TRANSPORT] => [LINKED] => [EXCEPTION]', e);
		return response;
	}
};
