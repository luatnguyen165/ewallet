const _ = require('lodash');
const Numeral = require('numeral');
const Decimal = require('decimal.js');

const ErrorCodeConstant = require('../constants/errorCode.constant');
const WithdrawConstant = require('../constants/withdraw.constant');
const TransportConstant = require('../constants/transport.constant');
const AccountConstant = require('../constants/account.constant');
const FrontendConstant = require('../constants/frontend.constant');
const PaymentConstant = require('../constants/payment.constant');

const TelegramGroup = process.env.NOTIFY_TELEGRAM;
module.exports = async function (ctx) {
	try {
		const payload = _.get(ctx, 'params.body', {});
		const authInfo = _.get(ctx, 'meta.auth.data', {});

		// const authInfo = {
		// 	accountId: 566980335,
		// 	fullname: 'Nguyen Van A'
		// };
		if (!authInfo.accountId) {
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.AUTH_ERROR,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.AUTH_ERROR.toString())
			};
		}
		if (authInfo.accountType === AccountConstant.ACCOUNT_TYPE.BUSINESS && !_.includes(authInfo.scope, 'account.bussiness.withdraw')) {
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.WITHDRAW_IS_NOT_ALLOWED,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.WITHDRAW_IS_NOT_ALLOWED.toString())
			};
		}

		let withdrawEnable = await this.broker.call('v1.eWalletSetting.getSettingConfig', { body: { keys: ['withdraw.config.enable'] } });

		if (_.get(withdrawEnable, 'code', null) === 202500 && !_.isEmpty(withdrawEnable.data) && withdrawEnable.data[0]) {
			withdrawEnable = withdrawEnable.data[0].value;
		} else {
			this.logger.info(`[eWalletWithdraw] Withdraw SDK -> withdrawEnable: ${JSON.stringify(withdrawEnable)}`);
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.FAILED,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.FAILED.toString())
			};
		}

		let transportInput = _.get(payload, 'transport', {});

		transportInput = _.pick(transportInput, WithdrawConstant.ALLOWED_TRANSPORT_METHOD);
		if (_.isEmpty(transportInput)) {
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.TRANSPORT_METHOD_IS_NOT_SUPPORTED,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.TRANSPORT_METHOD_IS_NOT_SUPPORTED.toString())
			};
		}

		let paymentInput = _.get(payload, 'payment', {});
		paymentInput = _.pick(paymentInput, WithdrawConstant.ALLOWED_PAYMENT_METHOD);
		if (_.isEmpty(paymentInput)) {
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.PAYMENT_METHOD_IS_NOT_SUPPORTED,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.PAYMENT_METHOD_IS_NOT_SUPPORTED.toString())
			};
		}

		let amountLimit = await this.broker.call('v1.eWalletSetting.getSettingConfig', { body: { keys: [WithdrawConstant.LIMIT_SETTING_KEY] } });
		if (_.get(amountLimit, 'code', null) === 202500 && !_.isEmpty(amountLimit.data) && amountLimit.data[0]) {
			if (_.isString(amountLimit)) amountLimit = JSON.parse(amountLimit);
			amountLimit = amountLimit.data[0].value;
		}

		if (_.get(amountLimit, 'min', null) === null) _.set(amountLimit, 'min', WithdrawConstant.DEFAULT_AMOUNT_MIN);
		if (_.get(amountLimit, 'max', null) === null) _.set(amountLimit, 'max', WithdrawConstant.DEFAULT_AMOUNT_MAX);

		if (_.includes([AccountConstant.ACCOUNT_TYPE.PERSONAL], authInfo.accountType)) {
			if (payload.amount < amountLimit.min) {
				return {
					code: ErrorCodeConstant.WITHDRAW_SDK.MIN_AMOUNT_ERROR,
					message: this.__(ErrorCodeConstant.WITHDRAW_SDK.PAYMENT_METHOD_IS_NOT_SUPPORTED.toString(), { amount: Numeral(amountLimit.min).format('0,0') })
				};
			}

			if (payload.amount > amountLimit.max) {
				return {
					code: ErrorCodeConstant.WITHDRAW_SDK.MAX_AMOUNT_ERROR,
					message: this.__(ErrorCodeConstant.WITHDRAW_SDK.PAYMENT_METHOD_IS_NOT_SUPPORTED.toString(), { amount: Numeral(amountLimit.max).format('0,0') })
				};
			}
		}

		let settingKey = null;
		if (transportInput.bankCard || transportInput.bankAccount) {
			settingKey = 'NAPAS';
		} else if (transportInput.linked) {
			const linkedInfo = await this.broker.call('v1.eWalletLinkedModel.findOne', [{
				accountId: authInfo.accountId,
				id: _.get(transportInput, 'linked.linkedId', null),
				state: 'LINKED'
			}]);
			if (_.get(linkedInfo, 'id', 0) < 1) {
				return {
					code: ErrorCodeConstant.WITHDRAW_SDK.LINKED_NOT_FOUND,
					message: this.__(ErrorCodeConstant.WITHDRAW_SDK.LINKED_NOT_FOUND.toString())
				};
			}
			if (linkedInfo.supplier === TransportConstant.SUPPLIER.NAPAS_GATEWAY) {
				settingKey = 'NAPAS_LINKED';
			} else {
				settingKey = 'LINKED';
			}
		}
		if (!settingKey) {
			this.logger.info(`[eWalletWithdraw] Withdraw SDK -> settingKey: ${settingKey}`);
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.WITHDRAW_NOT_COMPATIBLE,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.WITHDRAW_NOT_COMPATIBLE.toString())
			};
		}

		let feeConfig = await this.broker.call('v1.eWalletSetting.getSettingConfig', { body: { keys: ['fee.withdraw.config'] } });
		if (_.get(feeConfig, 'code', null) === 202500 && !_.isEmpty(feeConfig.data) && feeConfig.data[0]) {
			feeConfig = feeConfig.data[0].value;
		}

		// const calFeeResult = await CalculateFeeHelper.CalFeePerDays({
		// 	accountId: authInfo.accountId,
		// 	amount: payload.amount,
		// 	feeConfig,
		// 	settingKey: WithdrawConstant.FEE_SETTING_KEY[settingKey],
		// 	chargeFeeKey: `TransferMoneyWithdrawChargeFee_${authInfo.accountId}`,
		// 	model: WithdrawModel,
		// 	filter: {
		// 		accountId: authInfo.accountId,
		// 		state: WithdrawConstant.WITHDRAW_STATE.SUCCEEDED
		// 	}
		// });
		// if (_.isNumber(_.get(calFeeResult, 'fee')) === false) {
		// 	return {
		// 		succeeded: false,
		// 		message: 'Đã có lỗi xảy ra. Vui lòng thử lại.'
		// 	};
		// }

		const calFeeResult = {
			fee: 0
		};

		if (_.isNumber(_.get(calFeeResult, 'fee')) === false) {
			return {
				succeeded: false,
				message: 'Đã có lỗi xảy ra. Vui lòng thử lại.'
			};
		}

		const { fee } = calFeeResult;
		const totalPay = _.toNumber((new Decimal(payload.amount)).add(new Decimal(fee)));

		const bankTransferService = await this.BankTransferService();
		const supplier = await bankTransferService.GetSupplier();
		// const bankService = new BankConnectorService(ocbBankServiceConfig.url, ocbBankServiceConfig.authorization, PayMEConfig.EnvTag);
		const paymeAccountName = _.toUpper(_.trim(this.RemoveUnicode(authInfo.fullname)));
		if (transportInput.bankCard) {
			let receiverFullname = _.get(transportInput, 'bankCard.cardHolder', null);
			if (!receiverFullname) {
				const GetRecipientNameCardNumber = await bankTransferService.GetBankName({
					cardNumber: _.get(transportInput, 'bankCard.cardNumber', null),
					swiftCode: _.get(transportInput, 'bankCard.swiftCode', null),
					supplier
				});
				if (GetRecipientNameCardNumber.code !== 1 || GetRecipientNameCardNumber.data.length <= 0) {
					this.logger.info(`[eWalletWithdraw] withdrawSDK --> GetRecipientNameCardNumber Error: ${JSON.stringify(GetRecipientNameCardNumber)}`);
					return {
						code: ErrorCodeConstant.WITHDRAW_SDK.BANK_INFO_NOT_FOUND,
						message: this.__(ErrorCodeConstant.WITHDRAW_SDK.BANK_INFO_NOT_FOUND.toString())
					};
				}

				receiverFullname = _.get(GetRecipientNameCardNumber.data, 'fullname', null);
			}

			receiverFullname = _.toUpper(_.trim(this.RemoveUnicode(receiverFullname)));
			if (!receiverFullname) {
				return {
					code: ErrorCodeConstant.WITHDRAW_SDK.BANK_INFO_NOT_FOUND,
					message: this.__(ErrorCodeConstant.WITHDRAW_SDK.BANK_INFO_NOT_FOUND.toString())
				};
			}

			const splitedCardHolder = receiverFullname.split(/\s+/);
			const splitedPaymeName = paymeAccountName.split(/\s+/);

			if (splitedCardHolder[0] !== splitedPaymeName[0] || _.last(splitedCardHolder) !== _.last(splitedPaymeName)) {
				const startNumCheckPayME = splitedPaymeName.length - 2;
				const startNumCheckCardHolder = splitedCardHolder.length - 2;

				if ((splitedPaymeName[startNumCheckPayME] !== splitedCardHolder[startNumCheckCardHolder])
					|| (splitedPaymeName[startNumCheckPayME + 1] !== splitedCardHolder[startNumCheckCardHolder + 1])
				) {
					return {
						code: ErrorCodeConstant.WITHDRAW_SDK.PAYME_AND_BANK_DIFFERENT,
						message: this.__(ErrorCodeConstant.WITHDRAW_SDK.PAYME_AND_BANK_DIFFERENT.toString())
					};
				}
			}
			_.set(transportInput, 'bankCard.supplier', supplier);
			_.set(transportInput, 'bankCard.cardHolder', receiverFullname);
		} else if (transportInput.bankAccount) {
			let receiverFullname = _.get(transportInput, 'bankAccount.bankAccountName', null);
			if (!receiverFullname) {
				const GetRecipientNameBankAccount = await bankTransferService.GetBankName({
					bankAccountNumber: _.get(transportInput, 'bankAccount.bankAccountNumber', null),
					swiftCode: _.get(transportInput, 'bankAccount.swiftCode', null),
					supplier
				});
				if (GetRecipientNameBankAccount.code !== 1 || GetRecipientNameBankAccount.data.length <= 0) {
					this.logger.info(`[eWalletWithdraw] withdrawSDK --> GetRecipientNameBankAccount Error: ${JSON.stringify(GetRecipientNameBankAccount)}`);
					return {
						code: ErrorCodeConstant.WITHDRAW_SDK.BANK_INFO_NOT_FOUND,
						message: this.__(ErrorCodeConstant.WITHDRAW_SDK.BANK_INFO_NOT_FOUND.toString())
					};
				}

				receiverFullname = _.get(GetRecipientNameBankAccount.data, 'fullname', null);
			}

			receiverFullname = _.toUpper(_.trim(this.RemoveUnicode(receiverFullname)));
			if (!receiverFullname) {
				return {
					code: ErrorCodeConstant.WITHDRAW_SDK.BANK_INFO_NOT_FOUND,
					message: this.__(ErrorCodeConstant.WITHDRAW_SDK.BANK_INFO_NOT_FOUND.toString())
				};
			}

			const splitedBankAccountName = receiverFullname.split(/\s+/);
			const splitedPaymeName = paymeAccountName.split(/\s+/);
			if (splitedBankAccountName[0] !== splitedPaymeName[0] || _.last(splitedBankAccountName) !== _.last(splitedPaymeName)) {
				const startNumCheckPayME = splitedPaymeName.length - 2;
				const startNumCheckCardHolder = splitedBankAccountName.length - 2;

				if ((splitedPaymeName[startNumCheckPayME] !== splitedBankAccountName[startNumCheckCardHolder])
					|| (splitedPaymeName[startNumCheckPayME + 1] !== splitedBankAccountName[startNumCheckCardHolder + 1])
				) {
					return {
						code: ErrorCodeConstant.WITHDRAW_SDK.PAYME_AND_BANK_DIFFERENT,
						message: this.__(ErrorCodeConstant.WITHDRAW_SDK.PAYME_AND_BANK_DIFFERENT.toString())
					};
				}
			}
			_.set(transportInput, 'bankAccount.supplier', supplier);
			_.set(transportInput, 'bankAccount.bankAccountName', receiverFullname);
		}

		const walletSearch = await this.WalletLibs.Information(authInfo.accountId);
		if (!walletSearch.succeeded) {
			this.logger.info(`[eWalletWithdraw] withdrawSDK --> Get Wallet Information Error: ${JSON.stringify(walletSearch)}`);
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.FAILED,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.FAILED.toString())
			};
		}

		if (_.get(walletSearch, 'balance', 0) < totalPay) {
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.BALANCE_NOT_ENOUGH,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.BALANCE_NOT_ENOUGH.toString())
			};
		}

		const withdrawTransaction = await this.broker.call('v1.walletUuid.pick', { prefix: WithdrawConstant.TRANSACTION_UUID_PREFIX });
		if (!withdrawTransaction) {
			this.logger.info(`[eWalletWithdraw] Withdraw SDK -> withdrawTransaction: ${withdrawTransaction}`);
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.FAILED,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.FAILED.toString())
			};
		}

		const withdrawId = await this.broker.call('v1.walletUuid.pick', { prefix: WithdrawConstant.MODEL_ID_UUID_PREFIX });
		if (!withdrawId) {
			this.logger.info(`[eWalletWithdraw] Withdraw SDK -> withdrawId: ${withdrawId}`);
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.FAILED,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.FAILED.toString())
			};
		}

		const withdrawObj = {
			id: _.toNumber(withdrawId),
			accountId: authInfo.accountId,
			appId: authInfo.appId || WithdrawConstant.DEFAULT_APP_ID,
			transaction: withdrawTransaction,
			description: null,
			amount: payload.amount,
			fee,
			total: totalPay,
			state: WithdrawConstant.WITHDRAW_STATE.PENDING
		};

		let createdWithdraw = await this.broker.call('v1.eWalletWithdrawModel.create', [withdrawObj]);
		if (_.get(createdWithdraw, 'id', 0) < 1) {
			this.logger.info(`[eWalletWithdraw] Withdraw SDK -> createdWithdraw Error: ${JSON.stringify(createdWithdraw)}`);
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.FAILED,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.FAILED.toString())
			};
		}

		let method = null;
		let serviceCode = FrontendConstant.SERVICES.WITHDRAW_BANK_GATEWAY;
		if (transportInput.bankCard) method = TransportConstant.METHOD.BANK_CARD;
		else if (transportInput.bankAccount) method = TransportConstant.METHOD.BANK_ACCOUNT;
		else if (transportInput.linked) {
			method = TransportConstant.METHOD.LINKED;
			serviceCode = FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_GATEWAY;
		}

		let serviceName = `Rút tiền về ${WithdrawConstant.METHOD_CODE_TO_TEXT_MAPPING[method]}`;

		const historyObj = {
			accountId: authInfo.accountId,
			appId: authInfo.appId,
			service: {
				type: FrontendConstant.SERVICES.WITHDRAW,
				code: serviceCode,
				name: serviceName,
				id: createdWithdraw.id,
				transaction: createdWithdraw.transaction,
				state: createdWithdraw.state,
				data: {
					resolveType: WithdrawConstant.RESOLVE_TYPE,
					withdrawId: createdWithdraw.id,
					transaction: createdWithdraw.transaction,
					accountId: createdWithdraw.accountId,
					state: createdWithdraw.state
				}
			},
			amount: createdWithdraw.amount,
			fee: createdWithdraw.fee,
			total: createdWithdraw.total,
			discount: 0,
			bonus: 0,
			state: FrontendConstant.HISTORY_STATE.PENDING,
			description: 'Rút tiền về ví PayME.',
			changed: '',
			tags: [
				FrontendConstant.TAGS.WITHDRAW
			]
			// extraData: createdWithdraw
		};
		let createdHistory = await this.broker.call('v1.eWalletHistory.create', { dataHistory: historyObj });
		if (_.get(createdHistory, 'data.id', 0) < 1) {
			createdWithdraw = await this.broker.call('v1.eWalletWithdrawModel.findOneAndUpdate', [{
				id: createdWithdraw.id
			}, {
				$set: {
					state: WithdrawConstant.WITHDRAW_STATE.FAILED
				}
			}, {
				new: true
			}]);
			if (_.get(createdWithdraw, 'nModified', 0) < 1) {
				this.logger.info(`[eWalletWithdraw] Withdraw SDK -> Update CreatedWithdraw Error: ${JSON.stringify(createdWithdraw)}`);
			}
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.FAILED,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.FAILED.toString())
			};
		}
		createdHistory = createdHistory.data;

		if (!_.isEmpty(paymentInput.wallet)) {
			paymentInput.wallet.requireSecurityCode = false;
		}

		const paymentObj = {
			accountId: authInfo.accountId,
			appId: authInfo.appId || null,
			amount: createdWithdraw.total,
			service: {
				code: serviceCode,
				type: FrontendConstant.SERVICES.WITHDRAW,
				transaction: createdWithdraw.transaction
			},
			description: serviceName,
			method: paymentInput,
			ip: _.get(ctx, 'meta.clientIp', null),
			clientId: _.get(ctx, 'meta.auth.credetials.clientId', null)
		};
		this.logger.info(`[eWalletWithdraw] withdrawSDK PaymentObj: ${JSON.stringify(paymentObj)}`);
		const paymentResponse = await this.broker.call('v1.eWalletPayment.pay', paymentObj);
		this.logger.info(`[eWalletWithdraw] withdrawSDK PaymentResponse: ${JSON.stringify(paymentResponse)}`);
		// const withdrawWalletResponse = await WalletService.Withdraw(authInfo.accountId, createdWithdraw.total, 'Rút tiền từ ví PayeME.', createdWithdraw);
		// if (withdrawWalletResponse.code !== 1) {
		if (paymentResponse.state !== PaymentConstant.STATE.SUCCEEDED) {
			createdWithdraw = await this.broker.call('v1.eWalletWithdrawModel.findOneAndUpdate', [{
				id: createdWithdraw.id
			}, {
				$set: {
					state: WithdrawConstant.WITHDRAW_STATE.FAILED,
					payment: paymentResponse.payment
				}
			}, {
				new: true
			}]);
			if (_.get(createdWithdraw, 'id', 0) < 1) {
				this.logger.info(`[eWalletWithdraw] Withdraw SDK -> Update CreatedWithdraw Error: ${JSON.stringify(createdWithdraw)}`);
			}

			createdHistory = await this.broker.call('v1.eWalletHistory.update', {
				fields: {
					id: createdHistory.id
				},
				updateInfo: {
					state: FrontendConstant.HISTORY_STATE.FAILED,
					'service.state': WithdrawConstant.WITHDRAW_STATE.FAILED,
					'service.data.state': WithdrawConstant.WITHDRAW_STATE.FAILED,
					payment: paymentResponse.payment
				}
			});
			if (_.get(createdHistory, 'code', 0) !== 202710) {
				this.logger.info(`[eWalletWithdraw] Withdraw SDK -> Update createdHistory Error: ${JSON.stringify(createdHistory)}`);
			}

			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.WITHDRAW_BALANCE_ERROR,
				message: paymentResponse.message || this.__(ErrorCodeConstant.WITHDRAW_SDK.WITHDRAW_BALANCE_ERROR.toString())
			};
		}

		createdWithdraw = await this.broker.call('v1.eWalletWithdrawModel.findOneAndUpdate', [{
			id: createdWithdraw.id
		}, {
			$set: {
				state: WithdrawConstant.WITHDRAW_STATE.SUCCEEDED,
				payment: paymentResponse.payment
			}
		}, {
			new: true
		}]);

		if (_.get(createdWithdraw, 'id', 0) < 1) {
			this.logger.info(`[eWalletWithdraw] Withdraw SDK -> Update CreatedWithdraw Error After Pay Wallet Succeeded: ${JSON.stringify(createdHistory)}`);
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.FAILED,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.FAILED.toString())
			};
		}

		createdHistory = await this.broker.call('v1.eWalletHistory.update', {
			fields: {
				id: createdHistory.id
			},
			updateInfo: {
				state: FrontendConstant.HISTORY_STATE.SUCCEEDED,
				'service.state': WithdrawConstant.WITHDRAW_STATE.SUCCEEDED,
				'service.data.state': WithdrawConstant.WITHDRAW_STATE.SUCCEEDED,
				changed: '-',
				balance: _.get(paymentResponse, 'data.balance', {}),
				payment: paymentResponse.payment,
				publishedAt: new Date()
			}
		});
		if (_.get(createdHistory, 'code', null) !== 202710) {
			this.logger.info(`[eWalletWithdraw] Withdraw SDK -> Update History Error After Pay Wallet Succeeded: ${JSON.stringify(createdHistory)}`);
			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.FAILED,
				message: this.__(ErrorCodeConstant.WITHDRAW_SDK.FAILED.toString())
			};
		}

		createdHistory = createdHistory.data;

		const transportObj = {
			accountId: authInfo.accountId,
			appId: authInfo.appId || 0,
			amount: createdWithdraw.amount,
			description: 'Rut tien tu vi PayME.',
			service: {
				code: serviceCode,
				type: FrontendConstant.SERVICES.WITHDRAW,
				transaction: createdWithdraw.transaction
			},
			transport: transportInput
		};
		this.logger.info(`[ewalletWithdraw] WithdrawSDK >> transportObj: ${JSON.stringify(transportObj)}`);
		const transportResponse = await this.broker.call('v1.eWalletTransport.transfer', transportObj);
		this.logger.info(`[ewalletWithdraw] WithdrawSDK >> transportResponse: ${JSON.stringify(transportResponse)}`);
		// const transportResponse = {
		// 	state: TransportConstant.STATE.SUCCEEDED,
		// 	transportInfo: {
		// 		description: 'TPBANK-8201'
		// 	}
		// };
		if (transportResponse.state !== TransportConstant.STATE.SUCCEEDED) {
			let transportUpdateState = WithdrawConstant.WITHDRAW_STATE.FAILED;

			if (transportResponse.state === TransportConstant.STATE.TIME_OUT) {
				transportUpdateState = WithdrawConstant.WITHDRAW_STATE.PENDING;
			}

			createdWithdraw = await this.broker.call('v1.eWalletWithdrawModel.findOneAndUpdate', [{
				id: createdWithdraw.id,
				state: WithdrawConstant.WITHDRAW_STATE.SUCCEEDED
			}, {
				$set: {
					state: transportUpdateState,
					transport: transportResponse.transportInfo
				}
			}, {
				new: true
			}]);
			if (_.get(createdWithdraw, 'id', 0) < 1) {
				this.logger.info(`[eWalletWithdraw] Withdraw SDK -> Update CreatedWithdraw Error After Transport Failed: ${JSON.stringify(createdWithdraw)}`);
			} else if (transportResponse.state !== TransportConstant.STATE.TIME_OUT) {
				const formatedAmount = Numeral(createdWithdraw.total).format('0,0');
				const refundObj = {
					paymentId: _.get(paymentResponse, 'payment.id', null),
					appId: createdWithdraw.appId,
					amount: createdWithdraw.total,
					serviceCode,
					serviceId: createdWithdraw.id,
					description: `Bạn được hoàn ${formatedAmount}đ do rút tiền thất bại.`
				};
				this.logger.info(`[eWalletWithdraw] WithdrawSDK RefundObj: ${JSON.stringify(refundObj)}`);
				const refundResponse = await this.broker.call('v1.eWalletWithdraw.refund', refundObj);
				this.logger.info(`[eWalletWithdraw] WithdrawSDK refundResponse: ${JSON.stringify(refundResponse)}`);
				if (refundResponse.state !== PaymentConstant.STATE.SUCCEEDED) {
					const alertMsg = `Hoàn tiền thất bại cho dịch vụ rút tiền
				\nwithdrawId: ${createdWithdraw.id}
				\nrefundResponse: ${JSON.stringify(refundResponse)}`;
					try {
						await this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: [TelegramGroup] });
					} catch (error) {
						this.logger.info(`[ewalletWithdraw] withdrawSDK >> Notify Error ${JSON.stringify(alertMsg)}${error}`);
					}
				} else {
					let historySendNotification = refundResponse.history;
					if (_.get(historySendNotification, 'toObject', null)) {
						historySendNotification = historySendNotification.toObject();
					}

					// historySendNotification.clientId = _.get('authInfo.clientId', null);
					await this.broker.call('v1.ewalletNotification.createNotification', {
						accountId: createdWithdraw.accountId,
						message: `Bạn được hoàn ${formatedAmount}đ do rút tiền thất bại.`,
						extraData: historySendNotification,
						title: 'Hoàn tiền ví PayME'
					});
				}
			}

			createdHistory = await this.broker.call('v1.eWalletHistory.update', {
				fields: { id: createdHistory.id },
				updateInfo: {
					state: transportUpdateState,
					'service.state': transportUpdateState,
					'service.data.state': transportUpdateState,
					transport: transportResponse.transportInfo
				}
			});
			if (_.get(createdHistory, 'code', null) !== 202710) {
				this.logger.info(`[eWalletWithdraw] Withdraw SDK -> Update History Error After Transport Failed: ${JSON.stringify(createdHistory)}`);
			}

			return {
				code: ErrorCodeConstant.WITHDRAW_SDK.FAILED,
				message: transportResponse.message || this.__(ErrorCodeConstant.WITHDRAW_SDK.FAILED.toString())
			};
		}

		createdWithdraw = await this.broker.call('v1.eWalletWithdrawModel.findOneAndUpdate', [{
			id: createdWithdraw.id
		}, {
			$set: {
				transport: transportResponse.transportInfo
			}
		}, {
			new: true
		}]);
		if (_.get(createdWithdraw, 'id', 0) < 1) {
			this.logger.info(`[eWalletWithdraw] Withdraw SDK -> Update createdWithdraw Error After Transport Succeeded: ${JSON.stringify(createdWithdraw)}`);
		}

		serviceName = `Rút tiền về ${_.get(transportResponse, 'transportInfo.description', '')}`;

		createdHistory = await this.broker.call('v1.eWalletHistory.update', {
			fields: { id: createdHistory.id },
			updateInfo: {
				transport: transportResponse.transportInfo,
				'service.code': serviceCode,
				'service.name': serviceName
			}
		});
		if (_.get(createdHistory, 'code', 0) !== 202710) {
			this.logger.info(`[eWalletWithdraw] Withdraw SDK -> Update History Error After Transport Succeeded: ${JSON.stringify(createdHistory)}`);
		}

		createdHistory = _.get(createdHistory, 'data', {});

		const formatedAmount = Numeral(createdWithdraw.amount).format('0,0');

		const notifyTitle = `Rút tiền về ${_.get(transportResponse, 'transportInfo.description', '')}`;
		const pushedData = createdHistory; // createdHistory.toObject();
		pushedData.service.name = notifyTitle;

		// pushedData.clientId = _.get(context, 'authInfo.clientId', null);
		await this.broker.call('v1.ewalletNotification.createNotification', {
			accountId: authInfo.accountId,
			message: `Bạn đã rút thành công ${formatedAmount}đ về ${_.get(transportResponse, 'transportInfo.description', '')}.`,
			extraData: pushedData,
			title: notifyTitle
		});

		if (payload.remember) {
			let bankInfo = null;
			if (transportResponse.method === TransportConstant.METHOD.BANK_CARD) {
				bankInfo = {
					swiftCode: _.get(transportResponse, 'bankInfo.swiftCode'),
					cardNumber: _.get(transportResponse, 'bankInfo.cardNumber'),
					cardHolder: _.get(transportResponse, 'bankInfo.cardHolder'),
					bankName: _.get(transportResponse, 'bankInfo.bankName'),
					recentType: FrontendConstant.TRANSACTION_METHOD_TYPE.ATM_CARD
				};
			} else if (transportResponse.method === TransportConstant.METHOD.BANK_ACCOUNT) {
				bankInfo = {
					swiftCode: _.get(transportResponse, 'bankInfo.swiftCode'),
					cardNumber: _.get(transportResponse, 'bankInfo.bankAccountNumber'),
					cardHolder: _.get(transportResponse, 'bankInfo.bankAccountName'),
					bankName: _.get(transportResponse, 'bankInfo.bankName'),
					recentType: FrontendConstant.TRANSACTION_METHOD_TYPE.BANK_ACCOUNT
				};
			}

			if (bankInfo) {
				const recent = await this.broker.call('v1,eWalletRecentCardModel.findOne', [{
					accountId: createdWithdraw.accountId,
					'bankInfo.cardNumber': bankInfo.cardNumber,
					'bankInfo.swiftCode': bankInfo.swiftCode,
					source: FrontendConstant.TRANSACTION_TYPE.WITHDRAW,
					type: bankInfo.recentType
				}]);
				if (_.get(recent, 'id', null) === null) {
					await this.broker.call('v1,eWalletRecentCardModel..create', [{
						accountId: createdWithdraw.accountId,
						bankInfo: {
							swiftCode: bankInfo.swiftCode,
							bankName: bankInfo.bankName,
							cardNumber: bankInfo.cardNumber,
							cardHolder: bankInfo.cardHolder
						},
						count: 1,
						type: bankInfo.recentType,
						source: FrontendConstant.TRANSACTION_TYPE.WITHDRAW
					}]);
				} else {
					await this.broker.call('v1.eWalletRecentCardModel.updateOne', [{
						id: recent.id
					}, {
						$inc: { count: 1 }
					}]);
				}
			}
		}
		const notifyLogs = {
			accountId: authInfo.fullname,
			amount: createdHistory.amount,
			total: createdHistory.total,
			fee: createdHistory.fee,
			discount: createdHistory.discount,
			cashback: createdHistory.cashback
		};
		this.broker.call('v1.utility.notifyTelegram', {
			message: `Tạo lệnh thanh toán Rút tiền từ ${authInfo.fullname} thành công
\n transaction: ${withdrawTransaction}
\n logs: ${JSON.stringify(notifyLogs)}`,
			buttons: [],
			receiverList: [TelegramGroup]
		});
		return {
			code: ErrorCodeConstant.WITHDRAW_SDK.SUCEEDED,
			message: this.__(ErrorCodeConstant.WITHDRAW_SDK.SUCEEDED.toString()),
			data: {
				historyId: createdHistory.id,
				transaction: createdHistory.transaction
			}
		};
	} catch (error) {
		this.logger.info(`[eWalletWithdraw] Withdraw SDK Error ${error}`);
		return {
			code: ErrorCodeConstant.WITHDRAW_SDK.FAILED,
			message: this.__(ErrorCodeConstant.WITHDRAW_SDK.FAILED.toString())
		};
	}
};
