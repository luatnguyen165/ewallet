const _ = require('lodash');
const moment = require('moment');
const Numeral = require('numeral');
const Decimal = require('decimal.js');
const { MoleculerError } = require('moleculer').Errors;
const ErrorCode = require('../constants/errorCode.constant');
const WithdrawConstant = require('../constants/withdraw.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	const response = {
		code: ErrorCode.VERIFY_WITHDRAW_CODE.FAILED,
		message: 'Rút tiền thất bại. Vui lòng thử lại sau.(VER_E00)'
	};

	try {
		const {
			phone
		} = _.get(ctx, 'params.body');
		const payload = _.get(ctx, 'params.body');

		const authInfo = this.GetAuthInfo(ctx.meta);

		const eWalletAppInfo = await Broker.call('v1.ewalletAppInfoModel.findOne', [{
			merchantId: authInfo.merchantId
		}]);

		if (_.get(eWalletAppInfo, 'id', null) === null) {
			response.message = 'Không tìm thấy thông tin kết nối';
			return response;
		}

		const checkPhone = this.PhoneUtility(phone);
		if (!checkPhone.isValid) {
			response.message = 'Số điện thoại không hợp lệ';
			return response;
		}
		const { phoneFormatted } = checkPhone;

		const connectedUserInfo = await this.broker.call('v1.eWalletConnectedUserModel.findOne', [{
			phone: phoneFormatted,
			appId: eWalletAppInfo.id
		}]);

		if (_.get(connectedUserInfo, 'id', null) === null) {
			response.message = 'Số điện thoại này chưa liên kết với PayME (1)';
			return response;
		}

		const accountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [{
			phone: phoneFormatted
		}]);

		if (_.get(connectedUserInfo, 'id', null) === null) {
			response.message = 'Số điện thoại chưa liên kết với PayME (2)';
			return response;
		}

		const dataWithdraw = {
			accountId: accountInfo.id,
			transaction: payload.transaction,
			appId: eWalletAppInfo.id,
			state: WithdrawConstant.WITHDRAW_STATE.PENDING
		};

		let withdrawCreated = await Broker.call('v1.eWalletWithdrawModel.findOne', [dataWithdraw]);

		if (_.get(withdrawCreated, 'id', null) === null) {
			response.message = 'Xác thực rút tiền thất bại. Giao dịch này không tồn tại hoặc đã hoàn thành trước đó. (1)';
			return response;
		}

		const dataHistory = {
			accountId: accountInfo.id,
			appId: eWalletAppInfo.id,
			'service.transaction': payload.transaction,
			'service.type': 'WITHDRAW',
			'service.method': 'LINKED',
			state: WithdrawConstant.WITHDRAW_STATE.PENDING
		};

		let historyCreated = await Broker.call('v1.eWalletHistoryModel.findOne', [dataHistory]);

		if (_.get(historyCreated, 'id', null) === null) {
			response.message = 'Xác thực rút tiền thất bại. Giao dịch này không tồn tại hoặc đã hoàn thành trước đó. (2)';
			return response;
		}

		const RedisCache = this.RedisCache();
		const keyCheck = `CountWrongOTPWithdraw-${phoneFormatted}`;

		const countCheckLock = _.toNumber(await RedisCache.get({ key: keyCheck })) || 0;

		if (countCheckLock >= 3) {
			const timeLock = _.toNumber(await RedisCache.ttl({ key: keyCheck })) || 0;

			let resultText = '';
			const momentTime = moment.duration(timeLock, 'seconds');

			const hours = momentTime.hours();
			const minutes = momentTime.minutes();
			const seconds = momentTime.seconds();
			if (hours > 0) {
				resultText = `${hours} giờ ${minutes} phút ${seconds} giây`;
			} else if (minutes > 0) {
				resultText = `${minutes} phút ${seconds} giây`;
			} else resultText = `${seconds} giây`;

			response.message = `SDT đã sai mã xác nhận nhiều lần. Tạm khoá : ${resultText}`;
			return response;
		}
		const activeCode = await this.broker.call('v1.ewalletActiveCodeModel.findOne', [{
			phone: phoneFormatted,
			code: payload.otp,
			source: WithdrawConstant.WITHDRAW_SOURCE
		}]);

		if (_.get(activeCode, 'id', null) === null) {
			await RedisCache.set({ key: keyCheck, value: countCheckLock + 1, ttl: 60 });
			response.message = 'Mã xác nhận không đúng';
			return response;
		}

		if (moment(activeCode.expiredAt).isBefore(moment(new Date()))) {
			response.message = 'Mã xác nhận đã hết hạn. Vui lòng tạo giao dịch mới';
			return response;
		}
		const linkedInfo = await this.broker.call('v1.eWalletLinkedModel.findOne', [{
			id: _.get(withdrawCreated, 'extraData.linkedId', 0),
			accountId: accountInfo.id
		}]);

		let walletInfo = null;
		try {
			walletInfo = await this.WalletLibs.Information(accountInfo.id);
		} catch (error) {
			Logger(`[WITHDRAW] => [INFO_WALLET_ERROR]: ${error}`);
			response.message = 'Rút tiền thất bại. Vui lòng thử lại (VER_E01)';
			return response;
		}

		if (_.get(walletInfo, 'balance', 0) < withdrawCreated.total) {
			response.message = 'Số dư của bạn không đủ để thực hiện giao dịch';
			return response;
		}

		const paymentParams = {
			accountInfo,
			appId: eWalletAppInfo.id,
			transaction: withdrawCreated.transaction,
			amount: withdrawCreated.total,
			walletBalance: walletInfo.balance
		};

		const PayWallet = await this.PayWallet(paymentParams);

		if (PayWallet.state !== WithdrawConstant.PAYMENT_STATE.SUCCEEDED) {
			withdrawCreated = await Broker.call('v1.eWalletWithdrawModel.updateOne', [
				{ id: withdrawCreated.id },
				{
					state: WithdrawConstant.WITHDRAW_STATE.FAILED,
					payment: PayWallet.payment
				}
			]);

			historyCreated = await Broker.call('v1.eWalletHistoryModel.updateOne', [
				{ id: historyCreated.id },
				{
					state: WithdrawConstant.WITHDRAW_STATE.FAILED,
					'service.state': WithdrawConstant.WITHDRAW_STATE.FAILED,
					'service.data.state': WithdrawConstant.WITHDRAW_STATE.FAILED,
					payment: PayWallet.payment
				}
			]);

			response.message = _.get(PayWallet, 'message', 'Rút tiền thất bại. Vui lòng thử lại (VER_E02)');
			return response;
		}

		// Withdraw Succeed, Dich vu cung succeed
		withdrawCreated = await Broker.call('v1.eWalletWithdrawModel.findOneAndUpdate', [
			{ id: withdrawCreated.id },
			{
				state: WithdrawConstant.WITHDRAW_STATE.SUCCEEDED,
				payment: PayWallet.payment
			}, { new: true }
		]);

		historyCreated = await Broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [
			{ id: historyCreated.id },
			{
				'service.state': WithdrawConstant.WITHDRAW_STATE.SUCCEEDED,
				'service.data.state': WithdrawConstant.WITHDRAW_STATE.SUCCEEDED,
				changed: '-',
				balance: PayWallet.balance,
				payment: PayWallet.payment,
				publishedAt: new Date().toISOString()
			},
			{ new: true }
		]);

		// Goi Transport
		let transportResponse = null;
		try {
			transportResponse = await Broker.call('v1.eWalletWithdraw.transportLinked', {
				linkedId: linkedInfo.id,
				amount: withdrawCreated.amount,
				phone: accountInfo.phone,
				accountId: accountInfo.id,
				appId: eWalletAppInfo.id,
				transaction: withdrawCreated.transaction
			});
		} catch (e) {
			console.log(e);
		}

		let transportUpdateState = WithdrawConstant.TRANSPORT_STATE.FAILED;
		if (transportResponse.state === GeneralConstant.TRANSPORT_STATE.FAILED) {
			const formatedAmount = Numeral(withdrawCreated.total).format('0,0');
			const paramsRefund = {
				paymentId: PayWallet.payment.id,
				appId: eWalletAppInfo.id,
				amount: withdrawCreated.amount,
				serviceCode: 'WITHDRAW',
				serviceId: withdrawCreated.id,
				description: `Bạn được hoàn ${formatedAmount}đ do rút tiền thất bại.`
			};
			// Goi action hoan tien
			const refundWithdraw = await Broker.call('v1.eWalletWithdraw.refund', {
				...paramsRefund
			});

			if (refundWithdraw.state !== GeneralConstant.REFUND_STATE.SUCCEEDED) {
				try {
					const alertMsg = `Hoàn tiền thất bại cho dịch vụ rút tiền
					\nwithdrawId: ${withdrawCreated.id}
					\nrefundResponse: ${JSON.stringify(refundWithdraw)}`;
					Broker.call('v1.utility.notifyTelegram', {
						message: alertMsg,
						buttons: [],
						receiverList: ['api']
					});
				} catch (e) {
					Logger('[VERIFY_WITHDRAW_FAILED] => [NOTIFY_FAILED_WHEN_REFUND]');
				}
			}
			response.message = _.get(transportResponse, 'message', 'Rút tiền thất bại');
		} else {
			response.code = ErrorCode.VERIFY_WITHDRAW_CODE.SUCCEED;
			response.message = 'Rút tiền thành công';
			transportUpdateState = WithdrawConstant.TRANSPORT_STATE.SUCCEEDED;
		}

		const serviceName = `Rút tiền về ${_.get(transportResponse, 'transportInfo.description', '')}`;

		withdrawCreated = await Broker.call('v1.eWalletWithdrawModel.findOneAndUpdate', [
			{ id: withdrawCreated.id },
			{
				state: transportUpdateState,
				transport: transportResponse.transportInfo
			}
		]);

		historyCreated = await Broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [
			{ id: historyCreated.id },
			{
				state: transportUpdateState,
				'service.state': transportUpdateState,
				'service.data.state': transportUpdateState,
				'service.name': serviceName,
				transport: transportResponse.transportInfo
			}
		]);

		try {
			const alertMsg = `[S2S] Tạo lệnh thanh toán Rút tiền từ ${accountInfo.fullname} thành công
			\n transaction: ${withdrawCreated.transaction}
			\n logs: ${JSON.stringify({
		accountId: authInfo.fullname,
		amount: withdrawCreated.amount,
		total: withdrawCreated.total,
		fee: withdrawCreated.fee,
		discount: historyCreated.discount,
		cashback: historyCreated.cashback
	})}`;
			const nameGroupNoti = process.env.TRANSACTION_NOTIFY_GROUP;
			Broker.call('v1.utility.notifyTelegram', {
				message: alertMsg,
				buttons: [],
				receiverList: [nameGroupNoti]
			});
		} catch (e) {
			Logger('NOTIFY_FAILED');
		}
		response.data = {
			transaction: withdrawCreated.transaction
		};
		// succeed cap nhat
		return response;
	} catch (error) {
		console.log(error);
		Logger('[OPENEWALLET] >>> [WITHDRAW] >> [ERROR] >>', JSON.stringify(error));

		return response;
	}
};
