const _ = require('lodash');
const moment = require('moment');
const GeneralConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;
	const payload = ctx.params;

	const response = {
		state: GeneralConstant.REFUND_STATE.FAILED,
		message: 'Hoàn tiền thất bại'
	};
	try {
		const condition = {
			id: payload.paymentId
		};
		const paymentObj = await Broker.call('v1.eWalletPaymentModel.findOne', [condition]);

		if (!paymentObj) {
			response.message = 'Thanh toán không tồn tại';
			return response;
		}

		if (paymentObj.state !== GeneralConstant.PAYMENT_STATE.SUCCEEDED) {
			response.message = 'Thanh toán chưa thành công không thể hoàn tiền';
			return response;
		}

		const checkTransactionRefund = await Broker.call('v1.eWalletPaymentRefundModel.findOne',
			[{ 'service.transaction': payload.transaction, 'service.code': payload.code }]);

		if (_.get(checkTransactionRefund, 'id', false) !== false) {
			response.message = 'Giao dịch đã được hoàn trước đó';
			return response;
		}

		const refundAmount = paymentObj.total || paymentObj.amount;
		const refundObj = await Broker.call('v1.eWalletPaymentRefundModel.create', [{
			accountId: paymentObj.accountId,
			service: {
				id: payload.serviceId,
				code: payload.serviceCode
			},
			// TO DO Thêm transaction cho Payment V2
			paymentTransaction: paymentObj.service.transaction,
			amount: refundAmount,
			state: GeneralConstant.REFUND_STATE.SUCCEEDED,
			description: paymentObj.description
		}]);

		if (_.get(refundObj, 'id', false) === false) {
			console.log('refundObj ERROR', refundObj);
			response.message = 'Lỗi tạo giao dịch hoàn tiền lỗi';
			return response;
		}

		// thêm lịch sử
		const dataHistory = {
			accountId: paymentObj.accountId,
			service: {
				type: 'REFUND_MONEY',
				method: 'REFUND_MONEY',
				id: refundObj.id,
				transaction: paymentObj.service.transaction,
				state: GeneralConstant.REFUND_STATE.SUCCEEDED
			},
			amount: refundAmount,
			total: refundAmount,
			state: GeneralConstant.REFUND_STATE.SUCCEEDED,
			appId: payload.appId || paymentObj.appId,
			description: payload.description || 'Hoàn tiền giao dịch',
			changed: '+',
			tags: [
				'REFUND_MONEY'
			]
		};
		const createdHistory = await Broker.call('v1.eWalletHistoryModel.create', [dataHistory]);

		if (_.get(createdHistory, 'id', false) === false) {
			console.log('CREATE History Obj Reund ERROR:', createdHistory);
			response.message = 'Lỗi tạo lịch sử giao dịch';
			return response;
		}

		const params = {
			accountId: refundObj.accountId,
			amount: refundAmount,
			description: 'Hoàn tiền vào ví user',
			referData: refundObj
		};
		let depositInfo = null;
		try {
			depositInfo = await this.WalletLibs.Deposit(params);
		} catch (error) {
			Logger(`[REFUND] => [DEPOSIT_WALLET_ERROR]: ${error}`);
			const updatePaymentObj = Broker.call('v1.eWalletPaymentRefundModel.findOneAndUpdate',
				[{ id: refundObj.id },
					{
						state: GeneralConstant.REFUND_STATE.FAILED
					}]);
			if (_.get(updatePaymentObj, 'id', false) === false) {
				console.log('UPDATE REUFUND FAILED ERROR ', JSON.stringify(refundObj));
			}

			const updateHistoryData = {
				publishedAt: moment(new Date()).add(3, 'seconds'),
				state: GeneralConstant.REFUND_STATE.FAILED
			};
			const updateHistoryRefundObj = await Broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [
				{ id: createdHistory.id },
				{ ...updateHistoryData }, { new: true }]);

			if (_.get(updateHistoryRefundObj, 'id', false) === false) {
				console.log('UPDATE HISTORY REFUND FAILED', JSON.stringify(refundObj));
			}
			response.message = 'Hoàn tiền vào ví thất bại';
			return response;
		}

		const updateHistoryData = {
			publishedAt: moment(new Date()).add(3, 'seconds'),
			balance: depositInfo.balance
		};
		const updateHistoryRefundObj = await Broker.call('v1.eWalletHistoryModel.findOneAndUpdate', [{ id: createdHistory.id },
			{ ...updateHistoryData }, { new: true }]);

		// xuống tới đây tức là code = 1 tức thành công
		response.state = GeneralConstant.REFUND_STATE.SUCCEEDED;
		response.refund = refundObj;
		response.history = updateHistoryRefundObj;
		return response;
	} catch (error) {
		Logger('[REFUND] => [EXCEPTION]', error);
		response.state = GeneralConstant.REFUND_STATE.FAILED;
		response.message = error;
		return response;
	}
};
