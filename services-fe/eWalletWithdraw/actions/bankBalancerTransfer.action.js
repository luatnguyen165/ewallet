const _ = require('lodash');
const BankBalancerConstant = require('../constants/bankBalancer.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;

	const payload = ctx.params;
	const { bankInfo } = payload;

	const response = {
		state: GeneralConstant.TRANSPORT_STATE.FAILED,
		message: 'Kết nối tới nhà cung cấp bị lỗi'
	};

	try {
		const BankCodeConfig = await Broker.call('v1.eWalletBankCodeModel.findMany',
			[{ isActive: true }]);

		const bankTransferInfo = _.find(BankCodeConfig, { swiftCode: bankInfo.swiftCode });
		if (_.isNil(bankTransferInfo)) {
			response.message = 'Ngân hàng không đươc hỗ trợ';
			return response;
		}
		const bankTransferCode = _.get(bankTransferInfo, 'card.atm.prefix', null);

		if (_.isNull(bankTransferCode)) {
			response.message = 'Không tìm thấy mã ngân hàng';
			return response;
		}

		let { supplier } = payload;
		if (_.isNil(supplier)) {
			const dataRandomSupplier = await Broker.call('v1.eWalletSettingModel.findOne', [{
				key: 'bank.transfer.rate'
			}]);

			const JsonData = JSON.parse(dataRandomSupplier.value);
			supplier = this.BankBalancerSupplierSelect(JsonData);
		}

		// supplier la OCB thi chan 20000, khong thi lay mac dinh tu trong setting (co the 10000)
		if (supplier === BankBalancerConstant.SUPPLIER.OCB) {
			const AMOUNT_MIN = 20000;
			if (payload.amount < AMOUNT_MIN) {
				const t = Math.round(AMOUNT_MIN).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
				response.message = `Số tiền nhỏ hơn định mức ${t}`;
				return response;
			}
		} else {
			const AMOUNT_MIN = 10000;
			if (payload.amount < AMOUNT_MIN) {
				const t = Math.round(AMOUNT_MIN).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
				response.message = `Số tiền nhỏ hơn định mức ${t}`;
				return response;
			}
		}

		const { transaction } = payload;

		const createTransfer = await Broker.call('v1.eWalletTransferBankModel.create', [{
			serviceTransaction: payload.transaction,
			transaction,
			method: payload.method,
			amount: payload.amount,
			state: BankBalancerConstant.STATE.NEW,
			supplier,
			// Cập nhật các trạng thái nạp tiên
			stateAt: [{
				state: BankBalancerConstant.STATE.NEW,
				createdAt: new Date().toISOString()
			}],
			to: bankInfo
		}]);

		let result = {};
		switch (supplier) {
			// OCB
			case BankBalancerConstant.SUPPLIER.OCB: {
				if (bankInfo.swiftCode === 'ORCOVNVX') {
					const paramsSend = {
						accountNumber: bankInfo.accountNumber,
						amount: payload.amount,
						description: payload.description
					};
					result = await this.OCBBankLibs.TransferToOCB(paramsSend);
				} else {
					const paramsSend = {
						method: bankInfo.method,
						amount: payload.amount,
						cardNumber: _.get(bankInfo, 'cardNumber', ''),
						accountNumber: _.get(bankInfo, 'accountNumber', ''),
						description: payload.description,
						bankCode: bankTransferCode
					};
					result = await this.OCBBankLibs.Transfer247(paramsSend);
				}
				break;
			}

			// // BIDV
			case BankBalancerConstant.SUPPLIER.BIDV: {
				// Chuyen den ngan hang BIDV
				if (bankInfo.swiftCode === 'BIDVVNVX') {
					const paramsSend = {
						transaction,
						cardNumber: bankInfo.cardNumber,
						accountNumber: bankInfo.accountNumber,
						amount: payload.amount,
						description: payload.description,
						swiftCode: bankInfo.swiftCode
					};
					result = await this.BIDVBankLibs.TransferBIDV(paramsSend);
				} else {
					const paramsSend = {
						transaction,
						cardNumber: bankInfo.cardNumber,
						accountNumber: bankInfo.accountNumber,
						amount: payload.amount,
						description: payload.description,
						bankCode: bankTransferCode,
						swiftCode: bankInfo.swiftCode
					};
					result = await this.BIDVBankLibs.Transfer247(paramsSend);
				}
				break;
			}

			// BS
			case BankBalancerConstant.SUPPLIER.BS: {
				if (bankInfo.method === 'CARD') {
					const paramsSend = {
						transaction,
						amount: payload.amount,
						description: payload.description,
						cardNumber: bankInfo.cardNumber,
						swiftCode: bankInfo.swiftCode
					};

					result = await this.BSLibs.TransferCard(paramsSend);
				} else {
					const paramsSend = {
						transaction,
						amount: payload.amount,
						description: payload.description,
						accountNumber: bankInfo.accountNumber,
						swiftCode: bankInfo.swiftCode
					};
					result = await this.BSLibs.TransferBank(paramsSend);
				}

				break;
			}
			// BS
			case BankBalancerConstant.SUPPLIER.PG: {
				const paramsSend = {
					transaction,
					amount: payload.amount,
					description: payload.description,
					accountNumber: bankInfo.cardNumber || bankInfo.accountNumber,
					accountName: bankInfo.fullname,
					swiftCode: bankInfo.swiftCode
				};

				result = await this.PGLibs.Payout(paramsSend);

				break;
			}
			default: break;
		}

		const { stateAt } = createTransfer;
		stateAt.push({
			state: result.state,
			createdAt: new Date().toISOString()
		});
		let stateUpdate = BankBalancerConstant.STATE.FAILED;
		if (result.state === GeneralConstant.SUPPLIER_STATE.SUCCEEDED) {
			stateUpdate = BankBalancerConstant.STATE.TRANSFERED;
			response.state = GeneralConstant.TRANSPORT_STATE.SUCCEEDED;
		} else {
			try {
				const alertMsg = `[S2S] Tạo lệnh thanh toán Rút tiền từ sử dụng ${supplier} thất bại
			\n transaction: ${payload.transaction}
			\n logs: ${JSON.stringify(result)}`;
				Broker.call('v1.utility.notifyTelegram', {
					message: alertMsg,
					buttons: [],
					receiverList: ['api']
				});
			} catch (e) {}
		}
		// Cap nhat lai vao bang
		const updated = await Broker.call('v1.eWalletTransferBankModel.findOneAndUpdate', [{
			id: createTransfer.id
		}, {
			stateAt,
			description: _.get(result, 'description', 'Chuyen tien tu ngan hang den ngan hang'),
			clientTransId: _.get(result, 'clientTransId', null),
			state: stateUpdate,
			supplierTransaction: _.get(result, 'transaction', null),
			bankTransactionId: _.get(result, 'bankTransactionId', null),
			total: _.get(result, 'total', payload.amount) || payload.amount,
			exchangeRate: _.get(result, 'exchangeRate', 0) || 0,
			fee: _.get(result, 'fee', 0) || 0,
			taxAmount: _.get(result, 'taxAmount', 0) || 0,
			supplierResponsed: [result.supplierResponse]
		}]);

		response.message = `${supplier}: ${_.get(result, 'message', '')}`;
		// response.state = result.state;
		response.data = {
			message: ` ${supplier} ${_.get(result, 'message', '')}`
		};
		response.supplierResponse = result.supplierResponse;
		return response;
	} catch (error) {
		Logger('[BANK_BANLANCER] => [EXCEPTION] ', error);
		response.message = 'Đã có lỗi xảy ra';
		return response;
	}
};
