const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');

const GeneralConstant = require('../constants/general.constant');
const BankServiceConstant = require('../constants/bankService.constant');

function BSLibrary(config = {}) {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.BIDVBANK_TOKEN;

	const Logger = config.logger;

	async function PostRequest(urlRequest, body, headers = {}) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({
					url: urlRequest,
					timeout: 50000,
					body: JSON.stringify(body),
					headers: {
						Authorization,
						...headers
					}
				}, (err, httpResponse, response) => {
					if (err) {
						reject(err);
					}
					resolve(httpResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}

	return {
		async TransferBank(payload) {
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Rút tiền về tài khoản ngân hàng thất bại. Vui lòng thử lại sau',
				transaction: null,
				supplierResponse: null
			};

			const params = {
				transaction: payload.transaction,
				amount: payload.amount,
				description: payload.description || 'Chuyen tien ve TKNH',
				bankAccountNumber: payload.accountNumber,
				swiftCode: payload.swiftCode
			};

			const urlRequest = `${url}/v1/Transfer/Bank`;

			try {
				const result = await PostRequest(urlRequest, params);
				Logger(`[BS] => [WITHDRAW] => [Params] : ${JSON.stringify({ urlRequest, params })}`);
				Logger(`[BS] => [WITHDRAW] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				response.supplierResponse = _.get(result, 'body', result);
				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Rút tiền thất bại. Vui lòng thử lại sau');
					if (response.message === 'ESOCKETTIMEDOUT') {
						response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					}
					return response;
				}

				response.transaction = _.get(bodyResponse, 'data.bankTransaction', '');
				response.message = 'Rút tiền thành công.';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[BIDVBANK_LIBRARY] => [Deposit: ${urlRequest}] => [Error] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async TransferCard(payload) {
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Rút tiền về tài khoản liên kết thất bại. Vui lòng thử lại sau',
				transaction: null,
				supplierResponse: null
			};

			const params = {
				transaction: payload.transaction,
				amount: payload.amount,
				description: payload.description || 'Chuyen tien ve TKNH',
				cardNumber: payload.cardNumber,
				swiftCode: payload.swiftCode
			};

			const urlRequest = `${url}/v1/Transfer/Card`;

			try {
				const result = await PostRequest(urlRequest, params);
				Logger(`[BS] => [TRANSFER_247] => [Params] : ${JSON.stringify({ urlRequest, params })}`);
				Logger(`[BS] => [TRANSFER_247] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				response.supplierResponse = _.get(result, 'body', result);
				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Rút tiền thất bại. Vui lòng thử lại sau');
					if (response.message === 'ESOCKETTIMEDOUT') {
						response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					}
					return response;
				}

				response.transaction = _.get(bodyResponse, 'data.bankTransaction', '');
				response.message = 'Rút tiền thành công.';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[BIDVBANK_LIBRARY] => [TRANSFER_247: ${urlRequest}] => [Error] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async GetBankAccountName(payload) {
			const response = {
				state: BankServiceConstant.GET_NAME_STATE.FAILED,
				message: null
			};

			// validate params
			try {
				const params = {
					bankAccountNumber: payload.bankAccountNumber,
					swiftCode: payload.swiftCode
				};
				Logger('BS Bank Libs] GetBankAccountName Payload: ', JSON.stringify(params));

				let bsResponsed = await PostRequest(`${url}v1/Transfer/GetRecipientName/BankAccount`, params, {
					Authorization
				});
				Logger('BS Bank Libs] GetBankAccountName Response: ', bsResponsed);
				bsResponsed = JSON.parse(bsResponsed);
				if (bsResponsed.code === 1000) {
					response.state = BankServiceConstant.GET_NAME_STATE.SUCCEEDED;
				}
				response.supplierResponsed = JSON.stringify(bsResponsed);
				response.fullname = _.get(bsResponsed, 'data.fullname', null);
				response.message = _.get(bsResponsed, 'data.message', null) || 'Lấy thông tin thành công';
				return response;
			} catch (error) {
				Logger(error);
				response.state = BankServiceConstant.STATE.FAILED;
				response.message = 'Lấy thông tin thất bại';
				return response;
			}
		},

		async GetBankCardName(payload) {
			const response = {
				state: BankServiceConstant.GET_NAME_STATE.FAILED,
				message: null
			};

			// validate params
			try {
				const params = {
					cardNumber: payload.cardNumber,
					swiftCode: payload.swiftCode
				};
				Logger('BS Bank Libs] GetBankCardName Payload: ', JSON.stringify(params));

				const bsResponsed = await PostRequest(`${this.url}v1/Transfer/GetRecipientName/BankAccount`, params, {
					Authorization
					// Language: Private.languageMap[language]
				});
				Logger('BS Bank Libs] GetBankAccountName Response: ', bsResponsed);
				if (bsResponsed.code === 1000) {
					response.state = BankServiceConstant.GET_NAME_STATE.SUCCEEDED;
				}
				response.supplierResponsed = JSON.stringify(bsResponsed);
				response.fullname = _.get(bsResponsed, 'data.fullname', null);
				response.message = _.get(bsResponsed, 'data.message', null) || 'Lấy thông tin thành công';
				return response;
			} catch (error) {
				Logger(error);
				response.state = BankServiceConstant.STATE.FAILED;
				response.message = 'Lấy thông tin thất bại';
				return response;
			}
		}
	};
}

module.exports = BSLibrary;
