const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');
const GeneralConstant = require('../constants/general.constant');

function PVcomBankLibrary(config = {}) {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.PVCOMBANK_TOKEN;

	const Logger = config.logger;

	async function PostRequest(urlRequest, body, headers = {}) {
		return new Promise((resolve, reject) => {
			try {
				Request.post({
					url: urlRequest,
					timeout: 50000,
					body: JSON.stringify(body),
					headers: {
						Authorization,
						...headers
					}
				}, (err, httpResponse, response) => {
					if (err) {
						reject(err);
					}
					resolve(httpResponse);
				});
			} catch (error) {
				reject(error);
			}
		});
	}
	return {
		async Withdraw(payload) {
			// Response:  code - message - state - transaction - html
			const response = {
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				message: 'Rút tiền PVComBank thất bại. Vui lòng thử lại sau.',
				transaction: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/v1/card/withdraw`;
			const body = {
				trans_id: payload.transaction,
				request_id: Math.round(Math.random() * 100000000000).toString(),
				card_id: payload.cardId,
				amount: payload.amount,
				account_id: _.toString(payload.accountId),
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			Logger(`[PVCOMBANK_LIBRARY] => [Withdraw] => [Params] : ${JSON.stringify({ urlRequest, body })}`);
			try {
				const result = await PostRequest(urlRequest, body);
				Logger(`[PVCOMBANK_LIBRARY] => [Withdraw] => [Response] : ${JSON.stringify({ urlRequest, result })}`);

				response.supplierResponse = _.get(result, 'body', result);

				const bodyResponse = JSON.parse(result.body);
				if (bodyResponse.code !== 1000) {
					response.message = _.get(bodyResponse, 'data.message', 'Thanh toán thất bại. Vui lòng thử lại sau.');
					if (response.message === 'ESOCKETTIMEDOUT') {
						response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					}
					return response;
				}
				response.transaction = _.get(bodyResponse, 'data.trans_id', '');
				response.message = 'Thanh toán thành công.';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[PVCOMBANK_LIBRARY] => [Withdraw] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				if (error.message === 'ESOCKETTIMEDOUT') {
					response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
				}
				return response;
			}
		}
	};
}
module.exports = PVcomBankLibrary;
