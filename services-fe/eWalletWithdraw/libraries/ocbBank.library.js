/* eslint-disable no-unused-expressions */
const Request = require('request');
const _ = require('lodash');
const Moment = require('moment');
const Axios = require('axios');
const Qs = require('qs');
const Jose = require('jose');
const Md5 = require('md5');

const GeneralConstant = require('../constants/general.constant');
// const ResponseCode = require('../constants/code.constant');

function OCBBankLibrary(config = {}) {
	const Logger = config.logger;

	const OCBConfig = {
		url: process.env.OCB_URL,
		clientId: process.env.OCB_CLIENT_ID,
		clientSecret: process.env.OCB_CLIENT_SECRECT,
		username: process.env.OCB_USER_NAME,
		password: process.env.OCB_PASSWORD,
		debitAccount: process.env.OCB_DEBIT_ACCOUNT,
		privateKey: process.env.OCB_PRIVATE_KEY,
		partnerCode: process.env.OCB_PARTNER_CODE,
		xClientCertificate: process.env.OCB_CLIENT_CERTIFICATE
	};

	const { JWK, JWS, errors } = Jose;

	async function PostForm(url, form) {
		try {
			const response = await Axios.post(url, Qs.stringify(form));
			return response;
		} catch (error) {
			return error;
		}
	}

	async function GetToken(scope = 'OCB') {
		const response = {
			isSuccess: false,
			message: 'Lấy thông tin kết nối thất bai.',
			accessToken: null
		};

		const urlRequest = `${OCBConfig.url}/ocb-oauth-provider/oauth2/token`;
		const objForm = {
			grant_type: 'password',
			username: OCBConfig.username,
			password: OCBConfig.password,
			client_id: OCBConfig.clientId,
			client_secret: OCBConfig.clientSecret,
			scope
		};
		let resultGetToken = null;
		try {
			resultGetToken = await PostForm(urlRequest, objForm);
			const accessToken = _.get(resultGetToken, 'data.access_token', null);
			if (resultGetToken.status !== 200 || !!accessToken === false) {
				Logger(`[OCBBANK_LIBRARY] => [GetToken] => [Params] : ${JSON.stringify(objForm)}`);
				Logger(`[OCBBANK_LIBRARY] => [GetToken] => [Response] : ${resultGetToken}`);
				return response;
			}

			response.isSuccess = true;
			response.accessToken = accessToken;
			response.message = 'Lấy thông tin kết nối thành công.';
		} catch (err) {
			Logger(`[OCBBANK_LIBRARY] => [GetToken] => [Error] : ${err}`);
			Logger(`[OCBBANK_LIBRARY] => [GetToken] => [Params] : ${JSON.stringify(objForm)}`);
			Logger(`[OCBBANK_LIBRARY] => [GetToken] => [Response] : ${resultGetToken}`);
		}
		return response;
	}

	function Sign(obj) {
		try {
			const key = Jose.JWK.asKey(OCBConfig.privateKey);
			const payload = JSON.stringify(obj);
			const jwsData = JWS.sign(payload, key, { alg: 'RS256' });
			const xSignature = jwsData.split('.').pop();
			return xSignature;
		} catch (error) {
			Logger(`[OCBBANK_LIBRARY] => [Sign] => [Params] : ${JSON.stringify(obj)}`);
			Logger(`[OCBBANK_LIBRARY] => [Sign] => [Error] : ${error}`);
			return false;
		}
	}

	async function PostRequest(url, payload, headers = {}) {
		const response = {
			isSuccess: false,
			message: 'Kết nối với nhà cung cấp thất bại.',
			data: null
		};
		let resultRequest = null;
		let urlRequest = null;
		let paramsRequest = null;
		try {
			const resultGetToken = await GetToken();
			if (resultGetToken.isSuccess === false) {
				response.message = resultGetToken.message;
				response.data = resultGetToken;
				return response;
			}
			const { accessToken } = resultGetToken;

			urlRequest = url;
			paramsRequest = {
				...payload,
				trace: {
					clientTransId: Md5(Moment().format('YYYYMMDDHHmmssSSS') + _.random(111111, 999999)),
					clientTimestamp: Moment().format('YYYYMMDDHHmmssSSS')
				}
			};

			const XSignature = Sign(paramsRequest);
			resultRequest = await Axios.post(urlRequest, paramsRequest, {
				headers: _.merge({
					Authorization: `Bearer ${accessToken}`,
					'X-Client-Certificate': OCBConfig.xClientCertificate,
					'X-Signature': XSignature,
					'X-IBM-Client-Id': OCBConfig.clientId,
					'X-IBM-Client-Secret': OCBConfig.clientSecret
				}, headers)
			});

			response.isSuccess = true;
			response.resultRequest = _.get(resultRequest, 'data', {});
			response.message = 'Thành công.';
			response.data = {
				errCode: _.get(resultRequest, 'data.data.error.code', 1),
				status: _.get(resultRequest, 'status', null),
				message: _.get(resultRequest, 'statusText', null),
				transaction: _.get(resultRequest, 'data.trace.bankRefNo', null),
				clientTransId: _.get(resultRequest, 'data.trace.clientTransId', null),
				fee: _.get(resultRequest, 'data.data.transaction.transactionFee', 0),
				taxAmount : _.get(resultRequest, 'data.data.transaction.taxAmount', 0),
				total : _.get(resultRequest, 'data.data.transaction.totalDebitedAmount', 0),
				bankTransactionId: _.get(resultRequest, 'data.data.transaction.bankTransactionId', null),
				extraData: {
					dataRequest: _.get(resultRequest, 'config.data', null),
					data: _.get(resultRequest, 'data.data', null),
					ewallet: _.get(resultRequest, 'data.data.ewallet', null),
					trace: _.get(resultRequest, 'data.trace', null)
				}
			};
			return response;
		} catch (error) {
			if (_.get(error, 'response', null) !== null) {
				response.data = {
					status: _.get(error, 'response.status', null),
					message: _.get(error, 'response.statusText', null),
					extraData: {
						dataRequest: _.get(error, 'response.config.data', null),
						messageDetail: _.get(error, 'response.data.moreInformation', null),
						error: _.get(error, 'response.data.error', null)
					}
				};
			}
			if (error.code === 'ECONNABORTED') {
				response.data = error;
			}
			Logger(`[OCBBANK_LIBRARY] => [PostRequest] => [Params] : ${JSON.stringify(paramsRequest)}`);
			// console.log(_.get(error, 'response.data.error', null));
			Logger(`[OCBBANK_LIBRARY] => [PostRequest] => [Error] : ${error.message}`);
			return response;
		}
	}

	return {
		async Withdraw(payload = {}) {
			const response = {
				message: 'Rút tiền về OCB thất bại. Vui lòng thử lại sau.',
				transaction: null,
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				supplierResponse: null
			};

			const urlRequest = `${OCBConfig.url}/v1/ewallet/receive-money-ewallet`;
			const paramsRequest = {
				data: {
					rechargeEwallet: {
						ewalletLinkId: payload.cardId,
						mobilePhone: payload.phone,
						partnerCode: OCBConfig.partnerCode,
						tranferAmount: payload.amount,
						transferDescription: payload.description || 'Rut tien ve the lien ket'
					}
				}
			};

			try {
				const resultRequest = await PostRequest(urlRequest, paramsRequest);
				response.supplierResponse = JSON.stringify(resultRequest.resultRequest);
				Logger(`[OCBBANK_LIBRARY] => [Withdraw] => [Params] : ${JSON.stringify(paramsRequest)}`);
				Logger(`[OCBBANK_LIBRARY] => [Withdraw] => [Response] : ${JSON.stringify(resultRequest.resultRequest)}`);
				const responseStatus = _.get(resultRequest, 'data.status', -1);
				if (resultRequest.isSuccess === false || responseStatus !== 200) {
					const errorCode = _.toNumber(_.get(resultRequest, 'data.extraData.error.code', 0));
					const message = GeneralConstant.OCB_MESSAGE[errorCode] || response.message;
					response.message = `[OCB] ${message}`;
					return response;
				}
				response.bankTransactionId = _.get(resultRequest, 'data.bankTransactionId', null);
				response.transaction = _.get(resultRequest, 'data.transaction', null);
				response.message = 'Rút tiền thành công.';
				response.referenceTransaction = {
					clientTransId: _.get(resultRequest, 'data.clientTransId', null),
					bankTransactionId: _.get(resultRequest, 'data.bankTransactionId', null),
					supplierTransaction: _.get(resultRequest, 'data.transaction', null)
				};
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;
				return response;
			} catch (error) {
				Logger(`[OCBBANK_LIBRARY] => [Withdraw: ${urlRequest}] => [Error] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
			}
			return response;
		},
		async Transfer247(payload = {}) {
			const response = {
				message: 'Rút tiền thất bại. Vui lòng thử lại sau',
				transaction: null,
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				supplierResponse: null
			};

			const urlRequest = `${OCBConfig.url}/v1/payment/fastTransfers`;
			const paramsRequest = {
				data: {
					fastTransfer: {
						sourceAccountNumber: OCBConfig.debitAccount,
						payeeType: payload.type,
						tranferAmount: payload.amount,
						transferDescription: payload.description || 'Rut tien ve tai khoan OCB',
						payeeAccountNumber: payload.accountNumber,
						payeeCardNumber: payload.cardNumber,
						bankCode: payload.bankCode
					}
				}
			};

			try {
				const resultRequest = await PostRequest(urlRequest, paramsRequest);
				response.supplierResponse = JSON.stringify(resultRequest.resultRequest);
				Logger(`[OCBBANK_LIBRARY] => [Transfer_247] => [Params] : ${JSON.stringify(paramsRequest)}`);
				Logger(`[OCBBANK_LIBRARY] => [Transfer_247] => [Response] : ${JSON.stringify(resultRequest.resultRequest)}`);
				const responseStatus = _.get(resultRequest, 'data.status', -1);
				if (resultRequest.isSuccess === false || responseStatus !== 200) {
					const errorCode = _.toNumber(_.get(resultRequest, 'data.extraData.error.code', 0));
					const message = GeneralConstant.OCB_MESSAGE[errorCode] || response.message;
					response.message = `[OCB] ${message}`;
					return response;
				}
				response.fee = _.get(resultRequest, 'data.fee', 0);
				response.taxAmount = _.get(resultRequest, 'data.taxAmount', 0);
				response.total = _.get(resultRequest, 'data.total', 0);
				response.clientTransId = _.get(resultRequest, 'data.clientTransId', null);
				response.transaction = _.get(resultRequest, 'data.transaction', null);
				response.message = 'Rút tiền thành công';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;
				return response;
			} catch (error) {
				Logger(`[OCBBANK_LIBRARY] => [Transfer_247: ${urlRequest}] => [Error] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
			}
			return response;
		},
		async TransferToOCB(payload = {}) {
			const response = {
				message: 'Rút tiền thất bại. Vui lòng thử lại sau.',
				transaction: null,
				state: GeneralConstant.SUPPLIER_STATE.FAILED,
				supplierResponse: null
			};

			const urlRequest = `${OCBConfig.url}/v1/payment/internalTransfers`;
			const paramsRequest = {
				data: {
					internalTransfer: {
						fromAccountNumber: OCBConfig.debitAccount,
						tranferAmount: payload.amount,
						transferDescription: payload.description || 'Rut tien ve tai khoan OCB',
						toAccountNumber: payload.cardNUmber || payload.accountNumber
					}
				}
			};

			try {
				const resultRequest = await PostRequest(urlRequest, paramsRequest);
				response.supplierResponse = JSON.stringify(resultRequest.resultRequest);
				Logger(`[OCBBANK_LIBRARY] => [Transfer_OCB] => [Params] : ${JSON.stringify(paramsRequest)}`);
				Logger(`[OCBBANK_LIBRARY] => [Transfer_OCB] => [Response] : ${JSON.stringify(resultRequest.resultRequest)}`);
				const responseStatus = _.get(resultRequest, 'data.status', -1);
				if (resultRequest.isSuccess === false || responseStatus !== 200) {
					const errorCode = _.toNumber(_.get(resultRequest, 'data.extraData.error.code', 0));
					const message = GeneralConstant.OCB_MESSAGE[errorCode] || response.message;
					response.message = `[OCB] ${message}`;
					return response;
				}
				response.transaction = _.get(resultRequest, 'data.transaction', null);
				response.fee = _.get(resultRequest, 'data.fee', 0);
				response.taxAmount = _.get(resultRequest, 'data.taxAmount', 0);
				response.total = _.get(resultRequest, 'data.total', 0);
				response.clientTransId = _.get(resultRequest, 'data.clientTransId', null);
				response.message = 'Rút tiền thành công.';
				response.state = GeneralConstant.SUPPLIER_STATE.SUCCEEDED;
				return response;
			} catch (error) {
				Logger(`[OCBBANK_LIBRARY] => [Transfer_OCB: ${urlRequest}] => [Error] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
			}
			return response;
		}
	};
}
module.exports = OCBBankLibrary;
