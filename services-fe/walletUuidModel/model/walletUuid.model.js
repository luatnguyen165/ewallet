const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const walletUUIDConstant = require('../constants/walletUuid.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	uuid: {
		type: String,
		required: true
	},
	prefix: {
		type: String,
		required: true
	},
	info: {
		type: Object,
		default: null
	},
	state: {
		type: String,
		enum: _.values(walletUUIDConstant.UUID_STATE),
		default: walletUUIDConstant.UUID_STATE.NEW
	}
}, {
	collection: 'Service_Uuid',
	versionKey: false,
	timestamps: true
});

Schema.index({ uuid: 1, prefix: 1, state: 1 }, { index: true });
Schema.index({ prefix: 1, uuid: 1 }, { unique: true });
Schema.index({ prefix: 1, state: 1 });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
