const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		default: null
	},
	customerBillId: {
		type: Number,
		default: null
	},
	amount: {
		type: Number,
		required: true
	},
	total: {
		type: Number,
		default: null
	},
	fee: {
		type: Number,
		default: 0
	},
	transaction: {
		type: String,
		required: true,
		unique: true
	},
	bankTransaction: {
		type: String,
		default: null
	},
	partnerTransaction: {
		type: String,
		default: null
	},
	description: {
		type: String,
		default: null
	},
	customerInfo: {
		id: String,
		fullname: String,
		address: String
	},
	type: {
		type: String
	},
	supplierInfo: {
		fullname: String,
		code: String
	},
	// Chỉ dùng cho trường hợp thanh toán bill học phí
	studentInfo: {
		studentName: String,
		schoolName: String,
		schoolCode: String,
		className: String,
		classCode: String
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.BILL_TRANSACTION_STATE),
		default: GeneralConstant.BILL_TRANSACTION_STATE.PENDING
	},
	gateway: {
		type: String,
		default: null
	},
	paymentPeriod: {
		type: Array,
		default: []
	},
	supplierResponsed: Object,
	payment: Object,
	extraData: Object
}, {
	collection: 'Service_Bill',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
