const moleculerI18n = require('moleculer-i18n-js');
const path = require('path');
const moleculerRabbitmq = require('moleculer-rabbitmq');
const _ = require('lodash');
const GeneralConstant = require('./constants/general.constant');

const queueMixin = moleculerRabbitmq({
	connection: process.env.RABBITMQ_URI,
	asyncActions: true
});
module.exports = {
	name: 'ewalletNotification',

	version: 1,

	mixins: [moleculerI18n, queueMixin],

	i18n: {
		directory: path.join(__dirname, 'locales'),
		locales: ['vi', 'en'],
		defaultLocale: 'vi'
	},

	/**
	 * Settings
	 */
	settings: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		pushNotification: {
			queue: {
				amqp: {
					prefetch: 1
				},
				retry: {
					max_retry: 3,
					delay: (retryCount) => retryCount * 5000
				}
			},
			dedupHash: (ctx) => `CREATE_NOTIFICATION_QUEUE_${_.get(ctx, 'params.accountId', null)}${_.get(ctx, 'params.extraData.service.transaction', null)}`,
			params: {
				accountId: 'number',
				message: 'string',
				extraData: 'object',
				isBroadcast: 'boolean|default:false',
				title: 'string'
			},
			handler: require('./actions/pushNotification.action')
		},
		createNotification: {
			params: {
				accountId: 'number',
				message: 'string',
				extraData: 'object',
				isBroadcast: 'boolean|default:false',
				title: 'string'
			},
			timeout: 60000,
			handler: require('./actions/createNotification.action')
		},
		editTagOneSignal: {
			queue: {
				amqp: {
					prefetch: 1
				},
				retry: {
					max_retry: 3,
					delay: (retryCount) => retryCount * 5000
				}
			},
			params: {
				tags: 'object',
				clientId: 'string'
			},
			dedupHash: (ctx) => 'EDIT_TAG_ONESIGNAL',
			timeout: 60000,
			handler: async (ctx) => {
				const { tags, clientId } = ctx.params;
				await this.PushNotificationService(tags, clientId);
			}
		},
		editTags: {
			queue: {
				amqp: {
					prefetch: 1
				},
				retry: {
					max_retry: 3,
					delay: (retryCount) => retryCount * 5000
				}
			},
			params: {
				tags: 'object',
				clientId: 'string'
			},
			dedupHash: (ctx) => `EDIT_TAGS_${_.get(ctx, 'params.tags.accountId', '1')}_${_.get(ctx, 'params.clientId', '1')}`,
			timeout: 60000,
			handler: async (ctx) => {
				const { tags, clientId } = ctx.params;
				_.set(tags, 'env', process.env_ENV_TAG);
				try {
					await this.PushNotificationService().editTagsOneSignal(tags, clientId);
				} catch (e) {
					this.logger.info('EDIT_TAGS -> EXCEPTION :: ', e);
				}
			}
		}
	},

	// async started() {
	// },

	/**
		 * Events
		 */
	events: {},

	/**
		 * Methods
		 */
	methods: {
		GateDecoder: require('./methods/GateDecoder.method'),
		Private: require('./methods/getPrivate.method'),
		Public: require('./methods/getPublic.method'),
		PushNotificationService: require('./methods/pushNotificationOneSignal.method')
	}
};
