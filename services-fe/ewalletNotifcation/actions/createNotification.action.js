module.exports = async function (ctx) {
	try {
		const payload = ctx.params;
		const pushNotification = await this.broker.call('v1.ewalletNotification.pushNotification.async', { params: payload });
		console.log('pushNotification', pushNotification);
		if (pushNotification) {
			return {
				code: 1000,
				message: 'Thêm notification thành công'
			};
		}
		return {
			code: 1001,
			message: 'Thêm notification thất bại'
		};
	} catch (error) {
		this.logger.info(`[EwalletNotification] Create Notification Error: ${error}`);
		return {
			code: 1001,
			message: 'Thêm notification thất bại'
		};
	}
};
