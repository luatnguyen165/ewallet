/* eslint-disable no-useless-catch */
const _ = require('lodash');
const Path = require('path');

const GeneralConstant = require('../constants/general.constant');
const FrontendConstant = require('../constants/frontend.constant');
const HistoryConstant = require('../constants/history.constant');
const TransportConstant = require('../constants/transport.constant');
const SubscriptionConstant = require('../constants/subscription.constant');

const accountTestSBX = [3020598926, 371761312, 4600636802, 3128731321, 4445517898, 938750379, 7659611547, 1352969948, 2924987467, 796, 895602591, 827629154, 523388731, 5226773336, 163238860, 7595212807, 566980335, 434728419];

module.exports = async function (ctx) {
	try {
		const payload = ctx.params;
		const insertData = {
			accountId: payload.accountId,
			message: payload.message,
			extraData: payload.extraData,
			isBroadcast: _.isBoolean(payload.isBroadcast) ? payload.isBroadcast : false,
			notiType: _.get(payload, 'extraData.notiType', GeneralConstant.TYPE.PERSONAL),
			content: _.get(payload, 'extraData.content', null)
		};
		const insert = await this.broker.call('v1.ewalletNotificationModel.create', [insertData]);

		const pushedData = _.cloneDeep(insertData);
		pushedData.id = _.get(insert, 'id', null);
		// console.log(`PUSHHH : ${JSON.stringify(insert)}`);
		// StreamService.emit('NotifyNotification', pushedData);

		const tempExtraData = _.clone(insertData.extraData);
		const notificationObj = {
			resolveType: SubscriptionConstant.RESOLVE_TYPE.NOTIFICATION.NOTIFICATION_CREATE,
			accountId: _.get(insert, 'accountId', null),
			message: _.get(insert, 'message', null),
			isRead: _.get(insert, 'isRead', null),
			createdAt: _.get(insert, 'createdAt', null),
			feedId: _.get(tempExtraData, 'feedId', null),
			transaction: tempExtraData.service.transaction,
			service: _.get(tempExtraData, 'service.code', null),
			serviceName: _.get(tempExtraData, 'service.name', null),
			transactionType: _.get(tempExtraData, 'transactionType', ''),
			type: this.Public().MappingService(tempExtraData.service, tempExtraData) || 'BASIC',
			display: {
				[HistoryConstant.FORMATTER.OPEN_API_WALLET]: {
					icon: this.Public().IconHistory(_.get(tempExtraData, 'service.code', null) || _.get(tempExtraData, 'service.type', null), tempExtraData.changed, tempExtraData)
				},
				[HistoryConstant.FORMATTER.WALLET]: {
					icon: this.Public().IconHistory(_.get(tempExtraData, 'service.code', null) || _.get(tempExtraData, 'service.type', null), tempExtraData.changed, tempExtraData)
				}
			},
			history: tempExtraData,
			billData: {
				customerId: _.get(tempExtraData, 'service.data.customerId', null),
				supplierInfo: _.get(tempExtraData, 'service.data.supplierInfo', null),
				type: _.get(tempExtraData, 'service.data.type', null)
			},
			changed: _.findKey(HistoryConstant.CHANGED_ENUM, (v) => v === tempExtraData.changed) || null
		};
		if (_.get(tempExtraData, 'service.code') === 'ADD_MONEY' || _.get(tempExtraData, 'service.code') === FrontendConstant.TAGS.SALARY) {
			notificationObj.transport = { description: _.get(tempExtraData, 'extraData.desc', tempExtraData.description), method: TransportConstant.METHOD.BANK_ACCOUNT };
			notificationObj.service = HistoryConstant.SERVICE_TYPE.DEPOSIT;
			notificationObj.type = HistoryConstant.SERVICE_TYPE.DEPOSIT;
			if (_.get(tempExtraData, 'service.code', null) === FrontendConstant.TAGS.SALARY && _.get(tempExtraData, 'service.code') === 'ADD_MONEY') {
				notificationObj.service = HistoryConstant.SERVICE_TYPE.SALARY;
				notificationObj.type = HistoryConstant.SERVICE_TYPE.SALARY;
				const detailSalary = await this.broker.call('v1.ewalletAddMoneyModel.findOne', [{ transaction: _.get(tempExtraData, 'service.transaction', '') }]);
				notificationObj.transport = { description: tempExtraData.merchantName || _.get(detailSalary, 'sender', 'CTY CỔ PHẦN CÔNG NGHỆ PAYME'), method: TransportConstant.METHOD.BANK_ACCOUNT };
			}
			if (_.get(tempExtraData, 'service.code', null) === FrontendConstant.TAGS.SALARY && _.get(tempExtraData, 'service.type') === 'SALARY') {
				notificationObj.service = HistoryConstant.SERVICE_TYPE.SALARY;
				notificationObj.type = HistoryConstant.SERVICE_TYPE.SALARY;
				// const detailSalary = await AddMoneyModel.findOne({ transaction: _.get(tempExtraData, 'service.transaction', '') });
				// notificationObj.transport = { description: tempExtraData.merchantName || _.get(detailSalary, 'sender', 'CTY CỔ PHẦN CÔNG NGHỆ PAYME'), method: TransportConstant.METHOD.BANK_ACCOUNT };
			}
		}

		// SubscriptionPubsub.publish(SubscriptionConstant.EVENT.NOTIFICATION, { notificationObj });
		if (_.get(payload, 'extraData.clientId', null)) {
			const result = await this.PushNotificationService().pushWithFilterClientId(payload.extraData.clientId, {
				en: payload.title ? payload.title : 'PayME'
			}, {
				en: payload.message
			}, {
				feedId: _.get(payload, 'extraData.feedId', null),
				changed: _.get(payload, 'extraData.changed', null),
				serviceId: _.get(payload, 'extraData.service.id', null),
				serviceType: _.get(payload, 'extraData.service.type', null),
				serviceCode: _.get(payload, 'extraData.service.code', null),
				transaction: _.get(payload, 'extraData.service.transaction', null),
				type: _.get(pushedData, 'type', null),
				content: _.get(payload, 'extraData.content', null),
				extraData: _.get(pushedData, 'extraData', null)
			});
		} else if (payload.accountId) {
			const result = await this.PushNotificationService().pushWithFilterAccountId(payload.accountId, {
				en: payload.title ? payload.title : 'PayME'
			}, {
				en: payload.message
			}, {
				feedId: _.get(payload, 'extraData.feedId', null),
				changed: _.get(payload, 'extraData.changed', null),
				serviceId: _.get(payload, 'extraData.service.id', null),
				serviceType: _.get(payload, 'extraData.service.type', null),
				serviceCode: _.get(payload, 'extraData.service.code', null),
				transaction: _.get(payload, 'extraData.service.transaction', null),
				content: _.get(payload, 'extraData.content', null),
				extraData: _.get(pushedData, 'extraData', null)
			});
			// console.log(`NOTIIIIIIIIIIIIIII : ${JSON.stringify(result)}`);
		}

		if (_.get(insert, 'id', false) === false) {
			return {
				code: 1001,
				message: 'Thêm notification Lỗi'
			};
		}
		const accountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [{
			id: insertData.accountId
		}]);
		let info = {};
		let templatePath;
		let isGetFile = true;
		let emailTemplate;

		if (accountInfo.id < 0) {
			return {
				code: 1000,
				message: 'Thêm notification thành công',
				data: {
					stream: pushedData,
					pubsub: notificationObj
				}
			};
		}
		if (_.get(accountInfo, 'email', false) === false) {
			return {
				code: 1000,
				message: 'Thêm notification thành công',
				data: {
					stream: pushedData,
					pubsub: notificationObj
				}
			};
		}
		if (_.get(accountInfo, 'isVerifiedEmail', false) === false) {
			// nếu chưa verify ko gửi mail
			return {
				code: 1000,
				message: 'Thêm notification thành công',
				data: {
					stream: pushedData,
					pubsub: notificationObj
				}
			};
		}
		if (_.get(insertData.extraData, 'state', null) === FrontendConstant.BILL_STATE.SUCCEEDED
			// || (_.get(insertData.extraData, 'service.state') === FrontendConstant.BILL_STATE.PENDING
			//   && _.get(insertData.extraData, 'service.code', '') === FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_PVCBANK)
			// || (_.get(insertData.extraData, 'service.state') === FrontendConstant.BILL_STATE.PENDING
			// && _.get(insertData.extraData, 'service.code', '') === FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_PVCBANK)
		) {
			// sendmail restfull
			if (_.get(insertData.extraData, 'payment.id', 0) === 0 && _.get(insertData.extraData, 'transport.transportId', 0) === 0) {
				switch (_.get(insertData.extraData, 'service.code', '')) {
					// case thanh toan
					case FrontendConstant.SERVICES.BILL:
						info = await this.Private.BillPayment(accountInfo, insertData);
						templatePath = Path.resolve('../views/emails/payment-bill.html');
						emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'payment-bill' }]) || '';
						if (_.get(emailTemplate, 'id', false) !== false) {
							templatePath = emailTemplate.content;
							isGetFile = false;
						}
						break;
					case FrontendConstant.SERVICES.WITHDRAW_BANK_GATEWAY:
					case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_GATEWAY:
						info = await this.Private.WithdrawBankGateway(accountInfo, insertData);
						templatePath = Path.resolve('../views/emails/withdraw.html');
						emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'withdraw' }]) || '';
						if (_.get(emailTemplate, 'id', false) !== false) {
							templatePath = emailTemplate.content;
							isGetFile = false;
						}
						break;
					// rut tu PVcomBank
					case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_PVCBANK:
						info = await this.Private.WithdrawBankPVCBank(accountInfo, insertData);
						templatePath = Path.resolve('../views/emails/withdraw.html');
						emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'withdraw' }]) || '';
						if (_.get(emailTemplate, 'id', false) !== false) {
							templatePath = emailTemplate.content;
							isGetFile = false;
						}
						break;
					// case nap
					case FrontendConstant.SERVICES.DEPOSIT_BANK_MANUAL:
					case FrontendConstant.SERVICES.DEPOSIT_PVCBANK:
					case FrontendConstant.SERVICES.DEPOSIT:
						info = await this.Private.Deposit(accountInfo, insertData);
						templatePath = Path.resolve('../views/emails/deposit.html');
						emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'deposit' }]) || '';
						if (_.get(emailTemplate, 'id', false) !== false) {
							templatePath = emailTemplate.content;
							isGetFile = false;
						}
						break;
					// case chuyen
					case FrontendConstant.SERVICES.TRANSFER_PAYME:
						if (insertData.extraData.changed === '-') {
							info = await this.Private.TransferSender(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/transfer.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'transfer' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						if (insertData.extraData.changed === '+') {
							info = await this.Private.TransferReceiver(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/receive.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'receive' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						break;
					// case ứng lương nhận tiền
					case HistoryConstant.SERVICE_TYPE.ADVANCE_SALARY:
						if (insertData.extraData.changed === '+') {
							_.set(insertData, 'description', payload.message);
							info = await this.Private.ReceiveGraph(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/detail-receive.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'detail-receive' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						break;

					case FrontendConstant.SERVICES.REFUND_MONEY:
						info = await this.Private.RefundMoney(accountInfo, insertData);
						templatePath = Path.resolve('../views/emails/refund-card.html');
						emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'refund-money' }]) || '';
						if (_.get(emailTemplate, 'id', false) !== false) {
							templatePath = emailTemplate.content;
							isGetFile = false;
						}
						break;
					case FrontendConstant.SERVICES.CANCEL_ISEC:
						info = await this.Private.CancelISec(accountInfo, insertData);
						templatePath = Path.resolve('../views/emails/cancel-isec.html');
						emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'cancel-isec' }]) || '';
						if (_.get(emailTemplate, 'id', false) !== false) {
							templatePath = emailTemplate.content;
							isGetFile = false;
						}
						break;
					case FrontendConstant.SERVICES.ISEC_REDEEM:
						if (_.get(insertData.extraData, 'changed', '') === '+') {
							info = await this.Private.RedeemISec(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/redeem-isec.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'redeem-isec' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						break;
					case FrontendConstant.SERVICES.SOCIAL_PAYMENT_REQUEST_MONEY_LINK:
					case FrontendConstant.SERVICES.SOCIAL_PAYMENT_PAYME_RECEIVE_MONEY:
					case FrontendConstant.SERVICES.SOCIAL_PAYMENT_DONATE_MONEY_LINK:
						// link yeu cau dc thanh toan
						if (_.get(insertData.extraData, 'changed', '') === '+') {
							info = await this.Private.SocialReceive(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/social-receive-payment.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'social-receive-payment' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						// link donate vua chuyen vua nhan
						if (_.get(insertData.extraData, 'changed', '') === '-' && _.get(insertData.extraData, 'service.code', '') === FrontendConstant.SERVICES.SOCIAL_PAYMENT_DONATE_MONEY_LINK) {
							info = await this.Private.SocialPayment(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/social-send-payment.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'social-send-payment' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						if (_.get(insertData.extraData, 'changed', '') === '') {
							// mail nhan tien tu link gui trong service
							// info = await this.Private.SocialRequestPayment(accountInfo, insertData);
							// templatePath = `${__dirname}/../../../../../../views/emails/social-request-payment.html`;
						}
						break;
					case FrontendConstant.SERVICES.SOCIAL_PAYMENT_SEND_MONEY:
					case FrontendConstant.SERVICES.SOCIAL_PAYMENT_SEND_MONEY_LINK:
						// link yeu cau dc thanh toan
						if (_.get(insertData.extraData, 'changed', '') === '-') {
							info = await this.Private.SocialPayment(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/social-send-payment.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'social-send-payment' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						// neu chuyen link bang email thi gui email cho ng nhan
						if (_.get(insertData.extraData, 'changed', '') === '+' && _.get(insertData.extraData, 'service.code', '') === FrontendConstant.SERVICES.SOCIAL_PAYMENT_SEND_MONEY_LINK) {
							info = await this.Private.SocialReceiveLink(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/social-receive-payment.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'social-receive-payment' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						break;
					case FrontendConstant.SERVICES.MOBILE_CARD:
						// link yeu cau dc thanh toan
						if (_.get(insertData.extraData, 'changed', '') === '-') {
							info = await this.Private.CartPayment(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/card-payment.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'card-payment' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						break;
					case FrontendConstant.SERVICES.MOBILE_TOPUP:
						// link yeu cau dc thanh toan
						if (_.get(insertData.extraData, 'changed', '') === '-') {
							info = await this.Private.TopupPayment(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/card-topup.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'card-topup' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						break;
					case FrontendConstant.SERVICES.ISEC:
						// link yeu cau dc thanh toan
						if (_.get(insertData.extraData, 'changed', '') === '-') {
							info = await this.Private.IsecPayment(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/isec-payment.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'isec-payment' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						break;
					default:
						break;
				}
			}
			if (_.get(insertData.extraData, 'service.type', null) === 'INSIGHT' && insertData.extraData.isSendMail) {
				info = await this.Private.InsightEmail(accountInfo, insertData);
				templatePath = _.get(insertData, 'extraData.service.content', '');
				isGetFile = false;
			}
			// sendmail graphQl
			if (_.get(insertData.extraData, 'service.type', null) !== null
				&& (_.get(insertData.extraData, 'payment.id', '') > 0 || _.get(insertData.extraData, 'transport.transportId', '') > 0)
				&& (_.includes(accountTestSBX, _.toNumber(_.get(accountInfo.data[0], 'id', null))) || process.env.ENV_TAG !== 'sandbox')
			) {
				switch (_.get(insertData.extraData, 'service.type', '')) {
					// case thanh toan
					case FrontendConstant.SERVICES.BILL:
						info = await this.Private.BillPaymentGraph(accountInfo, insertData);
						templatePath = Path.resolve('../views/emails/payment-bill.html');
						emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'payment-bill' }]) || '';
						if (_.get(emailTemplate, 'id', false) !== false) {
							templatePath = emailTemplate.content;
							isGetFile = false;
						}
						break;
					case FrontendConstant.TRANSACTION_TYPE.WITHDRAW:
						info = await this.Private.WithdrawBankGatewayGraph(accountInfo, insertData);
						templatePath = Path.resolve('../views/emails/withdraw.html');
						emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'withdraw' }]) || '';
						if (_.get(emailTemplate, 'id', false) !== false) {
							templatePath = emailTemplate.content;
							isGetFile = false;
						}
						break;
					// rut tu PVcomBank
					// case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_PVCBANK:
					//   info = await this.Private.WithdrawBankPVCBank(accountInfo, insertData);
					//   templatePath = Path.resolve('../views/emails/withdraw.html');
					//   emailTemplate = await EmailTemplateModel.findOne({ shortName: 'withdraw' }) || '';
					//   if (_.get(emailTemplate, 'id', false) !== false) {
					//     templatePath = emailTemplate.content;
					//     isGetFile = false;
					//   }
					//   break;
					// case nap
					case FrontendConstant.SERVICES.DEPOSIT:
						info = await this.Private.DepositGraph(accountInfo, insertData);
						if (_.includes(insertData.extraData.tags, FrontendConstant.TAGS.DEPOSIT_BANK_MANUAL)) {
							templatePath = Path.resolve('../views/emails/deposit-manual.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'deposit-manual' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						} else {
							templatePath = Path.resolve('../views/emails/deposit.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'deposit' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						break;
					// case chuyen
					case HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY:
						if (insertData.extraData.changed === '-') {
							info = await this.Private.TransferSenderGraph(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/transfer.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'transfer' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						if (insertData.extraData.changed === '+') {
							info = await this.Private.TransferReceiverGraph(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/receive.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'receive' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						break;
					// case ứng lương nhận tiền
					case HistoryConstant.SERVICE_TYPE.ADVANCE_SALARY:
						if (insertData.extraData.changed === '+') {
							insertData.description = payload.message;
							info = await this.Private.ReceiveGraph(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/detail-receive.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'detail-receive' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						break;
					case HistoryConstant.SERVICE_TYPE.SALARY:
						if (insertData.extraData.changed === '+') {
							insertData.description = payload.message;
							info = await this.Private.SalaryGraph(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/salary.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'salary' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						break;
					case HistoryConstant.SERVICE_TYPE.MOBILE_CARD:
						// link yeu cau dc thanh toan
						if (_.get(insertData.extraData, 'changed', '') === '-') {
							info = await this.Private.CartPaymentGraph(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/card-payment.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'card-paymnet' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						break;
					case HistoryConstant.SERVICE_TYPE.MOBILE_TOPUP:
						// link yeu cau dc thanh toan
						if (_.get(insertData.extraData, 'changed', '') === '-') {
							info = await this.Private.TopupPaymentGraph(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/card-topup.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'card-topup' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						break;
					case HistoryConstant.SERVICE_TYPE.ISEC:
						// link yeu cau dc thanh toan
						if (_.get(insertData.extraData, 'changed', '') === '-') {
							info = await this.Private.IsecPaymentGraph(accountInfo, insertData);
							templatePath = Path.resolve('../views/emails/isec-payment.html');
							emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'isec-payment' }]) || '';
							if (_.get(emailTemplate, 'id', false) !== false) {
								templatePath = emailTemplate.content;
								isGetFile = false;
							}
						}
						if (_.get(insertData.extraData, 'changed', '') === '+') {
							// cancel
							if (_.includes(_.get(insertData.extraData, 'tags', []), FrontendConstant.TAGS.CASHBACK_ISEC)) {
								info = await this.Private.CancelISecGraph(accountInfo, insertData);
								templatePath = Path.resolve('../views/emails/cancel-isec.html');
								emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'cancel-isec' }]) || '';
								if (_.get(emailTemplate, 'id', false) !== false) {
									templatePath = emailTemplate.content;
									isGetFile = false;
								}
							}
							// redeem
							if (_.includes(_.get(insertData.extraData, 'tags', []), FrontendConstant.TAGS.RECEIVE_MONEY_ISEC)) {
								info = await this.Private.RedeemISecGraph(accountInfo, insertData);
								templatePath = Path.resolve('../views/emails/redeem-isec.html');
								emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'redeem-isec' }]) || '';
								if (_.get(emailTemplate, 'id', false) !== false) {
									templatePath = emailTemplate.content;
									isGetFile = false;
								}
							}
						}
						break;
					case HistoryConstant.SERVICE_TYPE.REFUND_MONEY:
						info = await this.Private.RefundMoney(accountInfo, insertData);
						templatePath = Path.resolve('../views/emails/refund-card.html');
						emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'refund-money' }]) || '';
						if (_.get(emailTemplate, 'id', false) !== false) {
							templatePath = emailTemplate.content;
							isGetFile = false;
						}
						break;
					case HistoryConstant.SERVICE_TYPE.ADVANCE_MONEY:
						info = await this.Private.advanceMoney(accountInfo, insertData);
						templatePath = Path.resolve('../views/emails/receive-credit.html');
						emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'receive-credit' }]) || '';
						if (_.get(emailTemplate, 'id', false) !== false) {
							templatePath = emailTemplate.content;
							isGetFile = false;
						}
						break;
					case HistoryConstant.SERVICE_TYPE.CREDIT_STATEMENT:
					case HistoryConstant.SERVICE_TYPE.CREDIT_SETTLEMENT:
						info = await this.Private.creditStatement(accountInfo, insertData);
						templatePath = Path.resolve('../views/emails/payment-credit.html');
						emailTemplate = await this.broker.call('v1.ewalletEmailTemplateModel.findOne', [{ shortName: 'payment-credit' }]) || '';
						if (_.get(emailTemplate, 'id', false) !== false) {
							templatePath = emailTemplate.content;
							isGetFile = false;
						}
						break;
					// case HistoryConstant.SERVICE_TYPE.SOCIAL_PAYMENT:
					//   if (_.get(insertData.extraData, 'changed', '') === '-') {
					//     info = await this.Private.SocialPaymentGraph(accountInfo, insertData);
					//     templatePath = Path.resolve('../views/emails/social-send-payment.html');
					//     emailTemplate = await EmailTemplateModel.findOne({ shortName: 'social-send-payment' }) || '';
					//     if (_.get(emailTemplate, 'id', false) !== false) {
					//       templatePath = emailTemplate.content;
					//       isGetFile = false;
					//     }
					//   }
					//   // neu chuyen link bang email thi gui email cho ng nhan
					//   if (_.get(insertData.extraData, 'changed', '') === '+') {
					//     info = await this.Private.SocialReceiveLinkGraph(accountInfo, insertData);
					//     templatePath = Path.resolve('../views/emails/social-receive-payment.html');
					//     emailTemplate = await EmailTemplateModel.findOne({ shortName: 'social-receive-payment' }) || '';
					//     if (_.get(emailTemplate, 'id', false) !== false) {
					//       templatePath = emailTemplate.content;
					//       isGetFile = false;
					//     }
					//   }

					//   break;
					default:
						break;
				}
			}

			if (!_.isEmpty(info)) {
				try {
					console.log('sendmail info:>>>>>>>>>>>>>>', JSON.stringify(info));
					this.broker.call('v1.email.sendmail', { template: templatePath, data: info, isGetFile });
					// EmailService.sendMail(templatePath, info)
					//   .then((r) => console.log('Send email passs', r))
					//   .catch((e) => console.log('Send email faillll', e))
					//   .finally(() => console.log('Send Email done!'));
					// } catch (error) {
					//   console.log('send email faillll :>>', JSON.stringify(error));
					//   throw (error);
					// }
				} catch (errorMail) {
					console.log(errorMail);
					const alertMsg = `❗Gửi mail thông báo giao dịch thất bại !
			  \nInfo: ${JSON.stringify(info)}`;
					this.broker.call('v1.utility.notifyTelegram', { message: alertMsg, buttons: [], receiverList: ['api'] });
				}
			}
		}
		return {
			code: 1000,
			message: 'Thêm notification thành công',
			data: {
				stream: pushedData,
				pubsub: notificationObj
			}
		};
	} catch (error) {
		this.logger.info(`[ewalletNotification] create notification error: ${error}`);
		return {
			code: 1001,
			message: 'Thêm notification thất bại'
		};
	}
};
