const _ = require('lodash');

const FrontendConstant = require('../constants/frontend.constant');
const HistoryConstant = require('../constants/history.constant');
const SocialPaymentConstant = require('../constants/socialPayment.constant');
const WithdrawConstant = require('../constants/withdraw.constant');
const OpenEWalletPaymentConstant = require('../constants/openEwalletPayment.constant');
const TransferMoneyConstant = require('../constants/transferMoney.constant');

module.exports = function Public() {
	const IconHistory = (serviceCode, changed = null, tempExtraData = {}) => {
		let icon = null;
		switch (serviceCode) {
			case FrontendConstant.SERVICES.SOCIAL_DONATE_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_DONATE_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_REQUEST_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_REQUEST_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_PAYME_RECEIVE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_PAYME_RECEIVE_MONEY:
				icon = HistoryConstant.DISPLAY_ICON.RECEIVE_MONEY;
				break;
			case FrontendConstant.SERVICES.SOCIAL_DONATE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_DONATE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_SEND_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_SEND_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_SEND_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_SEND_MONEY:
				icon = HistoryConstant.DISPLAY_ICON.CALL_PAYMENT;
				break;
			case FrontendConstant.SERVICES.SOCIAL_NAPAS_RECEIVE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_NAPAS_RECEIVE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT:
				icon = HistoryConstant.DISPLAY_ICON.CALL_PAYMENT;
				if (changed === '+') {
					icon = HistoryConstant.DISPLAY_ICON.RECEIVE_MONEY;
					break;
				}
				break;
			case HistoryConstant.SERVICE_TYPE.WITHDRAW:
			case FrontendConstant.SERVICES.WITHDRAW_BANK_GATEWAY:
			case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_PVCBANK:
			case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_GATEWAY:
			case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_OCBBANK:
				icon = HistoryConstant.DISPLAY_ICON.WITHDRAW;
				break;
			case FrontendConstant.SERVICES.DEPOSIT:
			case FrontendConstant.SERVICES.DEPOSIT_BANK_MANUAL:
			case FrontendConstant.SERVICES.DEPOSIT_PVCBANK:
				icon = HistoryConstant.DISPLAY_ICON.DEPOSIT;
				break;
			case FrontendConstant.SERVICES.BILL:
				icon = _.find(HistoryConstant.DISPLAY_ICON_BILL, (k, v) => _.includes(tempExtraData.tags, v)) || HistoryConstant.DISPLAY_ICON.BILL;
				// icon = HistoryConstant.DISPLAY_ICON.BILL;
				break;
			case FrontendConstant.SERVICES.ISEC:
			case FrontendConstant.SERVICES.ISEC_REDEEM:
			case FrontendConstant.SERVICES.CANCEL_ISEC:
			case FrontendConstant.SERVICES.ISEC_SAVE:
			case FrontendConstant.SERVICES.ISEC_SCRATCH:
			case FrontendConstant.SERVICES.ISEC_DONATED:
			case FrontendConstant.SERVICES.ISEC_RECEIVED:
			case FrontendConstant.SERVICES.ISEC_SEND:
				icon = HistoryConstant.DISPLAY_ICON.ISEC;
				// console.log('changed', changed);
				if (changed === '+') {
					icon = HistoryConstant.DISPLAY_ICON.RECEIVE_MONEY;
				}
				if (serviceCode === FrontendConstant.SERVICES.CANCEL_ISEC || _.includes(tempExtraData.tags, FrontendConstant.TAGS.CASHBACK_ISEC)) {
					icon = HistoryConstant.DISPLAY_ICON.BONUS;
				}
				// if (serviceCode === FrontendConstant.SERVICES.ISEC_SAVE) {
				//   icon = HistoryConstant.DISPLAY_ICON.ISEC;
				// }
				// if (serviceCode === FrontendConstant.SERVICES.ISEC_REDEEM || _.includes(tempExtraData.tags, FrontendConstant.TAGS.RECEIVE_MONEY_ISEC)) {
				//   icon = HistoryConstant.DISPLAY_ICON.DEPOSIT;
				// }
				break;
			case FrontendConstant.SERVICES.MOBILE_CARD:
				icon = HistoryConstant.DISPLAY_ICON.BUY_CARD;
				break;
			case FrontendConstant.SERVICES.MOBILE_TOPUP:
				icon = HistoryConstant.DISPLAY_ICON.TOPUP_PHONE;
				break;
			case FrontendConstant.SERVICES.TRANSFER_PAYME:
			case HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY:
				if (changed === '+') {
					icon = HistoryConstant.DISPLAY_ICON.RECEIVE_MONEY;
					break;
				}
				icon = HistoryConstant.DISPLAY_ICON.TRANSFER_MONEY;
				break;
			case FrontendConstant.SERVICES.REFUND_MONEY:
				icon = HistoryConstant.DISPLAY_ICON.BONUS;
				break;
			case FrontendConstant.SERVICES.ADD_MONEY:
				if (changed === '+') {
					icon = HistoryConstant.DISPLAY_ICON.DEPOSIT;
					if (_.includes(_.get(tempExtraData, 'tags', []), FrontendConstant.TAGS.SALARY)) {
						// nhan tien
						icon = HistoryConstant.DISPLAY_ICON.RECEIVE_MONEY;
					}
				}
				if (changed === '-') {
					icon = HistoryConstant.DISPLAY_ICON.WITHDRAW;
					break;
				}
				break;
			case FrontendConstant.SERVICES.SALARY:
				icon = HistoryConstant.DISPLAY_ICON.RECEIVE_MONEY;
				break;
			case FrontendConstant.SERVICES.LOGIN_TRUST_DEVICE:
				icon = HistoryConstant.DISPLAY_ICON.EKYC;
				break;
			case HistoryConstant.SERVICE_TYPE.ADVANCE_MONEY:
				icon = HistoryConstant.DISPLAY_ICON.RECEIVE_MONEY;
				break;
			case HistoryConstant.SERVICE_TYPE.CREDIT_SETTLEMENT:
			case HistoryConstant.SERVICE_TYPE.CREDIT_STATEMENT:
				icon = HistoryConstant.DISPLAY_ICON.BILL;
				break;
			default:
				icon = HistoryConstant.DISPLAY_ICON.EKYC;
				if (_.includes(tempExtraData.tags, FrontendConstant.TAGS.REFUND_MONEY)) icon = HistoryConstant.DISPLAY_ICON.BONUS;
				break;
		}
		return icon;
	};
	const MappingService = (service, history = {}) => {
		let serviceType = null;

		const serviceCode = _.get(service, 'code', null);
		switch (serviceCode) {
			case SocialPaymentConstant.SERVICE_CODE:
			case FrontendConstant.SERVICES.SOCIAL_DONATE_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_DONATE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_REQUEST_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_SEND_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_SEND_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_PAYME_RECEIVE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_NAPAS_RECEIVE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_DONATE_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_DONATE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_REQUEST_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_SEND_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_SEND_MONEY_LINK:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_PAYME_RECEIVE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT_NAPAS_RECEIVE_MONEY:
			case FrontendConstant.SERVICES.SOCIAL_PAYMENT:
				serviceType = HistoryConstant.SERVICE_TYPE.SOCIAL_PAYMENT;
				break;
			case WithdrawConstant.SERVICE_CODE:
				serviceType = HistoryConstant.SERVICE_TYPE.WITHDRAW;
				break;
			case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_PVCBANK:
			case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_GATEWAY:
			case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_OCBBANK:
			case FrontendConstant.SERVICES.WITHDRAW_BANK_MANUAL:
				serviceType = HistoryConstant.SERVICE_TYPE.WITHDRAW;
				break;
			//
			case FrontendConstant.SERVICES.WITHDRAW_BANK_GATEWAY:
				{
					if (_.get(history, 'extraData.type', 'WITHDRAW') === 'TRANSFER') {
						serviceType = HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY;
					} else {
						serviceType = HistoryConstant.SERVICE_TYPE.WITHDRAW;
					}
					break;
				}
			//
			case FrontendConstant.SERVICES.DEPOSIT:
			case FrontendConstant.SERVICES.DEPOSIT_BANK_MANUAL:
			case FrontendConstant.SERVICES.DEPOSIT_PVCBANK:
				serviceType = HistoryConstant.SERVICE_TYPE.DEPOSIT;
				break;
			case FrontendConstant.SERVICES.BILL:
				serviceType = HistoryConstant.SERVICE_TYPE.BILL;
				break;
			case FrontendConstant.SERVICES.LINKED:
				serviceType = HistoryConstant.SERVICE_TYPE.LINKED;
				break;
			case FrontendConstant.SERVICES.MOBILE_CARD:
				serviceType = HistoryConstant.SERVICE_TYPE.MOBILE_CARD;
				break;
			case FrontendConstant.SERVICES.MOBILE_TOPUP:
				serviceType = HistoryConstant.SERVICE_TYPE.MOBILE_TOPUP;
				break;
			case FrontendConstant.SERVICES.TRANSFER_PAYME:
				serviceType = HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY;
				break;
			case TransferMoneyConstant.SERVICE_CODE:
				serviceType = HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY;
				break;
			case OpenEWalletPaymentConstant.SERVICE_CODE:
				serviceType = HistoryConstant.SERVICE_TYPE.OPEN_EWALLET_PAYMENT;
				break;
			case FrontendConstant.SERVICES.REFUND_MONEY:
				serviceType = HistoryConstant.SERVICE_TYPE.REFUND_MONEY;
				break;
			case FrontendConstant.SERVICES.ISEC:
			case FrontendConstant.SERVICES.ISEC_REDEEM:
			case FrontendConstant.SERVICES.ISEC_SCRATCH:
			case FrontendConstant.SERVICES.ISEC_DONATED:
			case FrontendConstant.SERVICES.ISEC_RECEIVED:
			case FrontendConstant.SERVICES.ISEC_SAVE:
			case FrontendConstant.SERVICES.ISEC_SEND:
			case FrontendConstant.SERVICES.CANCEL_ISEC:
				serviceType = HistoryConstant.SERVICE_TYPE.ISEC;
				break;

			case HistoryConstant.SERVICE_TYPE.ADVANCE_MONEY:
				serviceType = HistoryConstant.SERVICE_TYPE.ADVANCE_MONEY;
				break;
			case HistoryConstant.SERVICE_TYPE.CREDIT_SETTLEMENT:
			case HistoryConstant.SERVICE_TYPE.CREDIT_STATEMENT:
				serviceType = HistoryConstant.SERVICE_TYPE.CREDIT_STATEMENT;
				break;
			default:
				serviceType = HistoryConstant.SERVICE_TYPE.BASIC;
				break;
		}
		// for restfull convert to payment + transport
		if (!_.isEmpty(history.method)) {
			switch (serviceCode) {
				case SocialPaymentConstant.SERVICE_CODE:
				case FrontendConstant.SERVICES.SOCIAL_DONATE_MONEY_LINK:
				case FrontendConstant.SERVICES.SOCIAL_DONATE_MONEY:
				case FrontendConstant.SERVICES.SOCIAL_REQUEST_MONEY_LINK:
				case FrontendConstant.SERVICES.SOCIAL_SEND_MONEY:
				case FrontendConstant.SERVICES.SOCIAL_SEND_MONEY_LINK:
				case FrontendConstant.SERVICES.SOCIAL_PAYME_RECEIVE_MONEY:
				case FrontendConstant.SERVICES.SOCIAL_NAPAS_RECEIVE_MONEY:
				case FrontendConstant.SERVICES.SOCIAL_PAYMENT_DONATE_MONEY_LINK:
				case FrontendConstant.SERVICES.SOCIAL_PAYMENT_DONATE_MONEY:
				case FrontendConstant.SERVICES.SOCIAL_PAYMENT_REQUEST_MONEY_LINK:
				case FrontendConstant.SERVICES.SOCIAL_PAYMENT_SEND_MONEY:
				case FrontendConstant.SERVICES.SOCIAL_PAYMENT_SEND_MONEY_LINK:
				case FrontendConstant.SERVICES.SOCIAL_PAYMENT_PAYME_RECEIVE_MONEY:
				case FrontendConstant.SERVICES.SOCIAL_PAYMENT_NAPAS_RECEIVE_MONEY:
					serviceType = HistoryConstant.SERVICE_TYPE.SOCIAL_PAYMENT;
					break;
				case WithdrawConstant.SERVICE_CODE:
				case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_PVCBANK:
				case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_GATEWAY:
				case FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_OCBBANK:
					serviceType = HistoryConstant.SERVICE_TYPE.WITHDRAW;
					break;
				case FrontendConstant.SERVICES.WITHDRAW_BANK_GATEWAY:

					if (_.get(history, 'extraData.type', 'WITHDRAW') === 'TRANSFER') {
						serviceType = HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY;
					} else {
						serviceType = HistoryConstant.SERVICE_TYPE.WITHDRAW;
					}
					break;

				case FrontendConstant.SERVICES.DEPOSIT:
				case FrontendConstant.SERVICES.DEPOSIT_BANK_MANUAL:
				case FrontendConstant.SERVICES.DEPOSIT_PVCBANK:
					serviceType = HistoryConstant.SERVICE_TYPE.DEPOSIT;
					break;
				case FrontendConstant.SERVICES.BILL:
					serviceType = HistoryConstant.SERVICE_TYPE.BILL;
					break;
				case FrontendConstant.SERVICES.LINKED:
					serviceType = HistoryConstant.SERVICE_TYPE.LINKED;
					break;
				case FrontendConstant.SERVICES.MOBILE_CARD:
					serviceType = HistoryConstant.SERVICE_TYPE.MOBILE_CARD;
					break;
				case FrontendConstant.SERVICES.MOBILE_TOPUP:
					serviceType = HistoryConstant.SERVICE_TYPE.MOBILE_TOPUP;
					break;
				case FrontendConstant.SERVICES.TRANSFER_PAYME:
					serviceType = HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY;
					break;
				case TransferMoneyConstant.SERVICE_CODE:
					serviceType = HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY;
					break;
				case OpenEWalletPaymentConstant.SERVICE_CODE:
					serviceType = HistoryConstant.SERVICE_TYPE.OPEN_EWALLET_PAYMENT;
					break;
				case FrontendConstant.SERVICES.REFUND_MONEY:
					serviceType = HistoryConstant.SERVICE_TYPE.REFUND_MONEY;
					break;
				case FrontendConstant.SERVICES.ISEC:
					serviceType = HistoryConstant.SERVICE_TYPE.ISEC;
					break;
				default:
					serviceType = HistoryConstant.SERVICE_TYPE.BASIC;
					break;
			}
		}
		return service.type || serviceType;
	};
	return {
		IconHistory,
		MappingService
	};
};
