const _ = require('lodash');
const Moment = require('moment');
const Numeral = require('numeral');

const FrontendConstant = require('../constants/frontend.constant');
const SocialBankList = require('../constants/socialBankList.constant');
const PaymentConstant = require('../constants/payment.constant');
const TransportConstant = require('../constants/transport.constant');
const CreditBalanceConstant = require('../constants/creditBalance.constant');

module.exports = function Private() {
	const BillPayment = async (accountInfo, insertData) => {
		let billInfo = await this.broker.call('v1.eWalletBillModel.findOne', [{ transaction: _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		let period = billInfo.extraData ? _.map(billInfo.extraData, (v) => `${v.description}`).join(' - ') : '';
		if (_.isEmpty(billInfo)) {
			billInfo = await this.broker.call('v1.eWalletTuitionModel.findOne', [{ transaction: _.get(insertData.extraData, 'service.transaction', '') }]) || {};
			period = !_.isEmpty(billInfo.paymentPeriod) ? _.join(billInfo.paymentPeriod, ',') : '';
		}
		if (_.isEmpty(history)) {
			return {};
		}
		if (_.isEmpty(billInfo)) {
			return {};
		}
		const methodInfo = await this.broker.call('v1.eWalletPaymentMethodModel.findOne', [{ id: _.get(insertData.extraData, 'method.info.methodId', '') }]) || _.get(insertData.extraData, 'method', {});
		if (_.get(insertData.extraData, 'method.info.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_BANK || _.get(insertData.extraData, 'method.info.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_GATEWAY) {
			// methodInfo.title = 'Thẻ liên kết';
			methodInfo.title = 'Tài khoản liên kết';
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(history.createdAt).format('HH:mm DD/MM/YYYY'),
				total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
				serviceName: _.get(history, 'service.name', ''),
				methodTitle: methodInfo.title || 'Ví PayME',
				cardNumber: _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') ? _.get(insertData.extraData, 'method.bankInfo.cardNumber', '').substr(-4, 4) : '',
				shortName: _.get(insertData.extraData, 'method.bankInfo.shortName', false) ? (_.get(insertData.extraData, 'method.bankInfo.shortName', '')) : '',
				customerId: _.get(billInfo.customerInfo, 'id', '') || _.get(billInfo, 'SSCId', ''),
				period,
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				// to: 'tripm@paymen.vn',
				subject: `${(_.get(insertData.extraData, 'service.name', ''))}`
			}
		};
	};
	const WithdrawBankGateway = async (accountInfo, insertData) => {
		const withdrawInfo = await this.broker.call('v1.eWalletTransferToNapasModel.findOne', [
			{
				transactionId: _.get(insertData.extraData, 'service.transaction', '')
			}
		]);
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(withdrawInfo)) {
			return {};
		}
		const serviceName = _.get(history, 'service.name', '');
		let title = 'Tài khoản ngân hàng';
		if (_.get(history, 'service.code') === FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_GATEWAY
			|| _.get(history, 'service.code') === FrontendConstant.SERVICES.WITHDRAW_BANK_LINKED_PVCBANK) {
			// title = 'Thẻ liên kết';
			title = 'Tài khoản liên kết';
		}
		if (_.get(history, 'service.code') === FrontendConstant.SERVICES.WITHDRAW_BANK_GATEWAY
			&& _.includes(serviceName.toLowerCase(), 'thẻ atm nội địa')
		) {
			title = 'Thẻ ATM nội địa';
		}
		const banks = SocialBankList.find((bank) => bank.swiftCode === withdrawInfo.bankInfo.swiftCode);
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(_.get(history, 'createdAt', '')).format('HH:mm DD/MM/YYYY'),
				fee: Numeral(_.get(insertData.extraData, 'fee', '')).format(0, 0),
				total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
				methodTitle: title,
				type: _.get(history, 'extraData.type', '') === FrontendConstant.TRANSACTION_TYPE.TRANSFER ? 'CHUYỂN' : 'RÚT',
				note: _.get(history, 'extraData.description', ''),
				fullnameReceiver: _.trim(_.get(withdrawInfo, 'bankInfo.cardHolder', '') || ''),
				serviceName,
				cardNumber: _.get(withdrawInfo, 'bankInfo.cardNumber', '') ? _.get(withdrawInfo, 'bankInfo.cardNumber', '').substr(-4, 4) : '',
				shortName: banks.title || '',
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				subject: `${(_.get(insertData.extraData, 'service.name', ''))}`
			}
		};
	};
	const WithdrawBankPVCBank = async (accountInfo, insertData) => {
		const withdrawInfo = await this.broker.call('v1.eWalletTransferToLinkedBankPVCBankModel.findOne', [
			{
				transactionId: _.get(insertData.extraData, 'service.transaction', '')
				// state: FrontendConstant.TRANSFER_METHOD_LINKED_PVCBANK_STATE.SUCCEEDED
			}
		]);
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(withdrawInfo)) {
			return {};
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(_.get(history, 'createdAt', '')).format('HH:mm DD/MM/YYYY'),
				fee: Numeral(_.get(insertData.extraData, 'fee', '')).format(0, 0),
				total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
				fullnameReceiver: _.get(withdrawInfo, 'bankInfo.cardHolder', ''),
				methodTitle: 'Tài khoản liên kết',
				// methodTitle: 'Thẻ liên kết',
				note: _.get(history, 'extraData.description', ''),
				serviceName: _.get(history, 'service.name', ''),
				shortName: _.get(withdrawInfo, 'bankInfo.bankName', false) ? _.get(withdrawInfo, 'bankInfo.bankName', '') : '',
				cardNumber: _.get(withdrawInfo, 'bankInfo.cardNumber', '') ? _.get(withdrawInfo, 'bankInfo.cardNumber', '').substr(-4, 4) : '',
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				// subject: 'Rút tiền từ thẻ liên kết'
				subject: 'Rút tiền từ tài khoản liên kết'
			}
		};
	};
	const Deposit = async (accountInfo, insertData) => {
		const depositInfo = await this.broker.call('v1.ewalletDepositModel.findOne', [{
			transaction: _.get(insertData.extraData, 'service.transaction', ''),
			state: FrontendConstant.TRANSFER_METHOD_LINKED_PVCBANK_STATE.SUCCEEDED
		}]) || {};
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(depositInfo)) {
			return {};
		}
		if (_.isEmpty(history)) {
			return {};
		}
		// if (_.get(depositInfo, 'method.group', false) === FrontendConstant.PAYMENT_METHOD_GROUP.LINKED_GATEWAY) {
		//   methodTitle = 'Thẻ liên kết';
		// }
		// if (_.get(depositInfo, 'method.group', false) === FrontendConstant.PAYMENT_METHOD_GROUP.LINKED_BANK) {
		//   methodTitle = 'Ngân Hàng liên kết';
		// }
		// if (_.get(depositInfo, 'method.group', false) === FrontendConstant.PAYMENT_METHOD_GROUP.GATEWAY) {
		//   methodTitle = 'Thẻ ATM nội địa';
		// }
		// if (_.get(depositInfo, 'method.group', false) === FrontendConstant.PAYMENT_METHOD_GROUP.DEPOSIT_BANK_MANUAL) {
		//   methodTitle = 'Nạp tiền thủ công';
		// }
		const methodInfo = await this.broker.call('v1.eWalletPaymentMethodModel.findOne', [{ id: _.get(insertData.extraData, 'method.info.methodId', '') }]) || _.get(insertData.extraData, 'method', {});
		if (_.get(insertData.extraData, 'method.info.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_BANK
			|| _.get(insertData.extraData, 'method.info.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_GATEWAY
			|| _.get(insertData.extraData, 'method.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_GATEWAY
			|| _.get(insertData.extraData, 'method.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_BANK

		) {
			// methodInfo.title = 'Thẻ liên kết';
			methodInfo.title = 'Tài khoản liên kết';
		}

		const shortName = _.get(depositInfo, 'bankInfo.bankName', false) ? _.get(depositInfo, 'bankInfo.bankName', '') : '';
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(history.createdAt).format('HH:mm DD/MM/YYYY'),
				fee: Numeral(history.fee).format(0, 0),
				total: Numeral(history.total).format(0, 0),
				methodTitle: methodInfo.title || 'Chuyển khoản ngân hàng',
				serviceName: _.get(history, 'service.name', 'Nạp tiền thành công'),
				cardNumber: _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') ? _.get(insertData.extraData, 'method.bankInfo.cardNumber', '').substr(-4, 4) : '',
				shortName: _.get(insertData.extraData, 'method.bankInfo.shortName', false) ? (_.get(insertData.extraData, 'method.bankInfo.shortName', '')) : shortName,
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				subject: `${_.get(history, 'service.name', 'Nạp tiền thành công')}`
			}
		};
	};
	const TransferSender = async (accountInfo, insertData) => {
		const transferSender = await this.broker.call('v1.ewalletTransferServiceSendMoneyModel.findOne', [{
			transaction: _.get(insertData.extraData, 'service.transaction', ''),
			state: FrontendConstant.TRANSFER_MONEY_STATE.SUCCEEDED
		}]);
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(transferSender)) {
			return {};
		}

		if (_.isEmpty(history)) {
			return {};
		}
		const methodInfo = await this.broker.call('v1.eWalletPaymentMethodModel.findOne', [{ id: _.get(insertData.extraData, 'method.info.methodId', '') }]) || _.get(insertData.extraData, 'method', {});
		if (_.get(insertData.extraData, 'method.info.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_BANK || _.get(insertData.extraData, 'method.info.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_GATEWAY) {
			// methodInfo.title = 'Thẻ liên kết';
			methodInfo.title = 'Tài khoản liên kết';
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(history.createdAt).format('HH:mm DD/MM/YYYY'),
				// phoneReceiver: `0${(_.get(transferSender, 'receiverInfo.phone', '')).substr(2)}`,
				fullnameReceiver: _.get(transferSender, 'receiverInfo.fullname', ''),
				note: transferSender.note || '',
				total: Numeral(transferSender.total).format(0, 0),
				methodTitle: methodInfo.title || 'Ví PayME',
				serviceName: _.get(history, 'service.name', 'Chuyển tiền thành công'),
				cardNumber: _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') ? _.get(insertData.extraData, 'method.bankInfo.cardNumber', '').substr(-4, 4) : '',
				shortName: _.get(insertData.extraData, 'method.bankInfo.shortName', false) ? (_.get(insertData.extraData, 'method.bankInfo.shortName', '')) : '',
				titleEmail: 'chuyển tiền ví PayME',
				action: 'chuyển tiền thành công',
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				// subject: `Chuyển tiền đến ví của ${_.get(transferSender, 'receiverInfo.fullname', '')}`
				subject: `${_.get(history, 'service.name', 'Chuyển tiền thành công')}`
			}
		};
	};
	const TransferReceiver = async (accountInfo, insertData) => {
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		const transferReceive = await this.broker.call('v1.ewalletTransferReceiveMoneyModel.findOne', [{
			transaction: _.get(insertData.extraData, 'service.transaction', ''),
			state: FrontendConstant.TRANSFER_MONEY_STATE.SUCCEEDED
		}]);
		if (_.isEmpty(transferReceive)) {
			return {};
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(_.get(insertData.extraData, 'createdAt', '')).format('HH:mm DD/MM/YYYY'),
				phoneSender: `0${(_.get(transferReceive, 'senderInfo.phone', '') || '').substr(2)} `,
				fullnameSender: _.get(transferReceive, 'senderInfo.fullname', ''),
				note: transferReceive.note || '',
				serviceName: _.get(history, 'service.name', ''),
				total: Numeral(transferReceive.total).format(0, 0),
				titleEmail: 'Nhận tiền ví PayME',
				action: `nhận tiền thành công từ ${_.get(transferReceive, 'senderInfo.fullname', null) ? _.get(transferReceive, 'senderInfo.fullname', '').split(' ').slice(-1).join(' ') : ''}`,
				methodTitle: 'Ví PayME',
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				subject: `Nhận tiền từ ví của ${_.get(transferReceive, 'senderInfo.fullname', '')}`
			}
		};
	};
	const RefundCard = async (accountInfo, insertData) => ({
		content: {
			phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
			fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
			transaction: _.get(insertData.extraData, 'service.transaction', ''),
			dateTransaction: Moment(_.get(insertData, 'extraData.publishedAt', '')).format('HH:mm DD/MM/YYYY') || Moment(new Date()).format('HH:mm DD/MM/YYYY'),
			total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
			reason: insertData.message || 'Hoàn tiền dịch vụ mua thẻ',
			footer: 'PayME'
		},
		email: {
			to: _.trim(_.get(accountInfo.data[0], 'email', '')),
			subject: 'Hoàn tiền dịch vụ mua thẻ'
		}
	});
	const RefundMoney = async (accountInfo, insertData) => ({
		content: {
			phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
			fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
			transaction: _.get(insertData.extraData, 'service.transaction', ''),
			dateTransaction: Moment(_.get(insertData, 'extraData.publishedAt', '')).format('HH:mm DD/MM/YYYY') || Moment(new Date()).format('HH:mm DD/MM/YYYY'),
			total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
			reason: _.get(insertData.extraData, 'description', '') || 'Hoàn tiền dịch vụ',
			footer: 'PayME'
		},
		email: {
			to: _.trim(_.get(accountInfo.data[0], 'email', '')),
			subject: 'Hoàn tiền dịch vụ'
		}
	});
	const CancelISec = async (accountInfo, insertData) => ({
		content: {
			phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
			fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
			transaction: _.get(insertData.extraData, 'service.transaction', ''),
			dateTransaction: Moment(_.get(insertData, 'extraData.publishedAt', '')).format('HH:mm DD/MM/YYYY') || Moment(new Date()).format('HH:mm DD/MM/YYYY'),
			total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
			iSecHint: _.get(insertData.extraData, 'extraData.iSecInfo.hint', ''),
			amount: Numeral(_.get(insertData.extraData, 'amount', '')).format(0, 0),
			fee: Numeral(_.get(insertData.extraData, 'fee', '')).format(0, 0),
			footer: 'PayME'
		},
		email: {
			to: _.trim(_.get(accountInfo.data[0], 'email', '')),
			subject: 'Hủy mã iSec'
		}
	});
	const InsightEmail = async (accountInfo, insertData) => ({
		content: {
			footer: 'PayME'
		},
		email: {
			to: _.trim(_.get(accountInfo.data[0], 'email', '')),
			subject: _.get(insertData, 'extraData.service.title', 'Thông tin đến khách hàng sử dụng Ví PayME.')
		}
	});
	const RedeemISec = async (accountInfo, insertData) => ({
		content: {
			phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
			fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
			transaction: _.get(insertData.extraData, 'service.transaction', ''),
			dateTransaction: Moment(_.get(insertData, 'extraData.extraData.createdAt', '')).format('HH:mm DD/MM/YYYY') || Moment(new Date()).format('HH:mm DD/MM/YYYY'),
			total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
			iSecHint: _.get(insertData.extraData, 'extraData.iSecInfo.hint', ''),
			amount: Numeral(_.get(insertData.extraData, 'amount', '')).format(0, 0),
			fee: Numeral(_.get(insertData.extraData, 'fee', '')).format(0, 0),
			footer: 'PayME'
		},
		email: {
			to: _.trim(_.get(accountInfo.data[0], 'email', '')),
			subject: 'Nhận tiền iSec'
		}
	});
	const RefundISec = async (accountInfo, insertData) => ({
		content: {
			phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
			fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
			transaction: _.get(insertData.extraData, 'service.transaction', ''),
			dateTransaction: Moment(_.get(insertData, 'extraData.publishedAt', '')).format('HH:mm DD/MM/YYYY') || Moment(new Date()).format('HH:mm DD/MM/YYYY'),
			total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
			footer: 'PayME'
		},
		email: {
			to: _.trim(_.get(accountInfo.data[0], 'email', '')),
			subject: 'Hoàn tiền dịch vụ phát hành iSec'
		}
	});
	const SocialPayment = async (accountInfo, insertData) => {
		const socialLink = await this.broker.call('v1.eWalletSocialLinkModel.findOne', [{
			transaction: _.get(insertData.extraData, 'service.transaction', '')
			// state: FrontendConstant.TRANSFER_MONEY_STATE.SUCCEEDED
		}]);
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(history)) {
			return {};
		}
		if (_.isEmpty(socialLink)) {
			return {};
		}

		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(history.createdAt).format('HH:mm DD/MM/YYYY'),
				serviceName: _.get(history, 'service.name', ''),
				total: Numeral(history.total).format(0, 0),
				note: socialLink.note || _.get(insertData.extraData, 'note', ''),
				methodTitle: _.get(insertData.extraData, 'method.info.title', ''),
				link: _.get(socialLink, 'url', ''),
				footer: 'PayME'
				// cardNumber: _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') ? _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') : '',
				// shortName: _.get(insertData.extraData, 'method.bankInfo.shortName', '') ? _.get(insertData.extraData, 'method.bankInfo.shortName', '') : '',
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				subject: 'Chuyển tiền qua PayME Link'
			}
		};
	};
	const SocialRequestPayment = async (accountInfo, insertData) => {
		const socialLink = await this.broker.call('v1.eWalletSocialLinkModel.findOne', [{
			transaction: _.get(insertData.extraData, 'service.transaction', '')
			// state: FrontendConstant.TRANSFER_MONEY_STATE.SUCCEEDED
		}]);
		if (_.isEmpty(socialLink)) {
			return {};
		}
		if (_.get(socialLink, 'transferInfo.receiverInfo.email', false) === false) {
			return {};
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: _.get(accountInfo.data[0], 'fullname', ''),
				total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
				note: _.get(insertData.extraData, 'note', ''),
				link: _.get(socialLink, 'url', ''),
				footer: 'PayME'
				// cardNumber: _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') ? _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') : '',
				// shortName: _.get(insertData.extraData, 'method.bankInfo.shortName', '') ? _.get(insertData.extraData, 'method.bankInfo.shortName', '') : '',
			},
			email: {
				// email là người nhận link
				to: _.trim(_.get(socialLink, 'transferInfo.receiverInfo.email', '')),
				subject: 'Yêu cầu chuyển tiền qua liên kết'
			}
		};
	};
	const SocialReceiveLink = async (accountInfo, insertData) => {
		const socialLink = await this.broker.call('v1.eWalletSocialLinkModel.findOne', [{
			transaction: _.get(insertData.extraData, 'service.transaction', '')
			// state: FrontendConstant.TRANSFER_MONEY_STATE.SUCCEEDED
		}]);
		if (_.isEmpty(socialLink)) {
			return {};
		}
		if (socialLink.channel !== FrontendConstant.SOCIAL_CHANNEL.EMAIL) {
			return {};
		}
		if (_.get(socialLink, 'transferInfo.receiverInfo.email', false) === false) {
			return {};
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: _.get(accountInfo.data[0], 'fullname', ''),
				total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
				note: _.get(insertData.extraData, 'note', ''),
				link: _.get(socialLink, 'url', ''),
				footer: 'PayME'
				// cardNumber: _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') ? _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') : '',
				// shortName: _.get(insertData.extraData, 'method.bankInfo.shortName', '') ? _.get(insertData.extraData, 'method.bankInfo.shortName', '') : '',
			},
			email: {
				// email là người nhận link
				to: _.trim(_.get(socialLink, 'transferInfo.receiverInfo.email', '')),
				subject: 'Nhận tiền từ liên kết'
			}
		};
	};
	const SocialReceive = async (accountInfo, insertData) => {
		const transfer = await this.broker.call('v1.eWalletTransferMoneyModel.findOne', [{
			transaction: _.get(insertData.extraData, 'service.transaction', '')
		}]);
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(history)) {
			return {};
		}
		if (_.isEmpty(transfer)) {
			return {};
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '') || '').substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(history.createdAt).format('HH:mm DD/MM/YYYY'),
				total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
				serviceName: _.get(history, 'service.name', ''),
				sender: _.get(transfer.extraData, 'transferInfo.senderInfo.fullname', ''),
				link: _.get(transfer.extraData, 'url', ''),
				note: _.get(transfer.extraData, 'note', ''),
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				subject: _.get(history, 'service.name', '')
			}
		};
	};
	const CartPayment = async (accountInfo, insertData) => {
		const card = await this.broker.call('v1.eWalletMobileCardModel.findOne', [{ transaction: _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(card)) {
			return {};
		}
		if (_.isEmpty(history)) {
			return {};
		}
		let cardTypeText = 'thẻ';
		let carTypeTile = 'MÃ GAME';
		if (_.includes([
			FrontendConstant.CARD_SUPPLIER.VIETTEL,
			FrontendConstant.CARD_SUPPLIER.MOBI,
			FrontendConstant.CARD_SUPPLIER.VINA,
			FrontendConstant.CARD_SUPPLIER.BEELINE,
			FrontendConstant.CARD_SUPPLIER.VN_MOBI
		], card.supplier)
		) {
			carTypeTile = 'MÃ ĐIỆN THOẠI';
			cardTypeText = 'thẻ';
		}
		let supplier = card.supplier || '';
		if ((card.supplier).toLowerCase() === 'mobi') {
			supplier = 'Mobifone';
		}
		if ((card.supplier).toLowerCase() === 'viettel') {
			supplier = 'Viettel';
		}
		if ((card.supplier).toLowerCase() === 'vina') {
			supplier = 'Vinaphone';
		}
		if ((card.supplier).toLowerCase() === 'garena') {
			supplier = 'Garena';
		}
		if ((card.supplier).toLowerCase() === 'zing') {
			supplier = 'Zing';
		}
		if ((card.supplier).toLowerCase() === 'beeline') {
			supplier = 'Beeline';
		}
		if (card.supplier === FrontendConstant.CARD_SUPPLIER.ECASH) {
			supplier = 'OnGame';
		}
		const methodInfo = await this.broker.call('v1.eWalletPaymentMethodModel.findOne', [{ id: _.get(insertData.extraData, 'method.info.methodId', '') }]) || _.get(insertData.extraData, 'method', {});
		if (_.get(insertData.extraData, 'method.info.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_BANK || _.get(insertData.extraData, 'method.info.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_GATEWAY) {
			// methodInfo.title = 'Thẻ liên kết';
			methodInfo.title = 'Tài khoản liên kết';
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(history, 'service.transaction', ''),
				dateTransaction: Moment(history.createdAt).format('HH:mm DD/MM/YYYY'),
				methodTitle: methodInfo.title || _.get(insertData.extraData, 'method.info.title', ''),
				total: Numeral(card.amount).format(0, 0),
				pin: this.GateDecoder.decrypt(_.get(card, 'cardInfo[0].pin', '') || ''),
				supplier,
				serviceName: _.get(history, 'service.name', ''),
				cardTypeText: cardTypeText || '',
				carTypeTile: carTypeTile || '',
				serial: _.get(card, 'cardInfo[0].serial', ''),
				cardNumber: _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') ? _.get(insertData.extraData, 'method.bankInfo.cardNumber', '').substr(-4, 4) : '',
				shortName: _.get(insertData.extraData, 'method.bankInfo.shortName', false) ? (_.get(insertData.extraData, 'method.bankInfo.shortName', '')) : '',
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				// to: 'tripm@paymen.vn',
				subject: _.get(history, 'service.name', 'Mua mã thẻ')
			}
		};
	};
	const TopupPayment = async (accountInfo, insertData) => {
		const topup = await this.broker.call('v1.eWalletMobileTopupModel.findOne', [{ transaction: _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(topup)) {
			return {};
		}
		if (_.isEmpty(history)) {
			return {};
		}

		let supplier = topup.supplier || '';
		if ((topup.supplier).toLowerCase() === 'mobi') {
			supplier = 'Mobifone';
		}
		if ((topup.supplier).toLowerCase() === 'viettel') {
			supplier = 'Viettel';
		}
		if ((topup.supplier).toLowerCase() === 'vina') {
			supplier = 'Vinaphone';
		}
		const methodInfo = await this.broker.call('v1.eWalletPaymentMethodModel.findOne', [{ id: _.get(insertData.extraData, 'method.info.methodId', '') }]) || _.get(insertData.extraData, 'method', {});
		if (_.get(insertData.extraData, 'method.info.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_BANK || _.get(insertData.extraData, 'method.info.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_GATEWAY) {
			// methodInfo.title = 'Thẻ liên kết';
			methodInfo.title = 'Tài khoản liên kết';
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(history, 'service.transaction', ''),
				dateTransaction: Moment(history.createdAt).format('HH:mm DD/MM/YYYY'),
				methodTitle: methodInfo.title || 'Ví PayME',
				total: Numeral(topup.total).format(0, 0),
				phoneTopup: topup.phone,
				serviceName: _.get(history, 'service.name', ''),
				cardNumber: _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') ? _.get(insertData.extraData, 'method.bankInfo.cardNumber', '').substr(-4, 4) : '',
				shortName: _.get(insertData.extraData, 'method.bankInfo.shortName', false) ? (_.get(insertData.extraData, 'method.bankInfo.shortName', '')) : '',
				supplier,
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				// to: 'tripm@paymen.vn',
				subject: _.get(history, 'service.name', 'Nạp điện thoại')
			}
		};
	};
	const IsecPayment = async (accountInfo, insertData) => {
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		const iSecInfo = await this.broker.call('v1.eWalletIsecModel.findOne', [{ transaction: _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(iSecInfo)) {
			return {};
		}
		if (_.isEmpty(history)) {
			return {};
		}

		const methodInfo = await this.broker.call('v1.eWalletPaymentMethodModel.findOne', [{ id: _.get(insertData.extraData, 'method.info.methodId', '') }]) || _.get(insertData.extraData, 'method', {});
		if (_.get(insertData.extraData, 'method.info.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_BANK || _.get(insertData.extraData, 'method.info.group', '') === FrontendConstant.WITHDRAW_METHOD_GROUP.LINKED_GATEWAY) {
			// methodInfo.title = 'Thẻ liên kết';
			methodInfo.title = 'Tài khoản liên kết';
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(history, 'service.transaction', ''),
				amount: Numeral(iSecInfo.amount).format(0, 0),
				dateTransaction: Moment(history.createdAt).format('HH:mm DD/MM/YYYY'),
				methodTitle: methodInfo.title || 'Ví PayME',
				serviceName: _.get(history, 'service.name', ''),
				iSecHint: iSecInfo.iSec ? iSecInfo.iSec.hint : '',
				cardNumber: _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') ? _.get(insertData.extraData, 'method.bankInfo.cardNumber', '').substr(-4, 4) : '',
				shortName: _.get(insertData.extraData, 'method.bankInfo.shortName', false) ? (_.get(insertData.extraData, 'method.bankInfo.shortName', '')) : '',
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				// to: 'tripm@paymen.vn',
				subject: _.get(history, 'service.name', '')
			}
		};
	};
	// graphQl
	const BillPaymentGraph = async (accountInfo, insertData) => {
		let billInfo = await this.broker.call('v1.eWalletBillModel.findOne', [{ transaction: _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		let period = billInfo.extraData ? _.map(billInfo.extraData, (v) => `${v.description}`).join(' - ') : '';
		if (_.isEmpty(billInfo)) {
			billInfo = await this.broker.call('v1.eWalletTuitionModel.findOne', [{ transaction: _.get(insertData.extraData, 'service.transaction', '') }]) || {};
			period = !_.isEmpty(billInfo.paymentPeriod) ? _.join(billInfo.paymentPeriod, ',') : '';
		}
		if (_.isEmpty(history)) {
			return {};
		}
		if (_.isEmpty(billInfo)) {
			return {};
		}
		let methodTitle = 'Ví PayME';
		let cardNumber = '';
		let shortName = '';
		switch (_.get(history, 'payment.method', '')) {
			case PaymentConstant.PAYMENT_TYPE.LINKED:
				methodTitle = 'Tài khoản liên kết';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_CARD:
				methodTitle = 'Thẻ ATM nội địa';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_ACCOUNT:
				methodTitle = 'Tài khoản ngân hàng';
				break;
			case PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT:
				methodTitle = 'PayME Credit';
				break;
			case PaymentConstant.PAYMENT_TYPE.CREDIT_BALANCE:
				methodTitle = 'Ví tín dụng';
				break;
			default:
				break;
		}
		if (_.get(history, 'payment.method', null) !== null
			&& _.get(history, 'payment.method', null) !== PaymentConstant.PAYMENT_TYPE.WALLET
			&& _.get(history, 'payment.method', null) !== PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT
		) {
			const descriptionPayment = _.split(_.get(history, 'payment.description', ''), '-');
			shortName = _.get(descriptionPayment, '[0]', '');
			cardNumber = _.get(descriptionPayment, '[1]', '');
		}

		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(history.publishedAt).format('HH:mm DD/MM/YYYY'),
				total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
				serviceName: _.get(history, 'service.name', ''),
				methodTitle,
				cardNumber,
				shortName,
				customerId: _.get(billInfo.customerInfo, 'id', '') || _.get(billInfo, 'SSCId', ''),
				period,
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				// to: 'tripm@paymen.vn',
				subject: `${(_.get(insertData.extraData, 'service.name', ''))}`
			}
		};
	};
	const WithdrawBankGatewayGraph = async (accountInfo, insertData) => {
		const transportInfo = await this.broker.call('v1.eWalletTransportModel.findOne', [
			{
				'service.transaction': _.get(insertData.extraData, 'service.transaction', ''),
				'service.type': FrontendConstant.TRANSACTION_TYPE.WITHDRAW
			}
		]);
		const transferInfo = await this.broker.call('v1.eWalletTransferMoneyModel.findOne', [
			{
				transaction: _.get(insertData.extraData, 'service.transaction', '')
				// 'service.code': HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY
			}
		]);
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(transportInfo)) {
			return {};
		}
		const serviceName = _.get(history, 'service.name', '');
		let methodTitle = 'Tài khoản ngân hàng';
		// let shortName = '';
		switch (_.get(history, 'transport.method', '')) {
			case TransportConstant.METHOD.LINKED:
				methodTitle = 'Tài khoản liên kết';
				break;
			case TransportConstant.METHOD.BANK_CARD:
				methodTitle = 'Thẻ ATM nội địa';
				break;
			case TransportConstant.METHOD.BANK_ACCOUNT:
				methodTitle = 'Tài khoản ngân hàng';
				break;
			default:
				break;
		}
		// if (_.get(history, 'transport.method', null) !== null
		//   && _.get(history, 'transport.method', null) !== PaymentConstant.PAYMENT_TYPE.WALLET
		//   && _.get(history, 'transport.method', null) !== PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT
		// ) {
		//   const transportPayment = _.split(_.get(history, 'transport.description', ''), '-');
		//   shortName = _.get(transportPayment, '[0]', '');
		//   cardNumber = _.get(transportPayment, '[1]', '');
		// }
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(_.get(history, 'createdAt', '')).format('HH:mm DD/MM/YYYY'),
				fee: _.get(insertData.extraData, 'fee', 0) !== 0 ? `${Numeral(_.get(insertData.extraData, 'fee', '')).format(0, 0)} đ` : 'Miễn phí',
				total: Numeral(_.get(insertData.extraData, 'amount', '')).format(0, 0),
				methodTitle,
				type: 'RÚT',
				note: _.get(transportInfo, 'note', '') || _.get(transferInfo, 'note', ''),
				fullnameReceiver: _.trim(_.get(transportInfo, 'methodData.bankInfo.cardHolder', '') || _.get(transportInfo, 'methodData.bankInfo.bankAccountName', '') || ''),
				serviceName,
				cardNumber: _.get(transportInfo, 'methodData.bankInfo.cardNumber', '') || _.get(transportInfo, 'description', ''),
				// shortName,
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				subject: `${(_.get(insertData.extraData, 'service.name', ''))}`
			}
		};
	};
	const DepositGraph = async (accountInfo, insertData) => {
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};

		if (_.isEmpty(history)) {
			return {};
		}
		let methodTitle = 'Thẻ ATM nội địa';
		let cardNumber = '';
		let shortName = '';
		switch (_.get(history, 'payment.method', '')) {
			case PaymentConstant.PAYMENT_TYPE.LINKED:
				methodTitle = 'Tài khoản liên kết';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_CARD:
				methodTitle = 'Thẻ ATM nội địa';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_ACCOUNT:
				methodTitle = 'Tài khoản ngân hàng';
				break;
			case PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT:
				methodTitle = 'PayME Credit';
				break;
			case PaymentConstant.METHOD_CODE.BANK_TRANSFER:
				methodTitle = 'Chuyển khoản ngân hàng';
				break;
			case PaymentConstant.PAYMENT_TYPE.CREDIT_BALANCE:
				methodTitle = 'Ví tín dụng';
				break;
			default:
				break;
		}
		if (_.get(history, 'payment.method', null) !== null
			&& _.get(history, 'payment.method', null) !== PaymentConstant.PAYMENT_TYPE.WALLET
			&& _.get(history, 'payment.method', null) !== PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT
		) {
			const descriptionPayment = _.split(_.get(history, 'payment.description', ''), '-');
			shortName = _.get(descriptionPayment, '[0]', '');
			cardNumber = _.get(descriptionPayment, '[1]', '');
		}
		if (_.includes(history.tags, FrontendConstant.TAGS.DEPOSIT_BANK_MANUAL)) {
			_.set(history, 'service.name', 'Nạp tiền bằng chuyển khoản');
		}

		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(history.publishedAt).format('HH:mm DD/MM/YYYY'),
				fee: _.get(history, 'fee', 0) !== 0 ? Numeral(_.get(history, 'fee', '')).format(0, 0) : 'Miễn phí',
				amount: Numeral(history.amount).format(0, 0),
				total: Numeral(history.total).format(0, 0),
				methodTitle,
				serviceName: _.get(history, 'service.name', 'Nạp tiền thành công'),
				cardNumber,
				shortName,
				depositCode: _.get(history, 'payment.description', null),
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				subject: `${_.get(history, 'service.name', 'Nạp tiền thành công')}`
			}
		};
	};
	const TransferSenderGraph = async (accountInfo, insertData) => {
		const transportInfo = await this.broker.call('v1.eWalletTransportModel.findOne', [
			{
				id: _.get(insertData.extraData, 'transport.transportId', '')
				// 'service.code': HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY
			}
		]);
		const transferInfo = await this.broker.call('v1.eWalletTransferMoneyModel.findOne', [
			{
				transaction: _.get(insertData.extraData, 'service.transaction', '')
				// 'service.code': HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY
			}
		]);
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		// if (_.isEmpty(transferInfo)) {
		//   return {};
		// }

		if (_.isEmpty(history)) {
			return {};
		}
		let methodTitle = 'Ví PayME';
		// let cardNumber = '';
		// let shortName = '';
		switch (_.get(history, 'transport.method', '')) {
			case PaymentConstant.PAYMENT_TYPE.LINKED:
				methodTitle = 'Tài khoản liên kết';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_CARD:
				methodTitle = 'Thẻ ATM nội địa';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_ACCOUNT:
				methodTitle = 'Tài khoản ngân hàng';
				break;
			case PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT:
				methodTitle = 'PayME Credit';
				break;
			case PaymentConstant.PAYMENT_TYPE.CREDIT_BALANCE:
				methodTitle = 'Ví tín dụng';
				break;
			default:
				break;
		}
		// if (_.get(history, 'transport.method', null) !== null
		//   && _.get(history, 'transport.method', null) !== PaymentConstant.PAYMENT_TYPE.WALLET
		//   && _.get(history, 'transport.method', null) !== PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT
		// ) {
		//   const descriptionPayment = _.split(_.get(history, 'transport.description', ''), '-');
		//   shortName = _.get(descriptionPayment, '[0]', '');
		//   cardNumber = _.get(descriptionPayment, '[1]', '');
		// }

		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(history.createdAt).format('HH:mm DD/MM/YYYY'),
				// phoneReceiver: `0${(_.get(transferSender, 'receiverInfo.phone', '')).substr(2)}`,
				fullnameReceiver: _.get(transportInfo, 'methodData.accountInfo.fullname', '') || _.trim(_.get(transportInfo, 'methodData.bankInfo.cardHolder', '') || ''),
				note: _.get(transferInfo, 'note', '') || _.get(transportInfo, 'note', ''),
				fee: Numeral(_.get(insertData.extraData, 'fee', '')).format(0, 0),
				// fee: _.get(insertData.extraData, 'fee', null) ? Numeral(_.get(insertData.extraData, 'fee', '')).format(0, 0) : 'Miễn phí',
				total: Numeral(_.get(history, 'amount', 0)).format(0, 0),
				methodTitle,
				serviceName: _.get(history, 'service.name', 'Chuyển tiền thành công'),
				cardNumber: _.get(transportInfo, 'methodData.bankInfo.cardNumber', '') || _.get(transportInfo, 'description', ''),
				titleEmail: 'chuyển tiền ví PayME',
				action: 'chuyển tiền thành công',
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				// subject: `Chuyển tiền đến ví của ${_.get(transferSender, 'receiverInfo.fullname', '')}`
				subject: `${_.get(history, 'service.name', 'Chuyển tiền thành công')}`
			}
		};
	};
	const TransferReceiverGraph = async (accountInfo, insertData) => {
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		const transferInfo = await this.broker.call('v1.eWalletTransferMoneyModel.findOne', [
			{
				transaction: _.get(insertData.extraData, 'service.transaction', '')
				// 'service.code': HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY
			}
		]);
		// bên CTT lưu note transport chưa lưu note trong transfer
		const transport = await this.broker.call('v1.eWalletTransportModel.findOne', [
			{
				'service.transaction': _.get(insertData.extraData, 'service.transaction', '')
			}
		]);
		if (_.isEmpty(transferInfo)) {
			return {};
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(_.get(insertData.extraData, 'createdAt', '')).format('HH:mm DD/MM/YYYY'),
				phoneSender: `0${(_.get(transferInfo, 'senderInfo.phone', '') || '').substr(2)} `,
				fullnameSender: _.get(transferInfo, 'senderInfo.fullname', ''),
				note: transferInfo.note || _.get(transport, 'note', ''),
				serviceName: `Nhận tiền từ ví của ${_.get(transferInfo, 'senderInfo.fullname', '')}`,
				total: Numeral(transferInfo.total).format(0, 0),
				fee: Numeral(history.fee).format(0, 0),
				titleEmail: 'Nhận tiền ví PayME',
				action: `nhận tiền thành công từ ${_.get(transferInfo, 'senderInfo.fullname', null) ? _.get(transferInfo, 'senderInfo.fullname', '').split(' ').slice(-1).join(' ') : ''}`,
				methodTitle: 'Ví PayME',
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				subject: `Nhận tiền từ ví của ${_.get(transferInfo, 'senderInfo.fullname', '')}`
			}
		};
	};
	const CartPaymentGraph = async (accountInfo, insertData) => {
		const card = await this.broker.call('v1.eWalletMobileCardModel.findOne', [{ transaction: _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(card)) {
			return {};
		}
		if (_.isEmpty(history)) {
			return {};
		}
		let cardTypeText = 'thẻ';
		let carTypeTile = 'MÃ GAME';
		if (_.includes([
			FrontendConstant.CARD_SUPPLIER.VIETTEL,
			FrontendConstant.CARD_SUPPLIER.MOBI,
			FrontendConstant.CARD_SUPPLIER.VINA,
			FrontendConstant.CARD_SUPPLIER.BEELINE,
			FrontendConstant.CARD_SUPPLIER.VN_MOBI
		], card.supplier)
		) {
			carTypeTile = 'MÃ ĐIỆN THOẠI';
			cardTypeText = 'thẻ';
		}
		let supplier = card.supplier || '';
		if ((card.supplier).toLowerCase() === 'mobi') {
			supplier = 'Mobifone';
		}
		if ((card.supplier).toLowerCase() === 'viettel') {
			supplier = 'Viettel';
		}
		if ((card.supplier).toLowerCase() === 'vina') {
			supplier = 'Vinaphone';
		}
		if ((card.supplier).toLowerCase() === 'garena') {
			supplier = 'Garena';
		}
		if ((card.supplier).toLowerCase() === 'zing') {
			supplier = 'Zing';
		}
		if ((card.supplier).toLowerCase() === 'beeline') {
			supplier = 'Beeline';
		}
		if (card.supplier === FrontendConstant.CARD_SUPPLIER.ECASH) {
			supplier = 'OnGame';
		}
		let methodTitle = 'Ví PayME';
		let cardNumber = '';
		let shortName = '';
		switch (_.get(history, 'payment.method', '')) {
			case PaymentConstant.PAYMENT_TYPE.LINKED:
				methodTitle = 'Tài khoản liên kết';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_CARD:
				methodTitle = 'Thẻ ATM nội địa';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_ACCOUNT:
				methodTitle = 'Tài khoản ngân hàng';
				break;
			case PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT:
				methodTitle = 'PayME Credit';
				break;
			case PaymentConstant.PAYMENT_TYPE.CREDIT_BALANCE:
				methodTitle = 'Ví tín dụng';
				break;
			default:
				break;
		}
		if (_.get(history, 'payment.method', null) !== null
			&& _.get(history, 'payment.method', null) !== PaymentConstant.PAYMENT_TYPE.WALLET
			&& _.get(history, 'payment.method', null) !== PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT
		) {
			const descriptionPayment = _.split(_.get(history, 'payment.description', ''), '-');
			shortName = _.get(descriptionPayment, '[0]', '');
			cardNumber = _.get(descriptionPayment, '[1]', '');
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(history, 'service.transaction', ''),
				dateTransaction: Moment(history.createdAt).format('HH:mm DD/MM/YYYY'),
				methodTitle,
				total: Numeral(card.amount).format(0, 0),
				pin: this.GateDecoder.decrypt(_.get(card, 'cardInfo[0].pin', '') || ''),
				supplier,
				// serviceName: _.get(history, 'service.name', ''),
				serviceName: 'Mua mã thẻ',
				cardTypeText: cardTypeText || '',
				carTypeTile: carTypeTile || '',
				serial: _.get(card, 'cardInfo[0].serial', ''),
				cardNumber,
				shortName,
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				// to: 'tripm@paymen.vn',
				// subject: _.get(history, 'service.name', 'Mua mã thẻ')
				subject: 'Mua mã thẻ'
			}
		};
	};
	const TopupPaymentGraph = async (accountInfo, insertData) => {
		const topup = await this.broker.call('v1.eWalletMobileTopupModel.findOne', [{ transaction: _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(topup)) {
			return {};
		}
		if (_.isEmpty(history)) {
			return {};
		}

		let supplier = topup.supplier || '';
		if ((topup.supplier).toLowerCase() === 'mobi') {
			supplier = 'Mobifone';
		}
		if ((topup.supplier).toLowerCase() === 'viettel') {
			supplier = 'Viettel';
		}
		if ((topup.supplier).toLowerCase() === 'vina') {
			supplier = 'Vinaphone';
		}
		let methodTitle = 'Ví PayME';
		let cardNumber = '';
		let shortName = '';
		switch (_.get(history, 'payment.method', '')) {
			case PaymentConstant.PAYMENT_TYPE.LINKED:
				methodTitle = 'Tài khoản liên kết';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_CARD:
				methodTitle = 'Thẻ ATM nội địa';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_ACCOUNT:
				methodTitle = 'Tài khoản ngân hàng';
				break;
			case PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT:
				methodTitle = 'PayME Credit';
				break;
			case PaymentConstant.PAYMENT_TYPE.CREDIT_BALANCE:
				methodTitle = 'Ví tín dụng';
				break;
			default:
				break;
		}
		if (_.get(history, 'payment.method', null) !== null
			&& _.get(history, 'payment.method', null) !== PaymentConstant.PAYMENT_TYPE.WALLET
			&& _.get(history, 'payment.method', null) !== PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT
		) {
			const descriptionPayment = _.split(_.get(history, 'payment.description', ''), '-');
			shortName = _.get(descriptionPayment, '[0]', '');
			cardNumber = _.get(descriptionPayment, '[1]', '');
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(history, 'service.transaction', ''),
				dateTransaction: Moment(history.createdAt).format('HH:mm DD/MM/YYYY'),
				methodTitle,
				total: Numeral(history.amount).format(0, 0),
				phoneTopup: topup.phone,
				serviceName: 'Nạp điện thoại',
				// serviceName: _.get(history, 'service.name', ''),
				cardNumber,
				shortName,
				supplier,
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				// to: 'tripm@paymen.vn',
				subject: 'Nạp điện thoại'
				// subject: _.get(history, 'service.name', 'Nạp điện thoại')
			}
		};
	};
	const IsecPaymentGraph = async (accountInfo, insertData) => {
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		const iSecInfo = await this.broker.call('v1.eWalletIsecModel.findOne', [{ transaction: _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(iSecInfo)) {
			return {};
		}
		if (_.isEmpty(history)) {
			return {};
		}

		let methodTitle = 'Ví PayME';
		let cardNumber = '';
		let shortName = '';
		switch (_.get(history, 'payment.method', '')) {
			case PaymentConstant.PAYMENT_TYPE.LINKED:
				methodTitle = 'Tài khoản liên kết';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_CARD:
				methodTitle = 'Thẻ ATM nội địa';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_ACCOUNT:
				methodTitle = 'Tài khoản ngân hàng';
				break;
			case PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT:
				methodTitle = 'PayME Credit';
				break;
			case PaymentConstant.PAYMENT_TYPE.CREDIT_BALANCE:
				methodTitle = 'Ví tín dụng';
				break;
			default:
				break;
		}
		if (_.get(history, 'payment.method', null) !== null
			&& _.get(history, 'payment.method', null) !== PaymentConstant.PAYMENT_TYPE.WALLET
			&& _.get(history, 'payment.method', null) !== PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT
		) {
			const descriptionPayment = _.split(_.get(history, 'payment.description', ''), '-');
			shortName = _.get(descriptionPayment, '[0]', '');
			cardNumber = _.get(descriptionPayment, '[1]', '');
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(history, 'service.transaction', ''),
				amount: Numeral(iSecInfo.amount).format(0, 0),
				dateTransaction: Moment(history.createdAt).format('HH:mm DD/MM/YYYY'),
				methodTitle,
				serviceName: _.get(history, 'service.name', ''),
				iSecHint: iSecInfo.iSec ? iSecInfo.iSec.hint : '',
				cardNumber,
				shortName,
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				// to: 'tripm@paymen.vn',
				subject: _.get(history, 'service.name', '')
			}
		};
	};
	const RefundCardGraph = async (accountInfo, insertData) => ({
		content: {
			phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
			fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
			transaction: _.get(insertData.extraData, 'service.transaction', ''),
			dateTransaction: Moment(_.get(insertData, 'extraData.publishedAt', '')).format('HH:mm DD/MM/YYYY') || Moment(new Date()).format('HH:mm DD/MM/YYYY'),
			total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
			reason: insertData.message || 'Hoàn tiền dịch vụ mua thẻ',
			footer: 'PayME'
		},
		email: {
			to: _.trim(_.get(accountInfo.data[0], 'email', '')),
			subject: 'Hoàn tiền dịch vụ mua thẻ'
		}
	});
	const CancelISecGraph = async (accountInfo, insertData) => ({
		content: {
			phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
			fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
			transaction: _.get(insertData.extraData, 'service.transaction', ''),
			dateTransaction: Moment(_.get(insertData, 'extraData.publishedAt', '')).format('HH:mm DD/MM/YYYY') || Moment(new Date()).format('HH:mm DD/MM/YYYY'),
			total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
			iSecHint: _.get(insertData.extraData, 'extraData.iSecInfo.hint', ''),
			amount: Numeral(_.get(insertData.extraData, 'amount', '')).format(0, 0),
			fee: Numeral(_.get(insertData.extraData, 'fee', '')).format(0, 0),
			footer: 'PayME'
		},
		email: {
			to: _.trim(_.get(accountInfo.data[0], 'email', '')),
			subject: 'Hủy mã iSec'
		}
	});
	const RedeemISecGraph = async (accountInfo, insertData) => {
		const iSecInfo = await this.broker.call('v1.eWalletIsecModel.findOne', [{ transaction: _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(_.get(insertData, 'extraData.publishedAt', '')).format('HH:mm DD/MM/YYYY') || Moment(new Date()).format('HH:mm DD/MM/YYYY'),
				total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
				iSecHint: _.get(iSecInfo, 'iSec.hint', ''),
				amount: Numeral(_.get(insertData.extraData, 'amount', '')).format(0, 0),
				fee: Numeral(_.get(insertData.extraData, 'fee', '')).format(0, 0),
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				subject: 'Nhận tiền iSec'
			}
		};
	};
	const RefundISecGraph = async (accountInfo, insertData) => ({
		content: {
			phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
			fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
			transaction: _.get(insertData.extraData, 'service.transaction', ''),
			dateTransaction: Moment(_.get(insertData, 'extraData.publishedAt', '')).format('HH:mm DD/MM/YYYY') || Moment(new Date()).format('HH:mm DD/MM/YYYY'),
			total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
			footer: 'PayME'
		},
		email: {
			to: _.trim(_.get(accountInfo.data[0], 'email', '')),
			subject: 'Hoàn tiền dịch vụ phát hành iSec'
		}
	});
	const SocialPaymentGraph = async (accountInfo, insertData) => {
		const socialLink = await this.broker.call('v1.eWalletSocialLinkModel.findOne', [{
			transaction: _.get(insertData.extraData, 'service.transaction', '')
			// state: FrontendConstant.TRANSFER_MONEY_STATE.SUCCEEDED
		}]);
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(history)) {
			return {};
		}
		if (_.isEmpty(socialLink)) {
			return {};
		}

		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(history.createdAt).format('HH:mm DD/MM/YYYY'),
				serviceName: _.get(history, 'service.name', ''),
				total: Numeral(history.total).format(0, 0),
				note: socialLink.note || _.get(insertData.extraData, 'note', ''),
				methodTitle: _.get(insertData.extraData, 'method.info.title', ''),
				link: _.get(socialLink, 'url', ''),
				footer: 'PayME'
				// cardNumber: _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') ? _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') : '',
				// shortName: _.get(insertData.extraData, 'method.bankInfo.shortName', '') ? _.get(insertData.extraData, 'method.bankInfo.shortName', '') : '',
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				subject: 'Chuyển tiền qua PayME Link'
			}
		};
	};
	const SalaryGraph = async (accountInfo, insertData) => {
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		const transferInfo = await this.broker.call('v1.eWalletTransferMoneyModel.findOne', [
			{
				transaction: _.get(insertData.extraData, 'service.transaction', '')
				// 'service.code': HistoryConstant.SERVICE_TYPE.TRANSFER_MONEY
			}
		]);
		if (_.isEmpty(transferInfo)) {
			return {};
		}
		let sender;
		if (_.get(insertData.extraData, 'service.code', null) === FrontendConstant.TAGS.SALARY && _.get(insertData.extraData, 'service.code') === 'ADD_MONEY') {
			const detailSalary = await this.broker.call('v1.ewalletAddMoneyModel.findOne', [{ transaction: _.get(insertData.extraData, 'service.transaction', '') }]);
			sender = _.get(detailSalary, 'sender', 'CTY CỔ PHẦN CÔNG NGHỆ PAYME');
		} else sender = _.get(insertData, 'extraData.transport.description', null);
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(_.get(insertData.extraData, 'createdAt', '')).format('HH:mm DD/MM/YYYY'),
				// sender: _.get(transferInfo, 'senderInfo.fullname', ''),
				sender,
				note: insertData.description || '',
				total: Numeral(history.total).format(0, 0),
				titleEmail: 'Nhận tiền ví PayME',
				methodTitle: 'Ví PayME',
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				subject: 'Chi Tiết Nhận Tiền'
			}
		};
	};
	const SocialReceiveLinkGraph = async (accountInfo, insertData) => {
		const socialLink = await this.broker.call('v1.eWalletSocialLinkModel.findOne', [{
			transaction: _.get(insertData.extraData, 'service.transaction', '')
			// state: FrontendConstant.TRANSFER_MONEY_STATE.SUCCEEDED
		}]);
		if (_.isEmpty(socialLink)) {
			return {};
		}
		if (socialLink.channel !== FrontendConstant.SOCIAL_CHANNEL.EMAIL) {
			return {};
		}
		if (_.get(socialLink, 'transferInfo.receiverInfo.email', false) === false) {
			return {};
		}
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: _.get(accountInfo.data[0], 'fullname', ''),
				total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
				note: _.get(insertData.extraData, 'note', ''),
				link: _.get(socialLink, 'url', ''),
				footer: 'PayME'
				// cardNumber: _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') ? _.get(insertData.extraData, 'method.bankInfo.cardNumber', '') : '',
				// shortName: _.get(insertData.extraData, 'method.bankInfo.shortName', '') ? _.get(insertData.extraData, 'method.bankInfo.shortName', '') : '',
			},
			email: {
				// email là người nhận link
				to: _.trim(_.get(socialLink, 'transferInfo.receiverInfo.email', '')),
				subject: 'Nhận tiền từ liên kết'
			}
		};
	};
	const advanceMoney = async (accountInfo, insertData) => {
		const creditInfo = await this.broker.call('v1.creditBalanceRegisterModel.findOne', [{
			accountId: _.get(accountInfo.data[0], 'id', 0),
			state: CreditBalanceConstant.REGISTER_STATE.SUCCEEDED
		}]);
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(_.get(insertData, 'extraData.publishedAt', '')).format('HH:mm DD/MM/YYYY') || Moment(new Date()).format('HH:mm DD/MM/YYYY'),
				total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
				fee: _.get(insertData.extraData, 'fee', 0) !== 0 ? Numeral(_.get(insertData.extraData, 'fee', '')).format(0, 0) : 'Miễn phí',
				amount: Numeral(_.get(insertData.extraData, 'amount', '')).format(0, 0),
				issueDate: Moment(_.get(creditInfo, 'approvedDate', '')).format('HH:mm DD/MM/YYYY') || Moment(new Date()).format('DD/MM/YYYY'),
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				subject: 'Nhận tiền từ Ví tín dụng'
			}
		};
	};
	const creditStatement = async (accountInfo, insertData) => {
		const creditInfo = await this.broker.call('v1.creditBalanceRegisterModel.findOne', [{
			accountId: _.get(accountInfo.data[0], 'id', 0),
			state: CreditBalanceConstant.REGISTER_STATE.SUCCEEDED
		}]);
		const history = await this.broker.call('v1.eWalletHistoryModel.findOne', [{ 'service.transaction': _.get(insertData.extraData, 'service.transaction', '') }]) || {};
		if (_.isEmpty(history)) {
			return {};
		}

		let methodTitle = 'Ví PayME';
		switch (_.get(history, 'payment.method', '')) {
			case PaymentConstant.PAYMENT_TYPE.LINKED:
				methodTitle = 'Tài khoản liên kết';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_CARD:
			case PaymentConstant.PAYMENT_TYPE.BANK_CARD_PG:
				methodTitle = 'Thẻ ATM nội địa';
				break;
			case PaymentConstant.PAYMENT_TYPE.BANK_ACCOUNT:
				methodTitle = 'Tài khoản ngân hàng';
				break;
			case PaymentConstant.PAYMENT_TYPE.PAYME_CREDIT:
				methodTitle = 'PayME Credit';
				break;
			default:
				break;
		}
		const descriptionPayment = _.split(_.get(insertData.extraData, 'payment.description', ''), '-');
		const shortName = _.get(descriptionPayment, '[0]', '');
		const cardNumber = _.get(descriptionPayment, '[1]', '');
		return {
			content: {
				phone: `0${(_.get(accountInfo.data[0], 'phone', '')).substr(2)} `,
				fullname: (_.get(accountInfo.data[0], 'fullname', '') || '').split(' ').slice(-1).join(' '),
				transaction: _.get(insertData.extraData, 'service.transaction', ''),
				dateTransaction: Moment(_.get(insertData, 'extraData.publishedAt', '')).format('HH:mm DD/MM/YYYY') || Moment(new Date()).format('HH:mm DD/MM/YYYY'),
				total: Numeral(_.get(insertData.extraData, 'total', '')).format(0, 0),
				amount: Numeral(_.get(insertData.extraData, 'amount', '')).format(0, 0),
				fee: Numeral(_.get(insertData.extraData, 'fee', '')).format(0, 0),
				issueDate: Moment(_.get(creditInfo, 'approvedDate', '')).format('HH:mm DD/MM/YYYY') || Moment(new Date()).format('DD/MM/YYYY'),
				methodTitle,
				shortName,
				cardNumber,
				footer: 'PayME'
			},
			email: {
				to: _.trim(_.get(accountInfo.data[0], 'email', '')),
				subject: 'Thanh toán dư nợ Ví tín dụng'
			}
		};
	};
	return {
		BillPayment,
		WithdrawBankGateway,
		WithdrawBankPVCBank,
		Deposit,
		TransferSender,
		TransferReceiver,
		RefundCard,
		RefundMoney,
		CancelISec,
		InsightEmail,
		RedeemISec,
		RefundISec,
		SocialPayment,
		SocialRequestPayment,
		SocialReceiveLink,
		SocialReceive,
		CartPayment,
		TopupPayment,
		IsecPayment,
		BillPaymentGraph,
		WithdrawBankGatewayGraph,
		DepositGraph,
		TransferSenderGraph,
		TransferReceiverGraph,
		CartPaymentGraph,
		TopupPaymentGraph,
		IsecPaymentGraph,
		RefundCardGraph,
		CancelISecGraph,
		RedeemISecGraph,
		RefundISecGraph,
		SocialPaymentGraph,
		SalaryGraph,
		SocialReceiveLinkGraph,
		advanceMoney,
		creditStatement
	};
};
