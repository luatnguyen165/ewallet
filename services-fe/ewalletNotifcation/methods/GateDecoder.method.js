/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var hexdec = require('hex-to-dec');

var pgpCFB
const bytebit = [0200, 0100, 040, 020, 010, 04, 02, 01]
const bigbyte = [
	0x800000, 0x400000, 0x200000, 0x100000,
	0x80000, 0x40000, 0x20000, 0x10000,
	0x8000, 0x4000, 0x2000, 0x1000,
	0x800, 0x400, 0x200, 0x100,
	0x80, 0x40, 0x20, 0x10,
	0x8, 0x4, 0x2, 0x1
]

const pc1 = [
	56, 48, 40, 32, 24, 16, 8, 0, 57, 49, 41, 33, 25, 17,
	9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35,
	62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21,
	13, 5, 60, 52, 44, 36, 28, 20, 12, 4, 27, 19, 11, 3
]

const pc2 = [
	13, 16, 10, 23, 0, 4, 2, 27, 14, 5, 20, 9,
	22, 18, 11, 3, 25, 7, 15, 6, 26, 19, 12, 1,
	40, 51, 30, 36, 46, 54, 29, 39, 50, 44, 32, 47,
	43, 48, 38, 55, 33, 52, 45, 41, 49, 35, 28, 31
]
const totrot = [
	1, 2, 4, 6, 8, 10, 12, 14,
	15, 17, 19, 21, 23, 25, 27, 28
]
const SP1 = [
	0x01010400, 0x00000000, 0x00010000, 0x01010404,
	0x01010004, 0x00010404, 0x00000004, 0x00010000,
	0x00000400, 0x01010400, 0x01010404, 0x00000400,
	0x01000404, 0x01010004, 0x01000000, 0x00000004,
	0x00000404, 0x01000400, 0x01000400, 0x00010400,
	0x00010400, 0x01010000, 0x01010000, 0x01000404,
	0x00010004, 0x01000004, 0x01000004, 0x00010004,
	0x00000000, 0x00000404, 0x00010404, 0x01000000,
	0x00010000, 0x01010404, 0x00000004, 0x01010000,
	0x01010400, 0x01000000, 0x01000000, 0x00000400,
	0x01010004, 0x00010000, 0x00010400, 0x01000004,
	0x00000400, 0x00000004, 0x01000404, 0x00010404,
	0x01010404, 0x00010004, 0x01010000, 0x01000404,
	0x01000004, 0x00000404, 0x00010404, 0x01010400,
	0x00000404, 0x01000400, 0x01000400, 0x00000000,
	0x00010004, 0x00010400, 0x00000000, 0x01010004
]

const SP2 = [
	0x80108020, 0x80008000, 0x00008000, 0x00108020,
	0x00100000, 0x00000020, 0x80100020, 0x80008020,
	0x80000020, 0x80108020, 0x80108000, 0x80000000,
	0x80008000, 0x00100000, 0x00000020, 0x80100020,
	0x00108000, 0x00100020, 0x80008020, 0x00000000,
	0x80000000, 0x00008000, 0x00108020, 0x80100000,
	0x00100020, 0x80000020, 0x00000000, 0x00108000,
	0x00008020, 0x80108000, 0x80100000, 0x00008020,
	0x00000000, 0x00108020, 0x80100020, 0x00100000,
	0x80008020, 0x80100000, 0x80108000, 0x00008000,
	0x80100000, 0x80008000, 0x00000020, 0x80108020,
	0x00108020, 0x00000020, 0x00008000, 0x80000000,
	0x00008020, 0x80108000, 0x00100000, 0x80000020,
	0x00100020, 0x80008020, 0x80000020, 0x00100020,
	0x00108000, 0x00000000, 0x80008000, 0x00008020,
	0x80000000, 0x80100020, 0x80108020, 0x00108000
]

const SP3 = [
	0x00000208, 0x08020200, 0x00000000, 0x08020008,
	0x08000200, 0x00000000, 0x00020208, 0x08000200,
	0x00020008, 0x08000008, 0x08000008, 0x00020000,
	0x08020208, 0x00020008, 0x08020000, 0x00000208,
	0x08000000, 0x00000008, 0x08020200, 0x00000200,
	0x00020200, 0x08020000, 0x08020008, 0x00020208,
	0x08000208, 0x00020200, 0x00020000, 0x08000208,
	0x00000008, 0x08020208, 0x00000200, 0x08000000,
	0x08020200, 0x08000000, 0x00020008, 0x00000208,
	0x00020000, 0x08020200, 0x08000200, 0x00000000,
	0x00000200, 0x00020008, 0x08020208, 0x08000200,
	0x08000008, 0x00000200, 0x00000000, 0x08020008,
	0x08000208, 0x00020000, 0x08000000, 0x08020208,
	0x00000008, 0x00020208, 0x00020200, 0x08000008,
	0x08020000, 0x08000208, 0x00000208, 0x08020000,
	0x00020208, 0x00000008, 0x08020008, 0x00020200
]

const SP4 = [
	0x00802001, 0x00002081, 0x00002081, 0x00000080,
	0x00802080, 0x00800081, 0x00800001, 0x00002001,
	0x00000000, 0x00802000, 0x00802000, 0x00802081,
	0x00000081, 0x00000000, 0x00800080, 0x00800001,
	0x00000001, 0x00002000, 0x00800000, 0x00802001,
	0x00000080, 0x00800000, 0x00002001, 0x00002080,
	0x00800081, 0x00000001, 0x00002080, 0x00800080,
	0x00002000, 0x00802080, 0x00802081, 0x00000081,
	0x00800080, 0x00800001, 0x00802000, 0x00802081,
	0x00000081, 0x00000000, 0x00000000, 0x00802000,
	0x00002080, 0x00800080, 0x00800081, 0x00000001,
	0x00802001, 0x00002081, 0x00002081, 0x00000080,
	0x00802081, 0x00000081, 0x00000001, 0x00002000,
	0x00800001, 0x00002001, 0x00802080, 0x00800081,
	0x00002001, 0x00002080, 0x00800000, 0x00802001,
	0x00000080, 0x00800000, 0x00002000, 0x00802080
]

const SP5 = [
	0x00000100, 0x02080100, 0x02080000, 0x42000100,
	0x00080000, 0x00000100, 0x40000000, 0x02080000,
	0x40080100, 0x00080000, 0x02000100, 0x40080100,
	0x42000100, 0x42080000, 0x00080100, 0x40000000,
	0x02000000, 0x40080000, 0x40080000, 0x00000000,
	0x40000100, 0x42080100, 0x42080100, 0x02000100,
	0x42080000, 0x40000100, 0x00000000, 0x42000000,
	0x02080100, 0x02000000, 0x42000000, 0x00080100,
	0x00080000, 0x42000100, 0x00000100, 0x02000000,
	0x40000000, 0x02080000, 0x42000100, 0x40080100,
	0x02000100, 0x40000000, 0x42080000, 0x02080100,
	0x40080100, 0x00000100, 0x02000000, 0x42080000,
	0x42080100, 0x00080100, 0x42000000, 0x42080100,
	0x02080000, 0x00000000, 0x40080000, 0x42000000,
	0x00080100, 0x02000100, 0x40000100, 0x00080000,
	0x00000000, 0x40080000, 0x02080100, 0x40000100
]

const SP6 = [
	0x20000010, 0x20400000, 0x00004000, 0x20404010,
	0x20400000, 0x00000010, 0x20404010, 0x00400000,
	0x20004000, 0x00404010, 0x00400000, 0x20000010,
	0x00400010, 0x20004000, 0x20000000, 0x00004010,
	0x00000000, 0x00400010, 0x20004010, 0x00004000,
	0x00404000, 0x20004010, 0x00000010, 0x20400010,
	0x20400010, 0x00000000, 0x00404010, 0x20404000,
	0x00004010, 0x00404000, 0x20404000, 0x20000000,
	0x20004000, 0x00000010, 0x20400010, 0x00404000,
	0x20404010, 0x00400000, 0x00004010, 0x20000010,
	0x00400000, 0x20004000, 0x20000000, 0x00004010,
	0x20000010, 0x20404010, 0x00404000, 0x20400000,
	0x00404010, 0x20404000, 0x00000000, 0x20400010,
	0x00000010, 0x00004000, 0x20400000, 0x00404010,
	0x00004000, 0x00400010, 0x20004010, 0x00000000,
	0x20404000, 0x20000000, 0x00400010, 0x20004010
]

const SP7 = [
	0x00200000, 0x04200002, 0x04000802, 0x00000000,
	0x00000800, 0x04000802, 0x00200802, 0x04200800,
	0x04200802, 0x00200000, 0x00000000, 0x04000002,
	0x00000002, 0x04000000, 0x04200002, 0x00000802,
	0x04000800, 0x00200802, 0x00200002, 0x04000800,
	0x04000002, 0x04200000, 0x04200800, 0x00200002,
	0x04200000, 0x00000800, 0x00000802, 0x04200802,
	0x00200800, 0x00000002, 0x04000000, 0x00200800,
	0x04000000, 0x00200800, 0x00200000, 0x04000802,
	0x04000802, 0x04200002, 0x04200002, 0x00000002,
	0x00200002, 0x04000000, 0x04000800, 0x00200000,
	0x04200800, 0x00000802, 0x00200802, 0x04200800,
	0x00000802, 0x04000002, 0x04200802, 0x04200000,
	0x00200800, 0x00000000, 0x00000002, 0x04200802,
	0x00000000, 0x00200802, 0x04200000, 0x00000800,
	0x04000002, 0x04000800, 0x00000800, 0x00200002
]

const SP8 = [
	0x10001040, 0x00001000, 0x00040000, 0x10041040,
	0x10000000, 0x10001040, 0x00000040, 0x10000000,
	0x00040040, 0x10040000, 0x10041040, 0x00041000,
	0x10041000, 0x00041040, 0x00001000, 0x00000040,
	0x10040000, 0x10000040, 0x10001000, 0x00001040,
	0x00041000, 0x00040040, 0x10040040, 0x10041000,
	0x00001040, 0x00000000, 0x00000000, 0x10040040,
	0x10000040, 0x10001000, 0x00041040, 0x00040000,
	0x00041040, 0x00040000, 0x10041000, 0x00001000,
	0x00000040, 0x10040040, 0x00001000, 0x00041040,
	0x10001000, 0x00000040, 0x10000040, 0x10040000,
	0x10040040, 0x10000000, 0x00040000, 0x10001040,
	0x00000000, 0x10041040, 0x00040040, 0x10000040,
	0x10040000, 0x10001000, 0x10001040, 0x00000000,
	0x10041040, 0x00041000, 0x00041000, 0x00001040,
	0x00001040, 0x00040040, 0x10000000, 0x10041000
]

const BLOCK_SIZE = 8
//const key = '@1Pas!sw#or^d';
//const iv = '%1Az=-@qT';

//const bufOff = 0;
function count(o) {
	return o ? o.length : 0;
}
function ord(string) {
	//  discuss at: http://locutus.io/php/ord/
	// original by: Kevin van Zonneveld (http://kvz.io)
	// bugfixed by: Onno Marsman (https://twitter.com/onnomarsman)
	// improved by: Brett Zamir (http://brett-zamir.me)
	//    input by: incidence
	//   example 1: ord('K')
	//   returns 1: 75
	//   example 2: ord('\uD800\uDC00'); // surrogate pair to create a single Unicode character
	//   returns 2: 65536

	var str = string + ''
	var code = str.charCodeAt(0)

	if (code >= 0xD800 && code <= 0xDBFF) {
		// High surrogate (could change last hex to 0xDB7F to treat
		// high private surrogates as single characters)
		var hi = code
		if (str.length === 1) {
			// This is just a high surrogate with no following low surrogate,
			// so we return its value;
			return code
			// we could also throw an error as it is not a complete character,
			// but someone may want to know
		}
		var low = str.charCodeAt(1)
		return ((hi - 0xD800) * 0x400) + (low - 0xDC00) + 0x10000
	}
	if (code >= 0xDC00 && code <= 0xDFFF) {
		// Low surrogate
		// This is just a low surrogate with no preceding high surrogate,
		// so we return its value;
		return code
		// we could also throw an error as it is not a complete character,
		// but someone may want to know
	}

	return code
}
function chr(codePt) {
	//  discuss at: http://locutus.io/php/chr/
	// original by: Kevin van Zonneveld (http://kvz.io)
	// improved by: Brett Zamir (http://brett-zamir.me)
	//   example 1: chr(75) === 'K'
	//   example 1: chr(65536) === '\uD800\uDC00'
	//   returns 1: true
	//   returns 1: true

	if (codePt > 0xFFFF) { // Create a four-byte string (length 2) since this code point is high
		//   enough for the UTF-16 encoding (JavaScript internal use), to
		//   require representation with two surrogates (reserved non-characters
		//   used for building other characters; the first is "high" and the next "low")
		codePt -= 0x10000
		return String.fromCharCode(0xD800 + (codePt >> 10), 0xDC00 + (codePt & 0x3FF))
	} else if (codePt < 0) {
		return String.fromCharCode(codePt + 256)
	}
	return String.fromCharCode(codePt)
}

function strlen(str) {
	return str ? str.length : 0
}
module.exports = function GateDecoder() {
	keyByte = [];
	ivByte = [];
	IV = null;
	buf = null;
	cbcV = null;
	cbcNextV = null;

	workingKey1 = null;
	workingKey2 = null;
	workingKey3 = null;

	const Encoder = (encrypting = false, keyData = '', ivData = '') => {

		for (i = 0; i < 16; i++) {
			if (i < keyData.length) {
				this.keyByte[this.keyByte.length] = ord(keyData[i]);
			} else {
				this.keyByte[this.keyByte.length] = 0;
			}
		}

		for (i = 0; i < 8; i++) {
			this.ivByte[this.ivByte.length] = ord(ivData[i]);
		}

		if (this.initialize()) {

			if (count(this.keyByte) == 16 || count(this.keyByte) == 24) {
				var key1 = []
				for (i = 0; i < 8; i++) {
					key1[key1.length] = 0x0000;
				}

				for (i = 0; i < count(key1); i++) {
					key1[i] = this.keyByte[i];
				}

				this.workingKey1 = this.generateWorkingKey(encrypting, key1);
				var key2 = []
				for (i = 0; i < 8; i++) {
					key2[key2.length] = 0x0000;
				}

				for (i = 8; i < count(key2) + 8; i++) {
					key2[i - 8] = this.keyByte[i];
				}

				this.workingKey2 = this.generateWorkingKey(!encrypting, key2);
				var key3 = []
				if (count(this.keyByte) == 24) {
					for (i = 0; i < 8; i++) {
						key3[key3.length] = 0x0000;
					}

					for (i = 16; i < count(key3) + 16; i++) {
						key3[i - 16] = keyData[i];
					}

				} else {
					this.workingKey3 = this.workingKey1;
				}
			}
		}
	};
	const initialize = () => {

		this.buf = []
		this.IV = []
		this.cbcV = []
		this.cbcNextV = []

		for (i = 0; i < BLOCK_SIZE; i++) {
			this.buf[this.buf.length] = 0x0000;
			this.cbcV[this.cbcV.length] = 0x0000;
			this.cbcNextV[this.cbcNextV.length] = 0x0000;
		}

		if (count(this.ivByte) == BLOCK_SIZE) {

			for (i = 0; i < count(this.ivByte); i++) {

				this.IV[this.IV.length] = this.ivByte[i];

			}

			for (i = 0; i < count(this.IV); i++) {

				this.cbcV[i] = this.IV[i];

			}

			for (i = 0; i < count(this.cbcNextV); i++) {

				this.cbcNextV[i] = 0x0000;

			}

			return true;

		}

		return false;
	};
	const urshift = (n, s) => {
		return (n >= 0) ? (n >> s) :
			((n & 0x7fffffff) >> s) |
			(0x40000000 >> (s - 1));
	};
	const arraycopy = (inn, inOff, out, outOff, length) => {

		j = outOff;

		for (i = inOff; i < inOff + length; i++) {

			out[j++] = inn[i];

		}

	};
	const generateWorkingKey = (encrypting, key) => {

		var newKey = [];
		var pc1m = [];
		var pcr = [];

		for (j = 0; j < 56; j++) {

			l = pc1[j];

			pc1m[j] = ((key[this.urshift(l, 3)] & bytebit[l & 07]) != 0);

		}

		for (i = 0; i < 16; i++) {

			if (encrypting) {

				m = i << 1;

			} else {

				m = (15 - i) << 1;
			}

			n = m + 1;
			newKey[m] = newKey[n] = 0;

			for (j = 0; j < 28; j++) {

				l = j + totrot[i];

				if (l < 28) {

					pcr[j] = pc1m[l];

				} else {

					pcr[j] = pc1m[l - 28];

				}

			}

			for (j = 28; j < 56; j++) {

				l = j + totrot[i];

				if (l < 56) {

					pcr[j] = pc1m[l];

				} else {

					pcr[j] = pc1m[l - 28];

				}

			}

			for (j = 0; j < 24; j++) {

				if (pcr[pc2[j]]) {

					newKey[m] |= bigbyte[j];

				}

				if (pcr[pc2[j + 24]]) {

					newKey[n] |= bigbyte[j];

				}

			}
		}

		for (i = 0; i != 32; i += 2) {

			i1 = newKey[i];
			i2 = newKey[i + 1];

			newKey[i] = ((i1 & 0x00fc0000) << 6) | ((i1 & 0x00000fc0) << 10)
				| this.urshift((i2 & 0x00fc0000), 10) | this.urshift((i2 & 0x00000fc0), 6);

			newKey[i + 1] = ((i1 & 0x0003f000) << 12) | ((i1 & 0x0000003f) << 16)
				| this.urshift((i2 & 0x0003f000), 4) | (i2 & 0x0000003f);

		}

		return newKey;

	};
	const desFunc = (wKey, inn, inOff, out, outOff) => {

		left = 0;
		left = (inn[inOff + 0] & 0xff) << 24;
		left |= (inn[inOff + 1] & 0xff) << 16;
		left |= (inn[inOff + 2] & 0xff) << 8;
		left |= (inn[inOff + 3] & 0xff);

		right = 0;
		right = (inn[inOff + 4] & 0xff) << 24;
		right |= (inn[inOff + 5] & 0xff) << 16;
		right |= (inn[inOff + 6] & 0xff) << 8;
		right |= (inn[inOff + 7] & 0xff);

		work = 0;
		work = (this.urshift(left, 4) ^ right) & 0x0f0f0f0f;
		right ^= work;
		left ^= (work << 4);
		work = (this.urshift(left, 16) ^ right) & 0x0000ffff;
		right ^= work;
		left ^= (work << 16);
		work = (this.urshift(right, 2) ^ left) & 0x33333333;
		left ^= work;
		right ^= (work << 2);
		work = (this.urshift(right, 8) ^ left) & 0x00ff00ff;
		left ^= work;
		right ^= (work << 8);
		right = ((right << 1) | (this.urshift(right, 31) & 1)) & 0xffffffff;
		work = (left ^ right) & 0xaaaaaaaa;
		left ^= work;
		right ^= work;
		left = ((left << 1) | (this.urshift(left, 31) & 1)) & 0xffffffff;

		for (round = 0; round < 8; round++) {
			fval = 0;

			work = (right << 28) | this.urshift(right, 4);
			work ^= wKey[round * 4 + 0];
			fval = SP7[work & 0x3f];
			fval |= SP5[this.urshift(work, 8) & 0x3f];
			fval |= SP3[this.urshift(work, 16) & 0x3f];
			fval |= SP1[this.urshift(work, 24) & 0x3f];
			work = right ^ wKey[round * 4 + 1];
			fval |= SP8[work & 0x3f];
			fval |= SP6[this.urshift(work, 8) & 0x3f];
			fval |= SP4[this.urshift(work, 16) & 0x3f];
			fval |= SP2[this.urshift(work, 24) & 0x3f];
			left ^= fval;
			work = (left << 28) | this.urshift(left, 4);
			work ^= wKey[round * 4 + 2];
			fval = SP7[work & 0x3f];
			fval |= SP5[this.urshift(work, 8) & 0x3f];
			fval |= SP3[this.urshift(work, 16) & 0x3f];
			fval |= SP1[this.urshift(work, 24) & 0x3f];
			work = left ^ wKey[round * 4 + 3];
			fval |= SP8[work & 0x3f];
			fval |= SP6[this.urshift(work, 8) & 0x3f];
			fval |= SP4[this.urshift(work, 16) & 0x3f];
			fval |= SP2[this.urshift(work, 24) & 0x3f];
			right ^= fval;
		}

		right = (right << 31) | this.urshift(right, 1);
		work = (left ^ right) & 0xaaaaaaaa;
		left ^= work;
		right ^= work;
		left = (left << 31) | this.urshift(left, 1);
		work = (this.urshift(left, 8) ^ right) & 0x00ff00ff;
		right ^= work;
		left ^= (work << 8);
		work = (this.urshift(left, 2) ^ right) & 0x33333333;
		right ^= work;
		left ^= (work << 2);
		work = (this.urshift(right, 16) ^ left) & 0x0000ffff;
		left ^= work;
		right ^= (work << 16);
		work = (this.urshift(right, 4) ^ left) & 0x0f0f0f0f;
		left ^= work;
		right ^= (work << 4);

		x0 = (this.urshift(right, 24) & 0xff);
		x1 = (this.urshift(right, 16) & 0xff);
		x2 = (this.urshift(right, 8) & 0xff);
		x3 = (right & 0xff);
		x4 = (this.urshift(left, 24) & 0xff);
		x5 = (this.urshift(left, 16) & 0xff);
		x6 = (this.urshift(left, 8) & 0xff);
		x7 = (left & 0xff);

		out[outOff + 0] = x0 < 128 ? x0 : (x0 - 128) - 128;
		out[outOff + 1] = x1 < 128 ? x1 : (x1 - 128) - 128;
		out[outOff + 2] = x2 < 128 ? x2 : (x2 - 128) - 128;
		out[outOff + 3] = x3 < 128 ? x3 : (x3 - 128) - 128;
		out[outOff + 4] = x4 < 128 ? x4 : (x4 - 128) - 128;
		out[outOff + 5] = x5 < 128 ? x5 : (x5 - 128) - 128;
		out[outOff + 6] = x6 < 128 ? x6 : (x6 - 128) - 128;
		out[outOff + 7] = x7 < 128 ? x7 : (x7 - 128) - 128;

	};
	const cipherProcessBlock = (inn, inOff, out, outOff) => {

		if (this.workingKey1 != null
			&& (inOff + BLOCK_SIZE) <= count(inn)
			&& (outOff + BLOCK_SIZE) <= count(out)) {
			var temp = []
			for (i = 0; i < BLOCK_SIZE; i++) {
				temp[temp.length] = 0x0000;
			}

			this.desFunc(this.workingKey3, inn, inOff, temp, 0);

			this.desFunc(this.workingKey2, temp, 0, temp, 0);

			this.desFunc(this.workingKey1, temp, 0, out, outOff);

		}
		return BLOCK_SIZE;

	};
	const processBlock = (inn, inOff, out, outOff) => {

		if ((inOff + BLOCK_SIZE) <= count(inn)) {

			this.arraycopy(inn, inOff, this.cbcNextV, 0, BLOCK_SIZE);

			length = this.cipherProcessBlock(inn, inOff, out, outOff);

			for (i = 0; i < BLOCK_SIZE; i++) {

				out[outOff + i] ^= this.cbcV[i];

			}

			tmp = this.cbcV;
			this.cbcV = this.cbcNextV;
			this.cbcNextV = tmp;

			return length;

		}

	};
	const getUpdateOutputSize = (len, bufOff) => {

		//global pgpCFB;
		//pgpCFB;

		total = len + bufOff;
		leftOver = 0;

		if (pgpCFB) {
			leftOver = total % count(this.buf) - (BLOCK_SIZE + 2);
		} else {
			leftOver = total % count(this.buf);
		}

		return total - leftOver;
	};
	const processBytes = (inn, inOff, len, out, outOff) => {

		if (len >= 0) {

			let bufOff = 0;

			length = this.getUpdateOutputSize(len, bufOff);

			if (length > 0) {

				if ((outOff + length) <= count(out)) {

					resultLen = 0;
					gapLen = count(this.buf) - bufOff;

					if (len > gapLen) {

						this.arraycopy(inn, inOff, this.buf, bufOff, gapLen);

						resultLen += this.processBlock(this.buf, 0, out, outOff);

						bufOff = 0;
						len -= gapLen;
						inOff += gapLen;

						while (len > count(this.buf)) {
							resultLen += this.processBlock(inn, inOff, out, outOff + resultLen);

							len -= BLOCK_SIZE;
							inOff += BLOCK_SIZE;
						}

					}

					this.arraycopy(inn, inOff, this.buf, bufOff, len);

					bufOff += len;

					if (bufOff == count(this.buf)) {

						resultLen += this.processBlock(this.buf, 0, out, outOff + resultLen);
						bufOff = 0;

					}

					return resultLen;

				}

			}

		}

	};
	const decrypt = (encryptedData) => {

		this.initialize();

		var encByte = []
		for (i = 0; i < strlen(encryptedData) - 1; i += 2) {

			//console.log(encryptedData[i].encryptedData);
			//value = hexdec(encryptedData[i].encryptedData[i + 1]);
			value = hexdec(encryptedData[i] + encryptedData[i + 1]);

			if (value > 128) {
				encByte[encByte.length] = value - 256;

			} else {

				encByte[encByte.length] = value;

			}

		}
		var array = []
		for (i = 0; i < count(encByte); i++) {

			array[array.length] = 0x0000;

		}

		this.processBytes(encByte, 0, count(encByte), array, 0);

		start = -1;
		end = count(array);

		for (i = 0; i < count(array); i++) {

			if (start == -1 && array[i] >= 32) {

				start = i;

			}

			value = array[i] > 0 ? array[i] : (128 + array[i] + 128);

			if (start > -1 && ((value >= 48 && value <= 57)
				|| (value >= 65 && value <= 90)
				|| (value >= 97 && value <= 122))) {

				end = i;

			}

		}

		start = (start == -1 ? 0 : start);

		result = "";

		for (i = start; i <= end; i++) {

			result += chr(array[i]);

		}

		return result;

	}

	return {
		Encoder,
		initialize,
		urshift,
		arraycopy,
		generateWorkingKey,
		desFunc,
		cipherProcessBlock,
		processBlock,
		getUpdateOutputSize,
		processBytes,
		decrypt,
	}
}

