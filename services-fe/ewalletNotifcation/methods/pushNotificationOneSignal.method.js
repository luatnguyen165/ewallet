/* eslint-disable no-useless-catch */
const OneSignal = require('onesignal-node');
const _ = require('lodash');

const myClient = new OneSignal.Client(process.env.ONESIGNAL_APPID, process.env.ONESIGNAL_APIKEY);

module.exports = function PushNotificationService() {
	const pushWithFilterAccountId = async (accountId, headings, contents, additionalData = {}) => {
		try {
			const obj = {
				headings,
				contents,
				data: additionalData,
				filters: [
					{
						field: 'tag', key: 'accountId', relation: '=', value: accountId
					},
					{
						field: 'tag', key: 'env', relation: '=', value: process.env.ENV_TAG // live = prod,đưa ra file config
					}
				]
			};

			console.log(`obj : ${JSON.stringify(obj)}`);

			// const response = await client.createNotification(notification);
			const result = await myClient.createNotification(obj);
			console.log(`result : ${JSON.stringify(result)}`);
			return true;
		} catch (error) {
			throw error;
		}
	};

	const pushAllAccountId = async (headings, contents) => {
		try {
			const obj = {
				headings,
				contents,
				filters: [
					{
						field: 'tag', key: 'accountId', relation: 'exists'
					},
					{
						field: 'tag', key: 'env', relation: '=', value: process.env.ENV_TAG
					}
				]
			};
			console.log(`obj : ${JSON.stringify(obj)}`);

			// const response = await client.createNotification(notification);
			const result = await myClient.createNotification(obj);
			console.log(`result : ${JSON.stringify(result)}`);
			return true;
		} catch (error) {
			throw error;
			// return false;
		}
	};

	const pushWithFilterClientId = async (clientId, headings, contents, additionalData = {}) => {
		try {
			const obj = {
				headings,
				contents,
				data: additionalData,
				filters: [
					{
						field: 'tag', key: 'deviceId', relation: '=', value: clientId
					},
					{
						field: 'tag', key: 'env', relation: '=', value: process.env.ENV_TAG // live = prod,đưa ra file config
					}
				]
			};

			console.log(`obj : ${JSON.stringify(obj)}`);

			// const response = await client.createNotification(notification);
			const result = await myClient.createNotification(obj);
			console.log(`result : ${JSON.stringify(result)}`);
			return true;
		} catch (error) {
			throw error;
			// return false;
		}
	};

	const editTags = async (tags = {}, clientId) => {
		tags.env = process.env_ENV_TAG;
		return true;
	};

	const editTagsOneSignal = async (tags = {}, clientId) => {
		try {
			if (!_.isObject(tags)) return false;
			tags.deviceId = clientId;

			const response = await myClient.editTagsWithExternalUserIdDevice(clientId, {
				tags
			});
			// console.log(`OneSignal -> editTagsState: ${JSON.stringify(response.body)}`);
			return true;
		} catch (error) {
			// console.log(`OneSignal -> editTagsError: ${JSON.stringify(error)}`);
			return false;
		}
	};
	return {
		pushWithFilterAccountId,
		pushAllAccountId,
		pushWithFilterClientId,
		editTags,
		editTagsOneSignal
	};
};
