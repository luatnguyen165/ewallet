module.exports = {
	FORMATTER: {
		WALLET: 'wallet',
		OPEN_API_WALLET: 'openApiWallet',
		PAYMECREDIT: 'paymecredit'
	},
	CHANGED_ENUM: {
		IN: '+',
		OUT: '-',
		NONE: { $in: ['', null] }
	},
	STATE_TITLE: {
		PENDING: 'Chờ xử lý',
		PROCESSING: 'Đang xử lý',
		SUCCEEDED: 'Thành công',
		FAILED: 'Thất bại',
		CANCELED: 'Đã Huỷ',
		REFUNDED: 'Đã Hoàn',
		USED: 'Thành Công',
		EXPIRED: 'Hết hạn'
	},
	STATE_PAYMENT: {
		SUCCEEDED: 'thành công',
		USED: 'thành công',
		REFUNDED: 'thành công',
		FAILED: 'thất bại'
		// PENDING: 'đang xử lý',
	},
	SERVICE_TYPE: {
		BASIC: 'BASIC',
		SOCIAL_PAYMENT: 'SOCIAL_PAYMENT',
		SOCIAL_PAYMENT_PG: 'SOCIAL_PAYMENT_PG',
		WALLET_PAY: 'WALLET_PAY',
		WALLET_QR: 'WALLET_QR',
		TRANSFER_PAYME: 'TRANSFER_PAYME',
		DEPOSIT: 'DEPOSIT',
		WITHDRAW: 'WITHDRAW',
		TRANSFER_MONEY: 'TRANSFER_MONEY',
		OPEN_EWALLET_PAYMENT: 'OPEN_EWALLET_PAYMENT',
		BILL: 'BILL',
		LINKED: 'LINKED',
		MOBILE_CARD: 'MOBILE_CARD',
		MOBILE_TOPUP: 'MOBILE_TOPUP',
		REFUND_MONEY: 'REFUND_MONEY',
		ISEC: 'ISEC',
		SALARY: 'SALARY',
		ADVANCE_MONEY: 'ADVANCE_MONEY', // ƯNG TIỀN
		CREDIT_STATEMENT: 'CREDIT_STATEMENT', // THANH TOÁN NỢ
		CREDIT_SETTLEMENT: 'CREDIT_SETTLEMENT', // TẤT TOÁN NỢ
		PAYME_CREDIT: 'PAYME_CREDIT', // THANH TOÁN DỊCH VỤ
		ADVANCE_SALARY: 'ADVANCE_SALARY', // ỨNG LƯƠNG
		ADVANCE_SALARY_SETTLEMENT: 'ADVANCE_SALARY_SETTLEMENT',
		FAST_LOAN: 'FAST_LOAN',
		PAYMENT_CODE: 'PAYMENT_CODE',
		MINI_PROGRAM_ORDER_PAYMENT: 'MINI_PROGRAM_ORDER_PAYMENT',
		PAY_QRCODE: 'PAY_QRCODE',
		CREDIT_BALANCE: 'CREDIT_BALANCE',
		LIQUID_QR: 'LIQUID_QR'
	},
	SERVICE_METHOD: {
		DEPOSIT: 'DEPOSIT',
		WITHDRAW: 'WITHDRAW',
		TRANSFER_BANK: 'TRANSFER_BANK',
		TRANSFER_WALLET: 'TRANSFER_WALLET',
		RECEIVE_MONEY: 'RECEIVE_MONEY',
		PAYMENT: 'PAYMENT',
		REFUND_MONEY: 'REFUND_MONEY'
	},
	DISPLAY_ICON: {
		DEPOSIT: 'icon_gd_naptien', // icon gd nạp tiền
		RECEIVE_MONEY: 'wallet_act_nhan', // icon wallet nhận tiền
		TRANSFER_MONEY: 'iconChuyenTien', // icon chuyển tiền
		CALL_PAYMENT: 'icon_goiThanhToan', // icon gọi màn hình thanh toán
		ISEC: 'icon_gd_theisec', // icon Gd thẻ iSec
		CREATE: 'icon_tao', // icon tạo giao dịch
		WITHDRAW: 'icon_gd_3_ruttien', // icon rut tiền
		BUY_CARD: 'icon_gd_tt_mua_mathe', // icon mua mã thẻ
		GAME_CARD: 'icon_game_card', // icon mua mã thẻ game
		TOPUP_PHONE: 'icon_gd_tt_napdt', // icon giao dịch nạp điện thoại
		QRCODE_USED: 'iconIsecQrcodeUsed', // icon Qr code đã dùng
		BONUS: 'icon_gd_5_bonus', // icon bonus
		BILL: 'icon_gd_2_tt_bill', // icon thanh toán hóa đơn
		EKYC: 'eKyc', // icon eKYC
		SYSTEM: 'eKyc', // icon SYSTEM
		ADVANCE_MONEY: 'iconAdvanceMoney', // Ung tien
		STATEMENT: 'iconCreditSettlement', // Thanh toan, tat toan tin dung
		ADVANCE_SALARY: 'iconUngLuong', // Ứng lương
		FAST_LOAN: 'iconVayNhanh',
		PAYMENT_SUCCEEDED: 'eftSuccess',
		PAYMENT_FAILED: 'eftFailed',
		PAYMENT_PENDING: 'eftPending',
		PAYMENT_PROCESSING: 'eftPending',
		OPEN_EWALLET_PAYMENT: 'icon_pay_store',
		CREDIT_STATEMENT: 'iconCreditSettlement',
		ADVANCE_MONEY_CREDIT_BALANCE: 'iconUngTien',
		CREDIT_BALANCE_APPROVED: 'icon_dkytindung_thanhcong',
		CREDIT_BALANCE_REJECTED: 'icon_dkyvitindung_thatbai',
		CREDIT_STATEMENT_CREDIT_BALANCE: 'iconThanhToanDuNo',
		CREDIT_BALANCE_LATE_FEE: 'iconPhiTreHan',
		CREDIT_BALANCE_SERVICE_FEE: 'iconPhiDichVu'
	},

	DISPLAY_ICON_BILL: {
		BILL_PPMB: 'icon_ppmb', // điện thoại trả sau
		BILL_POWE: 'icon_powe', // điện
		BILL_WATE: 'icon_wate', // nước
		BILL_GAME_CARD: 'icon_game_card', // thẻ game
		BILL_ATIC: 'icon_atic', // ve máy bay
		BILL_TIVI: 'icon_tivi', // truyền hình
		BILL_ADSL: 'icon_adsl', // internet
		BILL_FTEL: 'icon_ftel', // điện thoại cố định
		BILL_TUITION: 'icon_tuition' // học phí
	},

	ITEM_METHOD: {
		PAYME: 'Ví PayME',
		PAYME_LINK: 'PayME Link',
		LINKED: 'Tài khoản liên kết',
		ATM: 'Thẻ ATM nội địa',
		ACCOUNT: 'Tài khoản ngân hàng',
		PAYME_CREDIT: 'PayME Credit',
		TRANSFER_BANK: 'Chuyển khoản',
		QR_PAY: 'QR_PAY',
		CREDIT_CARD: 'Thẻ Visa/MasterCard/JCB',
		CREDIT_BALANCE: 'Ví tín dụng'
	},
	ITEM_SERVICE: {
		TOPUP: 'Nạp tiền điện thoại',
		CARD: 'Mua mã thẻ điện thoại',
		CARD_GAME: 'Mua mã game',
		CANCEL_ISEC: 'Hoàn tiền iSec',
		ISEC: 'Phát hành iSec',
		RECEIVE_MONEY: 'Nhận tiền',
		TRANSFER_MONEY: 'Chuyển tiền',
		LINK_TRANSFER_MONEY: 'Chuyển tiền',
		DEPOSIT: 'Nạp tiền',
		DEPOSIT_TRANSFER_BANK: 'Nạp tiền bằng chuyển khoản',
		WITHDRAW: 'Rút tiền',
		PAYMENT: 'Thanh toán Dịch vụ',
		REFUND: 'Hoàn tiền giao dịch',
		CREDIT: 'Thanh toán ví tín dụng',
		ADVANCE_MONEY: 'Ứng tiền ví tín dụng'
	}
};
