module.exports = {
	SERVICE_CODE: 'TRANSFER_MONEY',
	RESOLVE_TYPE: 'TransferMoneyObject',
	STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		CANCELED: 'CANCELED',
		FAILED: 'FAILED'
	},
	TYPE: {
		SEND: 'SEND',
		RECEIVE: 'RECEIVE'
	},
	SUPPLIER: {
		WALLET: 'WALLET',
		NAPAS_GATEWAY: 'NAPAS_GATEWAY'
	},
	PG_SERVICE: {
		BASIC: {
			type: 'TRANSFER_MONEY',
			description: 'Nhận tiền từ ví của {}',
			RESOLVE_TYPE: 'TransferMoneyObject',
			notifyTitle: 'Nhận tiền từ {senderName}',
			notifyMessage: 'Bạn vừa nhận {formatedAmount}đ từ {senderName}',
			TAGS: []
		},
		SALARY: {
			type: 'SALARY',
			description: 'Nhận tiền từ {}',
			RESOLVE_TYPE: 'TransferMoneyObject',
			notifyTitle: 'Nhận tiền từ {senderName}',
			notifyMessage: '{note}',
			TAGS: ['SALARY']
		},
		ADVANCE_SALARY: {
			type: 'ADVANCE_SALARY',
			description: 'Nhận tiền từ {}',
			RESOLVE_TYPE: 'AdvanceSalaryObject',
			notifyTitle: 'Nhận tiền từ CTY CỔ PHẦN CÔNG NGHỆ PAYME',
			notifyMessage: 'Bạn vừa tạm ứng lương thành công {formatedAmount}đ từ {senderName}',
			TAGS: ['ADVANCE_SALARY']
		}
	}
};
