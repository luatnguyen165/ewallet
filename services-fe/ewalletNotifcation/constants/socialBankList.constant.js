module.exports = [
	{
		code: '970433',
		swiftCode: 'VNTTVNVX',
		title: 'VietBank',
		fullbankName: 'Ngân hàng Việt Nam Thương Tín (VietBank)'
	},
	{
		code: '422589',
		swiftCode: 'CIBBVNVN',
		title: 'CIMB Bank',
		fullbankName: 'Ngân hàng TNHH MTV CIMB Việt Nam'
	},
	{
		code: '970405',
		swiftCode: 'VBAAVNVX',
		title: 'Agribank',
		bankName: 'Ngân hàng Nông nghiệp và Phát triển Nông thôn Việt Nam (Agribank)'
	},
	{
		code: '970406',
		swiftCode: 'EACBVNVX',
		title: 'DongA Bank',
		bankName: 'Ngân hàng TMCP Đông Á (DongA Bank)'
	},
	{
		code: '970415',
		swiftCode: 'ICBVVNVX',
		title: 'VietinBank',
		bankName: 'Ngân hàng TMCP Công Thương Việt Nam (VietinBank)'
	},
	{
		code: '970427',
		swiftCode: 'VNACVNVX',
		title: 'VietABank',
		bankName: 'Ngân hàng TMCP Việt Á (VietABank)'
	},
	{
		code: '970437',
		swiftCode: 'HDBCVNVX',
		title: 'HDBank',
		bankName: 'Ngân hàng TMCP Phát triển TP Hồ Chí Minh (HDBank)'
	},
	{
		code: '970443',
		swiftCode: 'SHBAVNVX',
		title: 'SHB',
		bankName: 'Ngân hàng TMCP Sài Gòn - Hà Nội (SHB)'
	},
	{
		code: '970418',
		swiftCode: 'BIDVVNVX',
		title: 'BIDV',
		bankName: 'Ngân hàng TMCP Đầu Tư và Phát Triển Việt Nam (BIDV)'
	},
	{
		code: '970422',
		swiftCode: 'MSCBVNVX',
		title: 'MB Bank',
		bankName: 'Ngân hàng TMCP Quân Đội'
	},
	{
		code: '970431',
		swiftCode: 'EBVIVNVX',
		title: 'Eximbank',
		bankName: 'Ngân hàng TMCP Xuất nhập khẩu Việt Nam (Eximbank)'
	},
	{
		code: '970456',
		swiftCode: ' IBKOVNVX',
		title: 'IBK',
		bankName: 'Ngân Hàng Industrial Bank Of Korea (IBK - chi nhánh HCM)'
	},
	{
		code: '970419',
		swiftCode: 'NVBAVNVX',
		title: 'NCB',
		bankName: 'Ngân hàng TMCP Quốc Dân (NCB)'
	},
	{
		code: '970421',
		swiftCode: 'VRBAVNVX',
		title: 'VRB',
		bankName: 'Ngân hàng Liên Doanh Việt Nga'
	},
	{
		code: '970432',
		swiftCode: 'VPBKVNVX',
		title: 'VPBank',
		bankName: 'Ngân hàng TMCP Việt Nam Thịnh Vượng (VPBank)'
	},
	{
		code: '970449',
		swiftCode: 'LVBKVNVX',
		title: 'LienVietPostBank',
		bankName: 'Ngân hàng TMCP Bưu Điện Liên Việt'
	},
	{
		code: '970458',
		swiftCode: 'UOVBVNVX',
		title: 'UOB',
		bankName: 'Ngân hàng TNHH MTV United Overseas (UOB)'
	},
	{
		code: '970424',
		swiftCode: 'SHBKVNVX',
		title: 'Shinhan',
		bankName: 'Ngân hàng TNHH MTV Shinhan Việt Nam'
	},
	{
		code: '970434',
		swiftCode: 'IABBVNVX',
		title: 'IVB',
		bankName: 'Ngân hàng TNHH Indovina (IVB)'
	},
	{
		code: '970439',
		swiftCode: 'VIDPVNV5',
		title: 'PBVN',
		bankName: 'Ngân hàng TNHH MTV Public Việt Nam'
	},
	{
		code: '970440',
		swiftCode: 'SEAVVNVX',
		title: 'SeABank',
		bankName: 'Ngân hàng TMCP Đông Nam Á (SeABank)'
	},
	{
		code: '970428',
		swiftCode: 'NAMAVNVX',
		title: 'Nam A Bank',
		bankName: 'Ngân hàng TMCP Nam Á (Nam A Bank)'
	},
	{
		code: '970408',
		swiftCode: 'GBNKVNVX',
		title: 'GPBank',
		bankName: 'Ngân hàng TMCP Dầu Khí Toàn Cầu (GPBank)'
	},
	{
		code: '970409',
		swiftCode: 'NASCVNVX',
		title: 'BAC A BANK',
		bankName: 'Ngân hàng TMCP Bắc Á'
	},
	{
		code: '970426',
		swiftCode: 'MCOBVNVX',
		title: 'Maritime Bank',
		bankName: 'Ngân hàng TMCP Hàng Hải (Maritime Bank)'
	},
	{
		code: '970441',
		swiftCode: 'VNIBVNVX',
		title: 'VIB',
		bankName: 'Ngân hàng TMCP Quốc tế Việt Nam (VIB)'
	},
	{
		code: '970452',
		swiftCode: 'KLBKVNVX',
		title: 'Kienlongbank',
		bankName: 'Ngân hàng TMCP Kiên Long (Kienlongbank)'
	},
	{
		code: '970457',
		swiftCode: 'HVBKVNVX',
		title: 'Wooribank',
		bankName: 'Ngân hàng Wooribank'
	},
	{
		code: '970414',
		swiftCode: 'OJBAVNVX',
		title: 'OceanBank',
		bankName: 'Ngân hàng TMCP Đại Dương (OceanBank)'
	},
	{
		code: '970425',
		swiftCode: 'ABBKVNVX',
		title: 'ABBANK',
		bankName: 'Ngân hàng TMCP An Bình (ABBANK)'
	},
	{
		code: '970436',
		swiftCode: 'BFTVVNVX',
		title: 'VietcomBank',
		bankName: 'Ngân hàng TMCP Ngoại Thương Việt Nam (VietcomBank)'
	},
	{
		code: '970412',
		swiftCode: 'WBVNVNVX',
		title: 'PVcomBank',
		bankName: 'Ngân hàng TMCP Đại chúng Việt Nam (PVcomBank)'
	},
	{
		code: '970416',
		swiftCode: 'ASCBVNVX',
		title: 'ACB',
		bankName: 'Ngân hàng TMCP Á Châu'
	},
	{
		code: '970403',
		swiftCode: 'SGTTVNVX',
		title: 'Sacombank',
		bankName: 'Ngân hàng TMCP Sài Gòn Thương Tín (Sacombank)'
	},
	{
		code: '970430',
		swiftCode: 'PGBLVNVX',
		title: 'PG Bank',
		bankName: 'Ngân hàng TMCP Xăng dầu Petrolimex (PG Bank)'
	},
	{
		code: '970438',
		swiftCode: 'BVBVVNVX',
		title: 'Bao Viet Bank',
		bankName: 'Ngân hàng TMCP Bảo Việt (Bao Viet Bank)'
	},
	{
		code: '970423',
		swiftCode: 'TPBVVNVX',
		title: 'TPBank',
		bankName: 'Ngân hàng TMCP Tiên Phong (TPBank)'
	},
	{
		code: '970455',
		swiftCode: ' IBKOVNVXHAN',
		title: 'IBK',
		bankName: 'Ngân Hàng Industrial Bank Of Korea (IBK - chi nhánh Hà Nội)'
	},
	{
		code: '970429',
		swiftCode: 'SACLVNVX',
		title: 'SCB',
		bankName: 'Ngân hàng TMCP Sài Gòn (SCB)'
	},
	{
		code: '970446',
		swiftCode: '970446',
		title: 'Co-op Bank',
		bankName: 'Ngân Hàng Hợp Tác Xã Việt Nam (Co-op Bank)'
	},
	{
		code: '970454',
		swiftCode: 'VCBCVNVX',
		title: 'Vietcapital Bank',
		bankName: 'Ngân hàng TMCP Bản Việt (Vietcapital Bank)'
	},
	{
		code: '970400',
		swiftCode: 'SBITVNVX',
		title: 'Saigonbank',
		bankName: 'Ngân hàng TMCP Sài Gòn Công thương (Saigonbank)'
	},
	{
		code: '970407',
		swiftCode: 'VTCBVNVX',
		title: 'Techcombank',
		bankName: 'Ngân hàng TMCP Kỹ Thương Việt Nam (Techcombank)'
	},
	{
		code: '970442',
		swiftCode: 'HLBBVNVX',
		title: 'Hong Leong Bank',
		bankName: 'Ngân hàng TNHH MTV Hongleong Việt Nam (Hong Leong Bank)'
	},
	{
		code: '970402',
		swiftCode: 'SMLIB',
		title: 'Hong Leong Bank',
		bankName: 'Ngân hàng TNHH MTV Hongleong Việt Nam (Hong Leong Bank)'
	}
];
