module.exports = {
	EVENT: {
		WALLET: 'WALLET',
		TIP_RECEIVED: 'TIP_RECEIVED',
		PAYME_CREDIT: 'PAYME_CREDIT',
		NOTIFICATION: 'NOTIFICATION',
		ADVANCE_SALARY: 'ADVANCE_SALARY',
		SOCIAL_PAYMENT: 'SOCIAL_PAYMENT',
		FAST_LOAN: 'FAST_LOAN',
		CREDIT_CARD: 'CREDIT_CARD',
		PAYMENT_CODE: 'PAYMENT_CODE',
		BANK_CARD: 'BANK_CARD',
		LIQUID_QR: 'LIQUID_QR',
		OPEN_EWALLET_PAYMENT: 'OPEN_EWALLET_PAYMENT'
	},
	RESOLVE_TYPE: {
		WALLET: {
			WALLET_BALANCE_CHANGED: 'WalletBalanceChangedSubscription',
			WALLET_WITHDRAW: 'WalletWithDrawSubscription',
			WALLET_DEPOSIT: 'WalletDepositSubscription'
		},
		PAYME_CREDIT: {
			PAYME_CREDIT_BALANCE_CHANGED: 'PaymeCreditBalanceChangedSubscription',
			PAYME_CREDIT_REGISTER: 'PaymeCreditRegisterSubscription'
		},
		NOTIFICATION: {
			NOTIFICATION_CREATE: 'NotificationCreateSubscription'
		},
		ADVANCE_SALARY: {
			ADVANCE_SALARY_STATE: 'AdvanceSalaryStateSubscription'
		},
		SOCIAL_PAYMENT: {
			SOCIAL_PAYMENT_PAY: 'SocialPaymentPaySubscription'
		},
		FAST_LOAN: {
			FAST_LOAN_STATE: 'FastLoanStateSubscription'
		},
		PAYMENT_CODE: {
			PAYMENT_CODE_STATE: 'PaymentCodeStateSubscription'
		},
		CREDIT_CARD: {
			LINKED: 'LinkedStatusSubscription',
			PAYMENT: 'PaymentStatusSubscription'
		},
		BANK_CARD: {
			LINKED: 'LinkedBankCardSubscription',
			PAYMENT: 'PaymentStatusSubscription'
		},
		LIQUID_QR: {
			LIQUID_QR_STATE: 'LiquidQRStateSubscription'
		},
		OPEN_EWALLET_PAYMENT: {
			PAYMENT: 'OpenEWalletPaymentStateSubscription'
		}
	}
};
