module.exports = {
	ACTIVE_CODE_SOURCE: 'CREDIT_BALANCE',
	PACKAGE_STATE: {
		ACTIVE: 'ACTIVE',
		INACTIVE: 'INACTIVE'
	},
	STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED',
		CANCELLED: 'CANCELLED'
	},
	TRANSACTION_TYPE: {
		PAY: 'PAY',
		WITHDRAW: 'WITHDRAW',
		SETTLEMENT: 'SETTLEMENT',
		PAY_CREDIT: 'PAY_CREDIT',
		PURCHASE: 'PURCHASE'
	},
	REGISTER_STATE: {
		PENDING: 'PENDING',
		REQUESTING: 'REQUESTING',
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED',
		USED: 'USED',
		APPROVED: 'APPROVED',
		REJECTED: 'REJECTED',
		WAITING: 'WAITING',
		LOCKED: 'LOCKED',
		AUTO_REJECTED: 'AUTO_REJECTED'
	},
	HISTORY_STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED'
	},
	TYPE: {
		PAY: 'PAY',
		UPDATE_CREDIT_LIMIT: 'UPDATE_CREDIT_LIMIT',
		WITHDRAW: 'WITHDRAW',
		SETTLEMENT: 'SETTLEMENT',
		PAY_CREDIT: 'PAY_CREDIT',
		ADVANCE_MONEY: 'ADVANCE_MONEY',
		REFUND: 'REFUND',
		SERVICE_FEE: 'SERVICE_FEE',
		LATE_FEE: 'LATE_FEE'
	},
	REQUEST_INFO: {
		PARENTS: 'Parents',
		MARRIAGE: 'Husband/Wife',
		CHILDREN: 'Children',
		SIBLINGS: 'Brother/Sister',
		CLOSE_RELATIVE: 'Close relative',
		COLLEAGUE: 'Colleague',
		FRIEND: 'Friend',
		NEIGHBOR: 'Neighbor'
	},

	VN_PHONE_PREFIX: {
		VIETTEL: ['086', '096', '097', '098', '032', '033', '034', '035', '036', '037', '038', '039'],
		MOBIFONE: ['089', '090', '093', '070', '079', '077', '076', '078'],
		VINAPHONE: ['088', '091', '094', '083', '084', '085', '081', '082'],
		VIETNAMOBILE: ['092', '056', '058', '052'],
		GMOBILE: ['099', '059'],
		SFone: ['095'],
		ITELECOM: ['087']
	},
	SUPPLIER: {
		AIZEN_CREDIT: 'AIZEN_CREDIT'
	},
	PATH: {
		PAY_CREDIT: '/paymeCredit/payCredit',
		SEND_OTP: '/paymeCredit/sendOTP',
		VERIFY_OTP: '/paymeCredit/verifyOTP',
		REGISTER: '/paymeCredit/register',
		GET_FEE: '/paymeCredit/getFee'
	},
	ICON: {
		PAY: 'icon_pay_store',
		SETTLEMENT: '',
		REFUND: 'icon_gd_5_bonus',
		ADVANCE_MONEY: 'iconUngTien',
		PAY_CREDIT: 'iconThanhToanDuNo',
		LATE_FEE: 'iconPhiTreHan',
		SERVICE_FEE: 'iconPhiDichVu'
	}
};
