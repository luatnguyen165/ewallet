const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	appId: {
		type: Number,
		default: null
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	fee: {
		type: Number
	},
	total: {
		type: Number
	},
	service: {
		code: {
			type: String,
			default: null
		},
		type: {
			type: String,
			default: null
		},
		transaction: {
			type: String,
			required: true
		}
	},
	method: {
		type: String,
		default: null
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.BANK_STATE),
		default: GeneralConstant.BANK_STATE.PENDING
	},
	description: {
		type: String,
		default: null
	},
	refundData: {
		method: String,
		state: {
			type: String,
			enum: _.values(GeneralConstant.BANK_STATE)
		},
		reason: String, // ly do huy
		createdAt: {
			type: Date
		}
	},
	transaction: {
		type: String,
		default: null
	},
	supplierTransaction: {
		type: String,
		default: null
	},
	reason: {
		type: String,
		default: null
	},
	supplierResponsed: [String],
	ipnResponsed: [String],
	methodData: Object
}, {
	collection: 'Payment2',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ transactionId: 1 });
Schema.index({ state: 1 });
Schema.index({ method: 1 });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);