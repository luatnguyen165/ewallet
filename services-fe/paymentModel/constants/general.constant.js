module.exports = {
	LINKED_TYPE: {
		ATM: 'ATM',
		BANKING: 'BANKING',
		CREDIT_CARD: 'CREDIT_CARD',
		BANKING_OCB: 'BANKING_OCB',
		BANKING_VIETIN: 'BANKING_VIETIN'
	},
	LINKED_STATE: {
		NEW: 'NEW',
		LINKED: 'LINKED',
		LOCKED: 'LOCKED',
		UNLINK: 'UNLINK',
		FAILED: 'FAILED'
	},
	BANK_STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED',
		CANCELED: 'CANCELED',
		APPROVED: 'APPROVED',
		REJECTED: 'REJECTED'
	},
	PAYMENT_STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED',
		CANCELED: 'CANCELED'
	}
};
