const _ = require('lodash');

module.exports = {
	name: 'eWalletCheckQuota',
	version: 1,
	/**
	 * Settings
	 */
	settings: {

	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {

		checkNewKycOverLimit: {
			params: {
				accountId: 'number',
				amount: 'number'
			},
			handler: require('./actions/checkNewKycOverLimit.action') 
		},
		checkDepositOverLimit: {
			params: {
				accountId: 'number'
			},
			handler: require('./actions/checkDepositOverLimit.action')
		},
		checkWithdrawOverLimit: {
			params: {
				accountId: 'number'
			},
			handler: require('./actions/checkWithdrawOverLimit.action')
		}
        
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
