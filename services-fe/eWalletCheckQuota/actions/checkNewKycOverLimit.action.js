const Numeral = require('numeral');
const _ = require('lodash');
const MomentTimezone = require('moment-timezone');

const GeneralConstant = require('../methods/general.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;

	const payload = _.get(ctx, 'params', {});

	try {
		const accountInfo = await Broker.call('v1.eWalletAccountModel.findOne', [{
			id: payload.accountId
		}, '-_id kyc.kycId']);

		if (_.get(accountInfo, 'kyc.kycId', null) === null) {
			return {
				succeeded: false,
				message: 'Tài khoản chưa định danh. Vui lòng thực hiện định danh.'
			};
		}

		const kycInfo = await Broker.call('v1.eWalletKycModel.findOne', [{
			accountId: payload.accountId
		}, '-_id approvedAt']);
		if (kycInfo && kycInfo.approvedAt) {
			const createDate = new Date(kycInfo.approvedAt);
			const after24Hour = MomentTimezone(createDate).add('1', 'day');
			const now = new Date();
			if (after24Hour.isBefore(now) === false) {
				const condition = {
					accountId: payload.accountId,
					state: GeneralConstant.PAYMENT_STATE.SUCCEEDED,
					'service.code': { $ne: GeneralConstant.SERVICE_TYPE.DEPOSIT }
				};
				const report = await Broker.call('v1.eWalletHistoryModel.aggregate', [[
					{ $match: condition },
					{
						$group: {
							_id: null,
							total: { $sum: '$amount' },
							count: { $sum: 1 }
						}
					},
					{
						$project: {
							_id: 0,
							total: '$total',
							count: '$count'
						}
					}
				]]);

				if (!_.isEmpty(report)) {
					const userHistoryData = report[0];
					userHistoryData.total += payload.amount;
					if (userHistoryData.total > GeneralConstant.USER_TRANSACTION_MONEY_LIMIT.NEW_ACOUNT_LIMIT) {
						const monitorData = {
							rule: `NEW_USER_TRANSACTION_OVER_LIMIT_${GeneralConstant.USER_TRANSACTION_MONEY_LIMIT.NEW_ACOUNT_LIMIT}`,
							tags: [`#${payload.accountId}`],
							source: 'API',
							type: 'WALLET',
							message: `Tài khoản ${payload.accountId} GD ${Numeral(GeneralConstant.USER_TRANSACTION_MONEY_LIMIT.NEW_ACOUNT_LIMIT).format('0,0')} VNĐ trong vòng 24h đầu tiên sau khi định danh.`
						};

						await Broker.call('v1.eWalletMonitorModel.create', [monitorData]);
						return {
							succeeded: false,
							message: 'Tài khoản vừa định danh trong vòng 24h không được giao dịch quá 5 triệu đồng'
						};
					}
				}

				return {
					succeeded: true
				};
			}
		}
	} catch (e) {
		Logger('[QUOTA] -> [NEW_KYC_LIMIT] -> [EXCEPTION] :: -> ', e);
		return {
			succeeded: false
		};
	}

	return {
		succeeded: true
	};
};