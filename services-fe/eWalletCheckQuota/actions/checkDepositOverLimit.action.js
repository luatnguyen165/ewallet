const moment = require('moment');
const MomentTimezone = require('moment-timezone');
const Numeral = require('numeral');
const _ = require('lodash');

const GeneralConstant = require('../methods/general.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;

	try {
		const accountId = _.get(ctx.params, 'accountId', null);

		const accountInfo = await Broker.call('v1.eWalletAccountModel.findOne', [{ id: accountId }, '-_id id']);
		if (accountInfo) {
			const createDate = new Date();
			const startOfDay = MomentTimezone(createDate).startOf('day').toISOString();
			const endOfDay = MomentTimezone(createDate).endOf('day').toISOString();
			let condition = {
				accountId,
				'service.type': GeneralConstant.SERVICE_TYPE.DEPOSIT,
				changed: '+',
				state: GeneralConstant.PAYMENT_STATE.SUCCEEDED
			};

			const where = {
				$expr: {
					$and: [
						{
							$gte: [
								'$createdAt',
								{
									$dateFromString: {
										dateString: startOfDay
									}
								}
							]
						},
						{
							$lte: [
								'$createdAt',
								{
									$dateFromString: {
										dateString: endOfDay
									}
								}
							]
						}
					]
				}
			};

			condition = { ...condition, ...where };
			const report = await Broker.call('v1.eWalletHistoryModel.aggregate', [[
				{ $match: condition },
				{
					$group: {
						_id: null,
						total: { $sum: '$amount' },
						count: { $sum: 1 }
					}
				},
				{
					$project: {
						_id: 0,
						total: '$total',
						count: '$count'
					}
				}
			]]);
			if (!_.isEmpty(report)) {
				const userHistoryData = report[0];
				// Check AMOUNT
				let priority = '';
				if (userHistoryData.total >= GeneralConstant.USER_TRANSACTION_MONEY_LIMIT.DAY_DEPOSIT_LOW_LIMIT) priority = 'DAY_DEPOSIT_LOW_LIMIT';
				if (userHistoryData.total >= GeneralConstant.USER_TRANSACTION_MONEY_LIMIT.DAY_DEPOSIT_MEDIUM_LIMIT) priority = 'DAY_DEPOSIT_MEDIUM_LIMIT';
				if (userHistoryData.total >= GeneralConstant.USER_TRANSACTION_MONEY_LIMIT.DAY_DEPOSIT_HIGH_LIMIT) priority = 'DAY_DEPOSIT_HIGH_LIMIT';

				if (priority) {
					const monitorData = {
						rule: `DAILY_DEPOSIT_OVER_LIMIT_${GeneralConstant.USER_TRANSACTION_MONEY_LIMIT[priority] / 1000000}M`,
						tags: [`#${accountId}`],
						source: 'API',
						type: 'WALLET',
						message: `Tài khoản ${accountId} có số tiền nạp vượt quá ${Numeral(GeneralConstant.USER_TRANSACTION_MONEY_LIMIT[priority] / 1000000).format('0,0')}M VNĐ trong ngày`
					};
					await Broker.call('v1.eWalletMonitorModel.create', [monitorData]);
				}

				// Check COUNT
				let times = '';
				if (userHistoryData.count >= GeneralConstant.USER_TRANSACTION_MONEY_LIMIT.DAY_DEPOSIT_LOW_COUNT_LIMIT) times = 'DAY_DEPOSIT_LOW_COUNT_LIMIT';
				if (userHistoryData.count >= GeneralConstant.USER_TRANSACTION_MONEY_LIMIT.DAY_DEPOSIT_MEDIUM_COUNT_LIMIT) times = 'DAY_DEPOSIT_MEDIUM_COUNT_LIMIT';
				if (userHistoryData.count >= GeneralConstant.USER_TRANSACTION_MONEY_LIMIT.DAY_DEPOSIT_HIGH_COUNT_LIMIT) times = 'DAY_DEPOSIT_HIGH_COUNT_LIMIT';

				if (times) {
					const monitorData = {
						rule: `DAILY_DEPOSIT_OVER_COUNT_LIMIT_${GeneralConstant.USER_TRANSACTION_MONEY_LIMIT[times]}TIMES`,
						tags: [`#${accountId}`],
						source: 'API',
						type: 'WALLET',
						message: `Tài khoản ${accountId} nạp vượt quá ${GeneralConstant.USER_TRANSACTION_MONEY_LIMIT[times]} lần trong ngày`
					};
					await Broker.call('v1.eWalletMonitorModel.create', [monitorData]);
				}
			}
		}
	} catch (e) {
		Logger(`[QUOTA] -> [DEPOSIT] -> [CHECK OVER LIMIT] -> [EXCEPTION] -> :: , ${e}`);
		return false;
	}
	return true;
};
