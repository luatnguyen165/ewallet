const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	SSCId: { // id học sinh
		type: String,
		required: true
	},
	transaction: {
		type: String,
		required: true
	},
	amount: {
		type: Number,
		required: true
	},
	total: {
		type: Number,
		default: null
	},
	fee: {
		type: Number,
		default: 0
	},
	description: {
		type: String,
		default: null
	},

	billInfos: Object,
	paymentPeriod: {
		type: Array,
		default: []
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.BILL_TRANSACTION_STATE),
		default: GeneralConstant.BILL_TRANSACTION_STATE.PENDING
	},
	studentName: {
		type: String,
		default: null
	},
	address: {
		type: String,
		default: null
	},
	schoolCode: {
		type: String,
		default: null
	},
	schoolName: {
		type: String,
		default: null
	},
	classCode: {
		type: String,
		default: null
	},
	className: {
		type: String,
		default: null
	},
	customerId: {
		type: Number,
		default: null
	},
	supplierResponsed: Object,
	extraData: Object,
	errorCode: {
		type: String,
		default: null
	}
}, {
	collection: 'Service_Tuition',
	versionKey: false,
	timestamps: true
});

Schema.index({ SSCId: 1 }, { sparse: false, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
