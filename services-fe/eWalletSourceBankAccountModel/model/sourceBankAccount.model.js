const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const BankServiceConstant = require('../constants/bankService.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountNumber: {
		type: String,
		default: null
	},
	cardNumber: {
		type: String,
		default: null
	},
	swiftCode: {
		type: String,
		required: true
	},
	sourceSupplier: {
		type: String,
		enum: _.values(BankServiceConstant.SUPPLIER),
		required: true
	},
	isActive: {
		type: Boolean,
		default: true
	}
}, {
	collection: 'SourceBankAccount',
	versionKey: false,
	timestamps: true
});

/*
  | ==========================================================
  | Plugins
  | ==========================================================
  */

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
  | ==========================================================
  | Methods
  | ==========================================================
  */

/*
  | ==========================================================
  | HOOKS
  | ==========================================================
  */

module.exports = mongoose.model(Schema.options.collection, Schema);
