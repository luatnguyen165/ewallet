const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	appId: {
		type: Number,
		default: 1
	},
	supplier: {
		type: String,
		enum: _.values(GeneralConstant.SUPPLIER),
		default: null
	},
	partnerTransaction: {
		type: String
	},
	transaction: {
		type: String,
		required: true
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	fee: {
		type: Number,
		required: true,
		default: 0
	},
	total: {
		type: Number,
		required: true,
		default: 0
	},
	note: {
		type: String,
		default: null
	},
	description: {
		type: String,
		default: null
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.STATE),
		default: GeneralConstant.STATE.PENDING
	},
	senderInfo: {
		accountId: { type: Number },
		fullname: { type: String }
	},
	receiverInfo: {
		accountId: { type: Number },
		fullname: { type: String },
		cardNumber: { type: String },
		accountNumber: { type: String },
		swiftCode: { type: String }
	},
	refundData: {
		method: String,
		state: {
			type: String,
			enum: _.values(GeneralConstant.REFUND_STATE)
		},
		reason: String, // ly do huy
		createdAt: {
			type: Date
		}
	},
	payment: {
		id: { type: Number },
		transaction: { type: String },
		method: { type: String },
		state: { type: String },
		description: { type: String }
	},
	transport: {
		accountId: { type: Number },
		transportId: { type: Number },
		transaction: { type: String },
		state: { type: String },
		method: { type: String },
		description: { type: String }
	},
	supplierResponse: []
}, {
	collection: 'Service_TransferMoney',
	versionKey: false,
	timestamps: true
});

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
