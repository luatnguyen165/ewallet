const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	transactionId: {
		type: String,
		required: true
	},
	paymentId: {
		type: Number
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.STATE)
	},
	description: {
		type: String,
		default: null
	},
	balance: {
		before: {
			cash: {
				type: Number
			},
			credit: Number
		},
		after: {
			cash: {
				type: Number
			},
			credit: Number
		}
	}
}, {
	collection: 'Payment_Payme',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ state: 1 });
/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
