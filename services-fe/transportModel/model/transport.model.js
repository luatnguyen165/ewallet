const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	appId: {
		type: Number,
		default: 1
	},
	transaction: {
		type: String,
		required: true
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	service: {
		code: {
			type: String,
			default: null
		},
		type: {
			type: String,
			default: null
		},
		transaction: {
			type: String,
			required: true
		}
	},
	method: { // transport to ??
		type: String,
		enum: _.values(GeneralConstant.METHOD)
	},
	methodData: Object,
	state: {
		type: String,
		enum: _.values(GeneralConstant.STATE),
		default: GeneralConstant.STATE.PENDING
	},
	description: {
		type: String,
		default: null
	},
	note: {
		type: String,
		default: null
	},
	refundData: {
		method: String,
		state: {
			type: String,
			enum: _.values(GeneralConstant.REFUND_STATE)
		},
		reason: String, // ly do huy
		createdAt: {
			type: Date
		}
	}
}, {
	collection: 'Transport',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ appId: 1 }, { sparse: true, unique: false });
Schema.index({ transaction: 1 }, { sparse: false, unique: true });
Schema.index({ method: 1 }, { sparse: false, unique: false });
Schema.index({ 'service.code': 1, 'service.transaction': 1 }, { sparse: false, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
