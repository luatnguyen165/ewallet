const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	appId: {
		type: Number,
		default: null
	},
	transaction: {
		type: String,
		required: true
	},
	supplierTransaction: {
		type: String
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	fee: {
		type: Number,
		required: true,
		default: 0
	},
	total: {
		type: Number,
		required: true,
		default: 0
	},
	method: {
		gateway: {
			type: String,
			default: null
		},
		id: {
			type: Number,
			default: null
		},
		refer: {
			type: Number,
			default: null
		},
		description: {
			type: String,
			default: null
		},
		group: {
			type: String,
			default: null
		},
		bankInfo: Object
	},
	supplierResponsed: [String],
	description: {
		type: String,
		default: null
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.BANK_STATE),
		default: GeneralConstant.BANK_STATE.PENDING
	},
	payment: Object,
	extraData: {
		linkedId: { type: Number },
		bankRefCode: { type: String },
		supplierTransaction: { type: String },
		ipnUrl: { type: String },
		merchantId: { type: Number }
	}
}, {
	collection: 'Service_Deposit',
	versionKey: false,
	timestamps: true
});

Schema.index({ supplierTransaction: 1 }, { unique: true, sparse: true });
Schema.index({ accountId: 1 }, { sparse: false, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
