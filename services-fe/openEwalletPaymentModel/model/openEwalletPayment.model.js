const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	id: {
		type: Number,
		required: true,
		unique: true
	},
	accountId: {
		type: Number,
		required: true
	},
	targetAppId: {
		type: Number,
		default: null
	},
	destAppId: {
		type: Number,
		required: true
	},
	storeId: {
		type: Number,
		default: null
	},
	merchantId: {
		type: Number,
		required: true
	},
	orderId: {
		type: String,
		required: true
	},
	referExtraData: {
		type: String
	},
	transaction: {
		type: String,
		required: true
	},
	note: {
		type: String,
		default: null
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	fee: {
		type: Number,
		required: true,
		default: 0
	},
	total: {
		type: Number,
		required: true,
		default: 0
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.STATE),
		default: GeneralConstant.STATE.PENDING
	},
	description: {
		type: String,
		default: null
	},
	extraData: Object,
	payment: {
		id: { type: Number },
		transaction: { type: String },
		method: { type: String },
		state: { type: String },
		description: { type: String }
	},
	userName: {
		type: String,
		default: null
	}
}, {
	collection: 'OpenEWalletPayment',
	versionKey: false,
	timestamps: true
});

Schema.index({ orderId: 1, destAppId: 1 }, { sparse: false, unique: true });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
