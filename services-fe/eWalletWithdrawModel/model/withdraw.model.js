const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	id: {
		type: Number,
		required: true,
		unique: true
	},
	accountId: {
		type: Number,
		required: true
	},
	appId: {
		type: Number,
		default: 1
	},
	transaction: {
		type: String,
		required: true
	},
	description: {
		type: String,
		default: null
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	fee: {
		type: Number,
		required: true,
		default: 0
	},
	total: {
		type: Number,
		required: true,
		default: 0
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.WITHDRAW_STATE),
		default: GeneralConstant.WITHDRAW_STATE.PENDING
	},
	refundData: {
		method: String,
		state: {
			type: String,
			enum: _.values(GeneralConstant.REFUND_STATE)
		},
		reason: String, // ly do huy
		createdAt: {
			type: Date
		}
	},
	payment: {
		id: { type: Number },
		transaction: { type: String },
		method: { type: String },
		state: { type: String },
		description: { type: String }
	},
	transport: {
		accountId: { type: Number },
		transportId: { type: Number },
		transaction: { type: String },
		state: { type: String },
		method: { type: String },
		description: { type: String }
	},
	extraData: {
		linkedId: { type: Number }
	}
}, {
	collection: 'Service_Withdraw',
	versionKey: false,
	timestamps: true
});

Schema.index({ transaction: 1 }, { unique: true, sparse: true });
Schema.index({ accountId: 1 }, { sparse: false, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-idAI`,
	field: 'idAI',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
