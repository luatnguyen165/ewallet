const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	id: {
		type: String
	},
	en: {
		type: String
	},
	vi: {
		type: String
	},
	shortName: {
		type: String
	},
	bankNameTCB: {
		type: String
	},
	swiftCode: {
		type: String,
		required: true
	},
	aliasIdPVComBank: {
		type: Number
	},
	card: {
		atm: {
			prefix: {
				type: String
			},
			length: {
				type: Number
			}
		}
	},
	link: {
		gateway: {
			type: String
		}
	},
	isActive: {
		type: Boolean,
		default: true
	},
	vietQRAccepted: {
		type: Boolean,
		default: false
	},
	requiredDate: {
		type: String
	},
	isVNPay: {
		type: Boolean,
		default: false
	},
	isVietQR: {
		type: Boolean,
		default: false
	}
}, {
	collection: 'BankCode',
	versionKey: false,
	timestamps: true
});

/*
  | ==========================================================
  | Plugins
  | ==========================================================
  */

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
  | ==========================================================
  | Methods
  | ==========================================================
  */

/*
  | ==========================================================
  | HOOKS
  | ==========================================================
  */

module.exports = mongoose.model(Schema.options.collection, Schema);
