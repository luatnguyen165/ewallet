const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');
const Moment = require('moment');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: { // nguoi tao link
		type: Number,
		required: true
	},
	feedId: {
		type: Number,
		default: null
	},
	pageId: {
		type: Number,
		default: null
	},
	isAllowGuest: {
		type: Boolean
	},
	amount: {
		type: Number,
		required: true
	},
	fee: {
		type: Number,
		default: 0
	},
	total: {
		type: Number
	},
	transaction: {
		type: String
	},
	orderTransaction: {
		type: String
	},
	userTransaction: {
		type: String
	},
	ipnTransaction: {
		type: String
	},
	parentToken: {
		type: String
	},
	token: {
		type: String,
		unique: true
	},
	url: {
		type: String
	},
	description: String,
	note: {
		type: String,
		default: null
	},
	reply: String,
	template: {
		type: String,
		enum: [..._.values(GeneralConstant.TEMPLATE), null],
		default: null
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.SOCIAL_LINK_STATE)
	},
	password: {
		type: String,
		default: null
	},
	expiredAt: {
		type: Date,
		default: Moment(new Date()).add(24, 'hours')
	},
	usedAt: {
		type: Date
	},
	type: {
		type: String,
		enum: _.values(GeneralConstant.TYPE),
		required: true
	},
	initLink: {
		senderInfo: { // thong tin nguoi tạo link
			accountId: {
				type: Number,
				default: null
			},
			fullname: String,
			phone: {
				type: String,
				default: null
			},
			campaignId: {
				type: Number,
				default: null
			},
			campaignTitle: {
				type: String,
				default: null
			}
		},
		receiverInfo: { // thong tin nguoi nhận link
			accountId: Number,
			fullname: String,
			phone: String,
			email: String,
			username: String
		}
	},
	transferInfo: {
		senderInfo: { // thong tin nguoi gui tiền
			accountId: {
				type: Number,
				default: null
			},
			fullname: String,
			phone: {
				type: String,
				default: null
			}
		},
		receiverInfo: { // thong tin nguoi nhan tiền
			accountId: {
				type: Number,
				default: null
			},
			fullname: String,
			phone: {
				type: String,
				default: null
			}
		}
	},
	channel: {
		type: String,
		enum: _.values(GeneralConstant.SOCIAL_CHANNEL),
		default: GeneralConstant.SOCIAL_CHANNEL.NULL
	},
	payment: {
		id: { type: Number },
		transaction: { type: String },
		method: { type: String },
		state: { type: String },
		description: { type: String }
	},
	transport: {
		accountId: { type: Number },
		transportId: { type: Number },
		transaction: { type: String },
		state: { type: String },
		method: { type: String },
		description: { type: String }
	},
	theme: Object,
	withdrawMethod: Object, // thông tin PT rút tiền từ link
	paymentMethod: Object, // thong tin PTTT
	extraData: Object // thong tin day du lenh thanh toan da tao ở module payment
}, {
	collection: 'Service_SocialLink',
	versionKey: false,
	timestamps: true
});

Schema.index({ state: 1, accountId: 1, token: 1, transaction: 1, id: 1, type: 1 });
Schema.index({ userTransaction: 1 }, { sparse: true, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
