const mongoose = require('mongoose');
const generalConstant = require('../constants/general.constant');

const Schema = mongoose.Schema({
	accountId: {
		type: Number
	},
	amount: {
		type: Number
	},
	fee: {
		type: Number
	},
	total: {
		type: Number
	},
	count: {
		type: Number
	},
	changed: {
		type: String,
		enum: Object.values(generalConstant.CHANGED)
	},
	key: {
		type: String,
		enum: generalConstant.PAYMENT_TYPE
	},
	isActive: {
		type: Boolean
	},
	logKey: {
		type: String
	},
	type: {
		type: String
	},
	dateTime: {
		type: Date
	}
}, {
	collection: 'LogEwalletByAccount',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ type: 1 }, { sparse: false, unique: false });

module.exports = mongoose.model(Schema.options.collection, Schema);
