module.exports = {
	CHANGED: {
		ADD: '+',
		SUBTRACT: '-'
	},
	PAYMENT_TYPE: [
		// Nạp
		'DEPOSIT',
		'RECEIVE_MONEY',
		'ADD_MONEY',
		// Rút
		'WITHDRAW',
		'MINUS_MONEY',
		/// GD thanh toán, chuyển tiền
		'PAYMENT_ISEC',
		'MOBILE_TOPUP',
		'MOBILE_CARD',
		'BILL',
		'SOCIAL_PAYMENT',
		'OPEN_EWALLET_PAYMENT',
		'WITHDRAW_FAILED_REFUND',
		'PAY_QRCODE',
		'CREDIT_STATEMENT',
		// Hoàn
		'REFUND_MONEY'
	]
};
