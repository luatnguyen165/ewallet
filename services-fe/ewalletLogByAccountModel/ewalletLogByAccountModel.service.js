const DbService = require('moleculer-db');
const MongooseAdapter = require('moleculer-db-adapter-mongoose');
const MongooseAction = require('moleculer-db-adapter-mongoose-action');
const ewalletLogByAccountModel = require('./model/ewalletLogByAccount.model');

module.exports = {
	name: 'ewalletLogByAccountModel',

	version: 1,

	mixins: [DbService],

	adapter: new MongooseAdapter(process.env.MONGO_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		keepAlive: true
	}),

	model: ewalletLogByAccountModel,
	settings: {},
	dependencies: [],
	actions: MongooseAction(),
	events: {},
	methods: {},
	created() {},

	async afterConnected() {
		this.logger.info('Connected successfully...');
	}
};
