const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
// const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	key: {
		type: String,
		required: true
	},
	// type: {
	// 	type: String,
	// 	enum: _.values(GeneralConstant.SETTING_TYPE),
	// 	default: GeneralConstant.SETTING_TYPE.STRING
	// },
	value: {
		type: mongoose.Schema.Types.Mixed,
		default: null
	},
	inherit: [String],
	tags: [String],
	isActive: {
		type: Boolean,
		default: true
	},
	description: {
		type: String,
		default: 'Mô tả'
	}
}, {
	collection: 'Setting',
	versionKey: false,
	timestamps: true
});

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
