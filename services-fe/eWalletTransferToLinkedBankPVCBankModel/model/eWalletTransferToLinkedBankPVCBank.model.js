const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	transactionId: {
		type: String,
		required: true
	},
	transferId: {
		type: Number
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	total: {
		type: Number,
		required: true,
		default: 0
	},
	fee: {
		type: Number,
		required: true,
		default: 0
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.TRANSFER_METHOD_LINKED_PVCBANK_STATE)
	},
	description: {
		type: String,
		default: null
	},
	bankInfo: {
		swiftCode: String,
		bankName: String,
		cardNumber: String,
		cardHolder: String
	},
	supplierResponsed: Object
}, {
	collection: 'Transfer_ToLinkedBankPVCBank',
	versionKey: false,
	timestamps: true
});

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
