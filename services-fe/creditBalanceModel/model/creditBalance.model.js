const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	transaction: { type: String, required: true },
	accountId: { type: Number, required: true },
	amount: { type: Number, default: 0 },
	supplierId: { type: String, required: true },
	change: { type: String, enum: ['+', '-', null] },
	description: { type: String },
	referData: { type: Object }
}, {
	collection: 'CreditBalance',
	versionKey: false,
	timestamps: true
});

Schema.index({ transaction: 1, supplierId: 1 }, { unique: true });
Schema.index({ accountId: 1 });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}_id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
