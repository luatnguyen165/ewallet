const moleculerI18n = require('moleculer-i18n-js');
const path = require('path');
const _ = require('lodash');
const { stringify } = require('querystring');

module.exports = {
	name: 'eWalletSetting',
	version: 1,
	mixins: [moleculerI18n],
	i18n: {
		directory: path.join(__dirname, 'locales'),
		locales: ['vi', 'en'],
		defaultLocale: 'vi'
	},

	/**
	 * Settings
	 */
	settings: {},
	/**
	 * Dependencies
	 */
	dependencies: [],
	/**
	 * Actions
	 */
	actions: {
		getListBankCode: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/sdk/setting/banks',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			handler: require('./actions/bankCodeList.action.rest')
		},
		getDetectBank: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/sdk/setting/detectBank',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object|optional',
					cardNumber: 'string',
					type: 'string|optional',
					clientId: 'string',
					swiftCode: 'string',
					language: 'string|optional|default:vi'
				}
			},
			handler: require('./actions/detectBank.action.rest')
		},
		getSettingConfig: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/sdk/setting/configs',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object|optional',
					keys: { type: 'array', items: 'string', optional: true },
					tags: 'string|optional',
					appId: 'string|optional'
				}

			},
			handler: require('./actions/getSettingConfig.action.rest')
		}
	},
	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		Init: require('./methods/Init.methods'),
		Get: require('./methods/GetKey.methods'),
		GetTag: require('./methods/GetTag.methods'),
		GetAllKey: require('./methods/GetAllkey.methods')
	},
	/**
   * Service created lifecycle event handler
   */
	async created() {

	},
	async started() {
		setTimeout(async () => {
			this.Variable = {
				settings: {},
				tags: {},
				isReady: false
			};
			await this.Init();
		}, 2000);
	}
};
