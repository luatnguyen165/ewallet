const _ = require('lodash');
const errorCodeConstant = require('../constants/errorCode.constant');

// eslint-disable-next-line consistent-return
module.exports = async function (ctx) {
	const result = {
		code: errorCodeConstant.SYSTEM_ERROR,
		message: this.__(errorCodeConstant.SYSTEM_ERROR.toString())
	};
	try {
		const payload = _.get(ctx, 'params.body', {});
		this.logger.info('[Ewallet setting configs] payload', JSON.stringify(payload));
		const keys = _.get(payload, 'keys', false);
		const tags = _.get(payload, 'tags', false);
		const appId = _.get(payload, 'appId', null);
		let data = [];
		if (keys && tags === false) {
			data = this.Get(keys, null, appId);
		} else if (tags && keys === false) {
			data = this.GetTag(tags, null, appId);
		} else {
			data = this.GetAllKey();
		}
		if (_.isEmpty(data) || !data) {
			return {
				code: errorCodeConstant.GET_SETTING_CONFIG_FAILED,
				message: this.__(errorCodeConstant.GET_SETTING_CONFIG_FAILED.toString())
			};
		}
		return {
			code: errorCodeConstant.GET_SETTING_CONFIG_SUCCEEDED,
			message: this.__(errorCodeConstant.GET_SETTING_CONFIG_SUCCEEDED.toString()),
			data
		};
	} catch (e) {
		this.logger.info('[Ewallet setting configs Error]', JSON.stringify(e.message));
		if (e.message === 'MoleculerError') {
			this.logger.info('[Ewallet setting configs ERRROR]>>>>.', JSON.stringify(e));
			return {
				code: errorCodeConstant.SYSTEM_ERROR,
				message: this.__(errorCodeConstant.SYSTEM_ERROR.toString())
			};
		}
		return result;
	}
};
