const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const CodeConstant = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	const response = {
		code: CodeConstant.DETECT_BANK_FAILED,
		message: ''
	};

	try {
		const payload = _.get(ctx, 'params.body', {});
		const number = payload.cardNumber.replace(/\s/g, '');
		// this.setLocale(payload.language);
		const { swiftCode, type } = payload;

		if (type === 'LINKED') {
			const findCard = {
				state: 'LINKED',
				$or: [
					{
						'cardInfo.cardNumber': number
					},
					{
						'cardInfo.accountNumber': number
					}
				]
			};

			const linkedInfo = await this.broker.call('v1.eWalletLinkedModel.findOne', [findCard]);
			if (_.get(linkedInfo, 'id', false) !== false) {
				const accountInfo = _.get(ctx, 'meta.auth.data', null) || { id: -1 };
				response.message = linkedInfo.accountId === accountInfo.accountId ? 'Số thẻ/STK này đã được liên kết với tài khoản của bạn.' : 'Số thẻ/STK đã được liên kết với tài khoản khác.';
				return response;
			}
			this.logger.info(`[FIND_CARD] >>> ${JSON.stringify(findCard)}`);
		}

		if (process.env.ENV_NAME === 'SANDBOX') {
			let fullName = null;
			switch (number) {
				case '9704000000000018': fullName = 'NGUYEN VAN A'; break;
				case '9704480248306775': fullName = 'THOI THUC PHAN'; break;
				case '0011100003367009': fullName = 'GB NAME 1-803585'; break;
				case '9704125600182062': fullName = 'TRAN TUAN CUONG'; break;
				case '9704125600182054': fullName = 'TRAN TUAN CUONG'; break;
				case '9704150100005654': fullName = 'TRAN THI HUONG'; break;
				default: fullName = 'NGUYEN VAN A'; break; // Seri napas fake
			}

			response.code = CodeConstant.DETECT_BANK_SUCCEEDED;
			response.message = this.__(_.toString(response.code));
			response.data = {
				fullname: fullName
			};
			return response;
		}

		const paramsRequest = {
			body: {
				swiftCode,
				cardNumber: number
			}
		};
		const detectBank = await this.broker.call('v1.utility.detectBank', paramsRequest);
		if (detectBank.code !== 107001) {
			this.logger.info(`[DETECT_BANK_ERROR]::: Call v1.utility.detectBank ::: ${JSON.stringify(detectBank)}`);
			response.message = this.__(_.toString(response.code));
			return response;
		}

		response.code = CodeConstant.DETECT_BANK_SUCCEEDED;
		response.message = this.__(_.toString(response.code));
		response.data = {
			fullname: _.get(detectBank, 'data.fullname', '')
		};
		return response;
	} catch (error) {
		this.logger.info(`[DETECT_BANK_ERROR]::: ${String(error)}`);
		console.error('[DETECT_BANK_ERROR]:::', error);
		response.message = this.__(_.toString(response.code));
		return response;
	}
};
