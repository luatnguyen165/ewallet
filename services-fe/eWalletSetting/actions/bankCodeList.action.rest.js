const _ = require('lodash');
const errorCodeConstant = require('../constants/errorCode.constant');

// eslint-disable-next-line consistent-return
module.exports = async function (ctx) {
	const result = {
		code: errorCodeConstant.SYSTEM_ERROR,
		message: this.__(errorCodeConstant.SYSTEM_ERROR.toString())
	};
	try {
		const listBankCode = await this.broker.call('v1.eWalletBankCodeModel.findMany', [{ isActive: true }]);
		if (_.isEmpty(listBankCode) || !listBankCode) {
			return {
				code: errorCodeConstant.GET_LIST_BANKCODE_FAILED,
				message: this.__(errorCodeConstant.GET_LIST_BANKCODE_FAILED.toString())
			};
		}
		let banks = [];
		listBankCode.map((bank) => (
			banks.push({
				id: _.toNumber(_.get(bank, 'id', null)),
				viName: _.get(bank, 'vi', null),
				enName: _.get(bank, 'en', null),
				shortName: _.get(bank, 'shortName', null),
				swiftCode: _.get(bank, 'swiftCode', null),
				cardPrefix: _.get(bank, 'card.atm.prefix', null),
				cardNumberLength: _.get(bank, 'card.atm.length', null),
				linkable: _.get(bank, 'link.gateway', null) !== null,
				depositable: true,
				withdrawable: true,
				gateway: _.get(bank, 'link.gateway', null),
				requiredDate: _.get(bank, 'requiredDate', 'ISSUE_DATE'),
                otpType: _.get(bank, 'otpType', '6_NUMBER')
			})
		));
		banks = banks.sort((a, b) => a.id - b.id);
		return {
			code: errorCodeConstant.GET_LIST_BANKCODE_SUCCEEDED,
			message: this.__(errorCodeConstant.GET_LIST_BANKCODE_SUCCEEDED.toString()),
			data: banks
		};
	} catch (err) {
		this.logger.info('[Ewallet setting bank code list Error]', JSON.stringify(err));
		if (err.message === 'MoleculerError') {
			this.logger.info('[Ewallet Setting bank code list ERRROR]>>>>.', JSON.stringify(err));
			return {
				code: errorCodeConstant.SYSTEM_ERROR,
				message: this.__(errorCodeConstant.SYSTEM_ERROR.toString())
			};
		}
		return result;
	}
};
