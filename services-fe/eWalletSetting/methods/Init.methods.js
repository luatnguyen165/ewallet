const _ = require('lodash');

module.exports = async function (ctx) {
	const settings = await this.broker.call('v1.eWalletSettingModel.findMany', [{}, '-_id']);
	this.Variable.isReady = false;
	_.forEach(settings, (setting) => {
		if (_.isArray(setting.tag) === true) {
			throw new Error('Setting must is array');
		}
		_.forEach(setting.tags, (tag) => {
			// eslint-disable-next-line no-param-reassign
			tag = _.toLower(tag);
			if (_.isUndefined(this.Variable.tags[tag]) === true) this.Variable.tags[tag] = [];
			this.Variable.tags[tag].push(setting);
		});
		let realKey = setting.key;
		if (setting.appId) {
			realKey = `${setting.key}-${setting.appId}`;
		}
		this.Variable.settings[realKey] = setting;
	});
	this.Variable.isReady = true;
};
