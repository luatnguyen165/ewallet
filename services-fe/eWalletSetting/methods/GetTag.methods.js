const _ = require('lodash');

module.exports = function (tag, isFullObject = false, appId = null) {
	tag = _.toLower(tag);
	if (this.Variable.isReady === false) throw new Error('Setting not ready !');
	const tags = this.Variable.tags[tag];
	if (_.isUndefined(tags) === true) return null;
	const result = {};
	if (isFullObject) {
		_.forEach(tags, (setting) => {
			const settingAppId = _.get(setting, 'appId', 0) || 0;
			const appIdReceive = appId || 0;
			if (!_.isNil(appIdReceive) && appIdReceive === settingAppId) {
				const settingObj = _.clone(setting);
				settingObj.value = this.Get(setting.key);
				result[setting.key] = settingObj;
			}
		});
	} else {
		_.forEach(tags, (setting) => {
			if (!_.isNil(appId) && appId === setting.appId) {
				result[setting.key] = this.Get(setting.key);
			}
		});
	}
	return [result];
};
