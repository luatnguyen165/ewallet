const _ = require('lodash');
const FrontendConstant = require('../constants/FrontendConstant');
module.exports = function (key, isFullObject = false, appId = null) {
	if (this.Variable.isReady === false) throw new Error('Setting not ready !');
	let realKey = key;
	// console.log('AppId ', appId)
	if (appId) {
		realKey = `${key}-${appId}`;
		// console.log(`key :${realKey}`);
	}
	const obj = this.Variable.settings[realKey];
	// if (!_.isNil(appId) && appId !== obj.appId) {
	//   return null;
	// }
	if (_.isUndefined(obj) === true) return null;
	if (_.isNull(obj.value) === true && _.isArray(obj.inherit) && obj.inherit.length > 0) {
		let flag = false;
		_.forEach(obj.inherit, (inherit) => {
			if (flag === true) return false;
			const tmp = this.Get(inherit);
			if (_.isNull(tmp) === false) {
				flag = true;
				obj.value = tmp;
			}
			return true;
		});
	}
	if (_.isNull(obj.value) === true) return null;
	if (obj.type === FrontendConstant.SETTING_TYPE.JSON) {
		return JSON.parse(obj.value);
	}
	if (obj.type === FrontendConstant.SETTING_TYPE.NUMBER) {
		return parseFloat(obj.value);
	}
	if (isFullObject === true) {
		return obj;
	}
	return [obj];

};
