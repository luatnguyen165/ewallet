const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	swiftCode: {
		type: String,
		require: true
	},
	bankName: {
		type: String,
		require: true
	},
	fullName: {
		type: String,
		require: true
	},
	number: {
		type: String,
		require: true
	},
	image: String,
	balance: {
		type: Number,
		required: true,
		default: 0
	},
	totalDeposit: {
		type: Number,
		required: true,
		default: 0
	},
	branch: String,
	city: String,
	description: {
		type: String,
		default: null
	},
	isActive: {
		type: Boolean,
		default: true
	},
	isFastSupport: {
		type: Boolean,
		default: false
	},
	qrContent: {
		type: String,
		default: null
	},
	vietQRAccepted: {
		type: Boolean,
		default: false
	}
	// withdrawFee: { // map với mảng setting
	//   settingId: Number, // bao gồm 2 key : withdraw.manualbank.percent , withdraw.manualbank.fee.min
	//   isActive: {
	//     type: Boolean,
	//     default: true
	//   }
	// },
	// depositFee: {
	//   settingId: Number, // tương tự nhu withdrawFee
	//   isActive: {
	//     type: Boolean,
	//     default: true
	//   }
	// }
}, {
	collection: 'DepositBank',
	versionKey: false,
	timestamps: true
});

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
