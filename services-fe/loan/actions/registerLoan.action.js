const { MoleculerError } = require('moleculer').Errors;
const _ = require('lodash');

// Constants
const LoanConstant = require('../constants/loan.constant');
const GeneralConstant = require('../constants/general.constant');
const CodeConstant = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;
	const response = {
		code: CodeConstant.REGISTER_LOAN_FAILED,
		message: this.__(_.toString(CodeConstant.REGISTER_LOAN_FAILED), { index: 1 })
	};
	const { WooriBankLoanRegister } = GeneralConstant.ACTION;
	try {
		const payload = _.get(ctx, 'params.body', {});

		const paramsRequest = {
			accountId: payload.accountId,
			customerName: payload.fullname,
			customerIdDoctype: payload.docsType,
			customerIdNo: payload.customerIdNo,
			customerIdIssueDate: payload.customerIdIssueDate,
			customerIdExpireDate: payload.customerIdExpireDate,
			birthday: payload.birthday,
			email: payload.email,
			gender: payload.gender,
			address: payload.address,
			phoneNo: payload.phoneNumber,
			placeWork: payload.placeWork,
			hasSalaryThroughBank: payload.hasSalaryThroughBank,
			productCode: payload.productCode,
			loanAmount: payload.loanAmount,
			loanPeriod: payload.loanPeriod,
			registrationNo: payload.registrationNo
		};

		const registerLoan = await Broker.call(WooriBankLoanRegister, paramsRequest);
		Logger(`[REGISTER_LOAN] => [PARAMS]: ${JSON.stringify(paramsRequest)}`);
		Logger(`[REGISTER_LOAN] => [RESPONSE]: ${JSON.stringify(registerLoan)}`);

		if (registerLoan.code !== CodeConstant.WOORI_BANK_LOAN.REGISTER_SUCCESSED) {
			response.message = _.get(registerLoan, 'message', this.__(_.toString(response.code), { index: 2 }));
			return response;
		}

		response.code = CodeConstant.REGISTER_LOAN_SUCCESSED;
		response.message = _.get(registerLoan, 'message', this.__(_.toString(response.code)));
		response.data = registerLoan.data;
		return response;
	} catch (error) {
		Logger(`[REGISTER_LOAN] => [Exception]: ${error}`);
		if (error.name === 'MoleculerError') {
			return {
				code: error.code,
				message: error.message
			};
		}
		return response;
	} finally {
		// this.logger.info('LoanList', JSON.stringify(kycFinal));
	}
};
