const { MoleculerError } = require('moleculer').Errors;
const _ = require('lodash');

// Constants
const LoanConstant = require('../constants/loan.constant');
const GeneralConstant = require('../constants/general.constant');
const CodeConstant = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;
	const response = {
		code: CodeConstant.QUERY_LOAN_DATE_INFO_FAILED,
		message: this.__(_.toString(CodeConstant.QUERY_LOAN_DATE_INFO_FAILED), { index: 1 }),
		data: []
	};
	const { WooriBankLoanInquiryDate } = GeneralConstant.ACTION;
	try {
		const payload = _.get(ctx, 'params.body', {});

		const paramsRequest = {
			date: payload.date,
			language: _.get(payload, 'language', 'vi')
		};

		const registerLoanInfo = await Broker.call(WooriBankLoanInquiryDate, paramsRequest);
		Logger(`[INQUIRY_LOAN_DATE] => [PARAMS]: ${JSON.stringify(paramsRequest)}`);
		Logger(`[INQUIRY_LOAN_DATE] => [RESPONSE]: ${JSON.stringify(registerLoanInfo)}`);

		if (registerLoanInfo.code !== CodeConstant.WOORI_BANK_LOAN.INQUIRY_DATE_SUCCESSED) {
			response.message = _.get(registerLoanInfo, 'message', this.__(_.toString(response.code), { index: 2 }));
			return response;
		}

		response.code = CodeConstant.QUERY_LOAN_DATE_SUCCESSED;
		response.message = _.get(registerLoanInfo, 'message', this.__(_.toString(response.code)));
		response.data = registerLoanInfo.data;
		return response;
	} catch (error) {
		Logger(`[INQUIRY_LOAN] => [Exception]: ${error}`);
		if (error.name === 'MoleculerError') {
			return {
				code: error.code,
				message: error.message
			};
		}
		return response;
	} finally {
		// this.logger.info('LoanList', JSON.stringify(kycFinal));
	}
};
