const { MoleculerError } = require('moleculer').Errors;
const _ = require('lodash');

// Constants
const LoanConstant = require('../constants/loan.constant');
const GeneralConstant = require('../constants/general.constant');
const CodeConstant = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;
	const response = {
		code: CodeConstant.GET_LIST_LOAN_FAILED,
		message: this.__(_.toString(CodeConstant.GET_LIST_LOAN_FAILED), { index: 1 }),
		data: {},
	};
	const { WooriBankLoanList } = GeneralConstant.ACTION;
	try {
		response.code = CodeConstant.GET_LIST_LOAN_SUCCESSED;
		response.message = this.__(_.toString(response.code));

		const paramRequest = {};
		const listLoan = await Broker.call(WooriBankLoanList, paramRequest);

		response.data = listLoan;
		if (listLoan.code === CodeConstant.WOORI_BANK_LOAN.GET_LIST_SUCCESSED) {
			response.data = listLoan;
		}
		return response;
	} catch (error) {
		Logger(`[LIST_LOAN] => [Exception]: ${error}`);
		if (error.name === 'MoleculerError') {
			return {
				code: error.code,
				message: error.message,
			};
		}
		return response;
	} finally {
		// this.logger.info('LoanList', JSON.stringify(kycFinal));
	}
};
