module.exports = {
	ACTION: {
		WooriBankLoanList: 'v1.wooriBankLoan.requestLoanInfo',
		WooriBankLoanRegister: 'v1.wooriBankLoan.registerLoan',
		WooriBankLoanInquiry: 'v1.wooriBankLoan.inquiryProgressLoan',
		WooriBankLoanInquiryDate: 'v1.wooriBankLoan.reconcile'
	}
};
