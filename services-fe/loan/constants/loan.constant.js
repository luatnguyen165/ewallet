module.exports = {
	SUPPLIER: {
		WOORIBANK: 'WOORIBANK',
	},
	CUSTOMER_DOCS: {
		ON: 'ON', // Chứng minh nhân dân
		SN: 'SN', // Căn cước công dân
		PN: 'PN', // Hộ chiếu
	},
};
