const _ = require('lodash');
const path = require('path');
const moleculerI18n = require('moleculer-i18n-js');
const LoanConstant = require('./constants/loan.constant');

module.exports = {
	name: 'loan',

	mixins: [moleculerI18n],

	i18n: {
		directory: path.join(__dirname, 'locales'),
		locales: ['vi', 'en'],
		defaultLocale: 'vi'
	},

	version: 1,

	/**
	 * Settings
	 */
	settings: {

	},

	/**
	 * Dependencies
	 */
	dependencies: ['v1.roleModel'],

	/**
	 * Actions
	 */
	actions: {
		getLoanList: {
			rest: {
				method: 'POST',
				fullPath: '/loan/wooribank/list',
				auth: {
					strategies: ['Local'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object|optional'
				}
			},
			timeout: 100000,
			handler: require('./actions/getLoanList.action')
		},
		registerLoan: {
			rest: {
				method: 'POST',
				fullPath: '/loan/wooribank/register',
				auth: {
					strategies: ['Local'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					accountId: 'number',
					fullname: 'string',
					docsType: {
						type: 'string',
						enum: _.values(LoanConstant.CUSTOMER_DOCS),
						optional: false
					},
					customerIdNo: 'string',
					customerIdIssueDate: 'string',
					customerIdExpireDate: 'string',
					birthday: 'string',
					email: 'string',
					gender: 'string',
					phoneNumber: 'string',
					placeWork: 'string',
					hasSalaryThroughBank: 'boolean|optional|default:true',
					address: 'string',
					productCode: 'string',
					loanAmount: 'number',
					loanPeriod: 'number',
					registrationNo: 'string'
				}
			},
			timeout: 100000,
			handler: require('./actions/registerLoan.action')
		},
		inquiryRegisterLoan: {
			rest: {
				method: 'POST',
				fullPath: '/loan/wooribank/inquiry',
				auth: {
					strategies: ['Local'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					accountId: 'number',
					registrationNo: 'string'
				}
			},
			timeout: 100000,
			handler: require('./actions/inquiryRegisterLoan.action')
		},
		inquiryDateLoan: {
			rest: {
				method: 'POST',
				fullPath: '/loan/wooribank/date-inquiry',
				auth: {
					strategies: ['Local'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					date: 'string',
					language: 'string|default:vi'
				}
			},
			timeout: 100000,
			handler: require('./actions/inquiryDateLoan.action')
		}
	},
	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	}

	/**
	 * Service created lifecycle event handler
	 */
	// created() {},

	/**
	 * Service started lifecycle event handler
	 */

	/**
	 * Service stopped lifecycle event handler
	 */
	// async stopped() {},
};
