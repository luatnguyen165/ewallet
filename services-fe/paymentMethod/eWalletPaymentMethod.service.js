const moleculerI18n = require('moleculer-i18n-js');
const path = require('path');

module.exports = {
	name: 'eWalletPaymentMethod',
	version: 1,
	mixins: [moleculerI18n],
	i18n: {
		directory: path.join(__dirname, 'locales'),
		locales: ['vi', 'en'],
		defaultLocale: 'vi'
	},

	/**
	 * Settings
	 */
	settings: {},
	/**
	 * Dependencies
	 */
	dependencies: [],
	/**
	 * Actions
	 */
	actions: {
		registerMethod: {
			params: {
				registerCode: 'string',
				accountId: 'number',
				group: 'string',
				title: 'string',
				description: 'string',
				isActive: 'boolean|default:true',
				paymentInfo: 'object',
				isWithdrawable: 'boolean|default:false',
				linkedType: 'string|optional'
			},
			handler: require('./actions/registerPaymentMethod.action')
		},
		unregisterMethod: {
			params: {
				registerCode: 'string'
			},
			handler: require('./actions/unregisterPaymentMethod.action')
		}
	},
	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {

	}
};
