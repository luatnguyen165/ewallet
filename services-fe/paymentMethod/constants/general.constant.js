module.exports = {
	MODEL: {
		KYCModel: {
			create: 'v1.eWalletKycModel.create',
			find: 'v1.eWalletKycModel.findMany',
			findOne: 'v1.eWalletKycModel.findOne',
			updateOne: 'v1.eWalletKycModel.updateOne'
		},
		AccountModel: {
			create: 'v1.eWalletAccountModel.create',
			find: 'v1.eWalletAccountModel.findMany',
			findOne: 'v1.eWalletAccountModel.findOne',
			updateOne: 'v1.eWalletAccountModel.updateOne'
		},
		LinkedModel: {
			create: 'v1.eWalletLinkedModel.create',
			find: 'v1.eWalletLinkedModel.findMany',
			findOne: 'v1.eWalletLinkedModel.findOne',
			updateOne: 'v1.eWalletLinkedModel.updateOne'
		},
		HistoryModel: {
			create: 'v1.eWalletHistoryModel.create',
			find: 'v1.eWalletHistoryModel.findMany',
			findOne: 'v1.eWalletHistoryModel.findOne',
			updateOne: 'v1.eWalletHistoryModel.updateOne'
		},
		BankCodeModel: {
			create: 'v1.eWalletBankCodeModel.create',
			find: 'v1.eWalletBankCodeModel.findMany',
			findOne: 'v1.eWalletBankCodeModel.findOne',
			updateOne: 'v1.eWalletBankCodeModel.updateOne'

		},
		SettingModel: {
			create: 'v1.eWalletSettingModel.create',
			find: 'v1.eWalletSettingModel.findMany',
			findOne: 'v1.eWalletSettingModel.findOne',
			updateOne: 'v1.eWalletSettingModel.updateOne'
		},
		PaymentMethodModel: {
			create: 'v1.eWalletPaymentMethodModel.create',
			find: 'v1.eWalletPaymentMethodModel.findMany',
			findOne: 'v1.eWalletPaymentMethodModel.findOne',
			updateOne: 'v1.eWalletPaymentMethodModel.updateOne',
			delete: 'v1.eWalletPaymentMethodModel.delete'
		},
		TransferMethodModel: {
			create: 'v1.eWalletTransferMethodModel.create',
			find: 'v1.eWalletTransferMethodModel.findMany',
			findOne: 'v1.eWalletTransferMethodModel.findOne',
			updateOne: 'v1.eWalletTransferMethodModel.updateOne',
			delete: 'v1.eWalletTransferMethodModel.delete'
		}
	},
	SERVICE: {
		UUID: {
			pick: 'v1.walletUuid.pick'
		},
		BANK: {
			getRecipientByCard: 'v1.bank.getRecipientByCard',
			getRecipientByAccount: 'v1.bank.getRecipientByAccount'
		}
	}
};
