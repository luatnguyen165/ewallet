// Libraries
const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const ResponseCode = require('../constants/code.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;

	const { PaymentMethodModel, TransferMethodModel } = GeneralConstant.MODEL;

	try {
		const {
			registerCode
		} = ctx.params;
		Logger(`[UNREGISTER_PAYMENT_METHOD] => [registerCode: ${registerCode}]`);

		const resultDeletePaymentMethod = await Broker.call(PaymentMethodModel.delete, [{ registerCode }]);
		const resultDeleteTransfertMethod = await Broker.call(TransferMethodModel.delete, [{ registerCode }]);

		return {
			code: ResponseCode.UNREGISTER_PAYMENT_METHOD_SUCCESSED,
			message: 'Hủy kí thành công.'
		};
	} catch (error) {
		if (error.name === 'MoleculerError') throw error;
		throw new MoleculerError(`Hủy đăng kí phương thức thanh toán thất bại: ${error.message}`, ResponseCode.UNREGISTER_PAYMENT_METHOD_FAILED);
	}
};
