// Libraries
const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const ResponseCode = require('../constants/code.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;

	const { PaymentMethodModel, TransferMethodModel } = GeneralConstant.MODEL;

	try {
		const {
			registerCode, accountId, group, title, description, isActive, paymentInfo, isWithdrawable, linkedType, order = 1
		} = ctx.params;

		const objCreatePaymentMethod = {
			registerCode,
			accountId,
			group,
			title,
			description,
			isActive,
			paymentInfo,
			order
		};
		const createdPaymentMethod = await Broker.call(PaymentMethodModel.create, [objCreatePaymentMethod]);

		if (!_.isObject(createdPaymentMethod) || createdPaymentMethod.id < 1) {
			Logger(`[REGISTER_PAYMENT_METHOD] => [CREATE_PAYMENT_ERROR] => ${JSON.stringify(objCreatePaymentMethod)}`);
			throw new MoleculerError('Đăng kí phương thức thanh toán thất bại.', ResponseCode.REGISTER_PAYMENT_METHOD_FAILED);
		}
		let createdWithdrawMethod = null;
		if (isWithdrawable === true) {
			const objCreateTransferMethod = {
				registerCode,
				accountId,
				group,
				title,
				description,
				isActive: true,
				paymentInfo,
				order,
				type: linkedType
			};
			createdWithdrawMethod = await Broker.call(TransferMethodModel.create, [objCreateTransferMethod]);
			if (!_.isObject(createdWithdrawMethod) || createdWithdrawMethod.id < 1) {
				Logger(`[REGISTER_PAYMENT_METHOD] => [CREATE_TRANSFER_ERROR] => ${JSON.stringify(objCreatePaymentMethod)}`);
				throw new MoleculerError('Đăng kí phương thức chuyển tiền thất bại.', ResponseCode.REGISTER_PAYMENT_METHOD_FAILED);
			}
		}
		return {
			code: ResponseCode.REGISTER_PAYMENT_METHOD_SUCCESSED,
			message: 'Đăng kí thành công.',
			data: {
				paymentMethodId: _.get(createdPaymentMethod, 'id', 0),
				transferMethodId: _.get(createdWithdrawMethod, 'id', 0)
			}
		};
	} catch (error) {
		if (error.name === 'MoleculerError') throw error;
		throw new MoleculerError(`Đăng kí phương thức thanh toán thất bại: ${error.message}`, ResponseCode.REGISTER_PAYMENT_METHOD_FAILED);
	}
};
