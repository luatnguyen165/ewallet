const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const generalConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		default: null // null khi dùng cho toàn bộ hệ thống
	},
	group: {
		type: String,
		enum: _.values(generalConstant.PAYMENT_METHOD_GROUP),
		default: (generalConstant.PAYMENT_METHOD_GROUP.PAYME)
	},
	isActive: {
		type: Boolean,
		default: true
	},
	title: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	registerCode: {
		type: String,
		required: true
	},
	paymentInfo: {
		type: Object,
		default: {}
		// với linked bank :
		// swiftcode:
		// card-hitn:  xxxx-xxxx-xxxx-0000
		// linked ID : refer to Linkerd
		// với gateway:
		// swiftcode:
		// gatewayURL
	},
	order: {
		type: Number,
		default: 1
	},
	type: {
		type: String,
		enum: _.values(generalConstant.TRANSFER_GATEWAY_METHOD_TYPE),
		default: null
	}
}, {
	collection: 'Transfer_Method',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: true, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
