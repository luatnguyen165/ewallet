const _ = require('lodash');
const MeAPI = require('./crypto.library');

const Private = {
	isSecurity: false,
	privateKey: '',
	publicKey: '',

	async requestPost(url, body, headers = { Authorization: '' }, uri) {
		try {
			console.log(`PG Service --- Request --- url = ${url}${uri} with params = ${JSON.stringify(body)}`);

			const result = {
				code: -1,
				data: {},
				original: null
			};
			const meAPI = new MeAPI({
				url,
				privateKey: this.privateKey,
				publicKey: this.publicKey,
				isSecurity: this.isSecurity,
				'x-api-client': 'app'
			});
			const response = await meAPI.Post(`${uri}`, body, headers.Authorization);
			if (response.code !== 1) {
				return response;
			}
			result.code = response.data.code;
			result.data = response.data.data;
			result.original = response.original;

			console.log(`PG Service --- Response --- url = ${url}${uri}  with data = ${JSON.stringify(result)}`);
			return result;
		} catch (error) {
			return error;
		}
	}
};

class PaymentGatewayService {
	constructor(url = '', accessToken = '', isSecurity = false, privateKey = '', publicKey = '') {
		this.url = url;
		this.accessToken = accessToken;
		Private.isSecurity = isSecurity;
		Private.privateKey = privateKey;
		Private.publicKey = publicKey;
	}

	/**
	 * Chuyển tiền thông qua ví PayME
	 * @param payload
	 * @returns {Promise<{code: number, original: null, data: {}}>}
	 */
	async ipnSDK(accessToken, payload = {}) {
		const response = {
			code: -1,
			data: null,
			original: null
		};
		try {
			const result = await Private.requestPost(this.url, payload, { Authorization: accessToken }, '/v1/Payment/PayME/SDK/IPN');
			if (result.code === 1000) response.code = 1;
			response.data = result.data;
			response.original = result.original;

			return response;
		} catch (error) {
			response.data = {
				message: error.message
			};

			return response;
		}
	}
}

const paymentGatewayService = new PaymentGatewayService(
	process.env.PG_URL,
	process.env.PG_TOKEN,
	process.env.PG_SECURITY,
	process.env.PG_PRIVATE_KEY,
	process.env.PG_PUBLIC_KEY
);

module.exports = paymentGatewayService;
