const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const MeAPILibs = require('./crypto.library');
const GeneralConstant = require('../constants/general.constant');

function PayMeLibrary(config = {}) {
	const Logger = config.logger;

	const apiConfig = {
		url: process.env.PAYME_URL,
		isSecurity: process.env.PAYME_SECURITY,
		privateKey: process.env.PAYME_PRIVATE_KEY,
		publicKey: process.env.PAYME_PUBLIC_KEY,
		'x-api-client': 'app'
	};
	const MeAPI = new MeAPILibs(apiConfig);

	const PostRequest = async function (body, accessToken, headers = { }, url) {
		const result = {
			code: -1,
			data: {},
			original: null
		};
		try {
			Logger(`[FE_PayMeLibrary] => [Request]: ${JSON.stringify({ url: `${apiConfig.url}${url}`, body })}`);
			const response = await MeAPI.Post(url, body, accessToken, headers);
			Logger(`[FE_PayMeLibrary] => [Response]: ${JSON.stringify({ url: `${apiConfig.url}${url}`, response })}`);
			if (response.code !== 1) {
				result.original = response;
				return result;
			}

			result.code = response.code;
			result.data = response.data;
			result.original = response.original;
		} catch (error) {
			Logger(`[EwalletOpenPaymentAPI] => [PayMeLibrary] => [Exception]: ${error}`);
			result.original = error.message;
		}
		return result;
	};

	return {
		async Pay(accessToken = '', params = {
			phone: '',
			amount: '',
			partnerTransaction: '',
			description: '',
			merchantId: '',
			payment: ''
		}) {
			const response = {
				code: -1,
				data: {},
				original: null
			};
			try {
				const body = {
					...params
				};

				const result = await PostRequest(body, accessToken, {}, '/v3/OpenApi/OpenEWalletPayment/PayV3');

				if (result.code !== 1) {
					response.original = JSON.stringify(result);
					return response;
				}

				const responseCode = _.get(result, 'data.code', -99);
				if (responseCode === 1000) response.code = 1;
				else if (responseCode === 4040) response.code = 2; // OTP
				else if (responseCode === 4050) response.code = 3; // Form

				response.data = _.get(result, 'data.data', {});
				response.original = JSON.stringify(result);
				return response;
			} catch (err) {
				Logger(`[FE_PayMeLibrary] => [PAY] => [Exception]: ${err}`);
				response.data.message = err.message;
				return response;
			}
		},
		async Verify(accessToken = '', params = {
			transaction: '',
			otp: ''
		}) {
			const response = {
				code: -1,
				data: {},
				original: null
			};
			try {
				const body = {
					...params
				};

				const result = await PostRequest(body, accessToken, {}, '/v3/OpenApi/OpenEWalletPayment/VerifyV3');

				if (result.code !== 1) {
					response.original = JSON.stringify(result);
					return response;
				}

				const responseCode = _.get(result, 'data.code', -99);
				if (responseCode === 1000) response.code = 1;
				else if (result.code === 4042) response.code = 2; // OTP

				response.data = _.get(result, 'data.data', {});
				response.original = JSON.stringify(result);
				return response;
			} catch (err) {
				Logger(`[FE_PayMeLibrary] => [VERIFY] => [Exception]: ${err}`);
				response.data.message = err.message;
				return response;
			}
		}
	};
}
module.exports = PayMeLibrary;
