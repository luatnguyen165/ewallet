const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const Moment = require('moment');

const ResponseCode = require('../constants/errorCode.constant');
const {
	LinkedModel, BankCodeModel, AccountModel, AppInfoModel, ConnectedUserModel
} = require('../constants/general.constant').MODEL;

module.exports = {

	async getUserInfo(ctx) {
		const Broker = ctx.broker;
		const Logger = ctx.broker.logger.info;

		const payload = _.get(ctx, 'params.body', {});
		const query = _.get(ctx, 'params.query', {});

		let authInfo = _.get(ctx, 'meta.auth.data', {});
		if (_.isEmpty(authInfo) === true) {
			authInfo = this.GetAuthInfo(ctx.meta);
		}

		const listActionValid = ['pay_BE'];
		const rawNameAction = _.get(ctx, 'action.rawName', '');

		if (_.includes(listActionValid, rawNameAction) === false) {
			authInfo.sourceConnected = _.isEmpty(authInfo) === true ? 'WAP' : 'FE';
			_.set(ctx, 'meta.auth.walletAuthInfo', authInfo);
			return true;
		}

		let responseCode = null;
		let message = '';

		switch (rawNameAction) {
			case 'pay_BE':
				responseCode = ResponseCode.PAYMENT_FAILED;
				message = 'Thanh toán thất bại. Vui lòng thử lại sau.';
				break;
			case 'verifyPay_BE':
				responseCode = ResponseCode.VERIFY_FAILED;
				message = 'Xác thực thanh toán thất bại. Vui lòng thử lại sau.';
				break;
			default:
				return false;
		}

		try {
			// HardCode for test
			let phoneUser = _.get(payload, 'phone', false) || _.get(query, 'phone', false);
			if (!!phoneUser === false) throw new MoleculerError('Không tìm thấy thông tin phone user. Vui lòng kiểm tra lại.', responseCode);

			const checkPhone = this.ValidatePhone(phoneUser);
			if (checkPhone.isValid === false) throw new MoleculerError('Số điện thoại không hợp lệ. Vui lòng kiểm tra lại.', responseCode);

			phoneUser = checkPhone.phone;
			let findCondition = {
				phone: phoneUser
			};
			const accountInfo = await Broker.call(AccountModel.findOne, [findCondition]);
			if (_.get(accountInfo, 'id', false) === false) {
				Logger(`[BEFORE_HOOK] => [getUserInfo] => [findCondition] => ${JSON.stringify(findCondition)}`);
				throw new MoleculerError('Không tìm thấy thông tin tài khoản Ví.', responseCode);
			}

			if (_.get(accountInfo, 'kyc.state', '') !== 'APPROVED') {
				Logger(`[BEFORE_HOOK] => [getUserInfo] => [findCondition] => ${JSON.stringify(findCondition)}`);
				throw new MoleculerError('Tài khoản Ví chưa được định danh. Vui lòng kiểm tra lại.', responseCode);
			}

			findCondition = {
				merchantId: _.get(authInfo, 'merchantId', '')
			};

			// Lấy APP mới nhất
			const appInfo = await Broker.call(AppInfoModel.findOne, [findCondition, '-_id', {
				sort: {
					createdAt: 1
				}
			}]);

			if (_.get(appInfo, 'id', false) === false) {
				Logger(`[BEFORE_HOOK] => [getUserInfo] => [findCondition] => ${JSON.stringify(findCondition)}`);
				throw new MoleculerError('Không tìm thấy thông tin APP', responseCode);
			}

			findCondition = {
				accountId: accountInfo.id,
				phone: phoneUser,
				appId: appInfo.id
			};
			const userConnected = await Broker.call(ConnectedUserModel.findOne, [findCondition]);
			if (_.get(userConnected, 'id', false) === false) {
				Logger(`[BEFORE_HOOK] => [getUserInfo] => [findCondition] => ${JSON.stringify(findCondition)}`);
				throw new MoleculerError('Số điện thoại chưa được liên kết.', responseCode);
			}

			const walletAuthInfo = {
				sourceConnected: 'BE',
				accountId: accountInfo.id,
				phone: accountInfo.phone,
				appId: appInfo.id,
				fullname: accountInfo.fullname,
				avatar: _.get(accountInfo, 'avatar', null),
				kyc: {
					kycId: _.get(accountInfo, 'kyc.kycId', null),
					state: _.get(accountInfo, 'kyc.state', null),
					identifyNumber: _.get(accountInfo, 'kyc.identifyNumber', null)
				},
				isVerifiedEmail: _.get(accountInfo, 'isVerifiedEmail', false),
				email: _.get(accountInfo, 'email', null),
				accountGroupId: _.get(accountInfo, 'accountGroupId', null),
				accountType: _.get(accountInfo, 'accountType', 'PERSONAL')
			};
			_.set(ctx, 'meta.auth.walletAuthInfo', walletAuthInfo);

			return true;
		} catch (error) {
			if (error.name === 'MoleculerError') throw error;
			Logger(`[BEFORE_HOOK] => [getUserInfo] => [Exception] => ${error}`);
			throw new MoleculerError(message, responseCode);
		}
	}

};
