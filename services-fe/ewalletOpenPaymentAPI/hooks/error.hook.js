module.exports = {
	handlerError(ctx, err) {
		return {
			code: err.code,
			message: err.message
		};
	}
};
