const _ = require('lodash');
const PayMELibrary = require('./libraries/payME.library');

// Hooks
const BeforeHooks = require('./hooks/before.hook');
const ErrorHooks = require('./hooks/error.hook');

module.exports = {
	name: 'ewalletOpenPayAPI',

	version: 1,

	// mixins: [moleculerI18n],
	// i18n: {
	// 	directory: path.join(__dirname, 'locales'),
	// 	locales: ['vi', 'en'],
	// 	defaultLocale: 'vi'
	// },
	hooks: {
		before: {
			'*': [
				BeforeHooks.getUserInfo // ==> Trường kết nối server to server thì nhảy vào đây để kiếm auth user
			]
		},
		error: {
			'*': ErrorHooks.handlerError
		}
	},
	/**
	 * Settings
	 */
	settings: {
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		pay: {
			rest: {
				method: 'POST',
				fullPath: '/fe/ewallet/payment/pay',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					merchantId: 'number|optional',
					phone: 'string',
					amount: 'number',
					partnerTransaction: 'string',
					description: 'string|optional',
					payment: {
						$$type: 'object',
						type: {
							type: 'string',
							enum: ['WALLET', 'LINKED'],
							default: 'WALLET'
						},
						linkedId: 'number|optional'
					}
				}
			},
			handler: require('./actions/pay.action.rest')
		},
		pay_BE: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/payment/pay',
				auth: false
				// auth: {
				// 	strategies: ['Default'],
				// 	mode: 'required'
				// }
			},
			params: {
				body: {
					$$type: 'object',
					merchantId: 'number|optional',
					phone: 'string',
					amount: 'number',
					partnerTransaction: 'string',
					description: 'string|optional',
					payment: {
						$$type: 'object',
						type: {
							type: 'string',
							enum: ['WALLET', 'LINKED'],
							default: 'WALLET'
						},
						linkedId: 'number|optional'
					}
				}
			},
			handler: require('./actions/pay.action.rest')
		},
		verifyPay: {
			rest: {
				method: 'POST',
				fullPath: '/fe/ewallet/payment/verify',
				auth: {
					strategies: ['Ewallet'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					transaction: 'string',
					otp: 'string',
					clientId: 'string'
				}
			},
			handler: require('./actions/verifyPayment.action.rest')

		},
		verifyPay_BE: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/payment/verify',
				auth: false
				// auth: {
				// 	strategies: ['Default'],
				// 	mode: 'required'
				// }
			},
			params: {
				body: {
					$$type: 'object',
					transaction: 'string',
					otp: 'string',
					clientId: 'string'
				}
			},
			handler: require('./actions/verifyPayment.action.rest')

		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		ValidatePhone: require('./methods/validatePhone.method'),
		GetAuthInfo: require('./methods/getAuthInfo.method')
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		try {
			_.set(this, 'FEPayMELibs', PayMELibrary({ broker: this.broker, logger: this.logger.info }));
		} catch (error) {
			this.logger.info(`[Generate_Library_Error] => ${error}`);
		}
	},

	/**
	 * Service started lifecycle event handler
	 */

	/**
	 * Service stopped lifecycle event handler
	 */

	// async stopped() {},

	async afterConnected() {
		this.logger.info('Connected successfully...');
	}
};
