module.exports = {

	VN_PHONE_PREFIX: {
		VIETTEL: ['086', '096', '097', '098', '032', '033', '034', '035', '036', '037', '038', '039'],
		MOBIFONE: ['089', '090', '093', '070', '079', '077', '076', '078'],
		VINAPHONE: ['088', '091', '094', '083', '084', '085', '081', '082'],
		VIETNAMOBILE: ['092', '056', '058', '052'],
		GMOBILE: ['099', '059'],
		SFone: ['095'],
		ITELECOM: ['087']
	},

	MODEL: {
		KYCModel: {
			create: 'v1.eWalletKycModel.create',
			find: 'v1.eWalletKycModel.findMany',
			findOne: 'v1.eWalletKycModel.findOne',
			updateOne: 'v1.eWalletKycModel.updateOne',
			findOneAndUpdate: 'v1.eWalletKycModel.findOneAndUpdate'
		},
		AccountModel: {
			create: 'v1.eWalletAccountModel.create',
			find: 'v1.eWalletAccountModel.findMany',
			findOne: 'v1.eWalletAccountModel.findOne',
			updateOne: 'v1.eWalletAccountModel.updateOne',
			findOneAndUpdate: 'v1.eWalletAccountModel.findOneAndUpdate'
		},
		AppInfoModel: {
			create: 'v1.ewalletAppInfoModel.create',
			find: 'v1.ewalletAppInfoModel.findMany',
			findOne: 'v1.ewalletAppInfoModel.findOne',
			updateOne: 'v1.ewalletAppInfoModel.updateOne',
			findOneAndUpdate: 'v1.ewalletAppInfoModel.findOneAndUpdate'
		},
		ConnectedUserModel: {
			create: 'v1.eWalletConnectedUserModel.create',
			find: 'v1.eWalletConnectedUserModel.findMany',
			findOne: 'v1.eWalletConnectedUserModel.findOne',
			updateOne: 'v1.eWalletConnectedUserModel.updateOne',
			findOneAndUpdate: 'v1.eWalletConnectedUserModel.findOneAndUpdate'
		},
		LinkedModel: {
			create: 'v1.eWalletLinkedModel.create',
			find: 'v1.eWalletLinkedModel.findMany',
			findOne: 'v1.eWalletLinkedModel.findOne',
			updateOne: 'v1.eWalletLinkedModel.updateOne',
			findOneAndUpdate: 'v1.eWalletLinkedModel.findOneAndUpdate'
		},
		HistoryModel: {
			create: 'v1.eWalletHistoryModel.create',
			find: 'v1.eWalletHistoryModel.findMany',
			findOne: 'v1.eWalletHistoryModel.findOne',
			updateOne: 'v1.eWalletHistoryModel.updateOne',
			findOneAndUpdate: 'v1.eWalletHistoryModel.findOneAndUpdate'
		},
		BankCodeModel: {
			create: 'v1.eWalletBankCodeModel.create',
			find: 'v1.eWalletBankCodeModel.findMany',
			findOne: 'v1.eWalletBankCodeModel.findOne',
			updateOne: 'v1.eWalletBankCodeModel.updateOne',
			findOneAndUpdate: 'v1.eWalletBankCodeModel.findOneAndUpdate'
		},
		SettingModel: {
			create: 'v1.eWalletSettingModel.create',
			find: 'v1.eWalletSettingModel.findMany',
			findOne: 'v1.eWalletSettingModel.findOne',
			updateOne: 'v1.eWalletSettingModel.updateOne',
			findOneAndUpdate: 'v1.eWalletSettingModel.findOneAndUpdate'
		},
		DepositModel: {
			create: 'v1.eWalletDepositModel.create',
			find: 'v1.eWalletDepositModel.findMany',
			findOne: 'v1.eWalletDepositModel.findOne',
			updateOne: 'v1.eWalletDepositModel.updateOne',
			findOneAndUpdate: 'v1.eWalletDepositModel.findOneAndUpdate'
		},
		// eWalletPaymentModel
		PaymentEwalletModel: {
			create: 'v1.eWalletPaymentModel.create',
			find: 'v1.eWalletPaymentModel.findMany',
			findOne: 'v1.eWalletPaymentModel.findOne',
			updateOne: 'v1.eWalletPaymentModel.updateOne',
			findOneAndUpdate: 'v1.eWalletPaymentModel.findOneAndUpdate'
		},
		PaymentPGModel: {
			create: 'v1.paymentModel.create',
			find: 'v1.paymentModel.findMany',
			findOne: 'v1.paymentModel.findOne',
			updateOne: 'v1.paymentModel.updateOne',
			findOneAndUpdate: 'v1.paymentModel.findOneAndUpdate'
		}
	},
	SERVICE: {
		UUID: {
			pick: 'v1.walletUuid.pick'
		},
		BANK: {
			getRecipientByCard: 'v1.bank.getRecipientByCard',
			getRecipientByAccount: 'v1.bank.getRecipientByAccount'
		},
		PAYMENT_METHOD: {
			registerPaymentMethod: 'v1.eWalletPaymentMethod.registerMethod',
			unregisterPaymentMethod: 'v1.eWalletPaymentMethod.unregisterMethod'
		},
		PAYMENT_GATEWAY: {
			createLinkedOrder: 'v1.order.paymentWeb',
			linkedCredit: 'v1.payment.fePayment',
			unlinkCredit: 'v1.payment.creditUnlinked',
			refundCredit: 'v1.order.refund'
		}
	}

};
