const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const PayMeLibrary = require('../libraries/payME.library');
const ResponseCode = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	const response = {
		code: ResponseCode.VERIFY_FAILED,
		message: 'Xác thực thanh toán. Vui lòng thử lại sau (E001).'
	};

	try {
		const {
			// accessToken,
			transaction,
			otp
		} = _.get(ctx, 'params.body');
		// const
		Logger(`[EWALLET_PAYMENT] => [VERIFY_PAY] => [payload] => ${JSON.stringify(_.get(ctx, 'params.body'))}`);
		Logger(`[EWALLET_PAYMENT] => [VERIFY_PAY] => [credentials] => ${JSON.stringify(_.get(ctx, 'meta.auth.credentials'))}`);
		Logger(`[EWALLET_PAYMENT] => [VERIFY_PAY] => [walletAuthInfo]: ${JSON.stringify(_.get(ctx, 'meta.auth.walletAuthInfo', {}))}`);

		const paramsRequest = {
			otp,
			transaction
		};
		const accessToken = process.env.PAYME_TOKEN_LOCAL;
		const resultPayment = await this.FEPayMELibs.Verify(accessToken, paramsRequest);

		Logger(`[EWALLET_PAYMENT] => [VERIFY_PAY] => [Params] => ${JSON.stringify(paramsRequest)}`);
		Logger(`[EWALLET_PAYMENT] => [VERIFY_PAY] => [Response]: ${JSON.stringify(resultPayment)}`);

		if (resultPayment.code === 1) {
			response.code = ResponseCode.VERIFY_SUCCESSED;
			response.message = 'Xác nhật thanh toán thành công.';
			response.data = {
				transaction: _.get(resultPayment, 'data.supplierTransaction', null)
			};
			return response;
		}

		if (resultPayment.code === 2) {
			response.code = ResponseCode.VERIFY_RETRY_OTP;
			response.message = 'Mã OTP không chính xác. Vui lòng nhập lại.';
			return response;
		}

		response.message = _.get(resultPayment, 'data.message', 'Xác thực thanh toán. Vui lòng thử lại sau (E002).');
		return response;
	} catch (err) {
		if (err.name === 'MoleculerError') {
			response.message = err.message;
		}
		Logger(`[EWALLET_PAYMENT] => [VERIFY_PAY] => [Exception] => ${err}`);
		return response;
	}
};
