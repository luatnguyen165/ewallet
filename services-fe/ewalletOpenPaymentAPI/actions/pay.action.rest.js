const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const ResponseCode = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const Broker = this.broker;

	const response = {
		code: ResponseCode.PAYMENT_FAILED,
		message: 'Thanh thanh toán thất bại. Vui lòng thử lại sau (E001).'
	};
	try {
		const {
			// accessToken,
			phone,
			amount,
			partnerTransaction,
			description,
			payment
		} = _.get(ctx, 'params.body');

		Logger(`[EWALLET_PAYMENT] => [PAY] => [payload] => ${JSON.stringify(_.get(ctx, 'params.body'))}`);
		Logger(`[EWALLET_PAYMENT] => [PAY] => [credentials] => ${JSON.stringify(_.get(ctx, 'meta.auth.credentials'))}`);
		Logger(`[EWALLET_PAYMENT] => [PAY] => [walletAuthInfo]: ${JSON.stringify(_.get(ctx, 'meta.auth.walletAuthInfo', {}))}`);

		let { merchantId } = _.get(ctx, 'params.body');
		if (_.isNil(merchantId) === true) {
			const authInfo = _.get(ctx, 'meta.auth.walletAuthInfo', {});

			const appInfo = await Broker.call('v1.ewalletAppInfoModel.findOne', [{ id: authInfo.appId }]);
			if (_.get(appInfo, 'id', false) === false) {
				response.message = 'Thanh thanh toán thất bại. Vui lòng thử lại sau (E002).';
				return response;
			}
			merchantId = appInfo.merchantId;
		}
		const paramsRequest = {
			merchantId,
			phone,
			amount,
			partnerTransaction,
			description,
			payment
		};
		const accessToken = process.env.PAYME_TOKEN_LOCAL;
		const resultPayment = await this.FEPayMELibs.Pay(accessToken, paramsRequest);

		Logger(`[EWALLET_PAYMENT] => [PAY] => [Params] => ${JSON.stringify(paramsRequest)}`);
		Logger(`[EWALLET_PAYMENT] => [PAY] => [Response] => ${JSON.stringify(resultPayment)}`);

		if (resultPayment.code === 1) {
			response.code = ResponseCode.PAYMENT_SUCCESSED;
			response.message = 'Thanh toán thành công.';
			response.data = {
				transaction: _.get(resultPayment, 'data.supplierTransaction', null)
			};
			return response;
		}

		if (resultPayment.code === 2) {
			response.code = ResponseCode.PAYMENT_REQUIRE_OTP;
			response.message = 'Thanh toán cần xác thực mã OTP.';
			response.data = {
				transaction: _.get(resultPayment, 'data.transaction', null)
			};
			return response;
		}

		if (resultPayment.code === 3) {
			response.code = ResponseCode.PAYMENT_REQUIRE_VERIFY;
			response.message = 'Thanh toán cần xác thực bằng form ngân hàng.';
			response.data = {
				transaction: _.get(resultPayment, 'data.transaction', null),
				form: _.get(resultPayment, 'data.form', null)
			};
			return response;
		}

		response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.';
		return response;
	} catch (err) {
		if (err.name === 'MoleculerError') {
			response.message = err.message;
		}
		Logger(`[EWALLET_PAYMENT] => [PAY] => [Exception] => ${err}`);
		return response;
	}
};
