const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const TransportConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema(
	{
		accountId: {
			type: Number,
			required: true
		},
		transportId: {
			type: Number,
			required: true
		},
		appId: {
			type: Number,
			default: 1
		},
		transaction: {
			type: String,
			required: true
		},
		amount: {
			type: Number,
			required: true,
			default: 0
		},
		service: {
			code: {
				type: String,
				default: null
			},
			type: {
				type: String,
				default: null
			},
			transaction: {
				type: String,
				required: true
			}
		},
		state: {
			type: String,
			enum: _.values(TransportConstant.STATE),
			default: TransportConstant.STATE.PENDING
		},
		description: {
			type: String,
			default: null
		},
		note: {
			type: String,
			default: null
		},
		balance: {
			before: {
				cash: {
					type: Number
				},
				credit: Number
			},
			after: {
				cash: {
					type: Number
				},
				credit: Number
			}
		}
	},
	{
		collection: 'TransportWallet',
		versionKey: false,
		timestamps: true
	}
);

Schema.index({ accountId: 1 }, { index: true });
Schema.index({ transaction: 1 }, { index: true });
Schema.index({ state: 1 }, { index: true });
Schema.index({ 'service.code': 1 }, { index: true });

/*
	| ==========================================================
	| Plugins
	| ==========================================================
	*/
Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
	| ==========================================================
	| Methods
	| ==========================================================
	*/

/*
	| ==========================================================
	| HOOKS
	| ==========================================================
	*/

module.exports = mongoose.model(Schema.options.collection, Schema);
