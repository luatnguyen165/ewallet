const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const generalConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	id: {
		type: Number,
		required: true
	},
	appId: {
		type: Number,
		default: null
	},
	accountId: {
		type: Number,
		required: true
	},
	transaction: {
		type: String,
		required: true
	},
	requestId: {
		type: String,
		default: null
	},
	cardInfo: Object,
	linkedInfo: Object,
	linkedAt: {
		type: Date
	},
	type: {
		type: String,
		enum: _.values(generalConstant.LINKED_TYPE)
	},
	state: {
		type: String,
		enum: _.values(generalConstant.LINKED_STATE)
	},
	source: {
		type: String // nguon lk, banking=>swiftCode || visa,master...
	},
	referId: {
		type: Number
	},
	supplier: {
		type: String
	},
	supplierResponsed: [String],
	depositTransaction: String
}, {
	collection: 'Service_Linked',
	versionKey: false,
	timestamps: true
});

Schema.index({ uuid: 1, prefix: 1, state: 1 }, { index: true });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-idAI`,
	field: 'idAI',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
