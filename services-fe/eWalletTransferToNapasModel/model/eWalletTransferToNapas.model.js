const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const generalConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number
		// required: true
	},
	transactionId: {
		type: String,
		required: true
	},
	transferId: {
		type: Number
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	state: {
		type: String,
		enum: _.values(generalConstant.TRANSFER_METHOD_NAPAS_STATE)
	},
	description: {
		type: String,
		default: null
	},
	bankInfo: {
		swiftCode: String,
		bankName: String,
		cardNumber: String,
		cardHolder: String
	},
	type: {
		type: String,
		default: null,
		enum: _.values(generalConstant.TRANSFER_GATEWAY_METHOD_TYPE)
	},
	bankTransaction: String,
	supplierResponsed: Object
}, {
	collection: 'Transfer_ToNapas',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: true, unique: false });
Schema.index({ state: 1 });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
