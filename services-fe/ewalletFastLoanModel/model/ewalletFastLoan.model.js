const _ = require('lodash');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	fullname: String,
	phone: String,
	packageId: {
		type: Number,
		required: true
	},
	transactionList: [
		{
			type: String
		}
	],
	customerId: {
		type: String,
		default: null
	},
	transaction: {
		type: String,
		required: true
	},
	supplierTransaction: {
		type: String,
		default: null
	},
	// Nhà cung cấp gói tín dụng
	supplierInfo: {
		id: {
			type: Number,
			required: true
		},
		name: {
			type: String
		},
		logo: {
			type: String
		},
		title: {
			type: String
		}
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.FAST_LOAN_STATE),
		default: GeneralConstant.FAST_LOAN_STATE.PENDING
	},
	approvedAt: {
		// Thời gian cấp
		type: Date,
		default: null
	},
	amount: Number,
	total: Number,
	monthlyTotal: Number,
	fee: Number,
	termFee: Number,
	dueDate: {
		// Thời hạn thanh toán
		type: Number,
		default: null
	},
	settlementDate: {
		type: Date,
		default: null
	},
	term: {
		type: Number,
		default: null
	},
	interestTitle: String,
	interestRate: {
		type: Number,
		default: null
	},
	// Thông tin sao kê của khách hàng (bảng lương, hợp đồng lao động, ...)
	customerInfo: mongoose.Schema.Types.Mixed,
	// Thông tin trả về của nhà cung cấp khi IPN
	supplierResponsed: [String]
}, {
	collection: 'Service_FastLoanV2',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
