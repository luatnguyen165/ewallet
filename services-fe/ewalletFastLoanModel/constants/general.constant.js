module.exports = {
	PACKAGE_GROUP: {
		EMAIL: 'EMAIL',
		COMPANY: 'COMPANY',
		PHONE_NUMBER: 'PHONE_NUMBER',
		PARENT: 'PARENT',
		KYC_LEVEL: 'KYC_LEVEL',
		KYC_APPROVED: 'KYC_APPROVED',
		ACCOUNT_GROUP: 'ACCOUNT_GROUP'
	},
	PACKAGE_LEVEL: {
		STANDARD: 'STANDARD',
		GOLD: 'GOLD',
		PLATINIUM: 'PLATINIUM'
	},
	PACKAGE_STATEMENT: {
		DRIVING_LICENSE_ATTACH: 'DRIVING_LICENSE_ATTACH', // GIÁY PHÉP LÁI XE
		FAMILY_BOOK_ATTACH: 'FAMILY_BOOK_ATTACH', // HỘ KHẨU
		IDCARD_ATTACH: 'IDCARD_ATTACH', // CCCD
		CLIENT_PICTURE_ATTACH: 'CLIENT_PICTURE_ATTACH' // HÌNH CHỤP KHÁCH HÀNG
	},
	PACKAGE_TERM_TYPE: {
		MONTH: 'MONTH',
		YEAR: 'YEAR'
	},
	PACKAGE_STATE: {
		// Hoạt động
		ACTIVE: 'ACTIVE',
		// Tạm ngưng
		PAUSE: 'PAUSE',
		// Ngừng hoạt động
		INACTIVE: 'INACTIVE'
	},
	PACKAGE_APPROVEDAT: {
		0: { title: 'Tức thì', color: '' },
		1: { title: '{} ngày', color: '' }
	},
	FAST_LOAN_STATE: {
		// Chờ thẩm định
		PENDING: 'PENDING',
		// ỨNG TIỀN THÀNH CÔNG ~ Đã được giải ngân
		SUCCEEDED: 'SUCCEEDED',
		// ỨNG TIỀN THẤT BẠI
		FAILED: 'FAILED',
		// Tất toán ~ Đa tất toán
		CLOSED: 'CLOSED',
		// Từ chối
		REJECTED: 'REJECTED',
		// Đủ điều kiện vay, nhưng chưa giải ngân
		APPROVED: 'APPROVED',
		// Chờ xử lý hồ sơ
		PROCESSING: 'PROCESSING',
		// TẠO HỒ SƠ MỚI
		REQUESTED: 'REQUESTED'
	},
	WOORI_LOAN_STATE: {
		// Chờ tiệp nhận
		WAITING_FOR_RECEPTION: 'WAITING_FOR_RECEPTION',
		// Hoàn tất tiếp nhận
		COMPLETION_OF_RECEPTION: 'COMPLETION_OF_RECEPTION',
		// Đang chờ thẩm định
		UNDER_REVIEW: 'UNDER_REVIEW',
		// Đủ điều kiện vay
		LOAN_AVAILABLE: 'LOAN_AVAILABLE',
		// Giải ngân thành công
		LOAN_COMPLETION: 'LOAN_COMPLETION',
		// Hủy
		CANCELLATION_OF_REGISTRATION: 'CANCELLATION_OF_REGISTRATION'
	},

	CONNECTOR_STATE: {
		LEAD_NOT_CONTACTED: 'LEAD_NOT_CONTACTED',
		LEAD_NOT_ELIGIBLE: 'LEAD_NOT_ELIGIBLE',
		LEAD_NOT_INTERESTED: 'LEAD_NOT_INTERESTED',
		LEAD_FOLLOWING: 'LEAD_FOLLOWING',
		LEAD_COLLECT_DOCUMENT: 'LEAD_COLLECT_DOCUMENT',
		APP_PENDING: 'APP_PENDING',
		APP_REJECTED: 'APP_REJECTED',
		APP_APPROVED: 'APP_APPROVED',
		APP_DISBURSED: 'APP_DISBURSED',
		APP_CANCEL: 'APP_CANCEL'
	},
	SERVICES: {
		FAST_LOAN: 'FAST_LOAN'
	},
	RESOLVE_TYPE: 'FastLoanObject',
	CUSTOMER_STATE: {
		PENDING: 'PENDING',
		CANCELED: 'CANCELED',
		REJECTED: 'REJECTED',
		APPROVED: 'APPROVED',
		NULL: null
	},
	SUPPLIER: {
		// OCB: {
		//   createUrl: 'v1/ocb/cash_loan/create_new_lead',
		//   statusUrl: 'v1/ocb/cash_loan/lead_status',
		//   ipn: 'v1/ocb/cash_loan/lead_status/ipn',
		//   title: 'OCB'
		// },
		WOORI_BANK: {
			id: 1,
			name: 'Ngân hàng TNHH MTV Woori Việt Nam',
			logo: 'https://static.payme.vn/image_bank/image_method/methodHVBKVNVX@2x.png',
			title: 'Woori Bank'
		}
	},
	TAGS: {
		FAST_LOAN: 'FAST_LOAN',
		FAST_LOAN_RECEIVED: 'FAST_LOAN_RECEIVED'
	},
	APPTYPE: {
		INAPP: 'INAPP',
		WEBAPP: 'WEBAPP',
		WEB_MINI_PROGRAM: 'WEB_MINI_PROGRAM'
	}
};
