const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	service: {
		id: {
			type: String,
			default: null
		},
		transaction: {
			type: String,
			default: null
		},
		code: {
			type: String,
			required: true
		}
	},
	paymentTransaction: {
		type: String,
		required: true
	},
	amount: {
		type: Number,
		default: 0
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.REFUND_STATE),
		default: GeneralConstant.REFUND_STATE.PENDING
	},
	description: {
		type: String,
		default: null
	}
}, {
	collection: 'PaymentRefund',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ paymentTransaction: 1 });
Schema.index({ state: 1 });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
