const MeAPI = require('./crypto.library');

const Private = {
	isSecurity: false,
	privateKey: '',
	publicKey: '',

	async requestPost(url, body, headers = { Authorization: '' }, uri) {
		try {
			console.log(`PG Service --- Request --- url = ${url}${uri} with params = ${JSON.stringify(body)}`);

			const result = {
				code: -1,
				data: {},
				original: null
			};
			const meAPI = new MeAPI({
				url,
				privateKey: this.privateKey,
				publicKey: this.publicKey,
				isSecurity: this.isSecurity,
				'x-api-client': 'app'
			});
			const response = await meAPI.Post(`${uri}`, body, headers.Authorization);
			if (response.code !== 1) {
				return response;
			}
			result.code = response.data.code;
			result.data = response.data.data;
			result.original = response.original;

			console.log(`PG Service --- Response --- url = ${url}${uri}  with data = ${JSON.stringify(result)}`);
			return result;
		} catch (error) {
			return error;
		}
	}
};

class FEService {
	constructor(url = '', accessToken = '', isSecurity = false, privateKey = '', publicKey = '') {
		this.url = url;
		this.accessToken = accessToken;
		Private.isSecurity = isSecurity;
		Private.privateKey = privateKey;
		Private.publicKey = publicKey;
	}

	/**
	 * History của ví
	 * @param payload
	 * @returns {Promise<{code: number, original: null, data: {}}>}
	 */
	async History(payload = {
		method: '',
		src: {},
		dest: {}
	}) {
		const response = {
			code: -1,
			data: null,
			original: null
		};
		try {
			const result = await Private.requestPost(this.url, payload, { Authorization: this.accessToken }, '/v3/History/Service');
			if (result.code === 1000) response.code = 1;
			if (result.code === 1001) response.code = 2;
			response.data = result.data;
			response.original = result.original;

			return response;
		} catch (error) {
			response.data = {
				message: error.message
			};

			return response;
		}
	}
	async Notification(payload = {
		accountId: null,
		message: 'Nội dung',
		extraData: null,
		isBroadcast: false,
		title: 'Tiêu đề'
	}) {
		const response = {
			code: -1,
			data: null,
			original: null
		};
		try {
			const result = await Private.requestPost(this.url, payload, { Authorization: this.accessToken }, '/v3/Local/Notification');
			if (result.code === 1000) response.code = 1;
			if (result.code === 1001) response.code = 2;
			response.data = result.data;
			response.original = result.original;

			return response;
		} catch (error) {
			response.data = {
				message: error.message
			};

			return response;
		}
	}
}

const feService = new FEService(
	process.env.FE_URL,
	process.env.FE_TOKEN,
	process.env.FE_SECURITY,
	process.env.FE_PRIVATE_KEY,
	process.env.FE_PUBLIC_KEY
);

module.exports = feService;
