const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;

// Method lấy thông tin một giao dịch
module.exports = function (meta) {
	const credentials = {};
	try {
		if (!_.isEmpty(_.get(meta, 'security.credentials', {}))) {
			const security = meta.security.credentials;
			credentials.appId = _.get(security, 'appId', null);
			credentials.supplierType = _.get(security, 'supplierType', null);
			credentials.supplierName = _.get(security, 'supplierName', null);
			credentials.extraData = _.get(security, 'extraData', {});
		}

		if (!_.isEmpty(_.get(meta, 'auth.credentials', {}))) {
			const auth = meta.auth.credentials;
			credentials.appId = _.get(auth, 'appId', _.get(meta, 'security.credentials.appId', null));
			credentials.accountId = _.get(auth, 'accountId', _.get(meta, 'security.credentials.accountId', null));
		}
		return credentials;
	} catch (err) {
		if (err.name === 'MoleculerError') {
			throw err;
		}
		throw new MoleculerError(err);
	}
};
