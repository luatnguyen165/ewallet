const moleculerI18n = require('moleculer-i18n-js');
const path = require('path');
const _ = require('lodash');

const WalletLibrary = require('./libraries/wallet.library');

module.exports = {
	name: 'bankLink',
	version: 1,
	mixins: [moleculerI18n],
	i18n: {
		directory: path.join(__dirname, 'locales'),
		locales: ['vi', 'en'],
		defaultLocale: 'vi'
	},

	/**
	 * Settings
	 */
	settings: {},
	/**
	 * Dependencies
	 */
	dependencies: [],
	/**
	 * Actions
	 */
	actions: {
		bankLink: {
			rest: {
				method: 'POST',
				fullPath: '/ewallet/link',
				auth: false
			},
			params: {
				body: {
					$$type: 'object',
					linkType: 'string',
					linkInfo: {
						$$type: 'object',
						cardNumber: 'string|optional',
						cardHolder: 'string|optional',
						issuedAt: 'string|optional',
						expiredAt: 'string|optional',
						accountNumber: 'string|optional',
						accountName: 'string|optional'
					},
					accountId: 'number|optional',
					phone: 'string|optional',
					linkedId: 'string',
					amount: 'number|optional',
					transactionId: 'string|optional'
				}
			},
			handler: require('./actions/linked.action')
		},
		bankUnlink: {
			rest: {
				method: 'POST',
				fullPath: '/ewallet/unlink',
				auth: false
			},
			params: {
				body: {
					$$type: 'object',
					accountId: 'number|optional',
					phone: 'string|optional',
					linkedId: 'string',
					note: 'string|optional',
					transactionId: 'string|optional'
				}
			},
			handler: require('./actions/unlinked.action')
		},
		accountInfo: {
			rest: {
				method: 'GET',
				fullPath: '/ewallet',
				auth: false
			},
			params: {},
			handler: require('./actions/accountInfo.action')
		},
		linkedInfo: {
			rest: {
				method: 'POST',
				fullPath: '/ewallet/linked/checkAvailable',
				auth: false
			},
			params: {
				body: {
					$$type: 'object',
					phone: 'string|optional',
					linkedId: 'string|optional',
					accountId: 'number|optional',
					transactionId: 'string|optional'
				}
			},
			handler: require('./actions/checkLinked.action')
		},
		deposit: {
			rest: {
				method: 'POST',
				fullPath: '/ewallet/deposit',
				auth: false
			},
			params: {
				body: {
					$$type: 'object',
					phone: 'string',
					linkedId: 'string',
					amount: 'number',
					bankRefCode: 'string',
					transactionId: 'string',
				}
			},
			handler: require('./actions/deposit.action')
		}
	},
	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		GetAuthInfo: require('./methods/getAuthInfo.method'),
		PhoneUtility: require('./methods/phoneUtility.method'),
		CardDescription: require('./methods/cardDescription.method')
	},

	created(broker) {
		_.set(this, 'WalletLibs', WalletLibrary({ logger: this.logger.info }));
	}
};
