// Libraries
const _ = require('lodash');

// Constants
const bankLinkConstant = require('../constants/bankLink.constant');
const ErrorCodeConstant = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	try {
		// const { bankcode } = ctx.params.params;
		const { phone, accountId, linkedId } = _.get(ctx, 'params.body', {});
		const authInfo = this.GetAuthInfo(ctx.meta);
		if (_.get(authInfo, 'extraData.swiftCode', null) === null) {
			return {
				code: ErrorCodeConstant.CHECK_LINKED.AUTH_ERROR,
				message: this.__(ErrorCodeConstant.CHECK_LINKED.AUTH_ERROR.toString())
			};
		}
		if (_.get(authInfo, 'extraData.bankCode', null) === null) {
			return {
				code: ErrorCodeConstant.CHECK_LINKED.AUTH_ERROR,
				message: this.__(ErrorCodeConstant.CHECK_LINKED.AUTH_ERROR.toString())
			};
		}
		const accountObj = {};
		if (phone) {
			const checkPhone = this.PhoneUtility(phone);
			if (!checkPhone.isValid) {
				return {
					code: ErrorCodeConstant.CHECK_LINKED.PHONE_NUMBER_NOT_VALID,
					message: this.__(ErrorCodeConstant.CHECK_LINKED.PHONE_NUMBER_NOT_VALID.toString())
				};
			}

			accountObj.phone = checkPhone.phoneFormatted;
		}
		if (accountId) accountObj.id = accountId;

		if (_.isEmpty(accountObj)) {
			return {
				code: ErrorCodeConstant.CHECK_LINKED.ACCOUNT_INFO_NOT_FOUND,
				message: this.__(ErrorCodeConstant.CHECK_LINKED.ACCOUNT_INFO_NOT_FOUND.toString())
			};
		}

		const accountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [accountObj]);

		if (_.get(accountInfo, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.CHECK_LINKED.ACCOUNT_PAYME_NOT_EXIST,
				message: this.__(ErrorCodeConstant.CHECK_LINKED.ACCOUNT_PAYME_NOT_EXIST.toString())
			};
		}

		const linkedObj = {
			accountId: accountInfo.id,
			'cardInfo.swiftCode': _.get(authInfo, 'extraData.swiftCode')
		};

		if (linkedId) _.set(linkedObj, 'linkedInfo.cardId', linkedId);
		const linkedInfo = await this.broker.call('v1.eWalletLinkedModel.findOne', [linkedObj]);
		if (_.get(linkedInfo, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.UNLINK.IS_NOT_LINKED,
				message: this.__(ErrorCodeConstant.UNLINK.IS_NOT_LINKED.toString())
			};
		}

		let linkType;
		if (linkedInfo.type === bankLinkConstant.LINKED_TYPE.ATM) linkType = bankLinkConstant.LINKED_TYPE_SUPPLIER.CARD;
		if (linkedInfo.type === bankLinkConstant.LINKED_TYPE.BANKING) linkType = bankLinkConstant.LINKED_TYPE_SUPPLIER.ACCOUNT;

		return {
			code: ErrorCodeConstant.ACCOUNT_INFO.SUCCEEDED,
			message: this.__(ErrorCodeConstant.ACCOUNT_INFO.SUCCEEDED.toString()),
			data: {
				linkType,
				accountId: accountInfo.id,
				phone: accountInfo.phone,
				linkedId: linkedInfo.linkedInfo.cardId
			}
		};
	} catch (error) {
		this.logger.info(`[bankLink] Check Linked Account Info Error ${error}`);
		return {
			code: ErrorCodeConstant.ACCOUNT_INFO.FAILED,
			message: this.__(ErrorCodeConstant.ACCOUNT_INFO.FAILED.toString())
		};
	}
};
