// Libraries
const _ = require('lodash');
const feLibrary = require('../libraries/fe.library');

// Constants
const bankLinkConstant = require('../constants/bankLink.constant');
const ErrorCodeConstant = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	try {
		// const { bankcode } = ctx.params.params;
		const payload = ctx.params.query;

		let phone;
		if (payload.phone) {
			const checkPhone = this.PhoneUtility(payload.phone);
			if (!checkPhone.isValid) {
				return {
					code: ErrorCodeConstant.ACCOUNT_INFO.PHONE_NUMBER_NOT_VALID,
					message: this.__(ErrorCodeConstant.ACCOUNT_INFO.PHONE_NUMBER_NOT_VALID.toString())
				};
			}
			phone = checkPhone.phoneFormatted;
		}
		const accountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [{ phone }]);

		if (_.get(accountInfo, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.ACCOUNT_INFO.ACCOUNT_PAYME_NOT_EXIST,
				message: this.__(ErrorCodeConstant.ACCOUNT_INFO.ACCOUNT_PAYME_NOT_EXIST.toString())
			};
		}

		const kycInfo = await this.broker.call('v1.eWalletKycModel.findOne', [{
			accountId: accountInfo.id
		}]);

		return {
			code: ErrorCodeConstant.ACCOUNT_INFO.SUCCEEDED,
			message: this.__(ErrorCodeConstant.ACCOUNT_INFO.SUCCEEDED.toString()),
			data: {
				accountId: accountInfo.id,
				fullname: accountInfo.fullname,
				identifyNumber: kycInfo.identifyNumber,
				isKYC: _.get(accountInfo, 'kyc.state') === bankLinkConstant.ACCOUNT_KYC_STATE.APPROVED
			}
		};
	} catch (error) {
		this.logger.info(`[bankLink] Get Account Info Error ${error}`);
		return {
			code: ErrorCodeConstant.ACCOUNT_INFO.FAILED,
			message: this.__(ErrorCodeConstant.ACCOUNT_INFO.FAILED.toString())
		};
	}
};
