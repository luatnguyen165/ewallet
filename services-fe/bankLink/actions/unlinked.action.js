// Libraries
const _ = require('lodash');

// Constants
const BankLinkConstant = require('../constants/bankLink.constant');
const ErrorCodeConstant = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		const authInfo = this.GetAuthInfo(ctx.meta);

		if (_.get(authInfo, 'extraData.swiftCode', null) === null) {
			return {
				code: ErrorCodeConstant.UNLINK.AUTH_ERROR,
				message: this.__(ErrorCodeConstant.UNLINK.AUTH_ERROR.toString())
			};
		}
		if (_.get(authInfo, 'extraData.bankCode', null) === null) {
			return {
				code: ErrorCodeConstant.UNLINK.AUTH_ERROR,
				message: this.__(ErrorCodeConstant.UNLINK.AUTH_ERROR.toString())
			};
		}

		const accountObj = {};
		if (payload.phone) {
			const checkPhone = this.PhoneUtility(payload.phone);
			if (!checkPhone.isValid) {
				return {
					code: ErrorCodeConstant.UNLINK.PHONE_NUMBER_NOT_VALID,
					message: this.__(ErrorCodeConstant.UNLINK.PHONE_NUMBER_NOT_VALID.toString())
				};
			}
			const phone = checkPhone.phoneFormatted;
			accountObj.phone = phone;
		}
		if (payload.accountId) accountObj.id = payload.accountId;

		if (_.isEmpty(accountObj)) {
			return {
				code: ErrorCodeConstant.REQUEST_FAIL,
				message: 'Thiếu thông tin ví.'
			};
		}
		if (_.isEmpty(accountObj)) {
			return {
				code: ErrorCodeConstant.UNLINK.ACCOUNT_INFO_NOT_FOUND,
				message: this.__(ErrorCodeConstant.UNLINK.ACCOUNT_INFO_NOT_FOUND.toString())
			};
		}

		const accountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [accountObj]);

		if (_.get(accountInfo, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.UNLINK.ACCOUNT_PAYME_NOT_EXIST,
				message: this.__(ErrorCodeConstant.UNLINK.ACCOUNT_PAYME_NOT_EXIST.toString())
			};
		}

		let linkedInfo = await this.broker.call('v1.eWalletLinkedModel.findOne', [{
			accountId: accountInfo.id,
			'cardInfo.swiftCode': authInfo.extraData.swiftCode,
			'linkedInfo.cardId': payload.linkedId,
			state: BankLinkConstant.LINKED_STATE.LINKED
		}]);

		if (_.get(linkedInfo, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.UNLINK.IS_NOT_LINKED,
				message: this.__(ErrorCodeConstant.UNLINK.IS_NOT_LINKED.toString())
			};
		}

		linkedInfo = await this.broker.call('v1.eWalletLinkedModel.findOneAndUpdate', [{
			accountId: accountInfo.id,
			'cardInfo.swiftCode': authInfo.extraData.swiftCode,
			'linkedInfo.cardId': payload.linkedId,
			state: BankLinkConstant.LINKED_STATE.LINKED
		}, {
			state: BankLinkConstant.LINKED_STATE.UNLINK
		}, {
			new: true
		}]);

		if (_.get(linkedInfo, 'state', null) !== BankLinkConstant.LINKED_STATE.UNLINK) {
			return {
				code: ErrorCodeConstant.UNLINK.FAILED,
				message: this.__(ErrorCodeConstant.UNLINK.FAILED.toString())
			};
		}

		const resultUnregisterMethod = await this.broker.call('v1.eWalletPaymentMethod.unregisterMethod', { registerCode: `${BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK}${linkedInfo.id}` });

		if (resultUnregisterMethod.code !== 301100) {
			this.logger.info(`[BankLink] Unlink Unregister Payment Method Error ${resultUnregisterMethod}`);
		}

		// const dataHistory = {
		// 	method: 'CREATE',
		// 	src: {
		// 		accountId: accountInfo.id,
		// 		appId: authInfo.appId,
		// 		service: {
		// 			type: BankLinkConstant.SERVICES.LINKED,
		// 			id: linkedInfo.id,
		// 			transaction: linkedInfo.transaction,
		// 			state: BankLinkConstant.STATE.SUCCEEDED
		// 		},
		// 		amount: _.get(payload, 'amount', 0),
		// 		total: _.get(payload, 'amount', 0),
		// 		state: BankLinkConstant.STATE.SUCCEEDED,
		// 		description: 'Huỷ liên kết ngân hàng',
		// 		tags: [BankLinkConstant.TAGS.UNLINK]
		// 	}
		// };

		// let history = await feLibrary.History(dataHistory);
		// if (_.get(history, 'data.history.id', null) === null) {
		// 	return {
		// 		code: ErrorCodeConstant.REQUEST_FAIL,
		// 		message: 'Hủy liên kết thất bại.'
		// 	};
		// }
		return {
			code: ErrorCodeConstant.UNLINK.SUCCEEDED,
			message: this.__(ErrorCodeConstant.UNLINK.SUCCEEDED.toString())
		};
	} catch (error) {
		this.logger.info(`[bankLink] Unlink Error ${error}`);
		return {
			code: ErrorCodeConstant.UNLINK.FAILED,
			message: this.__(ErrorCodeConstant.UNLINK.FAILED.toString())
		};
	}
};
