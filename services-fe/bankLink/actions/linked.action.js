// Libraries
const _ = require('lodash');
const shortid = require('shortid');

// Constants
const BankLinkConstant = require('../constants/bankLink.constant');
const ErrorCodeConstant = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		const authInfo = this.GetAuthInfo(ctx.meta);
		if (_.get(authInfo, 'extraData.swiftCode', null) === null) {
			return {
				code: ErrorCodeConstant.LINKED.AUTH_ERROR,
				message: this.__(ErrorCodeConstant.LINKED.AUTH_ERROR.toString())
			};
		}
		if (_.get(authInfo, 'extraData.bankCode', null) === null) {
			return {
				code: ErrorCodeConstant.LINKED.AUTH_ERROR,
				message: this.__(ErrorCodeConstant.LINKED.AUTH_ERROR.toString())
			};
		}
		let cardInfo = {
			bankCode: _.get(authInfo, 'extraData.bankCode', null),
			swiftCode: _.get(authInfo, 'extraData.swiftCode', null),
			bankName: _.get(authInfo, 'supplierName', null)
		};
		let typeLink;
		switch (payload.linkType) {
			case BankLinkConstant.LINKED_TYPE_SUPPLIER.CARD:
				if (!_.get(payload, 'linkInfo.cardNumber', null)) {
					return {
						code: ErrorCodeConstant.LINKED.BANK_NUMBER_NOT_FOUND,
						message: this.__(ErrorCodeConstant.LINKED.BANK_NUMBER_NOT_FOUND.toString())
					};
				}
				if (!_.get(payload, 'linkInfo.cardHolder', null)) {
					return {
						code: ErrorCodeConstant.LINKED.ACCOUNT_NAME_NOT_FOUND,
						message: this.__(ErrorCodeConstant.LINKED.ACCOUNT_NAME_NOT_FOUND.toString())
					};
				}
				if (!_.get(payload, 'linkInfo.issuedAt', null)) {
					return {
						code: ErrorCodeConstant.LINKED.ISSUED_AT_NOT_FOUND,
						message: this.__(ErrorCodeConstant.LINKED.ISSUED_AT_NOT_FOUND.toString())
					};
				}
				typeLink = BankLinkConstant.LINKED_TYPE.ATM;
				cardInfo = {
					cardNumber: payload.linkInfo.cardNumber,
					cardHolder: payload.linkInfo.cardHolder,
					issuedAt: payload.linkInfo.issuedAt,
					...cardInfo
				};
				break;
			case BankLinkConstant.LINKED_TYPE_SUPPLIER.ACCOUNT:
				if (!_.get(payload, 'linkInfo.accountNumber', null)) {
					return {
						code: ErrorCodeConstant.LINKED.ACCOUNT_NUMBER_NOT_FOUND,
						message: this.__(ErrorCodeConstant.LINKED.ACCOUNT_NUMBER_NOT_FOUND.toString())
					};
				}
				if (!_.get(payload, 'linkInfo.accountName', null)) {
					return {
						code: ErrorCodeConstant.LINKED.ACCOUNT_NAME_NOT_FOUND,
						message: this.__(ErrorCodeConstant.LINKED.ACCOUNT_NAME_NOT_FOUND.toString())
					};
				}
				typeLink = BankLinkConstant.LINKED_TYPE.BANKING;
				cardInfo = {
					accountNumber: payload.linkInfo.accountNumber,
					accountName: payload.linkInfo.accountName,
					...cardInfo
				};
				break;
			default:
				return {
					code: ErrorCodeConstant.LINKED.LINK_TYPE_ERROR,
					message: this.__(ErrorCodeConstant.LINKED.LINK_TYPE_ERROR.toString())
				};
		}

		const accountObj = {};
		if (payload.phone) {
			const checkPhone = this.PhoneUtility(payload.phone);
			if (!checkPhone.isValid) {
				return {
					code: ErrorCodeConstant.LINKED.PHONE_NUMBER_NOT_VALID,
					message: this.__(ErrorCodeConstant.LINKED.PHONE_NUMBER_NOT_VALID.toString())
				};
			}
			const phone = checkPhone.phoneFormatted;
			accountObj.phone = phone;
		}
		if (payload.accountId) accountObj.id = payload.accountId;

		if (_.isEmpty(accountObj)) {
			return {
				code: ErrorCodeConstant.LINKED.ACCOUNT_INFO_NOT_FOUND,
				message: this.__(ErrorCodeConstant.LINKED.ACCOUNT_INFO_NOT_FOUND.toString())
			};
		}

		const accountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [accountObj]);
		if (_.get(accountInfo, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.LINKED.ACCOUNT_PAYME_NOT_EXIST,
				message: this.__(ErrorCodeConstant.LINKED.ACCOUNT_PAYME_NOT_EXIST.toString())
			};
		}

		let linkedInfo = await this.broker.call('v1.eWalletLinkedModel.findOne', [{
			accountId: accountInfo.id,
			'cardInfo.swiftCode': cardInfo.swiftCode,
			state: BankLinkConstant.LINKED_STATE.LINKED
		}]);
		if (_.get(linkedInfo, 'id', null) !== null) {
			return {
				code: ErrorCodeConstant.LINKED.IS_LINKED,
				message: this.__(ErrorCodeConstant.LINKED.IS_LINKED.toString())
			};
		}

		const linkedId = await this.broker.call('v1.walletUuid.pick', {
			prefix: 'LINKED_BANK'
		});

		if (!linkedId) {
			return {
				code: ErrorCodeConstant.LINKED.FAILED,
				message: this.__(ErrorCodeConstant.LINKED.FAILED.toString())
			};
		}

		const transaction = await this.broker.call('v1.walletUuid.pick', {
			prefix: 'LINKED'
		});

		if (!transaction) {
			return {
				code: ErrorCodeConstant.LINKED.FAILED,
				message: this.__(ErrorCodeConstant.LINKED.FAILED.toString())
			};
		}

		const linkedObj = {
			id: linkedId,
			// appId: authInfo.appId || null,
			accountId: accountInfo.id,
			transaction,
			requestId: shortid.generate(),
			cardInfo,
			supplier: cardInfo.bankCode,
			linkedInfo: {
				cardId: payload.linkedId,
				transactionId: payload.transactionId
			},
			type: typeLink,
			state: BankLinkConstant.LINKED_STATE.LINKED
		};

		linkedInfo = await this.broker.call('v1.eWalletLinkedModel.create', [linkedObj]);
		if (_.get(linkedInfo, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.LINKED.FAILED,
				message: this.__(ErrorCodeConstant.LINKED.FAILED.toString())
			};
		}

		const paramsRegister = {
			registerCode: `${BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK}${linkedInfo.id}`,
			accountId: linkedInfo.accountId,
			group: BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK,
			title: _.get(linkedInfo, 'cardInfo.bankName', ''),
			titleLinked: 'Tài khoản liên kết',
			isActive: true,
			paymentInfo: {
				swiftCode: _.get(linkedInfo, 'cardInfo.swiftCode', 'NONE'),
				linkedId: linkedInfo.id,
				cardNumber: _.get(linkedInfo, 'cardInfo.cardNumber', '') || '',
				accountNumber: _.get(linkedInfo, 'cardInfo.accountNumber', '') || '',
				cardHolder: _.get(linkedInfo, 'cardInfo.cardHolder', '') || '',
				routeName: cardInfo.bankCode,
				bankName: _.get(linkedInfo, 'cardInfo.bankName', '')
			},
			description: `Liên kết ngân hàng ${cardInfo.bankCode}`,
			isWithdrawable: true
		};
		let resultCreateMethod;
		try {
			resultCreateMethod = await this.broker.call('v1.eWalletPaymentMethod.registerMethod', paramsRegister);
		} catch (error) {
			this.logger.info(error);
		}

		if (_.get(resultCreateMethod, 'id', null) === null) {
			this.logger.info(`[BankLink] Link Register Payment Method Error ${resultCreateMethod}`);
		}

		const historyObj = {
			accountId: linkedInfo.accountId,
			// appId: authInfo.appId || null,
			service: {
				type: BankLinkConstant.SERVICES.LINKED,
				id: linkedInfo.id,
				transaction: linkedInfo.transaction,
				state: BankLinkConstant.STATE.SUCCEEDED
			},
			amount: _.get(payload, 'amount', 0),
			total: _.get(payload, 'amount', 0),
			state: BankLinkConstant.STATE.SUCCEEDED,
			description: 'Liên kết ngân hàng',
			tags: [BankLinkConstant.TAGS.LINKED]
		};
		const history = await this.broker.call('v1.eWalletHistory.create', { dataHistory: historyObj });

		if (_.get(history, 'data.id', null) === null) {
			this.logger.info(`[BankLink] Linked Create History Error ${JSON.stringify()}`);
		}
		return {
			code: ErrorCodeConstant.LINKED.SUCCEEDED,
			message: this.__(ErrorCodeConstant.LINKED.SUCCEEDED.toString()),
			data: {
				linkedId
			}
		};
	} catch (error) {
		this.logger.info(`[bankLink] Linked Error ${error}`);
		return {
			code: ErrorCodeConstant.LINKED.FAILED,
			message: this.__(ErrorCodeConstant.LINKED.FAILED.toString())
		};
	}
};
