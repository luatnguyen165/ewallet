const _ = require('lodash');
const moment = require('moment');
const Decimal = require('decimal.js');

const ErrorCodeConstant = require('../constants/errorCode.constant');
const DepositConstant = require('../constants/deposit.constant');
const GeneralConstant = require('../constants/general.constant');
const BankLinkConstant = require('../constants/bankLink.constant');
const bankLinkConstant = require('../constants/bankLink.constant');

module.exports = async function (ctx) {
	try {
		const { phone, linkedId, amount } = _.get(ctx, 'params.body');

		const authInfo = this.GetAuthInfo(ctx.meta);

		if (_.get(authInfo, 'extraData.swiftCode', null) === null) {
			return {
				code: ErrorCodeConstant.CHECK_LINKED.AUTH_ERROR,
				message: this.__(ErrorCodeConstant.CHECK_LINKED.AUTH_ERROR.toString())
			};
		}
		if (_.get(authInfo, 'extraData.bankCode', null) === null) {
			return {
				code: ErrorCodeConstant.CHECK_LINKED.AUTH_ERROR,
				message: this.__(ErrorCodeConstant.CHECK_LINKED.AUTH_ERROR.toString())
			};
		}

		const payload = _.get(ctx, 'params.body', {});
		this.logger.info(`[BANKLINK_DEPOSIT] => [payload] => ${JSON.stringify(payload)}`);

		// if (_.get(authInfo, 'id', null) === null) {
		// 	return {
		// 		code: ErrorCodeConstant.REQUEST_FAIL,
		// 		message: 'Không tìm thấy thông tin kết nối'
		// 	};
		// }

		const checkPhone = this.PhoneUtility(phone);
		if (!checkPhone.isValid) {
			return {
				code: ErrorCodeConstant.CHECK_LINKED.PHONE_NUMBER_NOT_VALID,
				message: this.__(ErrorCodeConstant.CHECK_LINKED.PHONE_NUMBER_NOT_VALID.toString())
			};
		}
		const { phoneFormatted } = checkPhone;

		const accountInfo = await this.broker.call('v1.eWalletAccountModel.findOne', [{
			phone: phoneFormatted
		}]);

		// Xet linkedId
		const linkedInfo = await this.broker.call('v1.eWalletLinkedModel.findOne', [{
			'linkedInfo.cardId': linkedId,
			accountId: accountInfo.id,
			state: bankLinkConstant.LINKED_STATE.LINKED
		}]);

		if (_.get(linkedInfo, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.UNLINK.IS_NOT_LINKED,
				message: this.__(ErrorCodeConstant.UNLINK.IS_NOT_LINKED.toString())
			};
		}

		const paymentMethod = await this.broker.call('v1.eWalletPaymentMethodModel.findOne', [{
			'paymentInfo.linkedId': linkedInfo.id
		}]);

		if (_.get(paymentMethod, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.UNLINK.IS_NOT_LINKED,
				message: this.__(ErrorCodeConstant.UNLINK.IS_NOT_LINKED.toString())
			};
		}

		if (_.get(paymentMethod, 'group', '') === BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_CREDIT) {
			return {
				code: ErrorCodeConstant.DEPOSIT.DEPOSIT_NOT_ALLOW,
				message: this.__(ErrorCodeConstant.DEPOSIT.DEPOSIT_NOT_ALLOW.toString())
			};
		}

		// let valueDepositConfig = await this.broker.call('v1.eWalletSettingModel.findOne', [{
		// 	key: 'fee.deposit.config'
		// }]);

		// valueDepositConfig = valueDepositConfig.value;

		// const calFeeResult = await this.CalcFee({
		// 	accountId: accountInfo.id,
		// 	amount,
		// 	feeConfig: valueDepositConfig,
		// 	settingKey: DepositConstant.FEE_SETTING_KEY[settingKey],
		// 	chargeFeeKey: `DepositChargeFee_${accountInfo.id}`,
		// 	filter: {
		// 		accountId: accountInfo.id,
		// 		appId: 'app',
		// 		state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
		// 		'method.group': { $ne: 'CREDIT_CARD' }
		// 	}
		// });
		// if (_.isNumber(_.get(calFeeResult, 'fee')) === false) {
		// 	return {
		// 		code: ErrorCodeConstant.REQUEST_FAIL,
		// 		message: 'Nạp tiền thất bại. Vui lòng thử lại sau.(E02)'
		// 	};
		// }

		// const { fee } = calFeeResult;
		const fee = 0;
		const totalPay = _.toNumber((new Decimal(amount)).add(new Decimal(fee)));

		let transaction = null;
		try {
			transaction = await this.broker.call('v1.walletUuid.pick', { prefix: 'DEPOSIT' });
		} catch (error) {
			return {
				code: ErrorCodeConstant.DEPOSIT.FAILED,
				message: this.__(ErrorCodeConstant.DEPOSIT.FAILED.toString())
			};
		}

		if (_.isNull(transaction)) {
			return {
				code: ErrorCodeConstant.DEPOSIT.FAILED,
				message: this.__(ErrorCodeConstant.DEPOSIT.FAILED.toString())
			};
		}

		const dataDeposit = {
			accountId: linkedInfo.accountId,
			transaction,
			amount,
			fee,
			total: totalPay,
			description: 'Nap tien vao Vi PayME',
			state: DepositConstant.DEPOSIT_STATE.PENDING,
			method: {
				id: _.get(paymentMethod, 'id', 0),
				group: _.get(paymentMethod, 'group', BankLinkConstant.PAYMENT_METHOD_GROUP.LINKED_BANK),
				description: 'Tài khoản liên kết.',
				bankInfo: _.get(paymentMethod, 'paymentInfo', {})
			},
			extraData: {
				bankRefCode: payload.bankRefCode,
				supplierTransaction: payload.transactionId,
				linkedId: linkedInfo.id
			}
		};
		let depositCreated;
		try {
			depositCreated = await this.broker.call('v1.eWalletDepositModel.create', [{ ...dataDeposit }]);
		} catch (error) {
			this.logger.info(`[BankLink] Deposit Create Error ${error}`);
			return {
				code: ErrorCodeConstant.DEPOSIT.FAILED,
				message: this.__(ErrorCodeConstant.DEPOSIT.FAILED.toString())
			};
		}
		if (_.get(depositCreated, 'id', null) === null) {
			this.logger.info(`[BankLink] Deposit Create Error ${JSON.stringify(depositCreated)}`);
			return {
				code: ErrorCodeConstant.DEPOSIT.FAILED,
				message: this.__(ErrorCodeConstant.DEPOSIT.FAILED.toString())
			};
		}

		let paymentTransaction;
		try {
			paymentTransaction = await this.broker.call('v1.walletUuid.pick', { prefix: 'PAYMENT' });
		} catch (error) {
			return {
				code: ErrorCodeConstant.DEPOSIT.FAILED,
				message: this.__(ErrorCodeConstant.DEPOSIT.FAILED.toString())
			};
		}
		const cardDescription = await this.CardDescription({ ...linkedInfo.cardInfo, swiftCode: authInfo.extraData.swiftCode });

		const paymentData = {
			accountId: accountInfo.id,
			transaction: paymentTransaction,
			amount: totalPay,
			state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
			method: 'LINKED',
			service: {
				code: 'DEPOSIT',
				type: 'DEPOSIT',
				transaction
			},
			description: `Nạp tiền từ ${cardDescription}`,
			extraData: {
				bankRefCode: payload.bankRefCode,
				supplierTransaction: payload.transactionId,
				bankInfo: _.get(paymentMethod, 'paymentInfo', {})
			},
			methodData: {
				resolveType: DepositConstant.RESOLVE_TYPE.LINKED,
				linkedId: linkedInfo.id,
				bankInfo: _.get(linkedInfo, 'cardInfo', {})
			},
			supplierTransaction: payload.transactionId
		};

		let paymentCreated = null;
		try {
			paymentCreated = await this.broker.call('v1.eWalletPaymentModel.create', [paymentData]);
		} catch (error) {
			return {
				code: ErrorCodeConstant.DEPOSIT.FAILED,
				message: this.__(ErrorCodeConstant.DEPOSIT.FAILED.toString())
			};
		}
		if (_.get(paymentCreated, 'id', null) === null) {
			return {
				code: ErrorCodeConstant.DEPOSIT.FAILED,
				message: this.__(ErrorCodeConstant.DEPOSIT.FAILED.toString())
			};
		}

		const dataHistory = {
			accountId: linkedInfo.accountId,
			service: {
				type: 'DEPOSIT',
				code: 'DEPOSIT',
				id: depositCreated.id,
				transaction: depositCreated.transaction,
				method: 'DEPOSIT',
				state: DepositConstant.DEPOSIT_STATE.PENDING,
				name: `Nạp tiền từ ${cardDescription}`,
				data: {
					id: depositCreated.id,
					resolveType: 'DepositObject',
					state: DepositConstant.DEPOSIT_STATE.PENDING,
					transaction,
					accountId: accountInfo.id
				}
			},
			payment: {
				id: paymentCreated.id,
				method: DepositConstant.PAYMENT_TYPE.LINKED,
				description: cardDescription,
				state: paymentCreated.state,
				transaction: paymentCreated.transaction
			},
			amount,
			total: totalPay,
			fee,
			state: DepositConstant.DEPOSIT_STATE.PENDING,
			description: 'Nap tien vao Vi PayME',
			changed: '+',
			tags: ['DEPOSIT']
		};

		let historyDepositCreated = null;
		try {
			historyDepositCreated = await this.broker.call('v1.eWalletHistory.create', { dataHistory });
		} catch (error) {
			this.logger.info(`[DEPOSIT_EWALLET] => [CREATE_HISTORY_DEPOSIT_ERROR]: ${error} `);
			return {
				code: ErrorCodeConstant.DEPOSIT.FAILED,
				message: this.__(ErrorCodeConstant.DEPOSIT.FAILED.toString())
			};
		}
		if (_.get(historyDepositCreated, 'data.id', null) === null) {
			this.logger.info(`[DEPOSIT_EWALLET] => [CREATE_HISTORY_DEPOSIT_ERROR_2]: ${JSON.stringify(dataHistory)} `);
			return {
				code: ErrorCodeConstant.DEPOSIT.FAILED,
				message: this.__(ErrorCodeConstant.DEPOSIT.FAILED.toString())
			};
		}

		// call supplier

		// Logic of Payment (Update Record)
		let updateHistoryData;
		let depositDataUpdate;
		const response = {
			code: ErrorCodeConstant.DEPOSIT.FAILED,
			message: this.__(ErrorCodeConstant.DEPOSIT.FAILED.toString())
		};

		const paramsRequest = {
			accountId: linkedInfo.accountId,
			amount,
			description: 'Nạp tiền vào ví PayME.',
			referData: depositCreated
		};
		// WalletLibs
		let resultDeposit = null;
		try {
			this.logger.info(`[DEPOSIT] => [DEPOSIT_WALLET_PARAMS]: ${JSON.stringify(paramsRequest)} `);
			resultDeposit = await this.WalletLibs.Deposit(paramsRequest);
			this.logger.info(`[DEPOSIT] => [DEPOSIT_WALLET]: ${JSON.stringify(resultDeposit)} `);

			updateHistoryData = {
				state: DepositConstant.DEPOSIT_STATE.SUCCEEDED,
				'service.state': DepositConstant.DEPOSIT_STATE.SUCCEEDED,
				'service.data.state': DepositConstant.DEPOSIT_STATE.SUCCEEDED,
				balance: resultDeposit.balance
			};
			depositDataUpdate = {
				state: DepositConstant.DEPOSIT_STATE.SUCCEEDED
			};
		} catch (error) {
			depositDataUpdate = {
				state: DepositConstant.DEPOSIT_STATE.FAILED
			};
			this.logger.info(`[DEPOSIT] => [DEPOSIT_WALLET_ERROR]: ${error} `);
		}

		// Neu kq tra ra la false
		if (resultDeposit.succeeded === false) {
			updateHistoryData = {
				...updateHistoryData,
				state: DepositConstant.DEPOSIT_STATE.FAILED,
				'service.state': DepositConstant.DEPOSIT_STATE.FAILED,
				'service.data.state': DepositConstant.DEPOSIT_STATE.FAILED
			};
			response.message = 'Nạp tiền thất bại. Vui lòng thử lại sau.(E07)';
			depositDataUpdate = {
				...depositDataUpdate,
				state: DepositConstant.DEPOSIT_STATE.FAILED
			};
		} else {
			// Neu kq la true thi tra ve response code SUCCEED
			response.data = { transaction };
			response.code = ErrorCodeConstant.DEPOSIT.SUCCEEDED;
			response.message = this.__(ErrorCodeConstant.DEPOSIT.SUCCEEDED.toString());
		}

		depositCreated = await this.broker.call('v1.eWalletDepositModel.findOneAndUpdate', [
			{ id: depositCreated.id },
			depositDataUpdate
		]);

		historyDepositCreated = await this.broker.call('v1.eWalletHistory.update', {
			fields: {
				id: historyDepositCreated.data.id
			},
			updateInfo: updateHistoryData
		});

		return response;
	} catch (error) {
		this.logger.info('[OPENEWALLET] >>> [DEPOSIT] >> [ERROR] >>', JSON.stringify(error));

		return {
			code: ErrorCodeConstant.DEPOSIT.FAILED,
			message: this.__(ErrorCodeConstant.DEPOSIT.FAILED.toString())
		};
	}
};
