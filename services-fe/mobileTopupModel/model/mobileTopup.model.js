const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		default: null
	},
	amount: {
		type: Number,
		required: true
	},
	total: {
		type: Number,
		default: null
	},
	discount: {
		type: Number,
		default: 0
	},
	fee: {
		type: Number,
		default: 0
	},
	quantity: {
		type: Number,
		default: 1
	},
	partnerTransaction: {
		type: String,
		default: null
	},
	supplier: {
		type: String,
		default: null,
		required: true
	},
	transaction: {
		type: String,
		required: true,
		unique: true
	},
	description: {
		type: String,
		default: null
	},
	phone: String,
	type: {
		type: String,
		enum: _.values(GeneralConstant.TOPUP_TYPE)
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.CARD_TRANSACTION_STATE),
		default: null
	},
	supplierResponsed: Object,
	payment: Object,
	extraData: Object,
	isRefunded: {
		type: Boolean,
		default: false
	}
}, {
	collection: 'Service_MobileTopup',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
