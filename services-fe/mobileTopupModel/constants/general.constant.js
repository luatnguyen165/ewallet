module.exports = {
	TOPUP_TYPE: {
		PREPAID: 'PREPAID', // thuê bao di động trả trước
		POSTPAID: 'POSTPAID' // thuê bao di động trả sau
	},
	CARD_TRANSACTION_STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		WAITING: 'WAITING',
		PROCESSING: 'PROCESSING', // tach khac pending để chạy worker OpenApi
		FAILED: 'FAILED',
		CANCELED: 'CANCELED'
	}
};
