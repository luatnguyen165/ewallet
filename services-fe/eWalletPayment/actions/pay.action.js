const _ = require('lodash');
const numeral = require('numeral');

const PaymentConstant = require('../constants/payment.constant');
const GeneralConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;
	const response = {
		state: PaymentConstant.STATE.FAILED,
		message: 'Thanh toán thất bại',
		payment: {
			id: null,
			transaction: null,
			method: null,
			state: null,
			description: ''
		}
	};
	try {
		// STEP 1 : Check accountId is exists
		const payload = _.get(ctx, 'params', {});
		const paymentKey = _.keys(payload.method)[0];

		const accountInfo = await Broker.call('v1.eWalletAccountModel.findOne', [{
			id: payload.accountId
		}, '-_id id phone kyc.state accountType']);

		if (_.get(accountInfo, 'id', null) === null) {
			response.message = 'Thông tin tài khoản không đúng hoặc không tồn tại (PAY_ERR_01)';
			return response;
		}

		// STEP 2 : Check KYC of Account
		if (_.get(accountInfo, 'kyc.state', null) !== GeneralConstant.ACCOUNT_KYC_STATE.APPROVED) {
			response.message = 'Tài khoản chưa định danh không thể thanh toán, vui lòng định danh';
			return response;
		}

		// STEP 3 : Check Quota
		const serviceCode = _.get(payload, 'service.code', null);
		const serviceType = _.get(payload, 'service.type', null);

		try {
			if (serviceType === GeneralConstant.SERVICE_TYPE.WITHDRAW) {
				const checkNewKYC = await Broker.call('v1.eWalletCheckQuota.checkNewKycOverLimit',
					{ accountId: accountInfo.id, amount: payload.amount });

				if (!checkNewKYC.succeeded) {
					response.message = _.get(checkNewKYC, 'message', 'Tài khoản mới định danh không thể sử dụng Ví, vui lòng định danh');
					return response;
				}
				await Broker.call('v1.eWalletCheckQuota.checkWithdrawOverLimit', { accountId: accountInfo.id });
			} else if (serviceType === GeneralConstant.SERVICE_TYPE.DEPOSIT) {
				await Broker.call('v1.eWalletCheckQuota.checkDepositOverLimit', { accountId: accountInfo.id });
			} else {
			// TODO : Check User Limit
			}
		} catch (e) {
			Logger('[PAY] -> [QUOTA] -> [EXCEPTION] ', e);
		}

		let limitSetting = await Broker.call('v1.eWalletSettingModel.findOne', [{
			key: 'limit.param.amount.payment'
		}]);

		if (!_.isNil(limitSetting)) {
			try {
				limitSetting = JSON.parse(limitSetting.value);
			} catch (e) {
				limitSetting = limitSetting.value;
			}

			if (_.get(limitSetting, 'min', false) === false) {
				limitSetting = { min: 0, max: 100000000 };
			}
			if (_.includes([GeneralConstant.ACCOUNT_TYPE.PERSONAL], accountInfo.accountType)) {
				if (payload.amount < limitSetting.min) {
					response.message = `Số tiền thanh toán không được nhỏ hơn ${numeral(limitSetting.min).format('0,0')} đ (PAY_ERR_01)`;
					return response;
				}
				if (payload.amount > limitSetting.max) {
					response.message = `Số tiền thanh toán không vượt quá ${numeral(limitSetting.max).format('0,0')} đ (PAY_ERR_02)`;
					return response;
				}
			}
		}

		// STEP 4 : Create Payment
		let paymentInfo = null;
		const isVerify = paymentKey === 'linked' && !_.isNil(_.get(payload, 'method.linked.otp'));

		if (isVerify) {
			const dataFind = {
				accountId: payload.accountId,
				transaction: payload.transaction,
				'service.transaction': payload.transaction,
				'service.code': serviceCode,
				'service.type': serviceType,
				state: PaymentConstant.STATE.PENDING
			};

			paymentInfo = await Broker.call('v1.eWalletPaymentModel.findOne', [dataFind, '-_id id transaction method']);

			if (_.get(paymentInfo, 'id', null) === null) {
				response.state = PaymentConstant.STATE.FAILED;
				response.message = 'Thanh táon không tồn tại';
				return response;
			}
		} else {
			const nameMethod = this.GetNameMethod(paymentKey);
			const paymentData = {
				accountId: accountInfo.id,
				appId: payload.appId || 0,
				transaction: _.get(payload, 'service.transaction', null),
				amount: payload.amount,
				state: PaymentConstant.STATE.PENDING,
				method: nameMethod,
				service: {
					code: serviceCode,
					type: serviceType,
					transaction: _.get(payload, 'service.transaction', null)
				},
				description: payload.description || 'Thanh toán dịch vụ ví PayME'
			};

			try {
				paymentInfo = await Broker.call('v1.eWalletPaymentModel.create', [paymentData]);
			} catch (error) {
				Logger('[PAYMENT] -> [CREATE_RECORD] -> [EXCEPTION] -> ::', error);
				response.state = PaymentConstant.STATE.FAILED;
				response.message = 'Thanh toán thất bại. Vui lòng thử lại sau (PAY_ERR_03)';
				return response;
			}
		}

		// STEP 5 : Call Into child service
		const childAction = this.GetActionPayment(payload.method);

		// Define Params for Each Method
		// 4 fields below must exits on payload for preference
		let paramsCall = {
			paymentId: paymentInfo.id,
			transaction: paymentInfo.transaction,
			accountId: accountInfo.id,
			amount: payload.amount
		};

		if (paymentKey === PaymentConstant.PAYMENT_PAYLOAD_KEY.WALLET) {
			paramsCall = {
				...paramsCall,
				requireSecurityCode: _.get(payload, 'method.wallet.requireSecurityCode'),
				securityCode: _.get(payload, 'method.wallet.securityCode')
			};
		} else if (paymentKey === PaymentConstant.PAYMENT_PAYLOAD_KEY.BANK_CARD) {
			paramsCall = {
				...paramsCall,
				cardNumber: _.get(payload, 'method.bankCard.cardNumber'),
				cardHolder: _.get(payload, 'method.bankCard.cardHolder'),
				issuedAt: _.get(payload, 'method.bankCard.issuedAt'),
				expiredAt: _.get(payload, 'method.bankCard.expiredAt'),
				notifyUrl: _.get(payload, 'method.bankCard.notifyUrl'),
				envName: _.get(payload, 'method.bankCard.envName')
			};
		} else if (paymentKey === PaymentConstant.PAYMENT_PAYLOAD_KEY.LINKED) {
			paramsCall = {
				...paramsCall,
				phone: accountInfo.phone,
				serviceType,
				linkedId: _.get(payload, 'method.linked.linkedId'),
				otp: _.get(payload, 'method.linked.otp'),
				notifyUrl: _.get(payload, 'method.linked.notifyUrl'),
				envName: _.get(payload, 'method.linked.envName')
			};
		}

		// console.log(paramsCall)
		const payResult = await Broker.call(childAction, paramsCall);

		// console.log(payResult)
		// STEP 6 : Switch Case
		/* -- FAILED Case -- */
		if (payResult.state === PaymentConstant.STATE.FAILED) {
			const updateData = {
				state: PaymentConstant.STATE.FAILED,
				supplierTransaction: _.get(payResult, 'supplierTransaction', null),
				methodData: _.get(payResult, 'methodData', {})
			};

			await Broker.call('v1.eWalletPaymentModel.updateOne', [
				{ id: paymentInfo.id },
				updateData]);

			const paymentObj = {
				id: paymentInfo.id,
				transaction: paymentInfo.transaction,
				method: paymentInfo.method,
				state: PaymentConstant.STATE.FAILED,
				description: _.get(payResult, 'methodDescription', 'Thanh toán')
			};
			response.payment = paymentObj;
			response.message = _.get(payResult, 'message', 'Thanh toán thất bại. Vui lòng thử lại sau (PAY_ERR_04)');
			return response;
		}

		/* -- PENDING Case -- */
		const NEED_VERIFY_STATE = [PaymentConstant.STATE.REQUIRE_OTP,
			PaymentConstant.STATE.REQUIRE_VERIFY, PaymentConstant.STATE.PENDING];
		if (_.includes(NEED_VERIFY_STATE, payResult.state)) {
			const updateData = {
				supplierTransaction: _.get(payResult, 'supplierTransaction', null),
				methodData: _.get(payResult, 'methodData', {})
			};
			await Broker.call('v1.eWalletPaymentModel.updateOne', [
				{ id: paymentInfo.id },
				updateData
			]);

			const paymentObj = {
				id: paymentInfo.id,
				transaction: paymentInfo.transaction,
				method: paymentInfo.method,
				state: PaymentConstant.STATE.PENDING,
				description: _.get(payResult, 'methodDescription', 'Thanh toán')
			};
			response.payment = paymentObj;
			response.message = 'Thanh toán cần xác thực';
			response.state = payResult.state;
			response.data = {
				html: _.get(payResult, 'data.html', null),
				transaction: paymentInfo.transaction
			};

			return response;
		}

		/* -- SUCCEEDED Case -- */
		if (payResult.state === PaymentConstant.STATE.SUCCEEDED) {
			const updateData = {
				supplierTransaction: _.get(payResult, 'supplierTransaction', null),
				methodData: _.get(payResult, 'methodData', {}),
				state: PaymentConstant.STATE.SUCCEEDED
			};
			await Broker.call('v1.eWalletPaymentModel.updateOne', [
				{ id: paymentInfo.id },
				updateData
			]);

			const paymentObj = {
				id: paymentInfo.id,
				transaction: paymentInfo.transaction,
				method: paymentInfo.method,
				state: PaymentConstant.STATE.SUCCEEDED,
				description: _.get(payResult, 'methodDescription', 'Thanh toán')
			};
			response.message = 'Thanh toán thành công';
			response.data ={
				balance: _.get(payResult, 'data.balance', {})
			}
			response.payment = paymentObj;
			response.state = PaymentConstant.STATE.SUCCEEDED;
		}

		// console.log(response);
	} catch (e) {
		Logger(`[PAYMENT] -> [PAY] -> [EXCEPTION] -> :: ${e}`);
	}
	return response;
};
