const _ = require('lodash');
const moment = require('moment');
const RefundConstant = require('../constants/refund.constant');
const PaymentConstant = require('../constants/payment.constant');

const WalletLib = require('../libraries/wallet.library');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;
	const payload = _.get(ctx, 'params', {});

	const response = {
		state: RefundConstant.STATE.FAILED,
		message: 'Hoàn tiền thất bại'
	};

	try {
		// STEP 1 : Find Payment
		const condition = {
			id: payload.paymentId
		};
		const paymentInfo = await Broker.call('v1.eWalletPaymentModel.findOne', [
			condition, '-_id id appId total amount state accountId transaction service description']);

		if (_.get(paymentInfo, 'id', null) === null) {
			response.message = 'Thanh toán không tồn tại';
			return response;
		}

		if (paymentInfo.state !== PaymentConstant.STATE.SUCCEEDED) {
			response.message = 'Thanh toán chưa thành công không thể hoàn tiền';
			return response;
		}

		const checkTransactionRefund = await Broker.call('v1.eWalletPaymentRefundModel.findOne',
			[{ 'service.transaction': paymentInfo.transaction, 'service.code': paymentInfo.service.code }]);

		if (_.get(checkTransactionRefund, 'id', null) !== null) {
			response.message = 'Giao dịch đã được hoàn trước đó';
			return response;
		}

		// STEP 2 : Create Payment
		const refundAmount = paymentInfo.total || paymentInfo.amount;
		const refundInfo = await Broker.call('v1.eWalletPaymentRefundModel.create', [{
			accountId: paymentInfo.accountId,
			service: {
				id: payload.serviceId,
				code: paymentInfo.service.code
			},
			paymentTransaction: paymentInfo.transaction,
			amount: refundAmount,
			state: RefundConstant.STATE.SUCCEEDED,
			description: paymentInfo.description
		}]);

		if (_.get(refundInfo, 'id', null) === null) {
			response.message = 'Lỗi tạo giao dịch hoàn tiền lỗi';
			return response;
		}

		// STEP 3 : Create History
		const dataHistory = {
			accountId: paymentInfo.accountId,
			service: {
				type: RefundConstant.HISTORY.SERVICE_TYPE,
				method: RefundConstant.HISTORY.METHOD,
				id: refundInfo.id,
				transaction: paymentInfo.service.transaction,
				state: RefundConstant.STATE.SUCCEEDED
			},
			amount: refundAmount,
			total: refundAmount,
			state: RefundConstant.STATE.SUCCEEDED,
			appId: paymentInfo.appId,
			description: payload.description || 'Hoàn tiền giao dịch',
			changed: '+',
			tags: [
				RefundConstant.HISTORY.METHOD
			]
		};
		const createdHistory = await Broker.call('v1.eWalletHistoryModel.create', [dataHistory]);

		if (_.get(createdHistory, 'id', null) === null) {
			response.message = 'Lỗi tạo lịch sử giao dịch';
			return response;
		}

		// STEP 3 : Call we.payme.vn to refund into Wallet
		let depositInfo = null;
		try {
			const paramsDeposit = {
				accountId: refundInfo.accountId,
				amount: refundAmount,
				description: 'Hoàn tiền vào ví user',
				referData: refundInfo
			};

			const WalletRequestFunc = WalletLib.bind(this)();
			depositInfo = await WalletRequestFunc.Deposit(paramsDeposit);
		} catch (error) {
			// If deposit into Wallet failed
			// Revert Failed into DB
			const updateRefund = await Broker.call('v1.eWalletPaymentRefundModel.updateOne',
				[{ id: refundInfo.id },
					{
						state: RefundConstant.STATE.FAILED
					}
				]);

			if (updateRefund.nModified < 1) {
				Logger('[REFUND] -> [UPDATE_DB] -> [REFUND_MODEL] -> [FAILED]!!!!');
			}

			const updateHistoryData = {
				// HUGE HACK ?
				publishedAt: moment(new Date()).toISOString(),
				state: RefundConstant.STATE.FAILED
			};

			const updateHistoryrefundInfo = await Broker.call('v1.eWalletHistoryModel.updateOne', [
				{ id: createdHistory.id }, updateHistoryData]);

			if (updateHistoryrefundInfo.nModified < 1) {
				Logger('[REFUND] -> [UPDATE_DB] -> [HISTORY_MODEL] -> [FAILED]!!!!');
			}
			response.message = 'Hoàn tiền vào ví thất bại';
			return response;
		}

		const updateHistoryData = {
			publishedAt: moment(new Date()).toISOString(),
			balance: depositInfo.balance
		};
		const updateHistoryRefund = await Broker.call('v1.eWalletHistoryModel.updateOne',
			[{ id: createdHistory.id }, updateHistoryData]);

		if (updateHistoryRefund.nModified < 1) {
			response.message = 'Hoàn tiền thất bại (ERR_01)';
			return response;
		}

		response.message = 'Hoàn tiền thành công';
		response.state = RefundConstant.STATE.SUCCEEDED;
		return response;
	} catch (error) {
		Logger('[REFUND] -> [EXCEPTION] -> :: ', error);
		response.state = RefundConstant.STATE.FAILED;
		response.message = 'Hoàn tiền thất bại';
		return response;
	}
};
