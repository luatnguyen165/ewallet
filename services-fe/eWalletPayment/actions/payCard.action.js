const _ = require('lodash');

const PaymentConstant = require('../constants/payment.constant');
const NapasLib = require('../libraries/napas.library');

const SupplierConstant = require('../constants/supplier.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;

	const response = {
		resolveType: 'PaymentBankCardResponsed',
		state: PaymentConstant.STATE.FAILED,
		methodDescription: 'Thẻ ATM nội địa',
		message: 'Thanh toán thất bại',
		methodData: {}
	};

	const payload = _.get(ctx, 'params', {});
	try {
		// STEP 1 : Check black list
		const blackList = await Broker.call('v1.eWalletSettingModel.findOne', [{
			key: 'bank.deposit.black'
		}]);

		let configBlackList = {};
		if (_.isString(blackList)) {
			configBlackList = JSON.parse(blackList);
		}
		const listBinBlackList = _.get(configBlackList, 'bankCard', []);

		const cardBin = payload.cardNumber.substring(0, 6);
		if (_.includes(listBinBlackList, cardBin)) {
			response.state = PaymentConstant.STATE.FAILED;
			response.message = 'Ngân hàng Techcombank đang được bảo trì trên hệ thống PayME. Vui lòng thử lại sau';
			return response;
		}

		let paymentNapasLimit = await Broker.call('v1.eWalletSettingModel.findOne', [{
			key: 'limit.deposit.linked.gateway.napas'
		}]);

		if (_.isNil(paymentNapasLimit)) paymentNapasLimit = 50000000;
		if (payload.amount > paymentNapasLimit) {
			response.message = 'Số tiền vượt hạn mức giao dịch';
			return response;
		}

		const { transaction } = payload;

		const bankInfo = {
			cardNumber: payload.cardNumber,
			cardHolder: this.RemoveUnicode(payload.cardHolder),
			issuedAt: payload.issuedAt
		};

		const prefix = bankInfo.cardNumber.substring(0, 6);

		const bankCodeSupportList = await Broker.call('v1.eWalletBankCodeModel.findMany',
			[{ isActive: true }]);
		_.forEach(bankCodeSupportList, (bankConfig) => {
			if (_.get(bankConfig, 'card.atm.prefix', false) && _.isEqual(bankConfig.card.atm.prefix, prefix)) {
				bankInfo.swiftCode = bankConfig.swiftCode;
				bankInfo.bankName = bankConfig.vi;
				bankInfo.shortName = bankConfig.shortName;
			}
		});

		if (!bankInfo.swiftCode) {
			response.message = 'Ngân Hàng chưa được hỗ trợ';
			return response;
		}

		const paymentCardNapas = await Broker.call('v1.eWalletPaymentCardNapasModel.create', [{
			accountId: payload.accountId,
			paymentId: payload.paymentId,
			transactionId: transaction,
			bankInfo,
			amount: payload.amount,
			total: payload.amount,
			fee: 0,
			description: 'Thanh toán từ Napas',
			state: PaymentConstant.STATE.PENDING,
			supplierResponsed: [],
			extraData: payload.extraData
		}]);

		let redirectUrl = _.get(payload, 'redirectUrl', '') || '';
		if (redirectUrl === '') {
			redirectUrl = 'https://payme.vn';
		}

		const notifyUrl = `${process.env.DOMAIN_URL}/be/ewallet/card/ipn`;
		const params = {
			cardNumber: payload.cardNumber,
			cardHolder: _.toUpper(bankInfo.cardHolder),
			issuedAt: payload.issuedAt,
			amount: payload.amount,
			accountId: payload.accountId,
			transaction: payload.transaction,
			redirectUrl, // redirect url after user type bank's otp
			notifyUrl,
			clientIp: _.get(payload, 'clientIp', ''),
			envName: _.get(payload, 'envName', 'MobileApp') // MobileApp is default Env
		};

		const NapasLibFunc = NapasLib.bind(this)();
		const napasResponsed = await NapasLibFunc.cardPay(params);

		if (napasResponsed.state !== SupplierConstant.STATE.REQUIRE_VERIFY) {
			const alertMsg = `❗Gửi yêu cầu thanh toán Napas Card thất bại!
          	\nParam: ${JSON.stringify(params)}
          	\nResponse: ${JSON.stringify(napasResponsed)}`;

			// TODO : later
			// NotifyService.SendAlertNotify(alertMsg, [], ['api']);

			// In case of timeout payment, dont set the State is FAILED yet
			await Broker.call('v1.eWalletPaymentCardNapasModel.updateOne', [
				{ id: paymentCardNapas.id },
				{
					// state: FrontendConstant.BANK_STATE.FAILED,
					supplierResponsed: [JSON.stringify(napasResponsed)],
					supplierTransaction: _.get(napasResponsed.data, 'order_id', null)
				}
			]);

			response.state = PaymentConstant.STATE.FAILED;
			response.message = _.get(napasResponsed, 'data.message', 'Thanh toán thất bại!');

			if (response.message === 'ECONNABORTED') {
				_.set(response, 'message', 'Không nhận được phản hồi từ ngân hàng, vui lòng thử lại sau');
			}
			return response;
		}

		const updateResult = await Broker.call('v1.eWalletPaymentCardNapasModel.updateOne', [
			{ id: paymentCardNapas.id },
			{
				supplierResponsed: [JSON.stringify(napasResponsed.original)],
				supplierTransaction: _.get(napasResponsed.data, 'order_id', null)
			}
		]);

		if (updateResult.nModified < 1) {
			response.state = PaymentConstant.STATE.FAILED;
			response.message = 'Thanh toán thất bại (ERR_01)';
			return response;
		}

		response.data = {
			html: _.get(napasResponsed, 'data.data', null)
		};
		response.message = _.get(napasResponsed, 'data.message', 'Thanh toán cần xác thực');
		response.state = PaymentConstant.STATE.REQUIRE_VERIFY;
		response.methodData = {
			resolveType: PaymentConstant.RESOLVE_TYPE.BANK_CARD,
			paymentBankCardId: paymentCardNapas.id,
			issuedDate: bankInfo.issuedAt,
			cardHolder: bankInfo.cardHolder,
			cardNumber: bankInfo.cardNumber
		};
		response.paymentInfo = bankInfo;
		response.supplierTransaction = _.get(napasResponsed.data, 'order_id', null);
	} catch (e) {
		response.state = PaymentConstant.STATE.FAILED;
		response.message = 'Thanh toán thất bại (ERR_02)';
		Logger('[PAYMENT] -> [PAY] -> [ATM_CARD] -> [EXCEPTION] -> :: ', e);
	}

	return response;
};
