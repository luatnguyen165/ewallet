const _ = require('lodash');
const numeral = require('numeral');

const PaymentConstant = require('../constants/payment.constant');
const GeneralConstant = require('../constants/general.constant');

const WalletLib = require('../libraries/wallet.library');
/**
 *  This action will excute the payment which using method PayME Wallet
 */

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;

	const payload = _.get(ctx, 'params', {});
	const response = {
		resolveType: 'PaymentWalletResponsed',
		state: PaymentConstant.STATE.FAILED,
		methodDescription: 'Ví PayME',
		message: null,
		methodData: {}
	};
	try {
		// STEP 1 : Check the min max Amount which PayME Wallet method can Pay
		const accountInfo = await Broker.call('v1.eWalletAccountModel.findOne', [{
			id: payload.accountId
		}, '-_id id fullname accountType']);

		let limitWithdrawKey = await Broker.call('v1.eWalletSettingModel.findOne', [{
			key: 'limit.param.amount.withdraw'
		}]);

		limitWithdrawKey = limitWithdrawKey.value;
		if (_.get(limitWithdrawKey, 'min', false) === false) limitWithdrawKey = { min: 0, max: 2100000000 };

		if (accountInfo.accountType !== GeneralConstant.ACCOUNT_TYPE.BUSINESS) {
			if (payload.amount < limitWithdrawKey.min) {
				response.message = `Số tiền thanh toán không được nhỏ hơn ${numeral(limitWithdrawKey.min).format('0,0')} đ`;
				return response;
			}
			if (payload.amount > limitWithdrawKey.max) {
				response.message = `Số tiền thanh toán không vượt quá ${numeral(limitWithdrawKey.max).format('0,0')} đ`;
				return response;
			}
		}

		// STEP 2: If requireSecurityCode is true, Check it
		if (_.get(payload, 'requireSecurityCode', true) === true) {
			const securityCode = _.get(payload, 'securityCode', null);
			if (!securityCode) {
				response.message = 'Mã bảo mật không chính xác.';
				return response;
			}

			const securityCodeInfo = await Broker.call('v1.eWalletSecurityCodeModel.findOne', [{
				accountId: payload.accountId,
				code: securityCode
			}, '-_id id']);
			if (_.get(securityCodeInfo, 'id', null) === null) {
				response.message = 'Mã bảo mật không chính xác.';
				return response;
			}

			await Broker.call('v1.eWalletSecurityCodeModel.delete', [{
				id: securityCodeInfo.id
			}]);

			let limitTouchId = await Broker.call('v1.eWalletSettingModel.findOne', [{
				key: 'limit.payment.touchId'
			}]);
			limitTouchId = limitTouchId.value;

			// if User using touch Id or FaceId to Pay, check the amount of Payment
			// If it excceed the allowed amount, return FAILED in Payment Module
			if (_.get(limitTouchId, 'min', false) === false) limitTouchId = { min: 0, max: 5000000 };
			if (payload.amount >= limitTouchId.max
                && securityCodeInfo.source !== PaymentConstant.SECURITY_CODE_SOURCE.PASSWORD) {
				const showedMoney = Math.round(limitTouchId.max).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
				response.message = `Với giao dịch từ ${showedMoney}đ vui lòng xác thực bằng mật khẩu`;
				return response;
			}
		}

		// STEP 3: Check the remaining amount of User
		const paramsRequestInfoWallet = {
			accountId: accountInfo.id
		};

		const WalletRequestFunc = WalletLib.bind(this)();
		const walletInfo = await WalletRequestFunc.Information(paramsRequestInfoWallet);

		const currentBalance = _.get(walletInfo, 'balance', 0);
		if (currentBalance < payload.amount) {
			response.message = 'Số dư của bạn không đủ để thực hiện giao dịch.';
			response.state = PaymentConstant.STATE.FAILED;
			return response;
		}

		// STEP 4: Create Payment
		const createdPaymentPayme = await Broker.call('v1.eWalletPaymentPayMEModel.create', [{
			accountId: accountInfo.id,
			transactionId: payload.transaction,
			paymentId: payload.paymentId,
			amount: payload.amount,
			state: PaymentConstant.STATE.PENDING,
			description: null,
			balance: {
				before: {
					cash: currentBalance
				},
				after: {
					cash: currentBalance
				}
			}
		}]);
		if (_.get(createdPaymentPayme, 'id', null) === null) {
			response.message = 'Có lỗi xảy ra. Vui lòng thử lại.';
			response.state = PaymentConstant.STATE.FAILED;
			return response;
		}

		const paramsWithdraw = {
			accountId: accountInfo.id,
			amount: payload.amount,
			description: 'Thanh toan tu vi PayME',
			referData: createdPaymentPayme
		};

		// STEP 5: Withdraw the amount in User's PayME Wallet
		const withdrawWallet = await WalletRequestFunc.Withdraw(paramsWithdraw);

		if (_.get(withdrawWallet, 'succeeded', false) === false) {
			response.message = 'Trừ tiền ví thất bại, vui lòng kiểm tra số dư và thử lại.';
			response.state = PaymentConstant.STATE.FAILED;
			return response;
		}
		const balanceWallet = _.get(withdrawWallet, 'balance', {});

		// HACK ?
		// Maybe the update State does not importance
		await Broker.call('v1.eWalletPaymentPayMEModel.updateOne', [
			{ id: createdPaymentPayme.id },
			{ state: PaymentConstant.STATE.SUCCEEDED, 'balance.after': balanceWallet }
		]);

		// Add methodData
		response.methodData = {
			paymentWalletId: createdPaymentPayme.id,
			accountInfo: {
				accountId: _.get(accountInfo, 'id', null),
				fullname: _.get(accountInfo, 'fullname', null)
			},
			balance: balanceWallet
		};

		response.data = {
			balance: balanceWallet
		};
		response.methodDescription = 'Ví PayME';
		response.state = PaymentConstant.STATE.SUCCEEDED;
		response.message = 'Thanh toán thành công.';
	} catch (error) {
		Logger('[PAY] -> [WALLET] -> [EXCEPTION] -> :: ', error);
		response.state = PaymentConstant.STATE.FAILED;
		response.message = 'Thanh toán thất bại';
	}
	return response;
};
