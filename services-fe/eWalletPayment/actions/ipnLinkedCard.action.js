const _ = require('lodash');

const PaymentConstant = require('../constants/payment.constant');

module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;
	try {
		const payload = _.get(ctx, 'params.body', {});

		Logger(`[PAY] -> [IPN] -> [NAPAS_LINKED] -> [PAYLOAD] :: ${JSON.stringify(payload)}`);

		const paymentData = await Broker.call('v1.eWalletPaymentLinkedNapasModel.findOne', [
			{
				transactionId: payload.trans_id,
				supplierTransaction: payload.order_id,
				state: PaymentConstant.STATE.PENDING
			},
			'-_id id accountId supplierResponsed']);

		if (_.get(paymentData, 'id', null) === null) {
			return {
				code: 1001,
				message: 'Không tìm thấy giao dịch'
			};
		}

		const paymentInfo = await Broker.call('v1.eWalletPaymentModel.findOne', [{
			accountId: paymentData.accountId,
			transaction: payload.trans_id,
			supplierTransaction: payload.order_id,
			state: PaymentConstant.STATE.PENDING
		}]);

		if (_.get(paymentInfo, 'id', null) === null) {
			return {
				code: 1001,
				message: 'Không tìm thấy thông tin thanh toán'
			};
		}

		let { supplierResponsed } = paymentData;
		if (_.isArray(supplierResponsed)) {
			supplierResponsed.push(payload);
		} else {
			supplierResponsed = [JSON.stringify(payload)];
		}

		const paymentLinked = await Broker.call('v1.eWalletPaymentLinkedNapasModel.updateOne',
			[
				{
					id: paymentData.id
				},
				{
					state: PaymentConstant.STATE.SUCCEEDED,
					supplierResponsed
				}
			]);

		const filedsNeedUpdate = {
			state: PaymentConstant.STATE.SUCCEEDED,
			method: PaymentConstant.METHOD.BANK_CARD
		};

		const updatedPayment = await Broker.call('v1.eWalletPaymentModel.updateOne', [
			{ id: paymentInfo.id },
			filedsNeedUpdate
		]);

		// if (!_.isObject(updatedPayment)) {
		// 	const alertMsg = `❗UPDATE giao dịch thanh toán bằng ${PaymentConstant.METHOD_CODE.BANK_CARD} thất bại
		//       \nThông tin trước khi Update: ${JSON.stringify(paymentInfo)}`;
		// 	// NotifyService.SendAlertNotify(alertMsg, [], ['api']);
		// }

		const bankInfo = _.get(paymentData, 'bankInfo', {}); // paymentLinked.bankInfo;
		const paymentDescription = await this.PaymentDescription(PaymentConstant.METHOD.LINKED, bankInfo);

		const argsIPN = {
			amount: paymentInfo.amount,
			state: PaymentConstant.STATE.SUCCEEDED,
			serviceType: paymentInfo.service.type,
			serviceCode: paymentInfo.service.code,
			transaction: paymentInfo.transaction,
			method: PaymentConstant.METHOD.LINKED,
			payment: {
				id: paymentInfo.id,
				transaction: payload.trans_id,
				method: PaymentConstant.METHOD.LINKED,
				state: PaymentConstant.STATE.SUCCEEDED,
				description: paymentDescription,
				payment: bankInfo
			}
		};
		// TODO : IPN ve dich vu can thiet
		// const IPN = await PaymentGraphqlModule.IPN(argumentsIPNPayment);

		const actionIPN = this.GetActionIPN(paymentInfo.service.type);

		try {
			await Broker.call(actionIPN, argsIPN, { timeout: 30000 });
		} catch (error) {
			Logger('[PAY] -> [IPN] -> [LINKED_NAPAS] -> [FAILED] -> :: ', error);
		}

		return {
			code: 1000,
			message: 'Thanh toán thành công'
		};
	} catch (error) {
		console.log(error);
		Logger('[PAY] -> [IPN] -> [NAPAS_CARD] -> [EXCEPTION] :: ', error);
	}

	return {
		code: 1001,
		message: 'Thanh toán thất bại (EXCEPTION)'
	};
};
