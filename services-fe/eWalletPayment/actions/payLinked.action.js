const _ = require('lodash');
const moment = require('moment');

const PaymentConstant = require('../constants/payment.constant');
const LinkedCardConstant = require('../constants/linkCard.constant');

const PVCMethod = require('../linkedCardPayMethods/PVCBank.method');
const NapasMethod = require('../linkedCardPayMethods/Napas.method');
const OCBMethod = require('../linkedCardPayMethods/OCB.method');
const BIDVMethod = require('../linkedCardPayMethods/BIDV.method');
const VietinMethod = require('../linkedCardPayMethods/Vietin.method');
/**
 *  This action will excute the payment which using method linked card
 */
module.exports = async function (ctx) {
	const Broker = this.broker;
	const Logger = this.logger.info;
	const RedisCache = this.RedisCache();

	const response = {
		resolveType: 'PaymentLinkedResponsed',
		state: PaymentConstant.STATE.FAILED,
		methodDescription: 'Thẻ liên kết',
		message: 'Thất bại',
		methodData: {}
	};

	const payload = ctx.params;
	try {
		// STEP 1: Check this account is locked bacause of failed bank otp too many times
		const keyCountFailedTransaction = `BankTransactionFailedOTP-${payload.accountId}`;
		let countFailedTransaction = await RedisCache.get({ key: keyCountFailedTransaction });
		countFailedTransaction = _.parseInt(countFailedTransaction) || 0;

		if (countFailedTransaction >= 3) {
			const ttl = await RedisCache.getTTL({ key: keyCountFailedTransaction });
			let d = moment.duration(ttl, 'seconds').format('m:ss');
			if (d < 60) d = `${d} giây`;
			else {
				d = d.split(':');
				if (d[1] !== '00') d = `${d[0]} phút ${d[1]} giây`;
				else d = `${d[0]} phút`;
			}
			response.message = `Bạn không thể thao tác Thanh toán! Vui lòng thử lại sau ${d} hoặc liên hệ CSKH để được hỗ trợ`;
			return response;
		}

		// STEP 2: Check this linkedId is legit
		const conditionQuery = {
			id: payload.linkedId,
			accountId: payload.accountId
		};
		const linkedInfo = await Broker.call('v1.eWalletLinkedModel.findOne',
			[conditionQuery, '_id id state cardInfo linkedInfo supplier']);

		if (_.get(linkedInfo, 'id', null) === null
        || linkedInfo.state !== LinkedCardConstant.STATE.LINKED) {
			response.state = PaymentConstant.STATE.FAILED;
			response.message = 'Liên kết không tồn tại';
			return response;
		}

		// STEP 3 : Check black list
		const bankInfo = _.get(linkedInfo, 'cardInfo', null);
		bankInfo.linkedId = linkedInfo.id;
		const cardNumber = _.get(bankInfo, 'cardNumber', null);

		const blackList = await Broker.call('v1.eWalletSettingModel.findOne', [{
			key: 'bank.deposit.black'
		}]);

		let configBlackList = {};
		if (_.isString(blackList)) {
			configBlackList = JSON.parse(blackList);
		}
		const listBinBlackList = _.get(configBlackList, 'bankCard', []);

		const cardBin = cardNumber.substring(0, 6);
		if (_.includes(listBinBlackList, cardBin)) {
			response.state = PaymentConstant.STATE.FAILED;
			response.message = `Ngân hàng ${bankInfo.bankName || bankInfo.bankCode} đang được bảo trì trên hệ thống PayME. Vui lòng thử lại sau`;
			return response;
		}

		const isVerify = _.isNil(payload.otp) === false;

		// STEP 4 : Based on Supplier, Switch case one to one supplier to handle appropiately
		let supplierResponse = {};
		switch (linkedInfo.supplier) {
			// PVComBank
			case LinkedCardConstant.SUPPLIER.PVCOMBANK: {
				const PVCFuncs = PVCMethod.bind(this)();
				if (!isVerify) {
					const params = {
						accountId: payload.accountId,
						transaction: payload.transaction,
						amount: payload.amount,
						cardInfo: bankInfo,
						paymentId: payload.paymentId,
						cardId: _.get(linkedInfo, 'linkedInfo.cardId', null)
					};
					supplierResponse = await PVCFuncs.Pay(params);
				} else {
					const params = {
						accountId: payload.accountId,
						transaction: payload.transaction,
						paymentId: payload.paymentId
					};
					supplierResponse = await PVCFuncs.VerifyPay(params);
				}
				break;
			}

			// OCB
			case LinkedCardConstant.SUPPLIER.OCBBANK: {
				const OCBFuncs = OCBMethod.bind(this)();
				if (!isVerify) {
					const params = {
						accountId: payload.accountId,
						phone: payload.phone,
						transaction: payload.transaction,
						paymentId: payload.paymentId,
						amount: payload.amount,
						cardInfo: bankInfo,
						cardId: _.get(linkedInfo, 'linkedInfo.cardId', null)
					};
					supplierResponse = await OCBFuncs.Pay(params);
				} else {
					const params = {
						accountId: payload.accountId,
						transaction: payload.transaction,
						paymentId: payload.paymentId
					};
					supplierResponse = await OCBFuncs.VerifyPay(params);
				}
				break;
			}
			// Napas
			case LinkedCardConstant.SUPPLIER.NAPAS_GATEWAY: {
				const params = {
					accountId: payload.accountId,
					transaction: payload.transaction,
					paymentId: payload.paymentId,
					amount: payload.amount,
					cardInfo: bankInfo,
					clientIp: _.get(payload, 'clientIp', ''),
					envName: _.get(payload, 'envName', 'MobileApp'),
					serviceType: _.get(payload, 'serviceType', ''),
					redirectUrl: _.get(payload, 'redirectUrl', ''),
					cardId: _.get(linkedInfo, 'linkedInfo.cardId', null)
				};
				const NapasFuncs = NapasMethod.bind(this)();
				supplierResponse = await NapasFuncs.Pay(params);
				break;
			}

			// Vietin Bank
			case LinkedCardConstant.SUPPLIER.VIETINBANK: {
				const VietinFuncs = VietinMethod.bind(this)();

				if (!isVerify) {
					const params = {
						accountId: payload.accountId,
						transaction: payload.transaction,
						amount: payload.amount,
						cardInfo: bankInfo,
						paymentId: payload.paymentId,
						cardId: _.get(linkedInfo, 'linkedInfo.cardId', null)
					};
					supplierResponse = await VietinFuncs.Pay(params);
				} else {
					const params = {
						accountId: payload.accountId,
						transaction: payload.transaction,
						paymentId: payload.paymentId
					};
					supplierResponse = await VietinFuncs.VerifyPay(params);
				}

				break;
			}

			// SacomBank
			case LinkedCardConstant.SUPPLIER.BIDVBANK: {
				const BIDVFuncs = BIDVMethod.bind(this)();

				if (!isVerify) {
					const params = {
						accountId: payload.accountId,
						transaction: payload.transaction,
						amount: payload.amount,
						cardInfo: bankInfo,
						paymentId: payload.paymentId
					};
					supplierResponse = await BIDVFuncs.Pay(params);
				} else {
					const params = {
						accountId: payload.accountId,
						transaction: payload.transaction,
						paymentId: payload.paymentId
					};
					supplierResponse = await BIDVFuncs.VerifyPay(params);
				}
				break;
			}

			default: break;
		}

		// STEP 5: Return Data into Payment
		response.methodData = {
			paymentLinkedId: supplierResponse.paymentLinkedId,
			linkedId: linkedInfo.id,
			bankInfo: {
				swiftCode: bankInfo.swiftCode,
				cardNumber: bankInfo.cardNumber,
				bankAccountNumber: bankInfo.accountNumber,
				cardHolder: bankInfo.cardHolder
			}
		};
		response.data = {
			html: _.get(supplierResponse, 'html', null),
			supplierTransaction: _.get(supplierResponse, 'supplierTransaction', '')
		};
		response.state = supplierResponse.state;
		response.message = _.get(supplierResponse, 'message', '');

		// Delete key count OTP
		if (response.state === PaymentConstant.STATE.SUCCEEDED) {
			await RedisCache.delete({ key: keyCountFailedTransaction });
		}
	} catch (error) {
		Logger('[PAYMENT] -> [PAY] -> [LINKED] -> [EXCEPTION] -> :: ', error);
		response.state = PaymentConstant.STATE.FAILED;
		response.message = 'Thanh toán thất bại';
	}
	return response;
};
