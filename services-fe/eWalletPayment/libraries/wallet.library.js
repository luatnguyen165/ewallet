const Axios = require('axios');
const _ = require('lodash');

module.exports = function () {
	const url = process.env.WALLET_URL;
	const Authorization = process.env.WALLET_TOKEN;

	const Logger = this.logger.info;

	async function PostRequest(urlRequest, payload) {
		try {
			const response = await Axios.post(urlRequest, payload, {
				headers: {
					Authorization
				},
				timeout: 50000
			});
			return response;
		} catch (error) {
			const data = {
				code: -2,
				data: {
					code: error.code,
					message: error.code === 'ECONNABORTED' ? 'ECONNABORTED' : error.message
				},
				original: error
			};
			return data;
		}
	}
	async function GetRequest(urlRequest) {
		try {
			const response = await Axios.get(urlRequest, {
				headers: {
					Authorization
				},
				timeout: 50000
			});
			return response;
		} catch (error) {
			const data = {
				code: -2,
				data: {
					code: error.code,
					message: error.code === 'ECONNABORTED' ? 'ECONNABORTED' : error.message
				},
				original: error
			};
			// if (_.get(error, 'response', null) !== null) { return error.response; }
			// timeout
			return data;
		}
	}
	return {
		async Deposit(params) {
			const response = {
				successed: false,
				message: 'Nạp tiền vào Ví thất bại.',
				balance: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/Wallet/Deposit`;
			const body = {
				accountId: params.accountId,
				amount: params.amount,
				description: params.description,
				referData: params.referData
			};

			try {
				const result = await PostRequest(urlRequest, body);
				response.supplierResponse = JSON.stringify(result.data);

				const bodyResponse = result.data;
				if (bodyResponse.code !== 1000) {
					Logger(`[WALLET_LIBRARY] -> [Deposit] -> [Params] : ${JSON.stringify(body)}`);
					Logger(`[WALLET_LIBRARY] -> [Deposit] -> [Response] : ${JSON.stringify(result.data)}`);
					response.message = _.get(bodyResponse, 'data.message', response.message);
					return response;
				}
				response.successed = true;
				response.message = 'Nạp tiền vào ví PayME thành công.';
				response.balance = _.get(bodyResponse, 'data.balance', null);
				return response;
			} catch (error) {
				Logger(`[WALLET_LIBRARY] -> [WALLET_LIBRARY]: ${urlRequest} -> [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async Information(params) {
			const response = {
				successed: false,
				message: 'Lấy thông tin Ví thất bại.',
				balance: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/Local/Wallet/${params.accountId}`;

			try {
				const result = await GetRequest(urlRequest);

				const bodyResponse = result.data;
				if (bodyResponse.code !== 1000) {
					Logger(`[WALLET_LIBRARY] -> [Info] -> [Params] : ${params.accountId}`);
					Logger(`[WALLET_LIBRARY] -> [Info] -> [Response] : ${JSON.stringify(result.data)}`);
					response.message = _.get(bodyResponse, 'data.message', response.message);
					return response;
				}
				response.succeeded = true;
				response.message = 'Lấy thông tin ví PayME thành công.';
				response.balance = _.get(bodyResponse, 'data.amount', 0);
				return response;
			} catch (error) {
				Logger(`[WALLET_LIBRARY] -> [WALLET_LIBRARY]: ${urlRequest} -> [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async Withdraw(params) {
			const response = {
				successed: false,
				message: 'Trừ tiền Ví thất bại.',
				balance: null,
				supplierResponse: null
			};
			const urlRequest = `${url}/Wallet/Withdraw`;
			const body = {
				accountId: params.accountId,
				amount: params.amount,
				description: params.description,
				referData: params.referData
			};

			try {
				const result = await PostRequest(urlRequest, body);

				response.supplierResponse = JSON.stringify(result.data);

				const bodyResponse = result.data;
				if (bodyResponse.code !== 1000) {
					Logger(`[WALLET_LIBRARY] -> [Withdraw] -> [Response] : ${JSON.stringify(result.data)}`);
					Logger(`[WALLET_LIBRARY] -> [Withdraw] -> [Params] : ${JSON.stringify(body)}`);
					response.message = _.get(bodyResponse, 'data.message', response.message);
					return response;
				}
				response.succeeded = true;
				response.message = 'Rút tiền từ ví PayME thành công.';
				response.balance = _.get(bodyResponse, 'data.balance', null);
				return response;
			} catch (error) {
				Logger(`[WALLET_LIBRARY] -> [WALLET_LIBRARY]: ${urlRequest} -> [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}
	};
};
