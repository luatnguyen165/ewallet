const Axios = require('axios');
const _ = require('lodash');
const SupplierConstant = require('../constants/supplier.constant');

module.exports = function () {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.VIETINBANK_TOKEN;

	const Logger = this.logger.info;

	async function RequestPost(urlRequest, payload) {
		try {
			const response = await Axios.post(urlRequest, payload, {
				headers: {
					Authorization
				},
				timeout: 50000
			});
			return response;
		} catch (error) {
			const data = {
				code: -1,
				data: {
					code: error.code,
					message: error.code === 'ECONNABORTED' ? 'ECONNABORTED' : error.message
				},
				original: error
			};

			if (error.code === 'ECONNABORTED') {
				data.code = -2;
			}
			return data;
		}
	}
	async function GetRequest(urlRequest) {
		try {
			const response = await Axios.get(urlRequest, {
				headers: {
					Authorization
				},
				timeout: 50000
			});
			return response;
		} catch (error) {
			const data = {
				code: -1,
				data: {
					code: error.code,
					message: error.code === 'ECONNABORTED' ? 'ECONNABORTED' : error.message
				},
				original: error
			};
			if (error.code === 'ECONNABORTED') {
				data.code = -2;
			}
			return data;
		}
	}

	return {
		async Pay(payload) {
			const response = {
				state: SupplierConstant.STATE.FAILED,
				message: 'Thanh toán VietinBank thất bại. Vui lòng thử lại sau.',
				data: null,
				original: null
			};
			const urlRequest = `${url}/v1/wallet/vietin/token/pay`;
			const body = {
				trans_id: _.toString(payload.transaction),
				amount: payload.amount,
				account_id: _.toString(payload.accountId),
				card_id: _.toNumber(payload.cardId),
				trans_desc: 'Thanh toan qua vi PayME',
				channel: 'MOBILE',
				language: 'vi',
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};
			try {
				Logger(`[VIETIN] -> [PAY] -> [Params] : ${JSON.stringify({ urlRequest, body })}`);
				const request = await RequestPost(urlRequest, body);

				Logger(`[VIETIN] -> [PAY] -> [Response] : ${JSON.stringify({ urlRequest, result: request.data })}`);

				const result = request.data;
				response.original = result;

				response.transaction = _.get(result, 'data.trans_id', null);
				response.data = _.get(result, 'data', {});
				if (result.code === 2053) {
					response.state = SupplierConstant.STATE.REQUIRE_OTP;
					response.message = _.get(result, 'data.message', 'Thanh toán cần xác thực OTP');
					return response;
				}

				if (result.code === -2) {
					response.state = SupplierConstant.STATE.TIME_OUT;
					response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					return response;
				}

				if (result.code !== 1000) {
					response.message = _.get(request, 'data.message', 'Thanh toán thất bại. Vui lòng thử lại sau.');
					return response;
				}

				response.message = 'Thanh toán thành công.';
				response.state = SupplierConstant.STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[VIETIN] => [PAY] => [EXCEPTION] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async VerifyPay(payload) {
			const response = {
				state: SupplierConstant.STATE.FAILED,
				message: 'Xác thực thanh toán thất bại. Vui lòng thử lại sau.',
				data: null,
				original: null
			};
			const urlRequest = `${url}/v1/wallet/vietin/token/pay/verify_otp`;
			const body = {
				connector_trans_id: payload.transaction,
				otp: payload.otp,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};

			try {
				Logger(`[VIETIN] -> [VERIFY_PAY] -> [PARAMS] : ${JSON.stringify({ urlRequest, body })}`);
				const request = await RequestPost(urlRequest, body);

				const result = request.data;
				response.original = result;
				response.data = _.get(result, 'data', {});
				Logger(`[VIETIN] -> [VERIFY_PAY] -> [Response] : ${JSON.stringify({ urlRequest, result })}`);

				if (result.code !== 1000) {
					response.message = _.get(result, 'data.message', 'Hủy liên kết thẻ thất bại. Vui lòng thử lại sau.');
					return response;
				}
				response.transaction = _.get(result, 'data.trans_id', null);
				response.message = 'Xác thực thanh toán thành công';
				response.state = SupplierConstant.STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[VIETIN] -> [VERIFY_PAY] -> [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}

	};
};
