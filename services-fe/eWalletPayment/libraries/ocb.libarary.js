/* eslint-disable no-unused-expressions */
const _ = require('lodash');
const Moment = require('moment');
const Axios = require('axios');
const Qs = require('qs');
const Jose = require('jose');
const Md5 = require('md5');

const PaymentConstant = require('../constants/payment.constant');
const OCBConstant = require('../constants/ocbbank.constant');
const SupplierConstant = require('../constants/supplier.constant');
// const ResponseCode = require('../constants/code.constant');

module.exports = function () {
	const Logger = this.logger.info;

	const OCBConfig = {
		url: process.env.OCB_URL,
		clientId: process.env.OCB_CLIENT_ID,
		clientSecret: process.env.OCB_CLIENT_SECRET,
		username: process.env.OCB_USER_NAME,
		password: process.env.OCB_PASSWORD,
		debitAccount: process.env.OCB_DEBIT_ACCOUNT,
		privateKey: process.env.OCB_PRIVATE_KEY,
		partnerCode: process.env.OCB_PARTNER_CODE,
		xClientCertificate: process.env.OCB_CLIENT_CERTIFICATE
	};

	const { JWK, JWS, errors } = Jose;

	async function PostForm(url, form) {
		try {
			const response = await Axios.post(url, Qs.stringify(form));
			return response;
		} catch (error) {
			return error;
		}
	}

	async function GetToken(scope = 'OCB') {
		const response = {
			isSuccess: false,
			message: 'Lấy thông tin kết nối thất bại.',
			accessToken: null
		};

		const urlRequest = `${OCBConfig.url}/ocb-oauth-provider/oauth2/token`;
		const objForm = {
			grant_type: 'password',
			username: OCBConfig.username,
			password: OCBConfig.password,
			client_id: OCBConfig.clientId,
			client_secret: OCBConfig.clientSecret,
			scope
		};
		let resultGetToken = null;
		try {
			Logger(`[OCB] -> [GetToken] -> [Params] : ${JSON.stringify(objForm)}`);
			resultGetToken = await PostForm(urlRequest, objForm);
			const accessToken = _.get(resultGetToken, 'data.access_token', null);
			if (resultGetToken.status !== 200) {
				Logger(`[OCB] -> [GetToken] -> [Response] : ${resultGetToken}`);
				return response;
			}

			response.isSuccess = true;
			response.accessToken = accessToken;
			response.message = 'Lấy thông tin kết nối thành công.';
		} catch (err) {
			Logger(`[OCB] -> [GetToken] -> [Error] : ${err}`);
			Logger(`[OCB] -> [GetToken] -> [Response] : ${resultGetToken}`);
		}
		return response;
	}

	function Sign(obj) {
		try {
			const key = Jose.JWK.asKey(OCBConfig.privateKey);
			const payload = JSON.stringify(obj);
			const jwsData = JWS.sign(payload, key, { alg: 'RS256' });
			const xSignature = jwsData.split('.').pop();
			return xSignature;
		} catch (error) {
			Logger(`[OCB] -> [Sign] -> [Params] : ${JSON.stringify(obj)}`);
			Logger(`[OCB] -> [Sign] -> [Error] : ${error}`);
			return false;
		}
	}

	async function PostRequest(url, payload, headers = {}) {
		const response = {
			state: SupplierConstant.STATE.FAILED,
			message: 'Kết nối với nhà cung cấp thất bại.',
			data: null
		};
		let resultRequest = null;
		let urlRequest = null;
		let paramsRequest = null;
		try {
			const resultGetToken = await GetToken();
			if (resultGetToken.isSuccess === false) {
				response.message = resultGetToken.message;
				response.data = resultGetToken;
				return response;
			}
			const { accessToken } = resultGetToken;

			urlRequest = url;
			paramsRequest = {
				...payload,
				trace: {
					clientTransId: Md5(Moment().format('YYYYMMDDHHmmssSSS') + _.random(111111, 999999)),
					clientTimestamp: Moment().format('YYYYMMDDHHmmssSSS')
				}
			};

			const XSignature = Sign(paramsRequest);
			resultRequest = await Axios.post(urlRequest, paramsRequest, {
				timeout: 10000 * 5,
				headers: _.merge({
					Authorization: `Bearer ${accessToken}`,
					'X-Client-Certificate': OCBConfig.xClientCertificate,
					'X-Signature': XSignature,
					'X-IBM-Client-Id': OCBConfig.clientId,
					'X-IBM-Client-Secret': OCBConfig.clientSecret
				}, headers)
			});

			response.isSuccess = true;
			response.resultRequest = _.get(resultRequest, 'data', {});
			response.message = 'Thành công.';
			response.data = {
				status: _.get(resultRequest, 'status', null),
				message: _.get(resultRequest, 'statusText', null),
				data: _.get(resultRequest, 'data.data', {}),
				trace: _.get(resultRequest, 'data.trace', {}),
				original: _.get(resultRequest, 'data', {})
			};
			return response;
		} catch (error) {
			if (_.get(error, 'response', null) !== null) {
				response.data = {
					status: _.get(error, 'response.status', null),
					errCode: _.get(error, 'response.data.error.code', 1),
					errMessage: _.get(error, 'response.statusText', null),
					message: _.get(error, 'response.data.error.details', null),
					original: _.get(error, 'response.data', {})
				};
			}
			if (error.code === 'ECONNABORTED') {
				response.data = error;
				response.state = SupplierConstant.STATE.TIME_OUT;
			}
			Logger(`[OCB] -> [PostRequest] -> [Error] : ${error.message}`);
			Logger(`[OCB] -> [PostRequest] -> [Params] : ${JSON.stringify(paramsRequest)}`);
			return response;
		}
	}

	return {
		async Pay(payload = {}) {
			const response = {
				message: 'Thanh toán OCB thất bại. Vui lòng thử lại sau.',
				transaction: null,
				state: PaymentConstant.STATE.FAILED,
				original: null
			};

			const urlRequest = `${OCBConfig.url}/v1/ewallet/send-authorization-ewallet-payment`;
			const paramsRequest = {
				data: {
					ewallet: {
						mobilePhone: payload.phone,
						ewalletLinkId: payload.cardId,
						partnerCode: OCBConfig.partnerCode,
						tranferAmount: payload.amount
					}
				}
			};

			try {
				Logger(`[OCB] -> [Pay] -> [Params] : ${JSON.stringify(paramsRequest)}`);
				const resultRequest = await PostRequest(urlRequest, paramsRequest);
				Logger(`[OCB] -> [Pay] -> [Response] : ${JSON.stringify(resultRequest.resultRequest)}`);
				response.original = resultRequest.resultRequest;

				const responseStatus = _.get(resultRequest, 'data.status', -1);
				if (resultRequest.isSuccess === false || responseStatus !== 200) {
					const errorCode = _.get(resultRequest, 'data.errCode', 0);
					response.message = OCBConstant[errorCode] || response.message;

					if (resultRequest.state === SupplierConstant.STATE.TIME_OUT) {
						response.state = SupplierConstant.STATE.TIME_OUT;
					}

					return response;
				}
				response.bankTransactionId = _.get(resultRequest, 'data.trace.clientTransId', null);
				response.transaction = _.get(resultRequest, 'data.trace.bankRefNo', null);
				response.message = 'Thanh toán cần xác thực bằng mã OTP ngân hàng.';
				response.state = SupplierConstant.STATE.SUCCEEDED;
				return response;
			} catch (error) {
				Logger(`[OCBBANK] -> [PAY] -> [Error] : ${error}`);
				response.original = JSON.stringify({ errorMessage: error.message });
			}
			return response;
		},
		async VerifyPay(payload = {}) {
			const response = {
				state: SupplierConstant.STATE.FAILED,
				message: 'Xác thực thanh toán OCB thất bại. Vui lòng thử lại sau.',
				original: null
			};

			const urlRequest = `${OCBConfig.url}/v1/ewallet/recharge-ewallet-payment`;
			const paramsRequest = {
				data: {
					rechargeEwallet: {
						bankRefNo: payload.transaction,
						ewalletLinkId: payload.cardId,
						mobilePhone: payload.phone,
						partnerCode: OCBConfig.partnerCode,
						tranferAmount: payload.amount,
						transferDescription: 'Nap tien vao vi PayME',
						OTPCode: payload.otp
					}
				}
			};

			try {
				Logger(`[OCB] -> [VerifyPay] -> [Params] : ${JSON.stringify(paramsRequest)}`);
				const resultRequest = await PostRequest(urlRequest, paramsRequest);
				response.original = JSON.stringify(resultRequest);

				const responseStatus = _.get(resultRequest, 'data.status', -1);
				Logger(`[OCB] -> [VerifyPay] -> [Response] : ${JSON.stringify(resultRequest)}`);

				response.errCode = _.get(resultRequest, 'errCode', null);
				response.transaction = _.get(resultRequest, 'data.trace.bankRefNo', null);
				response.clientTransId = _.get(resultRequest, 'data.trace.clientTransId', null);
				response.bankTransactionId = _.get(resultRequest, 'data.data.transaction.bankTransactionId', null);

				const errorCode = _.toNumber(_.get(resultRequest, 'data.extraData.error.code', 0));
				const wrongOTPCode = [41724, 41725];

				// Wrong OTP or timeout
				if (resultRequest.isSuccess === false || responseStatus !== 200) {
					response.state = SupplierConstant.STATE.FAILED;
					if (_.includes(wrongOTPCode, errorCode)) {
						response.state = SupplierConstant.STATE.INVALID_OTP;
					} else if (resultRequest.state === SupplierConstant.STATE.TIME_OUT) {
						response.state = SupplierConstant.STATE.TIME_OUT;
					}
					response.message = OCBConstant[errorCode];
				} else {
					response.state = SupplierConstant.STATE.SUCCEEDED;
					response.message = 'Xác thực thanh toán thành công';
				}
				return response;
			} catch (error) {
				Logger(`[OCB] -> [Verify_Pay] -> [Exception] : ${error}`);
				response.original = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}
	};
};
