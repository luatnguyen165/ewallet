const Axios = require('axios');
const { toString } = require('lodash');
const SupplierConstant = require('../constants/supplier.constant');

module.exports = function () {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.NAPAS_TOKEN;

	const Logger = this.logger.info;

	async function RequestPost(urlRequest, payload) {
		try {
			const response = await Axios.post(urlRequest, payload, {
				headers: {
					Authorization
				},
				timeout: 50000
			});
			return response;
		} catch (error) {
			const data = {
				code: -1,
				data: {
					code: error.code,
					message: error.code === 'ECONNABORTED' ? 'ECONNABORTED' : error.message
				},
				original: error
			};

			if (error.code === 'ECONNABORTED') {
				data.code = -2;
			}
			return data;
		}
	}
	async function GetRequest(urlRequest) {
		try {
			const response = await Axios.get(urlRequest, {
				headers: {
					Authorization
				},
				timeout: 50000
			});
			return response;
		} catch (error) {
			const data = {
				code: -1,
				data: {
					code: error.code,
					message: error.code === 'ECONNABORTED' ? 'ECONNABORTED' : error.message
				},
				original: error
			};

			if (error.code === 'ECONNABORTED') {
				data.code = -2;
			}
			return data;
		}
	}

	return {
	/**
   *
   * @param {*} params
   *
   * @example
   * {
   *   "transaction": "123",
   *   "cardId": "123",
   *   "amount": 1000,
   *   "accountId": 23333
    *}
   */
		async Pay(payload) {
			const response = {
				state: SupplierConstant.STATE.FAILED,
				data: null,
				original: null
			};
			try {
				const paramsSend = {
					trans_id: payload.transaction,
					request_id: Math.round(Math.random() * 100000000000).toString(),
					card_id: payload.cardId,
					amount: payload.amount,
					account_id: toString(payload.accountId),
					sign_type: 'RSA',
					sign: 'xxxxxxxx'
				};
				Logger('[PVCBANK] -> [PAY] -> [PAYMENT] -> [PAYLOAD] -> :: ', JSON.stringify(paramsSend));
				const request = await RequestPost(`${url}/v1/card/topup`, paramsSend);
				Logger('[PVCBANK] -> [PAY] -> [PAYMENT] -> [RESPONSE] -> :: ', JSON.stringify(request.data));

				const result = request.data;
				response.data = _.get(result, 'data', {});
				if (result.code === 1000) response.state = SupplierConstant.STATE.SUCCEEDED;
				if (result.code === 1008) response.state = SupplierConstant.STATE.REQUIRE_OTP;

				if (result.code === -2) {
					response.state = SupplierConstant.STATE.TIME_OUT;
				}
				response.original = result;

				return response;
			} catch (error) {
				Logger('[PVCBANK] -> [PAY] -> [PAYMENT] -> [EXCEPTION] -> :: ', JSON.stringify(error));
				response.data = {
					message: error.message
				};
				return response;
			}
		},

		/**
   * Thanh toan
   * @param {*} params
   *
   * @example
   * {
        "supplierTransaction": "123",
        "otp": "012345"
     }

	 @returns
   	*/
		async VerifyPay(payload) {
			const response = {
				state: SupplierConstant.STATE.FAILED,
				data: null,
				original: null
			};
			try {
				const params = {
					trans_id: payload.supplierTransaction,
					request_id: Math.round(Math.random() * 100000000000).toString(),
					otp: payload.otp,
					sign_type: 'RSA',
					sign: 'xxxxxxxx'
				};

				Logger('[PVC] -> [VERIFY_PAY] -> [PAYLOAD] -> :: ', JSON.stringify(params));
				const request = await RequestPost(`${url}/v1/card/verify_trans`, params);
				Logger('[PVC] -> [VERIFY_PAY] -> [RESPONSE] -> :: ', JSON.stringify(request.data));

				const result = request.data;
				response.data = _.get(result, 'data', {});
				if (result.code === 1000) response.state = SupplierConstant.STATE.SUCCEEDED;
				if (result.code === 1008) response.state = SupplierConstant.STATE.INVALID_OTP;

				if (result.code === -2) {
					response.state = SupplierConstant.STATE.TIME_OUT;
				}
				response.original = result;

				return response;
			} catch (error) {
				Logger('[PVC] -> [VERIFY_PAYMENT] -> [EXCEPTION] -> :: ', JSON.stringify(error));
				response.data = {
					message: error.message
				};
				return response;
			}
		}
	};
};
