const Axios = require('axios');
const _ = require('lodash');
const SupplierConstant = require('../constants/supplier.constant');

module.exports = function () {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.NAPAS_TOKEN;
	const Logger = this.logger.info;

	async function RequestPost(urlRequest, payload) {
		try {
			const response = await Axios.post(urlRequest, payload, {
				headers: {
					Authorization
				},
				timeout: 50000
			});
			return response;
		} catch (error) {
			const data = {
				code: -1,
				data: {
					code: error.code,
					message: error.code === 'ECONNABORTED' ? 'ECONNABORTED' : error.message
				},
				original: error
			};

			if (error.code === 'ECONNABORTED') {
				data.code = -2;
			}
			return data;
		}
	}
	async function GetRequest(urlRequest) {
		try {
			const response = await Axios.get(urlRequest, {
				headers: {
					Authorization
				},
				timeout: 50000
			});
			return response;
		} catch (error) {
			const data = {
				code: -1,
				data: {
					code: error.code,
					message: error.code === 'ECONNABORTED' ? 'ECONNABORTED' : error.message
				},
				original: error
			};
			if (error.code === 'ECONNABORTED') {
				data.code = -2;
			}

			return data;
		}
	}

	return {

		/**
   * thanh toán dịch vụ bằng thẻ ATM
   * @param {*} params
   */
		async cardPay(params) {
			const response = {
				state: SupplierConstant.STATE.FAILED,
				data: null,
				original: null
			};
			try {
				const payload = {
					card_number: params.cardNumber,
					card_holder: _.toUpper(params.cardHolder),
					issue_date: params.issuedAt,
					amount: params.amount,
					account_id: _.toString(params.accountId),
					trans_id: params.transaction,
					return_url: params.redirectUrl, // redirect url after user type bank's otp
					notify_url: params.notifyUrl,
					client_ip: _.get(params, 'clientIp', ''),
					device_id: '',
					env: _.get(params, 'envName', 'MobileApp'), // MobileApp is default Env
					sign_type: 'RSA',
					sign: 'xxxxxxxx'
				};

				Logger('[NAPAS] -> [PAY_CARD] -> [PAYLOAD] -> :: ', JSON.stringify(payload));
				const request = await RequestPost(`${url}/v1/global/card/pay`, payload);
				Logger('[NAPAS] -> [PAY_CARD] -> [RESPONSE] -> :: ', JSON.stringify(request.data));
				const result = request.data;
				if (result.code === 1000) response.state = SupplierConstant.STATE.REQUIRE_VERIFY;
				if (result.code === -2) response.state = SupplierConstant.STATE.TIME_OUT;
				response.data = result.data;
				response.bankCode = result.code;
				response.original = result;
				return response;
			} catch (error) {
				console.log(error);
				Logger('[NAPAS] -> [PAY_CARD] -> [EXCEPTION] -> :: ', error);
				response.data = {
					message: error.message
				};
				return response;
			}
		},
		/**
  		* thanh toán dịch vụ bằng token (linked Card)
   		* @param {*} params
   		*/
		async TokenPay(payload) {
			const response = {
				state: SupplierConstant.STATE.FAILED,
				data: null,
				original: null
			};
			try {
				const params = {
					amount: payload.amount,
					account_id: _.toString(payload.accountId),
					trans_id: payload.transaction,
					card_id: _.get(payload, 'cardId', ''),
					subject: 'Thanh Toán Dịch vụ',
					return_url: payload.redirectUrl, // redirect sau khi user thanh toán thành công
					notify_url: payload.notifyUrl,
					client_ip: _.get(payload, 'clientIp', ''),
					device_id: '',
					env: _.get(payload, 'envName', 'MobileApp'),
					sign_type: 'RSA',
					sign: 'xxxxxxxx'
				};
				Logger('[NAPAS] -> [PAY_LINKED] -> [PAYLOAD] -> :: ', JSON.stringify(params));
				const request = await RequestPost(`${url}/v1/global/token/pay`, params);
				Logger('[NAPAS] -> [PAY_LINKED] -> [RESPONSE] -> :: ', JSON.stringify(request.data));

				const result = request.data;
				if (result.code === 1000) response.state = SupplierConstant.STATE.REQUIRE_VERIFY;
				if (result.code === -2) response.state = SupplierConstant.STATE.TIME_OUT;
				response.data = result.data;
				response.original = result;

				return response;
			} catch (error) {
				Logger('[NAPAS] -> [PAY_LINKED] -> [EXCEPTION] -> :: ', JSON.stringify(error));
				response.data = {
					message: error.message
				};
				return response;
			}
		}
	};
};
