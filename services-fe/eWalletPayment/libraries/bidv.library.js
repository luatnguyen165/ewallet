const Axios = require('axios');
const _ = require('lodash');
const SupplierConstant = require('../constants/supplier.constant');

module.exports = function () {
	const url = process.env.BANK_CONNECTOR_URL;
	const Authorization = process.env.BIDVBANK_TOKEN;

	const Logger = this.logger.info;

	async function RequestPost(urlRequest, payload) {
		try {
			const response = await Axios.post(urlRequest, payload, {
				headers: {
					Authorization
				},
				timeout: 50000
			});
			return response;
		} catch (error) {
			const data = {
				code: -1,
				data: {
					code: error.code,
					message: error.code === 'ECONNABORTED' ? 'ECONNABORTED' : error.message
				},
				original: error
			};

			if (error.code === 'ECONNABORTED') {
				data.code = -2;
			}
			return data;
		}
	}
	async function GetRequest(urlRequest) {
		try {
			const response = await Axios.get(urlRequest, {
				headers: {
					Authorization
				},
				timeout: 50000
			});
			return response;
		} catch (error) {
			const data = {
				code: -1,
				data: {
					code: error.code,
					message: error.code === 'ECONNABORTED' ? 'ECONNABORTED' : error.message
				},
				original: error
			};
			if (error.code === 'ECONNABORTED') {
				data.code = -2;
			}
			return data;
		}
	}

	return {
		async Pay(payload) {
			const response = {
				state: SupplierConstant.STATE.FAILED,
				message: 'Thanh toán thất bại. Vui lòng thử lại sau.',
				transaction: null,
				original: null
			};

			const params = {
				trans_id: payload.transaction,
				trans_desc: payload.description || 'Nap tien vao vi PayME',
				amount: payload.amount,
				account_id: payload.accountId,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};

			let urlRequest = null;
			if (payload.version === 2) {
				urlRequest = `${url}/v2/bidv/wallet/bank_to_wallet`;
			} else {
				urlRequest = `${url}/v1/bidv/wallet/bank_to_wallet`;
			}

			try {
				Logger(`[BIDV] -> [PAY] -> [Params] : ${JSON.stringify({ urlRequest, params })}`);
				const request = await RequestPost(urlRequest, params);

				const result = request.data;
				Logger(`[BIDV] -> [PAY] -> [Response] : ${JSON.stringify({ urlRequest, result })}`);

				response.original = result;
				response.data = _.get(result, 'data', {});
				response.transaction = _.get(result, 'data.trans_id', '');
				if (result.code === 2053) {
					response.state = SupplierConstant.STATE.REQUIRE_OTP;
					response.message = _.get(result, 'data.message', 'Thanh toán cần xác thực OTP');
					return response;
				}
				if (result.code === -2) {
					response.state = SupplierConstant.STATE.TIME_OUT;
					response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					return response;
				}
				if (result.code !== 1000) {
					response.message = _.get(result, 'data.message', 'Thanh toán thất bại. Vui lòng thử lại sau.');
					return response;
				}

				response.message = 'Nạp tiền thành công.';
				response.state = SupplierConstant.STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[BIDV -> [Pay] -> [Error] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		},
		async VerifyPay(payload) {
			// Response:  code - message - state - transaction - html
			const response = {
				state: SupplierConstant.STATE.FAILED,
				message: 'Xác thực thanh toán thất bại. Vui lòng thử lại sau',
				original: null
			};

			const params = {
				trans_id: payload.transaction,
				otp_number: payload.otp,
				account_id: payload.accountId,
				sign_type: 'RSA',
				sign: 'xxxxxxxx'
			};

			let urlRequest = null;
			if (payload.version === 2) {
				urlRequest = `${url}/v2/bidv/wallet/bank_to_wallet/check_otp`;
			} else {
				urlRequest = `${url}/v1/bidv/wallet/check_otp`;
			}

			try {
				Logger(`[BIDV] -> [VerifyPay] -> [Params] : ${JSON.stringify({ urlRequest, params })}`);
				const request = await RequestPost(urlRequest, params);
			
				const result = request.data;
				Logger(`[BIDV] -> [VerifyPay] -> [Response] : ${JSON.stringify({ urlRequest, result })}`);
				response.original = result;
				response.data = _.get(result, 'data', {});

				if (result.code === -2) {
					response.state = SupplierConstant.STATE.TIME_OUT;
					response.message = 'Ngân hàng thụ hưởng từ chối giao dịch, vui lòng thử lại sau';
					return response;
				}
				if (result.code !== 1000) {
					response.message = _.get(result, 'data.message', 'Xác thực thanh toán thất bại. Vui lòng thử lại sau');
					return response;
				}
				response.message = 'Nạp tiền thành công';
				response.state = SupplierConstant.STATE.SUCCEEDED;

				return response;
			} catch (error) {
				Logger(`[BIDV] -> [VerifyPay] => [Exception] : ${error}`);
				response.supplierResponse = JSON.stringify({ errorMessage: error.message });
				return response;
			}
		}

	};
};
