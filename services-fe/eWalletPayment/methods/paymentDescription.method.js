const _ = require('lodash');
const PaymentConstant = require('../constants/payment.constant');


/**
 * - Get Payment description based on method name

 * Payload
 * @param {String} method : Method
 * @param {String} cardNumber : bankCard
 * @param {String} accountNumber : accountNumber
 * @param {String} swiftCode : swiftCode of bank
 */


module.exports = async function (data = {}) {
	
	const Broker = this.broker;
	const Logger = this.logger.info;

	let description = '';
	try{

		if(data.method === PaymentConstant.METHOD.WALLET){
			description = 'Ví PayME';
		}else if(_.includes([PaymentConstant.METHOD.BANK_CARD, 
			PaymentConstant.METHOD.BANK_ACCOUNT,
			PaymentConstant.METHOD.LINKED], data.method)){

			let number = _.get(data, 'cardNumber', '') || '';
			if (number === '') {
				number = _.get(data, 'accountNumber', '') || '';
			}

			if (number === '') {
				number = _.get(data, 'bankAccountNumber', '') || '';
			}
			const bankCodeConfig = await Broker.call('v1.eWalletBankCodeModel.findOne',
				[{ isActive: true, swiftCode: data.swiftCode }]);

			const bankName = _.get(bankCodeConfig, 'shortName', '');

			description = `${bankName}-${number.substr(-4, 4)}`;
		}

	} catch (e) {
		Logger('[PAYMENT] -> [DESCRIPTION] -> [EXCEPTION] :: -> , ', e);    
	}
	return description;
};
