const _ = require('lodash');
const PaymentConstant = require('../constants/payment.constant');

module.exports = function(params) {
	const keyPayment = _.keys(params)[0];

	let result = null;
	switch(keyPayment) {
		case 'wallet':
			result = 'v1.eWalletPayment.payWallet';
			break;
		case 'bankCard':
			result = 'v1.eWalletPayment.payCard';
			break;

		case 'linked':
			result = 'v1.eWalletPayment.payLinked';
			break;
		default:
			result = PaymentConstant.METHOD.WALLET;
			break;
	}

	return result;
};