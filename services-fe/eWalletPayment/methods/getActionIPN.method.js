const HistoryConstant = require('../constants/history.constant');

/**
 * - Get Action IPN based on servieType

 * Payload
 * @param {String} serviceType : Service Type of payment
 */

module.exports = function(serviceType) {

	let result = null;
	switch(serviceType) {
		case HistoryConstant.SERVICE_TYPE.DEPOSIT:
			result = 'v1.eWalletDeposit.ipnDepositEwallet';
			break;
		default:
			result = 'v1.eWalletDeposit.ipnDepositEwallet';
			break;
	}

	return result;
};