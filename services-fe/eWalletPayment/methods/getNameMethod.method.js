const _ = require('lodash');
const PaymentConstant = require('../constants/payment.constant');

/**
 * - Get name Method based on methodName

 * Payload
 * @param {String} methodName : Method Name

 */

module.exports = function(methodName) {

	let result = '';
	switch(methodName) {
		case 'wallet':
			result = PaymentConstant.METHOD.WALLET;
			break;
		case 'bankCard':
			result = PaymentConstant.METHOD.BANK_CARD;
			break;

		case 'linked':
			result = PaymentConstant.METHOD.LINKED;
			break;
		case 'bankTransfer':
			result = PaymentConstant.METHOD.BANK_TRANSFER;
			break;

		case 'creditBalance': 
			result = PaymentConstant.METHOD.CREDIT_BALANCE;
			break;

		default:
			result = PaymentConstant.METHOD.WALLET;
			break;
	}

	return result;
};