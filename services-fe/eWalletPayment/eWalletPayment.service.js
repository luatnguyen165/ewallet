const _ = require('lodash');

module.exports = {
	name: 'eWalletPayment',
	version: 1,
	/**
	 * Settings
	 */
	settings: {

	},

	hooks: {
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		pay: {
			params: {
				accountId: 'number|positive',
				appId: 'number|optional',
				amount: 'number',
				description: 'string|optional',
				ip: 'string|optional',
				clientId: 'string|optional',
				note: 'string|optional',
				service: {
					$$type: 'object',
					type: 'string',
					code: 'string',
					transaction: 'string'
				},
				method :{
					type: 'object',
					strict: 'remove',
					props: {
						wallet: {
							$$type: 'object|optional',
							requireSecurityCode: 'boolean',
							securityCode: 'string|optional'
						},
						bankCard: {
							$$type: 'object|optional',
							cardNumber: 'string',
							cardHolder: 'string',
							issuedAt: 'string|optional',
							expiredAt: 'string|optional',
							notifyUrl: 'string|optional',
							envName: 'string|optional'
						},
						linked: {
							$$type: 'object|optional',
							linkedId: 'number',
							otp: 'string|optional',
							phone: 'string|optional',
							notifyUrl: 'string|optional',
							envName: 'string|optional'
						},
						creditBalance: {
							$$type: 'object|optional',
							securityCode: 'string',
							supplierLinkedId: 'string',
							note: 'string'
						},
						bankTransfer: {
							$$type: 'object|optional',
							active: 'boolean'
						}
					},
					minProps: 1,
					maxProps: 1
				}
			},
			timeout: 30000,
			handler : require('./actions/pay.action')
		},
		payWallet: {
			params :{
				paymentId : 'number',
				transaction: 'string',
				accountId: 'number',
				amount: 'number',
				requireSecurityCode: 'boolean',
				securityCode: 'string|optional'
			},
			timeout: 30000,
			handler: require('./actions/payWallet.action')
		},
		payCard: {
			params :{
				paymentId : 'number',
				transaction: 'string',
				accountId: 'number',
				amount: 'number',
				cardNumber: 'string',
				cardHolder: 'string',
				issuedAt: 'string|optional',
				expiredAt: 'string|optional',
				notifyUrl: 'string|optional',
				envName: 'string|optional'
			},
			timeout: 30000,
			handler: require('./actions/payCard.action')
		},
		payLinked: {
			params :{
				paymentId : 'number',
				transaction: 'string',
				accountId: 'number',
				amount: 'number',
				description: 'string|optional',
				amount: 'number|positive|min:1',
				otp: 'string|optional',
				linkedId: 'number'
			},
			timeout: 30000,
			handler: require('./actions/payLinked.action')
		},
		test: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/pay/test',
				auth: false
			},
			handler: require('./actions/test.action')
		},
		testRefund: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/refund/test',
				auth: false
			},
			params: {},
			handler: require('./actions/test.refund.action')
		},
		refund: {
			params: {
				paymentId: 'number',
				serviceId: 'number'
			},
			timeout: 30000,
			handler: require('./actions/refund.action')
		},
		ipnCard: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/card/ipn',
				security: false,
				auth: false
			},
			params: {},
			timeout: 30000,
			handler: require('./actions/ipnCard.action')
		},
		ipnLinkedCardNapas: {
			rest: {
				method: 'POST',
				fullPath: '/be/ewallet/linked/napas/ipn',
				security: false,
				auth: false
			},
			params: {},
			timeout: 30000,
			handler: require('./actions/ipnCard.action')
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		GetNameMethod: require('./methods/getNameMethod.method'),
		GetActionPayment: require('./methods/getActionPayment.method'),
		PaymentDescription: require('./methods/paymentDescription.method'),
		RemoveUnicode: require('./methods/removeUnicode.method'),
		RedisCache: require('./methods/redisCache.method'),
		GetActionIPN: require('./methods/getActionIPN.method')
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
