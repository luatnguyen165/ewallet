module.exports ={
	SERVICE_TYPE: {
		BASIC: 'BASIC',
		SOCIAL_PAYMENT: 'SOCIAL_PAYMENT',
		SOCIAL_PAYMENT_PG: 'SOCIAL_PAYMENT_PG',
		WALLET_PAY: 'WALLET_PAY',
		WALLET_QR: 'WALLET_QR',
		TRANSFER_PAYME: 'TRANSFER_PAYME',
		DEPOSIT: 'DEPOSIT',
		WITHDRAW: 'WITHDRAW',
		TRANSFER_MONEY: 'TRANSFER_MONEY',
		OPEN_EWALLET_PAYMENT: 'OPEN_EWALLET_PAYMENT',
		BILL: 'BILL',
		LINKED: 'LINKED',
		MOBILE_CARD: 'MOBILE_CARD',
		MOBILE_TOPUP: 'MOBILE_TOPUP',
		REFUND_MONEY: 'REFUND_MONEY',
		ISEC: 'ISEC',
		SALARY: 'SALARY',
		ADVANCE_MONEY: 'ADVANCE_MONEY', // ƯNG TIỀN
		CREDIT_STATEMENT: 'CREDIT_STATEMENT', // THANH TOÁN NỢ
		CREDIT_SETTLEMENT: 'CREDIT_SETTLEMENT', // TẤT TOÁN NỢ
		PAYME_CREDIT: 'PAYME_CREDIT', // THANH TOÁN DỊCH VỤ
		ADVANCE_SALARY: 'ADVANCE_SALARY', // ỨNG LƯƠNG
		ADVANCE_SALARY_SETTLEMENT: 'ADVANCE_SALARY_SETTLEMENT',
		FAST_LOAN: 'FAST_LOAN',
		PAYMENT_CODE: 'PAYMENT_CODE',
		MINI_PROGRAM_ORDER_PAYMENT: 'MINI_PROGRAM_ORDER_PAYMENT',
		PAY_QRCODE: 'PAY_QRCODE',
		CREDIT_BALANCE: 'CREDIT_BALANCE',
		LIQUID_QR: 'LIQUID_QR'
	}
};