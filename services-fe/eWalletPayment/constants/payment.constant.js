module.exports = {
	RESOLVE_TYPE: {
		WALLET: 'PaymentWalletObject',
		BANK_CARD: 'PaymentBankCardObject',
		BANK_QR_CODE: 'PaymentBankQRCode',
		BANK_TRANSFER: 'PaymentBankAccountObject ',
		LINKED: 'PaymentLinkedObject',
		CREDIT_CARD: 'PaymentCreditCardResponsed',
		PAYME_CREDIT: 'PaymeCreditPaymentObject',
		BANK_CARD_PG: 'PaymentBankCardPGObject',
		CREDIT_CARD_PG: 'PaymentCreditCardObject',
		MOMO_PG: 'PaymentMoMoPGObject',
		ZALOPAY_PG: 'PaymentZaloPayPGObject',
		CREDIT_BALANCE: 'PaymentCreditBalanceObject',
		VIET_QR: 'PaymentVietQRObject'
	},
	METHOD: {
		WALLET: 'WALLET',
		BANK_CARD: 'BANK_CARD',
		BANK_ACCOUNT: 'BANK_ACCOUNT',
		BANK_QR_CODE: 'BANK_QR_CODE',
		BANK_TRANSFER: 'BANK_TRANSFER',
		LINKED: 'LINKED',
		CREDIT_CARD: 'CREDIT_CARD',
		PAYME_CREDIT: 'PAYME_CREDIT',
		PAYME_PG: 'PAYME_PG', // Pttt la PayME ben CTT,
		OPEN_EWALLET_PG: 'OPEN_EWALLET_PG', // OpenEWalletPaymentGateWay
		BANK_CARD_PG: 'BANK_CARD_PG', // Thhanh toan bankCard qua duong CTT(payment Gateway)
		MOMO_PG: 'MOMO_PG', // Thanh toan MOMO qua duong CTT(Payment Gateway)
		CREDIT_CARD_PG: 'CREDIT_CARD_PG', // Thanh toan Credit Card qua duong CTT(Payment Gateway),
		BANK_QR_CODE_PG: 'BANK_QR_CODE_PG',
		ZALOPAY_PG: 'ZALOPAY_PG',
		CREDIT_BALANCE: 'CREDIT_BALANCE',
		VIET_QR: 'VIET_QR',
		BANK_TRANSFER_PG: 'BANK_TRANSFER_PG'
	},
	STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		REQUIRE_VERIFY: 'REQUIRE_VERIFY',
		REQUIRE_OTP: 'REQUIRE_OTP',
		INVALID_OTP: 'INVALID_OTP',
		CANCELED: 'CANCELED',
		FAILED: 'FAILED'
	},
	SECURITY_CODE_SOURCE: {
		PASSWORD: 'PASSWORD',
		OTP: 'OTP',
		TOUCH_ID: 'TOUCH_ID',
		TOTP: 'TOTP',
		ORDER_ISEC: 'ORDER_ISEC'
	},
	LIMIT_SETTING_KEY: 'limit.param.amount.payment',
	PAYMENT_PAYLOAD_KEY: {
		WALLET: 'wallet',
		BANK_CARD: 'bankCard',
		LINKED: 'linked'
	}
};