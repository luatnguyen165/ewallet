const _ = require('lodash');
const SupplierConstant = require('../constants/supplier.constant');

const PaymentConstant = require('../constants/payment.constant');
const NapasLib = require('../libraries/napas.library');

module.exports = function () {
	const Logger = this.logger.info;
	const Broker = this.broker;
	const NapasFuncs = NapasLib.bind(this)();

	return {

		/**
		 * Create Payment and call to Napas Supplier
		 * Four mains case
		 * If succeeded -> return state SUCCEEDED into linked Payment
		 * If Require_verify -> return State REQUIRE_VERIFY into linked Payment
		 * If Failed -> return State FAILED into linked Payment
		 * If Timeout -> return State TIMEOUT into linked Payment
 		 *
		 * Payload
 		 * @param {Number} accountId : AccountId of User
 		 * @param {Number} paymentId : PaymentId to reference
 		 * @param {String} transaction : Transaction of Payment
 		 * @param {Object} cardInfo : CardInfo of linked Card
		 * @param {Number} amount: The amount of Money which User want to Pay
		 * @param {String} clientIp : ClienIp (optional)
         * @param {String} envName: envName (optional),
         * @param {String} serviceType: ServiceType
         * @param {String} redirectUrl: rdirectUrl
         * @param {String} cardId: CardId of Connector

			@return
			@example
		*/
		async Pay(payload) {
			const response = {
				state: PaymentConstant.STATE.FAILED,
				message: 'Thanh toán thất bại. Vui lòng thử lại sau.(NAPAS_ERR)'
			};

			try {
				const paymentInfo = await Broker.call('v1.eWalletPaymentLinkedNapasModel.create', [{
					accountId: payload.accountId,
					paymentId: payload.paymentId,
					transactionId: payload.transaction,
					bankInfo: payload.cardInfo,
					amount: payload.amount,
					description: 'Thanh toán từ Napas',
					state: PaymentConstant.STATE.PENDING
				}]);

				if (_.get(paymentInfo, 'id', null) === null) {
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(NAPAS_ERR_01)';
					return response;
				}

				let redirectUrl = _.get(payload, 'redirectUrl', '') || '';
				if (redirectUrl === '') {
					redirectUrl = 'https://payme.vn';
				}

				const notifyUrl = `${process.env.DOMAIN_URL}/be/ewallet/linked/napas/ipn`;
				const paramsSend = {
					amount: payload.amount,
					accountId: payload.accountId,
					transaction: payload.transaction,
					cardId: _.get(payload, 'cardId', ''),
					redirectUrl, // redirect sau khi user thanh toán thành công
					notifyUrl,
					clientIp: _.get(payload, 'clientIp', ''),
					envName: _.get(payload, 'envName', 'MobileApp')
				};

				const napasResponse = await NapasFuncs.TokenPay(paramsSend);

				const supplierTransaction = _.get(napasResponse, 'data.order_id', null);

				/* Case FAILED */
				if (napasResponse.state !== SupplierConstant.STATE.REQUIRE_VERIFY) {
					const alertMsg = `❗Gửi yêu cầu Thanh toán liên kết Napas thất bại!
					\nParam: ${JSON.stringify(paramsSend)}
					\nResponse: ${JSON.stringify(napasResponse.original)}
					\nServiceType: ${JSON.stringify(payload.serviceType)}`;

					// TODO: Send Alert

					const updateData = {
						supplierResponsed: [`${JSON.stringify(napasResponse.original)}`],
						supplierTransaction: _.get(napasResponse, 'data.order_id', null)
					};

					// Timeout Case
					if (napasResponse.message === 'ECONNABORTED') {
						_.set(response, 'message', 'Không nhận được phản hồi từ ngân hàng, vui lòng thử lại sau');
					} else {
					// Failed case
						response.state = PaymentConstant.STATE.FAILED;
						response.message = _.get(napasResponse, 'data.message', 'Thanh toán thất bại!');
						updateData.state = PaymentConstant.STATE.FAILED;
					}

					const updatePayResult = await Broker.call('v1.eWalletPaymentLinkedNapasModel.updateOne', [{
						id: paymentInfo.id
					}, updateData
					]);

					if (updatePayResult.nModified < 1) {
						// TODO
						response.state = PaymentConstant.STATE.FAILED;
						response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(NAPAS_ERR_02)';
						return response;
					}

					response.state = PaymentConstant.STATE.FAILED;
					response.message = _.get(napasResponse, 'data.message', 'Thanh toán thất bại!');
					return response;
				}

				/* Case Require_verify (using web view to verify) */
				const updatePayResult = await Broker.call('v1.eWalletPaymentLinkedNapasModel.updateOne', [{
					id: paymentInfo.id
				}, {
					supplierResponsed: [JSON.stringify(napasResponse.original)],
					supplierTransaction
				}]);

				if (updatePayResult.nModified < 1) {
					response.state = PaymentConstant.STATE.FAILED;
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(NAPAS_ERR_03)';
					return response;
				}

				response.supplierTransaction = supplierTransaction;
				response.message = _.get(napasResponse, 'data.message', null) || 'Tạo yêu cầu thanh toán thành công';
				response.state = PaymentConstant.STATE.REQUIRE_VERIFY;
				response.html = _.get(napasResponse, 'data.data', null);
				response.paymentLinkedId = paymentInfo.id;
			} catch (err) {
				Logger('[PAYMENT] -> [PAY] -> [NAPAS] -> [EXCEPTION] -> :: ', err);
				response.state = PaymentConstant.STATE.FAILED;
				response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(NAPAS_ERR_04)';
			}

			return response;
		}
	};
};
