const _ = require('lodash');
const moment = require('moment');

const PaymentConstant = require('../constants/payment.constant');
const SupplierConstant = require('../constants/supplier.constant');

const PVCLib = require('../libraries/pvcb.library');

module.exports = function () {
	const Logger = this.logger.info;
	const Broker = this.broker;
	const RedisCache = this.RedisCache();
	const PVCFuncs = PVCLib.bind(this)();

	return {

		/**
		 * Create Payment and call to PVC Supplier
		 * Four mains case
		 * If succeeded -> return state SUCCEEDED into linked Payment
		 * If Require_otp -> return State REQUIRE_OTP into linked Payment
		 * If Failed -> return State FAILED into linked Payment
		 * If Timeout -> return State TIMEOUT into linked Payment
 		 *
		 * Payload
 		 * @param {Number} accountId : AccountId of User
 		 * @param {Number} paymentId : PaymentId to reference
 		 * @param {String} transaction : Transaction of Payment
 		 * @param {Object} cardInfo : CardInfo of linked Card
		 * @param {Number} amount: The amount of Money which User want to Pay
 		 * @param {String} cardId: Card Id of Connector

			@return
			@example
			{
				state: PaymentConstant.STATE.FAILED,
				message: 'Thanh toán thất bại. Vui lòng thử lại sau.(PVC_ERR_01)'
			}
		*/
		async Pay(payload) {
			const response = {
				state: PaymentConstant.STATE.FAILED,
				message: 'Thanh toán thất bại. Vui lòng thử lại sau.(PVC_ERR_01)'
			};
			try {
				// STEP 1: Create Payment
				const paymentInfo = await Broker.call('v1.eWalletPaymentLinkedPVCModel.create', [{
					accountId: payload.accountId,
					paymentId: payload.paymentId,
					transactionId: payload.transaction,
					bankInfo: payload.cardInfo,
					amount: payload.amount,
					description: 'Thanh toán từ PVCBank',
					state: PaymentConstant.STATE.PENDING
				}]);

				if (_.get(paymentInfo, 'id', null) === null) {
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(PVC_ERR_02)';
					return response;
				}

				// STEP 2: Call Supplier
				const paramsSend = {
					transaction: paymentInfo.transactionId,
					cardId: payload.cardId,
					amount: payload.amount,
					accountId: payload.accountId
				};

				const pvcResponse = await PVCFuncs.Pay(paramsSend);

				const supplierResponsed = [JSON.stringify(pvcResponse.original)];
				const supplierTransaction = _.get(pvcResponse, 'data.trans_id', null);
				// STEP 3: Based on pvcResponse.state in supplier Response, handle appropriately

				if (pvcResponse.state !== SupplierConstant.STATE.SUCCEEDED) {
					// All not succeed Case is handled here
					const alertMsg = `❗Thanh toán từ PVCB fail
					\nParamRequest: ${JSON.stringify(paramsSend)}
					\nResponse: ${JSON.stringify(pvcResponse)}
					\nPaymentId: ${paymentInfo.id}`;

					// TODO: Notify in here

					const updateFailPayResult = await Broker.call('v1.eWalletPaymentLinkedPVCModel.updateOne', [
						{ id: paymentInfo.id },
						{
							state: PaymentConstant.STATE.FAILED,
							supplierResponsed,
							supplierTransaction
						}
					]);

					if (updateFailPayResult.nModified < 1) {
						response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(PVC_ERR_03)';
						return response;
					}

					response.message = 'Thanh toán thất bại.(PVC_FAILED)';
					return response;
				}

				// Need OTP case
				if (_.get(pvcResponse, 'data.otp_required', false) === true) {
					const updatePendingPayResult = await Broker.call('v1.eWalletPaymentLinkedPVCModel.updateOne', [
						{ id: paymentInfo.id },
						{
							supplierResponsed,
							supplierTransaction

						}
					]);

					if (updatePendingPayResult.nModified < 1) {
						response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(PVC_ERR_04)';
						return response;
					}
					response.supplierTransaction = supplierTransaction;
					response.state = PaymentConstant.STATE.REQUIRE_OTP;
					response.message = 'Vui lòng nhập mã OTP';
					return response;
				}

				// Succeeded case
				const updateSucceededPayResult = await Broker.call('v1.eWalletPaymentLinkedPVCModel.updateOne',
					[
						{
							id: paymentInfo.id
						}, {
							state: PaymentConstant.STATE.SUCCEEDED,
							supplierResponsed,
							supplierTransaction
						}
					]);

				if (updateSucceededPayResult.nModified < 1) {
					response.state = PaymentConstant.STATE.FAILED;
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau. (PVC_ERR_05)';
					return response;
				}

				response.paymentLinkedId = paymentInfo.id;
				response.supplierTransaction = supplierTransaction;
				response.message = _.get(pvcResponse, 'data.message', null) || 'Thanh toán thành công';

				return response;
			} catch (err) {
				Logger('[PAYMENT] -> [PAY] -> [PVCBANK] -> [EXCEPTION] -> :: ', err);

				response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(PVC_ERR_06)';
				return response;
			}
		},

		/**
		 * Send OTP to PVC Supplier to Verify Payment
		 * Four mains case
		 * If succeeded -> return state SUCCEEDED into verify linked Payment
		 * If invalid otp -> return State INVALID_OTP into verify linked Payment
		 * If Failed -> return State FAILED into verify linked Payment
 		 * If Timeout -> return State TIMEOUT into verify linked Payment
		 *
		 * Payload
 		 * @param {Number} accountId : AccountId of User
 		 * @param {String} transaction : Transaction of Payment want to verify
 		 * @param {String} otp : Bank's OTP
		 *
		 * @return
		 * @example

		*/
		async VerifyPay(payload) {
			const response = {
				state: PaymentConstant.STATE.FAILED,
				message: 'Xác thực thanh toán thất bại. Vui lòng thử lại sau.(PVC_VER_ERR_01)'
			};
			try {
				// STEP 1: Find Payment based on Transaction
				const paymentInfo = await Broker.call('v1.eWalletPaymentLinkedPVCModel.findOne', [{
					accountId: payload.accountId,
					paymentId: payload.paymentId,
					transactionId: payload.transaction,
					state: PaymentConstant.STATE.PENDING
				}, '-_id id amount supplierTransaction supplierResponsed']);

				if (_.get(paymentInfo, 'id', null) === null) {
					response.message = 'Không tìm thấy giao dịch.(PVC_VER_ERR_02)';
					return response;
				}

				const paramsSend = {
					supplierTransaction: paymentInfo.supplierTransaction,
					otp: payload.otp
				};

				// STEP 2: Call Supplier
				const pvcResponse = await PVCFuncs.VerifyPay(paramsSend);

				// STEP 3: Based on pvcResponse.state, handle appropriately
				const supplierResponsed = _.get(paymentInfo, 'supplierResponsed', []);
				supplierResponsed.push(JSON.stringify(pvcResponse.pvcResponse));
				/* All not succeed Case is handled here */
				const keyCountFailedTransaction = `BankTransactionFailedOTP-${payload.accountId}`;
				/* Fail OTP Case */
				if (pvcResponse.state === SupplierConstant.STATE.INVALID_OTP) {
					let countOTPFailed = await RedisCache.get({ key: keyCountFailedTransaction });
					countOTPFailed = _.toNumber(countOTPFailed) || 0;
					if (countOTPFailed >= 3) {
						await Broker.call('v1.eWalletPaymentLinkedPVCModel.updateOne', [{
							id: paymentInfo.id
						}, {
							state: PaymentConstant.STATE.FAILED,
							supplierResponsed
						}]);

						const ttl = await RedisCache.getTTL({ key: keyCountFailedTransaction });
						let d = moment.duration(ttl, 'seconds').format('m:ss');
						if (d < 60) d = `${d} giây`;
						else {
							d = d.split(':');
							if (d[1] !== '00') d = `${d[0]} phút ${d[1]} giây`;
							else d = `${d[0]} phút`;
						}
						response.state = PaymentConstant.STATE.FAILED;
						response.message = `Bạn đã nhập sai OTP quá 3 lần liên tiếp, vui lòng thử lại sau ${d} hoặc liên hệ bộ phận CSKH để được hỗ trợ!`;
						return response;
					}
					await RedisCache.set({ key: keyCountFailedTransaction, value: countOTPFailed + 1, ttl: 300 });
					response.state = PaymentConstant.STATE.INVALID_OTP;
					response.message = _.get(pvcResponse, 'data.message', 'Mã OTP không chính xác');
					return response;
				}

				/* Fail Payment Case */
				if (pvcResponse.state === SupplierConstant.STATE.FAILED) {
					const stringAmount = Math.round(_.toNumber(paymentInfo.amount)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
					const alertMsg = `❗Thanh toán từ PVCBank thất bại
						  \nTopup Response: ${JSON.stringify(pvcResponse.original)}
						  \nID: ${paymentInfo.id} - Amount: ${stringAmount} - AccountId: ${payload.accountId}`;

					// TODO: Notify Failed Payment here

					const updateFailedPayResult = await Broker.call('v1.eWalletPaymentLinkedPVCModel.updateOne', [{
						id: paymentInfo.id
					}, {
						state: PaymentConstant.STATE.FAILED,
						supplierResponsed
					}]);

					if (updateFailedPayResult.nModified < 1) {
						response.message = 'Thanh toán thất bại.(PVC_VER_ERR_03)';
						return response;
					}

					await RedisCache.delete({ key: keyCountFailedTransaction });
					response.state = PaymentConstant.STATE.FAILED;
					response.message = _.get(pvcResponse, 'data.message', 'Thanh toán thất bại!');
					return response;
				}

				// Succeed case
				const updateSucceedPayData = await Broker.call('v1.eWalletPaymentLinkedPVCModel.updateOne', [{
					id: paymentInfo.id
				}, {
					state: PaymentConstant.STATE.SUCCEEDED,
					supplierResponsed
				}]);

				// HACK ?
				// Maybe just notify in here
				if (updateSucceedPayData.nModified < 1) {
					response.message = 'Thanh toán thất bại.(PVC_VER_ERR_04)';
					return response;
				}

				response.state = PaymentConstant.STATE.SUCCEEDED;
				response.message = _.get(pvcResponse, 'message', null) || 'Thanh toán thành công';
			} catch (err) {
				Logger('[PAYMENT] -> [VERIFY_PAY] -> [PVCBANK] -> [EXCEPTION] -> :: ', JSON.stringify(err));
				response.message = 'Thanh toán thất bại.(PVC_VER_ERR_05)';
			}
			return response;
		}

	};
};
