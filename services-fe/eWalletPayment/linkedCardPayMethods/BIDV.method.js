const _ = require('lodash');
const moment = require('moment');
const SupplierConstant = require('../constants/supplier.constant');
const PaymentConstant = require('../constants/payment.constant');

const BIDVLib = require('../libraries/bidv.library');

module.exports = function (logger, payload) {
	// Payload
	const Logger = this.logger.info;
	const Broker = this.broker;
	const BIDVFuncs = BIDVLib.bind(this)();
	const RedisCache = this.RedisCache();

	return {

		async Pay() {
			const response = {
				state: PaymentConstant.STATE.FAILED,
				message: 'Thanh toán thất bại. Vui lòng thử lại sau.(BIDV_ERR)'
			};
			try {
				const paymentInfo = await Broker.call('v1.eWalletPaymentLinkedBIDVModel.create', [{
					accountId: payload.accountId,
					paymentId: payload.paymentId,
					transactionId: payload.transaction,
					bankInfo: payload.cardInfo,
					amount: payload.amount,
					version: payload.version,
					description: 'Thanh toán từ BIDVBank',
					state: SupplierConstant.STATE.PENDING
				}]);

				if (_.get(paymentInfo, 'id', null) === null) {
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(BIDV_ERR_01)';
					return response;
				}

				const paramsSend = {
					transaction: payload.transaction,
					description: 'Nap tien vao vi PayME',
					amount: payload.amount,
					accountId: payload.accountId,
					version: payload.version
				};

				const supplierResponse = await BIDVFuncs.Pay(paramsSend);

				// Supplier Responsed will be saved into DB
				const supplierResponsed = supplierResponse.original;
				const supplierTransaction = supplierResponse.transaction;

				/* Need OTP to verify transaction Case */
				if (supplierResponse.state === SupplierConstant.STATE.REQUIRE_OTP) {
					const updatePendingPayResult = await Broker.call('v1.eWalletPaymentLinkedBIDVModel.updateOne', [
						{ id: paymentInfo.id },
						{
							supplierResponsed,
							supplierTransaction

						}
					]);

					if (updatePendingPayResult.nModified < 1) {
						response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(BIDV_ERR_02)';
						return response;
					}
					response.supplierTransaction = supplierTransaction;
					response.state = PaymentConstant.STATE.REQUIRE_OTP;
					response.message = 'Vui lòng nhập mã OTP';
					return response;
				}

				if (supplierResponse.state !== SupplierConstant.STATE.SUCCEEDED) {
					// All not succeed Case is handled here
					const alertMsg = `❗Thanh toán từ PVCB fail
					\nParamRequest: ${JSON.stringify(paramsSend)}
					\nResponse: ${JSON.stringify(supplierResponse)}
					\nPaymentId: ${paymentInfo.id}`;

					// TODO: Notify in here

					const updateFailPayResult = await Broker.call('v1.eWalletPaymentLinkedBIDVModel.updateOne', [
						{ id: paymentInfo.id },
						{
							state: PaymentConstant.STATE.FAILED,
							supplierResponsed,
							supplierTransaction
						}
					]);

					if (updateFailPayResult.nModified < 1) {
						response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(BIDV_ERR_03)';
						return response;
					}

					response.message = 'Thanh toán thất bại.(BIDV_FAILED)';
					return response;
				}

				// Succeeded case
				const updateSucceededPayResult = await Broker.call('v1.eWalletPaymentLinkedBIDVModel.updateOne',
					[
						{
							id: paymentInfo.id
						}, {
							state: PaymentConstant.STATE.SUCCEEDED,
							supplierResponsed,
							supplierTransaction
						}
					]);

				if (updateSucceededPayResult.nModified < 1) {
					response.state = PaymentConstant.STATE.FAILED;
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau. (BIDV_ERR_04)';
					return response;
				}
				response.supplierTransaction = supplierTransaction;
				response.message = _.get(supplierResponse, 'message', null) || 'Thanh toán thành công';

				return response;
			} catch (err) {
				response.state = PaymentConstant.STATE.FAILED;
				response.message = 'Thanh toán thất bại. Vui lòng thử lại sau. (BIDV_ERR_05)';
				Logger('[BIDV] -> [PAY] -> [EXCEPTION] -> :: ', err);

				return response;
			}
		},

		async VerifyPay() {
			const response = {
				state: PaymentConstant.STATE.FAILED,
				message: 'Thanh toán thất bại. Vui lòng thử lại sau.(BIDV_VER_ERR)'
			};
			try {
				const paymentInfo = await Broker.call('v1.eWalletPaymentLinkedBIDVModel.findOne', [{
					accountId: payload.accountId,
					paymentId: payload.paymentId,
					transactionId: payload.transaction,
					state: PaymentConstant.STATE.PENDING
				}, '-_id id amount accountId supplierResponsed supplierTransaction']);

				if (_.get(paymentInfo, 'id', null) === null) {
					response.message = 'Thanh toán thất bại. Không tìm thấy giao dịch.';
					return response;
				}

				const paramsSend = {
					transaction: paymentInfo.supplierTransaction,
					otp: payload.otp,
					accountId: paymentInfo.accountId,
					version: payload.version
				};

				const supplierResponse = await BIDVFuncs.VerifyPay(paramsSend);

				// Supplier responsed will be saved into DB
				const supplierResponsed = _.get(paymentInfo, 'supplierResponsed', []);
				supplierResponsed.push(JSON.stringify(supplierResponse.original));

				/* Wrong OTP Case */
				const keyLockBankOTP = `BankTransactionFailedOTP-${payload.accountId}`;

				if (supplierResponse.state === SupplierConstant.STATE.INVALID_OTP) {
					let countOTPFailed = await RedisCache.get({ key: keyLockBankOTP });
					countOTPFailed = _.toNumber(countOTPFailed) || 0;
					if (countOTPFailed >= 3) {
						await this.broker.call('v1.eWalletPaymentLinkedBIDVModel.updateOne', [{
							id: paymentInfo.id
						}, { state: SupplierConstant.STATE.FAILED }]);
						const ttl = await RedisCache.getTTL({ key: keyLockBankOTP });
						let d = moment.duration(ttl, 'seconds').format('m:ss');
						if (d < 60) d = `${d} giây`;
						else {
							d = d.split(':');
							if (d[1] !== '00') d = `${d[0]} phút ${d[1]} giây`;
							else d = `${d[0]} phút`;
						}
						response.state = SupplierConstant.STATE.FAILED;
						response.message = `Bạn đã nhập sai OTP quá 3 lần liên tiếp, vui lòng thử lại sau ${d} hoặc liên hệ bộ phận CSKH để được hỗ trợ!`;
						return response;
					}
					await RedisCache.set({ key: keyLockBankOTP, value: countOTPFailed + 1, ttl: 300 });
					response.state = SupplierConstant.STATE.INVALID_OTP;
					response.message = _.get(supplierResponse, 'data.message', 'Mã OTP không chính xác');
					return response;
				}

				/* Fail Case */
				if (supplierResponse.state === SupplierConstant.STATE.FAILED) {
					const t = Math.round(_.toNumber(paymentInfo.amount)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
					const alertMsg = `❗Thanh toán từ BIDVBank thất bại
					  \nTopup Response: ${JSON.stringify(supplierResponse.original)}
					  \nID: ${paymentInfo.id} - Amount: ${t} - AccountId: ${payload.accountId}`;
					// NotifyService.SendAlertNotify(alertMsg, [], ['api']);

					await Broker.call('v1.eWalletPaymentLinkedBIDVModel.updateOne', [{
						id: paymentInfo.id
					}, { state: SupplierConstant.STATE.FAILED, supplierResponsed }]);

					await RedisCache.delete({ key: keyLockBankOTP });
					response.state = SupplierConstant.STATE.FAILED;
					response.message = _.get(supplierResponse, 'data.message', 'Thanh toán thất bại!');
					return response;
				}

				const updateSucceedResult = await Broker.call('v1.eWalletPaymentLinkedBIDVModel.updateOne', [{
					id: paymentInfo.id
				}, {
					state: PaymentConstant.STATE.SUCCEEDED,
					supplierResponsed,
					supplierTransaction: _.get(supplierResponse, 'transaction', null)
				}]);

				if (updateSucceedResult.nModified < 1) {
					response.state = PaymentConstant.STATE.FAILED;
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(BIDV_VER_ERR_02)';
				}

				response.supplierTransaction = _.get(supplierResponse, 'transaction', null);
				response.state = PaymentConstant.STATE.SUCCEEDED;
				response.message = _.get(supplierResponse, 'data.message', null) || 'Thanh toán thành công';
				response.paymentLinkedId = paymentInfo.id;

				return response;
			} catch (err) {
				Logger('[BIDV] -> [VERIFY_PAY] -> [EXCEPTION] -> :: ', err);

				response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(BIDV_VER_ERR_03)';
				return response;
			}
		}

	};
};
