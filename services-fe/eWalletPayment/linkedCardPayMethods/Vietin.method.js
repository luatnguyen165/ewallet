const _ = require('lodash');
const moment = require('moment');
const PaymentConstant = require('../constants/payment.constant');
const SupplierConstant = require('../constants/supplier.constant');

const VietinLib = require('../libraries/vietin.library');

module.exports = function () {
	// Payload
	const Logger = this.logger.info;
	const Broker = this.broker;
	const VietinFuncs = VietinLib.bind(this)();
	const RedisCache = this.RedisCache();

	return {
		async Pay(payload) {
			const response = {
				state: PaymentConstant.STATE.FAILED,
				message: 'Thanh toán thất bại. Vui lòng thử lại sau.(VIETIN_ERR)'
			};
			try {
				const paymentInfo = await Broker.call('v1.eWalletPaymentLinkedVietinModel.create', [{
					accountId: payload.accountId,
					paymentId: payload.paymentId,
					transactionId: payload.transaction,
					bankInfo: payload.cardInfo,
					amount: payload.amount,
					description: 'Thanh toán từ VietinBank',
					state: PaymentConstant.STATE.PENDING
				}]);

				if (_.get(paymentInfo, 'id', null) === null) {
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(VIETIN_ERR_01)';
					return response;
				}

				const paramsSend = {
					transaction: payload.transaction,
					accountId: payload.accountId,
					amount: payload.amount,
					cardId: payload.cardId,
					description: 'Thanh toan dich vu tu vi PayME'
				};

				const vietinResponse = await VietinFuncs.Pay(paramsSend);

				const supplierTransaction = _.get(vietinResponse, 'transaction', null);
				const supplierResponsed = _.get(vietinResponse, 'original', {});

				/* Verify OTP case */
				if (vietinResponse.state === SupplierConstant.STATE.REQUIRE_OTP) {
					const updatePendingPayResult = await Broker.call('v1.eWalletPaymentLinkedVietinModel.updateOne', [
						{ id: paymentInfo.id },
						{
							supplierResponsed,
							supplierTransaction
						}
					]);

					if (updatePendingPayResult.nModified < 1) {
						response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(VIETIN_ERR_02)';
						return response;
					}
					response.paymentLinkedId = paymentInfo.id;
					response.supplierTransaction = supplierTransaction;
					response.state = PaymentConstant.STATE.REQUIRE_OTP;
					response.message = 'Vui lòng nhập mã OTP';
					return response;
				}

				/* Failed Case */
				if (vietinResponse.state !== SupplierConstant.STATE.SUCCEEDED) {
					// All not succeed Case is handled here
					const alertMsg = `❗Thanh toán từ Vietin fail
					\nParamRequest: ${JSON.stringify(paramsSend)}
					\nResponse: ${JSON.stringify(vietinResponse)}
					\nPaymentId: ${paymentInfo.id}`;

					// TODO: Notify in here

					const updateFailPayResult = await Broker.call('v1.eWalletPaymentLinkedVietinModel.updateOne', [
						{ id: paymentInfo.id },
						{
							state: PaymentConstant.STATE.FAILED,
							supplierResponsed,
							supplierTransaction
						}
					]);

					if (updateFailPayResult.nModified < 1) {
						response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(VIETIN_ERR_03)';
						return response;
					}

					response.paymentLinkedId = paymentInfo.id;
					response.supplierTransaction = supplierTransaction;
					response.state = PaymentConstant.STATE.FAILED;
					response.message = 'Thanh toán thất bại.(VIETIN_FAILED)';
					return response;
				}

				// Succeeded case
				const updateSucceededPayResult = await Broker.call('v1.eWalletPaymentLinkedVietinModel.updateOne',
					[
						{
							id: paymentInfo.id
						}, {
							state: PaymentConstant.STATE.SUCCEEDED,
							supplierResponsed,
							supplierTransaction
						}
					]);

				if (updateSucceededPayResult.nModified < 1) {
					response.state = PaymentConstant.STATE.FAILED;
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau. (VIETIN_ERR_04)';
					return response;
				}

				response.paymentLinkedId = paymentInfo.id;
				response.supplierTransaction = supplierTransaction;
				response.message = _.get(vietinResponse, 'message', null) || 'Thanh toán thành công';
				response.state = PaymentConstant.STATE.SUCCEEDED;

				return response;
			} catch (err) {
				console.log(err);
				response.state = PaymentConstant.STATE.FAILED;
				response.message = 'Thanh toán thất bại. Vui lòng thử lại sau (VIETIN_ERR_05)';
				Logger('[PAYMENT] -> [PAY] -> [VIETIN] -> [EXCEPTION] :: ', JSON.stringify(err));

				return response;
			}
		},

		async VerifyPay(payload) {
			const response = {
				state: PaymentConstant.STATE.FAILED,
				message: 'Thanh toán thất bại. Vui lòng thử lại sau.(VIETIN_VER_ERR)'
			};
			try {
				const paymentInfo = await Broker.call('v1.eWalletPaymentLinkedVietinModel.findOne', [{
					accountId: payload.accountId,
					paymentId: payload.paymentId,
					transactionId: payload.transaction,
					state: PaymentConstant.STATE.PENDING
				}, '-_id id supplierResponsed']);

				if (_.get(paymentInfo, 'id', null) === null) {
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(VIETIN_VER_ERR_01)';
					return response;
				}

				const paramsSend = {
					transaction: paymentInfo.supplierTransaction,
					otp: payload.otp
				};

				const vietinResponse = await VietinFuncs.VerifyPay(paramsSend);

				const supplierResponsed = _.get(paymentInfo, 'vietinResponsed', []);
				supplierResponsed.push(JSON.stringify(vietinResponse.vietinResponse));

				// Sai ma OTP
				if (vietinResponse.state === PaymentConstant.STATE.INVALID_OTP) {
					let countOTPFailed = await RedisCache.get(`BankTransactionFailedOTP-${payload.accountId}`);
					countOTPFailed = _.toNumber(countOTPFailed) || 0;
					if (countOTPFailed >= 3) {
						await Broker.call('v1.eWalletPaymentLinkedVietinModel.updateOne', [{
							id: paymentInfo.id
						}, {
							state: PaymentConstant.STATE.FAILED,
							supplierResponsed
						}]);
						const ttl = await RedisCache.getTTL(`BankTransactionFailedOTP-${payload.accountId}`);
						let d = moment.duration(ttl, 'seconds').format('m:ss');
						if (d < 60) d = `${d} giây`;
						else {
							d = d.split(':');
							if (d[1] !== '00') d = `${d[0]} phút ${d[1]} giây`;
							else d = `${d[0]} phút`;
						}
						response.state = PaymentConstant.STATE.FAILED;
						response.message = `Bạn đã nhập sai OTP quá 3 lần liên tiếp, vui lòng thử lại sau ${d} hoặc liên hệ bộ phận CSKH để được hỗ trợ!`;
						return response;
					}
					await RedisCache.set(`BankTransactionFailedOTP-${payload.accountId}`, countOTPFailed + 1, 300);
					response.state = PaymentConstant.STATE.INVALID_OTP;
					response.message = _.get(vietinResponse, 'data.message', 'Mã OTP không chính xác');
					return response;
				}

				// Thanh toan Failed
				if (vietinResponse.state === PaymentConstant.STATE.FAILED) {
					const t = Math.round(_.toNumber(payload.total)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
					const alertMsg = `❗Thanh toán từ VietinBank thất bại
					  \nTopup Response: ${JSON.stringify(vietinResponse.original)}
					  \nID: ${paymentInfo.id} - Amount: ${t} - AccountId: ${payload.accountId}`;
					// NotifyService.SendAlertNotify(alertMsg, [], ['api']);

					await Broker.call('v1.eWalletPaymentLinkedVietinModel.updateOne', [{
						id: paymentInfo.id

					}, {
						state: PaymentConstant.STATE.FAILED,
						supplierResponsed
					}]);

					await RedisCache.delete(`BankTransactionFailedOTP-${payload.accountId}`);
					response.paymentLinkedId = paymentInfo.id;
					response.state = PaymentConstant.STATE.FAILED;
					response.message = _.get(vietinResponse, 'data.message', 'Thanh toán thất bại!');
					return response;
				}

				const updatePayResult = await Broker.call('v1.eWalletPaymentLinkedVietinModel.updateOne', [{
					id: paymentInfo.id
				}, {
					state: PaymentConstant.STATE.SUCCEEDED,
					supplierResponsed,
					supplierTransaction: _.get(vietinResponse, 'transaction', null)
				}]);

				if (updatePayResult.nModified < 1) {
					response.state = PaymentConstant.STATE.FAILED;
					response.message = 'Xác thực thanh toán thất bại. Vui lòng thử lại sau (VIETIN_ERR_03)';
					return response;
				}

				response.paymentLinkedId = paymentInfo.id;
				response.supplierTransaction = _.get(vietinResponse, 'transaction', null);
				response.state = vietinResponse.state;
				response.message = _.get(vietinResponse, 'message', null) || 'Thanh toán thành công';

				return response;
			} catch (err) {
				response.state = PaymentConstant.STATE.FAILED;
				response.message = 'Xác thực thanh toán thất bại. Vui lòng thử lại sau (VIETIN_ERR_04)';
				Logger('[PAYMENT] -> [PAY] -> [VIETIN] -> :: ', JSON.stringify(err));
				return response;
			}
		}
	};
};
