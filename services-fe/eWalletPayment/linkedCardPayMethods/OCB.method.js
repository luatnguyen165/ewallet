const _ = require('lodash');
const moment = require('moment');

const PaymentConstant = require('../constants/payment.constant');
const SupplierConstant = require('../constants/supplier.constant');
const OcbLib = require('../libraries/ocb.libarary');

module.exports = function () {
	const Logger = this.logger.info;
	const Broker = this.broker;
	const RedisCache = this.RedisCache();
	const OCBFuncs = OcbLib.bind(this)();

	return {

		/**
		 * Create Payment and call to OCB Supplier
		 * Four mains case
		 * If succeeded -> return state SUCCEEDED into linked Payment
		 * If Require_otp -> return State REQUIRE_OTP into linked Payment
		 * If Failed -> return State FAILED into linked Payment
		 * If Timeout -> return State TIMEOUT into linked Payment
 		 *
		 * Payload
 		 * @param {Number} accountId : AccountId of User
 		 * @param {Number} paymentId : PaymentId to reference
 		 * @param {String} transaction : Transaction of Payment
 		 * @param {Object} cardInfo : CardInfo of linked Card
		 * @param {Number} amount: The amount of Money which User want to Pay
 		 * @param {String} cardId: Card Id of OCB
		 * @param {String} phone: Phone of Account

			@return
			@example
			{
				state: PaymentConstant.STATE.FAILED,
				message: 'Thanh toán thất bại. Vui lòng thử lại sau.(PVC_ERR_01)'
			}
		*/
		async Pay(payload) {
			const response = {
				state: PaymentConstant.STATE.FAILED,
				message: 'Thanh toán thất bại. Vui lòng thử lại sau.(OCB_ERR)'
			};
			try {
				const paymentInfo = await Broker.call('v1.eWalletPaymentLinkedOCBModel.create', [{
					accountId: payload.accountId,
					paymentId: payload.paymentId,
					transactionId: payload.transaction,
					bankInfo: payload.cardInfo,
					amount: payload.amount,
					description: 'Thanh toán từ OCBBank',
					state: PaymentConstant.STATE.PENDING
				}]);

				if (_.get(paymentInfo, 'id', null) === null) {
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(OCB_ERR_01)';
					return response;
				}

				const vanillaPhoneFormat = payload.phone.replace(/^84/, 0);
				const paramsSend = {
					phone: vanillaPhoneFormat,
					amount: payload.amount,
					cardId: payload.cardId
				};

				const supplierResponse = await OCBFuncs.Pay(paramsSend);

				const supplierResponsed = JSON.stringify(supplierResponse.original);
				const supplierTransaction = _.get(supplierResponse, 'transaction', null);
				/* Failed Case */
				if (supplierResponse.state !== SupplierConstant.STATE.SUCCEEDED) {
					// All not succeed Case is handled here
					const alertMsg = `❗Thanh toán từ PVCB fail
					\nParamRequest: ${JSON.stringify(paramsSend)}
					\nResponse: ${JSON.stringify(supplierResponse)}
					\nPaymentId: ${paymentInfo.id}`;

					// TODO: Notify in here
					const updateFailPayResult = await Broker.call('v1.eWalletPaymentLinkedOCBModel.updateOne', [
						{ id: paymentInfo.id },
						{
							state: PaymentConstant.STATE.FAILED,
							supplierResponsed,
							supplierTransaction
						}
					]);

					if (updateFailPayResult.nModified < 1) {
						response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(OCB_ERR_02)';
						return response;
					}

					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.';
					return response;
				}

				/* Verify OTP Case */
				const updateDataResult = await Broker.call('v1.eWalletPaymentLinkedOCBModel.updateOne', [{
					id: paymentInfo.id
				}, {
					supplierResponsed,
					supplierTransaction,
					clientTransId: supplierResponse.clientTransId
				}]);

				if (updateDataResult.nModified < 1) {
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(OCB_ERR_03)';
					return response;
				}

				response.state = PaymentConstant.STATE.REQUIRE_OTP;
				response.supplierTransaction = supplierTransaction;
				response.message = _.get(supplierResponse, 'message', null) || 'Tạo yêu cầu thanh toán thành công';

				return response;
			} catch (err) {
				console.log(err);
				Logger('[OCB] -> [PAY] -> [EXCEPTION] -> :: ', err);

				return response;
			}
		},

		async VerifyPay(payload) {
			const response = {
				state: PaymentConstant.STATE.FAILED,
				message: 'Thanh toán thất bại. Vui lòng thử lại sau.(OCB_VER_ERR)'
			};
			try {
				const paymentInfo = await Broker.call('v1.eWalletPaymentLinkedOCBModel.findOne', [{
					accountId: payload.accountId,
					paymentId: payload.paymentId,
					transactionId: payload.transaction,
					state: PaymentConstant.STATE.PENDING
				}]);

				if (_.get(paymentInfo, 'id', null) === null) {
					response.message = 'Thanh toán thất bại. Không tìm thấy giao dịch';
					return response;
				}

				const vanillaPhoneFormat = payload.phone.replace(/^84/, 0);
				const paramsSend = {
					transaction: paymentInfo.supplierTransaction,
					cardId: payload.cardId,
					phone: vanillaPhoneFormat,
					amount: payload.amount,
					otp: payload.otp
				};

				const ocbResponse = await OcbLib.VerifyPay(paramsSend);

				const supplierResponsed = _.get(paymentInfo, 'supplierResponsed', []);
				supplierResponsed.push(JSON.stringify(ocbResponse.original));
				// Sai ma OTP
				const keyLockBankOTP = `BankTransactionFailedOTP-${payload.accountId}`;
				if (ocbResponse.state === SupplierConstant.STATE.INVALID_OTP) {
					let countOTPFailed = await RedisCache.get({ key: keyLockBankOTP });
					countOTPFailed = _.toNumber(countOTPFailed) || 0;
					if (countOTPFailed >= 3) {
						// update Payment failed and show locked time
						await Broker.call('v1.eWalletPaymentLinkedOCBModel.updateOne', [{
							id: paymentInfo.id
						}, { state: SupplierConstant.STATE.FAILED }]);

						const ttl = await RedisCache.getTTL({ key: keyLockBankOTP });
						let d = moment.duration(ttl, 'seconds').format('m:ss');
						if (d < 60) d = `${d} giây`;
						else {
							d = d.split(':');
							if (d[1] !== '00') d = `${d[0]} phút ${d[1]} giây`;
							else d = `${d[0]} phút`;
						}
						response.state = SupplierConstant.STATE.FAILED;
						response.message = `Bạn đã nhập sai OTP quá 3 lần liên tiếp, vui lòng thử lại sau ${d} hoặc liên hệ bộ phận CSKH để được hỗ trợ!`;
						return response;
					}
					await RedisCache.set(`BankTransactionFailedOTP-${payload.accountId}`, countOTPFailed + 1, 300);
					response.state = SupplierConstant.STATE.INVALID_OTP;
					response.message = _.get(ocbResponse, 'data.message', 'Thanh toán thất bại');
					return response;
				}

				// Thanh toan Failed
				if (ocbResponse.state === SupplierConstant.STATE.FAILED) {
					const t = Math.round(_.toNumber(payload.total)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
					const alertMsg = `❗Thanh toán từ OCBBank thất bại
					  \nTopup Response: ${JSON.stringify(ocbResponse.original)}
					  \nID: ${paymentInfo.id} - Amount: ${t} - AccountId: ${payload.accountId}`;

					// TODO : Send Telegram Message
					// NotifyService.SendAlertNotify(alertMsg, [], ['api']);

					const updateFailResult = await Broker.call('v1.eWalletPaymentLinkedOCBModel.updateOne', [{
						id: paymentInfo.id

					}, { state: SupplierConstant.STATE.FAILED, supplierResponsed }]);

					if (updateFailResult.nModified < 1) {
						response.state = SupplierConstant.STATE.FAILED;
						response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(OCB_VER_ERR_01)';
						return response;
					}
					await RedisCache.delete({ key: keyLockBankOTP });
					response.state = SupplierConstant.STATE.FAILED;
					response.message = _.get(ocbResponse, 'data.message', 'Thanh toán thất bại!');
					return response;
				}

				const updateSucceedResult = await Broker.call('v1.eWalletPaymentLinkedOCBModel.updateOne', [{
					id: paymentInfo.id
				}, {
					bankTransactionId: _.get(ocbResponse, 'bankTransactionId', null),
					supplierResponsed,
					state: SupplierConstant.STATE.SUCCEEDED,
					clientTransId: _.get(ocbResponse, 'clientTransId', null),
					supplierTransaction: _.get(ocbResponse, 'transaction', null)
				}]);

				if (updateSucceedResult.nModified < 1) {
					response.state = SupplierConstant.STATE.FAILED;
					response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(OCB_VER_ERR_02)';
					return response;
				}

				response.supplierTransaction = _.get(ocbResponse, 'transaction', null);
				response.state = SupplierConstant.STATE.SUCCEEDED;
				response.message = _.get(ocbResponse, 'data.message', null) || 'Thanh toán thành công';
				response.paymentLinkedId = paymentInfo.id;

				return response;
			} catch (err) {
				response.state = SupplierConstant.STATE.FAILED;
				response.message = 'Thanh toán thất bại. Vui lòng thử lại sau.(OCB_VER_ERR_03)';
				Logger('[OCB] -> [VERIFY_PAY] -> [EXCEPTION] ->', err);
				return response;
			}
		}

	};
};
