const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const generalConstant = require('../constants/general.constant');
const _ = require('lodash');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	alias: {
		type: String,
		required: true
	},
	createdAt: {
		type: Date,
		default: new Date()
	},
	updateTimes: {
		type: Number,
		default: 0
	},
	state :{
		type : String,
		enum: _.values(generalConstant.ALIAS_STATE),
		default: generalConstant.ALIAS_STATE.NEW
	}
}, {
	collection: 'ProfileAlias',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ alias: 1 }, { sparse: false, unique: true });;

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}_id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
