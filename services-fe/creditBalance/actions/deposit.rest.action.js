const { MoleculerError } = require('moleculer').Errors;
const errorCode = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	try {

		this.logger.info(`credit deposit: ${JSON.stringify(ctx.params)}`);

		const {
			accountId, amount, description, referData, supplierId, transaction, language
		} = ctx.params.body;
		this.setLocale(language || 'vi');

		const deposit = await this.Deposit(accountId, amount, description,
			referData, supplierId, transaction);
		if (deposit.succeeded) {
			return {
				code: deposit.code,
				message: deposit.data.message
			};
		}

		throw new MoleculerError(deposit.data.message, deposit.code);
	} catch (err) {
		this.logger.info(`Credit Balance deposit Error: ${err.message}`);
		if (err.name === 'MoleculerError') {
			return {
				code: err.code,
				message: err.message
			};
		}
		return {
			code: errorCode.SYSTEM_ERROR,
			message: errorCode.SYSTEM_ERROR.toString()
		};
	}
};
