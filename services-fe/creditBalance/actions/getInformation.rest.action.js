const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const errorCode = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	try {
		const { accountId, language } = ctx.params.body;
		this.setLocale(language || 'vi');

		const aggregation = await this.broker.call('v1.creditBalanceModel.aggregate', [
			[
				{
					$match: {
						accountId
					}
				},
				{
					$group: {
						_id: 0,
						deposit: {
							$sum: {
								$cond: [
									{
										$eq: ['$change', '+']
									},
									'$amount',
									0
								]
							}
						},
						withdraw: {
							$sum: {
								$cond: [
									{
										$eq: ['$change', '-']
									},
									'$amount',
									0
								]
							}
						}
					}
				},
				{
					$project: {
						_id: null,
						deposit: '$deposit',
						withdraw: '$withdraw',
						amount: {
							$subtract: ['$deposit', '$withdraw']
						}
					}
				}
			]
		]);

		return {
			code: errorCode.CREDIT_BALANCE_SERVICE.GET_INFORMATION_SUCCEEDED,
			message: this.__(errorCode.CREDIT_BALANCE_SERVICE.GET_INFORMATION_SUCCEEDED.toString()),
			data: {
				accountId,
				cash: _.get(aggregation, '[0].amount', 0)
			}
		};
	} catch (err) {
		this.logger.info(`Credit Balance information Error: ${err.message}`);
		if (err.name === 'MoleculerError') {
			return {
				code: err.code,
				message: err.message
			};
		}
		return {
			code: errorCode.SYSTEM_ERROR,
			message: errorCode.SYSTEM_ERROR.toString()
		};
	}
};
