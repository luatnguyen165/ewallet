const { MoleculerError } = require('moleculer').Errors;

module.exports = async function (ctx) {
	try {

		this.logger.info(`credit deposit: ${JSON.stringify(ctx.params)}`);

		const {
			accountId, amount, description, referData, supplierId, transaction
		} = ctx.params;
		const { language } = ctx.meta;
		this.setLocale(language || 'vi');

		const deposit = await this.Deposit(accountId, amount, description,
			referData, supplierId, transaction);
		if (deposit.succeeded) {
			return {
				succeeded: true,
				message: deposit.data.message
			};
		}

		throw new MoleculerError(deposit.data.message, deposit.code);
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Credit Balance Deposit Error: ${err.message}`, 99);
	}
};
