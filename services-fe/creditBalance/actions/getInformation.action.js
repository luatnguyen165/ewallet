const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const errorCode = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	try {
		const { accountId } = ctx.params;

		const aggregation = await this.broker.call('v1.creditBalanceModel.aggregate', [
			[
				{
					$match: {
						accountId
					}
				},
				{
					$group: {
						_id: 0,
						deposit: {
							$sum: {
								$cond: [
									{
										$eq: ['$change', '+']
									},
									'$amount',
									0
								]
							}
						},
						withdraw: {
							$sum: {
								$cond: [
									{
										$eq: ['$change', '-']
									},
									'$amount',
									0
								]
							}
						}
					}
				},
				{
					$project: {
						_id: null,
						deposit: '$deposit',
						withdraw: '$withdraw',
						amount: {
							$subtract: ['$deposit', '$withdraw']
						}
					}
				}
			]
		]);

		return {
			creditBalance: {
				accountId,
				cash: _.get(aggregation, '[0].amount', 0)
			}
		};
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Credit Balance information Error: ${err.message}`, 99);
	}
};
