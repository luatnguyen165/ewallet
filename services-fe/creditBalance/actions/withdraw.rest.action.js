const { MoleculerError } = require('moleculer').Errors;
const errorCode = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	try {
		const {
			accountId, amount, description, referData, supplierId, transaction, language
		} = ctx.params.body;
		this.setLocale(language || 'vi');

		const withdraw = await this.Withdraw(accountId, amount, description, referData, supplierId, transaction);
		if (withdraw.succeeded) {
			return {
				code: withdraw.code,
				message: withdraw.data.message
			};
		}
		throw new MoleculerError(withdraw.data.message, withdraw.code);
	} catch (err) {
		this.logger.info(`Credit Balance withdraw Error: ${err.message}`);
		if (err.name === 'MoleculerError') {
			return {
				code: err.code,
				message: err.message
			};
		}
		return {
			code: errorCode.SYSTEM_ERROR,
			message: errorCode.SYSTEM_ERROR.toString()
		};
	}
};
