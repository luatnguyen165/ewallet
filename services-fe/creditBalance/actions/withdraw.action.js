const { MoleculerError } = require('moleculer').Errors;
const errorCode = require('../constants/errorCode.constant');

module.exports = async function (ctx) {
	try {
		const {
			accountId, amount, description, referData, supplierId, transaction
		} = ctx.params;
		const { language } = ctx.meta;
		this.setLocale(language || 'vi');

		const withdraw = await this.Withdraw(accountId, amount, description, referData, supplierId, transaction);
		if (withdraw.succeeded) {
			return {
				code: withdraw.data.code,
				message: withdraw.data.message
			};
		}
		throw new MoleculerError(withdraw.data.message, withdraw.code);
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;

		throw new MoleculerError(`Credit Balance withdraw Error: ${err.message}`, 99);
	}
};
