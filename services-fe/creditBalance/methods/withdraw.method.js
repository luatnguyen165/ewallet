const Decimal = require('decimal.js');
const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const errorCode = require('../constants/errorCode.constant');

module.exports = async function (accountId, amount, description, referData = {}, supplierId, transaction) {
	let unlock;
	const response = {
		code: errorCode.CREDIT_BALANCE_SERVICE.WITHDRAW_FAILED,
		succeeded: false,
		data: {
			message: this.__(errorCode.CREDIT_BALANCE_SERVICE.WITHDRAW_FAILED.toString())
		}
	};
	try {
		unlock = await this.broker.cacher.lock(`credit_balance_${accountId}`);

		const checkDupTrans = await this.broker.call('v1.creditBalanceModel.findOne', [{ transaction, supplierId }]);

		if (_.get(checkDupTrans, 'id', false) !== false) {
			response.code = errorCode.CREDIT_BALANCE_SERVICE.TRANSACTION_ALREADY_EXIST;
			response.succeeded = false;
			response.data.message = this.__(errorCode.CREDIT_BALANCE_SERVICE.TRANSACTION_ALREADY_EXIST.toString());
			return response;
		}

		const aggregation = await this.broker.call('v1.creditBalanceModel.aggregate', [
			[
				{
					$match: {
						accountId
					}
				},
				{
					$group: {
						_id: 0,
						deposit: {
							$sum: {
								$cond: [
									{
										$eq: ['$change', '+']
									},
									'$amount',
									0
								]
							}
						},
						withdraw: {
							$sum: {
								$cond: [
									{
										$eq: ['$change', '-']
									},
									'$amount',
									0
								]
							}
						}
					}
				},
				{
					$project: {
						_id: null,
						deposit: '$deposit',
						withdraw: '$withdraw',
						amount: {
							$subtract: ['$deposit', '$withdraw']
						}
					}
				}
			]
		]);

		console.log(_.get(aggregation, '[0].amount', 0));

		const twCash = new Decimal(_.get(aggregation, '[0].amount', 0));
		const tAmount = new Decimal(amount);
		if (tAmount.gt(twCash) === true) {
			response.code = errorCode.CREDIT_BALANCE_SERVICE.NOT_ENOUGH_BALANCE;
			response.succeeded = false;
			response.data.message = this.__(errorCode.CREDIT_BALANCE_SERVICE.NOT_ENOUGH_BALANCE.toString());
			return response;
		}

		let creditBalance;
		try {
			creditBalance = await this.broker.call('v1.creditBalanceModel.create', [{
				accountId,
				transaction,
				change: '-',
				amount,
				description,
				referData,
				supplierId
			}]);
		} catch (err) {
			console.log('Error creditBalance withdraw', err);
		}

		if (_.get(creditBalance, 'id', null) === null) {
			response.succeeded = false;
			response.code = errorCode.CREDIT_BALANCE_SERVICE.WITHDRAW_FAILED;
			response.data.message = this.__(errorCode.CREDIT_BALANCE_SERVICE.WITHDRAW_FAILED.toString());
			return response;
		}

		response.code = errorCode.CREDIT_BALANCE_SERVICE.WITHDRAW_SUCCEEDED;
		response.succeeded = true;
		response.data.message = this.__(errorCode.CREDIT_BALANCE_SERVICE.WITHDRAW_SUCCEEDED.toString());
		return response;
	} catch (err) {
		throw new MoleculerError(err);
	} finally {
		if (_.isFunction(unlock)) { await unlock(); }
	}
};
