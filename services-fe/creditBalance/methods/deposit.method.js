const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;

const errorCode = require('../constants/errorCode.constant');

module.exports = async function (accountId, amount, description, referData = {}, supplierId, transaction) {
	let unlock;
	const response = {
		code: errorCode.CREDIT_BALANCE_SERVICE.DEPOSIT_FAILED,
		succeeded: false,
		data: {
			message: this.__(errorCode.CREDIT_BALANCE_SERVICE.DEPOSIT_FAILED.toString())
		}
	};
	try {
		unlock = await this.broker.cacher.lock(`credit_balance_${accountId}`);

		const checkDupTrans = await this.broker.call('v1.creditBalanceModel.findOne', [{ transaction, supplierId }]);

		if (_.get(checkDupTrans, 'id', false) !== false) {
			response.code = errorCode.CREDIT_BALANCE_SERVICE.TRANSACTION_ALREADY_EXIST;
			response.data.message = this.__(errorCode.CREDIT_BALANCE_SERVICE.TRANSACTION_ALREADY_EXIST.toString());
			return response;
		}

		let creditBalance;
		try {
			creditBalance = await this.broker.call('v1.creditBalanceModel.create', [
				{
					accountId,
					transaction,
					supplierId,
					change: '+',
					amount,
					description,
					referData
				}
			]);
		} catch (error) {
			this.logger.info('creditBalance deposit failed : ', error);
		}

		if (_.get(creditBalance, 'id', null) === null) {
			response.code = errorCode.CREDIT_BALANCE_SERVICE.DEPOSIT_FAILED;
			response.succeeded = false;
			response.data.message = this.__(errorCode.CREDIT_BALANCE_SERVICE.DEPOSIT_FAILED.toString());
		}
		response.code = errorCode.CREDIT_BALANCE_SERVICE.DEPOSIT_SUCCEEDED;
		response.succeeded = true;
		response.data.message = this.__(errorCode.CREDIT_BALANCE_SERVICE.DEPOSIT_SUCCEEDED.toString());
		return response;
	} catch (err) {
		throw new MoleculerError(err);
	} finally {
		if (_.isFunction(unlock)) { await unlock(); }
	}
};
