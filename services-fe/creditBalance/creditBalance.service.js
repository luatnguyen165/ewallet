const moleculerI18n = require('moleculer-i18n-js');
const path = require('path');

module.exports = {
	name: 'creditBalance',

	version: 1,

	mixins: [moleculerI18n],

	i18n: {
		directory: path.join(__dirname, 'locales'),
		locales: ['vi', 'en'],
		defaultLocale: 'vi'
	},

	/**
	 * Settings
	 */
	settings: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		deposit: {
			params: {
				accountId: 'number',
				amount: 'number|min:1000',
				description: 'string|optional',
				referData: 'object|optional',
				supplierId: 'string',
				transaction: 'string'
			},
			handler: require('./actions/deposit.action')
		},
		withdraw: {
			params: {
				accountId: 'number',
				amount: 'number|min:1000',
				description: 'string|optional',
				referData: 'object|optional',
				supplierId: 'string',
				transaction: 'string'
			},
			handler: require('./actions/withdraw.action')
		},
		getInformation: {
			params: {
				accountId: 'number'
			},
			handler: require('./actions/getInformation.action')
		},
		getInformationRest: {
			rest: {
				method: 'POST',
				fullPath: '/creditBalance/information',
				auth: {
					strategies: ['Local'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					accountId: 'number|optional',
					language: 'string|optional|default:vi'
				}
			},
			handler: require('./actions/getInformation.rest.action')
		},
		depositRest: {
			rest: {
				method: 'POST',
				fullPath: '/creditBalance/deposit',
				auth: {
					strategies: ['Local'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					accountId: 'number',
					amount: 'number|min:1000',
					description: 'string|optional',
					referData: 'object|optional',
					supplierId: 'string',
					transaction: 'string',
					language: 'string|optional|default:vi'
				}
			},
			handler: require('./actions/deposit.rest.action')
		},
		withdrawRest: {
			rest: {
				method: 'POST',
				fullPath: '/creditBalance/withdraw',
				auth: {
					strategies: ['Local'],
					mode: 'required'
				}
			},
			params: {
				body: {
					$$type: 'object',
					accountId: 'number',
					amount: 'number|min:1000',
					description: 'string|optional',
					referData: 'object|optional',
					supplierId: 'string',
					transaction: 'string',
					language: 'string|optional|default:vi'
				}
			},
			handler: require('./actions/withdraw.rest.action')
		}
	},

	async started() {
		setTimeout(async () => {
			// const test = await this.broker.call('v1.creditBalance.deposit', {
			// 	accountId: 12,
			// 	amount: 5000000,
			// 	description: 'Vay tieu dung',
			// 	supplierId: '22',
			// 	transaction: '12313131'
			// });
			// const test = await this.broker.call('v1.creditBalance.withdraw', {
			// 	accountId: 12,
			// 	amount: 500000,
			// 	description: 'Thanh toan hoa don dien',
			// 	supplierId: '22',
			// 	transaction: '1231313111'
			// });
			// const test = await this.broker.call('v1.creditBalance.getInformation', {
			// 	accountId: 12
			// });
			// this.logger.info(test);
		}, 10);
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		Deposit: require('./methods/deposit.method'),
		Withdraw: require('./methods/withdraw.method')
	}
};
