module.exports = {
	SYSTEM_ERROR: 500,
	CREDIT_BALANCE_SERVICE: {
		DEPOSIT_SUCCEEDED: 147000,
		DEPOSIT_FAILED: 147001,
		TRANSACTION_ALREADY_EXIST: 147002,

		WITHDRAW_SUCCEEDED: 147100,
		WITHDRAW_FAILED: 147101,
		TRANSACTION_ALREADY_EXIST: 147102,
		NOT_ENOUGH_BALANCE: 147103,

		GET_INFORMATION_SUCCEEDED: 147200
	}
};
