const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		require: true
	},
	appId: {
		type: Number,
		require: true
	},
	phone: {
		type: String,
		require: true
	},
	payType: {
		type: String,
		default: null
	},
	qrCodeInfo: {
		providerCode: {
			type: String,
			default: null
		},
		serviceCode: {
			type: String,
			default: null
		},
		customerName: {
			type: String,
			default: null
		},
		customerAddress: {
			type: String,
			default: null
		},
		billDetail: {
			type: String,
			default: null
		},
		merchantName: {
			type: String,
			default: null
		},
		payLocation: {
			type: String,
			default: null
		},
		payLocationCode: {
			type: String,
			default: null
		},
		description: {
			type: String,
			default: null
		}
	},
	transaction: {
		type: String,
		require: true,
		unique: true
	},
	partnerTransaction: {
		type: String,
		default: null
	},
	amount: {
		type: Number,
		require: true
	},
	fee: {
		type: Number,
		default: 0
	},
	discount: {
		type: Number,
		default: 0
	},
	total: {
		type: Number,
		require: true
	},
	voucherCode: {
		type: String,
		default: null
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.STATE)
	},
	description: {
		type: String,
		default: null
	},
	qrContent: [{
		quantity: String,
		note: String,
		qrInfor: String
	}],
	note: {
		type: String,
		default: null
	},
	connectorTransaction: {
		type: String,
		default: null
	},
	vnPayTransaction: {
		type: String,
		default: null
	},
	payQRCodeResponsed: Object,
	payment: Object
}, {
	collection: 'Service_PayQRCode',
	versionKey: false,
	timestamps: true
});

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
