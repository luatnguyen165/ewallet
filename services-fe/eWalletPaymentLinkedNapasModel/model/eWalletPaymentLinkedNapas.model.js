const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const _ = require('lodash');
const generalConstant = require('../constants/general.constant');

const MongoSchema = mongoose.Schema;

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	transactionId: {
		type: String,
		required: true
	},
	paymentId: {
		type: String,
		default: null
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	},
	state: {
		type: String,
		enum: _.values(generalConstant.PAYMENT_STATE)
	},
	description: {
		type: String,
		default: null
	},
	bankInfo: {
		linkedId: {
			type: Number, // id tới bảng linked Bank
			required: true
		},
		bankCode: {
			type: String
		},
		swiftCode: {
			type: String
		},
		cardNumber: {
			type: String
		},
		cardHolder: {
			type: String,
			default: null,
			required: true
		},
		issuedAt: {
			type: Date
		},
		expiredAt: {
			type: Date,
			default: null
		},
		description: {
			type: String
		},
		methodId: {
			type: Number
		}
	},
	supplierTransaction: {
		type: String,
		default: null
	},
	extraData: MongoSchema.Types.Mixed,
	supplierResponsed: MongoSchema.Types.Mixed,
	isChecked: {
		type: Boolean,
		default: false
	}
}, {
	collection: 'Payment_LinkedNapas',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });
Schema.index({ state: 1 });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
