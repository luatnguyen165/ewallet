const _ = require('lodash');
// const Redis = require('ioredis');
const moleculerCron = require('moleculer-cron');
const moleculerRabbitmq = require('moleculer-rabbitmq');
const generalConstant = require('./constants/general.constant');

const queueMixin = moleculerRabbitmq({
	connection: process.env.RABBITMQ_URI,
	asyncActions: true
});

global.Variable = {
	isReady: false
};

module.exports = {
	name: 'walletUuid',

	version: 1,

	mixins: [moleculerCron, queueMixin],

	/**
	 * Settings
	 */
	settings: {
	},

	/**
			 * Dependencies
			 */
	dependencies: [],

	// crons: [
	// {
	// 	name: 'GENERATE_UUID',
	// 	cronTime: '*/20 * * * * *',
	// 	async onTick() {
	// 		console.log('Run Cron generate_uuid');
	// 		await this.call('v1.walletUuid.generateUuid', {});
	// 	}
	// }
	// ],

	/**
			 * Actions
			 */
	actions: {
		pick: {
			params: {
				prefix: 'string|uppercase'
			},
			handler: require('./actions/pick.action')
		},
		getUuidPrefix: {
			params: {
			},
			handler: require('./actions/getUuidPrefix.action')
		}
		// generateUuidTask: {
		// 	queue: {
		// 		amqp: {
		// 			prefetch: 1
		// 		},
		// 		retry: {
		// 			max_retry: 2,
		// 			delay: (retryCount) => retryCount * 5000
		// 		}
		// 	},
		// 	params: {
		// 		prefix: 'string|optional'
		// 	},
		// 	dedupHash: (ctx) => `GENERATE_UUID${_.get(ctx, 'params.body.prefix', null)}`,
		// 	timeout: 60000,
		// 	handler: require('./actions/generateUuidTask.action')
		// },
		// generateRecordUuidTask: {
		// 	queue: {
		// 		amqp: {
		// 			prefetch: 1
		// 		},
		// 		retry: {
		// 			max_retry: 2,
		// 			delay: (retryCount) => retryCount * 5000
		// 		}
		// 	},
		// 	params: {
		// 		prefix: 'string',
		// 		uuid: 'string'
		// 	},
		// 	dedupHash: (ctx) => `CREATE_RECORD_UUID_${_.get(ctx, 'params.body.prefix', null)}_${_.get(ctx, 'params.body.uuid', null)}`,
		// 	timeout: 60000,
		// 	handler: require('./actions/createRecordUuid.task')
		// }

		// generateUuid: {
		// 	params: {},
		// 	handler: require('./actions/generateUuid.action')
		// }
	},

	/**
			 * Events
			 */
	events: {

	},

	/**
			 * Methods
			 */
	methods: {
		RedisCache: require('./methods/redisCache.method'),
		Generate: require('./methods/generate.method')
	},

	/**
			 * Service created lifecycle event handler
			 */
	async created() {
		// const redis = new Redis({
		// 	port: _.toInteger(process.env.WALLET_REDIS_PORT),
		// 	host: process.env.WALLET_REDIS_HOST,
		// 	db: _.toInteger(process.env.WALLET_REDIS_DB)
		// });
		// this.$redis = redis;
		// return true;

	},

	/**
			 * Service started lifecycle event handler
			 */
	async started() {
		// do nothing
		setTimeout(async () => {
			await this.broker.call('v1.walletUuid.getUuidPrefix');
		}, 1000);
	},

	/**
	 * Service stopped lifecycle event handler
	 */

	async afterConnected() {
	},

	/**
	 * Service stopped lifecycle event handler
	 */

	async stopped() {
		// this.$redis.disconnect();
	}
};
