const Private = {
	Genegate(chars, length) {
		let s = '';
		for (let i = 0; i < length; i += 1) {
			const pos = (Math.floor(Math.random() * Math.floor(chars.length)));
			s += chars[pos];
		}
		return s.toUpperCase().toString();
	},
	GenegateNumber(length) {
		const chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
		return Private.Genegate(chars, length);
	},
	GenerateAlphabet(length) {
		const chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
			'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
			'w', 'x', 'y', 'z'];
		return Private.Genegate(chars, length);
	},
	GenerateAlphabetAndNumber(length) {
		const chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
			'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
			'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9'];
		return Private.Genegate(chars, length);
	}
};

module.exports = function (payload) {
	let randomString = '';
	if (payload.type === 'NUMBER') {
		randomString = Private.GenegateNumber(payload.length);
	} else if (payload.type === 'ALPHABET') {
		randomString = Private.GenerateAlphabet(payload.length);
	} else {
		randomString = Private.GenerateAlphabetAndNumber(payload.length);
	}

	return randomString;
};
