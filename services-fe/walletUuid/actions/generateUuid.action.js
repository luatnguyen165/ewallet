module.exports = async function (ctx) {
	const Logger = this.logger.info;
	try {
		// const URL_REDIS = process.env.CACHER;
		// Logger('URL_DB_REDIS => ', URL_REDIS);
		const Broker = this.broker;
		// lay het Prefix trong DB
		const prefixInfos = await Broker.call('v1.walletUuidPrefixModel.findMany', [{ isActive: true }]);

		if (prefixInfos.length === 0) {
			return true;
		}
		for (let i = 0; i < prefixInfos.length; i += 1) {
			Broker.call('v1.walletUuid.generateUuidTask.async', {
				params: {
					prefix: prefixInfos[i].prefix
				}
			});
		}
		return true;
	} catch (e) {
		Logger('GENERATE_UUID_EXCEPTION', e);
		console.log(e);
		console.log('Loi ');
	}
};
