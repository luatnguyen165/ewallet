const _ = require('lodash');
const generalConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	const { params } = ctx;
	try {
		const Broker = this.broker;
		// const RedisCache = this.RedisCache();
		const createUuid = await Broker.call('v1.walletUuidModel.create', [{

			uuid: params.uuid,
			prefix: params.prefix,
			state: generalConstant.UUID_STATE.NEW
		}]);

		// await RedisCache.sadd({ key: params.prefix, value: params.uuid });
	} catch (e) {
		if (_.startsWith(_.get(e, 'message', ''), 'E11000 duplicate key error')) {
			Logger('CREATE_UUID_ERROR => PAYLOAD => ', JSON.stringify(params), ' DUPLICATE RECORD ERROR');
		} else {
			Logger('CREATE_UUID_ERROR => PAYLOAD => ', JSON.stringify(params), ' error => ', e);
		}
	}
};
