const _ = require('lodash');
const { customAlphabet } = require('nanoid');

const generalConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	try {
		// const available = await ctx.broker.call('v1.walletUuidModel.findOne', [
		// 	{
		// 		prefix: ctx.params.prefix,
		// 		state: generalConstant.UUID_STATE.NEW
		// 	}
		// ]);

		// if (_.isNil(available)) {
		// 	return null;
		// }

		// const uuidInfo = await ctx.broker.call('v1.walletUuidModel.findOneAndUpdate', [
		// 	{
		// 		uuid: available.uuid,
		// 		prefix: ctx.params.prefix,
		// 		state: generalConstant.UUID_STATE.NEW
		// 	},
		// 	{
		// 		state: generalConstant.UUID_STATE.USED
		// 	},
		// 	{ new: true }
		// ]);
		// console.log('global.Variable', global.Variable);
		const { prefix } = ctx.params;
		const length = global.Variable[`${prefix}`];
		if (!length) {
			return null;
		}
		const nanoid = customAlphabet('0123456789', length);
		let transaction = '0';
		for (let j = 0; j < 3; j += 1) {
			if (transaction[0] === '0') { transaction = nanoid(); } else break;
		}
		if (transaction[0] === '0') {
			return null;
		}
		return transaction;

		// const uuid = await this.$redis.spop(ctx.params.prefix, 1);
		// if (uuid.length < 1) {
		// 	return null;
		// }

		// const uuidInfo = await ctx.broker.call('v1.walletUuidModel.findOneAndUpdate', [
		// 	{
		// 		uuid: { $in: uuid },
		// 		prefix: ctx.params.prefix,
		// 		state: generalConstant.UUID_STATE.NEW
		// 	},
		// 	{
		// 		state: generalConstant.UUID_STATE.USED
		// 	},
		// 	{ new: true }
		// ]);

		// return uuidInfo.uuid;
	} catch (error) {
		this.logger.info('v1.walletUuid.pick error =>', error.message);
		return null;
	}
};
