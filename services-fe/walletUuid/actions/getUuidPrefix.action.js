const _ = require('lodash');

module.exports = async function (ctx) {
	try {
		// await this.broker.waitForServices('v1.walletUuidPrefixModel');
		const prefixConfig = await this.broker.call('v1.walletUuidPrefixModel.findMany', [{}]);
		_.forEach(prefixConfig, (val) => {
			global.Variable[`${val.prefix}`] = val.length + 1;
		});
		global.Variable.isReady = true;
		this.logger.info('Get Uuid Prefix Successfully');
		return true;
	} catch (error) {
		this.logger.info(`[WalletUuid] Get Uuid Prefix Error ${error}`);
		return false;
	}
};
