const _ = require('lodash');
const AsyncForEach = require('await-async-foreach');
const generalConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	const Logger = this.logger.info;
	try {
		const { prefix } = ctx.params;
		const Broker = this.broker;

		// lay het Prefix trong DB
		// const RedisCache = this.RedisCache();
		const prefixInfo = await Broker.call('v1.walletUuidPrefixModel.findOne', [{
			prefix
		}]);

		// Logger('CREATE_UUID_FOR_PREFIX   ', prefix);
		if (_.get(prefixInfo, 'prefix', null) === null) {
			Logger('GET_WILLING_AVAILABLE_UUID_FAILED,UUID_NOT_FOUND =>', prefix);
			return {
				state: 'FAILED',
				message: 'Không tìm thấy thông tin UUID'
			};
		}

		let UUID_AVALABLE = 200;

		try {
			UUID_AVALABLE = _.parseInt(process.env.UUID_AVAILABLE_ITEM);
		} catch (e) {
			Logger(`GET_WILLING_AVAILABLE_UUID_FAILED, DEFAULT IS ${UUID_AVALABLE}`);
		}

		if (_.isNaN(UUID_AVALABLE)) {
			UUID_AVALABLE = 200;
		}

		const availableItem = await Broker.call('v1.walletUuidModel.aggregate', [[{
			$match: {
				prefix,
				state: generalConstant.UUID_STATE.NEW
			}
		},
		{ $group: { _id: null, count: { $sum: 1 } } },
		{ $project: { _id: 0 } }]
		]);


		if (_.get(availableItem, '[0].count', 0) >= UUID_AVALABLE) {
			return true;
		}
		Logger(`LOWER_UUID NUMBER: ${_.get(availableItem, '[0].count', 0)}, UUID_NAME =>  ${prefixInfo.prefix}`);

		const availableNumber = _.get(availableItem, '[0].count', 0);
		let count = UUID_AVALABLE;
		if (UUID_AVALABLE - availableNumber <= count) {
			count = UUID_AVALABLE - availableNumber;
		}

		const uuidGenerate = [];

		for (let i = 0; i < count; i += 1) {
			const uuid = this.Generate({
				type: prefixInfo.type,
				length: prefixInfo.length
			});

			uuidGenerate.push(uuid);
		}
		// const uuidIntersecs = [];
		// await AsyncForEach(uuidGenerate, async (uuid) => {
		// 	const checkExist = await RedisCache.sismember({ set: prefixInfo.prefix, key: uuid });

		// 	if (checkExist !== 0) {
		// 		uuidIntersecs.push(uuid);
		// 	}
		// }, 'parallel', 10);

		// const uuidAvailable = _.remove(uuidGenerate, (uuid) => !_.includes(uuidIntersecs, uuid));

		// await RedisCache.sadd({ key: prefixInfo.prefix, value: uuidAvailable });
		// Call createUUID
		_.forEach(uuidGenerate, (uuid) => {
			Broker.call('v1.walletUuid.generateRecordUuidTask.async', {
				params: {
					prefix: prefixInfo.prefix,
					uuid
				}
			});
		});

		return true;
	} catch (e) {
		Logger('ERROR WHEN GENERATE UUID =>', e);
		console.log(e);
		return false;
	}
};
