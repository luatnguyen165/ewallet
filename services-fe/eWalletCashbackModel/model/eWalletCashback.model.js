const _ = require('lodash');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const GeneralConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true
	},
	transaction: String,
	service: {
		type: String,
		required: true,
		enum: _.values(GeneralConstant.SERVICES)
	},
	value: {
		type: Number,
		default: 0
	},
	amount: {
		type: Number,
		default: 0
	},
	referId: Number,
	description: {
		type: String,
		default: null
	},
	state: {
		type: String,
		enum: _.values(GeneralConstant.CASHBACK_STATE),
		default: GeneralConstant.CASHBACK_STATE.SUCCEEDED
	}
}, {
	collection: 'Service_Cashback',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: false, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
