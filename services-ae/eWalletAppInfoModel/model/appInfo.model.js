const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const { customAlphabet } = require('nanoid');
const { alphanumeric } = require('nanoid-dictionary');

const AccountConstant = require('../constants/AccountConstant');
const _ = require('lodash');

const nanoId = customAlphabet(alphanumeric, 15);

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	accountId: { // account ID ben vi
		type: String,
		default: null
	},
	phone: { // Phone cua ben doi tac
		type: String,
		default: null
	},
	appCode: {
		type: String,
		default: null
	},
	secretKey: {
		type: String,
		default: null
	},
	isActive: {
		type: Boolean,
		default: true
	},
	description: {
		type: String,
		default: null
	},
	address: {
		type: String,
		default: null
	},
	location: {
		province: String,
		district: String,
		wards: String,
		address: String
	},
	setting: {
		logo: String,
		backgroundColor: String,
		fontName: String,
		brandName: String
	},
	storeName: {
		type: String,
		default: null
	},
	storeId: {
		type: Number,
		default: true
	},
	merchantId: {
		type: Number,
		default: null
	},
	merchantName: {
		type: String,
		default: null
	},
	gatewayAccessToken: {
		type: String,
		default: null
	},
	partnerPublicKey: {
		type: String,
		default: null
	},
	payme: {
		privateKey: {
			type: String,
			default: null
		},
		publicKey: {
			type: String,
			default: null
		}
	},
	callbackURL: {
		type: String,
		default: null
	},
	linkedFlow: {
		type: String,
		enum: _.values(AccountConstant.APP_LINKED_FLOW)
	}
}, {
	collection: 'AppInfo',
	versionKey: false,
	timestamps: true
});
/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
