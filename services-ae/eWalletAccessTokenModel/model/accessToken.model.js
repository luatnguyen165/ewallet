const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const { customAlphabet } = require('nanoid');
const { alphanumeric } = require('nanoid-dictionary');
const Moment = require('moment');

const _ = require('lodash');
const AccountConstant = require('../constants/AccountConstant');

const nanoId = customAlphabet(alphanumeric, 15);

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number
	},
	appId: {
		type: Number,
		default: null
	},
	expiredAt: {
		type: Date,
		default: Moment(new Date()).add(30, 'days')
	},
	type: {
		type: String,
		default: AccountConstant.ACCESS_TOKEN_TYPE.AUTH
	},
	platform: {
		type: String,
		default: null
	},
	scope: [String]
}, {
	collection: 'AccessToken',
	versionKey: false,
	timestamps: true
});
/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
