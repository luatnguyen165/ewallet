const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const { customAlphabet } = require('nanoid');
const { alphanumeric } = require('nanoid-dictionary');

const _ = require('lodash');
const AccountConstant = require('../constants/AccountConstant');
const Moment = require('moment');

const nanoId = customAlphabet(alphanumeric, 15);

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	phone: {
		type: String,
		required: true
	},
	appId: {
		type: Number,
		default: null
	},
	code: {
		type: String,
		required: true
	},
	source: {
		type: String,
		enum: _.values(AccountConstant.ACTIVE_CODE_SOURCE)
	},
	deviceId: {
		type: String,
		default: null
	},
	clientId: {
		type: String,
		default: null
	},
	expiredAt: {
		type: Date,
		default: Moment(new Date()).add(15, 'minutes')
	}
}, {
	collection: 'ActiveCode',
	versionKey: false,
	timestamps: true
});
	/*
	| ==========================================================
	| Plugins
	| ==========================================================
	*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
	| ==========================================================
	| Methods
	| ==========================================================
	*/

/*
	| ==========================================================
	| HOOKS
	| ==========================================================
	*/

module.exports = mongoose.model(Schema.options.collection, Schema);
