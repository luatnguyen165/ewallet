const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');
const generalConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	id: {
		type: Number,
		required: true,
		unique: true
	},
	accountGroupId: {
		type: Number,
		default: null
	},
	phone: {
		type: String,
		required: true
	},
	password: {
		type: String
	},
	fullname: {
		type: String,
		default: null
	},
	avatar: {
		type: String,
		default: null
	},
	updatedAvatarAt: {
		type: Date,
		default: null
	},
	gender: {
		type: String,
		enum: _.values(generalConstant.ACCOUNT_GENDER)
	},
	birthday: {
		type: Date,
		default: null
	},
	address: {
		street: { type: String, default: null },
		city: {
			title: { type: String, default: null },
			identifyCode: { type: String, default: null },
			path: { type: String, default: null }
		},
		district: {
			title: { type: String, default: null },
			identifyCode: { type: String, default: null },
			path: { type: String, default: null }
		},
		ward: {
			title: { type: String, default: null },
			identifyCode: { type: String, default: null },
			path: { type: String, default: null }
		}
	},
	email: {
		type: String,
		default: null
	},
	channel: {
		type: Object,
		default: {}
	},
	isActive: {
		type: Boolean,
		default: true
	},
	state: {
		type: String,
		enum: _.values(generalConstant.ACCOUNT_STATE),
		default: generalConstant.ACCOUNT_STATE.OPENED
	},
	lastedLoginAt: {
		type: Date,
		default: null
	},
	lastedLogoutAt: {
		type: Date,
		default: null
	},
	createdDeviceInfo: {
		platform: {
			type: String,
			default: generalConstant.PLATFORM.UNKNOWN
		},
		channel: {
			type: String,
			default: null
		},
		version: {
			type: String,
			default: null
		}
	},
	createdClientId: {
		type: String,
		default: null
	},
	createdIp: {
		type: String,
		default: null
	},
	kyc: {
		_id: false,
		kycId: {
			type: Number,
			default: null
		},
		identifyNumber: {
			type: String,
			default: null
		},
		state: {
			type: String,
			enum: _.values(generalConstant.ACCOUNT_KYC_STATE)
		},
		sentAt: {
			type: Date,
			default: null
		},
		reason: {
			type: String,
			default: null
		}
	},
	scope: [String],
	source: {
		type: String,
		enum: _.values(generalConstant.ACCOUNT_SOURCE),
		default: generalConstant.ACCOUNT_SOURCE.DIRECT
	},
	passwordTemp: {
		_id: false,
		password: {
			type: String,
			default: null
		},
		expiredAt: {
			type: Date,
			default: null
		}
	},
	countLoginFail: {
		type: Number,
		default: 0
	},
	extraData: {
		type: Array,
		default: []
	},
	isVerifiedEmail: {
		type: Boolean,
		default: false
	},
	tokenVerify: {
		type: String,
		default: null
	},
	requestChange: {
		type: Array,
		default: []
	},
	accountType: {
		type: String,
		default: null
	}
}, {
	collection: 'Account',
	versionKey: false,
	timestamps: true
});

Schema.index({ phone: 1 }, { sparse: true, unique: true });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'idAI',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
