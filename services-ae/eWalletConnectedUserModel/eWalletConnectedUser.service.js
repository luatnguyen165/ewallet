const DbService = require('moleculer-db');
const MongooseAdapter = require('moleculer-db-adapter-mongoose');
const MongooseAction = require('moleculer-db-adapter-mongoose-action');
const _ = require('lodash');
const eWalletConnectedUserModel = require('./model/eWalletConnectedUser.model');

module.exports = {
	name: 'eWalletConnectedUserModel',

	version: 1,

	mixins: [DbService],

	adapter: new MongooseAdapter(process.env.AE_MONGO_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		keepAlive: true

	}),

	model: eWalletConnectedUserModel,

	/**
   * Settings
   */
	settings: {
	},

	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: MongooseAction(),

	/**
   * Events
   */
	events: {

	},

	/**
   * Methods
   */
	methods: {
	},

	/**
   * Service created lifecycle event handler
   */
	created() {

	},

	/**
   * Service started lifecycle event handler
   */
	started() { },


	/**
   * Service stopped lifecycle event handler
   */

	// async stopped() {},

	async afterConnected() {
		this.logger.info('Connected successfully...');
	}
};
