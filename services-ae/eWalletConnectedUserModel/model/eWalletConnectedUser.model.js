const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: { 
    type: Number,
    required: true
  },
  phone: { 
    type: String,
    required: true
  },
  appId:{
    type: Number,
    required: true
  },
  userId:{
    type: String,
    default: null
  },
  timestamps:{
    type: String,
    default: null
  }
}, {
	collection: 'ConnectedUser',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: true, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
