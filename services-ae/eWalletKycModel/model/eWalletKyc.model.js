const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const { customAlphabet } = require('nanoid');
const { alphanumeric } = require('nanoid-dictionary');

const nanoId = customAlphabet(alphanumeric, 15);

const _ = require('lodash');
const AccountConstant = require('../constants/AccountConstant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	id: {
		type: Number,
		required: true,
		unique: true
	},
	accountId: {
		type: Number,
		required: true
	},
	fullname: {
		type: String,
		default: null
	},
	gender: {
		type: String
		// enum: _.values(AccountConstant.ACCOUNT_GENDER) // allow null
	},
	type: {
		type: String,
		enum: _.values(AccountConstant.ACCOUNT_KYC_TYPE),
		default: AccountConstant.ACCOUNT_KYC_TYPE.CMND
	},
	birthday: {
		type: Date,
		default: null
	},
	address: {
		street: { type: String, default: null },
		city: {
			title: { type: String, default: null },
			identifyCode: { type: String, default: null },
			path: { type: String, default: null }
		},
		district: {
			title: { type: String, default: null },
			identifyCode: { type: String, default: null },
			path: { type: String, default: null }
		},
		ward: {
			title: { type: String, default: null },
			identifyCode: { type: String, default: null },
			path: { type: String, default: null }
		}
	},
	identifyNumber: {
		type: String,
		default: null
	},
	issuedAt: {
		type: Date,
		default: null
	},
	placeOfIssue: {
		type: String,
		default: null
	},
	image: {
		front: {
			type: String
		},
		back: {
			type: String
		},
		state: {
			type: String,
			enum: _.values(AccountConstant.ACCOUNT_KYC_STATE),
			default: AccountConstant.ACCOUNT_KYC_STATE.PENDING
		}
	},
	video: {
		video: String,
		state: {
			type: String,
			enum: _.values(AccountConstant.ACCOUNT_KYC_STATE),
			default: AccountConstant.ACCOUNT_KYC_STATE.PENDING
		}
	},
	face: {
		face: String,
		state: {
			type: String,
			enum: _.values(AccountConstant.ACCOUNT_KYC_STATE),
			default: AccountConstant.ACCOUNT_KYC_STATE.PENDING
		}
	},
	identifyIC: {
		front: {
			type: String
		},
		back: {
			type: String
		},
		state: {
			type: String,
			enum: _.values(AccountConstant.ACCOUNT_KYC_STATE),
			default: AccountConstant.ACCOUNT_KYC_STATE.PENDING
		},
		reason: {
			type: String,
			default: null
		}
	},
	state: {
		type: String,
		enum: _.values(AccountConstant.ACCOUNT_KYC_STATE),
		required: true,
		default: AccountConstant.ACCOUNT_KYC_STATE.PENDING
	},
	email: {
		type: String,
		default: null
	},
	reason: {
		type: String,
		default: null
	},
	eKYC: {
		type: Boolean,
		default: true
	},
	approvedTimeExpected: {
		type: String,
		default: null
	},
	approvedAt: {
		type: Date,
		default: null
	},
	merchant: {
		_id: false,
		taxCode: {
			type: String,
			default: null
		},
		name: {
			type: String,
			default: null
		},
		shortName: {
			type: String,
			default: null
		},
		email: {
			type: String,
			default: null
		},
		website: {
			type: String,
			default: null
		},
		logo: {
			type: String,
			default: null
		},
		address: {
			type: String,
			default: null
		},
		business: {
			type: String,
			default: null
		},
		representative: {
			type: String,
			default: null
		},
		openTime: {
			type: String
		},
		state: {
			type: String,
			enum: _.values(AccountConstant.ACCOUNT_KYC_STATE),
			default: AccountConstant.ACCOUNT_KYC_STATE.PENDING
		},
		lincenseImage: [{
			type: String
		}]
	},
	shareholders: [{
		_id: false,
		fullname: {
			type: String,
			default: null
		},
		identifyNumber: {
			type: String,
			default: null
		},
		title: {
			type: String,
			default: null
		},
		capitalRatio: {
			type: String,
			default: null
		}
	}]
}, {
	collection: 'Kyc',
	versionKey: false,
	timestamps: true
});
/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-idAI`,
	field: 'idAI',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
