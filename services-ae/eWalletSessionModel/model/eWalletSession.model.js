const mongoose = require('mongoose');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
  accountId: {
    type: String,
    required: true
  },
  appId: {
    type: Number,
    default: null
  },
  clientInfo:{
    type: Object
  },
  lastLoginTime: {
    type: Date,
    default: new Date()
  },
  lastLogoutTime: {
    type: Date,
    default: null
  },
  ip:{
    type: String
  },
  state: {
    type: Boolean
  }
}, {
	collection: 'Session',
	versionKey: false,
	timestamps: true
});

Schema.index({ accountId: 1 }, { sparse: true, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
