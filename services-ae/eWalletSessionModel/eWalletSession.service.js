const DbService = require('moleculer-db');
const MongooseAdapter = require('moleculer-db-adapter-mongoose');
const MongooseAction = require('moleculer-db-adapter-mongoose-action');
const _ = require('lodash');
const eWalletSessionModel = require('./model/eWalletSession.model');

module.exports = {
	name: 'eWalletSessionModel',

	version: 1,

	mixins: [DbService],

	adapter: new MongooseAdapter(process.env.AE_MONGO_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		keepAlive: true

	}),

	model: eWalletSessionModel,

	/**
   * Settings
   */
	settings: {
	},

	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: MongooseAction(),

	/**
   * Events
   */
	events: {

	},

	/**
   * Methods
   */
	methods: {
	},

	/**
   * Service created lifecycle event handler
   */
	created() {

	},

	/**
   * Service started lifecycle event handler
   */
	started() { },


	/**
   * Service stopped lifecycle event handler
   */

	// async stopped() {},

	async afterConnected() {
		this.logger.info('Connected successfully...');
	}
};
